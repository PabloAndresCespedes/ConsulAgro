prompt --application/set_environment
set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_200100 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2020.03.31'
,p_release=>'20.1.0.00.13'
,p_default_workspace_id=>2933390818164287
,p_default_application_id=>100
,p_default_id_offset=>7656439127971453890
,p_default_owner=>'ADCS'
);
end;
/
 
prompt APPLICATION 100 - FINANZAS
--
-- Application Export:
--   Application:     100
--   Name:            FINANZAS
--   Date and Time:   13:43 Thursday September 1, 2022
--   Exported By:     ADMIN
--   Flashback:       0
--   Export Type:     Page Export
--   Manifest
--     PAGE: 8007
--   Manifest End
--   Version:         20.1.0.00.13
--   Instance ID:     218232033625678
--

begin
null;
end;
/
prompt --application/pages/delete_08007
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>8007);
end;
/
prompt --application/pages/page_08007
begin
wwv_flow_api.create_page(
 p_id=>8007
,p_user_interface_id=>wwv_flow_api.id(7567100396693658791)
,p_name=>unistr('FINI040 - REGISTRAR SOLICITUD Y APROBACI\00D3N DE CRED')
,p_alias=>unistr('FINI040-REGISTRAR-SOLICITUD-Y-APROBACI\00D3N-DE-CRED')
,p_step_title=>unistr('FINI040 - REGISTRAR SOLICITUD Y APROBACI\00D3N DE CRED')
,p_autocomplete_on_off=>'OFF'
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'function obtenerRegistrosSeleccionados(){',
'  var gridView = apex.region("files").widget().interactiveGrid("getViews").grid;',
'  var records;',
'  try {',
'    records = gridView.getSelectedRecords(); // te deja todos los items seleccionados en un arreglo',
'      console.log(records);    ',
'    var clave = "";',
'    var item = "";',
'    // La variable acum guarda todos los items seleccionados separados por ":"',
'    for (let index = 0; index < records.length; index++)',
'     {',
'            //clave = records[index][0];',
'            //item = records[index][2]; ',
'         clave = records[index][1];',
'         item = records[index][11]; ',
'        }',
'    apex.item(''P8007_FILE_ID'').setValue(clave);',
'    apex.item(''P8007_FILE_ID'').refresh();',
'    apex.item(''P8007_ITEM'').setValue(item);',
'    apex.item(''P8007_ITEM'').refresh();',
'    ',
'  } catch (e) {',
'    apex.item(''P8007_FILE_ID'').setValue('''');',
'    apex.item(''P8007_ITEM'').setValue('''');',
'  } finally{',
'    apex.server.process(''DUMMY'', { pageItems: ''#P8007_FILE_ID''}, { dataType: "text" });',
'    apex.server.process(''DUMMY'', { pageItems: ''#P8007_ITEM''}, { dataType: "text" });',
'  }',
'}',
'',
'function cargarImagen(seq_id){',
'    console.log(seq_id);',
'    apex.item(''P8007_SEQID_IMAGEN'').setValue(seq_id);',
'    apex.item(''P8007_SEQID_IMAGEN'').refresh();',
'}',
'',
'',
'function disable_cuotas(){',
'  var grid = apex.region("cuotas").call("getViews").grid;',
'  grid.view$.grid("option", "editable", false);',
'  grid.model.setOption("editable", false);',
'}',
'',
'',
'function enable_cuotas(){',
'  var grid = apex.region("cuotas").call("getViews").grid;',
'  grid.view$.grid("option", "editable", true);',
'  grid.model.setOption("editable", true);',
'}',
'',
'function disable_referencias(){',
'  var grid = apex.region("referencias").call("getViews").grid;',
'  grid.view$.grid("option", "editable", false);',
'  grid.model.setOption("editable", false);',
'}',
'',
'',
'function enable_referencias(){',
'  var grid = apex.region("referencias").call("getViews").grid;',
'  grid.view$.grid("option", "editable", true);',
'  grid.model.setOption("editable", true);',
'}',
'',
'function disable_financiamiento(){',
'  var grid = apex.region("financiamiento").call("getViews").grid;',
'  grid.view$.grid("option", "editable", false);',
'  grid.model.setOption("editable", false);',
'}',
'',
'',
'function enable_financiamiento(){',
'  var grid = apex.region("financiamiento").call("getViews").grid;',
'  grid.view$.grid("option", "editable", true);',
'  grid.model.setOption("editable", true);',
'}',
'',
'',
'var arreglo     = [];',
'',
'function sumar(){',
'var imp_total = 0; //fila 6',
'    ',
'var widget      = apex.region(''financiamiento'').widget();',
'var grid        = widget.interactiveGrid(''getViews'',''grid'');  ',
'var model       = grid.model; ',
'var results     = [];',
'arreglo     = [];',
'var i = 0;',
'    ',
'model.forEach(function(r) {',
'    var record = r;',
'    arreglo[i] = record;',
'    i++;',
'',
'})',
'',
'    console.log(arreglo);    ',
'   ',
'    for (let index = 0; index < arreglo.length; index++) {',
'        if (arreglo[index][6] != ""){',
'            imp_total += parseInt(arreglo[index][6]);    ',
'        }             ',
'    }',
'       ',
'    apex.item("P8007_S_TOT_IMP_MENSUAL").setValue(imp_total);',
'    apex.item(''P8007_S_TOT_IMP_MENSUAL'').refresh();',
'    ',
'}'))
,p_inline_css=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#P8007_S_FICHA_MONEDA{',
'    display:contents;',
'}',
'',
'#P8007_FARC_IMG{',
'    width: 60%;',
'    margin-left: 20%;',
'}',
'',
'#P8007_FSOL_NRO,#P8007_S_FICHA_TEC,#P8007_S_FSOL_FEC_SOL,#P8007_FSOL_VENDEDOR{',
'    background: #80bbe9;',
'}',
'',
'#P8007_TITULO{',
'    background: none;',
'    width: 100%;',
'    text-align: center;',
'    font-size: 18px;',
'}',
'',
'.myButton {',
'        background:linear-gradient(to bottom, #599bb3 5%, #408c99 100%);',
'        background-color:#599bb3;',
'        border-radius:8px;',
'        display:inline-block;',
'        cursor:pointer;',
'        color:#ffffff;',
'        font-family:Arial;',
'        font-size:13px;',
'        font-weight:bold;',
'        padding:3px 24px;',
'        text-decoration:none;',
'        text-shadow:0px 1px 0px #3d768a;',
'        width: 70%;',
'        margin-left: 15%;',
'}',
'.myButton:hover {',
'        background:linear-gradient(to bottom, #408c99 5%, #599bb3 100%);',
'        background-color:#408c99;',
'}',
'.myButton:active {',
'        position:relative;',
'        top:1px;',
'}',
'',
'#cuotas .no_editable {',
'    background: #dfdfdf;',
'}',
'',
'#financiamiento .no_editable{',
'    background: #dfdfdf;',
'}',
'',
'#P8007_S_NOMBRE,#P8007_S_COD_CLI,#P8007_S_FEC_NAC,#P8007_S_SEXO,',
'#P8007_S_DOCUMENTO,#P8007_S_ESTADO_CIVIL,#P8007_S_DEPARTAMENTO,',
'#P8007_S_CIUDAD,#P8007_S_DIRECCION,#P8007_S_REF_UBI,#P8007_S_VIVIENDA,',
'#P8007_S_RECIDE,#P8007_S_TELEFONO,#P8007_S_CONYUGE,#P8007_FSOL_GARANTE,',
'#P8007_S_NOM_GARANTE,#P8007_S_RUC_DV,#P8007_S_DV,#P8007_S_CATEG,#P8007_S_CATEG_DESC{',
'	    font-weight: 700;',
'}',
'',
'#P8007_S_MIEMBROS_UF,#P8007_S_APORTANTES_UF,#P8007_S_OTROS_ING,#P8007_S_ESTADO_SOL,',
'#P8007_S_MON_SIMBOLO,#P8007_S_DEPENDIENTES_UF,#P8007_S_ASIST_ESC,#P8007_S_VEND_NOM{',
'	    font-weight: 700;',
'}',
'',
'',
'#P8007_S_ACT_DOMI,#P8007_S_PARCELA_DESC,#P8007_S_PARCELA_UBI,',
'#P8007_S_HAS_PROPIAS,#P8007_FSOL_ANTIG_TERRENO,#P8007_S_UTIL_ALQ,',
'#P8007_S_HAS_ALQ,#P8007_FSOL_ANTIG_TRAB,#P8007_S_UTIL_PROPIAS,',
'#P8007_S_TOT_UTIL,#P8007_S_ACTIV_EDAD_INI,#P8007_FSOL_OBS_DIRE_PARC{',
'    font-weight: 700;',
'}',
'',
'#P8007_LABEL1{',
'    font-size: 12px;',
'}'))
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'ODURE'
,p_last_upd_yyyymmddhh24miss=>'20211125103322'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7839572132257719897)
,p_plug_name=>'REGISTRAR SOLICITUD Y APROBACION  DE CREDITOS'
,p_region_template_options=>'#DEFAULT#:t-Region--accent2:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_plug_source_type=>'NATIVE_DISPLAY_SELECTOR'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'STANDARD'
,p_attribute_02=>'N'
,p_attribute_03=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7839572205032719898)
,p_plug_name=>'PAGINA 01'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7839572612435719902)
,p_plug_name=>'solicitud'
,p_parent_plug_id=>wwv_flow_api.id(7839572205032719898)
,p_region_template_options=>'#DEFAULT#:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7844463297859176287)
,p_plug_name=>'datos'
,p_parent_plug_id=>wwv_flow_api.id(7839572612435719902)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7839572705547719903)
,p_plug_name=>'Datos Personales del Solicitante'
,p_parent_plug_id=>wwv_flow_api.id(7839572205032719898)
,p_region_template_options=>'#DEFAULT#:t-Region--accent3:t-Region--scrollBody:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7839572778913719904)
,p_plug_name=>'Datos de la Unidad Familiar'
,p_parent_plug_id=>wwv_flow_api.id(7839572205032719898)
,p_region_template_options=>'#DEFAULT#:t-Region--accent3:t-Region--scrollBody:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>30
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7839574056315719916)
,p_plug_name=>'izq'
,p_parent_plug_id=>wwv_flow_api.id(7839572778913719904)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>10
,p_plug_grid_column_span=>4
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7839574145764719917)
,p_plug_name=>'center'
,p_parent_plug_id=>wwv_flow_api.id(7839572778913719904)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_grid_column_span=>4
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7839574320841719919)
,p_plug_name=>'#  Edades de los hijos'
,p_parent_plug_id=>wwv_flow_api.id(7839574145764719917)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570304732549401240)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT rownum,',
'        FDEH_EDAD_HIJO',
'FROM FIN_FICHA_DET_HIJO ',
'WHERE FDEH_CLAVE_FICHA = :P8007_S_CLAVE_FICHA_TEC',
'AND FDEH_EMPR = :P_EMPRESA;'))
,p_plug_source_type=>'NATIVE_IR'
,p_ajax_items_to_submit=>'P8007_S_CLAVE_FICHA_TEC'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'MILLIMETERS'
,p_prn_paper_size=>'A4'
,p_prn_width=>297
,p_prn_height=>210
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header=>'#  Edades de los hijos'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#EEEEEE'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'bold'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#FFFFFF'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_prn_border_color=>'#666666'
,p_plug_header=>'<div style="overflow: auto; height: 150px;">'
,p_plug_footer=>'</div>'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(7839574381340719920)
,p_max_row_count=>'1000000'
,p_no_data_found_message=>'No posee hijos registrados'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_show_search_bar=>'N'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_owner=>'ADIAZ'
,p_internal_uid=>388737524579739342
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7839574695664719923)
,p_db_column_name=>'FDEH_EDAD_HIJO'
,p_display_order=>30
,p_column_identifier=>'B'
,p_column_label=>'Edades de los hijos'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7881124995299744516)
,p_db_column_name=>'ROWNUM'
,p_display_order=>40
,p_column_identifier=>'C'
,p_column_label=>'#'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(7844328115927102126)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'3934913'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'FDEH_EDAD_HIJO:ROWNUM'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7839574190407719918)
,p_plug_name=>'der'
,p_parent_plug_id=>wwv_flow_api.id(7839572778913719904)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_grid_column_span=>4
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7839572913038719905)
,p_plug_name=>unistr('Direcci\00F3n de Parcelas')
,p_parent_plug_id=>wwv_flow_api.id(7839572205032719898)
,p_region_template_options=>'#DEFAULT#:t-Region--accent3:t-Region--scrollBody:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>40
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7839573013604719906)
,p_plug_name=>'Bienes que Posee'
,p_parent_plug_id=>wwv_flow_api.id(7839572205032719898)
,p_region_template_options=>'#DEFAULT#:t-Region--accent3:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>50
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7839572270805719899)
,p_plug_name=>'PAGINA 02'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>50
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7844701412444669780)
,p_plug_name=>unistr('Datos del Cr\00E9dito')
,p_parent_plug_id=>wwv_flow_api.id(7839572270805719899)
,p_region_template_options=>'#DEFAULT#:t-Region--accent3:t-Region--scrollBody:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7844703824843669804)
,p_plug_name=>'total'
,p_parent_plug_id=>wwv_flow_api.id(7844701412444669780)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>30
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7906803444366296393)
,p_plug_name=>'Desgloce del destino de financiamiento DUPLICADO'
,p_region_name=>'financiamiento'
,p_parent_plug_id=>wwv_flow_api.id(7844701412444669780)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570304732549401240)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  SEQ_ID           FDDF_ITEM, ',
'        C001             FDDF_DEST_FINAN,',
'        C002             S_DESGLOCE,',
'        to_number(C003)  FDDF_CANT,',
'        to_number(C004)  FDDF_PRECIO_MON,',
'        C005             S_TIPO_MON,',
'        to_number(C006)  S_IMP_MENSUAL',
' FROM   APEX_COLLECTIONS',
' WHERE  COLLECTION_NAME = ''FINI040_DESGLOCE'';'))
,p_plug_source_type=>'NATIVE_IG'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_prn_units=>'MILLIMETERS'
,p_prn_paper_size=>'A4'
,p_prn_width=>297
,p_prn_height=>210
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header=>'Desgloce del destino de financiamiento DUPLICADO'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#EEEEEE'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'bold'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#FFFFFF'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_prn_border_color=>'#666666'
,p_plug_header=>'<div style="overflow: auto; height: 150px;">'
,p_plug_footer=>'<div>'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906804403748296403)
,p_name=>'FDDF_ITEM'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FDDF_ITEM'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>30
,p_attribute_01=>'N'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>true
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906804509339296404)
,p_name=>'FDDF_DEST_FINAN'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FDDF_DEST_FINAN'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>40
,p_attribute_01=>'N'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906804652947296405)
,p_name=>'S_DESGLOCE'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'S_DESGLOCE'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Desgloce del destino de financiamiento'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>50
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_item_attributes=>'readonly'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'function(config) {',
'    config.defaultGridColumnOptions = {',
'        columnCssClasses: "no_editable"',
'    }',
'    return config;',
'}'))
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906804719063296406)
,p_name=>'FDDF_CANT'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FDDF_CANT'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Cantidad'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>60
,p_value_alignment=>'RIGHT'
,p_attribute_03=>'right'
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906804767101296407)
,p_name=>'FDDF_PRECIO_MON'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FDDF_PRECIO_MON'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Precio'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>70
,p_value_alignment=>'RIGHT'
,p_attribute_03=>'right'
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906804915986296408)
,p_name=>'S_TIPO_MON'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'S_TIPO_MON'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Moneda'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>80
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_item_attributes=>'readonly'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'function(config) {',
'    config.defaultGridColumnOptions = {',
'        columnCssClasses: "no_editable"',
'    }',
'    return config;',
'}'))
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906805043022296409)
,p_name=>'S_IMP_MENSUAL'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'S_IMP_MENSUAL'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Importe Mensual'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>90
,p_value_alignment=>'RIGHT'
,p_attribute_03=>'right'
,p_item_attributes=>'readonly'
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'function(config) {',
'    config.defaultGridColumnOptions = {',
'        columnCssClasses: "no_editable"',
'    }',
'    return config;',
'}'))
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906805416239296413)
,p_name=>'APEX$ROW_ACTION'
,p_item_type=>'NATIVE_ROW_ACTION'
,p_display_sequence=>20
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906805471649296414)
,p_name=>'APEX$ROW_SELECTOR'
,p_item_type=>'NATIVE_ROW_SELECTOR'
,p_display_sequence=>10
,p_attribute_01=>'N'
,p_attribute_03=>'Y'
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(7906804349237296402)
,p_internal_uid=>455967492476315824
,p_is_editable=>true
,p_edit_operations=>'u'
,p_lost_update_check_type=>'VALUES'
,p_submit_checked_rows=>false
,p_lazy_loading=>false
,p_requires_filter=>false
,p_select_first_row=>true
,p_fixed_row_height=>true
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>false
,p_toolbar_buttons=>null
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_flashback=>false
,p_define_chart_view=>false
,p_enable_download=>false
,p_download_formats=>null
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(7906836178749690769)
,p_interactive_grid_id=>wwv_flow_api.id(7906804349237296402)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(7906836297695690769)
,p_report_id=>wwv_flow_api.id(7906836178749690769)
,p_view_type=>'GRID'
,p_stretch_columns=>true
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906836775234690770)
,p_view_id=>wwv_flow_api.id(7906836297695690769)
,p_display_seq=>1
,p_column_id=>wwv_flow_api.id(7906804403748296403)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906837293288690772)
,p_view_id=>wwv_flow_api.id(7906836297695690769)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(7906804509339296404)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906837825709690774)
,p_view_id=>wwv_flow_api.id(7906836297695690769)
,p_display_seq=>3
,p_column_id=>wwv_flow_api.id(7906804652947296405)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906838353500690775)
,p_view_id=>wwv_flow_api.id(7906836297695690769)
,p_display_seq=>5
,p_column_id=>wwv_flow_api.id(7906804719063296406)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906838795636690777)
,p_view_id=>wwv_flow_api.id(7906836297695690769)
,p_display_seq=>6
,p_column_id=>wwv_flow_api.id(7906804767101296407)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906839335661690778)
,p_view_id=>wwv_flow_api.id(7906836297695690769)
,p_display_seq=>4
,p_column_id=>wwv_flow_api.id(7906804915986296408)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906839776315690780)
,p_view_id=>wwv_flow_api.id(7906836297695690769)
,p_display_seq=>7
,p_column_id=>wwv_flow_api.id(7906805043022296409)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906841181158698175)
,p_view_id=>wwv_flow_api.id(7906836297695690769)
,p_display_seq=>0
,p_column_id=>wwv_flow_api.id(7906805416239296413)
,p_is_visible=>false
,p_is_frozen=>true
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7844701501814669781)
,p_plug_name=>'Experiencia Crediticia'
,p_parent_plug_id=>wwv_flow_api.id(7839572270805719899)
,p_region_template_options=>'#DEFAULT#:t-Region--accent3:t-Region--scrollBody:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7844701671881669783)
,p_plug_name=>'Referencias Comerciales/Personales'
,p_parent_plug_id=>wwv_flow_api.id(7839572270805719899)
,p_region_template_options=>'#DEFAULT#:t-Region--accent3:t-Region--scrollBody:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>40
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7906264888362027015)
,p_plug_name=>'Reporte Referencias comerciales/personales DUPLICADO'
,p_region_name=>'referencias'
,p_parent_plug_id=>wwv_flow_api.id(7844701671881669783)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570304732549401240)
,p_plug_display_sequence=>30
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'    select SEQ_ID ITEM,',
'           C001 FREF_CLAVE_SOL,',
'           C002 FREF_ITEM,',
'           C003 FREF_TIPO_REF,',
'           C004 FREF_NOM,',
'           C005 FREF_DIR,',
'           C006 FREF_TEL,',
'           C007 FREF_LUGAR_ORIGEN_REPLICA,',
'           C008 FREF_EMPR',
'    from  APEX_COLLECTIONS',
'    WHERE COLLECTION_NAME = ''FINI040_REFERENCIAS'';'))
,p_plug_source_type=>'NATIVE_IG'
,p_ajax_items_to_submit=>'P8007_FSOL_CLAVE'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_prn_units=>'MILLIMETERS'
,p_prn_paper_size=>'A4'
,p_prn_width=>297
,p_prn_height=>210
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header=>'Reporte Referencias comerciales/personales DUPLICADO'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#EEEEEE'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'bold'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#FFFFFF'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_prn_border_color=>'#666666'
,p_plug_header=>'<div style="overflow: auto; height: 120px;">'
,p_plug_footer=>'</div>'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906266104025027027)
,p_name=>'ITEM'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'ITEM'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>30
,p_attribute_01=>'N'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>true
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906266180731027028)
,p_name=>'FREF_CLAVE_SOL'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FREF_CLAVE_SOL'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>40
,p_attribute_01=>'N'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906801961253296379)
,p_name=>'FREF_ITEM'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FREF_ITEM'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>50
,p_attribute_01=>'N'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906802117002296380)
,p_name=>'FREF_TIPO_REF'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FREF_TIPO_REF'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Tipo'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>60
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906802231019296381)
,p_name=>'FREF_NOM'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FREF_NOM'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>unistr('Nombre/Raz\00F3n social')
,p_heading_alignment=>'CENTER'
,p_display_sequence=>70
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
end;
/
begin
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906802300755296382)
,p_name=>'FREF_DIR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FREF_DIR'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>unistr('Direcci\00F3n')
,p_heading_alignment=>'CENTER'
,p_display_sequence=>80
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906802420061296383)
,p_name=>'FREF_TEL'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FREF_TEL'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>unistr('Tel\00E9fono')
,p_heading_alignment=>'CENTER'
,p_display_sequence=>90
,p_value_alignment=>'RIGHT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906802522959296384)
,p_name=>'FREF_LUGAR_ORIGEN_REPLICA'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FREF_LUGAR_ORIGEN_REPLICA'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>100
,p_attribute_01=>'N'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906802652914296385)
,p_name=>'FREF_EMPR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FREF_EMPR'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>110
,p_attribute_01=>'N'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906803041630296389)
,p_name=>'APEX$ROW_ACTION'
,p_item_type=>'NATIVE_ROW_ACTION'
,p_display_sequence=>20
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906803119588296390)
,p_name=>'APEX$ROW_SELECTOR'
,p_item_type=>'NATIVE_ROW_SELECTOR'
,p_display_sequence=>10
,p_attribute_01=>'Y'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(7906266019021027026)
,p_internal_uid=>455429162260046448
,p_is_editable=>true
,p_edit_operations=>'i:u'
,p_lost_update_check_type=>'VALUES'
,p_add_row_if_empty=>true
,p_submit_checked_rows=>false
,p_lazy_loading=>false
,p_requires_filter=>false
,p_select_first_row=>true
,p_fixed_row_height=>true
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>false
,p_toolbar_buttons=>null
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_flashback=>false
,p_define_chart_view=>false
,p_enable_download=>false
,p_download_formats=>null
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(7906807943639307340)
,p_interactive_grid_id=>wwv_flow_api.id(7906266019021027026)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(7906807969245307340)
,p_report_id=>wwv_flow_api.id(7906807943639307340)
,p_view_type=>'GRID'
,p_stretch_columns=>true
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906808486160307342)
,p_view_id=>wwv_flow_api.id(7906807969245307340)
,p_display_seq=>1
,p_column_id=>wwv_flow_api.id(7906266104025027027)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906808964124307344)
,p_view_id=>wwv_flow_api.id(7906807969245307340)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(7906266180731027028)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906809465700307346)
,p_view_id=>wwv_flow_api.id(7906807969245307340)
,p_display_seq=>3
,p_column_id=>wwv_flow_api.id(7906801961253296379)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906809989642307347)
,p_view_id=>wwv_flow_api.id(7906807969245307340)
,p_display_seq=>4
,p_column_id=>wwv_flow_api.id(7906802117002296380)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906810550373307349)
,p_view_id=>wwv_flow_api.id(7906807969245307340)
,p_display_seq=>5
,p_column_id=>wwv_flow_api.id(7906802231019296381)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906811053084307350)
,p_view_id=>wwv_flow_api.id(7906807969245307340)
,p_display_seq=>6
,p_column_id=>wwv_flow_api.id(7906802300755296382)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906811536207307352)
,p_view_id=>wwv_flow_api.id(7906807969245307340)
,p_display_seq=>7
,p_column_id=>wwv_flow_api.id(7906802420061296383)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906811996152307354)
,p_view_id=>wwv_flow_api.id(7906807969245307340)
,p_display_seq=>8
,p_column_id=>wwv_flow_api.id(7906802522959296384)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906812461842307355)
,p_view_id=>wwv_flow_api.id(7906807969245307340)
,p_display_seq=>9
,p_column_id=>wwv_flow_api.id(7906802652914296385)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906815278782320815)
,p_view_id=>wwv_flow_api.id(7906807969245307340)
,p_display_seq=>0
,p_column_id=>wwv_flow_api.id(7906803041630296389)
,p_is_visible=>false
,p_is_frozen=>true
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7844701840091669784)
,p_plug_name=>unistr('Propuesta de Cr\00E9dito')
,p_parent_plug_id=>wwv_flow_api.id(7839572270805719899)
,p_region_template_options=>'#DEFAULT#:t-Region--accent3:t-Region--scrollBody:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>50
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7844701929666669785)
,p_plug_name=>unistr('Resoluci\00F3n de Aprobaci\00F3n del Comit\00E9 de Cr\00E9ditos')
,p_parent_plug_id=>wwv_flow_api.id(7839572270805719899)
,p_region_template_options=>'#DEFAULT#:t-Region--accent3:t-Region--scrollBody:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>60
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7844810240638974982)
,p_plug_name=>'izq'
,p_parent_plug_id=>wwv_flow_api.id(7844701929666669785)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>10
,p_plug_grid_column_span=>4
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7844810740464974987)
,p_plug_name=>'New'
,p_parent_plug_id=>wwv_flow_api.id(7844810240638974982)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(8191053717011139456)
,p_plug_name=>'Tipo Vencimiento'
,p_parent_plug_id=>wwv_flow_api.id(7844810240638974982)
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7844810262941974983)
,p_plug_name=>'centro'
,p_parent_plug_id=>wwv_flow_api.id(7844701929666669785)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_grid_column_span=>4
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7844811040435974990)
,p_plug_name=>'New'
,p_parent_plug_id=>wwv_flow_api.id(7844810262941974983)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(8191056623256162676)
,p_plug_name=>unistr('Tipo Generaci\00F3n de Cuotas')
,p_parent_plug_id=>wwv_flow_api.id(7844810262941974983)
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7844810368447974984)
,p_plug_name=>'der'
,p_parent_plug_id=>wwv_flow_api.id(7844701929666669785)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_grid_column_span=>4
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7839572383813719900)
,p_plug_name=>'PAGINA 03'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>60
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7844811216140974992)
,p_plug_name=>'Adjuntar Archivos'
,p_parent_plug_id=>wwv_flow_api.id(7839572383813719900)
,p_region_template_options=>'#DEFAULT#:t-Region--accent3:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7844811569809974996)
,p_plug_name=>'reporte de archivos'
,p_region_name=>'files'
,p_parent_plug_id=>wwv_flow_api.id(7844811216140974992)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570304732549401240)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*SELECT * ',
'FROM APEX_APPLICATION_TEMP_FILES*/',
'',
'select FARC_CLAVE_SOL,',
'       FARC_EMPR,',
'       FARC_ITEM,',
'       FARC_TIPO,',
'       FARC_DESC,',
'       FARC_NOM_ARCH,',
'       FARC_LUGAR_ORIGEN_REPLICA,',
'       --FARC_IMG,',
'       FARC_IND_CROQUIS,',
'       null IMAGEN,',
'       null ARCHIVO,',
'       null VER',
'from FIN_FICHA_SOL BPER,',
'     FIN_FICHA_SOL_DET_ARCH BFOTO',
'where BPER.FSOL_CLAVE = BFOTO.FARC_CLAVE_SOL  ',
'AND   BPER.FSOL_EMPR  = BFOTO.FARC_EMPR  ',
'AND BPER.FSOL_CLAVE = :P8007_FSOL_CLAVE',
'AND BPER.FSOL_EMPR = :P_EMPRESA'))
,p_plug_source_type=>'NATIVE_IG'
,p_ajax_items_to_submit=>'P8007_FSOL_CLAVE'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_display_condition_type=>'NEVER'
,p_prn_units=>'MILLIMETERS'
,p_prn_paper_size=>'A4'
,p_prn_width=>297
,p_prn_height=>210
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header=>'reporte de archivos'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#EEEEEE'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'bold'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#FFFFFF'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_prn_border_color=>'#666666'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7881868579009129423)
,p_name=>'FARC_CLAVE_SOL'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FARC_CLAVE_SOL'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>30
,p_attribute_01=>'Y'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7881868659264129424)
,p_name=>'FARC_EMPR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FARC_EMPR'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>40
,p_attribute_01=>'Y'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7881868771692129425)
,p_name=>'FARC_ITEM'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FARC_ITEM'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'#'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>50
,p_value_alignment=>'CENTER'
,p_attribute_03=>'right'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7881868908686129426)
,p_name=>'FARC_TIPO'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FARC_TIPO'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>60
,p_attribute_01=>'Y'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7881869011251129427)
,p_name=>'FARC_DESC'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FARC_DESC'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>unistr('Descripci\00F3n del Archivo')
,p_heading_alignment=>'CENTER'
,p_display_sequence=>70
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_is_required=>true
,p_max_length=>50
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7881869074119129428)
,p_name=>'FARC_NOM_ARCH'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FARC_NOM_ARCH'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXTAREA'
,p_heading=>'Nombre del Archivo'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>80
,p_value_alignment=>'LEFT'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
,p_is_required=>false
,p_max_length=>100
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7884483682475251879)
,p_name=>'FARC_LUGAR_ORIGEN_REPLICA'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FARC_LUGAR_ORIGEN_REPLICA'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>90
,p_attribute_01=>'Y'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7884483966240251882)
,p_name=>'IMAGEN'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'IMAGEN'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HTML_EXPRESSION'
,p_heading=>'&nbsp'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>100
,p_value_alignment=>'LEFT'
,p_attribute_01=>'<input type="button" value="Imagen" class="myButton"/>'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7884484107768251883)
,p_name=>'ARCHIVO'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'ARCHIVO'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_LINK'
,p_heading=>'&nbsp'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>110
,p_value_alignment=>'LEFT'
,p_link_target=>'f?p=&APP_ID.:8008:&SESSION.::&DEBUG.:::'
,p_link_text=>'&ARCHIVO.'
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7884484193239251884)
,p_name=>'VER'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'VER'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HTML_EXPRESSION'
,p_heading=>'&nbsp'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>120
,p_value_alignment=>'LEFT'
,p_attribute_01=>'<input type="button" value="Ver" class="myButton"/>'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7884484544462251887)
,p_name=>'FARC_IND_CROQUIS'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FARC_IND_CROQUIS'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_CHECKBOX'
,p_heading=>unistr('\00BFEs croquis?')
,p_heading_alignment=>'CENTER'
,p_display_sequence=>130
,p_value_alignment=>'LEFT'
,p_attribute_01=>'2'
,p_is_required=>false
,p_lov_type=>'STATIC'
,p_lov_source=>'STATIC:Si;S,No;N'
,p_lov_display_extra=>false
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'LOV'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(7844811721824974997)
,p_internal_uid=>393974865063994419
,p_is_editable=>false
,p_lazy_loading=>false
,p_requires_filter=>false
,p_select_first_row=>true
,p_fixed_row_height=>true
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>true
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_flashback=>true
,p_define_chart_view=>true
,p_enable_download=>true
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(7844869493397139832)
,p_interactive_grid_id=>wwv_flow_api.id(7844811721824974997)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(7844869581013139833)
,p_report_id=>wwv_flow_api.id(7844869493397139832)
,p_view_type=>'GRID'
,p_stretch_columns=>true
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7884489661026254485)
,p_view_id=>wwv_flow_api.id(7844869581013139833)
,p_display_seq=>8
,p_column_id=>wwv_flow_api.id(7881868579009129423)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7884490163992254488)
,p_view_id=>wwv_flow_api.id(7844869581013139833)
,p_display_seq=>9
,p_column_id=>wwv_flow_api.id(7881868659264129424)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7884490670710254490)
,p_view_id=>wwv_flow_api.id(7844869581013139833)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(7881868771692129425)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7884491226370254492)
,p_view_id=>wwv_flow_api.id(7844869581013139833)
,p_display_seq=>11
,p_column_id=>wwv_flow_api.id(7881868908686129426)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7884491680558254494)
,p_view_id=>wwv_flow_api.id(7844869581013139833)
,p_display_seq=>4
,p_column_id=>wwv_flow_api.id(7881869011251129427)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7884492246202254495)
,p_view_id=>wwv_flow_api.id(7844869581013139833)
,p_display_seq=>5
,p_column_id=>wwv_flow_api.id(7881869074119129428)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7884492723477254497)
,p_view_id=>wwv_flow_api.id(7844869581013139833)
,p_display_seq=>14
,p_column_id=>wwv_flow_api.id(7884483682475251879)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7884494196900254502)
,p_view_id=>wwv_flow_api.id(7844869581013139833)
,p_display_seq=>8
,p_column_id=>wwv_flow_api.id(7884483966240251882)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7884494689791254504)
,p_view_id=>wwv_flow_api.id(7844869581013139833)
,p_display_seq=>9
,p_column_id=>wwv_flow_api.id(7884484107768251883)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7884495168263254505)
,p_view_id=>wwv_flow_api.id(7844869581013139833)
,p_display_seq=>10
,p_column_id=>wwv_flow_api.id(7884484193239251884)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7884508738705312998)
,p_view_id=>wwv_flow_api.id(7844869581013139833)
,p_display_seq=>7
,p_column_id=>wwv_flow_api.id(7884484544462251887)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7885754100544413798)
,p_plug_name=>'reporte de archivos DUPLICADO'
,p_region_name=>'files'
,p_parent_plug_id=>wwv_flow_api.id(7844811216140974992)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570304732549401240)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select SEQ_ID NRO_ITEM,',
'       C001 FARC_CLAVE_SOL,',
'       C002 FARC_EMPR,',
'       C003 FARC_ITEM,',
'       C004 FARC_TIPO,',
'       C005 FARC_DESC,',
'       C006 FARC_NOM_ARCH,',
'       C007 FARC_LUGAR_ORIGEN_REPLICA,',
'       --FARC_IMG,',
'       C008 FARC_IND_CROQUIS,',
'       null IMAGEN,',
'       null ARCHIVO,',
'       null VER',
'from APEX_COLLECTIONS',
'WHERE COLLECTION_NAME = ''FINI040_ARCHIVOS'';'))
,p_plug_source_type=>'NATIVE_IG'
,p_ajax_items_to_submit=>'P8007_FSOL_CLAVE'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7885754346833413800)
,p_name=>'FARC_IND_CROQUIS'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FARC_IND_CROQUIS'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_CHECKBOX'
,p_heading=>unistr('\00BFEs croquis?')
,p_heading_alignment=>'CENTER'
,p_display_sequence=>10
,p_value_alignment=>'LEFT'
,p_attribute_01=>'2'
,p_is_required=>false
,p_lov_type=>'STATIC'
,p_lov_source=>'STATIC:Si;S,No;N'
,p_lov_display_extra=>false
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'LOV'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7885754366324413801)
,p_name=>'FARC_CLAVE_SOL'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FARC_CLAVE_SOL'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>20
,p_attribute_01=>'Y'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7885754468062413802)
,p_name=>'FARC_EMPR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FARC_EMPR'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>30
,p_attribute_01=>'Y'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7885754583556413803)
,p_name=>'FARC_ITEM'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FARC_ITEM'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'#'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>40
,p_value_alignment=>'CENTER'
,p_attribute_03=>'right'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7885754730405413804)
,p_name=>'FARC_TIPO'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FARC_TIPO'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>50
,p_attribute_01=>'Y'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7885754787293413805)
,p_name=>'FARC_DESC'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FARC_DESC'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>unistr('Descripci\00F3n del Archivo')
,p_heading_alignment=>'CENTER'
,p_display_sequence=>60
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7885754953870413806)
,p_name=>'FARC_NOM_ARCH'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FARC_NOM_ARCH'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXTAREA'
,p_heading=>'Nombre del Archivo'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>70
,p_value_alignment=>'LEFT'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7885754994144413807)
,p_name=>'FARC_LUGAR_ORIGEN_REPLICA'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FARC_LUGAR_ORIGEN_REPLICA'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>80
,p_attribute_01=>'Y'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7885755090908413808)
,p_name=>'IMAGEN'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'IMAGEN'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HTML_EXPRESSION'
,p_heading=>'&nbsp'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>90
,p_value_alignment=>'LEFT'
,p_attribute_01=>'<input type="button" value="Imagen" class="myButton"/>'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7885755196040413809)
,p_name=>'ARCHIVO'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'ARCHIVO'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HTML_EXPRESSION'
,p_heading=>'&nbsp'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>100
,p_value_alignment=>'LEFT'
,p_attribute_01=>'<input type="button" value="Archivo" class="myButton"/>'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
end;
/
begin
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7885755322175413810)
,p_name=>'VER'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'VER'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HTML_EXPRESSION'
,p_heading=>'&nbsp'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>110
,p_value_alignment=>'LEFT'
,p_attribute_01=>'<input type="button" value="Ver" class="myButton"/>'
,p_link_target=>'f?p=&APP_ID.:8008:&SESSION.::&DEBUG.::P8008_SEQ_ID:&NRO_ITEM.'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7885755425759413811)
,p_name=>'NRO_ITEM'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'NRO_ITEM'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>120
,p_attribute_01=>'N'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(7885754240890413799)
,p_internal_uid=>434917384129433221
,p_is_editable=>false
,p_lazy_loading=>false
,p_requires_filter=>false
,p_select_first_row=>true
,p_fixed_row_height=>true
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>true
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_flashback=>true
,p_define_chart_view=>true
,p_enable_download=>true
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(7885771698949459260)
,p_interactive_grid_id=>wwv_flow_api.id(7885754240890413799)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(7885771843596459261)
,p_report_id=>wwv_flow_api.id(7885771698949459260)
,p_view_type=>'GRID'
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7885772322755459263)
,p_view_id=>wwv_flow_api.id(7885771843596459261)
,p_display_seq=>1
,p_column_id=>wwv_flow_api.id(7885754346833413800)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7885772815815459265)
,p_view_id=>wwv_flow_api.id(7885771843596459261)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(7885754366324413801)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7885773347342459267)
,p_view_id=>wwv_flow_api.id(7885771843596459261)
,p_display_seq=>3
,p_column_id=>wwv_flow_api.id(7885754468062413802)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7885773765403459269)
,p_view_id=>wwv_flow_api.id(7885771843596459261)
,p_display_seq=>4
,p_column_id=>wwv_flow_api.id(7885754583556413803)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7885774302562459270)
,p_view_id=>wwv_flow_api.id(7885771843596459261)
,p_display_seq=>5
,p_column_id=>wwv_flow_api.id(7885754730405413804)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7885774799422459272)
,p_view_id=>wwv_flow_api.id(7885771843596459261)
,p_display_seq=>6
,p_column_id=>wwv_flow_api.id(7885754787293413805)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7885775343494459273)
,p_view_id=>wwv_flow_api.id(7885771843596459261)
,p_display_seq=>7
,p_column_id=>wwv_flow_api.id(7885754953870413806)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7885775847467459275)
,p_view_id=>wwv_flow_api.id(7885771843596459261)
,p_display_seq=>8
,p_column_id=>wwv_flow_api.id(7885754994144413807)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7885776304093459278)
,p_view_id=>wwv_flow_api.id(7885771843596459261)
,p_display_seq=>9
,p_column_id=>wwv_flow_api.id(7885755090908413808)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7885776764468459280)
,p_view_id=>wwv_flow_api.id(7885771843596459261)
,p_display_seq=>10
,p_column_id=>wwv_flow_api.id(7885755196040413809)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7885777326713459282)
,p_view_id=>wwv_flow_api.id(7885771843596459261)
,p_display_seq=>11
,p_column_id=>wwv_flow_api.id(7885755322175413810)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7885777813786459283)
,p_view_id=>wwv_flow_api.id(7885771843596459261)
,p_display_seq=>12
,p_column_id=>wwv_flow_api.id(7885755425759413811)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7844811274870974993)
,p_plug_name=>unistr('CROQUIS DE UBICACI\00D3N DEL DOMICILIO Y LA FINCA')
,p_parent_plug_id=>wwv_flow_api.id(7839572383813719900)
,p_region_template_options=>'#DEFAULT#:t-Region--accent3:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_display_condition_type=>'NEVER'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7885760695007457303)
,p_plug_name=>'IMAGEN'
,p_parent_plug_id=>wwv_flow_api.id(7844811274870974993)
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<p align="center">',
'<iframe src="f?p=&APP_ID.:0:&SESSION.:APPLICATION_PROCESS=show_image:NO::P8007_SEQID_IMAGEN,&P8007_SEQID_IMAGEN." width="99%" height="1000">',
'</iframe>',
'</p>'))
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7839572516674719901)
,p_plug_name=>'PAGINA 04'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>70
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7906261851701026984)
,p_plug_name=>'Cuotas Manuales DUPLICADA'
,p_region_name=>'cuotas'
,p_parent_plug_id=>wwv_flow_api.id(7839572516674719901)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570304732549401240)
,p_plug_display_sequence=>30
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select SEQ_ID NRO_ITEM,',
'       C001 FSDC_CLAVE,',
'       C002 FSDC_EMPR,',
'       C003 FSDC_ITEM,',
'       C004 FSDC_FEC_VENC,',
'       C005 FSDC_SALDO_CAPITAL,',
'       C006 FSDC_CAPITAL,',
'       C007 FSDC_INTERES,',
'       C008 S_IMP_CUO,',
'       C009 FSDC_LUGAR_ORIGEN_REPLICA,',
'       C010 S_MON_SIMB',
'from APEX_COLLECTIONS',
'WHERE COLLECTION_NAME = ''FINI040_CUOTAS'';'))
,p_plug_source_type=>'NATIVE_IG'
,p_ajax_items_to_submit=>'P8007_S_FSOL_FEC_PRIM_VTO,P8007_FSOL_CLAVE'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_prn_units=>'MILLIMETERS'
,p_prn_paper_size=>'A4'
,p_prn_width=>297
,p_prn_height=>210
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header=>'Cuotas Manuales DUPLICADA'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#EEEEEE'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'bold'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#FFFFFF'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_prn_border_color=>'#666666'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906263238105026998)
,p_name=>'NRO_ITEM'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'NRO_ITEM'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>30
,p_attribute_01=>'N'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>true
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906263269404026999)
,p_name=>'FSDC_CLAVE'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FSDC_CLAVE'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>40
,p_attribute_01=>'N'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906263372729027000)
,p_name=>'FSDC_EMPR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FSDC_EMPR'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>50
,p_attribute_01=>'N'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906263538149027001)
,p_name=>'FSDC_ITEM'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FSDC_ITEM'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'&nbsp'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>60
,p_value_alignment=>'RIGHT'
,p_attribute_05=>'BOTH'
,p_item_attributes=>'readonly'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906263620205027002)
,p_name=>'FSDC_FEC_VENC'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FSDC_FEC_VENC'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Fecha Venc.'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>70
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906263728187027003)
,p_name=>'FSDC_SALDO_CAPITAL'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FSDC_SALDO_CAPITAL'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Saldo Cap.'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>80
,p_value_alignment=>'RIGHT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906263804843027004)
,p_name=>'FSDC_CAPITAL'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FSDC_CAPITAL'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Capital'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>90
,p_value_alignment=>'RIGHT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906263882770027005)
,p_name=>'FSDC_INTERES'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FSDC_INTERES'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>unistr('Inter\00E9s')
,p_heading_alignment=>'CENTER'
,p_display_sequence=>100
,p_value_alignment=>'RIGHT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906264040493027006)
,p_name=>'S_IMP_CUO'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'S_IMP_CUO'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Cuota'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>110
,p_value_alignment=>'RIGHT'
,p_attribute_05=>'BOTH'
,p_item_attributes=>'readonly'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'function(config) {',
'    config.defaultGridColumnOptions = {',
'        columnCssClasses: "no_editable"',
'    }',
'    return config;',
'}'))
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906264114390027007)
,p_name=>'FSDC_LUGAR_ORIGEN_REPLICA'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'FSDC_LUGAR_ORIGEN_REPLICA'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>120
,p_attribute_01=>'N'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906264211709027008)
,p_name=>'S_MON_SIMB'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'S_MON_SIMB'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'&nbsp'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>130
,p_value_alignment=>'CENTER'
,p_attribute_05=>'BOTH'
,p_item_attributes=>'readonly'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'function(config) {',
'    config.defaultGridColumnOptions = {',
'        columnCssClasses: "no_editable"',
'    }',
'    return config;',
'}'))
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906264331980027009)
,p_name=>'APEX$ROW_ACTION'
,p_item_type=>'NATIVE_ROW_ACTION'
,p_display_sequence=>20
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(7906264387393027010)
,p_name=>'APEX$ROW_SELECTOR'
,p_item_type=>'NATIVE_ROW_SELECTOR'
,p_display_sequence=>10
,p_attribute_01=>'N'
,p_attribute_03=>'Y'
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(7906263143874026997)
,p_internal_uid=>455426287113046419
,p_is_editable=>true
,p_edit_operations=>'i:u'
,p_lost_update_check_type=>'VALUES'
,p_add_row_if_empty=>true
,p_submit_checked_rows=>false
,p_lazy_loading=>false
,p_requires_filter=>false
,p_select_first_row=>true
,p_fixed_row_height=>true
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>false
,p_toolbar_buttons=>null
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_flashback=>false
,p_define_chart_view=>false
,p_enable_download=>false
,p_download_formats=>null
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(7906544359583428922)
,p_interactive_grid_id=>wwv_flow_api.id(7906263143874026997)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(7906544526388428923)
,p_report_id=>wwv_flow_api.id(7906544359583428922)
,p_view_type=>'GRID'
,p_stretch_columns=>true
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906545009062428926)
,p_view_id=>wwv_flow_api.id(7906544526388428923)
,p_display_seq=>1
,p_column_id=>wwv_flow_api.id(7906263238105026998)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906545420357428930)
,p_view_id=>wwv_flow_api.id(7906544526388428923)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(7906263269404026999)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906545945142428932)
,p_view_id=>wwv_flow_api.id(7906544526388428923)
,p_display_seq=>3
,p_column_id=>wwv_flow_api.id(7906263372729027000)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906546444744428934)
,p_view_id=>wwv_flow_api.id(7906544526388428923)
,p_display_seq=>5
,p_column_id=>wwv_flow_api.id(7906263538149027001)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>99.929
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906546933535428936)
,p_view_id=>wwv_flow_api.id(7906544526388428923)
,p_display_seq=>5
,p_column_id=>wwv_flow_api.id(7906263620205027002)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906547421556428937)
,p_view_id=>wwv_flow_api.id(7906544526388428923)
,p_display_seq=>6
,p_column_id=>wwv_flow_api.id(7906263728187027003)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906547881685428939)
,p_view_id=>wwv_flow_api.id(7906544526388428923)
,p_display_seq=>7
,p_column_id=>wwv_flow_api.id(7906263804843027004)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906548372064428941)
,p_view_id=>wwv_flow_api.id(7906544526388428923)
,p_display_seq=>8
,p_column_id=>wwv_flow_api.id(7906263882770027005)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906548896410428943)
,p_view_id=>wwv_flow_api.id(7906544526388428923)
,p_display_seq=>9
,p_column_id=>wwv_flow_api.id(7906264040493027006)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906549429827428944)
,p_view_id=>wwv_flow_api.id(7906544526388428923)
,p_display_seq=>10
,p_column_id=>wwv_flow_api.id(7906264114390027007)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906549870031428946)
,p_view_id=>wwv_flow_api.id(7906544526388428923)
,p_display_seq=>11
,p_column_id=>wwv_flow_api.id(7906264211709027008)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(7906550375879428948)
,p_view_id=>wwv_flow_api.id(7906544526388428923)
,p_display_seq=>0
,p_column_id=>wwv_flow_api.id(7906264331980027009)
,p_is_visible=>false
,p_is_frozen=>true
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7844466962019176324)
,p_plug_name=>'Botones'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>80
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7879792388208359701)
,p_plug_name=>'PARAMETROS'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>90
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(7844467075168176325)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(7844466962019176324)
,p_button_name=>'Imprimir'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(7570351310891401263)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Imprimir'
,p_button_position=>'BELOW_BOX'
,p_button_alignment=>'LEFT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(7844467254109176326)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(7844466962019176324)
,p_button_name=>'Aceptar'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(7570351310891401263)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Aceptar'
,p_button_position=>'BELOW_BOX'
,p_warn_on_unsaved_changes=>null
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(7844467315220176327)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(7844466962019176324)
,p_button_name=>'Cancelar'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(7570351310891401263)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Cancelar'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(7844467450938176328)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(7844466962019176324)
,p_button_name=>'Borrar'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(7570351310891401263)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Borrar'
,p_button_position=>'BELOW_BOX'
,p_warn_on_unsaved_changes=>null
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(7844701314608669779)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_api.id(7844466962019176324)
,p_button_name=>'Salir'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(7570351310891401263)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Salir'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(7844463434758176288)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(7839572612435719902)
,p_button_name=>'Copiar'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(7570351310891401263)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Copiar datos'
,p_button_position=>'TOP'
,p_warn_on_unsaved_changes=>null
,p_button_condition_type=>'NEVER'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(7844811412703974994)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(7844811274870974993)
,p_button_name=>'Quitar'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(7570351310891401263)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Quitar Archivo'
,p_button_position=>'TOP'
,p_warn_on_unsaved_changes=>null
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7839573111333719907)
,p_name=>'P8007_S_INMUEBLES'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7839573013604719906)
,p_prompt=>'Inmuebles'
,p_display_as=>'NATIVE_CHECKBOX'
,p_lov=>'STATIC:Inmuebles;S'
,p_colspan=>2
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'1'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7839573243815719908)
,p_name=>'P8007_S_IMPLEMENTOS'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7839573013604719906)
,p_prompt=>'New'
,p_display_as=>'NATIVE_CHECKBOX'
,p_lov=>'STATIC:Implementos;S'
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'1'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7839573330765719909)
,p_name=>'P8007_S_OTROS_BIENES'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(7839573013604719906)
,p_display_as=>'NATIVE_CHECKBOX'
,p_lov=>'STATIC:Otros Bienes;S'
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'1'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7839573442466719910)
,p_name=>'P8007_S_MIEMBROS_UF'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7839574056315719916)
,p_prompt=>unistr('N\00BA de miembros:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>5
,p_grid_label_column_span=>3
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7839573510082719911)
,p_name=>'P8007_S_APORTANTES_UF'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7839574056315719916)
,p_prompt=>unistr('N\00BA de aportantes:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>5
,p_grid_label_column_span=>3
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7839573656857719913)
,p_name=>'P8007_S_OTROS_ING'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(7839574056315719916)
,p_prompt=>'Otros ingresos:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>7
,p_grid_label_column_span=>3
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7839573781289719914)
,p_name=>'P8007_S_MON_SIMBOLO'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(7839574056315719916)
,p_prompt=>'New'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7839573858403719915)
,p_name=>'P8007_LABEL1'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(7839574056315719916)
,p_prompt=>'Label1'
,p_source=>'(Incluye el solicitante)'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_colspan=>4
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7839575026780719926)
,p_name=>'P8007_S_DEPENDIENTES_UF'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7839574190407719918)
,p_prompt=>unistr('N\00BA de dependientes:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>5
,p_grid_column=>4
,p_grid_label_column_span=>3
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7839575093339719927)
,p_name=>'P8007_S_ASIST_ESC'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7839574190407719918)
,p_prompt=>unistr('\00BFCu\00E1ntos asisten a la escuela?')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>5
,p_grid_column=>4
,p_grid_label_column_span=>3
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7839575173855719928)
,p_name=>'P8007_FSOL_NRO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_prompt=>unistr('Solicitud N\00BA:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>3
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844462544019176279)
,p_name=>'P8007_S_FICHA_TEC'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_prompt=>unistr('Ficha N\00BA:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>3
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844462585052176280)
,p_name=>'P8007_FSOL_VENDEDOR'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_prompt=>'Asesor:'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_ASESOR'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT E.EMPL_LEGAJO||'' - ''||E.EMPL_NOMBRE||'' ''||E.EMPL_APE D,',
'       E.EMPL_LEGAJO R',
'FROM  PER_EMPLEADO E, ',
'      FAC_VENDEDOR V',
'WHERE EMPL_LEGAJO = VEND_LEGAJO',
'  AND EMPL_EMPRESA = :P_EMPRESA',
'  AND EMPL_EMPRESA = VEND_EMPR'))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_colspan=>2
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'POPUP'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
);
end;
/
begin
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844462693518176281)
,p_name=>'P8007_S_VEND_NOM'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>7
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844462776590176282)
,p_name=>'P8007_FSOL_OBS_SOL'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_prompt=>unistr('Observaci\00F3n:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>9
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844462911109176283)
,p_name=>'P8007_S_FSOL_FEC_SOL'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_item_default=>'select sysdate from dual;'
,p_item_default_type=>'SQL_QUERY'
,p_prompt=>'Fec. Solicitud:'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844462994071176284)
,p_name=>'P8007_S_NRO_FICHA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_prompt=>unistr('Solicitud N\00BA:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_column=>7
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844463089841176285)
,p_name=>'P8007_S_FSOL_FEC_ASIG'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_prompt=>unistr('Fec. Asignaci\00F3n')
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844463198871176286)
,p_name=>'P8007_S_ESTADO_SOL'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Estado:'
,p_source=>'PEND'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_grid_column=>11
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844463506160176289)
,p_name=>'P8007_S_FICHA_MONEDA'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_prompt=>'Tipo Moneda:'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>'STATIC:Guarani;1,Dolar;2'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844463565290176290)
,p_name=>'P8007_FSOL_IND_FEDERACION'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_item_default=>'N'
,p_prompt=>unistr('\00BFPertenece o ha pertenecido a alguna Federaci\00F3n, Asociaci\00F3n u Organizaci\00F3n de Agropecuarios?')
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC:No;N,Si;S'
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_grid_label_column_span=>4
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844463672467176291)
,p_name=>'P8007_FSOL_NOM_FEDERACION'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_prompt=>unistr('\00BFCu\00E1l?')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>5
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844463854741176292)
,p_name=>'P8007_S_NOMBRE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>'Nombres:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>8
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844463935427176293)
,p_name=>'P8007_S_DIRECCION'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>unistr('Direcci\00F3n')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>6
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844463989760176294)
,p_name=>'P8007_S_DOCUMENTO'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>'Documento;'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844464083157176295)
,p_name=>'P8007_S_TELEFONO'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>unistr('Tel\00E9fono:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844464183977176296)
,p_name=>'P8007_S_FEC_NAC'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>'Fecha Nac.:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>3
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844464337847176297)
,p_name=>'P8007_S_DEPARTAMENTO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>'Departamento'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844464383471176298)
,p_name=>'P8007_S_VIVIENDA'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>'Vivienda'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>3
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844464532640176299)
,p_name=>'P8007_S_RUC_DV'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>'Ruc:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>2
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844464567426176300)
,p_name=>'P8007_S_COD_CLI'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>'Cod. Cliente:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844464686375176301)
,p_name=>'P8007_S_SEXO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>'Sexo:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844464769259176302)
,p_name=>'P8007_S_ESTADO_CIVIL'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>'Estado Civil:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>3
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844464898655176303)
,p_name=>'P8007_S_CIUDAD'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>'Ciudad:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>6
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844464994341176304)
,p_name=>'P8007_S_REF_UBI'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>'Refer. Ubic.:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>6
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844465135149176305)
,p_name=>'P8007_S_RECIDE'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>'Reside hace:'
,p_post_element_text=>unistr('a\00F1o(s)')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844465188977176306)
,p_name=>'P8007_FSOL_GARANTE'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>'Garante:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>2
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844465316333176307)
,p_name=>'P8007_S_CATEG'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>unistr('Categor\00EDa:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_grid_column=>6
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844465451609176308)
,p_name=>'P8007_S_CONYUGE'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>unistr('C\00F3nyugue:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>6
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844465525457176309)
,p_name=>'P8007_S_NOM_GARANTE'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>4
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844465624112176310)
,p_name=>'P8007_S_DV'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844465745047176311)
,p_name=>'P8007_S_CATEG_DESC'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_api.id(7839572705547719903)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844465828732176312)
,p_name=>'P8007_S_ACT_DOMI'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7839572913038719905)
,p_display_as=>'NATIVE_CHECKBOX'
,p_lov=>unistr('STATIC:\00BFRealiza activ. en Domicilio?;S')
,p_colspan=>4
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'1'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844465946478176313)
,p_name=>'P8007_S_HAS_ALQ'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(7839572913038719905)
,p_item_default=>'0'
,p_prompt=>'Has. Alquiladas:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>4
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844466039497176314)
,p_name=>'P8007_S_HAS_PROPIAS'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(7839572913038719905)
,p_item_default=>'0'
,p_prompt=>'Has. Propias:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>4
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844466131328176315)
,p_name=>'P8007_S_TOT_UTIL'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(7839572913038719905)
,p_item_default=>'0'
,p_prompt=>'Has. Utiliz. Generalmente:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>4
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844466159639176316)
,p_name=>'P8007_S_PARCELA_DESC'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7839572913038719905)
,p_prompt=>'Parcela:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>4
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844466311418176317)
,p_name=>'P8007_FSOL_ANTIG_TERRENO'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(7839572913038719905)
,p_prompt=>'Posee Terreno desde:'
,p_post_element_text=>unistr('a\00F1o(s)')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>4
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844466369047176318)
,p_name=>'P8007_FSOL_ANTIG_TRAB'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(7839572913038719905)
,p_prompt=>'Tiempo trabajo en lugar:'
,p_post_element_text=>unistr('a\00F1o(s)')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>4
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844466485217176319)
,p_name=>'P8007_S_ACTIV_EDAD_INI'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(7839572913038719905)
,p_prompt=>'Experiencia:'
,p_post_element_text=>unistr('a\00F1o(s)')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>4
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844466576749176320)
,p_name=>'P8007_S_PARCELA_UBI'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(7839572913038719905)
,p_prompt=>'Ref.Ubic.'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>4
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844466702906176321)
,p_name=>'P8007_S_UTIL_ALQ'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(7839572913038719905)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_column=>10
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844466808222176322)
,p_name=>'P8007_S_UTIL_PROPIAS'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(7839572913038719905)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_column=>10
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844466871675176323)
,p_name=>'P8007_FSOL_OBS_DIRE_PARC'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(7839572913038719905)
,p_prompt=>'Observaciones:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>7
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844701983859669786)
,p_name=>'P8007_FSOL_OBS_FPAG_FALLO_PROD'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(7844701501814669781)
,p_prompt=>unistr('\00BFEn caso de que fallara la producci\00F3n, c\00F3mo pagar\00EDa el cr\00E9dito?')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>8
,p_grid_label_column_span=>3
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844702156393669787)
,p_name=>'P8007_FSOL_CANT_HAS_PROD'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7844701412444669780)
,p_prompt=>unistr('\00BFCu\00E1ntas Has. va a Plantar?:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844702175887669788)
,p_name=>'P8007_FSOL_NRO_PRODUCTOR'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7844701412444669780)
,p_prompt=>unistr('Productor N\00BA:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844702325535669789)
,p_name=>'P8007_FSOL_NRO_COMITE'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(7844701412444669780)
,p_prompt=>unistr('Comit\00E9 N\00BA:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844702433395669790)
,p_name=>'P8007_S_FSOL_FEC_PLAZO_OTORG'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(7844701412444669780)
,p_item_default=>'select sysdate from dual'
,p_item_default_type=>'SQL_QUERY'
,p_prompt=>'Plazo de Otorgamiento:'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_colspan=>4
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844702497580669791)
,p_name=>'P8007_FSOL_OBS_DEST_FINAN'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(7844701412444669780)
,p_prompt=>'Destino del financiamiento'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>30
,p_cHeight=>2
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844703906603669805)
,p_name=>'P8007_S_TOT_IMP_MENSUAL'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(7844703824843669804)
,p_prompt=>'Total:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>3
,p_grid_column=>10
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--large'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844704008512669806)
,p_name=>'P8007_S_EXP_CRED'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7844701501814669781)
,p_prompt=>unistr('\00BFExperiencia crediticia?')
,p_display_as=>'NATIVE_CHECKBOX'
,p_lov=>unistr('STATIC:\00BFExperiencia crediticia?;S')
,p_colspan=>4
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'1'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844704083186669807)
,p_name=>'P8007_FSOL_CRED_NOM_EMPR'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(7844701501814669781)
,p_prompt=>unistr('\00BFCon qu\00E9 entidad?')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>4
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844704217076669808)
,p_name=>'P8007_S_EXP_CRED_DESC'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7844701501814669781)
,p_prompt=>unistr('\00BFD\00F3nde?')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>4
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844704287121669809)
,p_name=>'P8007_FSOL_DATO_ULT_CRED'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(7844701501814669781)
,p_prompt=>unistr('Datos del \00FAltimo cr\00E9dito:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>8
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844704386356669810)
,p_name=>'P8007_FSOL_IND_CRED_VGTE'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(7844701501814669781)
,p_prompt=>unistr('\00BFTiene credito vigente?')
,p_display_as=>'NATIVE_CHECKBOX'
,p_lov=>unistr('STATIC:\00BFTiene cr\00E9dito vigente?;S')
,p_colspan=>4
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'1'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844704510599669811)
,p_name=>'P8007_FSOL_DATO_INVER_CRED'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(7844701501814669781)
,p_prompt=>unistr('En qu\00E9 se invirti\00F3:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>8
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844705568147669822)
,p_name=>'P8007_S_PLAZO_OTORG'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7844701840091669784)
,p_item_default=>'select sysdate from dual'
,p_item_default_type=>'SQL_QUERY'
,p_prompt=>'Plazo de Otorgamiento:'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_colspan=>3
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844705693505669823)
,p_name=>'P8007_MON_SIMBOLO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(7844701840091669784)
,p_prompt=>'MON_SIMBOLO'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844705838381669824)
,p_name=>'P8007_FSOL_OBS_PROPUESTA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7844701840091669784)
,p_prompt=>unistr('Observaci\00F3n:')
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>30
,p_cHeight=>2
,p_begin_on_new_line=>'N'
,p_colspan=>7
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844705868599669825)
,p_name=>'P8007_S_GARANTE'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(7844701840091669784)
,p_prompt=>'Garante:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>5
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844706007696669826)
,p_name=>'P8007_S_MONTO_CRED'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(7844701840091669784)
,p_prompt=>unistr('Monto del Cr\00E9dito:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>3
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
end;
/
begin
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844706124713669827)
,p_name=>'P8007_S_DOCUMENTO_II'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(7844701840091669784)
,p_prompt=>'Documento:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>4
,p_grid_column=>7
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844706178055669828)
,p_name=>'P8007_FSOL_ESTADO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7844701929666669785)
,p_prompt=>'Estado:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC:Aprobado;A,Rechazado;R'
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_colspan=>4
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844809909646974979)
,p_name=>'P8007_MON_SIMBOLO_II'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(7844701929666669785)
,p_prompt=>'MON_SIMBOLO_II'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844810032597974980)
,p_name=>'P8007_S_FSOL_FEC_ESTADO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7844701929666669785)
,p_item_default=>'select sysdate from dual'
,p_item_default_type=>'SQL_QUERY'
,p_prompt=>'Fecha res.:'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>4
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844810108717974981)
,p_name=>'P8007_FSOL_MONTO_ESTADO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(7844701929666669785)
,p_prompt=>'Monto:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>4
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844810539178974985)
,p_name=>'P8007_FSOL_OBS_RES_COM'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7844810368447974984)
,p_prompt=>unistr('Observaci\00F3n:')
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>50
,p_cHeight=>10
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844810785732974988)
,p_name=>'P8007_FSOL_INTERES'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7844810740464974987)
,p_item_default=>'0'
,p_prompt=>unistr('% Inter\00E8s')
,p_post_element_text=>'Anual'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>5
,p_grid_column=>3
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844810945266974989)
,p_name=>'P8007_S_FSOL_FEC_PRIM_VTO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7844810740464974987)
,p_item_default=>'select sysdate from dual'
,p_item_default_type=>'SQL_QUERY'
,p_prompt=>'Fec.Prim.Vto.:'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_colspan=>6
,p_grid_column=>3
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844811089659974991)
,p_name=>'P8007_S_FSOL_FEC_CADUCA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7844811040435974990)
,p_item_default=>'select sysdate from dual'
,p_item_default_type=>'SQL_QUERY'
,p_prompt=>'Fecha caducidad solicitud:'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_colspan=>8
,p_grid_label_column_span=>4
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844811469953974995)
,p_name=>'P8007_ARCHIVO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7844811216140974992)
,p_prompt=>'Explorar:'
,p_display_as=>'NATIVE_FILE'
,p_cSize=>30
,p_colspan=>5
,p_grid_label_column_span=>1
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'APEX_APPLICATION_TEMP_FILES'
,p_attribute_09=>'SESSION'
,p_attribute_10=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844812467273975005)
,p_name=>'P8007_FARC_IMG'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7844811274870974993)
,p_prompt=>'Farc Img'
,p_display_as=>'NATIVE_DISPLAY_IMAGE'
,p_colspan=>12
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'SQL'
,p_attribute_06=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select BLOB001',
'from apex_collections',
'where collection_name = ''FINI040_ARCHIVOS''',
'and seq_id = :P8007_SEQID_IMAGEN;'))
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844846264110018357)
,p_name=>'P8007_FSOL_TIP_VENC'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(8191053717011139456)
,p_item_default=>'M'
,p_prompt=>'Tipo Vencimiento'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>'STATIC:Mensual;M,Otros;D'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844846713039018358)
,p_name=>'P8007_FSOL_S_DIAS_ENTRE_CUOTAS'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(8191053717011139456)
,p_prompt=>'Fsol S Dias Entre Cuotas'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>6
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844849276164041572)
,p_name=>'P8007_FSOL_TIP_GEN_CUO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(8191056623256162676)
,p_item_default=>'F'
,p_prompt=>'S Dias Entre Cuotas'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>unistr('STATIC:Sistema Alem\00E1n;A,Sistema Carga Manual;MA,Sistema Franc\00E9s;F,Sistema Franc\00E9s Especial;FE')
,p_colspan=>6
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844849694116041574)
,p_name=>'P8007_FSOL_S_PLAZO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(8191056623256162676)
,p_prompt=>'Plazo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844850106099041574)
,p_name=>'P8007_FSOL_S_MONTO_CUO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(8191056623256162676)
,p_prompt=>'Cuotas (de)'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>5
,p_grid_label_column_span=>3
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844850511272041574)
,p_name=>'P8007_FSOL_S_CANT_CUO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(8191056623256162676)
,p_prompt=>',en'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844850893278041575)
,p_name=>'P8007_LABEL'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(8191056623256162676)
,p_prompt=>'Cuotas(s)'
,p_source=>'Cuotas(s)'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7879791162263359689)
,p_name=>'P8007_PP_HABILITAR'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7879791351525359690)
,p_name=>'P8007_G_ORIGEN'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7879791393961359691)
,p_name=>'P8007_P_CLAVE_RECEPCION'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7879791503830359692)
,p_name=>'P8007_P_CLAVE_PRESUPUESTO'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7879791643025359693)
,p_name=>'P8007_P_GENERA_OR'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7879791666566359694)
,p_name=>'P8007_CONF_FEC_LIM_MOD'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7879791758227359695)
,p_name=>'P8007_CONF_PER_ACT_FIN'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7879791881341359696)
,p_name=>'P8007_CONF_PER_ACT_INI'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7879792039627359697)
,p_name=>'P8007_CONF_PER_SGTE_INI'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7879792088387359698)
,p_name=>'P8007_CONF_PER_SGTE_FIN'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7879792261111359700)
,p_name=>'P8007_IND_OPER_APRUEB'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7879792985011359707)
,p_name=>'P8007_P_NRO_SOL'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7879792388208359701)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7879793737824359714)
,p_name=>'P8007_FSOL_FORMA_CONOC_EMP'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7879794383157359721)
,p_name=>'P8007_LUGAR_ORIGEN_REPLICA'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7881121380799744480)
,p_name=>'P8007_FSOL_CLAVE'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7881121552817744481)
,p_name=>'P8007_FSOL_EMPR'
,p_item_sequence=>270
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7881121874995744485)
,p_name=>'P8007_S_CLAVE_FICHA_TEC'
,p_item_sequence=>280
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7881122029989744486)
,p_name=>'P8007_S_FICHA_PERSONA'
,p_item_sequence=>290
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7881122155069744487)
,p_name=>'P8007_W_MON_DEC_IMP'
,p_item_sequence=>300
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7881122666755744493)
,p_name=>'P8007_P_COPIAR'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7879792388208359701)
,p_item_default=>'N'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7881123054648744496)
,p_name=>'P8007_S_MON_SIMB4'
,p_item_sequence=>310
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7881123119999744497)
,p_name=>'P8007_S_IND_EXIGIR_HAS'
,p_item_sequence=>320
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7881124722045744513)
,p_name=>'P8007_S_CANT_HIJOS'
,p_item_sequence=>330
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7881866408469129401)
,p_name=>'P8007_FSOL_S_DIAS_GRACIA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(8191053717011139456)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7881867162799129409)
,p_name=>'P8007_BANDERA_CUO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(7879792388208359701)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7881867300990129410)
,p_name=>'P8007_P_CANT_CUO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(7879792388208359701)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7881867605461129413)
,p_name=>'P8007_TITULO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7839572516674719901)
,p_item_default=>'CUOTAS MANUALES'
,p_prompt=>'Titulo'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7884484687542251889)
,p_name=>'P8007_FILE_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(7844811274870974993)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7884484855921251890)
,p_name=>'P8007_ITEM'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(7844811274870974993)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7884487367443251916)
,p_name=>'P8007_FSOL_FEC_GRAB'
,p_item_sequence=>340
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7884487531489251917)
,p_name=>'P8007_FSOL_CLAVE_FICH_TEC'
,p_item_sequence=>350
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7884487585651251918)
,p_name=>'P8007_FSOL_LOGIN'
,p_item_sequence=>360
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7884487681627251919)
,p_name=>'P8007_FSOL_SUC'
,p_item_sequence=>370
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7884487830609251920)
,p_name=>'P8007_S_CLAVE_COPIAR'
,p_item_sequence=>380
,p_item_plug_id=>wwv_flow_api.id(7844463297859176287)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7885759611337457292)
,p_name=>'P8007_SEQID_IMAGEN'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7844811274870974993)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7879793159582359709)
,p_name=>'asignar_atributos'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_IND_OPER_APRUEB'
,p_condition_element=>'P8007_IND_OPER_APRUEB'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'S'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7879793339679359710)
,p_event_id=>wwv_flow_api.id(7879793159582359709)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8007_FSOL_ESTADO,P8007_S_FSOL_FEC_ESTADO,P8007_FSOL_MONTO_ESTADO'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7879793416434359711)
,p_event_id=>wwv_flow_api.id(7879793159582359709)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8007_FSOL_ESTADO,P8007_S_FSOL_FEC_ESTADO,P8007_FSOL_MONTO_ESTADO'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7879793459398359712)
,p_name=>'cambio_valor_habilitar'
,p_event_sequence=>20
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_PP_HABILITAR'
,p_condition_element=>'P8007_PP_HABILITAR'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'N'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7879793592738359713)
,p_event_id=>wwv_flow_api.id(7879793459398359712)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8007_S_FICHA_TEC,P8007_S_FSOL_FEC_SOL,P8007_S_FSOL_FEC_ASIG,P8007_FSOL_VENDEDOR,P8007_FSOL_OBS_SOL,P8007_FSOL_FORMA_CONOC_EMP,P8007_FSOL_IND_FEDERACION,P8007_FSOL_NOM_FEDERACION,P8007_FSOL_GARANTE,P8007_FSOL_ANTIG_TERRENO,P8007_FSOL_ANTIG_TRAB,P8007'
||'_FSOL_OBS_DIRE_PARC,P8007_FSOL_CANT_HAS_PROD,P8007_FSOL_NRO_PRODUCTOR,P8007_FSOL_NRO_COMITE,P8007_S_FSOL_FEC_PLAZO_OTORG,P8007_FSOL_OBS_DEST_FINAN,P8007_FSOL_IND_CRED_VGTE,P8007_FSOL_CRED_NOM_EMPR,P8007_FSOL_DATO_ULT_CRED,P8007_FSOL_DATO_INVER_CRED,P'
||'8007_FSOL_OBS_FPAG_FALLO_PROD,P8007_FSOL_OBS_PROPUESTA,P8007_FSOL_MONTO_ESTADO'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7879793978245359717)
,p_event_id=>wwv_flow_api.id(7879793459398359712)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8007_S_FICHA_TEC,P8007_S_FSOL_FEC_SOL,P8007_S_FSOL_FEC_ASIG,P8007_FSOL_VENDEDOR,P8007_FSOL_OBS_SOL,P8007_FSOL_FORMA_CONOC_EMP,P8007_FSOL_IND_FEDERACION,P8007_FSOL_NOM_FEDERACION,P8007_FSOL_GARANTE,P8007_FSOL_ANTIG_TERRENO,P8007_FSOL_ANTIG_TRAB,P8007'
||'_FSOL_OBS_DIRE_PARC,P8007_FSOL_CANT_HAS_PROD,P8007_FSOL_NRO_PRODUCTOR,P8007_FSOL_NRO_COMITE,P8007_S_FSOL_FEC_PLAZO_OTORG,P8007_FSOL_OBS_DEST_FINAN,P8007_FSOL_IND_CRED_VGTE,P8007_FSOL_CRED_NOM_EMPR,P8007_FSOL_DATO_ULT_CRED,P8007_FSOL_DATO_INVER_CRED,P'
||'8007_FSOL_OBS_FPAG_FALLO_PROD,P8007_FSOL_OBS_PROPUESTA,P8007_FSOL_MONTO_ESTADO'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7879793757073359715)
,p_event_id=>wwv_flow_api.id(7879793459398359712)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(7844467450938176328)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7879794155857359718)
,p_event_id=>wwv_flow_api.id(7879793459398359712)
,p_event_result=>'FALSE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(7844467450938176328)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7879793930676359716)
,p_event_id=>wwv_flow_api.id(7879793459398359712)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(7844467254109176326)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7879794244641359719)
,p_event_id=>wwv_flow_api.id(7879793459398359712)
,p_event_result=>'FALSE'
,p_action_sequence=>60
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(7844467254109176326)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7906264632470027012)
,p_event_id=>wwv_flow_api.id(7879793459398359712)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'disable_cuotas();',
'disable_referencias();',
'disable_financiamiento();'))
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7906264717643027013)
,p_event_id=>wwv_flow_api.id(7879793459398359712)
,p_event_result=>'FALSE'
,p_action_sequence=>70
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'enable_cuotas();',
'enable_referencias();',
'enable_financiamiento();'))
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7879794512809359722)
,p_name=>'nro_solicitud_nonula'
,p_event_sequence=>30
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_FSOL_NRO'
,p_condition_element=>'P8007_FSOL_NRO'
,p_triggering_condition_type=>'NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7879794639701359723)
,p_event_id=>wwv_flow_api.id(7879794512809359722)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'RAISE_APPLICATION_ERROR(-20010,''Nro de Solicitud no puede ser nulo'');'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7879794668596359724)
,p_name=>'nro_solicitud_nocero'
,p_event_sequence=>40
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_FSOL_NRO'
,p_condition_element=>'P8007_FSOL_NRO'
,p_triggering_condition_type=>'LESS_THAN_OR_EQUAL'
,p_triggering_expression=>'0'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7879794807449359725)
,p_event_id=>wwv_flow_api.id(7879794668596359724)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'RAISE_APPLICATION_ERROR(-20010,''Nro. de Solicitud no puede ser menor o igual a 0..!'');'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7879794917345359726)
,p_name=>'validar_nro_solicitud'
,p_event_sequence=>50
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_FSOL_NRO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7879795000198359727)
,p_event_id=>wwv_flow_api.id(7879794917345359726)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'	V_CLAVE NUMERIC;	',
'BEGIN  ',
'	',
'		SELECT FSOL_CLAVE',
'		INTO V_CLAVE',
'		FROM FIN_FICHA_SOL',
'		WHERE FSOL_NRO = :P8007_FSOL_NRO',
'		  AND FSOL_EMPR = :P_EMPRESA;',
'		',
'	  :P8007_FSOL_CLAVE := V_CLAVE;',
'      :P8007_FSOL_EMPR := :P_EMPRESA;',
'	',
'EXCEPTION',
'WHEN NO_DATA_FOUND THEN',
unistr('		RAISE_APPLICATION_ERROR(-20010,''Est\00E1 registrando una solicitud nueva'');'),
'when others then ',
'			RAISE_APPLICATION_ERROR(-20010,''Error en key_next_item''||SQLCODE||''-''||SQLERRM);',
'END;'))
,p_attribute_02=>'P8007_FSOL_NRO'
,p_attribute_03=>'P8007_FSOL_CLAVE,P8007_FSOL_EMPR'
,p_attribute_04=>'N'
,p_stop_execution_on_error=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7879795110098359728)
,p_event_id=>wwv_flow_api.id(7879794917345359726)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8007_S_NRO_FICHA'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881121305698744479)
,p_event_id=>wwv_flow_api.id(7879794917345359726)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(7844463434758176288)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881122275326744489)
,p_name=>'cambio_clave'
,p_event_sequence=>60
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_FSOL_CLAVE'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881122421244744490)
,p_event_id=>wwv_flow_api.id(7881122275326744489)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'	SELECT  FSOL_CLAVE_FICH_TEC,',
'			FSOL_MON,',
'			FSOL_FEC_SOL,',
'			FSOL_FEC_ASIG,',
'			FSOL_FEC_PLAZO_OTORG,',
'			FSOL_FEC_ESTADO,',
'			FSOL_ESTADO,',
'			FSOL_VENDEDOR,',
'			FSOL_GARANTE,',
'			FSOL_FEC_PRIM_VTO,',
'			FSOL_FEC_CADUCA,',
'            -------AGREGADOS--------',
'            FSOL_OBS_DEST_FINAN,',
'            FSOL_MONTO_ESTADO,',
'            FSOL_TIP_VENC,',
'            FSOL_S_DIAS_ENTRE_CUOTAS,',
'            FSOL_S_DIAS_GRACIA,',
'            FSOL_INTERES,',
'            FSOL_OBS_RES_COM,',
'            FSOL_TIP_GEN_CUO,',
'            FSOL_S_PLAZO,',
'            FSOL_S_MONTO_CUO,',
'            FSOL_S_CANT_CUO,',
'            FSOL_CANT_HAS_PROD,',
'            FSOL_NRO_PRODUCTOR,',
'            FSOL_NRO_COMITE,',
'            FSOL_ANTIG_TERRENO,',
'            FSOL_ANTIG_TRAB',
'	INTO    :P8007_S_CLAVE_FICHA_TEC,',
'			:P8007_S_FICHA_MONEDA,',
'			:P8007_S_FSOL_FEC_SOL,',
'			:P8007_S_FSOL_FEC_ASIG,',
'			:P8007_S_FSOL_FEC_PLAZO_OTORG,',
'			:P8007_S_FSOL_FEC_ESTADO,',
'			:P8007_FSOL_ESTADO,',
'			:P8007_FSOL_VENDEDOR,',
'			:P8007_FSOL_GARANTE,',
'			:P8007_S_FSOL_FEC_PRIM_VTO,',
'			:P8007_S_FSOL_FEC_CADUCA,',
'            ---------AGREGADOS------------',
'            :P8007_FSOL_OBS_DEST_FINAN,',
'            :P8007_FSOL_MONTO_ESTADO,',
'            :P8007_FSOL_TIP_VENC,',
'            :P8007_FSOL_S_DIAS_ENTRE_CUOTAS,',
'            :P8007_FSOL_S_DIAS_GRACIA,',
'            :P8007_FSOL_INTERES,',
'            :P8007_FSOL_OBS_RES_COM,',
'            :P8007_FSOL_TIP_GEN_CUO,',
'            :P8007_FSOL_S_PLAZO,',
'            :P8007_FSOL_S_MONTO_CUO,',
'            :P8007_FSOL_S_CANT_CUO,',
'            :P8007_FSOL_CANT_HAS_PROD,',
'            :P8007_FSOL_NRO_PRODUCTOR,',
'            :P8007_FSOL_NRO_COMITE,',
'            :P8007_FSOL_ANTIG_TERRENO,',
'            :P8007_FSOL_ANTIG_TRAB',
'	FROM    FIN_FICHA_SOL ',
'	WHERE   FSOL_CLAVE = :P8007_FSOL_CLAVE',
'	AND     FSOL_EMPR = :P_EMPRESA;',
'',
'	FINI040.PP_TRAER_FICHA_TEC(I_S_CLAVE_FICHA_TEC => :P8007_S_CLAVE_FICHA_TEC,',
'                               I_EMPRESA           => :P_EMPRESA,',
'							   O_S_FICHA_TEC       => :P8007_S_FICHA_TEC,',
'							   O_S_FICHA_PERSONA   => :P8007_S_FICHA_PERSONA);',
'',
'	IF :P8007_FSOL_ESTADO = ''A'' THEN',
'		:P8007_S_ESTADO_SOL:=''APRO'';',
'	ELSIF :P8007_FSOL_ESTADO = ''R'' THEN',
'		:P8007_S_ESTADO_SOL:=''RECH'';',
'	ELSE',
'		:P8007_S_ESTADO_SOL:=''PEND'';',
'	END IF;',
'	--*',
'	IF :P8007_FSOL_VENDEDOR IS NOT NULL THEN',
'		FINI040.PP_TRAER_DESC_VENDEDOR(I_FSOL_VENDEDOR => :P8007_FSOL_VENDEDOR,',
'                                       I_EMPRESA       => :P_EMPRESA,',
'									   O_S_VEND_NOMB   => :P8007_S_VEND_NOM);',
'	END IF;',
'',
'	IF :P8007_FSOL_GARANTE IS NOT NULL THEN',
'		FINI040.PP_TRAER_DESC_GARANTE(I_FSOL_GARANTE  => :P8007_FSOL_GARANTE,',
'                                      I_EMPRESA       => :P_EMPRESA,',
'									  O_S_NOM_GARANTE => :P8007_S_NOM_GARANTE,',
'									  O_S_GARANTE	  => :P8007_S_GARANTE);',
'	END IF;	',
'',
'	:P8007_S_MON_SIMBOLO:=''GS.'';',
'	:P8007_W_MON_DEC_IMP:=0;',
'',
'	IF :P8007_FSOL_ESTADO IN (''A'',''R'') THEN',
'		:P8007_PP_HABILITAR := ''N'';',
'	END IF;	',
'END;'))
,p_attribute_02=>'P8007_FSOL_CLAVE,P8007_FSOL_EMPR'
,p_attribute_03=>'P8007_S_CLAVE_FICHA_TEC,P8007_S_FICHA_MONEDA,P8007_S_FSOL_FEC_SOL,P8007_S_FSOL_FEC_ASIG,P8007_S_FSOL_FEC_PLAZO_OTORG,P8007_S_FSOL_FEC_ESTADO,P8007_FSOL_ESTADO,P8007_FSOL_VENDEDOR,P8007_FSOL_GARANTE,P8007_S_FSOL_FEC_PRIM_VTO,P8007_S_FSOL_FEC_CADUCA,P8'
||'007_S_FICHA_TEC,P8007_S_FICHA_PERSONA,P8007_S_VEND_NOM,P8007_S_NOM_GARANTE,P8007_S_GARANTE,P8007_PP_HABILITAR,P8007_W_MON_DEC_IMP,P8007_S_MON_SIMBOLO,P8007_S_ESTADO_SOL,P8007_FSOL_OBS_DEST_FINAN,P8007_FSOL_MONTO_ESTADO,P8007_FSOL_TIP_VENC,P8007_FSOL_'
||'S_DIAS_ENTRE_CUOTAS,P8007_FSOL_S_DIAS_GRACIA,P8007_FSOL_INTERES,P8007_FSOL_OBS_RES_COM,P8007_FSOL_TIP_GEN_CUO,P8007_FSOL_S_PLAZO,P8007_FSOL_S_MONTO_CUO,P8007_FSOL_S_CANT_CUO,P8007_FSOL_CANT_HAS_PROD,P8007_FSOL_NRO_PRODUCTOR,P8007_FSOL_NRO_COMITE,P800'
||'7_FSOL_ANTIG_TERRENO,P8007_FSOL_ANTIG_TRAB'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881867738814129414)
,p_event_id=>wwv_flow_api.id(7881122275326744489)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'l_count number;',
'BEGIN',
'FINI040.PP_INICIAR_COL_DESGLOCE;',
'FINI040.PP_INICIAR_COL_REFERENCIAS;',
'FINI040.PP_INICIAR_COL_CUOTAS;',
'FINI040.PP_INICIAR_COL_ARCHIVOS;',
'',
'FINI040.PP_OBTENER_DESGLOCE (I_FSOL_MON     => :P8007_S_FICHA_MONEDA,',
'                             I_FSOL_CLAVE   => :P8007_FSOL_CLAVE,',
'                             I_EMPRESA      => :P_EMPRESA);',
'',
'l_count := APEX_COLLECTION.COLLECTION_MEMBER_COUNT(p_collection_name => ''FINI040_DESGLOCE''); ',
'IF l_count = 0 THEN',
'    FINI040.PP_DESGLOCE_VACIO;',
'END IF; ',
'',
'SELECT sum(C006)',
'INTO   :P8007_S_TOT_IMP_MENSUAL',
'FROM   APEX_COLLECTIONS',
'WHERE  COLLECTION_NAME = ''FINI040_DESGLOCE'';',
'',
'FINI040.PP_OBTENER_REFERENCIAS (I_FSOL_CLAVE   => :P8007_FSOL_CLAVE,',
'                                I_EMPRESA      => :P_EMPRESA);',
'',
'FINI040.PP_OBTENER_ARCHIVOS(I_FSOL_CLAVE   => :P8007_FSOL_CLAVE,',
'                            I_EMPRESA      => :P_EMPRESA);',
'                            ',
'FINI040.PP_OBTENER_CUOTAS (I_FSOL_CLAVE           => :P8007_FSOL_CLAVE,',
'                           I_EMPRESA              => :P_EMPRESA,',
'                           I_S_FSOL_FEC_PRIM_VTO  => :P8007_S_FSOL_FEC_PRIM_VTO);                            ',
'',
'END;                             '))
,p_attribute_02=>'P8007_FSOL_CLAVE,P8007_S_FICHA_MONEDA,P8007_S_FSOL_FEC_PRIM_VTO'
,p_attribute_03=>'P8007_S_TOT_IMP_MENSUAL'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
end;
/
begin
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7906805755007296416)
,p_event_id=>wwv_flow_api.id(7881122275326744489)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(7906803444366296393)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881868367672129421)
,p_event_id=>wwv_flow_api.id(7881122275326744489)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(7906261851701026984)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7884484595422251888)
,p_event_id=>wwv_flow_api.id(7881122275326744489)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(7885754100544413798)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881122495003744491)
,p_name=>'validar_s_nro_ficha'
,p_event_sequence=>70
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_S_NRO_FICHA'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881122568751744492)
,p_event_id=>wwv_flow_api.id(7881122495003744491)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P8007_S_NRO_FICHA IS NOT NULL THEN',
'	:P8007_P_COPIAR:=''S'';',
'END IF;	',
'IF to_number(:P8007_S_NRO_FICHA) <= 0 THEN ',
'	RAISE_APPLICATION_ERROR(-20010,''Nro. de Solic. a copiar no puede ser menor o igual a 0..!'');',
'END IF;'))
,p_attribute_02=>'P8007_S_NRO_FICHA'
,p_attribute_03=>'P8007_P_COPIAR'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881122853061744494)
,p_name=>'validacion_s_ficha_Tev'
,p_event_sequence=>80
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_S_FICHA_TEC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881122871888744495)
,p_event_id=>wwv_flow_api.id(7881122853061744494)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P8007_P_COPIAR=''N'' THEN',
'	IF :P8007_S_FICHA_TEC IS NOT NULL THEN',
'		FINI040.PP_BUSCAR_FICHA_TEC(I_S_FICHA_TEC         => :P8007_S_FICHA_TEC,',
'                                    I_S_FICHA_MONEDA      => :P8007_S_FICHA_MONEDA,  ',
'                                    O_S_CLAVE_FICHA_TEC   => :P8007_S_CLAVE_FICHA_TEC,',
'                                    O_S_FICHA_PERSONA     => :P8007_S_FICHA_PERSONA,',
'                                    O_MON_SIMBOLO         => :P8007_MON_SIMBOLO,',
'                                    O_MON_SIMBOLO_II      => :P8007_MON_SIMBOLO_II,',
'                                    O_S_MON_SIMB4         => :P8007_S_MON_SIMB4);',
'                                    ',
'		FINI040.PP_CARGAR_DATOS_PERS(I_S_CLAVE_FICHA_TEC  => :P8007_S_CLAVE_FICHA_TEC,',
'                                     I_S_FICHA_PERSONA    => :P8007_S_FICHA_PERSONA,',
'                                     I_O_FSOL_VENDEDOR    => :P8007_FSOL_VENDEDOR,',
'                                     O_S_COD_CLI          => :P8007_S_COD_CLI,',
'                                     O_S_FEC_NAC          => :P8007_S_FEC_NAC,',
'                                     O_S_DOCUMENTO        => :P8007_S_DOCUMENTO,',
'                                     O_S_DEPARTAMENTO     => :P8007_S_DEPARTAMENTO,',
'                                     O_S_CIUDAD           => :P8007_S_CIUDAD,',
'                                     O_S_DIRECCION        => :P8007_S_DIRECCION,',
'                                     O_S_REF_UBI          => :P8007_S_REF_UBI,',
'                                     O_S_VIVIENDA         => :P8007_S_VIVIENDA,',
'                                     O_S_RECIDE           => :P8007_S_RECIDE,',
'                                     O_S_TELEFONO         => :P8007_S_TELEFONO,',
'                                     O_S_MIEMBROS_UF      => :P8007_S_MIEMBROS_UF,',
'                                     O_S_APORTANTES_UF    => :P8007_S_APORTANTES_UF,',
'                                     O_S_DEPENDIENTES_UF  => :P8007_S_DEPENDIENTES_UF,',
'                                     O_S_OTROS_ING        => :P8007_S_OTROS_ING,',
'                                     O_S_ASIST_ESC        => :P8007_S_ASIST_ESC,',
'                                     O_S_ACTIV_EDAD_INI   => :P8007_S_ACTIV_EDAD_INI,',
'                                     O_S_IND_EXIGIR_HAS   => :P8007_S_IND_EXIGIR_HAS,',
'                                     O_S_EXP_CRED         => :P8007_S_EXP_CRED,',
'                                     O_S_EXP_CRED_DESC    => :P8007_S_EXP_CRED_DESC,',
'                                     O_S_RUC_DV           => :P8007_S_RUC_DV,',
'                                     O_S_DV               => :P8007_S_DV,',
'                                     O_S_CATEG            => :P8007_S_CATEG,',
'                                     O_S_CATEG_DESC       => :P8007_S_CATEG_DESC,',
'                                     O_S_NOMBRE           => :P8007_S_NOMBRE,',
'                                     O_S_SEXO             => :P8007_S_SEXO,',
'                                     O_S_ESTADO_CIVIL     => :P8007_S_ESTADO_CIVIL,',
'                                     O_S_CONYUGE          => :P8007_S_CONYUGE,',
'                                     O_S_VEND_NOMB        => :P8007_S_VEND_NOM);',
'    END IF;',
'END IF;	',
'	'))
,p_attribute_02=>'P8007_P_COPIAR,P8007_S_FICHA_TEC,P8007_S_FICHA_MONEDA,P8007_S_CLAVE_FICHA_TEC,P8007_S_FICHA_PERSONA,P8007_FSOL_VENDEDOR'
,p_attribute_03=>'P8007_S_CLAVE_FICHA_TEC,P8007_S_FICHA_PERSONA,P8007_MON_SIMBOLO,P8007_MON_SIMBOLO_II,P8007_S_MON_SIMB4,P8007_FSOL_VENDEDOR,P8007_S_COD_CLI,P8007_S_FEC_NAC,P8007_S_DOCUMENTO,P8007_S_DEPARTAMENTO,P8007_S_CIUDAD,P8007_S_DIRECCION,P8007_S_REF_UBI,P8007_S'
||'_VIVIENDA,P8007_S_RECIDE,P8007_S_TELEFONO,P8007_S_MIEMBROS_UF,P8007_S_APORTANTES_UF,P8007_S_DEPENDIENTES_UF,P8007_S_OTROS_ING,P8007_S_ASIST_ESC,P8007_S_ACTIV_EDAD_INI,P8007_S_IND_EXIGIR_HAS,P8007_S_EXP_CRED,P8007_S_EXP_CRED_DESC,P8007_S_RUC_DV,P8007_'
||'S_DV,P8007_S_CATEG,P8007_S_CATEG_DESC,P8007_S_NOMBRE,P8007_S_SEXO,P8007_S_ESTADO_CIVIL,P8007_S_CONYUGE,P8007_S_VEND_NOM'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881123238587744498)
,p_event_id=>wwv_flow_api.id(7881122853061744494)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--TRAER DIFERENTES INFORMACIONES',
'FINI040.PP_TRAER_ACT_DOMIC (I_S_FICHA_PERSONA => :P8007_S_FICHA_PERSONA,',
'                            O_S_ACT_DOMI      => :P8007_S_ACT_DOMI);',
'                            ',
'FINI040.PP_TRAER_PARCELA (I_S_CLAVE_FICHA_TEC => :P8007_S_CLAVE_FICHA_TEC,',
'                          O_S_PARCELA_DESC    => :P8007_S_PARCELA_DESC,',
'                          O_S_PARCELA_UBI     => :P8007_S_PARCELA_UBI);',
'                          ',
'FINI040.PP_TRAER_IMPLEMENTOS (I_S_CLAVE_FICHA_TEC  => :P8007_S_CLAVE_FICHA_TEC,',
'                              O_S_IMPLEMENTOS      => :P8007_S_IMPLEMENTOS);',
'                              ',
'FINI040.PP_TRAER_INMUEBLES (I_S_CLAVE_FICHA_TEC  => :P8007_S_CLAVE_FICHA_TEC,',
'                            O_S_INMUEBLES        => :P8007_S_INMUEBLES);',
'FINI040.PP_TRAER_BIENES (I_S_CLAVE_FICHA_TEC     => :P8007_S_CLAVE_FICHA_TEC,',
'                         O_S_OTROS_BIENES        => :P8007_S_OTROS_BIENES);',
'                         ',
':P8007_S_DOCUMENTO_II := :P8007_S_DOCUMENTO;                         '))
,p_attribute_02=>'P8007_S_FICHA_PERSONA,P8007_S_CLAVE_FICHA_TEC,P8007_S_DOCUMENTO'
,p_attribute_03=>'P8007_S_ACT_DOMI,P8007_S_PARCELA_DESC,P8007_S_PARCELA_UBI,P8007_S_IMPLEMENTOS,P8007_S_INMUEBLES,P8007_S_OTROS_BIENES,P8007_S_DOCUMENTO_II'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881124647071744512)
,p_event_id=>wwv_flow_api.id(7881122853061744494)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P8007_S_FICHA_TEC IS NULL THEN',
'	RAISE_APPLICATION_ERROR(-20010,''Nro de Ficha tecnica no puede ser nulo'');	',
'END IF;',
'IF to_number(:P8007_S_FICHA_TEC) <= 0  THEN',
'	RAISE_APPLICATION_ERROR(-20010,''Nro de Ficha tecnica no puede ser menor o igual a 0..!'');	',
'END IF;',
'',
'IF :P8007_S_CLAVE_FICHA_TEC IS NOT NULL THEN	',
'	FINI040.PP_CANT_HIJOS (I_S_CLAVE_FICHA_TEC  => :P8007_S_CLAVE_FICHA_TEC,',
'                           O_S_CANT_HIJOS       => :P8007_S_CANT_HIJOS);',
'END IF;	'))
,p_attribute_02=>'P8007_S_CLAVE_FICHA_TEC'
,p_attribute_03=>'P8007_S_CANT_HIJOS'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881123599817744502)
,p_name=>'cambio_s_acto_domi'
,p_event_sequence=>90
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_S_ACT_DOMI'
,p_condition_element=>'P8007_S_ACT_DOMI'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'S'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881123722301744503)
,p_event_id=>wwv_flow_api.id(7881123599817744502)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8007_S_PARCELA_UBI'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881123798062744504)
,p_event_id=>wwv_flow_api.id(7881123599817744502)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8007_S_PARCELA_UBI'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881123942635744505)
,p_name=>'cambio_ind_exigir_has'
,p_event_sequence=>100
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_S_IND_EXIGIR_HAS'
,p_condition_element=>'P8007_S_IND_EXIGIR_HAS'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'N'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881123965439744506)
,p_event_id=>wwv_flow_api.id(7881123942635744505)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8007_FSOL_CANT_HAS_PROD'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881124111290744507)
,p_event_id=>wwv_flow_api.id(7881123942635744505)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8007_FSOL_CANT_HAS_PROD'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881124219541744508)
,p_name=>'cambio_exp_cred'
,p_event_sequence=>110
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_S_EXP_CRED'
,p_condition_element=>'P8007_S_EXP_CRED'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'N'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881124350587744509)
,p_event_id=>wwv_flow_api.id(7881124219541744508)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8007_S_EXP_CRED_DESC'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881124434808744510)
,p_event_id=>wwv_flow_api.id(7881124219541744508)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8007_S_EXP_CRED_DESC'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881124546913744511)
,p_name=>'validar_ficha_tec2'
,p_event_sequence=>120
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_S_FICHA_TEC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881124784910744514)
,p_name=>'cargar_hijos'
,p_event_sequence=>130
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_S_FICHA_TEC'
,p_condition_element=>'P8007_S_CLAVE_FICHA_TEC'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881124947998744515)
,p_event_id=>wwv_flow_api.id(7881124784910744514)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(7839574320841719919)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881125149299744517)
,p_name=>'validar_fecha_solic'
,p_event_sequence=>140
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_S_FSOL_FEC_SOL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881125210832744518)
,p_event_id=>wwv_flow_api.id(7881125149299744517)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*IF :P8007_S_FSOL_FEC_SOL IS NULL THEN',
'  :P8007_S_FSOL_FEC_SOL := SYSDATE;',
'END IF;*/',
'',
'--',
'IF NOT TO_DATE(:P8007_S_FSOL_FEC_SOL,''DD/MM/YYYY'') BETWEEN TO_DATE(:P8007_CONF_FEC_LIM_MOD,''DD/MM/YYYY'')',
'                              AND TO_DATE(:P8007_CONF_PER_SGTE_FIN,''DD/MM/YYYY'')',
'THEN',
'  RAISE_APPLICATION_ERROR(-20010,''La fecha de solicitud debe estar comprendida entre el ''||',
'                   :P8007_CONF_FEC_LIM_MOD||'' y el ''||:P8007_CONF_PER_SGTE_FIN||'' del periodo de Finanzas..'');',
'END IF;',
'',
'FINI040.PL_VALIDAR_HAB_MES_FIN(TO_DATE(:P8007_S_FSOL_FEC_SOL,''DD/MM/YYYY''),:P_EMPRESA,:APP_USER);'))
,p_attribute_02=>'P8007_S_FSOL_FEC_SOL,P8007_CONF_FEC_LIM_MOD,P8007_CONF_PER_SGTE_FIN'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881125344924744519)
,p_name=>'validar_fecha_asig'
,p_event_sequence=>150
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_S_FSOL_FEC_ASIG'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881125436241744520)
,p_event_id=>wwv_flow_api.id(7881125344924744519)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*IF :P8007_S_FSOL_FEC_SOL IS NULL THEN',
'  :P8007_S_FSOL_FEC_SOL := SYSDATE;',
'END IF;*/',
'',
'--',
'IF NOT TO_DATE(:P8007_S_FSOL_FEC_ASIG,''DD/MM/YYYY'') BETWEEN TO_DATE(:P8007_CONF_FEC_LIM_MOD,''DD/MM/YYYY'')',
'                              AND TO_DATE(:P8007_CONF_PER_SGTE_FIN,''DD/MM/YYYY'')',
'THEN',
unistr('  RAISE_APPLICATION_ERROR(-20010,''La fecha de asignaci\00F3n debe estar comprendida entre el ''||'),
'                   :P8007_CONF_FEC_LIM_MOD||'' y el ''||:P8007_CONF_PER_SGTE_FIN||'' del periodo de Finanzas..'');',
'END IF;',
'',
'FINI040.PL_VALIDAR_HAB_MES_FIN(TO_DATE(:P8007_S_FSOL_FEC_ASIG,''DD/MM/YYYY''),:P_EMPRESA,:APP_USER);'))
,p_attribute_02=>'P8007_S_FSOL_FEC_ASIG,P8007_CONF_FEC_LIM_MOD,P8007_CONF_PER_SGTE_FIN'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881125502144744521)
,p_name=>'Deshabilitar_items'
,p_event_sequence=>160
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881125577243744522)
,p_event_id=>wwv_flow_api.id(7881125502144744521)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8007_S_ESTADO_SOL,P8007_S_VEND_NOM,P8007_S_NOMBRE,P8007_S_COD_CLI,P8007_S_FEC_NAC,P8007_S_SEXO,P8007_S_DOCUMENTO,P8007_S_ESTADO_CIVIL,P8007_S_DEPARTAMENTO,P8007_S_CIUDAD,P8007_S_DIRECCION,P8007_S_REF_UBI,P8007_S_VIVIENDA,P8007_S_RECIDE,P8007_S_TELEF'
||'ONO,P8007_S_CONYUGE,P8007_S_NOM_GARANTE,P8007_S_RUC_DV,P8007_S_DV,P8007_S_CATEG,P8007_S_CATEG_DESC,P8007_S_MIEMBROS_UF,P8007_S_APORTANTES_UF,P8007_LABEL1,P8007_S_MON_SIMBOLO,P8007_S_DEPENDIENTES_UF,P8007_S_ASIST_ESC,P8007_S_PARCELA_DESC,P8007_S_PARCE'
||'LA_UBI,P8007_S_HAS_PROPIAS,P8007_S_UTIL_ALQ,P8007_S_HAS_ALQ,P8007_S_UTIL_PROPIAS,P8007_S_TOT_UTIL,P8007_S_ACTIV_EDAD_INI,P8007_S_TOT_IMP_MENSUAL,P8007_S_EXP_CRED_DESC,P8007_FSOL_ANTIG_TERRENO,P8007_FSOL_ANTIG_TRAB,P8007_S_OTROS_ING,P8007_S_EXP_CRED'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881125685248744523)
,p_name=>'cambio_asesor'
,p_event_sequence=>170
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_FSOL_VENDEDOR'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881125818178744524)
,p_event_id=>wwv_flow_api.id(7881125685248744523)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P8007_FSOL_VENDEDOR IS NOT NULL THEN',
'  FINI040.PP_TRAER_DESC_VENDEDOR(I_FSOL_VENDEDOR    => :P8007_FSOL_VENDEDOR,',
'                                 I_EMPRESA          => :P_EMPRESA,',
'                                 O_S_VEND_NOMB      => :P8007_S_VEND_NOM);',
'END IF;'))
,p_attribute_02=>'P8007_FSOL_VENDEDOR'
,p_attribute_03=>'P8007_S_VEND_NOM'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881125869037744525)
,p_name=>'cambio_garante'
,p_event_sequence=>180
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_FSOL_GARANTE'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881126018295744526)
,p_event_id=>wwv_flow_api.id(7881125869037744525)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P8007_FSOL_GARANTE IS NOT NULL THEN',
'	FINI040.PP_TRAER_DESC_GARANTE (I_FSOL_GARANTE     => :P8007_FSOL_GARANTE,',
'                                   I_EMPRESA          => :P_EMPRESA,',
'                                   O_S_NOM_GARANTE    => :P8007_S_NOM_GARANTE,',
'                                   O_S_GARANTE        => :P8007_S_GARANTE);',
'	IF :P8007_FSOL_GARANTE = :P8007_S_FICHA_PERSONA THEN ',
'	 		RAISE_APPLICATION_ERROR(-20010,''El Garante no puede ser igual al Solicitante..!'');',
'	END IF;',
'END IF;'))
,p_attribute_02=>'P8007_FSOL_GARANTE,P8007_S_FICHA_PERSONA'
,p_attribute_03=>'P8007_S_NOM_GARANTE,P8007_S_GARANTE'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881126125470744527)
,p_name=>'cambio_obse_parcelas'
,p_event_sequence=>190
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_FSOL_OBS_DIRE_PARC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881864234758129379)
,p_event_id=>wwv_flow_api.id(7881126125470744527)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'    FINI040.PP_INICIAR_COL_DESGLOCE;',
'END;'))
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881126172635744528)
,p_event_id=>wwv_flow_api.id(7881126125470744527)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'	V_CONTROL NUMBER;',
'BEGIN	',
'		SELECT COUNT(*)',
'		INTO V_CONTROL',
'		FROM FIN_FICHA_SOL_DET_DEST_FINAN',
'		WHERE FDDF_CLAVE_SOL = :P8007_FSOL_CLAVE',
'		  AND FDDF_EMPR = :P_EMPRESA;',
'		  ',
'		  ',
'		IF V_CONTROL = 0 THEN',
'        ',
'			FINI040.PP_CARGAR_DESGLOCE (I_S_FICHA_MONEDA => :P8007_S_FICHA_MONEDA,',
'                                        I_S_CATEG        => :P8007_S_CATEG);',
'		END IF;			  ',
'END;'))
,p_attribute_02=>'P8007_S_FICHA_MONEDA,P8007_S_CATEG'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881864760283129385)
,p_name=>'validar_cant_has_prod'
,p_event_sequence=>200
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_FSOL_CANT_HAS_PROD'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881864917304129386)
,p_event_id=>wwv_flow_api.id(7881864760283129385)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF to_number(:P8007_FSOL_CANT_HAS_PROD) < 0 THEN',
'	RAISE_APPLICATION_ERROR(-20010,''Valor debe ser mayor o igual a 0..!'');',
'END IF;'))
,p_attribute_02=>'P8007_FSOL_CANT_HAS_PROD'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881865009479129387)
,p_name=>'validar_plazo_otorg'
,p_event_sequence=>210
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_S_FSOL_FEC_PLAZO_OTORG'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881865077409129388)
,p_event_id=>wwv_flow_api.id(7881865009479129387)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF TO_DATE(:P8007_S_FSOL_FEC_PLAZO_OTORG,''DD/MM/YYYY'') < TO_DATE(:P8007_CONF_PER_ACT_INI,''DD/MM/YYYY'')',
'THEN',
'  RAISE_APPLICATION_ERROR(-20010,''La fecha debe ser mayor a la fecha Inicial ''||',
'  :P8007_CONF_PER_ACT_INI||'' del periodo de Finanzas..'');',
'END IF;',
''))
,p_attribute_02=>'P8007_S_FSOL_FEC_PLAZO_OTORG,P8007_CONF_PER_ACT_INI'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881865231965129389)
,p_name=>'dest_nonulo'
,p_event_sequence=>220
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_FSOL_OBS_DEST_FINAN'
,p_bind_type=>'bind'
,p_bind_event_type=>'focusout'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881865318993129390)
,p_event_id=>wwv_flow_api.id(7881865231965129389)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P8007_FSOL_OBS_DEST_FINAN IS NULL THEN',
'	RAISE_APPLICATION_ERROR(-20010,''Dest. del Financiamiento no pueder ser nulo..!'');',
'END IF;'))
,p_attribute_02=>'P8007_FSOL_OBS_DEST_FINAN'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881865368399129391)
,p_name=>'validar_monto_estado'
,p_event_sequence=>230
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_FSOL_MONTO_ESTADO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881865484470129392)
,p_event_id=>wwv_flow_api.id(7881865368399129391)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF to_number(:P8007_FSOL_MONTO_ESTADO) <= 0 THEN',
'	RAISE_APPLICATION_ERROR(-20010,''El monto debe ser mayor a 0..!'');',
'END IF;	',
'IF to_number(:P8007_FSOL_MONTO_ESTADO) > to_number(:P8007_S_MONTO_CRED) THEN',
'	RAISE_APPLICATION_ERROR(-20010,''El monto Aprobado no puede ser mayor al Solicitado..!'');',
'END IF;'))
,p_attribute_02=>'P8007_FSOL_MONTO_ESTADO,P8007_S_MONTO_CRED'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881865616643129393)
,p_event_id=>wwv_flow_api.id(7881865368399129391)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P8007_FSOL_ESTADO IN (''A'',''R'') THEN',
'	IF :P8007_FSOL_MONTO_ESTADO IS NULL THEN',
'		RAISE_APPLICATION_ERROR(-20010,''El monto no puede ser nulo..!'');',
'	END IF;',
'END IF;'))
,p_attribute_02=>'P8007_FSOL_ESTADO,P8007_FSOL_MONTO_ESTADO'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881865911479129396)
,p_name=>'click_tipo_venc'
,p_event_sequence=>240
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_FSOL_TIP_VENC'
,p_condition_element=>'P8007_FSOL_TIP_VENC'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'M'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881865966359129397)
,p_event_id=>wwv_flow_api.id(7881865911479129396)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8007_FSOL_S_DIAS_ENTRE_CUOTAS'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881866068869129398)
,p_event_id=>wwv_flow_api.id(7881865911479129396)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8007_FSOL_S_DIAS_ENTRE_CUOTAS'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881866175279129399)
,p_name=>'cambio_dias_cuotas'
,p_event_sequence=>250
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_FSOL_S_DIAS_ENTRE_CUOTAS'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881866335643129400)
,p_event_id=>wwv_flow_api.id(7881866175279129399)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P8007_FSOL_TIP_VENC = ''D'' THEN',
'	 IF :P8007_FSOL_S_DIAS_ENTRE_CUOTAS IS NULL THEN',
unistr('	 	  RAISE_APPLICATION_ERROR(-20010,''Debe seleccionar la cant. de d\00EDas!'');'),
'	 ELSE',
'	 	  :P8007_FSOL_S_DIAS_GRACIA:= 0;',
'	 END IF;',
'END IF;'))
,p_attribute_02=>'P8007_FSOL_TIP_VENC,P8007_FSOL_S_DIAS_ENTRE_CUOTAS'
,p_attribute_03=>'P8007_FSOL_S_DIAS_GRACIA'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881866540739129402)
,p_name=>'cambio_interes'
,p_event_sequence=>260
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_FSOL_INTERES'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881866595208129403)
,p_event_id=>wwv_flow_api.id(7881866540739129402)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF to_number(:P8007_FSOL_INTERES) < 0 OR to_number(:P8007_FSOL_INTERES) >= 100 THEN',
unistr('	 RAISE_APPLICATION_ERROR(-20010,''No corresponde al rango de %inter\00E9s'');'),
'END IF;'))
,p_attribute_02=>'P8007_FSOL_INTERES'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881866669784129404)
,p_name=>'cambio_plazo'
,p_event_sequence=>270
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_FSOL_S_PLAZO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881866821315129405)
,p_event_id=>wwv_flow_api.id(7881866669784129404)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF  to_number(:P8007_FSOL_S_PLAZO) < 0 THEN',
'	  RAISE_APPLICATION_ERROR(-20010,''El plazo no puede ser negativo!'');',
'END IF;',
'',
'IF to_number(:P8007_FSOL_S_PLAZO) <= to_number(:P8007_FSOL_S_DIAS_GRACIA) THEN',
unistr('	  RAISE_APPLICATION_ERROR(-20010,''El plazo debe ser mayor al Per\00EDodo de gracia!'');'),
'END IF;'))
,p_attribute_02=>'P8007_FSOL_S_PLAZO,P8007_FSOL_S_DIAS_GRACIA'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881866919708129406)
,p_name=>'cambio_monto_cuo'
,p_event_sequence=>280
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_FSOL_S_MONTO_CUO'
,p_bind_type=>'bind'
,p_bind_event_type=>'focusout'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881866957477129407)
,p_event_id=>wwv_flow_api.id(7881866919708129406)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'	v_cuo number;',
'begin',
'IF NVL(:P8007_FSOL_S_MONTO_CUO,0) <> 0 THEN',
'   IF to_number(:P8007_FSOL_S_MONTO_CUO) > to_number(:P8007_S_MONTO_CRED) THEN',
'   	  RAISE_APPLICATION_ERROR(-20010,''El monto ingresado de la cuota es mayor al capital!'');',
'      :P8007_FSOL_S_CANT_CUO := NVL(:P8007_FSOL_S_DIAS_GRACIA,0)+1;',
'      FINI040.PP_GENERAR_MONTO_CUO (I_FSOL_S_DIAS_ENTRE_CUOTAS  => :P8007_FSOL_S_DIAS_ENTRE_CUOTAS,',
'                                    I_FSOL_INTERES              => :P8007_FSOL_INTERES,',
'                                    I_S_MONTO_CRED              => :P8007_S_MONTO_CRED,',
'                                    I_FSOL_S_CANT_CUO           => :P8007_FSOL_S_CANT_CUO,',
'                                    I_FSOL_S_DIAS_GRACIA        => :P8007_FSOL_S_DIAS_GRACIA,',
'                                    O_FSOL_S_MONTO_CUO          => :P8007_FSOL_S_MONTO_CUO);',
'      --:PARAMETER.BANDERA_CUO := 0;',
'      v_cuo := :P8007_FSOL_S_CANT_CUO;',
'      --GO_ITEM(''GENERAR_CUOTAS'');',
'      if v_cuo <> :P8007_FSOL_S_CANT_CUO then',
'      	 :P8007_FSOL_S_CANT_CUO := v_cuo;',
'      end if; ',
'   ELSE',
'   	  :P8007_BANDERA_CUO:=1;',
'      FINI040.PP_GENERAR_CANT_CUO (I_FSOL_INTERES               => :P8007_FSOL_INTERES,',
'                                   I_FSOL_S_DIAS_ENTRE_CUOTAS   => :P8007_FSOL_S_DIAS_ENTRE_CUOTAS,',
'                                   I_S_MONTO_CRED               => :P8007_S_MONTO_CRED,',
'                                   I_FSOL_S_MONTO_CUO           => :P8007_FSOL_S_MONTO_CUO,',
'                                   I_FSOL_FEC_PRIM_VTO          => to_date(:P8007_S_FSOL_FEC_PRIM_VTO,''dd/mm/yyyy''),',
'                                   I_FSOL_FEC_ESTADO            => to_date(:P8007_S_FSOL_FEC_ESTADO,''dd/mm/yyyy''),',
'                                   I_FSOL_TIP_VENC              => :P8007_FSOL_TIP_VENC,',
'                                   I_FSOL_S_DIAS_GRACIA         => :P8007_FSOL_S_DIAS_GRACIA,',
'                                   O_FSOL_S_CANT_CUO            => :P8007_FSOL_S_CANT_CUO,',
'                                   O_P_CANT_CUO                 => :P8007_P_CANT_CUO);',
'      --NEXT_ITEM;   ',
'   END IF;',
'else',
'   	 --NEXT_ITEM;   ',
'     NULL;',
'END IF;',
'end;'))
,p_attribute_02=>'P8007_FSOL_S_MONTO_CUO,P8007_S_MONTO_CRED,P8007_FSOL_S_DIAS_GRACIA,P8007_FSOL_S_DIAS_ENTRE_CUOTAS,P8007_FSOL_INTERES,P8007_FSOL_S_CANT_CUO,P8007_S_FSOL_FEC_PRIM_VTO,P8007_S_FSOL_FEC_ESTADO,P8007_FSOL_TIP_VENC'
,p_attribute_03=>'P8007_FSOL_S_CANT_CUO,P8007_FSOL_S_MONTO_CUO,P8007_BANDERA_CUO,P8007_P_CANT_CUO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
end;
/
begin
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7881867429628129411)
,p_name=>'cambio_cant_cuo'
,p_event_sequence=>290
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_FSOL_S_CANT_CUO'
,p_condition_element=>'P8007_FSOL_S_CANT_CUO'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7881867456926129412)
,p_event_id=>wwv_flow_api.id(7881867429628129411)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF to_number(:P8007_FSOL_S_CANT_CUO) <= to_number(:P8007_FSOL_S_DIAS_GRACIA) THEN',
unistr('	 RAISE_APPLICATION_ERROR(-20010,''La Cantidad de Cuotas debe ser Mayor al per\00EDodo de gracia!'');'),
'END IF;',
'',
'',
'IF :P8007_BANDERA_CUO = 0 THEN',
'       FINI040.PP_GENERAR_MONTO_CUO(I_FSOL_S_DIAS_ENTRE_CUOTAS  => :P8007_FSOL_S_DIAS_ENTRE_CUOTAS,',
'                                    I_FSOL_INTERES              => :P8007_FSOL_INTERES,',
'                                    I_S_MONTO_CRED              => :P8007_S_MONTO_CRED,',
'                                    I_FSOL_S_CANT_CUO           => :P8007_FSOL_S_CANT_CUO,',
'                                    I_FSOL_S_DIAS_GRACIA        => :P8007_FSOL_S_DIAS_GRACIA,',
'                                    O_FSOL_S_MONTO_CUO          => :P8007_FSOL_S_MONTO_CUO);',
'       :P8007_BANDERA_CUO := 0;',
'ELSIF :P8007_P_CANT_CUO <> :P8007_FSOL_S_CANT_CUO THEN',
'      FINI040.PP_GENERAR_MONTO_CUO(I_FSOL_S_DIAS_ENTRE_CUOTAS  => :P8007_FSOL_S_DIAS_ENTRE_CUOTAS,',
'                                   I_FSOL_INTERES              => :P8007_FSOL_INTERES,',
'                                   I_S_MONTO_CRED              => :P8007_S_MONTO_CRED,',
'                                   I_FSOL_S_CANT_CUO           => :P8007_FSOL_S_CANT_CUO,',
'                                   I_FSOL_S_DIAS_GRACIA        => :P8007_FSOL_S_DIAS_GRACIA,',
'                                   O_FSOL_S_MONTO_CUO          => :P8007_FSOL_S_MONTO_CUO);',
'     :P8007_BANDERA_CUO := 0;',
'END IF; 	 ',
'',
':P8007_FSOL_S_PLAZO := :P8007_FSOL_S_CANT_CUO;'))
,p_attribute_02=>'P8007_BANDERA_CUO,P8007_P_CANT_CUO,P8007_FSOL_S_DIAS_ENTRE_CUOTAS,P8007_FSOL_INTERES,P8007_S_MONTO_CRED,P8007_FSOL_S_CANT_CUO,P8007_FSOL_S_DIAS_GRACIA'
,p_attribute_03=>'P8007_BANDERA_CUO,P8007_FSOL_S_MONTO_CUO,P8007_FSOL_S_PLAZO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7884485023600251892)
,p_name=>'quitar_Archivo'
,p_event_sequence=>300
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(7844811412703974994)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7884485199087251894)
,p_event_id=>wwv_flow_api.id(7884485023600251892)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'obtenerRegistrosSeleccionados();'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7885759325849457289)
,p_event_id=>wwv_flow_api.id(7884485023600251892)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P8007_ITEM IS NULL THEN',
unistr('     raise_application_error(-20010,''No seleccion\00F3 ningun archivo para eliminar'');'),
'END IF;'))
,p_attribute_02=>'P8007_ITEM'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7885759061294457287)
,p_event_id=>wwv_flow_api.id(7884485023600251892)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_CONFIRM'
,p_attribute_01=>unistr('\00BFSeguro que desea eliminar el archivo?')
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7884486981286251912)
,p_event_id=>wwv_flow_api.id(7884485023600251892)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*update  FIN_FICHA_SOL_DET_ARCH',
' 	set FARC_IMG = null',
'  where FARC_CLAVE_SOL = :P8007_FILE_ID',
'  	and FARC_ITEM = :P8007_ITEM',
'  	and FARC_EMPR = :P_EMPRESA;*/',
'    ',
'APEX_COLLECTION.UPDATE_MEMBER_ATTRIBUTE (',
'    p_collection_name => ''FINI040_ARCHIVOS'',',
'    p_seq             => :P8007_ITEM,',
'    p_attr_number     => 9,',
'    p_attr_value      => null); '))
,p_attribute_02=>'P8007_FILE_ID,P8007_ITEM'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7885759021375457286)
,p_event_id=>wwv_flow_api.id(7884485023600251892)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ALERT'
,p_attribute_01=>unistr('Se elimin\00F3 el archivo.')
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7885759202509457288)
,p_event_id=>wwv_flow_api.id(7884485023600251892)
,p_event_result=>'TRUE'
,p_action_sequence=>70
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8007_FARC_IMG'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7884487127874251913)
,p_name=>'borrar'
,p_event_sequence=>310
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(7844467450938176328)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7884487200223251914)
,p_event_id=>wwv_flow_api.id(7884487127874251913)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_CONFIRM'
,p_attribute_01=>unistr('\00BFBORRAR LOS DATOS?')
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7884487265360251915)
,p_event_id=>wwv_flow_api.id(7884487127874251913)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'    FINI040.PP_BORRAR_REGISTRO (I_FSOL_CLAVE  => :P8007_FSOL_CLAVE);',
'EXCEPTION WHEN OTHERS THEN',
'            RAISE_APPLICATION_ERROR(-20010,''Registro no borrado'');',
'END;'))
,p_attribute_02=>'P8007_FSOL_CLAVE'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7884487946910251921)
,p_name=>'da_copiar'
,p_event_sequence=>320
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(7844463434758176288)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7884487965225251922)
,p_event_id=>wwv_flow_api.id(7884487946910251921)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    PROCEDURE PP_CARGAR_SOLICITUD IS',
'    BEGIN',
'    --*	',
'      SELECT FSOL_CLAVE,FSOL_CLAVE_FICH_TEC,FSOL_FEC_SOL,FSOL_FEC_ASIG,',
'                 FSOL_OBS_SOL,FSOL_FORMA_CONOC_EMP,FSOL_IND_FEDERACION,',
'                 FSOL_NOM_FEDERACION,FSOL_GARANTE,FSOL_ANTIG_TERRENO,',
'                 FSOL_ANTIG_TRAB,FSOL_OBS_DIRE_PARC, FSOL_MON',
'      INTO :P8007_S_FSOL_CLAVE,:P8007_S_CLAVE_COPIAR,:P8007_FSOL_FEC_SOL,:P8007_FSOL_FEC_ASIG,',
'             :P8007_FSOL_OBS_SOL,:P8007_FSOL_FORMA_CONOC_EMP,:P8007_FSOL_IND_FEDERACION,',
'             :P8007_FSOL_NOM_FEDERACION,:P8007_FSOL_GARANTE,:P8007_FSOL_ANTIG_TERRENO,',
'             :P8007_FSOL_ANTIG_TRAB,:P8007_FSOL_OBS_DIRE_PARC, :P8007_FSOL_MON',
'        FROM FIN_FICHA_SOL',
'        WHERE FSOL_NRO=:P8007_S_NRO_FICHA',
'          AND FSOL_EMPR = :P_EMPRESA;',
'    --*',
'        :P8007_S_FSOL_FEC_SOL:=TO_CHAR(:P8007_FSOL_FEC_SOL,''DD-MM-YYYY'');',
'        :P8007_S_FSOL_FEC_ASIG:=TO_CHAR(:P8007_FSOL_FEC_ASIG,''DD-MM-YYYY'');',
'    --*',
'        FINI040.PP_BUSCAR_FICHA_COPIAR(I_S_CLAVE_COPIAR     => :P8007_S_CLAVE_COPIAR,',
'                                       O_S_FICHA_TEC        => :P8007_S_FICHA_TEC,',
'                                       O_S_FICHA_PERSONA    => :P8007_S_FICHA_PERSONA,',
'                                       O_S_FICHA_MONEDA     => :P8007_S_FICHA_MONEDA);',
'        PP_CARGAR_DATOS_PERS_COPIAR;',
'        PP_TRAER_DESC_GARANTE;',
'        PP_TRAER_HIJOS_COPIAR;',
'        PP_TRAER_REF_COPIAR;',
'    --*	',
'    EXCEPTION',
'        WHEN NO_DATA_FOUND THEN',
'            RAISE_APPLICATION_ERROR(-20010,''Nro. de Solicitud inexistente..!'');',
'        WHEN OTHERS THEN',
'        NULL;',
'    END;',
'',
'--PRINCIPAL',
'BEGIN',
'IF :P8007_S_NRO_FICHA IS NOT NULL THEN',
'	PP_CARGAR_SOLICITUD;',
'	:P8007_P_COPIAR:=''N'';',
'END IF;	',
'END;'))
,p_attribute_02=>'P8007_S_NRO_FICHA'
,p_attribute_03=>'P8007_P_COPIAR'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7885759728974457293)
,p_name=>'cargar_imagen'
,p_event_sequence=>330
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_SEQID_IMAGEN'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7885760025946457296)
,p_event_id=>wwv_flow_api.id(7885759728974457293)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'null;'
,p_attribute_02=>'P8007_SEQID_IMAGEN'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7885759813592457294)
,p_event_id=>wwv_flow_api.id(7885759728974457293)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8007_FARC_IMG'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7885760646575457302)
,p_event_id=>wwv_flow_api.id(7885759728974457293)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(7885760695007457303)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7906805814127296417)
,p_name=>'validar_cantidad'
,p_event_sequence=>340
,p_triggering_element_type=>'COLUMN'
,p_triggering_region_id=>wwv_flow_api.id(7906803444366296393)
,p_triggering_element=>'FDDF_CANT'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7906805857803296418)
,p_event_id=>wwv_flow_api.id(7906805814127296417)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :FDDF_CANT < 0 THEN',
'	RAISE_APPLICATION_ERROR(-20010,''Cantidad debe ser mayor o igual a 0..!'');',
'END IF;	'))
,p_attribute_02=>'FDDF_CANT'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7906805964814296419)
,p_name=>'cambio_precio'
,p_event_sequence=>350
,p_triggering_element_type=>'COLUMN'
,p_triggering_region_id=>wwv_flow_api.id(7906803444366296393)
,p_triggering_element=>'FDDF_PRECIO_MON'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7906806058848296420)
,p_event_id=>wwv_flow_api.id(7906805964814296419)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :FDDF_PRECIO_MON < 0 THEN',
'	RAISE_APPLICATION_ERROR(-20010,''Precio debe ser mayor o igual a 0..!'');',
'END IF;'))
,p_attribute_02=>'FDDF_PRECIO_MON'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7906806242974296421)
,p_name=>'calcular_importe'
,p_event_sequence=>360
,p_triggering_element_type=>'COLUMN'
,p_triggering_region_id=>wwv_flow_api.id(7906803444366296393)
,p_triggering_element=>'FDDF_PRECIO_MON,FDDF_CANT'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7906806343508296422)
,p_event_id=>wwv_flow_api.id(7906806242974296421)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':S_IMP_MENSUAL := NVL(:FDDF_CANT,0) * NVL(:FDDF_PRECIO_MON,0);'
,p_attribute_02=>'FDDF_CANT,FDDF_PRECIO_MON'
,p_attribute_03=>'S_IMP_MENSUAL'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7906806449127296423)
,p_event_id=>wwv_flow_api.id(7906806242974296421)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'sumar();'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7906806523969296424)
,p_name=>'cambio_tipo_Ref'
,p_event_sequence=>370
,p_triggering_element_type=>'COLUMN'
,p_triggering_region_id=>wwv_flow_api.id(7906264888362027015)
,p_triggering_element=>'FREF_TIPO_REF'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7906806581498296425)
,p_event_id=>wwv_flow_api.id(7906806523969296424)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF NVL(:FREF_TIPO_REF,''.'') <> ''.'' THEN',
'	IF :FREF_TIPO_REF IS NULL THEN',
'		RAISE_APPLICATION_ERROR(-20010,''Tipo de Ref. no puede ser nulo..!'');',
'	END IF;	',
'	IF NOT :FREF_TIPO_REF IN (''C'',''P'') THEN',
'		RAISE_APPLICATION_ERROR(-20010,''Tipo de Ref. debe ser C=Comerciales, P=Personales..!'');',
'	END IF;',
'END IF;  '))
,p_attribute_02=>'FREF_TIPO_REF'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7906806662889296426)
,p_name=>'cambio_nombre'
,p_event_sequence=>380
,p_triggering_element_type=>'COLUMN'
,p_triggering_region_id=>wwv_flow_api.id(7906264888362027015)
,p_triggering_element=>'FREF_NOM'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7906806825469296427)
,p_event_id=>wwv_flow_api.id(7906806662889296426)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :FREF_NOM IS NULL THEN',
'	RAISE_APPLICATION_ERROR(-20010,''Razon Social no puede ser nulo..!'');',
'END IF;	'))
,p_attribute_02=>'FREF_NOM'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7906806857740296428)
,p_name=>'cambio_direccion'
,p_event_sequence=>390
,p_triggering_element_type=>'COLUMN'
,p_triggering_region_id=>wwv_flow_api.id(7906264888362027015)
,p_triggering_element=>'FREF_DIR'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7906885495650127079)
,p_event_id=>wwv_flow_api.id(7906806857740296428)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :FREF_DIR IS NULL THEN',
'	RAISE_APPLICATION_ERROR(-20010,''Direccion no puede ser nulo..!'');',
'END IF;	'))
,p_attribute_02=>'FREF_DIR'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7906885651273127080)
,p_name=>'cambio_imp_cuo'
,p_event_sequence=>400
,p_triggering_element_type=>'COLUMN'
,p_triggering_region_id=>wwv_flow_api.id(7906261851701026984)
,p_triggering_element=>'S_IMP_CUO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7906885660890127081)
,p_event_id=>wwv_flow_api.id(7906885651273127080)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
' IF :P8007_FSOL_TIP_GEN_CUO = ''FE'' THEN',
'   IF to_number(:S_IMP_CUO) > (to_number(:FSDC_SALDO_CAPITAL) + to_number(:FSDC_INTERES)) THEN',
unistr('  	 RAISE_APPLICATION_ERROR(-20010,''El importe de la cuota no puede ser mayor a la suma del saldo capital y el importe de inter\00E9s!'');'),
'   ELSIF to_number(:S_IMP_CUO) < to_number(:FSDC_INTERES) THEN',
unistr('     RAISE_APPLICATION_ERROR(-20010,''El importe de la cuota no puede ser menor al importe de inter\00E9s!'');'),
'   END IF;',
' END IF;',
' ',
' --:PARAMETER.MONTO_CUO:=:BCUO.S_IMP_CUO;',
' null;'))
,p_attribute_02=>'P8007_FSOL_TIP_GEN_CUO,S_IMP_CUO,FSDC_SALDO_CAPITAL,FSDC_INTERES'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7906886017258127084)
,p_name=>'guardar'
,p_event_sequence=>410
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(7844467254109176326)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7906886134162127085)
,p_event_id=>wwv_flow_api.id(7906886017258127084)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'Aceptar'
,p_attribute_02=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7906886323927127087)
,p_name=>'calcular_cuota'
,p_event_sequence=>420
,p_triggering_element_type=>'COLUMN'
,p_triggering_region_id=>wwv_flow_api.id(7906261851701026984)
,p_triggering_element=>'FSDC_CAPITAL,FSDC_INTERES'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7906886441952127088)
,p_event_id=>wwv_flow_api.id(7906886323927127087)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':S_IMP_CUO := to_number(:FSDC_CAPITAL) + to_number(:FSDC_INTERES);'
,p_attribute_02=>'FSDC_CAPITAL,FSDC_INTERES'
,p_attribute_03=>'S_IMP_CUO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7906886924247127093)
,p_name=>'asignar_,moneda'
,p_event_sequence=>430
,p_triggering_element_type=>'COLUMN'
,p_triggering_region_id=>wwv_flow_api.id(7906261851701026984)
,p_triggering_element=>'FSDC_FEC_VENC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7906887010391127094)
,p_event_id=>wwv_flow_api.id(7906886924247127093)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'COLUMN'
,p_affected_elements=>'S_MON_SIMB'
,p_attribute_01=>'FUNCTION_BODY'
,p_attribute_06=>wwv_flow_string.join(wwv_flow_t_varchar2(
'	IF :P8007_S_FICHA_MONEDA = 2 THEN',
'	    return ''U$.'';',
'	ELSE ',
'	    return ''GS.'';',
'	END IF;',
'	'))
,p_attribute_07=>'P8007_S_FICHA_MONEDA'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7924780710300417607)
,p_name=>'CARGAR_FSOL_NRO'
,p_event_sequence=>440
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8007_FSOL_NRO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7924780771554417608)
,p_event_id=>wwv_flow_api.id(7924780710300417607)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'',
'FINI040.PP_BUSCAR_NRO_SOLICITUD(I_EMPRESA => :P_EMPRESA,',
'                                IO_NRO_SOL => :P8007_FSOL_NRO);',
'                                '))
,p_attribute_02=>'P8007_FSOL_NRO'
,p_attribute_03=>'P8007_FSOL_NRO'
,p_attribute_04=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(7895766574656239287)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'limpiar'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(7895766659084239288)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'limpiar_colecciones'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'    FINI040.PP_INICIAR_COL_DESGLOCE;',
'    ',
'    FINI040.PP_INICIAR_COL_REFERENCIAS;',
'    ',
'    FINI040.PP_INICIAR_COL_CUOTAS;',
'    ',
'    FINI040.PP_INICIAR_COL_ARCHIVOS;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(7879791076447359688)
,p_process_sequence=>30
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'CARGAR_DATOS'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'	:P8007_G_ORIGEN := '''';',
'    FINI040.PP_INICIAR_COL_DESGLOCE;',
'	FINI040.PP_DESGLOCE_VACIO;',
'  /*  ',
'  DECLARE',
'    ImpresoraNoRegistrada EXCEPTION;',
'    PRAGMA EXCEPTION_INIT(ImpresoraNoRegistrada,-302000);',
'    V_ALERT NUMBER;',
'    v_path_impresora varchar2(50);',
'    V_impresora text_io.file_type;',
'  BEGIN',
'    TOOL_ENV.GETVAR(''DIR_DOCUMENTOS_PATH'', V_Path_Impresora); --obtener direccion de la impresora',
'    IF V_PATH_IMPRESORA IS NULL THEN',
'      SET_ALERT_PROPERTY(''AL_MESSAGE'',ALERT_MESSAGE_TEXT,''Falta registrar DIR_DOCUMENTOS_PATH en el Register de Windows!'');',
'      V_ALERT := SHOW_ALERT(''AL_MESSAGE'');',
'    END IF;',
'  EXCEPTION',
'    WHEN ImpresoraNoRegistrada THEN',
'      SET_ALERT_PROPERTY(''AL_MESSAGE'',ALERT_MESSAGE_TEXT,''No se puede encontrar: ''||v_path_impresora||'' en el register de Windows necesario para adjuntar imagenes y archivos'');',
'      V_ALERT := SHOW_ALERT(''AL_MESSAGE'');',
'  END;*/',
'',
'	IF :P8007_P_CLAVE_RECEPCION IS NULL THEN',
'		:P8007_P_CLAVE_RECEPCION := 0;',
'	END IF;',
'	',
'	IF :P8007_P_CLAVE_PRESUPUESTO IS NULL THEN',
'		:P8007_P_CLAVE_PRESUPUESTO := 0;',
'	END IF;',
'	   ',
'	',
'	:P8007_P_GENERA_OR := 0;',
'		',
'--*',
'BEGIN',
' SELECT CONF_FEC_LIM_MOD,     ',
'		CONF_PER_ACT_FIN,',
' 		CONF_PER_ACT_INI,',
'	    CONF_PER_SGTE_INI,    ',
'		CONF_PER_SGTE_FIN',
'  INTO  :P8007_CONF_FEC_LIM_MOD, ',
'		:P8007_CONF_PER_ACT_FIN,',
'  		:P8007_CONF_PER_ACT_INI,',
'	    :P8007_CONF_PER_SGTE_INI,',
'		:P8007_CONF_PER_SGTE_FIN',
'  FROM  FIN_CONFIGURACION',
'  WHERE CONF_EMPR = :P_EMPRESA;',
'  ',
'EXCEPTION',
'  ',
'  WHEN OTHERS THEN',
'    NULL;  ',
'END;  ',
'--*',
'	/*SET_ITEM_PROPERTY(''BPER.S_NRO_FICHA'',ENABLED, PROPERTY_TRUE);',
'	SET_ITEM_PROPERTY(''BPER.S_NRO_FICHA'',NAVIGABLE, PROPERTY_TRUE);',
'	SET_ITEM_PROPERTY(''BPER.COPIAR'',ENABLED, PROPERTY_TRUE);',
'	SET_ITEM_PROPERTY(''BPER.COPIAR'',NAVIGABLE, PROPERTY_TRUE);*/',
'--*',
'',
'	:P8007_PP_HABILITAR := ''S'';',
'  --GO_ITEM(''BPER.FSOL_NRO'');',
'        ',
'    	BEGIN',
'          SELECT NVL(opem_ind_aprueba_prestamo,''N'')',
'            INTO :P8007_IND_OPER_APRUEB',
'            FROM GEN_OPERADOR, ',
'                 GEN_OPERADOR_EMPRESA GE',
'           WHERE OPER_LOGIN = :APP_USER',
'             AND OPER_CODIGO = GE.OPEM_OPER',
'             AND GE.OPEM_EMPR =  :P_EMPRESA;',
'            ',
'        EXCEPTION',
'            WHEN OTHERS THEN',
'                 :P8007_IND_OPER_APRUEB := ''N'';',
'        END;',
'',
'',
'  IF :P8007_P_NRO_SOL IS NOT NULL THEN',
'  	:P8007_FSOL_NRO:=:P8007_P_NRO_SOL;',
'  END IF;	',
'',
'',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(7879794327654359720)
,p_process_sequence=>40
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_INIT'
,p_process_sql_clob=>':P8007_LUGAR_ORIGEN_REPLICA:=GEN_LUGAR_ORIGEN;'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(7906885940985127083)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'obtener_clave'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P8007_FSOL_CLAVE IS NULL THEN',
'  :P8007_FSOL_CLAVE	:=FIN_SEQ_SOL_NEXTVAL;',
' END IF; '))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'Aceptar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(7906803351765296392)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(7906264888362027015)
,p_process_type=>'NATIVE_IG_DML'
,p_process_name=>'actualizar_coleccion_referencias'
,p_attribute_01=>'PLSQL_CODE'
,p_attribute_04=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'l_count     NUMBER;',
'begin',
'    case :APEX$ROW_STATUS',
'    when ''C'' then',
'        ',
'        l_count := APEX_COLLECTION.COLLECTION_MEMBER_COUNT(p_collection_name => ''FINI040_REFERENCIAS'');',
'        ',
'        APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => ''FINI040_REFERENCIAS'',',
'                                                 P_C001      => :P8007_FSOL_CLAVE,   --FREF_CLAVE_SOL',
'                                                 P_C002      => l_count+1,',
'                                                 P_C003      => :FREF_TIPO_REF,',
'                                                 P_C004      => :FREF_NOM,',
'                                                 P_C005      => :FREF_DIR,',
'                                                 P_C006      => :FREF_TEL,',
'                                                 P_C007      => GEN_LUGAR_ORIGEN,     --FREF_LUGAR_ORIGEN_REPLICA',
'                                                 P_C008      => :P_EMPRESA);          --FREF_EMPR',
'    when ''U'' then',
'        APEX_COLLECTION.UPDATE_MEMBER(P_COLLECTION_NAME => ''FINI040_REFERENCIAS'',',
'                                        P_SEQ           => :ITEM,',
'                                        P_C001          => :FREF_CLAVE_SOL,',
'                                        P_C002          => :FREF_ITEM,',
'                                        P_C003          => :FREF_TIPO_REF,',
'                                        P_C004          => :FREF_NOM,',
'                                        P_C005          => :FREF_DIR,',
'                                        P_C006          => :FREF_TEL,',
'                                        P_C007          => :FREF_LUGAR_ORIGEN_REPLICA,',
'                                        P_C008          => :FREF_EMPR);',
'    end case;',
'end;'))
,p_attribute_05=>'Y'
,p_attribute_06=>'N'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'Aceptar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_exec_cond_for_each_row=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(7906264770784027014)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(7906261851701026984)
,p_process_type=>'NATIVE_IG_DML'
,p_process_name=>'actualizar_coleccion_cuotas'
,p_attribute_01=>'PLSQL_CODE'
,p_attribute_04=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    V_MON    VARCHAR2(5);',
'    l_count     NUMBER;',
'begin',
'    case :APEX$ROW_STATUS',
'    when ''C'' then',
'        IF to_number(:P8007_FSOL_MON) = 2 THEN',
'            V_MON := ''U$.'';',
'        ELSE ',
'            V_MON := ''GS.'';',
'        END IF;',
'        ',
'        l_count := APEX_COLLECTION.COLLECTION_MEMBER_COUNT(p_collection_name => ''FINI040_CUOTAS'');',
'    ',
'        APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => ''FINI040_CUOTAS'',',
'                                                 P_C001      => :P8007_FSOL_CLAVE,           --FSDC_CLAVE',
'                                                 P_C002      => :P_EMPRESA,                  --FSDC_EMPR',
'                                                 P_C003      => l_count + 1,',
'                                                 P_C004      => :FSDC_FEC_VENC,',
'                                                 P_C005      => :FSDC_SALDO_CAPITAL,',
'                                                 P_C006      => :FSDC_CAPITAL,',
'                                                 P_C007      => :FSDC_INTERES,',
'                                                 P_C008      => :S_IMP_CUO,',
'                                                 P_C009      => GEN_LUGAR_ORIGEN,             --FSDC_LUGAR_ORIGEN_REPLICA',
'                                                 P_C010      => V_MON);',
'    when ''U'' then',
'        APEX_COLLECTION.UPDATE_MEMBER(P_COLLECTION_NAME => ''FINI040_CUOTAS'',',
'                                        P_SEQ           => :NRO_ITEM,',
'                                        P_C001          => :FSDC_CLAVE,',
'                                        P_C002          => :FSDC_EMPR,',
'                                        P_C003          => :FSDC_ITEM,',
'                                        P_C004          => :FSDC_FEC_VENC,',
'                                        P_C005          => :FSDC_SALDO_CAPITAL,',
'                                        P_C006          => :FSDC_CAPITAL,',
'                                        P_C007          => :FSDC_INTERES,',
'                                        P_C008          => :S_IMP_CUO,',
'                                        P_C009          => :FSDC_LUGAR_ORIGEN_REPLICA,',
'                                        P_C010          => :S_MON_SIMB);',
'    end case;',
'end;'))
,p_attribute_05=>'Y'
,p_attribute_06=>'N'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'Aceptar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_exec_cond_for_each_row=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(7906885758266127082)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(7906803444366296393)
,p_process_type=>'NATIVE_IG_DML'
,p_process_name=>'actualizar_coleccion_desgloce'
,p_attribute_01=>'PLSQL_CODE'
,p_attribute_04=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'V_CLAVE    NUMBER;',
'V_LUGAR_ORIGEN    NUMBER;',
'begin',
'    ',
'    V_CLAVE := :P8007_FSOL_CLAVE;',
'    V_LUGAR_ORIGEN := :P8007_LUGAR_ORIGEN_REPLICA;',
'',
'    case :APEX$ROW_STATUS',
'    when ''U'' then',
'                ',
'        APEX_COLLECTION.UPDATE_MEMBER(P_COLLECTION_NAME => ''FINI040_DESGLOCE'',',
'                                        P_SEQ           => :FDDF_ITEM,',
'                                        P_C001          => :FDDF_DEST_FINAN,',
'                                        P_C002          => :S_DESGLOCE,',
'                                        P_C003          => :FDDF_CANT,',
'                                        P_C004          => :FDDF_PRECIO_MON,',
'                                        P_C005          => :S_TIPO_MON,',
'                                        P_C006          => :S_IMP_MENSUAL,',
'                                        P_C007          => V_CLAVE,  --FDDF_CLAVE_SOL',
'                                        P_C008          => :P_EMPRESA,         --FDDF_EMPR',
'                                        P_C009          => V_LUGAR_ORIGEN);  --FDDF_LUGAR_ORIGEN_REPLICA    ',
'                                        ',
'    end case;',
'end;'))
,p_attribute_05=>'Y'
,p_attribute_06=>'N'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'Aceptar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_exec_cond_for_each_row=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(7884488135172251923)
,p_process_sequence=>60
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PP_ACTUALIZAR_REGISTRO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    SALIR EXCEPTION;',
'	v_Aux_Fecha VARCHAR2(16);',
'	V_TEXTO_ERROR varchar2(500);',
'BEGIN',
'  	',
'	FINI040.PP_ESTABLECER_VAR (IO_FSOL_CLAVE          => :P8007_FSOL_CLAVE,',
'                               I_S_CLAVE_FICHA_TEC    => :P8007_S_CLAVE_FICHA_TEC,',
'                               I_S_CLAVE_COPIAR       => :P8007_S_CLAVE_COPIAR,',
'                               O_FSOL_EMPR            => :P8007_FSOL_EMPR,',
'                               O_FSOL_SUC             => :P8007_FSOL_SUC,',
'                               O_FSOL_LOGIN           => :P8007_FSOL_LOGIN,',
'                               O_FSOL_FEC_GRAB        => :P8007_FSOL_FEC_GRAB,',
'                               O_FSOL_CLAVE_FICH_TEC  => :P8007_FSOL_CLAVE_FICH_TEC);',
'--*',
'',
'		IF :P8007_FSOL_ESTADO =''A'' THEN',
'			IF nvl(TO_NUMBER(:P8007_FSOL_MONTO_ESTADO),0) <= 0 THEN',
'				RAISE_APPLICATION_ERROR(-20010,''El monto Aprobado debe ser mayor a 0!'');',
'			END IF;	',
'			IF nvl(TO_NUMBER(:P8007_FSOL_MONTO_ESTADO),0) > nvl(TO_NUMBER(:P8007_S_MONTO_CRED),0) THEN',
'				RAISE_APPLICATION_ERROR(-20010,''El monto Aprobado no puede ser mayor al Solicitado..!'');',
'			END IF;',
'			FINI040.PP_INSERTAR_CLIENTE( I_S_FICHA_MONEDA       => :P8007_S_FICHA_MONEDA,',
'										 I_FSOL_MONTO_ESTADO    => :P8007_FSOL_MONTO_ESTADO,',
'										 I_S_NOMBRE             => :P8007_S_NOMBRE, 	',
'										 I_S_DIRECCION          => :P8007_S_DIRECCION, 			',
'										 I_S_DOCUMENTO          => :P8007_S_DOCUMENTO, ',
'										 I_S_FEC_NAC            => TO_DATE(:P8007_S_FEC_NAC,''DD/MM/YYYY''),    					',
'										 I_S_TELEFONO           => :P8007_S_TELEFONO, 			',
'										 I_S_RUC_DV             => :P8007_I_S_RUC_DV,',
'										 I_S_DV                 => :P8007_S_DV,',
'										 I_FSOL_FEC_ESTADO      => TO_DATE(:P8007_S_FSOL_FEC_ESTADO,''DD/MM/YYYY''),',
'										 I_S_FICHA_PERSONA      => :P8007_S_FICHA_PERSONA,',
'										 I_S_COD_CLI     		=> :P8007_S_COD_CLI);',
'		END IF;	',
'	',
'',
'	/*IF (GET_BLOCK_PROPERTY(''BFOTO'',STATUS)) = ''CHANGED'' THEN',
'		PP_ASIGNAR_ITEM;',
'		PP_CONTROL_DET_ARCH;',
'	END IF;	*/',
'    ',
'    ',
'    IF :P8007_PP_HABILITAR <> ''N'' THEN',
'        ',
'        FINI040.GUARDAR_SOLICITUD (  I_FSOL_CLAVE					=> :P8007_FSOL_CLAVE,',
'                                     I_FSOL_NRO						=> :P8007_FSOL_NRO,',
'                                     I_FSOL_FEC_SOL					=> to_date(:P8007_S_FSOL_FEC_SOL,''dd/mm/yyyy''),',
'                                     I_FSOL_FEC_ASIG				=> to_date(:P8007_S_FSOL_FEC_ASIG,''dd/mm/yyyy''),',
'                                     I_FSOL_VENDEDOR				=> :P8007_FSOL_VENDEDOR,',
'                                     I_FSOL_OBS_SOL					=> :P8007_FSOL_OBS_SOL,',
'                                     I_FSOL_FORMA_CONOC_EMP			=> :P8007_FSOL_FORMA_CONOC_EMP,',
'                                     I_FSOL_IND_FEDERACION			=> :P8007_FSOL_IND_FEDERACION,',
'                                     I_FSOL_NOM_FEDERACION			=> :P8007_FSOL_NOM_FEDERACION,',
'                                     I_FSOL_GARANTE					=> :P8007_FSOL_GARANTE,',
'                                     I_FSOL_ANTIG_TERRENO			=> :P8007_FSOL_ANTIG_TERRENO,',
'                                     I_FSOL_ANTIG_TRAB				=> :P8007_FSOL_ANTIG_TRAB,',
'                                     I_FSOL_OBS_DIRE_PARC			=> :P8007_FSOL_OBS_DIRE_PARC,',
'                                     I_FSOL_CANT_HAS_PROD			=> :P8007_FSOL_CANT_HAS_PROD,',
'                                     I_FSOL_NRO_PRODUCTOR			=> :P8007_FSOL_NRO_PRODUCTOR,',
'                                     I_FSOL_NRO_COMITE				=> :P8007_FSOL_NRO_COMITE,',
'                                     I_FSOL_FEC_PLAZO_OTORG			=> to_date(:P8007_S_FSOL_FEC_PLAZO_OTORG,''dd/mm/yyyy''),',
'                                     I_FSOL_OBS_DEST_FINAN			=> :P8007_FSOL_OBS_DEST_FINAN,',
'                                     I_FSOL_IND_CRED_VGTE			=> :P8007_FSOL_IND_CRED_VGTE,',
'                                     I_FSOL_CRED_NOM_EMPR			=> :P8007_FSOL_CRED_NOM_EMPR,',
'                                     I_FSOL_DATO_INVER_CRED			=> :P8007_FSOL_DATO_INVER_CRED,',
'                                     I_FSOL_OBS_FPAG_FALLO_PROD	    => :P8007_FSOL_OBS_FPAG_FALLO_PROD,',
'                                     I_FSOL_OBS_PROPUESTA			=> :P8007_FSOL_OBS_PROPUESTA,',
'                                     I_FSOL_ESTADO					=> :P8007_FSOL_ESTADO,',
'                                     I_FSOL_FEC_ESTADO				=> to_date(:P8007_S_FSOL_FEC_ESTADO,''dd/mm/yyyy''),',
'                                     I_FSOL_MONTO_ESTADO			=> :P8007_FSOL_MONTO_ESTADO,',
'                                     I_FSOL_OBS_PLAZO_OTORG			=> NULL,',
'                                     I_FSOL_OBS_FPAG				=> NULL,',
'                                     I_FSOL_OBS_TASA_INT			=> NULL,',
'                                     I_FSOL_FEC_GRAB				=> SYSDATE,',
'                                     I_FSOL_CLAVE_FICH_TEC			=> :P8007_FSOL_CLAVE_FICH_TEC,',
'                                     I_S_FICHA_MONEDA				=> :P8007_S_FICHA_MONEDA,',
'                                     I_FSOL_TIP_VENC				=> :P8007_FSOL_TIP_VENC,',
'                                     I_FSOL_S_DIAS_ENTRE_CUOTAS		=> :P8007_FSOL_S_DIAS_ENTRE_CUOTAS,',
'                                     I_FSOL_INTERES					=> :P8007_FSOL_INTERES,',
'                                     I_FSOL_S_DIAS_GRACIA			=> :P8007_FSOL_S_DIAS_GRACIA,',
'                                     I_FSOL_FEC_PRIM_VTO			=> to_date(:P8007_S_FSOL_FEC_PRIM_VTO,''dd/mm/yyyy''),',
'                                     I_FSOL_TIP_GEN_CUO				=> :P8007_FSOL_TIP_GEN_CUO,',
'                                     I_FSOL_S_PLAZO					=> :P8007_FSOL_S_PLAZO,',
'                                     I_FSOL_S_MONTO_CUO				=> :P8007_FSOL_S_MONTO_CUO,',
'                                     I_FSOL_S_CANT_CUO				=> :P8007_FSOL_S_CANT_CUO,',
'                                     I_FSOL_FEC_CADUCA				=> to_date(:P8007_S_FSOL_FEC_CADUCA,''dd/mm/yyyy''),',
'                                     I_FSOL_OBS_RES_COM				=> :P8007_FSOL_OBS_RES_COM,',
'                                     I_LUGAR_ORIGEN_REPLICA			=> :P8007_LUGAR_ORIGEN_REPLICA);',
'                                     ',
'        FINI040.PP_ACTUALIZAR_ARCHIVOS;',
'        FINI040.GUARDAR_COLECCIONES;',
'         ',
'    END IF;',
'    ',
'	IF :P8007_S_CLAVE_FICHA_TEC IS NULL THEN ',
'		RAISE_APPLICATION_ERROR(-20010,''No se encuentra la clave de la ficha tecnica'');',
'	END IF;',
'	',
'EXCEPTION',
'  WHEN SALIR THEN',
'    NULL;',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'Aceptar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
);
end;
/
prompt --application/end_environment
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
