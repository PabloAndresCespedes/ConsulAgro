prompt --application/set_environment
set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_200100 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2020.03.31'
,p_release=>'20.1.0.00.13'
,p_default_workspace_id=>2933390818164287
,p_default_application_id=>100
,p_default_id_offset=>7656439127971453890
,p_default_owner=>'ADCS'
);
end;
/
 
prompt APPLICATION 100 - FINANZAS
--
-- Application Export:
--   Application:     100
--   Name:            FINANZAS
--   Date and Time:   13:32 Thursday September 1, 2022
--   Exported By:     ADMIN
--   Flashback:       0
--   Export Type:     Page Export
--   Manifest
--     PAGE: 8006
--   Manifest End
--   Version:         20.1.0.00.13
--   Instance ID:     218232033625678
--

begin
null;
end;
/
prompt --application/pages/delete_08006
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>8006);
end;
/
prompt --application/pages/page_08006
begin
wwv_flow_api.create_page(
 p_id=>8006
,p_user_interface_id=>wwv_flow_api.id(7567100396693658791)
,p_name=>'FINI034 - PRESTAMOS EMITIDOS INGRESO'
,p_alias=>'FINI034-PRESTAMOS-EMITIDOS-INGRESO'
,p_step_title=>'FINI034 - PRESTAMOS EMITIDOS INGRESO'
,p_autocomplete_on_off=>'OFF'
,p_inline_css=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#P8006_S_PLAZO_CONTAINER{',
'    margin-top: 50px;',
'}',
'',
'#P8006_LABEL{',
'    background: none;',
'    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen,Ubuntu,Cantarell,"Fira Sans","Droid Sans","Helvetica Neue",sans-serif;',
'    font-size : 1.2rem;',
'}',
'',
'#generar{',
'    margin-top: 20px;',
'    width: 80%;',
'    margin-right: 10%;',
'}',
'',
'#P8006_ANUAL{',
'    background: none;',
'    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen,Ubuntu,Cantarell,"Fira Sans","Droid Sans","Helvetica Neue",sans-serif;',
'    font-size : 1.2rem;',
'}',
'',
'#P8006_CADA{',
'    background: none;',
'    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen,Ubuntu,Cantarell,"Fira Sans","Droid Sans","Helvetica Neue",sans-serif;',
'    font-size : 1.2rem;',
'}',
'',
'#P8006_TOTALES{',
'    background: none;',
'    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen,Ubuntu,Cantarell,"Fira Sans","Droid Sans","Helvetica Neue",sans-serif;',
'    font-size : 1.2rem;',
'}',
'',
'#P8006_TITULO{',
'    background: none;',
'    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen,Ubuntu,Cantarell,"Fira Sans","Droid Sans","Helvetica Neue",sans-serif;',
'    width: 100%;',
'    font-size: 1.4rem;',
'    text-align : center;',
'}',
'',
'#P8006_S_ETIQUETA_DIAS{',
'    background: none;',
'    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen,Ubuntu,Cantarell,"Fira Sans","Droid Sans","Helvetica Neue",sans-serif;',
'    font-size : 1.2rem;',
'}',
'',
'',
'#P8006_MON_DESC,#P8006_TIP_GEN_CUO,#P8006_TIP_VENC,',
'#P8006_S_DIAS_ENTRE_CUOTAS,#P8006_INTERES,#P8006_S_FEC_PRIM_VTO,',
'#P8006_S_ACT_DESC,#P8006_BCO_DESC,#P8006_CTA_DESC,#P8006_S_TASA_OFIC,',
'#P8006_DOC_CLI_TEL,#P8006_DOC_CLI_RUC,#P8006_DOC_CLI_DIR,#P8006_DOC_CLI_NOM,',
'#P8006_NOMBRE_CODEUDOR,#P8006_DOC_NRO_CREDITO_PREST,',
'#P8006_S_MON_DESC2,#P8006_DOC_INTERES_MON_CUO,',
'#P8006_DOC_INTERES_MON,#P8006_TOTAL_MON,#P8006_TOTALES,',
'#P8006_S_MON_SIMBOLO2,#P8006_S_TOT_IMP_CAP,#P8006_S_TOT_IMP_INT,',
'#P8006_S_TOT_IMP_CUO,#P8006_DOC_CAPITAL_MON{	',
'	    font-weight: 800;',
'}'))
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'LRAMIREZ'
,p_last_upd_yyyymmddhh24miss=>'20220125081759'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7789816991947819805)
,p_plug_name=>'PRESTAMOS EMITIDOS INGRESO'
,p_region_template_options=>'#DEFAULT#:t-Region--accent2:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7789816619131819801)
,p_plug_name=>'Panel izquierdo'
,p_parent_plug_id=>wwv_flow_api.id(7789816991947819805)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>20
,p_plug_grid_column_span=>6
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7789816767331819803)
,p_plug_name=>'datos credito'
,p_parent_plug_id=>wwv_flow_api.id(7789816619131819801)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--scrollBody:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7789818854759819823)
,p_plug_name=>'subregion'
,p_parent_plug_id=>wwv_flow_api.id(7789816767331819803)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7789816934467819804)
,p_plug_name=>'generacion cuotas'
,p_parent_plug_id=>wwv_flow_api.id(7789816619131819801)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>30
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7789819156531819826)
,p_plug_name=>'sub_izq'
,p_parent_plug_id=>wwv_flow_api.id(7789816934467819804)
,p_plug_display_sequence=>10
,p_plug_grid_column_span=>6
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7797044594144101680)
,p_plug_name=>'Tipo Vencimiento'
,p_parent_plug_id=>wwv_flow_api.id(7789819156531819826)
,p_region_template_options=>'#DEFAULT#:t-Region--accent2:t-Region--scrollBody:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7797045653743101690)
,p_plug_name=>'items'
,p_parent_plug_id=>wwv_flow_api.id(7789819156531819826)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7797044531089101679)
,p_plug_name=>'sub_der'
,p_parent_plug_id=>wwv_flow_api.id(7789816934467819804)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_grid_column_span=>6
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7797044898330101683)
,p_plug_name=>unistr('Tipo Generaci\00F3n de Cuotas')
,p_parent_plug_id=>wwv_flow_api.id(7797044531089101679)
,p_region_template_options=>'#DEFAULT#:t-Region--accent2:t-Region--scrollBody:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(8111619448247837966)
,p_plug_name=>'Cabecera'
,p_parent_plug_id=>wwv_flow_api.id(7789816619131819801)
,p_region_template_options=>'#DEFAULT#:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7789816722593819802)
,p_plug_name=>'panel derecho'
,p_parent_plug_id=>wwv_flow_api.id(7789816991947819805)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>40
,p_plug_new_grid_row=>false
,p_plug_grid_column_span=>6
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7797046598452101700)
,p_plug_name=>unistr('Distribuci\00F3n de Cuotas')
,p_parent_plug_id=>wwv_flow_api.id(7789816722593819802)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570304732549401240)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  SEQ_ID    NRO_ITEM,',
'          D001    FEC_VTO,',
'          C001    S_CUO_FEC_VTO,',
'          C002    S_SALDO_CAPITAL,',
'          C003    S_IMP_INT,',
'          C004    S_IMP_CUO,',
'          C005    S_IMP_AMORT,',
'          C006    S_IMP_CAP,',
'          C007    S_MON_SIMB',
'FROM APEX_COLLECTIONS',
'WHERE COLLECTION_NAME = ''FINI034_CUOTAS'';',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'MILLIMETERS'
,p_prn_paper_size=>'A4'
,p_prn_width=>297
,p_prn_height=>210
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header=>unistr('Distribuci\00F3n de Cuotas')
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#EEEEEE'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'bold'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#FFFFFF'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_prn_border_color=>'#666666'
,p_plug_header=>'<div style="overflow: auto; height: 450px;">'
,p_plug_footer=>'</div>'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(7797046835197101702)
,p_max_row_count=>'1000000'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_show_search_bar=>'N'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_owner=>'ADIAZ'
,p_internal_uid=>346209978436121124
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7800977091135239518)
,p_db_column_name=>'NRO_ITEM'
,p_display_order=>10
,p_column_identifier=>'AN'
,p_column_label=>'&nbsp'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7800977198160239519)
,p_db_column_name=>'FEC_VTO'
,p_display_order=>20
,p_column_identifier=>'AO'
,p_column_label=>'FEC_VTO'
,p_column_type=>'DATE'
,p_display_text_as=>'HIDDEN'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7800977286374239520)
,p_db_column_name=>'S_CUO_FEC_VTO'
,p_display_order=>30
,p_column_identifier=>'AP'
,p_column_label=>'Vencimiento'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7800977437837239521)
,p_db_column_name=>'S_SALDO_CAPITAL'
,p_display_order=>40
,p_column_identifier=>'AQ'
,p_column_label=>'Saldo Capital'
,p_column_type=>'STRING'
,p_column_alignment=>'RIGHT'
,p_format_mask=>'999G999G999G999G999G999G990'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7800977530333239522)
,p_db_column_name=>'S_IMP_INT'
,p_display_order=>50
,p_column_identifier=>'AR'
,p_column_label=>unistr('Inter\00E9s')
,p_column_type=>'STRING'
,p_column_alignment=>'RIGHT'
,p_format_mask=>'999G999G999G999G999G999G990'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7800977567231239523)
,p_db_column_name=>'S_IMP_CUO'
,p_display_order=>60
,p_column_identifier=>'AS'
,p_column_label=>'Importe'
,p_column_type=>'STRING'
,p_column_alignment=>'RIGHT'
,p_format_mask=>'999G999G999G999G999G999G990'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7800977727468239524)
,p_db_column_name=>'S_IMP_AMORT'
,p_display_order=>70
,p_column_identifier=>'AT'
,p_column_label=>'S Imp Amort'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7800977814324239525)
,p_db_column_name=>'S_IMP_CAP'
,p_display_order=>80
,p_column_identifier=>'AU'
,p_column_label=>'Capital'
,p_column_type=>'STRING'
,p_column_alignment=>'RIGHT'
,p_format_mask=>'999G999G999G999G999G999G990'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7800977924247239526)
,p_db_column_name=>'S_MON_SIMB'
,p_display_order=>90
,p_column_identifier=>'AV'
,p_column_label=>'&nbsp'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(7797171582567522379)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'3463348'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'NRO_ITEM:S_CUO_FEC_VTO:S_SALDO_CAPITAL:S_IMP_CAP:S_IMP_INT:S_IMP_CUO:S_MON_SIMB:'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7797046756156101701)
,p_plug_name=>'botonera'
,p_parent_plug_id=>wwv_flow_api.id(7789816722593819802)
,p_region_template_options=>'#DEFAULT#'
,p_region_attributes=>'style="margin-top:10px;"'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>30
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7797047411844101708)
,p_plug_name=>'Totales'
,p_parent_plug_id=>wwv_flow_api.id(7789816722593819802)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7798932423110678580)
,p_plug_name=>'PARAMETROS'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7798933024752678586)
,p_plug_name=>'CONFIGURACION'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>50
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7830260364754414303)
,p_plug_name=>'CODEUDORES'
,p_region_template_options=>'#DEFAULT#:js-dialog-size600x400'
,p_plug_template=>wwv_flow_api.id(7570302609627401239)
,p_plug_display_sequence=>60
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7830260517564414304)
,p_plug_name=>'consulta_codeudores'
,p_parent_plug_id=>wwv_flow_api.id(7830260364754414303)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570304732549401240)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT * ',
'    FROM FIN_CLIENTE ',
'   WHERE CLI_PERSONA = :P8006_P_GARANTE ',
'     AND CLI_EMPR    = :P_EMPRESA ',
'ORDER BY CLI_CODIGO'))
,p_plug_source_type=>'NATIVE_IR'
,p_ajax_items_to_submit=>'P8006_P_GARANTE'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'MILLIMETERS'
,p_prn_paper_size=>'A4'
,p_prn_width=>297
,p_prn_height=>210
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header=>'consulta_codeudores'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#EEEEEE'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'bold'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#FFFFFF'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_prn_border_color=>'#666666'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(7830260581693414305)
,p_max_row_count=>'1000000'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_show_search_bar=>'N'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_owner=>'ADIAZ'
,p_internal_uid=>379423724932433727
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830260672223414306)
,p_db_column_name=>'CLI_CODIGO'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Cliente'
,p_column_type=>'NUMBER'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830260772247414307)
,p_db_column_name=>'CLI_NOM'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Nombre'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830260930367414308)
,p_db_column_name=>'CLI_PROPIETARIO'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Cli Propietario'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830261055735414309)
,p_db_column_name=>'CLI_NOM_FANTASIA'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Cli Nom Fantasia'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830261107624414310)
,p_db_column_name=>'CLI_DIR'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Cli Dir'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830261221885414311)
,p_db_column_name=>'CLI_DOC_IDENT_PROPIETARIO'
,p_display_order=>60
,p_column_identifier=>'F'
,p_column_label=>'Cli Doc Ident Propietario'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830261351742414312)
,p_db_column_name=>'CLI_LOCALIDAD'
,p_display_order=>70
,p_column_identifier=>'G'
,p_column_label=>'Cli Localidad'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830261421594414313)
,p_db_column_name=>'CLI_FEC_NAC_PROPIETARIO'
,p_display_order=>80
,p_column_identifier=>'H'
,p_column_label=>'Cli Fec Nac Propietario'
,p_column_type=>'DATE'
,p_display_text_as=>'HIDDEN'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830261522820414314)
,p_db_column_name=>'CLI_TEL'
,p_display_order=>90
,p_column_identifier=>'I'
,p_column_label=>'Cli Tel'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830261581020414315)
,p_db_column_name=>'CLI_FAX'
,p_display_order=>100
,p_column_identifier=>'J'
,p_column_label=>'Cli Fax'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830261698357414316)
,p_db_column_name=>'CLI_RUC'
,p_display_order=>110
,p_column_identifier=>'K'
,p_column_label=>'Cli Ruc'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830261848757414317)
,p_db_column_name=>'CLI_RAMO'
,p_display_order=>120
,p_column_identifier=>'L'
,p_column_label=>'Cli Ramo'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830261896731414318)
,p_db_column_name=>'CLI_PERSONERIA'
,p_display_order=>130
,p_column_identifier=>'M'
,p_column_label=>'Cli Personeria'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830261967625414319)
,p_db_column_name=>'CLI_CATEG'
,p_display_order=>140
,p_column_identifier=>'N'
,p_column_label=>'Cli Categ'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830262085763414320)
,p_db_column_name=>'CLI_ZONA'
,p_display_order=>150
,p_column_identifier=>'O'
,p_column_label=>'Cli Zona'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830262206640414321)
,p_db_column_name=>'CLI_PAIS'
,p_display_order=>160
,p_column_identifier=>'P'
,p_column_label=>'Cli Pais'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830262272708414322)
,p_db_column_name=>'CLI_FEC_ANIV'
,p_display_order=>170
,p_column_identifier=>'Q'
,p_column_label=>'Cli Fec Aniv'
,p_column_type=>'DATE'
,p_display_text_as=>'HIDDEN'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830262414452414323)
,p_db_column_name=>'CLI_PERS_CONTACTO'
,p_display_order=>180
,p_column_identifier=>'R'
,p_column_label=>'Cli Pers Contacto'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830262545042414324)
,p_db_column_name=>'CLI_FORMA_ENVIO'
,p_display_order=>190
,p_column_identifier=>'S'
,p_column_label=>'Cli Forma Envio'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830262607781414325)
,p_db_column_name=>'CLI_OBS'
,p_display_order=>200
,p_column_identifier=>'T'
,p_column_label=>'Cli Obs'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830262750416414326)
,p_db_column_name=>'CLI_PORC_EXEN_IVA'
,p_display_order=>210
,p_column_identifier=>'U'
,p_column_label=>'Cli Porc Exen Iva'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830262833132414327)
,p_db_column_name=>'CLI_DOC_IDENT_CONTACTO'
,p_display_order=>220
,p_column_identifier=>'V'
,p_column_label=>'Cli Doc Ident Contacto'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7830262857201414328)
,p_db_column_name=>'CLI_EST_CLI'
,p_display_order=>230
,p_column_identifier=>'W'
,p_column_label=>'Cli Est Cli'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832095057737899579)
,p_db_column_name=>'CLI_FEC_NAC_CONTACTO'
,p_display_order=>240
,p_column_identifier=>'X'
,p_column_label=>'Cli Fec Nac Contacto'
,p_column_type=>'DATE'
,p_display_text_as=>'HIDDEN'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832095181472899580)
,p_db_column_name=>'CLI_MON'
,p_display_order=>250
,p_column_identifier=>'Y'
,p_column_label=>'Cli Mon'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832095259686899581)
,p_db_column_name=>'CLI_IMP_LIM_CR'
,p_display_order=>260
,p_column_identifier=>'Z'
,p_column_label=>'Cli Imp Lim Cr'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832095401260899582)
,p_db_column_name=>'CLI_BLOQ_LIM_CR'
,p_display_order=>270
,p_column_identifier=>'AA'
,p_column_label=>'Cli Bloq Lim Cr'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832095470049899583)
,p_db_column_name=>'CLI_MAX_DIAS_ATRASO'
,p_display_order=>280
,p_column_identifier=>'AB'
,p_column_label=>'Cli Max Dias Atraso'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832095608517899584)
,p_db_column_name=>'CLI_PERS_REPRESENTANTE'
,p_display_order=>290
,p_column_identifier=>'AC'
,p_column_label=>'Cli Pers Representante'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832095754138899585)
,p_db_column_name=>'CLI_FEC_INGRESO'
,p_display_order=>300
,p_column_identifier=>'AD'
,p_column_label=>'Cli Fec Ingreso'
,p_column_type=>'DATE'
,p_display_text_as=>'HIDDEN'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832095838418899586)
,p_db_column_name=>'CLI_FEC_ACTUALIZACION'
,p_display_order=>310
,p_column_identifier=>'AE'
,p_column_label=>'Cli Fec Actualizacion'
,p_column_type=>'DATE'
,p_display_text_as=>'HIDDEN'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832095926868899587)
,p_db_column_name=>'CLI_IND_POTENCIAL'
,p_display_order=>320
,p_column_identifier=>'AF'
,p_column_label=>'Cli Ind Potencial'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832096037011899588)
,p_db_column_name=>'CLI_DOC_IDENT_REPRESENTANTE'
,p_display_order=>330
,p_column_identifier=>'AG'
,p_column_label=>'Cli Doc Ident Representante'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832096069692899589)
,p_db_column_name=>'CLI_FEC_NAC_REPRESENTANTE'
,p_display_order=>340
,p_column_identifier=>'AH'
,p_column_label=>'Cli Fec Nac Representante'
,p_column_type=>'DATE'
,p_display_text_as=>'HIDDEN'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832096204396899590)
,p_db_column_name=>'CLI_LUGAR_ORIGEN_REPLICA'
,p_display_order=>350
,p_column_identifier=>'AI'
,p_column_label=>'Cli Lugar Origen Replica'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832096289810899591)
,p_db_column_name=>'CLI_NRO_FINCA'
,p_display_order=>360
,p_column_identifier=>'AJ'
,p_column_label=>'Cli Nro Finca'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832096434581899592)
,p_db_column_name=>'CLI_NOM_CONYUGUE'
,p_display_order=>370
,p_column_identifier=>'AK'
,p_column_label=>'Cli Nom Conyugue'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832096511539899593)
,p_db_column_name=>'CLI_DOC_IDENT_CONYUGUE'
,p_display_order=>380
,p_column_identifier=>'AL'
,p_column_label=>'Cli Doc Ident Conyugue'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832096559892899594)
,p_db_column_name=>'CLI_DIR_PARTICULAR'
,p_display_order=>390
,p_column_identifier=>'AM'
,p_column_label=>'Cli Dir Particular'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832096688157899595)
,p_db_column_name=>'CLI_LUGAR_TRABAJO'
,p_display_order=>400
,p_column_identifier=>'AN'
,p_column_label=>'Cli Lugar Trabajo'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832096784235899596)
,p_db_column_name=>'CLI_TEL_PARTICULAR'
,p_display_order=>410
,p_column_identifier=>'AO'
,p_column_label=>'Cli Tel Particular'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832096939805899597)
,p_db_column_name=>'CLI_BARRIO'
,p_display_order=>420
,p_column_identifier=>'AP'
,p_column_label=>'Cli Barrio'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832097046606899598)
,p_db_column_name=>'CLI_PROFESION'
,p_display_order=>430
,p_column_identifier=>'AQ'
,p_column_label=>'Cli Profesion'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832097114098899599)
,p_db_column_name=>'CLI_EMAIL'
,p_display_order=>440
,p_column_identifier=>'AR'
,p_column_label=>'Cli Email'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832097198394899600)
,p_db_column_name=>'CLI_NRO_FICHA'
,p_display_order=>450
,p_column_identifier=>'AS'
,p_column_label=>'Cli Nro Ficha'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832097312340899601)
,p_db_column_name=>'CLI_PROYECTO'
,p_display_order=>460
,p_column_identifier=>'AT'
,p_column_label=>'Cli Proyecto'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832097413597899602)
,p_db_column_name=>'CLI_SEXO'
,p_display_order=>470
,p_column_identifier=>'AU'
,p_column_label=>'Cli Sexo'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832097498539899603)
,p_db_column_name=>'CLI_TEL_CONTACTO'
,p_display_order=>480
,p_column_identifier=>'AV'
,p_column_label=>'Cli Tel Contacto'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832097648898899604)
,p_db_column_name=>'CLI_ESTADO_CIVIL'
,p_display_order=>490
,p_column_identifier=>'AW'
,p_column_label=>'Cli Estado Civil'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832097676780899605)
,p_db_column_name=>'CLI_SALARIO'
,p_display_order=>500
,p_column_identifier=>'AX'
,p_column_label=>'Cli Salario'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832097771399899606)
,p_db_column_name=>'CLI_FEC_NAC_CONYUGUE'
,p_display_order=>510
,p_column_identifier=>'AY'
,p_column_label=>'Cli Fec Nac Conyugue'
,p_column_type=>'DATE'
,p_display_text_as=>'HIDDEN'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832097899860899607)
,p_db_column_name=>'CLI_DIADOM'
,p_display_order=>520
,p_column_identifier=>'AZ'
,p_column_label=>'Cli Diadom'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832098054449899608)
,p_db_column_name=>'CLI_DIALUN'
,p_display_order=>530
,p_column_identifier=>'BA'
,p_column_label=>'Cli Dialun'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832098098140899609)
,p_db_column_name=>'CLI_DIAMAR'
,p_display_order=>540
,p_column_identifier=>'BB'
,p_column_label=>'Cli Diamar'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832098224987899610)
,p_db_column_name=>'CLI_DIAMIE'
,p_display_order=>550
,p_column_identifier=>'BC'
,p_column_label=>'Cli Diamie'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832098329139899611)
,p_db_column_name=>'CLI_DIAJUE'
,p_display_order=>560
,p_column_identifier=>'BD'
,p_column_label=>'Cli Diajue'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832098418036899612)
,p_db_column_name=>'CLI_DIAVIE'
,p_display_order=>570
,p_column_identifier=>'BE'
,p_column_label=>'Cli Diavie'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832098547189899613)
,p_db_column_name=>'CLI_DIASAB'
,p_display_order=>580
,p_column_identifier=>'BF'
,p_column_label=>'Cli Diasab'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
end;
/
begin
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832098608766899614)
,p_db_column_name=>'CLI_PEDIDO_REPETITIVO'
,p_display_order=>590
,p_column_identifier=>'BG'
,p_column_label=>'Cli Pedido Repetitivo'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832098715890899615)
,p_db_column_name=>'CLI_REC_LPREC'
,p_display_order=>600
,p_column_identifier=>'BH'
,p_column_label=>'Cli Rec Lprec'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832098827637899616)
,p_db_column_name=>'CLI_DV'
,p_display_order=>610
,p_column_identifier=>'BI'
,p_column_label=>'Cli Dv'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832098876245899617)
,p_db_column_name=>'CLI_RUC_DV'
,p_display_order=>620
,p_column_identifier=>'BJ'
,p_column_label=>'Cli Ruc Dv'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832099011492899618)
,p_db_column_name=>'CLI_CIUDAD'
,p_display_order=>630
,p_column_identifier=>'BK'
,p_column_label=>'Cli Ciudad'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832099123727899619)
,p_db_column_name=>'CLI_DIA_LUNES'
,p_display_order=>640
,p_column_identifier=>'BL'
,p_column_label=>'Cli Dia Lunes'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832099177284899620)
,p_db_column_name=>'CLI_DIA_MARTES'
,p_display_order=>650
,p_column_identifier=>'BM'
,p_column_label=>'Cli Dia Martes'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832099325936899621)
,p_db_column_name=>'CLI_DIA_MIERCOLES'
,p_display_order=>660
,p_column_identifier=>'BN'
,p_column_label=>'Cli Dia Miercoles'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832099408502899622)
,p_db_column_name=>'CLI_DIA_JUEVES'
,p_display_order=>670
,p_column_identifier=>'BO'
,p_column_label=>'Cli Dia Jueves'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832099467164899623)
,p_db_column_name=>'CLI_DIA_VIERNES'
,p_display_order=>680
,p_column_identifier=>'BP'
,p_column_label=>'Cli Dia Viernes'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832099622806899624)
,p_db_column_name=>'CLI_DIA_SABADO'
,p_display_order=>690
,p_column_identifier=>'BQ'
,p_column_label=>'Cli Dia Sabado'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832099687122899625)
,p_db_column_name=>'CLI_VENDEDOR'
,p_display_order=>700
,p_column_identifier=>'BR'
,p_column_label=>'Cli Vendedor'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832099800256899626)
,p_db_column_name=>'CLI_ORDEN_LUN'
,p_display_order=>710
,p_column_identifier=>'BS'
,p_column_label=>'Cli Orden Lun'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832099912270899627)
,p_db_column_name=>'CLI_ORDEN_MAR'
,p_display_order=>720
,p_column_identifier=>'BT'
,p_column_label=>'Cli Orden Mar'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832100001806899628)
,p_db_column_name=>'CLI_ORDEN_MIER'
,p_display_order=>730
,p_column_identifier=>'BU'
,p_column_label=>'Cli Orden Mier'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832100103085899579)
,p_db_column_name=>'CLI_ORDEN_JUE'
,p_display_order=>740
,p_column_identifier=>'BV'
,p_column_label=>'Cli Orden Jue'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832100224833899580)
,p_db_column_name=>'CLI_ORDEN_VIER'
,p_display_order=>750
,p_column_identifier=>'BW'
,p_column_label=>'Cli Orden Vier'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832100292140899581)
,p_db_column_name=>'CLI_ORDEN_SAB'
,p_display_order=>760
,p_column_identifier=>'BX'
,p_column_label=>'Cli Orden Sab'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832100456600899582)
,p_db_column_name=>'CLI_COD_BARRIO'
,p_display_order=>770
,p_column_identifier=>'BY'
,p_column_label=>'Cli Cod Barrio'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832100523485899583)
,p_db_column_name=>'CLI_REPOSITOR'
,p_display_order=>780
,p_column_identifier=>'BZ'
,p_column_label=>'Cli Repositor'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832100558138899584)
,p_db_column_name=>'CLI_IND_PAGO_CON_CHEQUE'
,p_display_order=>790
,p_column_identifier=>'CA'
,p_column_label=>'Cli Ind Pago Con Cheque'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832100676550899585)
,p_db_column_name=>'CLI_MAX_CANT_CUOTA_CONT'
,p_display_order=>800
,p_column_identifier=>'CB'
,p_column_label=>'Cli Max Cant Cuota Cont'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832100815950899586)
,p_db_column_name=>'CLI_COD_PROVEEDOR'
,p_display_order=>810
,p_column_identifier=>'CC'
,p_column_label=>'Cli Cod Proveedor'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832100859212899587)
,p_db_column_name=>'CLI_COMI_ASO'
,p_display_order=>820
,p_column_identifier=>'CD'
,p_column_label=>'Cli Comi Aso'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832100982666899588)
,p_db_column_name=>'CLI_APELLIDO'
,p_display_order=>830
,p_column_identifier=>'CE'
,p_column_label=>'Cli Apellido'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832101100582899589)
,p_db_column_name=>'CLI_NOMBRE'
,p_display_order=>840
,p_column_identifier=>'CF'
,p_column_label=>'Cli Nombre'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832101162812899590)
,p_db_column_name=>'CLI_DIR_ESPECIFICA'
,p_display_order=>850
,p_column_identifier=>'CG'
,p_column_label=>'Cli Dir Especifica'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832101349859899591)
,p_db_column_name=>'CLI_ASOCIACION'
,p_display_order=>860
,p_column_identifier=>'CH'
,p_column_label=>'Cli Asociacion'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832101410196899592)
,p_db_column_name=>'CLI_DIR_FOTO'
,p_display_order=>870
,p_column_identifier=>'CI'
,p_column_label=>'Cli Dir Foto'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832101486155899593)
,p_db_column_name=>'CLI_INFORMCONF'
,p_display_order=>880
,p_column_identifier=>'CJ'
,p_column_label=>'Cli Informconf'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832101628813899594)
,p_db_column_name=>'CLI_FEC_INGRESO_CLI'
,p_display_order=>890
,p_column_identifier=>'CK'
,p_column_label=>'Cli Fec Ingreso Cli'
,p_column_type=>'DATE'
,p_display_text_as=>'HIDDEN'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832101689395899595)
,p_db_column_name=>'CLI_SECTOR_PROD'
,p_display_order=>900
,p_column_identifier=>'CL'
,p_column_label=>'Cli Sector Prod'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832101796179899596)
,p_db_column_name=>'CLI_PERSONA'
,p_display_order=>910
,p_column_identifier=>'CM'
,p_column_label=>'Cli Persona'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832101899598899597)
,p_db_column_name=>'CLI_CANAL'
,p_display_order=>920
,p_column_identifier=>'CN'
,p_column_label=>'Cli Canal'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832101960242899598)
,p_db_column_name=>'CLI_IND_EXHIBIDOR'
,p_display_order=>930
,p_column_identifier=>'CO'
,p_column_label=>'Cli Ind Exhibidor'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832102152471899599)
,p_db_column_name=>'CLI_IND_MOD_CANAL'
,p_display_order=>940
,p_column_identifier=>'CP'
,p_column_label=>'Cli Ind Mod Canal'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832102177303899600)
,p_db_column_name=>'CLI_OBS_FACT'
,p_display_order=>950
,p_column_identifier=>'CQ'
,p_column_label=>'Cli Obs Fact'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832102340096899601)
,p_db_column_name=>'CLI_SUCURSAL'
,p_display_order=>960
,p_column_identifier=>'CR'
,p_column_label=>'Cli Sucursal'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832102423848899602)
,p_db_column_name=>'CLI_COBRADOR'
,p_display_order=>970
,p_column_identifier=>'CS'
,p_column_label=>'Cli Cobrador'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832102526681899603)
,p_db_column_name=>'CLI_COD_PROSPECTO'
,p_display_order=>980
,p_column_identifier=>'CT'
,p_column_label=>'Cli Cod Prospecto'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832102637957899604)
,p_db_column_name=>'CLI_IND_SILO'
,p_display_order=>990
,p_column_identifier=>'CU'
,p_column_label=>'Cli Ind Silo'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832102681638899605)
,p_db_column_name=>'CLI_COD_FICHA_HOLDING'
,p_display_order=>1000
,p_column_identifier=>'CV'
,p_column_label=>'Cli Cod Ficha Holding'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832102843141899606)
,p_db_column_name=>'CLI_CHORTI_DEB_AUTO'
,p_display_order=>1010
,p_column_identifier=>'CW'
,p_column_label=>'Cli Chorti Deb Auto'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832102951587899607)
,p_db_column_name=>'CLI_CHORTI_NRO_CTA'
,p_display_order=>1020
,p_column_identifier=>'CX'
,p_column_label=>'Cli Chorti Nro Cta'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832102980958899608)
,p_db_column_name=>'CLI_EXP_DIR'
,p_display_order=>1030
,p_column_identifier=>'CY'
,p_column_label=>'Cli Exp Dir'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832103059699899609)
,p_db_column_name=>'CLI_EXP_RUC'
,p_display_order=>1040
,p_column_identifier=>'CZ'
,p_column_label=>'Cli Exp Ruc'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832103190734899610)
,p_db_column_name=>'CLI_IND_TACHO'
,p_display_order=>1050
,p_column_identifier=>'DA'
,p_column_label=>'Cli Ind Tacho'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832103352470899611)
,p_db_column_name=>'CLI_TEL_SMS'
,p_display_order=>1060
,p_column_identifier=>'DB'
,p_column_label=>'Cli Tel Sms'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832103388082899612)
,p_db_column_name=>'CLI_FREC_VISITA_SEMANAL'
,p_display_order=>1070
,p_column_identifier=>'DC'
,p_column_label=>'Cli Frec Visita Semanal'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832103529117899613)
,p_db_column_name=>'CLI_FREC_VISITA_QUINCENAL'
,p_display_order=>1080
,p_column_identifier=>'DD'
,p_column_label=>'Cli Frec Visita Quincenal'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832103567639899614)
,p_db_column_name=>'CLI_LATITUD'
,p_display_order=>1090
,p_column_identifier=>'DE'
,p_column_label=>'Cli Latitud'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832103696215899615)
,p_db_column_name=>'CLI_LONGITUD'
,p_display_order=>1100
,p_column_identifier=>'DF'
,p_column_label=>'Cli Longitud'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832103841354899616)
,p_db_column_name=>'CLI_SUP_COBRANZA'
,p_display_order=>1110
,p_column_identifier=>'DG'
,p_column_label=>'Cli Sup Cobranza'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832103863658899617)
,p_db_column_name=>'CLI_RUC_HOLDING'
,p_display_order=>1120
,p_column_identifier=>'DH'
,p_column_label=>'Cli Ruc Holding'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832104042505899618)
,p_db_column_name=>'CLI_EMPR'
,p_display_order=>1130
,p_column_identifier=>'DI'
,p_column_label=>'Cli Empr'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832104123541899619)
,p_db_column_name=>'CLI_RESUMEN_FACT'
,p_display_order=>1140
,p_column_identifier=>'DJ'
,p_column_label=>'Cli Resumen Fact'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832104221320899620)
,p_db_column_name=>'CLI_ACA_COL_ORIG'
,p_display_order=>1150
,p_column_identifier=>'DK'
,p_column_label=>'Cli Aca Col Orig'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832104301101899621)
,p_db_column_name=>'CLI_HOLDING'
,p_display_order=>1160
,p_column_identifier=>'DL'
,p_column_label=>'Cli Holding'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832104424905899622)
,p_db_column_name=>'CLI_ACA_DIR_LAB'
,p_display_order=>1170
,p_column_identifier=>'DM'
,p_column_label=>'Cli Aca Dir Lab'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832104537025899623)
,p_db_column_name=>'CLI_ACA_TEL_LAB'
,p_display_order=>1180
,p_column_identifier=>'DN'
,p_column_label=>'Cli Aca Tel Lab'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832104613228899624)
,p_db_column_name=>'CLI_ACA_NOM_FAC'
,p_display_order=>1190
,p_column_identifier=>'DO'
,p_column_label=>'Cli Aca Nom Fac'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832104699357899625)
,p_db_column_name=>'CLI_ACA_DIR_FAC'
,p_display_order=>1200
,p_column_identifier=>'DP'
,p_column_label=>'Cli Aca Dir Fac'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832104815171899626)
,p_db_column_name=>'CLI_ACA_CIP_FAC'
,p_display_order=>1210
,p_column_identifier=>'DQ'
,p_column_label=>'Cli Aca Cip Fac'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832104866416899627)
,p_db_column_name=>'CLI_ACA_RUC_FAC'
,p_display_order=>1220
,p_column_identifier=>'DR'
,p_column_label=>'Cli Aca Ruc Fac'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832105047690899628)
,p_db_column_name=>'CLI_ACA_NIVEL'
,p_display_order=>1230
,p_column_identifier=>'DS'
,p_column_label=>'Cli Aca Nivel'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832105139473899579)
,p_db_column_name=>'CLI_ACA_GRADO'
,p_display_order=>1240
,p_column_identifier=>'DT'
,p_column_label=>'Cli Aca Grado'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832105195952899580)
,p_db_column_name=>'CLI_ACA_TURNO'
,p_display_order=>1250
,p_column_identifier=>'DU'
,p_column_label=>'Cli Aca Turno'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832105259096899581)
,p_db_column_name=>'CLI_ACA_SECCION'
,p_display_order=>1260
,p_column_identifier=>'DV'
,p_column_label=>'Cli Aca Seccion'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832105404062899582)
,p_db_column_name=>'CLI_ACA_APLAZADO'
,p_display_order=>1270
,p_column_identifier=>'DW'
,p_column_label=>'Cli Aca Aplazado'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832105532125899583)
,p_db_column_name=>'CLI_ACA_CLI_TIPO'
,p_display_order=>1280
,p_column_identifier=>'DX'
,p_column_label=>'Cli Aca Cli Tipo'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832105628587899584)
,p_db_column_name=>'CLI_ACA_INACTIVO'
,p_display_order=>1290
,p_column_identifier=>'DY'
,p_column_label=>'Cli Aca Inactivo'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832105668932899585)
,p_db_column_name=>'CLI_ACA_IMG_BLOB'
,p_display_order=>1300
,p_column_identifier=>'DZ'
,p_column_label=>'Cli Aca Img Blob'
,p_column_type=>'OTHER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832105834192899586)
,p_db_column_name=>'CLI_ACA_IMG_LR'
,p_display_order=>1310
,p_column_identifier=>'EA'
,p_column_label=>'Cli Aca Img Lr'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832105912897899587)
,p_db_column_name=>'CLI_COND_PAGO'
,p_display_order=>1320
,p_column_identifier=>'EB'
,p_column_label=>'Cli Cond Pago'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832105974629899588)
,p_db_column_name=>'CLI_FEC_ENTR_EXHIBIDOR'
,p_display_order=>1330
,p_column_identifier=>'EC'
,p_column_label=>'Cli Fec Entr Exhibidor'
,p_column_type=>'DATE'
,p_display_text_as=>'HIDDEN'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832106110320899589)
,p_db_column_name=>'CLI_COD_EMPL_EMPR_ORIG'
,p_display_order=>1340
,p_column_identifier=>'ED'
,p_column_label=>'Cli Cod Empl Empr Orig'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832106254559899590)
,p_db_column_name=>'CLI_CODIGO_ANTERIOR'
,p_display_order=>1350
,p_column_identifier=>'EE'
,p_column_label=>'Cli Codigo Anterior'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832106261462899591)
,p_db_column_name=>'CLI_CTRL_PRECIO_AQ'
,p_display_order=>1360
,p_column_identifier=>'EF'
,p_column_label=>'Cli Ctrl Precio Aq'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832106393745899592)
,p_db_column_name=>'CLI_FACT_COMB_GS'
,p_display_order=>1370
,p_column_identifier=>'EG'
,p_column_label=>'Cli Fact Comb Gs'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832106471256899593)
,p_db_column_name=>'CLI_IND_EXIGIR_RESP_FCRED'
,p_display_order=>1380
,p_column_identifier=>'EH'
,p_column_label=>'Cli Ind Exigir Resp Fcred'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832106602154899594)
,p_db_column_name=>'CLI_PORC_FLETE'
,p_display_order=>1390
,p_column_identifier=>'EI'
,p_column_label=>'Cli Porc Flete'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832106716327899595)
,p_db_column_name=>'CLI_SEGMENTO'
,p_display_order=>1400
,p_column_identifier=>'EJ'
,p_column_label=>'Cli Segmento'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832106765874899596)
,p_db_column_name=>'CLI_VALOR_AGREGADO'
,p_display_order=>1410
,p_column_identifier=>'EK'
,p_column_label=>'Cli Valor Agregado'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832106948464899597)
,p_db_column_name=>'CLI_SEGMENTO_MACRO'
,p_display_order=>1420
,p_column_identifier=>'EL'
,p_column_label=>'Cli Segmento Macro'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832106960376899598)
,p_db_column_name=>'CLI_SEGMENTO_MICRO'
,p_display_order=>1430
,p_column_identifier=>'EM'
,p_column_label=>'Cli Segmento Micro'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832107073728899599)
,p_db_column_name=>'CLI_AUTO_LNEGRA'
,p_display_order=>1440
,p_column_identifier=>'EN'
,p_column_label=>'Cli Auto Lnegra'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832107172693899600)
,p_db_column_name=>'CLI_FREC_VISITA_SEM_PAR_IMPAR'
,p_display_order=>1450
,p_column_identifier=>'EO'
,p_column_label=>'Cli Frec Visita Sem Par Impar'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832107309105899601)
,p_db_column_name=>'CLI_IND_BLOQUEO_VTA_PERDIDA'
,p_display_order=>1460
,p_column_identifier=>'EP'
,p_column_label=>'Cli Ind Bloqueo Vta Perdida'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832107377253899602)
,p_db_column_name=>'CLI_SEGMENTO_SUC_CANAL'
,p_display_order=>1470
,p_column_identifier=>'EQ'
,p_column_label=>'Cli Segmento Suc Canal'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832107467286899603)
,p_db_column_name=>'CLI_CANAL_PROVISORIO'
,p_display_order=>1480
,p_column_identifier=>'ER'
,p_column_label=>'Cli Canal Provisorio'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832107608417899604)
,p_db_column_name=>'CLI_CANAL_BETA'
,p_display_order=>1490
,p_column_identifier=>'ES'
,p_column_label=>'Cli Canal Beta'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832107717034899605)
,p_db_column_name=>'CLI_HORA_RECEPCION'
,p_display_order=>1500
,p_column_identifier=>'ET'
,p_column_label=>'Cli Hora Recepcion'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832107823081899606)
,p_db_column_name=>'CLI_HR_AM_RECEP_DESDE'
,p_display_order=>1510
,p_column_identifier=>'EU'
,p_column_label=>'Cli Hr Am Recep Desde'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832107877302899607)
,p_db_column_name=>'CLI_HR_AM_RECEP_HASTA'
,p_display_order=>1520
,p_column_identifier=>'EV'
,p_column_label=>'Cli Hr Am Recep Hasta'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832108027731899608)
,p_db_column_name=>'CLI_HR_PM_RECEP_DESDE'
,p_display_order=>1530
,p_column_identifier=>'EW'
,p_column_label=>'Cli Hr Pm Recep Desde'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(7832108065913899609)
,p_db_column_name=>'CLI_HR_PM_RECEP_HASTA'
,p_display_order=>1540
,p_column_identifier=>'EX'
,p_column_label=>'Cli Hr Pm Recep Hasta'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(7832161080950930939)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'3813243'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'CLI_CODIGO:CLI_NOM:CLI_PROPIETARIO:CLI_NOM_FANTASIA:CLI_DIR:CLI_DOC_IDENT_PROPIETARIO:CLI_LOCALIDAD:CLI_FEC_NAC_PROPIETARIO:CLI_TEL:CLI_FAX:CLI_RUC:CLI_RAMO:CLI_PERSONERIA:CLI_CATEG:CLI_ZONA:CLI_PAIS:CLI_FEC_ANIV:CLI_PERS_CONTACTO:CLI_FORMA_ENVIO:CLI'
||'_OBS:CLI_PORC_EXEN_IVA:CLI_DOC_IDENT_CONTACTO:CLI_EST_CLI:CLI_FEC_NAC_CONTACTO:CLI_MON:CLI_IMP_LIM_CR:CLI_BLOQ_LIM_CR:CLI_MAX_DIAS_ATRASO:CLI_PERS_REPRESENTANTE:CLI_FEC_INGRESO:CLI_FEC_ACTUALIZACION:CLI_IND_POTENCIAL:CLI_DOC_IDENT_REPRESENTANTE:CLI_F'
||'EC_NAC_REPRESENTANTE:CLI_LUGAR_ORIGEN_REPLICA:CLI_NRO_FINCA:CLI_NOM_CONYUGUE:CLI_DOC_IDENT_CONYUGUE:CLI_DIR_PARTICULAR:CLI_LUGAR_TRABAJO:CLI_TEL_PARTICULAR:CLI_BARRIO:CLI_PROFESION:CLI_EMAIL:CLI_NRO_FICHA:CLI_PROYECTO:CLI_SEXO:CLI_TEL_CONTACTO:CLI_ES'
||'TADO_CIVIL:CLI_SALARIO:CLI_FEC_NAC_CONYUGUE:CLI_DIADOM:CLI_DIALUN:CLI_DIAMAR:CLI_DIAMIE:CLI_DIAJUE:CLI_DIAVIE:CLI_DIASAB:CLI_PEDIDO_REPETITIVO:CLI_REC_LPREC:CLI_DV:CLI_RUC_DV:CLI_CIUDAD:CLI_DIA_LUNES:CLI_DIA_MARTES:CLI_DIA_MIERCOLES:CLI_DIA_JUEVES:CL'
||'I_DIA_VIERNES:CLI_DIA_SABADO:CLI_VENDEDOR:CLI_ORDEN_LUN:CLI_ORDEN_MAR:CLI_ORDEN_MIER:CLI_ORDEN_JUE:CLI_ORDEN_VIER:CLI_ORDEN_SAB:CLI_COD_BARRIO:CLI_REPOSITOR:CLI_IND_PAGO_CON_CHEQUE:CLI_MAX_CANT_CUOTA_CONT:CLI_COD_PROVEEDOR:CLI_COMI_ASO:CLI_APELLIDO:C'
||'LI_NOMBRE:CLI_DIR_ESPECIFICA:CLI_ASOCIACION:CLI_DIR_FOTO:CLI_INFORMCONF:CLI_FEC_INGRESO_CLI:CLI_SECTOR_PROD:CLI_PERSONA:CLI_CANAL:CLI_IND_EXHIBIDOR:CLI_IND_MOD_CANAL:CLI_OBS_FACT:CLI_SUCURSAL:CLI_COBRADOR:CLI_COD_PROSPECTO:CLI_IND_SILO:CLI_COD_FICHA_'
||'HOLDING:CLI_CHORTI_DEB_AUTO:CLI_CHORTI_NRO_CTA:CLI_EXP_DIR:CLI_EXP_RUC:CLI_IND_TACHO:CLI_TEL_SMS:CLI_FREC_VISITA_SEMANAL:CLI_FREC_VISITA_QUINCENAL:CLI_LATITUD:CLI_LONGITUD:CLI_SUP_COBRANZA:CLI_RUC_HOLDING:CLI_EMPR:CLI_RESUMEN_FACT:CLI_ACA_COL_ORIG:CL'
||'I_HOLDING:CLI_ACA_DIR_LAB:CLI_ACA_TEL_LAB:CLI_ACA_NOM_FAC:CLI_ACA_DIR_FAC:CLI_ACA_CIP_FAC:CLI_ACA_RUC_FAC:CLI_ACA_NIVEL:CLI_ACA_GRADO:CLI_ACA_TURNO:CLI_ACA_SECCION:CLI_ACA_APLAZADO:CLI_ACA_CLI_TIPO:CLI_ACA_INACTIVO:CLI_ACA_IMG_BLOB:CLI_ACA_IMG_LR:CLI'
||'_COND_PAGO:CLI_FEC_ENTR_EXHIBIDOR:CLI_COD_EMPL_EMPR_ORIG:CLI_CODIGO_ANTERIOR:CLI_CTRL_PRECIO_AQ:CLI_FACT_COMB_GS:CLI_IND_EXIGIR_RESP_FCRED:CLI_PORC_FLETE:CLI_SEGMENTO:CLI_VALOR_AGREGADO:CLI_SEGMENTO_MACRO:CLI_SEGMENTO_MICRO:CLI_AUTO_LNEGRA:CLI_FREC_V'
||'ISITA_SEM_PAR_IMPAR:CLI_IND_BLOQUEO_VTA_PERDIDA:CLI_SEGMENTO_SUC_CANAL:CLI_CANAL_PROVISORIO:CLI_CANAL_BETA:CLI_HORA_RECEPCION:CLI_HR_AM_RECEP_DESDE:CLI_HR_AM_RECEP_HASTA:CLI_HR_PM_RECEP_DESDE:CLI_HR_PM_RECEP_HASTA'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7832108533100899613)
,p_plug_name=>'botonera'
,p_parent_plug_id=>wwv_flow_api.id(7830260364754414303)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(7832109236051899620)
,p_plug_name=>'CHEQUES EMITIDOS'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>70
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(7797045481050101689)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(7797044898330101683)
,p_button_name=>'Generar'
,p_button_static_id=>'generar'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large'
,p_button_template_id=>wwv_flow_api.id(7570351310891401263)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Generar Cuotas'
,p_button_position=>'BELOW_BOX'
,p_warn_on_unsaved_changes=>null
,p_button_cattributes=>'style="margin-top:20px;"'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(7832108572808899614)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(7832108533100899613)
,p_button_name=>'cerrar_ventana'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(7570351310891401263)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Cerrar'
,p_button_position=>'BELOW_BOX'
,p_warn_on_unsaved_changes=>null
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(7797047024069101704)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(7797046756156101701)
,p_button_name=>'Cheques'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(7570351310891401263)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Cheques'
,p_button_position=>'BOTTOM'
,p_warn_on_unsaved_changes=>null
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(7797047078136101705)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(7797046756156101701)
,p_button_name=>'Aceptar'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(7570351310891401263)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Aceptar'
,p_button_position=>'BOTTOM'
,p_warn_on_unsaved_changes=>null
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(7797047255047101706)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(7797046756156101701)
,p_button_name=>'Cancelar'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(7570351310891401263)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Cancelar'
,p_button_position=>'BOTTOM'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(7797047345043101707)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(7797046756156101701)
,p_button_name=>'Salir'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(7570351310891401263)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Salir'
,p_button_position=>'BOTTOM'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7789817156253819806)
,p_name=>'P8006_DOC_NRO_DOC'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>'Nro.Prestamo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>5
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_is_persistent=>'U'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7789817211912819807)
,p_name=>'P8006_S_DOC_FEC_DOC'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>'Fec.Comprob.'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>5
,p_grid_column=>7
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_is_persistent=>'U'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
end;
/
begin
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7789817258201819808)
,p_name=>'P8006_DOC_CLI_RUC'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>'RUC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>4
,p_grid_column=>9
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7789817456009819809)
,p_name=>'P8006_DOC_CLI_CODEUDOR'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>'Codeudor'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES_FINI'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select distinct cli_codigo || '' - ''||cli_nom, cli_codigo',
'  from fin_cliente, fin_cli_empresa',
' where cli_codigo = clem_cli',
'   and clem_empr = :p_empresa',
'   AND CLI_EMPR = CLEM_EMPR',
'   and cli_est_cli <> ''I''',
' order by 1',
''))
,p_lov_display_null=>'YES'
,p_lov_cascade_parent_items=>'P8006_P_IND_OPERADOR'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_colspan=>4
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7789817466807819810)
,p_name=>'P8006_NOMBRE_CODEUDOR'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>'Nombre Codeudor'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7789817652398819811)
,p_name=>'P8006_DOC_ACT_PRESTAMO'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>'Actividad'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT R.RAMO_CODIGO ||'' - ''|| R.RAMO_DESC AS D,',
'       R.RAMO_CODIGO AS R',
'FROM  FIN_CLIENTE C, ',
'      FIN_SECTOR_PROD S,',
'      FIN_RAMO R',
'WHERE C.CLI_SECTOR_PROD    =  S.SEPR_CODIGO',
'  AND S.SEPR_CODIGO        =  R.RAMO_SEC_PRODUCCION',
'  AND C.CLI_CODIGO         =  :P8006_S_CUENTA',
'  AND C.CLI_EMPR           =  :P_EMPRESA',
'  AND C.CLI_EMPR           =  S.SEPR_EMPR',
'  AND R.RAMO_EMPR          =  C.CLI_EMPR'))
,p_lov_display_null=>'YES'
,p_lov_cascade_parent_items=>'P8006_S_CUENTA'
,p_ajax_items_to_submit=>'P8006_S_CUENTA'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_colspan=>4
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7789817752203819812)
,p_name=>'P8006_S_ACT_DESC'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>'Nombre Codeudor'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7789817795023819813)
,p_name=>'P8006_DOC_NRO_CREDITO_PREST'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7789816767331819803)
,p_prompt=>'NRO. CREDITO'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>5
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7789817926176819814)
,p_name=>'P8006_DOC_INTERES_MON'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(7789816767331819803)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7789818014514819815)
,p_name=>'P8006_DOC_CAPITAL_MON'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(7789816767331819803)
,p_item_default=>'0'
,p_prompt=>'CAPITAL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>5
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7789818078659819816)
,p_name=>'P8006_DOC_CAPITAL_LOC'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(7789818854759819823)
,p_prompt=>'MON. LOCAL GS'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>5
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7789818162564819817)
,p_name=>'P8006_S_MON_DESC2'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7789816767331819803)
,p_prompt=>'MONEDA'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>5
,p_grid_column=>6
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7789818301834819818)
,p_name=>'P8006_DOC_INTERES_MON_CUO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(7789816767331819803)
,p_prompt=>'INTERES'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>5
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7789818445437819819)
,p_name=>'P8006_TOTAL_MON'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(7789816767331819803)
,p_prompt=>'TOTAL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>5
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7789818496340819820)
,p_name=>'P8006_DOC_INTERES_LOC'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(7789818854759819823)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7789818603195819821)
,p_name=>'P8006_TOTAL_LOC'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(7789818854759819823)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7795236335793667753)
,p_name=>'P8006_S_DOC_FEC_OPER'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>'Fec.Oper'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_colspan=>5
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_is_persistent=>'U'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7795236688913667757)
,p_name=>'P8006_S_CUENTA'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>'Cliente'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES_FINI'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select distinct cli_codigo || '' - ''||cli_nom, cli_codigo',
'  from fin_cliente, fin_cli_empresa',
' where cli_codigo = clem_cli',
'   and clem_empr = :p_empresa',
'   AND CLI_EMPR = CLEM_EMPR',
'   and cli_est_cli <> ''I''',
' order by 1',
''))
,p_cSize=>100
,p_colspan=>4
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_08=>'600'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7795237107436667758)
,p_name=>'P8006_DOC_CLI_NOM'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>'Doc Cli Nom'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>4
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7795237524654667758)
,p_name=>'P8006_S_NRO_SOLIC'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>unistr('Solicitud N\00BA')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7795237918215667758)
,p_name=>'P8006_DOC_CLI_DIR'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>unistr('Direcci\00F3n')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7795238298466667758)
,p_name=>'P8006_DOC_CLI_TEL'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>'Telefono'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>5
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7795239463876667759)
,p_name=>'P8006_DOC_MON'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Doc Mon'
,p_source=>'&P_MON_LOC.'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_MONEDA'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select MON_DESC as display_value, MON_CODIGO as return_value ',
'  from GEN_MONEDA',
' where mon_empr = :p_empresa ',
' order by 2'))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_colspan=>4
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7795239933268667759)
,p_name=>'P8006_MON_DESC'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>4
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7795240278540667760)
,p_name=>'P8006_S_TASA_OFIC'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>'Tasa'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7795240743763667760)
,p_name=>'P8006_DOC_CTA_BCO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>'Caja/Banco'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  IF :P8006_S_NRO_SOLIC IS NOT NULL THEN',
'    RETURN   ''select cta_codigo||'''' ''''||bco_desc||'''' - ''''||cta_desc||'''' - ''''||mon_simbolo as D, ',
'                     cta_codigo as R',
'              from fin_cuenta_bancaria, ',
'                   fin_banco, ',
'                   gen_moneda',
'             where cta_mon = mon_codigo(+)',
'               and cta_bco = bco_codigo(+)',
'               and cta_empr = :p_empresa',
'               AND cta_empr = mon_EMPR(+)',
'               and cta_empr = bco_EMPR(+)',
'               and mon_codigo = :p_mon_loc',
'             order by 1'';',
'  ELSE',
'    RETURN   ''select cta_codigo||'''' ''''||bco_desc||'''' - ''''||cta_desc||'''' - ''''||mon_simbolo as D, ',
'                     cta_codigo as R',
'              from fin_cuenta_bancaria, ',
'                   fin_banco, ',
'                   gen_moneda',
'             where cta_mon   = mon_codigo (+)',
'               and cta_bco   = bco_codigo (+)',
'               and cta_empr  = :p_empresa',
'               AND cta_empr  = mon_EMPR (+)',
'               and cta_EMPR  = bco_EMPR (+)',
'             order by 1''; ',
'  END IF;',
'END;'))
,p_lov_cascade_parent_items=>'P8006_S_NRO_SOLIC'
,p_ajax_items_to_submit=>'P8006_S_NRO_SOLIC'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_colspan=>4
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7795241154706667760)
,p_name=>'P8006_BCO_DESC'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>4
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7795241473381667760)
,p_name=>'P8006_CTA_DESC'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>'Cta Desc'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7795241863445667760)
,p_name=>'P8006_DOC_OBS'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>unistr('Observaci\00F3n')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7795242264585667761)
,p_name=>'P8006_S_IND_REESTRUCTURADO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>unistr('\00BFReestructurado?')
,p_source=>'N'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_column=>7
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797044672263101681)
,p_name=>'P8006_TIP_VENC'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7797044594144101680)
,p_prompt=>'Tip Venc'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>'STATIC:Mensual;M,Otros;D'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797044834791101682)
,p_name=>'P8006_S_DIAS_ENTRE_CUOTAS'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7797044594144101680)
,p_prompt=>'S Dias Entre Cuotas'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>6
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797045047822101684)
,p_name=>'P8006_TIP_GEN_CUO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7797044898330101683)
,p_prompt=>'S Dias Entre Cuotas'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>unistr('STATIC:Sistema Alem\00E1n;A,Sistema Carga Manual;MA,Sistema Franc\00E9s;F,Sistema Franc\00E9s Especial;FE')
,p_colspan=>6
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797045093027101685)
,p_name=>'P8006_S_PLAZO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7797044898330101683)
,p_prompt=>'Plazo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'readonly'
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797045251855101686)
,p_name=>'P8006_S_MONTO_CUO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(7797044898330101683)
,p_prompt=>'Cuotas (de)'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>5
,p_grid_label_column_span=>3
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797045319732101687)
,p_name=>'P8006_S_CANT_CUO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(7797044898330101683)
,p_prompt=>',en'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797045400833101688)
,p_name=>'P8006_LABEL'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(7797044898330101683)
,p_prompt=>'Cuotas(s)'
,p_source=>'Cuotas(s)'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797045733679101691)
,p_name=>'P8006_INTERES'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7797045653743101690)
,p_prompt=>unistr('% Inter\00E9s:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>8
,p_grid_label_column_span=>3
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797045809455101692)
,p_name=>'P8006_S_FEC_DESEMBOLSO'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(7797045653743101690)
,p_prompt=>'Fec.Desembolso:'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_colspan=>8
,p_grid_label_column_span=>3
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797045918229101693)
,p_name=>'P8006_S_DIAS_GRACIA'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(7797045653743101690)
,p_prompt=>'Periodo de Gracia:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>8
,p_grid_label_column_span=>3
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797046051722101694)
,p_name=>'P8006_S_FEC_PRIM_VTO_ORIG'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(7797045653743101690)
,p_prompt=>'Fec.Prim.Vto:'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_colspan=>8
,p_grid_label_column_span=>3
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797046086402101695)
,p_name=>'P8006_ANUAL'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7797045653743101690)
,p_prompt=>'New'
,p_source=>'Anual'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797046211414101696)
,p_name=>'P8006_S_FEC_PRIM_VTO'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(7797045653743101690)
,p_prompt=>'Fec.Prim.Vto:'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_colspan=>8
,p_grid_label_column_span=>3
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797046304794101697)
,p_name=>'P8006_S_CANT_CUOTAS'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(7797045653743101690)
,p_prompt=>'Cant.Cuotas:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>8
,p_grid_label_column_span=>3
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797046427998101698)
,p_name=>'P8006_S_TIPO_VENCIMIENTO_ORIG'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(7797045653743101690)
,p_prompt=>'Tipo Vencim.:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC:FIJA;F,VARIABLE;V'
,p_cHeight=>1
,p_colspan=>5
,p_grid_label_column_span=>3
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797046503477101699)
,p_name=>'P8006_CADA'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(7797045653743101690)
,p_prompt=>'New'
,p_source=>'Cada'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797047550635101709)
,p_name=>'P8006_TOTALES'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7797047411844101708)
,p_prompt=>'Totales'
,p_source=>'TOTALES'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_colspan=>2
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797047598686101710)
,p_name=>'P8006_S_TOT_IMP_CAP'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(7797047411844101708)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797047691443101711)
,p_name=>'P8006_S_MON_SIMBOLO2'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7797047411844101708)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797047806630101712)
,p_name=>'P8006_S_TOT_IMP_INT'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(7797047411844101708)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797047908421101713)
,p_name=>'P8006_S_TOT_IMP_CUO'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(7797047411844101708)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797048173119101716)
,p_name=>'P8006_TITULO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7797046598452101700)
,p_prompt=>'Titulo'
,p_source=>'DISTRIBUCION DE CUOTAS'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_colspan=>12
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797048265292101717)
,p_name=>'P8006_S_DIAS_ENTRE_CUOTAS_ORIG'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(7797045653743101690)
,p_prompt=>'S Dias Entre Cuotas Orig'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7797048422325101718)
,p_name=>'P8006_S_ETIQUETA_DIAS'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_api.id(7797045653743101690)
,p_prompt=>'New'
,p_source=>unistr('D\00EDas')
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798932490529678581)
,p_name=>'P8006_P_FLAG_IVA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798932651052678582)
,p_name=>'P8006_P_ACT_SAL_CAP'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798932705147678583)
,p_name=>'P8006_P_IND_REGENERANDO_CUO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798932759885678584)
,p_name=>'P8006_P_INTERES'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798932913233678585)
,p_name=>'P8006_P_IMPRESORA'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798933121798678587)
,p_name=>'P8006_IMP_ESTABLECIMIENTO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798933163878678588)
,p_name=>'P8006_IMP_PUNTO_EMISION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798933315766678589)
,p_name=>'P8006_IMP_ULT_NRO_PMOCONC'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
end;
/
begin
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798933383160678590)
,p_name=>'P8006_CONF_FEC_LIM_MOD'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798933496885678591)
,p_name=>'P8006_CONF_IND_HAB_MES_ACT'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798933652320678592)
,p_name=>'P8006_CONF_PER_SGTE_INI'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798933730369678593)
,p_name=>'P8006_CONF_TMOV_CAP_PRCO'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798933773383678594)
,p_name=>'P8006_CONF_PER_ACT_FIN'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798933860061678595)
,p_name=>'P8006_CONF_PER_SGTE_FIN'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798934030253678596)
,p_name=>'P8006_CONF_TMOV_PRCO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798934094007678597)
,p_name=>'P8006_CONF_TMOV_INT_PRCO'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798934162790678598)
,p_name=>'P8006_CONF_CONC_PRCO_CR'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798934342632678599)
,p_name=>'P8006_CONF_CONC_INT_PRCO_DB'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798934428892678600)
,p_name=>'P8006_CONF_CONC_INTC_PRCO_DB'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798934546578678601)
,p_name=>'P8006_CF_CLAVE_CTACO_INTC_PRCO'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798934623779678602)
,p_name=>'P8006_CONF_CONC_IVA_PRCO_DB'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798934712102678603)
,p_name=>'P8006_CONF_PORC_IVA_INT'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798934852502678604)
,p_name=>'P8006_CONF_CTACO_PRCO_CR'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798934934565678605)
,p_name=>'P8006_CONF_CTACO_INT_PRCO_DB'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798935035654678606)
,p_name=>'P8006_DOC_TIPO_MOV'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798935107998678607)
,p_name=>'P8006_TMOV_DESC'
,p_item_sequence=>270
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798935209743678608)
,p_name=>'P8006_DOC_TIPO_SALDO'
,p_item_sequence=>280
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798935345632678609)
,p_name=>'P8006_W_BASE_IMPON'
,p_item_sequence=>300
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798935435949678610)
,p_name=>'P8006_W_IND_ER'
,p_item_sequence=>310
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798935552691678611)
,p_name=>'P8006_W_AFECTA_SALDO'
,p_item_sequence=>320
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798935615280678612)
,p_name=>'P8006_CONF_CTACO_IVA_PRCO_DB'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798936394428678620)
,p_name=>'P8006_DOC_FEC_OPER'
,p_item_sequence=>330
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798936783245678624)
,p_name=>'P8006_DOC_FEC_DOC'
,p_item_sequence=>340
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7798937168951678628)
,p_name=>'P8006_DOC_PROV'
,p_item_sequence=>350
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799262181515746579)
,p_name=>'P8006_DOC_CLI'
,p_item_sequence=>360
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799262297044746580)
,p_name=>'P8006_TMOV_NOTA_DEB_EMIT_PROV'
,p_item_sequence=>370
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799262435804746581)
,p_name=>'P8006_DOC_LEGAJO'
,p_item_sequence=>380
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799262466168746582)
,p_name=>'P8006_P_CLI_DIR'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799262570754746583)
,p_name=>'P8006_P_CLI_TEL'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799262696460746584)
,p_name=>'P8006_P_CLI_RUC'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799262821385746585)
,p_name=>'P8006_P_CLI_NOM'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799262924887746586)
,p_name=>'P8006_P_CLI_CODIGO'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799263054901746587)
,p_name=>'P8006_P_VENDEDOR'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799264693081746604)
,p_name=>'P8006_V_BANCO'
,p_item_sequence=>390
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799265280902746610)
,p_name=>'P8006_TIPO_S_AUX'
,p_item_sequence=>290
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799265634122746613)
,p_name=>'P8006_P_CTA_BCO_DIA'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799265683693746614)
,p_name=>'P8006_P_CTA_DESC'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799265765313746615)
,p_name=>'P8006_P_BANCO'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799265876765746616)
,p_name=>'P8006_P_IND_CHEQUE_DIF'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799265983938746617)
,p_name=>'P8006_S_IMP_CHEQUE'
,p_item_sequence=>400
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799266472944746622)
,p_name=>'P8006_W_MON_DEC_IMP'
,p_item_sequence=>410
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799267030747746627)
,p_name=>'P8006_W_MON_DEC_TASA'
,p_item_sequence=>420
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799267147651746628)
,p_name=>'P8006_MON_SIMBOLO'
,p_item_sequence=>430
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799827672624037187)
,p_name=>'P8006_DOC_NETO_EXEN_LOC'
,p_item_sequence=>440
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799827842414037188)
,p_name=>'P8006_DOC_NETO_GRAV_LOC'
,p_item_sequence=>450
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799827945742037189)
,p_name=>'P8006_DOC_IVA_LOC'
,p_item_sequence=>460
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799827977272037190)
,p_name=>'P8006_DOC_NETO_EXEN_MON'
,p_item_sequence=>470
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799828156705037191)
,p_name=>'P8006_DOC_NETO_GRAV_MON'
,p_item_sequence=>480
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799828208009037192)
,p_name=>'P8006_DOC_IVA_MON'
,p_item_sequence=>490
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7799829168922037202)
,p_name=>'P8006_DOC_TIP_MOV_PERMISO'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_prompt=>'Socio'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>'STATIC:Si;S,No;N'
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'2'
,p_attribute_02=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7800976241914239509)
,p_name=>'P8006_FEC_DESEMBOLSO'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_api.id(7797045653743101690)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7830259340283414292)
,p_name=>'P8006_P_MON_US'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7830259440125414293)
,p_name=>'P8006_P_MON_LOC'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7830259524203414294)
,p_name=>'P8006_P_IND_OPERADOR'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7830259635557414295)
,p_name=>'P8006_P_FORMATO'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7830259736990414296)
,p_name=>'P8006_P_VALIDAD_PE_ES'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7830259820054414297)
,p_name=>'P8006_P_MONEDA'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7830259928717414298)
,p_name=>'P8006_DOC_CLAVE_SOL'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7830260033782414299)
,p_name=>'P8006_P_GARANTE'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7830260089892414300)
,p_name=>'P8006_FEC_PRIM_VTO'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_api.id(7797045653743101690)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7830260198905414301)
,p_name=>'P8006_S_DOC_MONEDA'
,p_item_sequence=>500
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7830260338124414302)
,p_name=>'P8006_P_MON_SIMB'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7832108224333899610)
,p_name=>'P8006_MOSTRAR_VENTANA'
,p_item_sequence=>510
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7832109328886899621)
,p_name=>'P8006_M_BENEFICIARIO_INI'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(7832109236051899620)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7832109421392899622)
,p_name=>'P8006_S_TOTAL_IMP'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(7832109236051899620)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844985175418600981)
,p_name=>'P8006_W_LIM_AUTORIZADO'
,p_item_sequence=>520
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844985315935600982)
,p_name=>'P8006_LISTA_PRECIO_DESC'
,p_item_sequence=>530
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844985437728600983)
,p_name=>'P8006_PED_DEP'
,p_item_sequence=>540
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844985511292600984)
,p_name=>'P8006_S_DOC_NRO_FAC'
,p_item_sequence=>550
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844985601781600985)
,p_name=>'P8006_DOC_CLAVE_SCLI'
,p_item_sequence=>560
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844985661573600986)
,p_name=>'P8006_S_BASE_IMPON'
,p_item_sequence=>570
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844985838722600987)
,p_name=>'P8006_CONF_FACT_CR_REC'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844986123136600990)
,p_name=>'P8006_CONF_FACT_CR_EMIT'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844986250278600991)
,p_name=>'P8006_CONF_ADELANTO_CLI'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844986355965600992)
,p_name=>'P8006_CONF_ADELANTO_PROV'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844986419927600993)
,p_name=>'P8006_TMOV_DESPACHO'
,p_item_sequence=>580
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844986485479600994)
,p_name=>'P8006_TMOV_IMPORTACION'
,p_item_sequence=>590
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844986590313600995)
,p_name=>'P8006_TMOV_ORDEN_COMPRA'
,p_item_sequence=>600
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844986756409600996)
,p_name=>'P8006_CONF_NOTA_CR_REC'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844986792062600997)
,p_name=>'P8006_CONF_NOTA_CR_EMIT'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844986882920600998)
,p_name=>'P8006_CONF_NOTA_DB_REC'
,p_item_sequence=>270
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844986996340600999)
,p_name=>'P8006_CONF_CONF_NOTA_DB_EMIT'
,p_item_sequence=>280
,p_item_plug_id=>wwv_flow_api.id(7798933024752678586)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844987066257601000)
,p_name=>'P8006_DOC_EMPR'
,p_item_sequence=>610
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844987171836601001)
,p_name=>'P8006_DOC_SUC'
,p_item_sequence=>620
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844987299081601002)
,p_name=>'P8006_DOC_LOGIN'
,p_item_sequence=>630
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844987388528601003)
,p_name=>'P8006_DOC_FEC_GRAB'
,p_item_sequence=>640
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844987519120601004)
,p_name=>'P8006_DOC_SIST'
,p_item_sequence=>650
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844987569503601005)
,p_name=>'P8006_P_LISTA'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844987666462601006)
,p_name=>'P8006_P_ALMACEN'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844987796225601007)
,p_name=>'P8006_P_NRO_FACTURA'
,p_item_sequence=>270
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844987874570601008)
,p_name=>'P8006_P_FECHA_DOCUMENTO'
,p_item_sequence=>280
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844988046295601009)
,p_name=>'P8006_DOC_BASE_IMPON_MON'
,p_item_sequence=>290
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844988066450601010)
,p_name=>'P8006_DOC_BASE_IMPON_LOC'
,p_item_sequence=>300
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844988196880601011)
,p_name=>'P8006_DOC_BRUTO_EXEN_MON'
,p_item_sequence=>310
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844988354738601012)
,p_name=>'P8006_DOC_BRUTO_EXEN_LOC'
,p_item_sequence=>320
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844988453076601013)
,p_name=>'P8006_DOC_BRUTO_GRAV_MON'
,p_item_sequence=>330
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844988520921601014)
,p_name=>'P8006_DOC_BRUTO_GRAV_LOC'
,p_item_sequence=>340
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844988601282601015)
,p_name=>'P8006_DOC_SALDO_LOC'
,p_item_sequence=>350
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844988678798601016)
,p_name=>'P8006_DOC_SALDO_MON'
,p_item_sequence=>360
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844988797864601017)
,p_name=>'P8006_DOC_SAL_CAP_MON'
,p_item_sequence=>370
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844988885524601018)
,p_name=>'P8006_DOC_SAL_CAP_LOC'
,p_item_sequence=>380
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844989043130601019)
,p_name=>'P8006_DOC_SAL_INT_MON'
,p_item_sequence=>390
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844989120329601020)
,p_name=>'P8006_DOC_SAL_INT_LOC'
,p_item_sequence=>400
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844989220027601021)
,p_name=>'P8006_DOC_SALDO_PER_ACT_LOC'
,p_item_sequence=>410
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844989355795601022)
,p_name=>'P8006_DOC_SALDO_PER_ACT_MON'
,p_item_sequence=>420
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844989368082601023)
,p_name=>'P8006_DOC_SAL_CAP_PER_ACT_LOC'
,p_item_sequence=>430
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844989547122601024)
,p_name=>'P8006_DOC_SAL_CAP_PER_ACT_MON'
,p_item_sequence=>440
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844989574976601025)
,p_name=>'P8006_DOC_SAL_INT_PER_ACT_MON'
,p_item_sequence=>450
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844989681655601026)
,p_name=>'P8006_DOC_SAL_INT_PER_ACT_LOC'
,p_item_sequence=>460
,p_item_plug_id=>wwv_flow_api.id(7798932423110678580)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844989802911601027)
,p_name=>'P8006_DOC_INTERESES_LOC'
,p_item_sequence=>660
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7844989935461601028)
,p_name=>'P8006_DOC_IVA_INT_LOC'
,p_item_sequence=>670
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7845072033014005779)
,p_name=>'P8006_DOC_IVA_INT_MON'
,p_item_sequence=>680
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7845072115843005780)
,p_name=>'P8006_DOC_IVA_INT_MON_1'
,p_item_sequence=>690
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7845072216643005781)
,p_name=>'P8006_DOC_CLAVE_DOC_PMO'
,p_item_sequence=>700
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7845072310746005782)
,p_name=>'DOC_CLAVE_DOC_CAP'
,p_item_sequence=>710
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7845072422742005783)
,p_name=>'DOC_CLAVE_DOC_INT'
,p_item_sequence=>720
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7845072459480005784)
,p_name=>'P8006_S_TOT_IMP_CAP_LOC'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(7797047411844101708)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7845072567072005785)
,p_name=>'P8006_S_TOT_IMP_INT_LOC'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(7797047411844101708)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7845072731494005786)
,p_name=>'P8006_S_TOT_IMP_CUO_LOC'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(7797047411844101708)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7852133324416293790)
,p_name=>'P8006_DOC_CLAVE_DOC_CAP'
,p_item_sequence=>730
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7943949508689278789)
,p_name=>'P8006_CANT_PAGARES'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(7797047411844101708)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(7943949659612278791)
,p_name=>'P8006_DOC_CLAVE_DOC_INT'
,p_item_sequence=>740
,p_item_plug_id=>wwv_flow_api.id(8111619448247837966)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7789818946857819824)
,p_name=>'ocultar region'
,p_event_sequence=>10
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7789818984889819825)
,p_event_id=>wwv_flow_api.id(7789818946857819824)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(7789818854759819823)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7797047980321101714)
,p_name=>'Ocultar items'
,p_event_sequence=>20
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7797048060448101715)
,p_event_id=>wwv_flow_api.id(7797047980321101714)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_S_DIAS_GRACIA,P8006_S_CANT_CUOTAS,P8006_S_FEC_PRIM_VTO_ORIG,P8006_S_TIPO_VENCIMIENTO_ORIG,P8006_CADA,P8006_S_DIAS_ENTRE_CUOTAS_ORIG,P8006_S_ETIQUETA_DIAS'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7798935744953678613)
,p_name=>'validar_nro_prestamo'
,p_event_sequence=>30
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_NRO_DOC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7798936115767678617)
,p_event_id=>wwv_flow_api.id(7798935744953678613)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'FINI034.PP_VALIDAR_NRO_PRESTAMO (I_EMPRESA      => :P_EMPRESA,',
'                                 I_TIPO_MOV     => :P8006_DOC_TIPO_MOV,',
'                                 I_DOC_NRO_DOC  => :P8006_DOC_NRO_DOC);'))
,p_attribute_02=>'P8006_DOC_TIPO_MOV,P8006_DOC_NRO_DOC'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7798936216001678618)
,p_name=>'validar_fecha_oper'
,p_event_sequence=>40
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_S_DOC_FEC_OPER'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
end;
/
begin
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7798936272413678619)
,p_event_id=>wwv_flow_api.id(7798936216001678618)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'',
'--AF_VALIDAR_FECHA (''OVF'', ''BDOC.S_DOC_FEC_OPER'', NULL, :GLOBAL.FECHA_SISTEMA);',
'   --:P8006_DOC_FEC_OPER := TO_DATE(:P8006_S_DOC_FEC_OPER, ''DD/MM/YYYY'');',
'   :P8006_DOC_FEC_OPER := :P8006_S_DOC_FEC_OPER;',
'--',
'IF NOT TO_DATE(:P8006_DOC_FEC_OPER,''DD/MM/YYYY'') BETWEEN TO_DATE(:P8006_CONF_FEC_LIM_MOD,''DD/MM/YYYY'')',
'                               AND TO_DATE(:P8006_CONF_PER_SGTE_FIN,''DD/MM/YYYY'')',
'',
'THEN',
'    RAISE_APPLICATION_ERROR(-20010,''La fecha debe estar comprendida entre el ''||',
'                   TO_CHAR(:P8006_CONF_FEC_LIM_MOD)||',
'		  '' y el ''||TO_CHAR(:P8006_CONF_PER_SGTE_FIN));',
'END IF;',
'',
'    FINI034.PL_VALIDAR_HAB_MES_FIN(:P8006_DOC_FEC_OPER,:P_EMPRESA,:APP_USER);'))
,p_attribute_02=>'P8006_S_DOC_FEC_OPER,P8006_CONF_FEC_LIM_MOD,P8006_CONF_PER_SGTE_FIN'
,p_attribute_03=>'P8006_DOC_FEC_OPER'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7798936637710678622)
,p_name=>'validar_fecha_doc'
,p_event_sequence=>50
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_S_DOC_FEC_DOC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7798936487361678621)
,p_event_id=>wwv_flow_api.id(7798936637710678622)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*IF :P8006_S_DOC_FEC_DOC IS NULL THEN',
'   :P8006_DOC_FEC_DOC := TRUNC(SYSDATE);',
'   :P8006_S_DOC_FEC_DOC := TO_CHAR(:P8006_DOC_FEC_DOC);',
'END IF;*/',
'',
'--AF_VALIDAR_FECHA (''OVF'', ''BDOC.S_DOC_FEC_OPER'', NULL, :GLOBAL.FECHA_SISTEMA);',
'   :P8006_DOC_FEC_DOC := TO_DATE(:P8006_S_DOC_FEC_DOC, ''DD/MM/YYYY'');',
'--',
'IF NOT TO_DATE(:P8006_DOC_FEC_DOC,''DD/MM/YYYY'') BETWEEN TO_DATE(:P8006_CONF_FEC_LIM_MOD,''DD/MM/YYYY'')',
'                               AND TO_DATE(:P8006_CONF_PER_SGTE_FIN,''DD/MM/YYYY'')',
'THEN',
'    RAISE_APPLICATION_ERROR(-20010,''La fecha debe estar comprendida entre el ''||',
'                   TO_CHAR(:P8006_CONF_FEC_LIM_MOD)||',
'		  '' y el ''||TO_CHAR(:P8006_CONF_PER_SGTE_FIN));',
'END IF;',
'',
'    FINI034.PL_VALIDAR_HAB_MES_FIN(:P8006_DOC_FEC_DOC,:P_EMPRESA,:APP_USER);'))
,p_attribute_02=>'P8006_S_DOC_FEC_DOC,P8006_CONF_FEC_LIM_MOD,P8006_CONF_PER_SGTE_FIN'
,p_attribute_03=>'P8006_DOC_FEC_DOC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7798936904182678625)
,p_name=>'validar_cliente'
,p_event_sequence=>60
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_S_CUENTA'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7798936985219678626)
,p_event_id=>wwv_flow_api.id(7798936904182678625)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'    :P8006_DOC_CLI_NOM := NULL;',
'    :P8006_DOC_CLI_DIR := NULL;',
'    :P8006_DOC_CLI_TEL := NULL;',
'    :P8006_DOC_CLI_RUC := NULL;',
'    --break;',
'DECLARE',
'',
'    PROCEDURE PP_TRAER_DESC_CLI IS',
'--',
'    BEGIN',
'',
'        SELECT CLI_NOM, CLI_DIR, SUBSTR(CLI_TEL,1,15), CLI_RUC',
'        INTO   :P8006_DOC_CLI_NOM, :P8006_DOC_CLI_DIR,',
'               :P8006_DOC_CLI_TEL, :P8006_DOC_CLI_RUC           ',
'        FROM   FIN_CLIENTE',
'        WHERE  CLI_CODIGO = :P8006_S_CUENTA',
'          AND  CLI_EMPR = :P_EMPRESA;',
'',
'          :P8006_DOC_CLI      :=  :P8006_S_CUENTA;',
'          :P8006_P_CLI_DIR    :=  :P8006_DOC_CLI_DIR ;',
'          :P8006_P_CLI_TEL    :=  :P8006_DOC_CLI_TEL ;',
'          :P8006_P_CLI_RUC    :=  :P8006_DOC_CLI_RUC;',
'          :P8006_P_CLI_NOM    :=  :P8006_DOC_CLI_NOM ;',
'          :P8006_P_CLI_CODIGO :=  :P8006_DOC_CLI ;',
'          :P8006_P_VENDEDOR   :=  :P8006_DOC_LEGAJO;',
'    EXCEPTION',
'      WHEN NO_DATA_FOUND THEN',
'            :P8006_DOC_CLI     := NULL;',
'            :P8006_DOC_CLI_NOM := NULL;',
'            :P8006_DOC_CLI_DIR := NULL;',
'            :P8006_DOC_CLI_TEL := NULL;',
'            :P8006_DOC_CLI_RUC := NULL;',
'            RAISE_APPLICATION_ERROR(-20010,''Cliente inexistente'');',
'      ',
'      WHEN OTHERS THEN',
'            NULL;',
'    END;',
'    ',
'BEGIN',
'    IF :P8006_S_CUENTA IS NULL THEN ',
'          :P8006_DOC_PROV := NULL;',
'          :P8006_DOC_CLI  := NULL;',
'',
'    ELSE',
'          IF :P8006_W_IND_ER = ''E'' THEN',
'                IF :P8006_DOC_TIPO_MOV = :P8006_TMOV_NOTA_DEB_EMIT_PROV THEN',
'                        --Este movimiento es emitido pero es a proveedor',
'                        FINI034.PP_TRAER_DESC_PROV (I_EMPRESA     =>  :P_EMPRESA,',
'                                                    I_DOC_MON     =>  :P8006_DOC_MON,',
'                                                    I_S_CUENTA    =>  :P8006_S_CUENTA,',
'                                                    O_CLI_NOM     =>  :P8006_DOC_CLI_NOM,',
'                                                    O_CLI_DIR     =>  :P8006_DOC_CLI_DIR,    ',
'                                                    O_CLI_TEL     =>  :P8006_DOC_CLI_TEL,',
'                                                    O_CLI_RUC     =>  :P8006_DOC_CLI_RUC,',
'                                                    O_DOC_PROV    =>  :P8006_DOC_PROV);',
'                    ELSE',
'                        PP_TRAER_DESC_CLI;',
'                        FINI034.PP_TRAER_NRO_CRED(  I_S_CUENTA          => :P8006_S_CUENTA,',
'                                                    I_EMPRESA           => :P_EMPRESA,',
'                                                    O_NRO_CREDITO_PREST => :P8006_DOC_NRO_CREDITO_PREST);',
'                        FINI034.PP_BUSCAR_RAMO (  I_EMPRESA            => :P_EMPRESA,',
'                                                  I_S_CUENTA           => :P8006_S_CUENTA,',
'                                                  O_DOC_ACT_PRESTAMO   => :P8006_DOC_ACT_PRESTAMO);',
'                  END IF;',
'          ELSIF :P8006_W_IND_ER = ''R'' THEN',
'                FINI034.PP_TRAER_DESC_PROV (I_EMPRESA     =>  :P_EMPRESA,',
'                                            I_DOC_MON     =>  :P8006_DOC_MON,',
'                                            I_S_CUENTA    =>  :P8006_S_CUENTA,',
'                                            O_CLI_NOM     =>  :P8006_DOC_CLI_NOM,',
'                                            O_CLI_DIR     =>  :P8006_DOC_CLI_DIR,    ',
'                                            O_CLI_TEL     =>  :P8006_DOC_CLI_TEL,',
'                                            O_CLI_RUC     =>  :P8006_DOC_CLI_RUC,',
'                                            O_DOC_PROV    =>  :P8006_DOC_PROV);',
'          END IF;',
'    END IF;',
'END;'))
,p_attribute_02=>'P8006_S_CUENTA,P8006_DOC_LEGAJO,P8006_W_IND_ER,P8006_DOC_TIPO_MOV,P8006_TMOV_NOTA_DEB_EMIT_PROV'
,p_attribute_03=>'P8006_DOC_CLI_NOM,P8006_DOC_CLI_DIR,P8006_DOC_CLI_TEL,P8006_DOC_CLI_RUC,P8006_DOC_CLI,P8006_P_CLI_DIR,P8006_P_CLI_TEL,P8006_P_CLI_RUC,P8006_P_CLI_NOM,P8006_P_CLI_CODIGO,P8006_P_VENDEDOR,P8006_DOC_PROV,P8006_DOC_NRO_CREDITO_PREST,P8006_DOC_ACT_PRESTAM'
||'O'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7799263205960746589)
,p_name=>'validar_nombre'
,p_event_sequence=>70
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_CLI_NOM'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799263283681746590)
,p_event_id=>wwv_flow_api.id(7799263205960746589)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  IF   :P8006_S_CUENTA IS NULL',
'   AND :P8006_DOC_CLI_NOM IS NULL',
'  THEN',
'     RAISE_APPLICATION_ERROR(-20010,''Datos del Cliente/Proveedor deben ingresarse!'');',
'  END IF;',
'END;'))
,p_attribute_02=>'P8006_S_CUENTA,P8006_DOC_CLI_NOM'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7799263397390746591)
,p_name=>'asignar atributos'
,p_event_sequence=>80
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_S_CUENTA'
,p_condition_element=>'P8006_S_CUENTA'
,p_triggering_condition_type=>'NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799263467548746592)
,p_event_id=>wwv_flow_api.id(7799263397390746591)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_DOC_CLI_NOM,P8006_DOC_CLI_RUC,P8006_DOC_CLI_DIR,P8006_DOC_CLI_TEL'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799263689250746594)
,p_event_id=>wwv_flow_api.id(7799263397390746591)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_DOC_CLI_NOM,P8006_DOC_CLI_RUC,P8006_DOC_CLI_DIR,P8006_DOC_CLI_TEL'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7799263783582746595)
,p_name=>'validar_nro_solicitud'
,p_event_sequence=>90
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_S_NRO_SOLIC'
,p_condition_element=>'P8006_S_NRO_SOLIC'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799263913088746596)
,p_event_id=>wwv_flow_api.id(7799263783582746595)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_DOC_CAPITAL_MON'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7830258632413414285)
,p_event_id=>wwv_flow_api.id(7799263783582746595)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':P8006_DOC_CAPITAL_MON := 1;'
,p_attribute_03=>'P8006_DOC_CAPITAL_MON'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799264055112746597)
,p_event_id=>wwv_flow_api.id(7799263783582746595)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_DOC_CAPITAL_MON'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7799264194408746599)
,p_name=>'validar_cta_bco'
,p_event_sequence=>100
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_CTA_BCO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799264272614746600)
,p_event_id=>wwv_flow_api.id(7799264194408746599)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    PROCEDURE PP_VERIF_CTA_BANC IS',
'    BEGIN',
'    --*',
'    IF :P8006_S_NRO_SOLIC IS NOT NULL THEN	',
'      SELECT CTA_MON, 						',
'             BCO_DESC, ',
'             CTA_DESC,',
'             CTA_BCO_DIA',
'        INTO :P8006_DOC_MON,',
'             :P8006_BCO_DESC, ',
'             :P8006_CTA_DESC, 			',
'             :P8006_P_CTA_BCO_DIA',
'        FROM FIN_CUENTA_BANCARIA, ',
'             FIN_BANCO',
'       WHERE CTA_BCO    = BCO_CODIGO (+)',
'         AND CTA_EMPR   = :P_EMPRESA',
'         AND CTA_EMPR   = BCO_EMPR   (+)',
'         AND CTA_CODIGO = :P8006_DOC_CTA_BCO',
'         AND CTA_MON = :P_MON_LOC;',
'    ELSE     ',
'         SELECT CTA_MON, 						',
'                BCO_DESC, ',
'                CTA_DESC, 						',
'                CTA_BCO_DIA',
'        INTO   :P8006_DOC_MON, 			',
'               :P8006_BCO_DESC, ',
'               :P8006_CTA_DESC, 			',
'               :P8006_P_CTA_BCO_DIA',
'        FROM FIN_CUENTA_BANCARIA, ',
'             FIN_BANCO',
'       WHERE CTA_EMPR   = :P_EMPRESA',
'         AND CTA_EMPR   = BCO_EMPR (+)',
'         AND CTA_BCO    = BCO_CODIGO (+)',
'         AND CTA_CODIGO = :P8006_DOC_CTA_BCO;',
'    END IF;',
'    --*',
'      IF 	:P8006_P_CTA_DESC IS NULL THEN',
'            :P8006_P_CTA_DESC := :P8006_BCO_DESC;',
'      END IF;',
'      FINI034.PL_VALIDAR_OPCTA(:P8006_DOC_CTA_BCO, :P_EMPRESA, :APP_USER, :P8006_P_CTA_DESC,:P8006_DOC_TIPO_SALDO) ;',
'    --*  ',
'      IF :P8006_DOC_MON = :P_MON_LOC THEN',
'         :P8006_S_TASA_OFIC := 1;',
'        ELSE                                    ',
'         :P8006_S_TASA_OFIC :=GEN_COT_TIPO_MOV(:P_EMPRESA,:P8006_DOC_MON, :P8006_DOC_TIPO_MOV,TO_DATE(:P8006_S_DOC_FEC_OPER,''dd/mm/yyyy''));',
'         ',
'        END IF;',
'',
'    EXCEPTION',
'      WHEN NO_DATA_FOUND THEN',
'        :P8006_DOC_MON  := NULL;',
'        :P8006_BCO_DESC := NULL;',
'        :P8006_CTA_DESC := NULL;',
'        RAISE_APPLICATION_ERROR(-20010,''Cuenta bancaria inexistente o solo puede utilizar Cta. bancaria en moneda local..!'');',
'      ',
'      WHEN OTHERS THEN',
'        NULL;',
'    END;',
'BEGIN	',
'    ',
'    IF :P8006_DOC_CTA_BCO IS NULL THEN',
'	  :P8006_BCO_DESC := NULL;',
'	  :P8006_CTA_DESC := NULL;	 ',
'',
'	',
'	ELSE ',
'		  PP_VERIF_CTA_BANC;',
'		  FINI034.PP_CTA_CHEQ_EMIT_DIF(I_EMPRESA          => :P_EMPRESA,',
'                                       I_DOC_CTA_BCO      => :P8006_DOC_CTA_BCO,',
'                                       O_P_BANCO          => :P8006_P_BANCO,',
'                                       O_P_IND_CHEQUE_DIF => :P8006_P_IND_CHEQUE_DIF,',
'                                       O_S_IMP_CHEQUE     => :P8006_S_IMP_CHEQUE,',
'                                       O_V_BANCO          => :P8006_V_BANCO);',
'		',
'	END IF;',
'',
'END;'))
,p_attribute_02=>'P8006_DOC_CTA_BCO,P8006_P_CTA_DESC,P8006_DOC_TIPO_SALDO,P8006_S_DOC_FEC_OPER'
,p_attribute_03=>'P8006_DOC_MON,P8006_BCO_DESC,P8006_CTA_DESC,P8006_P_CTA_BCO_DIA,P8006_P_CTA_DESC,P8006_S_TASA_OFIC,P8006_P_BANCO,P8006_P_IND_CHEQUE_DIF,P8006_S_IMP_CHEQUE,P8006_V_BANCO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7799264383347746601)
,p_name=>'setear atributo cuenta'
,p_event_sequence=>110
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_CTA_BCO'
,p_condition_element=>'P8006_DOC_CTA_BCO'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799264545396746602)
,p_event_id=>wwv_flow_api.id(7799264383347746601)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_DOC_MON,P8006_S_TASA_OFIC'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799264590628746603)
,p_event_id=>wwv_flow_api.id(7799264383347746601)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_DOC_MON,P8006_S_TASA_OFIC'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7799264820181746605)
,p_name=>'boton_cheques'
,p_event_sequence=>120
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_V_BANCO'
,p_condition_element=>'P8006_V_BANCO'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799264872440746606)
,p_event_id=>wwv_flow_api.id(7799264820181746605)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(7797047024069101704)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799264997942746607)
,p_event_id=>wwv_flow_api.id(7799264820181746605)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(7797047024069101704)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7799265090586746608)
,p_name=>'cambio_tipo_saldo'
,p_event_sequence=>130
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_TIPO_SALDO'
,p_condition_element=>'P8006_DOC_TIPO_SALDO'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'C'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799265187747746609)
,p_event_id=>wwv_flow_api.id(7799265090586746608)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_TIPO_S_AUX'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'1'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7799265405698746611)
,p_name=>'cambio aux'
,p_event_sequence=>140
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_TIPO_S_AUX'
,p_condition_element=>'P8006_P_BANCO'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799265485601746612)
,p_event_id=>wwv_flow_api.id(7799265405698746611)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(7797047024069101704)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799266084755746618)
,p_event_id=>wwv_flow_api.id(7799265405698746611)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(7797047024069101704)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7799266166019746619)
,p_name=>'cambio_moneda'
,p_event_sequence=>150
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_MON'
,p_condition_element=>'P8006_DOC_MON'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799266324777746620)
,p_event_id=>wwv_flow_api.id(7799266166019746619)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'	 ',
'	',
'	  IF :P8006_DOC_MON = :P_MON_LOC THEN',
'	     :P8006_W_MON_DEC_IMP := 0;',
'	    --si es moneda local',
'	    --SET_ITEM_PROPERTY(''BDOC.S_TASA_OFIC'',       ENABLED,PROPERTY_FALSE);',
'	    ',
'	  ELSE',
'	    --si es moneda extranjera',
'        null;',
'	  END IF;',
'',
'    FINI034.PP_TRAER_DESC_MONE(I_DOC_MON         => :P8006_DOC_MON,',
'                               I_S_DOC_FEC_OPER  => TO_DATE(:P8006_S_DOC_FEC_OPER,''DD/MM/YYYY''),',
'                               O_MON_DESC        => :P8006_MON_DESC,',
'                               O_W_MON_DEC_IMP   => :P8006_W_MON_DEC_IMP,',
'                               O_W_MON_DEC_TASA  => :P8006_W_MON_DEC_TASA,',
'                               O_MON_SIMBOLO     => :P8006_MON_SIMBOLO,',
'                               O_S_TASA_OFIC     => :P8006_S_TASA_OFIC);',
'                               ',
'    FINI034.PP_BUSCAR_CLI_EMPR_MONEDA(I_S_CUENTA  => :P8006_S_CUENTA,',
'                                      I_DOC_MON   => :P8006_DOC_MON);',
'  --  PP_VALIDAR_LIMITE_CREDITO;',
'',
'END;'))
,p_attribute_02=>'P8006_DOC_MON,P8006_S_DOC_FEC_OPER,P8006_S_CUENTA'
,p_attribute_03=>'P8006_W_MON_DEC_IMP,P8006_MON_DESC,P8006_W_MON_DEC_TASA,P8006_MON_SIMBOLO,P8006_S_TASA_OFIC'
,p_attribute_04=>'N'
,p_stop_execution_on_error=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7799827099254037181)
,p_name=>'moneda nula'
,p_event_sequence=>150
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_MON'
,p_condition_element=>'P8006_DOC_MON'
,p_triggering_condition_type=>'NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799827032750037180)
,p_event_id=>wwv_flow_api.id(7799827099254037181)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':P8006_DOC_MON := :P_MON_LOC;'
,p_attribute_03=>'P8006_DOC_MON'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7799266628405746623)
,p_name=>'establecer atributos'
,p_event_sequence=>160
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_MON'
,p_condition_element=>'P8006_DOC_MON'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'&P_MON_LOC.'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799266713762746624)
,p_event_id=>wwv_flow_api.id(7799266628405746623)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_S_TASA_OFIC'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799266765991746625)
,p_event_id=>wwv_flow_api.id(7799266628405746623)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_S_TASA_OFIC'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799266927694746626)
,p_event_id=>wwv_flow_api.id(7799266628405746623)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_DOC_CAPITAL_LOC,P8006_DOC_INTERES_LOC,P8006_TOTAL_LOC'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7799827312082037183)
,p_name=>'Deshabilitar items'
,p_event_sequence=>170
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799827447581037184)
,p_event_id=>wwv_flow_api.id(7799827312082037183)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_MON_DESC'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7832322182386480585)
,p_event_id=>wwv_flow_api.id(7799827312082037183)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_TIP_GEN_CUO'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7832322610000480589)
,p_event_id=>wwv_flow_api.id(7799827312082037183)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_TIP_VENC,P8006_S_DIAS_ENTRE_CUOTAS,P8006_INTERES,P8006_S_FEC_PRIM_VTO'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7839570544559719881)
,p_event_id=>wwv_flow_api.id(7799827312082037183)
,p_event_result=>'TRUE'
,p_action_sequence=>70
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_S_ACT_DESC,P8006_BCO_DESC,P8006_CTA_DESC,P8006_NOMBRE_CODEUDOR,P8006_DOC_NRO_CREDITO_PREST,P8006_S_MON_DESC2,P8006_DOC_INTERES_MON_CUO,P8006_DOC_INTERES_MON,P8006_TOTAL_MON'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7839571322736719889)
,p_event_id=>wwv_flow_api.id(7799827312082037183)
,p_event_result=>'TRUE'
,p_action_sequence=>80
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_TOTALES,P8006_S_MON_SIMBOLO2,P8006_S_TOT_IMP_CAP,P8006_S_TOT_IMP_INT,P8006_S_TOT_IMP_CUO,P8006_DOC_CAPITAL_MON'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7799827525444037185)
,p_name=>'validar tasa oficial'
,p_event_sequence=>180
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_S_TASA_OFIC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799827652706037186)
,p_event_id=>wwv_flow_api.id(7799827525444037185)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P8006_S_TASA_OFIC IS NOT NULL THEN',
'  :P8006_DOC_NETO_EXEN_LOC := ROUND(NVL(:P8006_DOC_NETO_EXEN_MON,0) * NVL(:P8006_S_TASA_OFIC,1), 0);',
'  :P8006_DOC_NETO_GRAV_LOC := ROUND(NVL(:P8006_DOC_NETO_GRAV_MON,0) * NVL(:P8006_S_TASA_OFIC,1), 0);',
'  :P8006_DOC_IVA_LOC  := ROUND(NVL(:P8006_DOC_IVA_MON, 0) * NVL(:P8006_S_TASA_OFIC,1), 0);',
'END IF;',
''))
,p_attribute_02=>'P8006_S_TASA_OFIC,P8006_DOC_NETO_EXEN_MON,P8006_DOC_IVA_MON'
,p_attribute_03=>'P8006_DOC_NETO_EXEN_LOC,P8006_DOC_NETO_GRAV_LOC,P8006_DOC_IVA_LOC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7799828428826037194)
,p_name=>'validar_prestamo'
,p_event_sequence=>190
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_ACT_PRESTAMO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799828526356037195)
,p_event_id=>wwv_flow_api.id(7799828428826037194)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    PROCEDURE PP_TRAER_DESC_RAMO IS',
'    BEGIN',
'      IF :P8006_DOC_ACT_PRESTAMO IS NOT NULL THEN',
'         SELECT R.RAMO_DESC',
'         INTO   :P8006_S_ACT_DESC',
'             FROM FIN_CLIENTE C, ',
'                  FIN_SECTOR_PROD S,',
'                  FIN_RAMO R',
'             WHERE C.CLI_SECTOR_PROD = S.SEPR_CODIGO',
'               AND S.SEPR_CODIGO     = R.RAMO_SEC_PRODUCCION',
'               AND C.CLI_CODIGO      = :P8006_S_CUENTA',
'               AND R.RAMO_CODIGO     = :P8006_DOC_ACT_PRESTAMO',
'               AND R.RAMO_EMPR       = :P_EMPRESA',
'               AND R.RAMO_EMPR       = S.SEPR_EMPR',
'               AND S.SEPR_EMPR       = C.CLI_EMPR; ',
'      END IF;',
'    --',
'    EXCEPTION',
'      WHEN NO_DATA_FOUND THEN',
'         :P8006_S_ACT_DESC := NULL;',
'         RAISE_APPLICATION_ERROR(-20010,''Ramo de actividad inexistente o no corresponde al sector definido para el cliente..!'');',
'      ',
'      WHEN OTHERS THEN',
'         NULL;',
'',
'    END;',
'BEGIN',
'IF  :P8006_DOC_ACT_PRESTAMO IS NOT NULL THEN',
'  :P8006_S_ACT_DESC := NULL;',
'ELSE',
'  PP_TRAER_DESC_RAMO;',
'END IF;',
'END;'))
,p_attribute_02=>'P8006_DOC_ACT_PRESTAMO,P8006_S_CUENTA'
,p_attribute_03=>'P8006_S_ACT_DESC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7799828562160037196)
,p_name=>'valida desc act'
,p_event_sequence=>200
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_S_ACT_DESC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799828745624037197)
,p_event_id=>wwv_flow_api.id(7799828562160037196)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  IF   :P8006_S_CUENTA IS NULL',
'   AND :P8006_DOC_CLI_NOM IS NULL',
'  THEN',
'     RAISE_APPLICATION_ERROR(-20010,''Datos del Cliente/Proveedor deben ingresarse!'');',
'  END IF;',
'END;'))
,p_attribute_02=>'P8006_S_CUENTA,P8006_DOC_CLI_NOM'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7799828828081037198)
,p_name=>'validar_ruc'
,p_event_sequence=>210
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_CLI_RUC'
,p_bind_type=>'bind'
,p_bind_event_type=>'focusout'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799828932446037199)
,p_event_id=>wwv_flow_api.id(7799828828081037198)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  IF :P8006_DOC_CLI_RUC IS NULL THEN',
'    RAISE_APPLICATION_ERROR(-20010,''No puede quedar en blanco'');',
'  ELSE',
'    IF NOT FINI034.FL_RUC_VALIDO(:P8006_DOC_CLI_RUC) THEN',
'      --PL_EMITIR_SONIDO;',
'      RAISE_APPLICATION_ERROR(-20010,''AL_RUC'');',
'      /*IF V_ALERT = ALERT_BUTTON2 THEN',
'        RAISE FORM_TRIGGER_FAILURE;',
'      END IF;*/',
'    END IF;',
'    --NEXT_ITEM;',
'  END IF;',
'END;'))
,p_attribute_02=>'P8006_DOC_CLI_RUC'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7799829032341037200)
,p_name=>'validar_codeudor'
,p_event_sequence=>220
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_CLI_CODEUDOR'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7799829075560037201)
,p_event_id=>wwv_flow_api.id(7799829032341037200)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'IF :P8006_DOC_CLI_CODEUDOR IS NOT NULL THEN ',
'	 FINI034.PP_TRAER_DESC_CLI_CODEUDOR( I_EMPRESA            =>  :P_EMPRESA,',
'                                         I_DOC_CLI_CODEUDOR   => :P8006_DOC_CLI_CODEUDOR,',
'                                         O_NOMBRE_COD         =>  :P8006_NOMBRE_CODEUDOR);',
'	 IF :P8006_DOC_CLI_CODEUDOR = :P8006_S_CUENTA THEN ',
unistr('	 		RAISE_APPLICATION_ERROR(-20010,''El codigo de codeudor no puede ser igual al c\00F3digo de clliente!'');'),
'	 END IF;',
'ELSE',
'	 NULL;',
'	 --:DOC_CLI_NOM := NULL;',
'END IF;',
'END;'))
,p_attribute_02=>'P8006_DOC_CLI_CODEUDOR,P8006_S_CUENTA'
,p_attribute_03=>'P8006_NOMBRE_CODEUDOR'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7800973217413239479)
,p_name=>'validar_nro_credito'
,p_event_sequence=>230
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_NRO_CREDITO_PREST'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7800973334655239480)
,p_event_id=>wwv_flow_api.id(7800973217413239479)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'FINI034.PP_VALIDAR_NRO_PRESTAMO (I_EMPRESA     => :P_EMPRESA,',
'                                 I_TIPO_MOV    => :P8006_DOC_NRO_DOC,',
'                                 I_DOC_NRO_DOC => :P8006_DOC_NRO_CREDITO_PREST);',
'',
'END;'))
,p_attribute_02=>'P8006_DOC_TIPO_MOV,P8006_DOC_NRO_DOC'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7800973398894239481)
,p_name=>'cambio_doc_capital_mon'
,p_event_sequence=>240
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_CAPITAL_MON'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7800973505120239482)
,p_event_id=>wwv_flow_api.id(7800973398894239481)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF NVL(:P8006_DOC_INTERES_MON,0)  > NVL(:P8006_DOC_CAPITAL_MON,0)  THEN ',
'	:P8006_DOC_INTERES_MON :=0 ;',
'END IF;',
''))
,p_attribute_02=>'P8006_DOC_INTERES_MON,P8006_DOC_CAPITAL_MON'
,p_attribute_03=>'P8006_DOC_INTERES_MON'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
end;
/
begin
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7800973648156239483)
,p_name=>'cambio_dias_cuotas'
,p_event_sequence=>250
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_S_DIAS_ENTRE_CUOTAS'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7800973726224239484)
,p_event_id=>wwv_flow_api.id(7800973648156239483)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P8006_TIP_VENC = ''D'' and :P8006_TIP_GEN_CUO <> ''MA'' THEN',
'	 IF :P8006_S_DIAS_ENTRE_CUOTAS IS NULL THEN',
unistr('	 	  RAISE_APPLICATION_ERROR(-20010,''Debe seleccionar la cant. de d\00EDas!'');'),
'	 ELSE',
'	 	  :P8006_S_DIAS_GRACIA:= 0;',
'--	 	  :BCUO_ENC.FEC_PRIM_VTO:=:DOC_FEC_DOC + :BCUO_ENC.S_DIAS_ENTRE_CUOTAS;',
'--      :BCUO_ENC.S_FEC_PRIM_VTO:=TO_CHAR(:BCUO_ENC.FEC_PRIM_VTO,''DD/MM/YYYY'');',
'	 ',
'	 ',
'	 END IF;',
'END IF;'))
,p_attribute_02=>'P8006_TIP_VENC,P8006_TIP_GEN_CUO,P8006_S_DIAS_ENTRE_CUOTAS'
,p_attribute_03=>'P8006_S_DIAS_GRACIA'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7800973802359239485)
,p_name=>'cambio_capital_loc'
,p_event_sequence=>260
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_CAPITAL_LOC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7800973908493239486)
,p_event_id=>wwv_flow_api.id(7800973802359239485)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':P8006_DOC_NETO_EXEN_LOC := NVL(:P8006_DOC_CAPITAL_LOC,0) + NVL(:P8006_DOC_INTERES_LOC,0);'
,p_attribute_02=>'P8006_DOC_CAPITAL_LOC,P8006_DOC_INTERES_LOC'
,p_attribute_03=>'P8006_DOC_NETO_EXEN_LOC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7800973979939239487)
,p_name=>'cambio_interes_loc'
,p_event_sequence=>270
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_INTERES_LOC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7800974133374239488)
,p_event_id=>wwv_flow_api.id(7800973979939239487)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':P8006_DOC_NETO_EXEN_LOC := NVL(:P8006_DOC_CAPITAL_LOC,0) + NVL(:P8006_DOC_INTERES_LOC,0);'
,p_attribute_02=>'P8006_DOC_CAPITAL_LOC,P8006_DOC_INTERES_LOC'
,p_attribute_03=>'P8006_DOC_NETO_EXEN_LOC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7800974198286239489)
,p_name=>'cambio_total_loc'
,p_event_sequence=>280
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_TOTAL_LOC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7800974334398239490)
,p_event_id=>wwv_flow_api.id(7800974198286239489)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  IF :P8006_DOC_NETO_EXEN_MON < 0 THEN',
'    RAISE_APPLICATION_ERROR(-20010,''No puede ser negativo!'');',
'  END IF;',
'END;'))
,p_attribute_02=>'P8006_DOC_NETO_EXEN_MON'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7800974361648239491)
,p_name=>'total_loc_nulo'
,p_event_sequence=>290
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_TOTAL_LOC'
,p_condition_element=>'P8006_TOTAL_LOC'
,p_triggering_condition_type=>'NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7800974497032239492)
,p_event_id=>wwv_flow_api.id(7800974361648239491)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  :P8006_DOC_NETO_EXEN_MON := 0;',
''))
,p_attribute_03=>'P8006_DOC_NETO_EXEN_MON'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7800974680032239494)
,p_name=>'total_loc_lose_focus'
,p_event_sequence=>300
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_TOTAL_LOC'
,p_bind_type=>'bind'
,p_bind_event_type=>'focusout'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7800974816688239495)
,p_event_id=>wwv_flow_api.id(7800974680032239494)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':P8006_DOC_NETO_EXEN_LOC := ROUND(NVL(:P8006_DOC_NETO_EXEN_MON,0) * NVL(:P8006_S_TASA_OFIC,1), 0);'
,p_attribute_02=>'P8006_DOC_NETO_EXEN_MON,P8006_S_TASA_OFIC'
,p_attribute_03=>'P8006_DOC_NETO_EXEN_LOC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7800975061264239498)
,p_name=>'cambio interes'
,p_event_sequence=>310
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_INTERES'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7800975225349239499)
,p_event_id=>wwv_flow_api.id(7800975061264239498)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P8006_INTERES < 0 OR :P8006_INTERES >= 100 THEN',
unistr('	 RAISE_APPLICATION_ERROR(-20010,''No corresponde al rango de %inter\00E9s'');'),
'END IF;'))
,p_attribute_02=>'P8006_INTERES'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7800975290409239500)
,p_name=>'interes lose focus'
,p_event_sequence=>320
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_INTERES'
,p_bind_type=>'bind'
,p_bind_event_type=>'focusout'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7800975450352239501)
,p_event_id=>wwv_flow_api.id(7800975290409239500)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P8006_INTERES < 0 OR :P8006_INTERES >= 100 THEN',
unistr('	 RAISE_APPLICATION_ERROR(-20010,''No corresponde al rango de %inter\00E9s'');'),
'END IF;',
'IF :P8006_INTERES IS NULL OR :P8006_INTERES = 0 THEN',
'	RAISE_APPLICATION_ERROR(-20010,''El % de interes debe ser mayor a 0..!'');',
' --:BCUO_ENC.INTERES:=0;',
'END IF; '))
,p_attribute_02=>'P8006_INTERES'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7800975480279239502)
,p_name=>'valida_s_dias_gracia'
,p_event_sequence=>330
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_S_DIAS_GRACIA'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7800975579447239503)
,p_event_id=>wwv_flow_api.id(7800975480279239502)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P8006_S_DIAS_GRACIA < 0 THEN',
unistr('	 RAISE_APPLICATION_ERROR(-20010,''No puede ingresar D\00EDas de gracia negativo'');'),
'END IF;'))
,p_attribute_02=>'P8006_S_DIAS_GRACIA'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7800975785037239505)
,p_name=>'validar_fecha_desembolso'
,p_event_sequence=>340
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_S_FEC_DESEMBOLSO'
,p_condition_element=>'P8006_S_FEC_DESEMBOLSO'
,p_triggering_condition_type=>'NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7800975938572239506)
,p_event_id=>wwv_flow_api.id(7800975785037239505)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'',
'IF :P8006_S_FEC_desembolso IS NULL THEN',
'  :P8006_FEC_DESEMBOLSO := to_date(SYSDATE,''dd/mm/yyyy'');',
'  :P8006_S_FEC_DESEMBOLSO := to_char(SYSDATE,''dd/mm/yyyy'');',
'END IF;',
'',
'--AF_VALIDAR_FECHA (''OVF'', ''BCUO_ENC.S_FEC_DESEMBOLSO'', NULL, :GLOBAL.FECHA_SISTEMA);',
''))
,p_attribute_03=>'P8006_S_FEC_DESEMBOLSO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7800976052514239507)
,p_name=>'validar2'
,p_event_sequence=>350
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_S_FEC_DESEMBOLSO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7800976079510239508)
,p_event_id=>wwv_flow_api.id(7800976052514239507)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':P8006_FEC_DESEMBOLSO := TO_DATE(:P8006_S_FEC_DESEMBOLSO, ''DD/MM/YYYY'');'
,p_attribute_02=>'P8006_S_FEC_DESEMBOLSO'
,p_attribute_03=>'P8006_FEC_DESEMBOLSO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7800976292608239510)
,p_name=>'lose_focus_prim_venc'
,p_event_sequence=>360
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_S_FEC_PRIM_VTO_ORIG'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7800976396128239511)
,p_event_id=>wwv_flow_api.id(7800976292608239510)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P8006_S_FEC_PRIM_VTO IS NULL THEN',
'  RAISE_APPLICATION_ERROR(-20010,''El elemento no puede quedar en blanco!'');',
'END IF;'))
,p_attribute_02=>'P8006_S_FEC_PRIM_VTO'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7800976921619239516)
,p_name=>'generar_cuota'
,p_event_sequence=>370
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(7797045481050101689)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7800976967584239517)
,p_event_id=>wwv_flow_api.id(7800976921619239516)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--SET_ITEM_PROPERTY(''BCUO.S_IMP_CUO'',ENABLED,PROPERTY_FALSE);',
'--SET_ITEM_PROPERTY(''BCUO.S_IMP_CUO'',NAVIGABLE,PROPERTY_FALSE);',
'FINI034.PP_INICIAR_COLECCION_CUOT;',
'',
'IF :P8006_TIP_GEN_CUO = ''F'' THEN --GENERA EN EL SISTEMA FRANCES',
'	--break;',
'	FINI034.PP_GENERAR_CUO_FRANCES(I_FEC_PRIM_VTO		 => to_date(:P8006_S_FEC_PRIM_VTO,''dd/mm/yyyy''),',
'                                   I_S_DIAS_ENTRE_CUOTAS => :P8006_S_DIAS_ENTRE_CUOTAS,',
'                                   I_INTERES			 => :P8006_INTERES,',
'                                   I_S_PLAZO			 => :P8006_S_PLAZO,',
'                                   I_S_DIAS_GRACIA		 => :P8006_S_DIAS_GRACIA,',
'                                   I_DOC_CAPITAL_MON	 => :P8006_DOC_CAPITAL_MON,',
'                                   I_P_MON_SIMB			 => :P8006_MON_SIMBOLO,',
'                                   I_TIP_VENC			 => :P8006_TIP_VENC);',
'   --go_item(''bpie.ACEPTAR'');',
'ELSIF :P8006_TIP_GEN_CUO = ''A'' THEN',
'	  FINI034.PP_GENERAR_CUO_ALEMAN( I_FEC_PRIM_VTO		   => to_date(:P8006_S_FEC_PRIM_VTO,''dd/mm/yyyy''),',
'                                     I_FEC_DESEMBOLSO	   => to_date(:P8006_FEC_DESEMBOLSO,''dd/mm/yyyy''),',
'                                     I_S_DIAS_ENTRE_CUOTAS => :P8006_S_DIAS_ENTRE_CUOTAS,',
'                                     I_INTERES			   => :P8006_INTERES,',
'                                     I_S_PLAZO			   => :P8006_S_PLAZO,',
'                                     I_S_DIAS_GRACIA	   => :P8006_S_DIAS_GRACIA,',
'                                     I_DOC_CAPITAL_MON	   => :P8006_DOC_CAPITAL_MON,',
'                                     I_P_MON_SIMB		   => :P8006_MON_SIMBOLO);  --GENERA EN EL SISTEMA ALEMAN',
'      --go_item(''bpie.ACEPTAR'');',
'ELSE',
'   FINI034.PP_GENERAR_CUO_FRANCES_ESPEC(I_FEC_PRIM_VTO		  => to_date(:P8006_S_FEC_PRIM_VTO,''dd/mm/yyyy''),',
'                                        I_FEC_DESEMBOLSO	  => to_date(:P8006_FEC_DESEMBOLSO,''dd/mm/yyyy''),',
'                                        I_S_DIAS_ENTRE_CUOTAS => :P8006_S_DIAS_ENTRE_CUOTAS,',
'                                        I_INTERES			  => :P8006_INTERES,',
'                                        I_S_PLAZO			  => :P8006_S_PLAZO,',
'                                        I_S_DIAS_GRACIA		  => :P8006_S_DIAS_GRACIA,',
'                                        I_DOC_CAPITAL_MON	  => :P8006_DOC_CAPITAL_MON,',
'                                        I_P_MON_SIMB		  => :P8006_MON_SIMBOLO,',
'                                        I_S_CANT_CUO		  => :P8006_S_CANT_CUO,',
'                                        I_S_MONTO_CUO		  => :P8006_S_MONTO_CUO,',
'                                        I_TIP_VENC			  => :P8006_TIP_VENC) ; --GENERA EN EL SISTEMA FRANCES ESPECIAL',
'   --SET_ITEM_PROPERTY(''BCUO.S_IMP_CUO'',ENABLED,PROPERTY_TRUE);',
'   --SET_ITEM_PROPERTY(''BCUO.S_IMP_CUO'',NAVIGABLE,PROPERTY_TRUE);',
'END IF;',
'',
'select sum(to_number(C006)),',
'       sum(to_number(C003)),',
'       sum(to_number(C004)),',
'       count(*)',
'into  :P8006_S_TOT_IMP_CAP,',
'      :P8006_S_TOT_IMP_INT,',
'      :P8006_S_TOT_IMP_CUO,',
'      :P8006_CANT_PAGARES',
'from apex_collections',
'where collection_name = ''FINI034_CUOTAS'';',
'',
'--:P8006_S_TOT_IMP_CAP := :P8006_DOC_CAPITAL_MON;',
'',
':P8006_DOC_INTERES_MON_CUO := :P8006_S_TOT_IMP_INT;',
'',
'FINI034.PP_AJUSTAR_DIF(I_S_TOT_IMP_CAP     => :P8006_S_TOT_IMP_CAP,',
'                       I_DOC_CAPITAL_MON   => :P8006_DOC_CAPITAL_MON);'))
,p_attribute_02=>'P8006_TIP_GEN_CUO,P8006_S_FEC_PRIM_VTO,P8006_S_DIAS_ENTRE_CUOTAS,P8006_INTERES,P8006_S_PLAZO,P8006_S_DIAS_GRACIA,P8006_DOC_CAPITAL_MON,P8006_MON_SIMBOLO,P8006_TIP_VENC,P8006_FEC_DESEMBOLSO,P8006_S_CANT_CUO,P8006_S_MONTO_CUO,P8006_S_TOT_IMP_CAP'
,p_attribute_03=>'P8006_S_TOT_IMP_CAP,P8006_S_TOT_IMP_INT,P8006_S_TOT_IMP_CUO,P8006_DOC_INTERES_MON_CUO,P8006_CANT_PAGARES'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7800978004071239527)
,p_event_id=>wwv_flow_api.id(7800976921619239516)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(7797046598452101700)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7830258718475414286)
,p_name=>'lose_focus_nro_soli'
,p_event_sequence=>390
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_S_NRO_SOLIC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7830258763871414287)
,p_event_id=>wwv_flow_api.id(7830258718475414286)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'        PROCEDURE PP_TRAER_CODEUDOR IS',
'        BEGIN',
'            IF :P8006_P_GARANTE IS NOT NULL THEN',
'                SELECT CLI_CODIGO, ',
'                       CLI_NOM',
'                INTO :P8006_DOC_CLI_CODEUDOR, ',
'                     :P8006_NOMBRE_CODEUDOR',
'                FROM FIN_CLIENTE',
'                WHERE CLI_PERSONA= :P8006_P_GARANTE',
'                  AND CLI_EMPR = :P_EMPRESA;',
'            END IF;	',
'',
'            EXCEPTION',
'          WHEN NO_DATA_FOUND THEN',
unistr('            RAISE_APPLICATION_ERROR(-20010,''El Codeudor relacionado a la Solicitud de Cr\00E9dito no esta registrado como Cliente..!'');'),
'          WHEN TOO_MANY_ROWS THEN',
'            :P8006_MOSTRAR_VENTANA := 1;',
'            --PP_EJECUTAR_CONSULTA;  ',
'            --se ejecuta un refresh en la region ',
'          ',
'          WHEN OTHERS THEN',
'            NULL;',
'         END;  ',
'BEGIN',
':P8006_MOSTRAR_VENTANA := 0;',
'',
'IF :P8006_S_NRO_SOLIC IS NOT NULL THEN',
'	FINI034.PP_VALIDAR_SOLICITUD (  I_S_CUENTA		      => :P8006_S_CUENTA,',
'                                    I_S_NRO_SOLIC	      => :P8006_S_NRO_SOLIC,',
'                                    I_S_FEC_PRIM_VTO      => :P8006_S_FEC_PRIM_VTO,',
'                                    I_S_MON_SIMBOLO2      => :P8006_S_MON_SIMBOLO2,',
'                                    I_EMPRESA		      => :P_EMPRESA,',
'                                    I_DOC_FEC_OPER	      => to_date(:P8006_DOC_FEC_OPER,''dd/mm/yyyy''),',
'                                    I_tip_gen_cuo	      => :P8006_tip_gen_cuo,',
'                                    I_S_DOC_MONEDA	      => :P_MON_LOC,',
'                                    O_P_MON_US		      => :P8006_P_MON_US,',
'                                    O_P_MON_LOC		      => :P8006_P_MON_LOC,',
'                                    O_P_IND_OPERADOR      => :P8006_P_IND_OPERADOR,',
'                                    O_P_FORMATO		      => :P8006_P_FORMATO,',
'                                    O_P_VALIDAD_PE_ES     => :P8006_P_VALIDAD_PE_ES,',
'                                    O_P_MONEDA		      => :P8006_P_MONEDA,',
'                                    O_DOC_CLAVE_SOL       => :P8006_DOC_CLAVE_SOL,',
'                                    O_DOC_CAPITAL_MON     => :P8006_DOC_CAPITAL_MON,',
'                                    O_P_GARANTE			  => :P8006_P_GARANTE,',
'                                    O_P_INTERES			  => :P8006_P_INTERES,',
'                                    O_TIP_VENC			  => :P8006_TIP_VENC,',
'                                    O_s_dias_entre_cuotas => :P8006_s_dias_entre_cuotas,',
'                                    O_INTERES			  => :P8006_INTERES,',
'                                    O_S_DIAS_GRACIA		  => :P8006_S_DIAS_GRACIA,',
'                                    O_FEC_PRIM_VTO	      => :P8006_FEC_PRIM_VTO,',
'                                    O_S_FEC_PRIM_VTO	  => :P8006_S_FEC_PRIM_VTO,',
'                                    O_TIP_GEN_CUO		  => :P8006_TIP_GEN_CUO,',
'                                    O_S_PLAZO			  => :P8006_S_PLAZO,',
'                                    O_S_MONTO_CUO		  => :P8006_S_MONTO_CUO, ',
'                                    O_S_CANT_CUO		  => :P8006_S_CANT_CUO,',
'                                    O_S_DOC_MONEDA		  => :P8006_S_DOC_MONEDA,',
'                                    O_S_MON_DESC2		  => :P8006_S_MON_DESC2,',
'                                    O_S_MON_SIMBOLO2      => :P8006_S_MON_SIMBOLO2,',
'                                    O_P_MON_SIMB          => :P8006_P_MON_SIMB,',
'                                    O_DOC_MON             => :P8006_DOC_MON);',
'	PP_TRAER_CODEUDOR;',
'END IF;	',
'END;'))
,p_attribute_02=>'P8006_S_CUENTA,P8006_S_NRO_SOLIC,P8006_S_FEC_PRIM_VTO,P8006_S_MON_SIMBOLO2,P8006_DOC_FEC_OPER,P8006_TIP_GEN_CUO'
,p_attribute_03=>'P8006_P_MON_US,P8006_P_MON_LOC,P8006_P_IND_OPERADOR,P8006_P_FORMATO,P8006_P_VALIDAD_PE_ES,P8006_P_MONEDA,P8006_DOC_CLAVE_SOL,P8006_DOC_CAPITAL_MON,P8006_P_GARANTE,P8006_P_INTERES,P8006_TIP_VENC,P8006_S_DIAS_ENTRE_CUOTAS,P8006_INTERES,P8006_S_DIAS_GRA'
||'CIA,P8006_FEC_PRIM_VTO,P8006_S_FEC_PRIM_VTO,P8006_TIP_GEN_CUO,P8006_S_PLAZO,P8006_S_MONTO_CUO,P8006_S_CANT_CUO,P8006_S_DOC_MONEDA,P8006_S_MON_DESC2,P8006_S_MON_SIMBOLO2,P8006_P_MON_SIMB,P8006_DOC_MON,P8006_DOC_CLI_CODEUDOR,P8006_NOMBRE_CODEUDOR,P8006'
||'_MOSTRAR_VENTANA'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7839572005425719896)
,p_event_id=>wwv_flow_api.id(7830258718475414286)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'        PROCEDURE PP_TRAER_CODEUDOR IS',
'        BEGIN',
'            IF :P8006_P_GARANTE IS NOT NULL THEN',
'                SELECT CLI_CODIGO, ',
'                       CLI_NOM',
'                INTO :P8006_DOC_CLI_CODEUDOR, ',
'                     :P8006_NOMBRE_CODEUDOR',
'                FROM FIN_CLIENTE',
'                WHERE CLI_PERSONA= :P8006_P_GARANTE',
'                  AND CLI_EMPR = :P_EMPRESA;',
'            END IF;	',
'',
'            EXCEPTION',
'          WHEN NO_DATA_FOUND THEN',
unistr('            RAISE_APPLICATION_ERROR(-20010,''El Codeudor relacionado a la Solicitud de Cr\00E9dito no esta registrado como Cliente..!'');'),
'          WHEN TOO_MANY_ROWS THEN',
'            :P8006_MOSTRAR_VENTANA := 1;',
'            --PP_EJECUTAR_CONSULTA;  ',
'            --se ejecuta un refresh en la region ',
'          ',
'          WHEN OTHERS THEN',
'            NULL;',
'         END;  ',
'BEGIN',
':P8006_MOSTRAR_VENTANA := 0;',
'',
'IF :P8006_S_NRO_SOLIC IS NOT NULL THEN',
'	FINI034.PP_VALIDAR_SOLICITUD (  I_S_CUENTA		      => :P8006_S_CUENTA,',
'                                    I_S_NRO_SOLIC	      => :P8006_S_NRO_SOLIC,',
'                                    I_S_FEC_PRIM_VTO      => :P8006_S_FEC_PRIM_VTO,',
'                                    I_S_MON_SIMBOLO2      => :P8006_S_MON_SIMBOLO2,',
'                                    I_EMPRESA		      => :P_EMPRESA,',
'                                    I_DOC_FEC_OPER	      => to_date(:P8006_DOC_FEC_OPER,''dd/mm/yyyy''),',
'                                    I_tip_gen_cuo	      => :P8006_tip_gen_cuo,',
'                                    I_S_DOC_MONEDA	      => :P_MON_LOC,',
'                                    O_P_MON_US		      => :P8006_P_MON_US,',
'                                    O_P_MON_LOC		      => :P8006_P_MON_LOC,',
'                                    O_P_IND_OPERADOR      => :P8006_P_IND_OPERADOR,',
'                                    O_P_FORMATO		      => :P8006_P_FORMATO,',
'                                    O_P_VALIDAD_PE_ES     => :P8006_P_VALIDAD_PE_ES,',
'                                    O_P_MONEDA		      => :P8006_P_MONEDA,',
'                                    O_DOC_CLAVE_SOL       => :P8006_DOC_CLAVE_SOL,',
'                                    O_DOC_CAPITAL_MON     => :P8006_DOC_CAPITAL_MON,',
'                                    O_P_GARANTE			  => :P8006_P_GARANTE,',
'                                    O_P_INTERES			  => :P8006_P_INTERES,',
'                                    O_TIP_VENC			  => :P8006_TIP_VENC,',
'                                    O_s_dias_entre_cuotas => :P8006_s_dias_entre_cuotas,',
'                                    O_INTERES			  => :P8006_INTERES,',
'                                    O_S_DIAS_GRACIA		  => :P8006_S_DIAS_GRACIA,',
'                                    O_FEC_PRIM_VTO	      => :P8006_FEC_PRIM_VTO,',
'                                    O_S_FEC_PRIM_VTO	  => :P8006_S_FEC_PRIM_VTO,',
'                                    O_TIP_GEN_CUO		  => :P8006_TIP_GEN_CUO,',
'                                    O_S_PLAZO			  => :P8006_S_PLAZO,',
'                                    O_S_MONTO_CUO		  => :P8006_S_MONTO_CUO, ',
'                                    O_S_CANT_CUO		  => :P8006_S_CANT_CUO,',
'                                    O_S_DOC_MONEDA		  => :P8006_S_DOC_MONEDA,',
'                                    O_S_MON_DESC2		  => :P8006_S_MON_DESC2,',
'                                    O_S_MON_SIMBOLO2      => :P8006_S_MON_SIMBOLO2,',
'                                    O_P_MON_SIMB          => :P8006_P_MON_SIMB,',
'                                    O_DOC_MON             => :P8006_DOC_MON);',
'	PP_TRAER_CODEUDOR;',
'END IF;	',
'END;'))
,p_attribute_02=>'P8006_S_CUENTA,P8006_S_NRO_SOLIC,P8006_S_FEC_PRIM_VTO,P8006_S_MON_SIMBOLO2,P8006_DOC_FEC_OPER,P8006_TIP_GEN_CUO'
,p_attribute_03=>'P8006_P_MON_US,P8006_P_MON_LOC,P8006_P_IND_OPERADOR,P8006_P_FORMATO,P8006_P_VALIDAD_PE_ES,P8006_P_MONEDA,P8006_DOC_CLAVE_SOL,P8006_DOC_CAPITAL_MON,P8006_P_GARANTE,P8006_P_INTERES,P8006_TIP_VENC,P8006_S_DIAS_ENTRE_CUOTAS,P8006_INTERES,P8006_S_DIAS_GRA'
||'CIA,P8006_FEC_PRIM_VTO,P8006_S_FEC_PRIM_VTO,P8006_TIP_GEN_CUO,P8006_S_PLAZO,P8006_S_MONTO_CUO,P8006_S_CANT_CUO,P8006_S_DOC_MONEDA,P8006_S_MON_DESC2,P8006_S_MON_SIMBOLO2,P8006_P_MON_SIMB,P8006_DOC_MON,P8006_DOC_CLI_CODEUDOR,P8006_NOMBRE_CODEUDOR,P8006'
||'_MOSTRAR_VENTANA'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7830258964726414289)
,p_name=>'mostrar/ocultar dias'
,p_event_sequence=>400
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_S_NRO_SOLIC'
,p_condition_element=>'P8006_TIP_VENC'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'M'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7830259061083414290)
,p_event_id=>wwv_flow_api.id(7830258964726414289)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_S_DIAS_ENTRE_CUOTAS'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7830259202725414291)
,p_event_id=>wwv_flow_api.id(7830258964726414289)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_S_DIAS_ENTRE_CUOTAS'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7832108307597899611)
,p_name=>'mostrar_ventana'
,p_event_sequence=>410
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_MOSTRAR_VENTANA'
,p_condition_element=>'P8006_MOSTRAR_VENTANA'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'1'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7832108914490899617)
,p_event_id=>wwv_flow_api.id(7832108307597899611)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(7830260517564414304)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7832108442780899612)
,p_event_id=>wwv_flow_api.id(7832108307597899611)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_OPEN_REGION'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(7830260364754414303)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7832108693141899615)
,p_name=>'cerrar_modal'
,p_event_sequence=>420
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(7832108572808899614)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7832108854965899616)
,p_event_id=>wwv_flow_api.id(7832108693141899615)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_CLOSE_REGION'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(7830260364754414303)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7832109005731899618)
,p_name=>'PROCESS_CHEQUES'
,p_event_sequence=>430
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(7797047024069101704)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7832109112585899619)
,p_event_id=>wwv_flow_api.id(7832109005731899618)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'FINI034.PP_CHEQ_EMIT_DIF (I_EMPRESA             => :P_EMPRESA,',
'                          I_DOC_CLI_NOM         => :P8006_DOC_CLI_NOM,',
'                          I_DOC_CAPITAL_MON     => :P8006_DOC_CAPITAL_MON,',
'                          I_DOC_CTA_BCO         => :P8006_DOC_CTA_BCO,',
'                          O_M_BENEFICIARIO_INI  => :P8006_M_BENEFICIARIO_INI,',
'                          O_S_TOTAL_IMP         => :P8006_S_TOTAL_IMP);'))
,p_attribute_02=>'P8006_DOC_CLI_NOM,P8006_DOC_CAPITAL_MON,P8006_DOC_CTA_BCO'
,p_attribute_03=>'P8006_M_BENEFICIARIO_INI,P8006_S_TOTAL_IMP'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7832109504850899623)
,p_name=>'PROCESS ACEPTAR'
,p_event_sequence=>440
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(7797047078136101705)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7839570895013719885)
,p_event_id=>wwv_flow_api.id(7832109504850899623)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_MON_DESC'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7839571015748719886)
,p_event_id=>wwv_flow_api.id(7832109504850899623)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_TIP_GEN_CUO'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7839571102065719887)
,p_event_id=>wwv_flow_api.id(7832109504850899623)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_TIP_VENC,P8006_S_DIAS_ENTRE_CUOTAS,P8006_INTERES,P8006_S_FEC_PRIM_VTO'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7839571240569719888)
,p_event_id=>wwv_flow_api.id(7832109504850899623)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_S_ACT_DESC,P8006_BCO_DESC,P8006_CTA_DESC,P8006_NOMBRE_CODEUDOR,P8006_DOC_NRO_CREDITO_PREST,P8006_S_MON_DESC2,P8006_DOC_INTERES_MON_CUO,P8006_DOC_INTERES_MON,P8006_TOTAL_MON'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7839571446218719890)
,p_event_id=>wwv_flow_api.id(7832109504850899623)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_TOTALES,P8006_S_MON_SIMBOLO2,P8006_S_TOT_IMP_CAP,P8006_S_TOT_IMP_INT,P8006_S_TOT_IMP_CUO'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7845072937998005788)
,p_event_id=>wwv_flow_api.id(7832109504850899623)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_DOC_MON,P8006_DOC_CLI_TEL,P8006_DOC_CLI_RUC,P8006_DOC_CLI_DIR,P8006_DOC_CAPITAL_MON,P8006_S_TASA_OFIC,P8006_DOC_CLI_NOM'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7832109653684899624)
,p_event_id=>wwv_flow_api.id(7832109504850899623)
,p_event_result=>'TRUE'
,p_action_sequence=>70
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'Aceptar'
,p_attribute_02=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7924781688077417617)
,p_event_id=>wwv_flow_api.id(7832109504850899623)
,p_event_result=>'TRUE'
,p_action_sequence=>80
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'javascript:var myWindow = window.open(''f?p=100:3010:&SESSION.::::::'');'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7832109687251899625)
,p_name=>'calculo_total'
,p_event_sequence=>450
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_CAPITAL_MON,P8006_DOC_INTERES_MON_CUO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
end;
/
begin
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7832109811727899626)
,p_event_id=>wwv_flow_api.id(7832109687251899625)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
':P8006_TOTAL_MON := NVL(:P8006_DOC_CAPITAL_MON,0) + NVL(:P8006_DOC_INTERES_MON_CUO,0);',
'END;'))
,p_attribute_02=>'P8006_DOC_CAPITAL_MON,P8006_DOC_INTERES_MON_CUO'
,p_attribute_03=>'P8006_TOTAL_MON'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7832109948175899627)
,p_name=>'CALCULAR_TOTAL_LOC'
,p_event_sequence=>460
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_TOTAL_MON,P8006_S_TASA_OFIC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7832110042590899628)
,p_event_id=>wwv_flow_api.id(7832109948175899627)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
':P8006_TOTAL_LOC := ROUND(NVL(:P8006_TOTAL_MON,0) * NVL(:P8006_S_TASA_OFIC,1), 0);',
'END;'))
,p_attribute_02=>'P8006_TOTAL_MON,P8006_S_TASA_OFIC'
,p_attribute_03=>'P8006_TOTAL_LOC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7832321595827480579)
,p_name=>'activar/desactivar plazo'
,p_event_sequence=>470
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_TIP_GEN_CUO'
,p_condition_element=>'P8006_TIP_GEN_CUO'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'F'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7832321658037480580)
,p_event_id=>wwv_flow_api.id(7832321595827480579)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_S_PLAZO'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7832321947534480582)
,p_event_id=>wwv_flow_api.id(7832321595827480579)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_S_PLAZO'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7832322272268480586)
,p_name=>'activar/desactivar fr_es'
,p_event_sequence=>480
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_TIP_GEN_CUO'
,p_condition_element=>'P8006_TIP_GEN_CUO'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'FE'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7832322399159480587)
,p_event_id=>wwv_flow_api.id(7832322272268480586)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_S_MONTO_CUO,P8006_S_CANT_CUO,P8006_LABEL'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7832322468921480588)
,p_event_id=>wwv_flow_api.id(7832322272268480586)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P8006_S_MONTO_CUO,P8006_S_CANT_CUO,P8006_LABEL'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7832322749746480590)
,p_name=>'fecha_oper_nula'
,p_event_sequence=>490
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_S_DOC_FEC_OPER'
,p_condition_element=>'P8006_S_DOC_FEC_OPER'
,p_triggering_condition_type=>'NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7832322869860480592)
,p_event_id=>wwv_flow_api.id(7832322749746480590)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':P8006_S_DOC_FEC_OPER := SYSDATE;'
,p_attribute_03=>'P8006_S_DOC_FEC_OPER'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7839570347775719879)
,p_name=>'fecha_doc_nula'
,p_event_sequence=>500
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_S_DOC_FEC_DOC'
,p_condition_element=>'P8006_S_DOC_FEC_DOC'
,p_triggering_condition_type=>'NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7839570417661719880)
,p_event_id=>wwv_flow_api.id(7839570347775719879)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':P8006_S_DOC_FEC_DOC := SYSDATE;'
,p_attribute_03=>'P8006_S_DOC_FEC_DOC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7839571645003719892)
,p_name=>'cambio_tipo_venc'
,p_event_sequence=>510
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_S_FEC_PRIM_VTO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7839571684985719893)
,p_event_id=>wwv_flow_api.id(7839571645003719892)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(7797046598452101700)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7839571911503719895)
,p_event_id=>wwv_flow_api.id(7839571645003719892)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--PROCEDIMIENTO PARA CALCULAR TOTALES',
'SELECT    SUM(C003),   --    S_IMP_INT,',
'          SUM(C004),   --    S_IMP_CUO,',
'          SUM(C006)    --    S_IMP_CAP',
'INTO     :P8006_S_TOT_IMP_INT,',
'         :P8006_S_TOT_IMP_CUO,',
'         :P8006_S_TOT_IMP_CAP',
'FROM APEX_COLLECTIONS',
'WHERE COLLECTION_NAME = ''FINI034_CUOTAS'';'))
,p_attribute_03=>'P8006_S_TOT_IMP_INT,P8006_S_TOT_IMP_CUO,P8006_S_TOT_IMP_CAP'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7852132904295293786)
,p_name=>'calculo_doc_interes_loc'
,p_event_sequence=>530
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_INTERES_MON_CUO,P8006_S_TASA_OFIC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7852132991958293787)
,p_event_id=>wwv_flow_api.id(7852132904295293786)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P8006_DOC_INTERESES_LOC := ROUND(NVL(:P8006_DOC_INTERES_MON_CUO,0) * NVL(:P8006_S_TASA_OFIC,1), 0);',
'',
''))
,p_attribute_02=>'P8006_DOC_INTERES_MON_CUO,P8006_S_TASA_OFIC'
,p_attribute_03=>'P8006_DOC_INTERESES_LOC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7943949654303278790)
,p_event_id=>wwv_flow_api.id(7852132904295293786)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P8006_DOC_IVA_INT_MON := ROUND(:P8006_DOC_INTERES_MON_CUO*NVL(:P8006_CONF_PORC_IVA_INT,0)/(100+NVL(:P8006_CONF_PORC_IVA_INT,0)),0);',
'',
':P8006_DOC_IVA_INT_LOC := ROUND((:P8006_DOC_INTERESES_LOC*NVL(:P8006_CONF_PORC_IVA_INT,0)) /(100+NVL(:P8006_CONF_PORC_IVA_INT,0)),0);'))
,p_attribute_02=>'P8006_DOC_INTERES_MON_CUO,P8006_CONF_PORC_IVA_INT,P8006_DOC_INTERESES_LOC'
,p_attribute_03=>'P8006_DOC_IVA_INT_MON,P8006_DOC_IVA_INT_LOC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7852133069726293788)
,p_name=>'calcular_capital_loc'
,p_event_sequence=>540
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_TOTAL_LOC,P8006_DOC_INTERES_LOC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7852133170310293789)
,p_event_id=>wwv_flow_api.id(7852133069726293788)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':P8006_DOC_CAPITAL_LOC := NVL(:P8006_TOTAL_LOC,0) - NVL(:P8006_DOC_INTERESES_LOC,0);'
,p_attribute_02=>'P8006_TOTAL_LOC,P8006_DOC_INTERES_LOC,P8006_DOC_INTERESES_LOC'
,p_attribute_03=>'P8006_DOC_CAPITAL_LOC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7906261337392026979)
,p_name=>'traer_documento'
,p_event_sequence=>550
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_NRO_DOC'
,p_condition_element=>'P8006_DOC_NRO_DOC'
,p_triggering_condition_type=>'NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7906261430902026980)
,p_event_id=>wwv_flow_api.id(7906261337392026979)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'  FINI034.PP_TRAER_SGTE_NRO_PRESTAMO(O_DOC_NRO_DOC => :P8006_DOC_NRO_DOC);'
,p_attribute_03=>'P8006_DOC_NRO_DOC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(7943949298708278787)
,p_name=>'cambio_neto_exen_mon'
,p_event_sequence=>560
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8006_DOC_NETO_EXEN_MON'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(7943949425192278788)
,p_event_id=>wwv_flow_api.id(7943949298708278787)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P8006_DOC_NETO_EXEN_LOC := ROUND(NVL(:P8006_DOC_NETO_EXEN_MON,0)',
'                         * NVL(:P8006_S_TASA_OFIC,1),',
'                          0);'))
,p_attribute_02=>'P8006_DOC_NETO_EXEN_MON,P8006_S_TASA_OFIC'
,p_attribute_03=>'P8006_DOC_NETO_EXEN_LOC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(7906261523487026981)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'limpiar items'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(7798932338280678579)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PP_CARGAR_DATOS'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'FINI034.PP_INICIAR_COLECCION_CUOT;',
'',
'DECLARE',
'	V_CTACO_PROV_INT NUMBER;',
'	V_MENSAJE VARCHAR2(150);',
'    ',
'    ',
'BEGIN',
' 		:P8006_P_FLAG_IVA := ''N'';',
' 		:P8006_P_ACT_SAL_CAP := ''N'';',
' 		:P8006_P_IND_REGENERANDO_CUO := ''N'';',
' 		:P8006_P_INTERES:=NULL;',
'        ',
'        --EN DURO',
'        :P8006_P_IMPRESORA := 1;',
' 		--------------------------------------------------------------------------------------------',
'       BEGIN ',
' 		SELECT IMP_ESTABLECIMIENTO, ',
' 			   IMP_PUNTO_EMISION, ',
' 			   IMP_ULT_NRO_PMOCONC',
' 		INTO   :P8006_IMP_ESTABLECIMIENTO, ',
'               :P8006_IMP_PUNTO_EMISION, ',
'               :P8006_IMP_ULT_NRO_PMOCONC 					 ',
' 		FROM   GEN_IMPRESORA',
' 		WHERE  --IMPR_IP = FP_IP_USER',
'               IMP_CODIGO = :P8006_P_IMPRESORA',
' 		  AND  IMP_EMPR   = :P_EMPRESA;',
'        EXCEPTION',
'        WHEN others THEN',
'            RAISE_APPLICATION_ERROR(-20010,''No existe una impresora configurada'');',
'        END;',
'		--------------------------------------------------------------------------------------------',
'	/*  BEGIN	',
'			    SELECT 	CONF_MON_US, 								CONF_MON_LOC,',
'			    				CONF_IND_OPERADOR, 					CONF_FORMATO_FACTURA, ',
'			    				CONF_VALIDAR_PE_ES',
'			    INTO 		:PARAMETER.P_MON_US, 				:PARAMETER.P_MON_LOC, ',
'			    				:PARAMETER.P_IND_OPERADOR,	:PARAMETER.P_FORMATO, ',
'			    				:PARAMETER.P_VALIDAD_PE_ES',
'			    FROM GEN_CONFIGURACION',
'			    WHERE CONF_EMPR = :PARAMETER.P_EMPRESA;',
'			    ',
'			    -----22/07/2019',
'			    --------TODAS LAS CONSULTAS ESTABAN UTILIZANDO COMO PARAMETRO ":PARAMETER.P_MON_LOC"',
'			    -------- YA QUE AHORA SE VA A UTILIZAR GUARANIES Y DOLARES, ',
'			     ------- SE CREA UN PARAMETRO MONEDA QUE DEPENDIENDO DEL TIPO DE MONEDA SERA UTILIZADO,',
'			     ------ESTA CONSULTA SE REALIZARA UNA VEZ ELEGIDO EL NRO DE SOLICITUD',
'			   ',
'	  EXCEPTION',
'		    WHEN NO_DATA_FOUND THEN',
unistr('			       MESSAGE(''No fue cargada la tabla de configuraci\00F3n general de sistemas!.'');PAUSE;'),
'			       DO_KEY(''EXIT'');',
'		    WHEN FORM_TRIGGER_FAILURE THEN',
'		       	RAISE FORM_TRIGGER_FAILURE;',
'		    WHEN OTHERS THEN',
'		       	PL_EXHIBIR_ERROR_PLSQL;',
'	  END; ',
'  	--------------------------------------------------------------------------------------------',
'		 SELECT MON_DEC_IMP',
'		   INTO  :PARAMETER.P_CANT_DECIMALES_LOC',
'		   FROM  GEN_MONEDA',
'		  WHERE  MON_CODIGO = :PARAMETER.P_MON_LOC',
'		    AND  MON_EMPR = :PARAMETER.P_EMPRESA;',
'		    ',
'		--------------------------------------------------------------------------------------------  ',
'		*/',
'		  SELECT  CONF_FEC_LIM_MOD,     			',
'                  CONF_PER_ACT_FIN,',
'			      CONF_PER_SGTE_INI,    			',
'                  CONF_PER_SGTE_FIN,',
'				  CONF_IND_HAB_MES_ACT, ',
'						 ------------------------------------------------------------------------------------',
'				  CONF_TMOV_PRCO,',
'				  CONF_TMOV_CAP_PRCO,',
'				  CONF_TMOV_INT_PRCO,',
'						 ------------------------------------------------------------------------------------',
'				  CONF_CONC_PRCO_CR,',
'		          CONF_CONC_INT_PRCO_DB,',
'		          CONF_CONC_INTC_PRCO_DB,',
'		          CONF_CLAVE_CTACO_INTC_PRCO,',
'				  CONF_CONC_IVA_PRCO_DB,',
'						 ------------------------------------------------------------------------------------',
'				  CONF_PORC_IVA_INT,',
'                  CONF_MON_LOC',
'						 ------------------------------------------------------------------------------------',
'		  INTO   :P8006_CONF_FEC_LIM_MOD, 	',
'                 :P8006_CONF_PER_ACT_FIN,',
'			     :P8006_CONF_PER_SGTE_INI,	',
'                 :P8006_CONF_PER_SGTE_FIN,',
'			     :P8006_CONF_IND_HAB_MES_ACT,		         ',
'		         ------------------------------------------------------------------------------------',
'		         :P8006_CONF_TMOV_PRCO						,			',
'		         :P8006_CONF_TMOV_CAP_PRCO				,	',
'		         :P8006_CONF_TMOV_INT_PRCO				,',
'		         ------------------------------------------------------------------------------------	       ',
'		         :P8006_CONF_CONC_PRCO_CR			,		         ',
'		         :P8006_CONF_CONC_INT_PRCO_DB	, ',
'		         :P8006_CONF_CONC_INTC_PRCO_DB,  ',
'		         :P8006_CF_CLAVE_CTACO_INTC_PRCO,  ',
'		         :P8006_CONF_CONC_IVA_PRCO_DB,',
'		         ------------------------------------------------------------------------------------',
'		         :P8006_CONF_PORC_IVA_INT,',
'                 :P_MON_LOC',
'		         ------------------------------------------------------------------------------------',
'		  FROM   FIN_CONFIGURACION',
'		  WHERE  CONF_EMPR = :P_EMPRESA;',
'		  ',
'		--------------------------------------------------------------------------------------------    ',
'		-----------------------//TIPOS DE MOVIMIENTOS\\----------------------',
'		',
'    IF :P8006_CONF_TMOV_PRCO IS NULL THEN 	 				',
'            RAISE_APPLICATION_ERROR(-20010,''Primero debe definir configurar tipo de mvto. PRESTAMO EMITIDO en el Mant. de Configuraciones(FINMM33).'');',
'            --DO_KEY(''EXIT'');	',
'    END IF;	',
'',
'    IF :P8006_CONF_TMOV_CAP_PRCO IS NULL THEN 	 ',
'            RAISE_APPLICATION_ERROR(-20010,''Primero debe definir configurar tipo de mvto. CAPITAL.PREST.EMIT. en el mant. de configuraciones(FINMM33).'');',
'            --DO_KEY(''EXIT'');	',
'    END IF;',
'    IF :P8006_CONF_TMOV_INT_PRCO IS NULL THEN 	 ',
'            RAISE_APPLICATION_ERROR(-20010,''Primero debe definir configurar tipo de mvto. INTERES.PREST.EMIT. en el mant. de configuraciones(FINMM33).'');',
'            --DO_KEY(''EXIT'');	',
'    END IF;',
'		',
'		--------------------------------------------------------------------------------------------',
'		------------------------------//CONCEPTOS\\-------------------------------',
'',
'    IF :P8006_CONF_CONC_PRCO_CR IS NULL THEN 	 ',
'            RAISE_APPLICATION_ERROR(-20010,''Primero debe definir el concepto PREST.CONCEDIDO.CR en el mant. de configuraciones(FINMM33).'');',
'            --DO_KEY(''EXIT'');	',
'    END IF;			',
'',
'    IF :P8006_CONF_CONC_INT_PRCO_DB IS NULL THEN 	 ',
'            RAISE_APPLICATION_ERROR(-20010,''Primero debe definir el concepto de Int.a Devengar en el mant. de configuraciones(FINMM33).'');',
'            --INT.PMO.CONC.DB',
'            --DO_KEY(''EXIT'');	',
'    END IF;	',
'',
'    /*IF :BCONF.CONF_CONC_INTC_PRCO_DB IS NULL THEN',
'      PL_EXHIBIR_MENSAJE(''Primero debe definir el concepto para INT.PREST.CONC.DB en el mant. de configuraciones(FINMM33).'');',
'        DO_KEY(''EXIT'');	',
'    END IF;*/	',
'',
'    IF :P8006_CONF_CONC_IVA_PRCO_DB IS NULL THEN 	 ',
'            RAISE_APPLICATION_ERROR(-20010,''Primero debe definir el concepto para IVA.INT.PMO.REC.DB en el mant. de configuraciones(FINMM33).'');',
'            --DO_KEY(''EXIT'');	',
'    END IF;	',
'',
'		--------------------------------------------------------------------------------------------',
'		--------------------------//CUENTAS CONTABLES\\-----------------------------',
'		',
'	IF :P8006_CF_CLAVE_CTACO_INTC_PRCO IS NULL THEN',
'	      RAISE_APPLICATION_ERROR(-20010,''Primero debe definir la cuenta contable CTA.INT.COBRAR.PREST.EMIT en el mant. de configuraciones(FINMM33).'');',
'				--DO_KEY(''EXIT'');	',
'	END IF;	  ',
'		',
'    :P8006_CONF_CTACO_PRCO_CR 		:= FINI034.FP_CTACO_CONC(:P8006_CONF_CONC_PRCO_CR, :P_EMPRESA);',
'    :P8006_CONF_CTACO_INT_PRCO_DB 	:= FINI034.FP_CTACO_CONC(:P8006_CONF_CONC_INT_PRCO_DB, :P_EMPRESA);',
'    :P8006_CONF_CTACO_IVA_PRCO_DB 	:= FINI034.FP_CTACO_CONC(:P8006_CONF_CONC_IVA_PRCO_DB, :P_EMPRESA);',
'		',
'	IF :P8006_CONF_CTACO_PRCO_CR IS NULL THEN 	 ',
'				RAISE_APPLICATION_ERROR(-20010,''Primero debe definir la cuenta contable para el concepto PMO.CONC.CR'');',
'				--DO_KEY(''EXIT'');	',
'	END IF;		',
'	',
'    IF :P8006_CONF_CTACO_INT_PRCO_DB IS NULL THEN 	 ',
'				RAISE_APPLICATION_ERROR(-20010,''Primero debe definir la cuenta contable para el concepto INT.PMO.CONC.DB'');',
'	    		--DO_KEY(''EXIT'');	',
'	END IF;	',
'	',
'    IF :P8006_CONF_CTACO_INT_PRCO_DB IS NULL THEN 	 ',
'				RAISE_APPLICATION_ERROR(-20010,''Primero debe definir la cuenta contable para el concepto IVA.INT.PMO.CONC.DB'');',
'				--DO_KEY(''EXIT'');	',
'	END IF;				',
'  ---------------------------------------------------------------------------------------',
'    IF :P8006_CONF_PORC_IVA_INT IS NULL THEN ',
'				RAISE_APPLICATION_ERROR(-20010,''Primero debe definir el porcentaje de IVA a utilizar, en el mant. de configuraciones(FINM033)!'');',
'				--DO_KEY(''EXIT'');  		',
'    END IF;',
'  ---------------------------------------------------------------------------------------',
' 	   ',
'  :P8006_DOC_TIPO_MOV := :P8006_CONF_TMOV_PRCO;',
'  FINI034.PP_VALIDAR_TRAER_DESC_TMOV(I_DOC_TIPO_MOV     => :P8006_DOC_TIPO_MOV,',
'                                     O_TMOV_DESC        => :P8006_TMOV_DESC,',
'                                     O_DOC_TIPO_SALDO   => :P8006_DOC_TIPO_SALDO,',
'                                     O_W_BASE_IMPON     => :P8006_W_BASE_IMPON,',
'                                     O_W_IND_ER         => :P8006_W_IND_ER,',
'                                     O_W_AFECTA_SALDO   => :P8006_W_AFECTA_SALDO); ',
'                                     ',
'  FINI034.PP_TRAER_SGTE_NRO_PRESTAMO(O_DOC_NRO_DOC => :P8006_DOC_NRO_DOC);',
'',
'  ',
'EXCEPTION',
'  --WHEN FORM_TRIGGER_FAILURE THEN',
'    --	RAISE FORM_TRIGGER_FAILURE;',
'  WHEN OTHERS THEN',
'    	--PL_EXHIBIR_ERROR_PLSQL;',
'        RAISE_APPLICATION_ERROR(-20010,''error'');',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(7844985065343600980)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'aceptar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    V_RESPUESTA VARCHAR2(5);',
'    ',
'    PROCEDURE PP_ACTUALIZAR_REGISTRO IS',
'          SALIR EXCEPTION;',
'          V_DIFERENCIA NUMBER;',
'          v_clave NUMBER;',
'',
'    BEGIN',
'    ',
'          IF   NVL(:P8006_TOTAL_MON,0) = 0 THEN',
unistr('             RAISE_APPLICATION_ERROR(-20010,''Debe ingresar el importe de capital e inter\00E9s!'');'),
'          END IF;',
'    ',
'          IF :P8006_DOC_TIPO_MOV IS NULL THEN',
'             --GO_BLOCK(''BDOC'');',
'             RAISE SALIR;',
'          END IF;',
'',
'          FINI034.PP_VALIDAR_NRO_PRESTAMO(I_EMPRESA      => :P_EMPRESA,',
'                                          I_TIPO_MOV     => :P8006_DOC_TIPO_MOV,',
'                                          I_DOC_NRO_DOC  => :P8006_DOC_NRO_DOC);      ',
'',
'          IF  :P8006_W_LIM_AUTORIZADO = ''S'' THEN --BUSCAR DONDE SE GENERA',
'                FINI034.PP_ACTUAL_AUTORIZ_ESPEC(  I_DOC_CLI         => :P8006_DOC_CLI,',
'                                                  I_S_DOC_FEC_OPER  => :P8006_S_DOC_FEC_OPER,',
'                                                  I_DOC_MON         => :P8006_DOC_MON,',
'                                                  I_EMPRESA         => :P_EMPRESA); ',
'          END IF;',
'          --',
'',
'          FINI034.PP_VALIDAR_CUOTAS(I_S_TOT_IMP_CUO  =>  :P8006_S_TOT_IMP_CUO,',
'                                    I_TOTAL_MON      =>  :P8006_TOTAL_MON,',
'                                    I_TIP_GEN_CUO    =>  :P8006_TIP_GEN_CUO,',
'                                    I_doc_fec_doc    =>  TO_DATE(:P8006_DOC_FEC_DOC,''DD/MM/YYYY''),',
'                                    I_doc_fec_oper   =>  TO_DATE(:P8006_S_DOC_FEC_OPER,''DD/MM/YYYY''));  	',
'          ',
'          FINI034.PP_ACTUAL_DOC( I_EMPRESA              => :P_EMPRESA,',
'                                 I_SUCURSAL             => :P_SUCURSAL,',
'                                 I_USER                 => :APP_USER,',
'                                 I_DOC_CLI_DIR          => :P8006_DOC_CLI_DIR,',
'                                 I_DOC_CLI_TEL          => :P8006_DOC_CLI_TEL,',
'                                 I_DOC_CLI_RUC          => :P8006_DOC_CLI_RUC,',
'                                 I_DOC_CLI_NOM          => :P8006_DOC_CLI_NOM,',
'                                 I_DOC_CLI              => :P8006_DOC_CLI,',
'                                 I_DOC_LEGAJO           => :P8006_DOC_LEGAJO,',
'                                 I_LISTA_PRECIO_DESC    => :P8006_LISTA_PRECIO_DESC,',
'                                 I_PED_DEP              => :P8006_PED_DEP,',
'                                 I_S_DOC_NRO_FAC        => :P8006_S_DOC_NRO_FAC,',
'                                 I_S_FECHA_DOCUMENTO    => :P8006_S_DOC_FEC_DOC,',
'                                 I_P_FORMATO            => :P8006_P_FORMATO,',
'                                 I_W_IND_ER             => :P8006_W_IND_ER,',
'                                 I_W_AFECTA_SALDO       => :P8006_W_AFECTA_SALDO,',
'                                 I_DOC_CLAVE_SCLI       => :P8006_DOC_CLAVE_SCLI,',
'                                 I_S_BASE_IMPON         => :P8006_S_BASE_IMPON,',
'                                 I_S_TASA_OFIC          => :P8006_S_TASA_OFIC,',
'                                 I_DOC_TIPO_MOV         => :P8006_DOC_TIPO_MOV,',
'                                 I_CONF_FACT_CR_REC     => :P8006_CONF_FACT_CR_REC,',
'                                 I_CONF_FACT_CR_EMIT    => :P8006_CONF_FACT_CR_EMIT,',
'                                 I_CONF_ADELANTO_CLI    => :P8006_CONF_ADELANTO_CLI,',
'                                 I_CONF_ADELANTO_PRO    => :P8006_CONF_ADELANTO_PRO,',
'                                 I_TMOV_DESPACHO        => :P8006_TMOV_DESPACHO,',
'                                 I_TMOV_IMPORTACION     => :P8006_TMOV_IMPORTACION,',
'                                 I_TMOV_ORDEN_COMPRA    => :P8006_TMOV_ORDEN_COMPRA, ',
'                                 I_CONF_TMOV_PRCO       => :P8006_TMOV_PRCO,',
'                                 I_CONF_NOTA_CR_REC     => :P8006_CONF_NOTA_CR_REC,',
'                                 I_CONF_NOTA_CR_EMIT    => :P8006_CONF_NOTA_CR_EMIT,',
'                                 I_CONF_NOTA_DB_REC     => :P8006_CONF_NOTA_DB_REC,',
'                                 I_CONF_NOTA_DB_EMIT    => :P8006_CONF_NOTA_DB_EMIT,',
'                                 I_DOC_CTA_BCO          => :P8006_DOC_CTA_BCO,',
'                                 I_DOC_CAPITAL_MON      => :P8006_DOC_CAPITAL_MON,',
'                                 I_DOC_CAPITAL_LOC      => :P8006_DOC_CAPITAL_LOC,',
'                                 I_DOC_INTERES_MON      => :P8006_DOC_INTERES_MON,',
'                                 I_DOC_INTERES_LOC      => :P8006_DOC_INTERES_LOC,',
'                                 I_DOC_FEC_DOC          => TO_DATE(:P8006_DOC_FEC_DOC,''DD/MM/YYYY''),',
'                                 I_CONF_FEC_LIM_MOD     => TO_DATE(:P8006_CONF_FEC_LIM_MOD,''DD/MM/YYYY''),',
'                                 I_CONF_PER_ACT_FIN     => TO_DATE(:P8006_CONF_PER_ACT_FIN,''DD/MM/YYYY''),',
'                                 I_DOC_NETO_EXEN_MON    => :P8006_DOC_NETO_EXEN_MON,',
'                                 I_DOC_NETO_EXEN_LOC    => :P8006_DOC_NETO_EXEN_LOC,',
'                                 I_DOC_NETO_GRAV_MON    => :P8006_DOC_NETO_GRAV_MON,',
'                                 I_DOC_NETO_GRAV_LOC    => :P8006_DOC_NETO_GRAV_LOC,',
'                                 I_DOC_IVA_MON          => :P8006_DOC_IVA_MON,',
'                                 I_DOC_IVA_LOC          => :P8006_DOC_IVA_LOC,',
'                                 O_DOC_EMPR             => :P8006_DOC_EMPR,',
'                                 O_DOC_SUC              => :P8006_DOC_SUC,',
'                                 O_DOC_LOGIN            => :P8006_DOC_LOGIN,',
'                                 O_DOC_FEC_GRAB         => :P8006_DOC_FEC_GRAB,',
'                                 O_DOC_SIST             => :P8006_DOC_SIST,',
'                                 O_P_CLI_DIR            => :P8006_P_CLI_DIR,',
'                                 O_P_CLI_TEL            => :P8006_P_CLI_TEL,',
'                                 O_P_CLI_RUC            => :P8006_P_CLI_RUC,',
'                                 O_P_CLI_NOM            => :P8006_P_CLI_NOM,',
'                                 O_P_CLI_CODIGO         => :P8006_P_CLI_CODIGO,',
'                                 O_P_VENDEDOR           => :P8006_P_VENDEDOR,',
'                                 O_P_LISTA              => :P8006_P_LISTA,',
'                                 O_P_ALMACEN            => :P8006_ALMACEN,',
'                                 O_P_NRO_FACTURA        => :P8006_P_NRO_FACTURA,',
'                                 O_P_FECHA_DOCUMENTO    => :P8006_P_FECHA_DOCUMENTO,',
'                                 O_DOC_BASE_IMPON_MON   => :P8006_DOC_BASE_IMPON_MON,',
'                                 O_DOC_BASE_IMPON_LOC   => :P8006_DOC_BASE_IMPON_LOC,',
'                                 O_DOC_BRUTO_EXEN_MON   => :P8006_DOC_BRUTO_EXEN_MON,',
'                                 O_DOC_BRUTO_EXEN_LOC   => :P8006_DOC_BRUTO_EXEN_LOC,',
'                                 O_DOC_BRUTO_GRAV_MON   => :P8006_DOC_BRUTO_GRAV_MON,',
'                                 O_DOC_BRUTO_GRAV_LOC   => :P8006_DOC_BRUTO_GRAV_LOC,',
'                                 O_DOC_SALDO_LOC        => :P8006_DOC_SALDO_LOC,',
'                                 O_DOC_SALDO_MON        => :P8006_DOC_SALDO_MON,',
'                                 O_DOC_SAL_CAP_MON      => :P8006_DOC_SAL_CAP_MON,',
'                                 O_DOC_SAL_CAP_LOC      => :P8006_DOC_SAL_CAP_LOC,',
'                                 O_DOC_SAL_INT_MON      => :P8006_DOC_SAL_INT_MON,',
'                                 O_DOC_SAL_INT_LOC      => :P8006_DOC_SAL_INT_LOC,',
'                                 O_DOC_SALDO_PER_ACT_LOC     => :P8006_DOC_SALDO_PER_ACT_LOC,',
'                                 O_DOC_SALDO_PER_ACT_MON     => :P8006_DOC_SALDO_PER_ACT_MON,',
'                                 O_DOC_SAL_CAP_PER_ACT_LOC   => :P8006_DOC_SAL_CAP_PER_ACT_LOC,',
'                                 O_DOC_SAL_CAP_PER_ACT_MON   => :P8006_DOC_SAL_CAP_PER_ACT_MON,',
'                                 O_DOC_SAL_INT_PER_ACT_LOC   => :P8006_DOC_SAL_INT_PER_ACT_LOC,',
'                                 O_DOC_SAL_int_PER_ACT_MON   => :P8006_DOC_SAL_INT_PER_ACT_MON);  ',
'                    ',
'          FINI034.PP_ACTUAL_CUO( I_P_MON_SIMB        => :P8006_P_MON_SIMB,',
'                                 I_S_TASA_OFIC       => :P8006_S_TASA_OFIC,',
'                                 I_DOC_FEC_DOC       => to_date(:P8006_DOC_FEC_DOC,''dd/mm/yyyy''),',
'                                 I_CONF_FEC_LIM_MOD  => to_date(:P8006_CONF_FEC_LIM_MOD,''dd/mm/yyyy''),',
'                                 I_CONF_PER_ACT_FIN  => to_date(:P8006_CONF_PER_ACT_FIN,''dd/mm/yyyy''),',
'                                 I_TOTAL_LOC         => :P8006_TOTAL_LOC,',
'                                 I_DOC_INTERES_LOC   => :P8006_DOC_INTERESES_LOC,',
'                                 I_DOC_CAPITAL_LOC   => :P8006_DOC_CAPITAL_LOC);   ',
'        ',
'          FINI034.PP_INSERTAR_DOC_PRESTAMO(  I_CONF_TMOV_PRCO        => :P8006_CONF_TMOV_PRCO,',
'                                             I_doc_nro_doc           => :P8006_DOC_NRO_DOC,',
'                                             I_s_tasa_ofic           => :P8006_S_TASA_OFIC,',
'                                             I_doc_mon               => :P8006_DOC_MON,',
'                                             I_doc_capital_loc       => :P8006_DOC_CAPITAL_LOC,',
'                                             I_doc_capital_mon       => :P8006_DOC_CAPITAL_MON,',
'                                             I_doc_interes_loc       => :P8006_DOC_INTERESES_LOC,',
'                                             I_doc_iva_int_loc       => :P8006_DOC_IVA_INT_LOC,',
'                                             I_DOC_INTERES_MON_CUO   => :P8006_DOC_INTERES_MON_CUO,',
'                                             I_doc_iva_int_mon       => :P8006_DOC_IVA_INT_MON,',
'                                             I_s_cuenta              => :P8006_S_CUENTA,',
'                                             I_doc_cli_nom           => :P8006_DOC_CLI_NOM,',
'                                             I_doc_cli_dir           => :P8006_DOC_CLI_DIR,',
'                                             I_doc_cli_tel           => :P8006_DOC_CLI_TEL,',
'                                             I_doc_cli_ruc           => :P8006_DOC_CLI_RUC,',
'                                             I_doc_clave_sol         => :P8006_doc_clave_sol,',
'                                             I_doc_tip_mov_permiso   => :P8006_doc_tip_mov_permiso,',
'                                             I_USER                  => :APP_USER,',
'                                             I_doc_fec_oper          => to_date(:P8006_DOC_FEC_OPER,''dd/mm/yyyy''),',
'                                             I_doc_fec_doc           => to_date(:P8006_DOC_FEC_DOC,''dd/mm/yyyy''),',
'                                             I_doc_empr              => :P_EMPRESA,',
'                                             I_doc_SUC               => :P_SUCURSAL,',
'                                             I_S_IND_REESTRUCTURADO  => :P8006_S_IND_REESTRUCTURADO,',
'                                             I_DOC_NRO_CREDITO_PREST => :P8006_DOC_NRO_CREDITO_PREST,',
'                                             I_DOC_ACT_PRESTAMO      => :P8006_DOC_ACT_PRESTAMO,',
'                                             I_doc_cli_codeudor      => :P8006_doc_cli_codeudor,',
'                                             I_conf_porc_iva_int     => :P8006_conf_porc_iva_int,',
'                                             I_CANT_PAGARES          => :P8006_CANT_PAGARES,',
'                                             O_DOC_CLAVE_DOC_PMO     => :P8006_DOC_CLAVE_DOC_PMO); ',
'                                         ',
'          FINI034.PP_INSERTAR_DOC_CAPITAL(  I_CONF_TMOV_CAP_PRCO        => :P8006_CONF_TMOV_CAP_PRCO,',
'                                            I_doc_nro_doc               => :P8006_DOC_NRO_DOC,',
'                                            I_s_tasa_ofic               => :P8006_S_TASA_OFIC,',
'                                            I_doc_cta_bco               => :P8006_DOC_CTA_BCO,',
'                                            I_doc_mon                   => :P8006_DOC_MON,',
'                                            I_doc_capital_loc           => :P8006_DOC_CAPITAL_LOC,',
'                                            I_doc_capital_mon           => :P8006_DOC_CAPITAL_MON,',
'                                            I_s_cuenta                  => :P8006_S_CUENTA,',
'                                            I_doc_cli_nom               => :P8006_DOC_CLI_NOM,',
'                                            I_doc_cli_dir               => :P8006_DOC_CLI_DIR,',
'                                            I_doc_cli_tel               => :P8006_DOC_CLI_TEL,',
'                                            I_doc_cli_ruc               => :P8006_DOC_CLI_RUC,',
'                                            I_EMPRESA                   => :P_EMPRESA,',
'                                            I_SUCURSAL                  => :P_SUCURSAL,',
'                                            I_doc_fec_oper              => to_date(:P8006_S_DOC_FEC_OPER,''dd/mm/yyyy''),',
'                                            I_doc_fec_doc               => to_date(:P8006_DOC_FEC_DOC,''dd/mm/yyyy''),',
'                                            I_USER                      => :APP_USER,',
'                                            I_doc_clave_doc_pmo         => :P8006_DOC_CLAVE_DOC_PMO,',
'                                            I_DOC_ACT_PRESTAMO          => :P8006_DOC_ACT_PRESTAMO,           ',
'                                            I_DOC_NRO_CREDITO_PREST     => :P8006_DOC_NRO_CREDITO_PREST,',
'                                            I_S_IND_REESTRUCTURADO      => :P8006_S_IND_REESTRUCTURADO,',
'                                            I_CONF_CONC_PRCO_CR         => :P8006_CONF_CONC_PRCO_CR,',
'                                            I_CONF_CTACO_PRCO_CR        => :P8006_CONF_CTACO_PRCO_CR,',
'                                            I_conf_porc_iva_int         => :P8006_conf_porc_iva_int,',
'                                            I_CANT_PAGARES              => :P8006_CANT_PAGARES,',
'                                            O_DOC_CLAVE_DOC_CAP         => :P8006_DOC_CLAVE_DOC_CAP);  ',
'',
'',
'          --break;',
'          IF TO_NUMBER(NVL(:P8006_DOC_INTERES_MON_CUO,0)) > 0 THEN ',
'                FINI034.PP_INSERTAR_DOC_INTERES(I_CONF_TMOV_INT_PRCO        => :P8006_CONF_TMOV_INT_PRCO,',
'                                                I_doc_nro_doc               => :P8006_DOC_NRO_DOC,',
'                                                I_s_tasa_ofic               => :P8006_S_TASA_OFIC,',
'                                                I_doc_mon                   => :P8006_DOC_MON,',
'                                                I_doc_interes_loc           => :P8006_DOC_INTERESES_LOC,',
'                                                I_doc_iva_int_loc           => :P8006_DOC_IVA_INT_LOC,',
'                                                I_DOC_INTERES_MON_CUO       => :P8006_DOC_INTERES_MON_CUO,',
'                                                I_doc_iva_int_mon           => :P8006_DOC_IVA_INT_MON,',
'                                                I_s_cuenta                  => :P8006_S_CUENTA,',
'                                                I_doc_cli_nom               => :P8006_DOC_CLI_NOM,',
'                                                I_doc_cli_dir               => :P8006_DOC_CLI_DIR,',
'                                                I_doc_cli_tel               => :P8006_DOC_CLI_TEL,',
'                                                I_doc_cli_ruc               => :P8006_DOC_CLI_RUC,',
'                                                I_EMPRESA                   => :P_EMPRESA,',
'                                                I_SUCURSAL                  => :P_SUCURSAL,',
'                                                I_doc_fec_oper              => to_date(:P8006_S_DOC_FEC_OPER,''dd/mm/yyyy''),',
'                                                I_doc_fec_doc               => to_date(:P8006_DOC_FEC_DOC,''dd/mm/yyyy''),',
'                                                I_USER                      => :APP_USER,',
'                                                I_doc_clave_doc_pmo         => :P8006_DOC_CLAVE_DOC_PMO,',
'                                                I_DOC_ACT_PRESTAMO          => :P8006_DOC_ACT_PRESTAMO,           ',
'                                                I_DOC_NRO_CREDITO_PREST     => :P8006_DOC_NRO_CREDITO_PREST,',
'                                                I_S_IND_REESTRUCTURADO      => :P8006_S_IND_REESTRUCTURADO,',
'                                                I_CONF_CONC_INT_PRCO_DB     => :P8006_CONF_CONC_INT_PRCO_DB,',
'                                                I_CONF_CTACO_INT_PRCO_DB    => :P8006_CONF_CTACO_INT_PRCO_DB,',
'                                                I_conf_porc_iva_int         => :P8006_conf_porc_iva_int,',
'                                                I_CANT_PAGARES              => :P8006_CANT_PAGARES,',
'                                                O_DOC_CLAVE_DOC_INT         => :P8006_DOC_CLAVE_DOC_INT);   ',
'          END IF;',
'          FINI034.PP_ACTUALIZAR_ULT_NRO_PMO(I_DOC_NRO_DOC    => :P8006_DOC_NRO_DOC,',
'                                            I_P_IMPRESORA    => :P8006_P_IMPRESORA,',
'                                            I_EMPRESA        => :P_EMPRESA);  ',
'        ',
'          -- Si el tipo de saldo = ''C'' implica que se hace un pago.',
unistr('          -- Si :PARAMETER.P_BANCO es no nulo implica que el pago se realiz\00F3'),
'          -- con cheque de ese banco. ',
'',
'              /*IF :P8006_DOC_CAPITAL_MON > 0 THEN ',
'                IF :P8006_P_BANCO IS NOT NULL THEN',
'                        IF I_F_TOT_CH = I_DOC_CAPITAL_MON THEN',
'                                PP_INSERTAR_CH_EMIT;',
'                                PP_ACT_CTA_BAN;',
'                        END IF;	',
'                END IF;	',
'              END IF;*/',
'',
'    EXCEPTION',
'',
'      WHEN SALIR THEN',
'        NULL;',
'      WHEN OTHERS THEN ',
'                    --PL_EXHIBIR_ERROR(SQLERRM);',
'                    RAISE_APPLICATION_ERROR(-20010,''Error en PP_ACTUALIZAR_REGISTRO''||SQLCODE||''-''||SQLERRM);',
'    END;',
'',
'    ',
'',
'    PROCEDURE PP_VERIF_CHQ_DOC IS',
'    V_RESPUESTA VARCHAR2(5);',
'    BEGIN',
'        --Si tipo de movimiento es ''C'', si Parameter.P_banco es no nulo',
'        --y si el total de cheques es distinto de cero o es no nulo, implica que',
'        --se emiten cheques por tanto se debe validar que la suma total de los cheques ',
'        --emitidos coincidan con la el importe del documento',
'        IF :P8006_DOC_TIPO_SALDO = ''C'' THEN',
'            IF :P8006_P_BANCO IS NOT NULL THEN       ',
'                IF :P8006_DOC_CTA_BCO IS NOT NULL THEN',
'                    FINI034.PL_VALIDAR_OPCTA(:P8006_DOC_CTA_BCO, ',
'                                             :P_EMPRESA, ',
'                                             :APP_USER, ',
'                                             :P8006_P_CTA_DESC, ',
'                                             :P8006_DOC_TIPO_SALDO);',
'                END IF;',
'                PP_ACTUALIZAR_REGISTRO;',
'            END IF;',
'      ELSE',
'                IF :P8006_DOC_CTA_BCO IS NOT NULL THEN',
'                        FINI034.PL_VALIDAR_OPCTA(:P8006_DOC_CTA_BCO, :P_EMPRESA, :APP_USER, :P8006_P_CTA_DESC, :P8006_DOC_TIPO_SALDO);',
'                END IF;',
'                PP_ACTUALIZAR_REGISTRO;',
'      END IF;',
'',
'            exception when others then ',
'                 RAISE_APPLICATION_ERROR(-20010,''Error en PP_VERIF_CHQ_DOC''||SQLCODE||''-''||SQLERRM);',
'    END;',
'',
'--PROGRAMA PRINCIPAL',
'BEGIN',
'',
'IF :P8006_S_TOT_IMP_CAP IS NULL THEN',
'	 RAISE_APPLICATION_ERROR(-20010,''El Importe de Capital no puede ser Nulo!'');',
'ELSE',
'  PP_VERIF_CHQ_DOC;',
'END IF;',
'END;',
'',
''))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(7797047078136101705)
,p_process_when=>'Aceptar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'Registros guardados correctamente!! '
);
end;
/
begin
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(7844985020914600979)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'Limpiar items'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
null;
end;
/
prompt --application/end_environment
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
