create or replace package FINI019 is

  -- Author  : PROGRAMACION13
  -- Created : 21/07/2020 11:55:18
  -- Purpose :

  PROCEDURE PP_MOSTRAR_DATOS(P_DOC_TIMBRADO       OUT NUMBER,
                             P_DOC_NRO_DOC        OUT NUMBER,
                             P_DOC_TIPO_MOV       OUT NUMBER,
                             P_DOC_FEC_OPER       OUT DATE,
                             P_DOC_CTA_BCO        OUT NUMBER,
                             P_DOC_FEC_DOC        OUT DATE,
                             P_PROVEEDOR          OUT VARCHAR2,
                             P_CLIENTE            OUT VARCHAR2,
                             P_DOC_CLI_RUC        OUT VARCHAR2,
                             P_DOC_CLI_DIR        OUT VARCHAR2,
                             P_DOC_CLI_TEL        OUT VARCHAR2,
                             P_DOC_NRO_CHEQUE     OUT VARCHAR2,
                             P_DOC_BCO_CHEQUE     OUT NUMBER,
                             P_DOC_FEC_CHEQUE     OUT DATE,
                             P_DOC_CHEQUE_CERTIF  OUT VARCHAR2,
                             P_DOC_MON            OUT NUMBER,
                             P_DOC_NETO_EXEN_MON  OUT NUMBER,
                             P_DOC_NETO_GRAV_MON  OUT NUMBER,
                             P_DOC_IVA_MON        OUT NUMBER,
                             P_DOC_NETO_EXEN_LOC  OUT NUMBER,
                             P_DOC_NETO_GRAV_LOC  OUT NUMBER,
                             P_DOC_IVA_LOC        OUT NUMBER,
                             P_DOC_OBS            OUT VARCHAR2,
                             P_DOC_CLAVE          OUT NUMBER,
                             P_DOC_EMPR           OUT NUMBER,
                             P_DOC_SUC            OUT NUMBER,
                             P_DOC_DPTO           OUT NUMBER,
                             P_DOC_PROV           OUT NUMBER,
                             P_DOC_CLI            OUT NUMBER,
                             P_DOC_LEGAJO         OUT NUMBER,
                             P_DOC_COBRADOR       OUT NUMBER,
                             P_DOC_EST_CHEQUE     OUT VARCHAR2,
                             P_DOC_FEC_DEP_CHEQUE OUT DATE,
                             P_DOC_NRO_DOC_DEP    OUT NUMBER,
                             P_DOC_BASE_IMPON_LOC OUT NUMBER,
                             P_DOC_BASE_IMPON_MON OUT NUMBER,
                             P_DOC_SALDO_MON      OUT NUMBER,
                             P_DOC_SALDO_LOC      OUT NUMBER,
                             P_DOC_LOGIN          OUT VARCHAR2,
                             P_DOC_FEC_GRAB       OUT DATE,
                             P_DOC_SIST           OUT VARCHAR2,
                             P_DOC_PLAN_FINAN     OUT NUMBER,
                             P_DOC_CLAVE_STK      OUT NUMBER,
                             P_DOC_OPERADOR       OUT NUMBER,
                             P_W_CLAVE            IN NUMBER,
                             P_EMPRESA            IN NUMBER,
                             P_TASA_OFIC          OUT NUMBER,
                             P_TOT_DOC            OUT NUMBER,
                             P_AFECTA_SALDO       IN VARCHAR2,
                             P_IND_ER             IN VARCHAR2,
                             P_DOC_TIPO_FACTURA   OUT VARCHAR2);

    PROCEDURE PP_VALIDAR_DOC_EXIS(P_EMPRESA     IN NUMBER,
                                P_TIPO_MOV    IN NUMBER,
                                P_NVO_NRO_DOC IN NUMBER,
                                P_TIMBRADO    IN NUMBER);


    PROCEDURE PP_ACTUALIZAR_DOCUMENTO(P_EMPRESA IN NUMBER,
                                   P_NVO_NRO_DOC IN NUMBER,
                                   P_CLAVE_STK IN NUMBER,
                                   P_CLAVE_DOC IN NUMBER,
                                   P_S_TIMBRADO IN VARCHAR2,
                                   P_FEC_OPER IN DATE,
                                   P_FEC_DOC IN DATE,
                                   in_tipo_factura in fin_documento.doc_tipo_factura%type := null
                                   );

    PROCEDURE PP_VAL_CTA_BCO_MES_ACTUAL (P_FECHA IN DATE, P_CTA_BCO IN NUMBER, P_EMPRESA IN NUMBER);

     PROCEDURE PP_IMPRIMIR_REPORTE(P_EMPRESA  IN NUMBER,
                                P_SUCURSAL IN NUMBER,
                                P_CLAVE    IN NUMBER) ;

end FINI019;
/
CREATE OR REPLACE PACKAGE BODY FINI019 IS

  PROCEDURE PP_MOSTRAR_DATOS(P_DOC_TIMBRADO       OUT NUMBER,
                             P_DOC_NRO_DOC        OUT NUMBER,
                             P_DOC_TIPO_MOV       OUT NUMBER,
                             P_DOC_FEC_OPER       OUT DATE,
                             P_DOC_CTA_BCO        OUT NUMBER,
                             P_DOC_FEC_DOC        OUT DATE,
                             P_PROVEEDOR          OUT VARCHAR2,
                             P_CLIENTE            OUT VARCHAR2,
                             P_DOC_CLI_RUC        OUT VARCHAR2,
                             P_DOC_CLI_DIR        OUT VARCHAR2,
                             P_DOC_CLI_TEL        OUT VARCHAR2,
                             P_DOC_NRO_CHEQUE     OUT VARCHAR2,
                             P_DOC_BCO_CHEQUE     OUT NUMBER,
                             P_DOC_FEC_CHEQUE     OUT DATE,
                             P_DOC_CHEQUE_CERTIF  OUT VARCHAR2,
                             P_DOC_MON            OUT NUMBER,
                             P_DOC_NETO_EXEN_MON  OUT NUMBER,
                             P_DOC_NETO_GRAV_MON  OUT NUMBER,
                             P_DOC_IVA_MON        OUT NUMBER,
                             P_DOC_NETO_EXEN_LOC  OUT NUMBER,
                             P_DOC_NETO_GRAV_LOC  OUT NUMBER,
                             P_DOC_IVA_LOC        OUT NUMBER,
                             P_DOC_OBS            OUT VARCHAR2,
                             P_DOC_CLAVE          OUT NUMBER,
                             P_DOC_EMPR           OUT NUMBER,
                             P_DOC_SUC            OUT NUMBER,
                             P_DOC_DPTO           OUT NUMBER,
                             P_DOC_PROV           OUT NUMBER,
                             P_DOC_CLI            OUT NUMBER,
                             P_DOC_LEGAJO         OUT NUMBER,
                             P_DOC_COBRADOR       OUT NUMBER,
                             P_DOC_EST_CHEQUE     OUT VARCHAR2,
                             P_DOC_FEC_DEP_CHEQUE OUT DATE,
                             P_DOC_NRO_DOC_DEP    OUT NUMBER,
                             P_DOC_BASE_IMPON_LOC OUT NUMBER,
                             P_DOC_BASE_IMPON_MON OUT NUMBER,
                             P_DOC_SALDO_MON      OUT NUMBER,
                             P_DOC_SALDO_LOC      OUT NUMBER,
                             P_DOC_LOGIN          OUT VARCHAR2,
                             P_DOC_FEC_GRAB       OUT DATE,
                             P_DOC_SIST           OUT VARCHAR2,
                             P_DOC_PLAN_FINAN     OUT NUMBER,
                             P_DOC_CLAVE_STK      OUT NUMBER,
                             P_DOC_OPERADOR       OUT NUMBER,
                             P_W_CLAVE            IN NUMBER,
                             P_EMPRESA            IN NUMBER,
                             P_TASA_OFIC          OUT NUMBER,
                             P_TOT_DOC            OUT NUMBER,
                             P_AFECTA_SALDO       IN VARCHAR2,
                             P_IND_ER             IN VARCHAR2,
                             P_DOC_TIPO_FACTURA   OUT VARCHAR2) IS

    V_RAZON_SOCIAL VARCHAR2(100);
    V_RUC          VARCHAR2(30);
    V_DIRECCION    VARCHAR2(70);
    V_TELEFONO     VARCHAR2(30);
    TOT_MON        NUMBER;
    TOT_LOC        NUMBER;

  BEGIN
    SELECT DOC_TIMBRADO,
           DOC_NRO_DOC,
           DOC_TIPO_MOV,
           DOC_FEC_OPER,
           DOC_CTA_BCO,
           DOC_FEC_DOC,
           DOC_CLI_NOM,
           DOC_CLI_RUC,
           DOC_CLI_DIR,
           DOC_CLI_TEL,
           DOC_NRO_CHEQUE,
           DOC_BCO_CHEQUE,
           DOC_FEC_CHEQUE,
           DOC_CHEQUE_CERTIF,
           DOC_MON,
           DOC_NETO_EXEN_MON,
           DOC_NETO_GRAV_MON,
           DOC_IVA_MON,
           DOC_NETO_EXEN_LOC,
           DOC_NETO_GRAV_LOC,
           DOC_IVA_LOC,
           DOC_OBS,
           DOC_CLAVE,
           DOC_EMPR,
           DOC_SUC,
           DOC_DPTO,
           DOC_PROV,
           DOC_CLI,
           DOC_LEGAJO,
           DOC_COBRADOR,
           DOC_EST_CHEQUE,
           DOC_FEC_DEP_CHEQUE,
           DOC_NRO_DOC_DEP,
           DOC_BASE_IMPON_LOC,
           DOC_BASE_IMPON_MON,
           DOC_SALDO_MON,
           DOC_SALDO_LOC,
           DOC_LOGIN,
           DOC_FEC_GRAB,
           DOC_SIST,
           DOC_PLAN_FINAN,
           DOC_CLAVE_STK,
           DOC_OPERADOR,
           DOC_TIPO_FACTURA
           

      INTO P_DOC_TIMBRADO,
           P_DOC_NRO_DOC,
           P_DOC_TIPO_MOV,
           P_DOC_FEC_OPER,
           P_DOC_CTA_BCO,
           P_DOC_FEC_DOC,
           V_RAZON_SOCIAL,
           V_RUC,
           V_DIRECCION,
           V_TELEFONO,
           P_DOC_NRO_CHEQUE,
           P_DOC_BCO_CHEQUE,
           P_DOC_FEC_CHEQUE,
           P_DOC_CHEQUE_CERTIF,
           P_DOC_MON,
           P_DOC_NETO_EXEN_MON,
           P_DOC_NETO_GRAV_MON,
           P_DOC_IVA_MON,
           P_DOC_NETO_EXEN_LOC,
           P_DOC_NETO_GRAV_LOC,
           P_DOC_IVA_LOC,
           P_DOC_OBS,
           P_DOC_CLAVE,
           P_DOC_EMPR,
           P_DOC_SUC,
           P_DOC_DPTO,
           P_DOC_PROV,
           P_DOC_CLI,
           P_DOC_LEGAJO,
           P_DOC_COBRADOR,
           P_DOC_EST_CHEQUE,
           P_DOC_FEC_DEP_CHEQUE,
           P_DOC_NRO_DOC_DEP,
           P_DOC_BASE_IMPON_LOC,
           P_DOC_BASE_IMPON_MON,
           P_DOC_SALDO_MON,
           P_DOC_SALDO_LOC,
           P_DOC_LOGIN,
           P_DOC_FEC_GRAB,
           P_DOC_SIST,
           P_DOC_PLAN_FINAN,
           P_DOC_CLAVE_STK,
           P_DOC_OPERADOR,
           P_DOC_TIPO_FACTURA
      FROM FIN_DOCUMENTO A
     WHERE DOC_CLAVE = P_W_CLAVE
       AND DOC_EMPR = P_EMPRESA;

    ----TRAE LOS DATOS DEL CLIENTE O PROVEEDOR

    IF (P_AFECTA_SALDO = 'P' OR P_IND_ER = 'R') THEN
      IF P_DOC_PROV IS NULL THEN
        P_PROVEEDOR   := V_RAZON_SOCIAL;
        P_DOC_CLI_RUC := V_RUC;
        P_DOC_CLI_DIR := V_DIRECCION;
        P_DOC_CLI_TEL := V_TELEFONO;
      ELSE
        BEGIN
          SELECT PROV_RAZON_SOCIAL, PROV_DIR, PROV_TEL, PROV_RUC
            INTO P_PROVEEDOR, P_DOC_CLI_DIR, P_DOC_CLI_TEL, P_DOC_CLI_RUC
            FROM FIN_PROVEEDOR
           WHERE PROV_EMPR = P_EMPRESA
             AND PROV_CODIGO = P_DOC_PROV;
        EXCEPTION
          WHEN OTHERS THEN
            NULL;
        END;
      END IF;

    ELSIF (P_AFECTA_SALDO = 'C' OR P_IND_ER <> 'R') THEN
      IF P_DOC_CLI IS NULL THEN
        P_CLIENTE     := V_RAZON_SOCIAL;
        P_DOC_CLI_RUC := V_RUC;
        P_DOC_CLI_DIR := V_DIRECCION;
        P_DOC_CLI_TEL := V_TELEFONO;
      ELSE
        BEGIN
          SELECT CLI_NOM, CLI_DIR, CLI_TEL, CLI_RUC
            INTO P_CLIENTE, P_DOC_CLI_DIR, P_DOC_CLI_TEL, P_DOC_CLI_RUC
            FROM FIN_CLIENTE
           WHERE CLI_EMPR = P_EMPRESA
             AND CLI_CODIGO = P_DOC_CLI;
        END;
        --P_HABILITAR := 'N';
      END IF;
    END IF;

    ---CALCULA EL TOTAL DEL DOCUMENTO
  --  RAISE_APPLICATION_ERROR(-20001, P_TASA_OFIC);

    BEGIN
      -- RECALCULO DE TASA
      IF P_DOC_MON IS NOT NULL AND P_TASA_OFIC IS NULL THEN

        TOT_MON := NVL(P_DOC_NETO_EXEN_MON, 0) +
                   NVL(P_DOC_NETO_GRAV_MON, 0) + NVL(P_DOC_IVA_MON, 0);

        TOT_LOC := NVL(P_DOC_NETO_EXEN_LOC, 0) +
                   NVL(P_DOC_NETO_GRAV_LOC, 0) + NVL(P_DOC_IVA_LOC, 0);

        P_TASA_OFIC := TOT_LOC / TOT_MON;
      END IF;
    EXCEPTION
      WHEN ZERO_DIVIDE THEN
        P_TASA_OFIC := NULL;
    END;
    P_TOT_DOC := TOT_MON;

  END PP_MOSTRAR_DATOS;

  PROCEDURE PP_VALIDAR_DOC_EXIS(P_EMPRESA     IN NUMBER,
                                P_TIPO_MOV    IN NUMBER,
                                P_NVO_NRO_DOC IN NUMBER,
                                P_TIMBRADO    IN NUMBER) IS
    V_NRO_DOC   NUMBER;
    V_TIPO_MOV1 NUMBER;
  BEGIN

    IF P_TIPO_MOV IN (9, 10) THEN
      V_TIPO_MOV1 := 10;
    ELSE
      V_TIPO_MOV1 := P_TIPO_MOV;
    END IF;

    SELECT DOC_NRO_DOC
      INTO V_NRO_DOC
      FROM FIN_DOCUMENTO
     WHERE DOC_NRO_DOC = P_NVO_NRO_DOC
       AND DOC_EMPR = P_EMPRESA
       AND FIN_FACT_ANUL(DOC_CLAVE) = 'N'
       AND DECODE(DOC_TIPO_MOV, 9, 10, DOC_TIPO_MOV) = V_TIPO_MOV1
       AND NVL(DOC_TIMBRADO, 0) = NVL(P_TIMBRADO, 0);

    IF V_NRO_DOC IS NOT NULL THEN
      RAISE_APPLICATION_ERROR(-20001,
                              'Documento con ese timbrado existente!');
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
    WHEN TOO_MANY_ROWS THEN
      RAISE_APPLICATION_ERROR(-20001,
                              'Documento con ese timbrado existente!');
  END PP_VALIDAR_DOC_EXIS;


  PROCEDURE PP_VAL_CTA_BCO_MES_ACTUAL (P_FECHA IN DATE, P_CTA_BCO IN NUMBER, P_EMPRESA IN NUMBER) IS
  V_HABILITADO VARCHAR2(1);
  V_VARIABLE  VARCHAR2(1);
BEGIN


     SELECT 'X'
        INTO V_VARIABLE
        FROM FIN_CONFIGURACION
       WHERE p_fecha BETWEEN CONF_FEC_LIM_MOD AND CONF_PER_ACT_FIN
         AND CONF_EMPR = p_empresa;

  IF V_VARIABLE = 'X' THEN

SELECT OP_CTA_MES_ACTUAL
  INTO V_HABILITADO
  FROM FIN_OPER_CTA_BCO,GEN_OPERADOR_EMPRESA GE, GEN_OPERADOR
 WHERE GE.OPEM_EMPR =P_EMPRESA
   AND OPER_CODIGO = GE.OPEM_OPER
   AND GE.OPEM_EMPR = OP_CTA_EMPR
   AND OPER_CODIGO = OP_CTA_OPER
   AND OPER_LOGIN = gen_devuelve_user
   AND OP_CTA_CTA_CODIGO = P_CTA_BCO
   ;

        IF V_HABILITADO = 'N' THEN
          RAISE_APPLICATION_ERROR(-20001,'La Caja/Banco no est? habilitada en la fecha para ?ste usuario!.');
        END IF;


  END IF;
  EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
END;


  PROCEDURE PP_ACTUALIZAR_DOCUMENTO(P_EMPRESA     IN NUMBER,
                                    P_NVO_NRO_DOC IN NUMBER,
                                    P_CLAVE_STK   IN NUMBER,
                                    P_CLAVE_DOC   IN NUMBER,
                                    P_S_TIMBRADO  IN VARCHAR2,
                                    P_FEC_OPER    IN DATE,
                                    P_FEC_DOC     IN date,
                                    in_tipo_factura in fin_documento.doc_tipo_factura%type := null
                                    ) IS
  BEGIN
  ----=========OTRA EMPRESAS
  IF P_EMPRESA != 2 THEN
    UPDATE FIN_DOCUMENTO FD
       SET FD.DOC_FEC_OPER        = P_FEC_OPER,
           FD.DOC_FEC_DOC         = P_FEC_DOC,
           FD.DOC_TIMBRADO        = P_S_TIMBRADO,
           FD.DOC_NRO_DOC         = P_NVO_NRO_DOC,
           FD.doc_tipo_factura = in_tipo_factura
     where FD.doc_clave = P_CLAVE_DOC
       AND FD.DOC_EMPR = P_EMPRESA;
       
       
    --COMMIT;
  ELSE
    UPDATE FIN_DOCUMENTO
       SET DOC_NRO_DOC  = P_NVO_NRO_DOC
     WHERE DOC_CLAVE = P_CLAVE_DOC
       AND DOC_EMPR = P_EMPRESA;
  END IF;
    if P_NVO_NRO_DOC is not null then
      if P_CLAVE_STK is not null then
        update stk_documento
           set docu_nro_doc = P_NVO_NRO_DOC
         where docu_clave = P_CLAVE_STK
           AND DOCU_EMPR = P_EMPRESA;
      end if;
      
   
    end if;
    
     /***************************/
         ---erdm
    IF P_CLAVE_STK IS NOT NULL AND (P_FEC_DOC IS NOT NULL OR P_FEC_OPER IS NOT NULL) THEN
        
      UPDATE STK_DOCUMENTO
         SET DOCU_FEC_EMIS = P_FEC_DOC, 
             DOCU_FEC_OPER = P_FEC_OPER
       WHERE DOCU_CLAVE = P_CLAVE_STK
         AND DOCU_EMPR = P_EMPRESA;
    
    END IF;
    --COMMIT;
  END PP_ACTUALIZAR_DOCUMENTO;

  PROCEDURE PP_IMPRIMIR_REPORTE(P_EMPRESA  IN NUMBER,
                                P_SUCURSAL IN NUMBER,
                                P_CLAVE    IN NUMBER) AS
    V_PARAMETROS    VARCHAR2(32767);
    V_IDENTIFICADOR VARCHAR2(2) := '&'; --SE UTILIZA PARA QUE AL COMPILAR NO LO TOME COMO PARAMETROS                           IS
    V_EMPR_DESC     VARCHAR2(100);
    V_SUCURSAL      VARCHAR2(100);
  BEGIN

    BEGIN
      SELECT A.EMPR_RAZON_SOCIAL
        INTO V_EMPR_DESC
        FROM GEN_EMPRESA A
       WHERE A.EMPR_CODIGO = P_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20016, 'Empresa no valida!');
    END;

    BEGIN
      SELECT SUC_DESC
        INTO V_SUCURSAL
        FROM GEN_SUCURSAL
       WHERE SUC_CODIGO = P_SUCURSAL
         AND SUC_EMPR = P_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;

    V_PARAMETROS := 'P_FORMATO=' || URL_ENCODE('pdf');
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_EMPRESA=' ||
                    URL_ENCODE(P_EMPRESA);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_PROGRAMA=' ||
                    URL_ENCODE('FINC001GR');

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_DESC_EMPRESA=' ||
                    URL_ENCODE(V_EMPR_DESC);

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_desc_sucursal=' ||
                    URL_ENCODE(V_SUCURSAL);

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_DESC_PROGRAMA=' ||
                    URL_ENCODE('CAMBIAR NUMERO DE DOCUMENTOS');
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_CLAVE=' ||
                    URL_ENCODE(P_CLAVE);
    DELETE FROM GEN_PARAMETROS_REPORT WHERE USUARIO = GEN_DEVUELVE_USER;
    INSERT INTO GEN_PARAMETROS_REPORT
      (PARAMETROS, USUARIO, NOMBRE_REPORTE, FORMATO_SALIDA)
    VALUES
      (V_PARAMETROS, GEN_DEVUELVE_USER, 'FINC001GR', 'pdf');
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001, SQLERRM);
  END PP_IMPRIMIR_REPORTE;
END FINI019;
/
