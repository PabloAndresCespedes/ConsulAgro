create or replace function GEN_COT_TIPO_OPER(P_EMPRESA   IN NUMBER,
                                             P_MONEDA    IN NUMBER,
                                             P_TIPO_OPER IN NUMBER,
                                             P_FECHA     IN DATE)
  RETURN NUMBER IS
  V_COT_COMPRA NUMBER;
  V_COT_VENTA  NUMBER;
  V_IND_ER     VARCHAR(2);
BEGIN
  IF P_FECHA <= TO_DATE('31/12/2021') THEN
  --======EN EL HISTORICO SE USA ESTA FUNCION ---APERALTA
 
  RETURN FIN_BUSCAR_COTIZACION_FEC(FEC  => P_FECHA,
                                   MON  => P_MONEDA,
                                   EMPR => P_EMPRESA);

  ELSE

    --====ESTO EMPIEZA FUNCIONAR RECIEN APARTIR  DE ENERO 2022
  <<get_data_stk_operacion>>
  begin
    SELECT OPER_ENT_SAL
    into V_IND_ER
    FROM STK_OPERACION
    WHERE OPER_CODIGO = P_TIPO_OPER
    AND OPER_EMPR = P_EMPRESA;
  exception
    when no_data_found then
      Raise_application_error(-20000, 'Fnc. GEN_COT_TIPO_OPER: No se encuentra configurado la operaci'||chr(243)||'n');
  end get_data_stk_operacion;
  
  BEGIN
    SELECT CT.COT_COMPRA, CT.COT_VENTA
      INTO V_COT_COMPRA, V_COT_VENTA
      FROM STK_COTIZACION CT
     WHERE TO_CHAR(COT_FEC, 'DD/MM/YYYY') = NVL(TO_CHAR(P_FECHA,'DD/MM/YYYY'), TO_CHAR(SYSDATE,'DD/MM/YYYY'))
       AND COT_MON = P_MONEDA
       AND COT_EMPR = 1; ---SE DEFINIO QUE TODA LAS COTIZACIONES SOLO VA A ESTIRAR DE LA EMPRESA 1
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      IF P_MONEDA = 1 THEN
        RETURN 1;
      ELSE
        BEGIN
          SELECT CT.COT_COMPRA, CT.COT_VENTA
            INTO V_COT_COMPRA, V_COT_VENTA
            FROM STK_COTIZACION CT
           WHERE TO_CHAR(COT_FEC, 'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')
             AND COT_MON = P_MONEDA
             AND COT_EMPR = 1;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN

            RAISE_APPLICATION_ERROR(-20001,
                                    'Primero debe ingresar la cotizacion del dia ' ||
                                    P_FECHA || ' para la moneda ' ||
                                    P_MONEDA || '.!');
        END;
      END IF;
     END;

     IF V_IND_ER = 'S' THEN
       RETURN V_COT_COMPRA;
     ELSE
       RETURN V_COT_VENTA;
     END IF;
     END IF;

END GEN_COT_TIPO_OPER;
/
