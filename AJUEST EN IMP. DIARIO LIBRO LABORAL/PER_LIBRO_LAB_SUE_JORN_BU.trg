CREATE OR REPLACE TRIGGER "PER_LIBRO_LAB_SUE_JORN_BU"
  AFTER INSERT ON PER_LIBRO_LAB_SUE_JORN
  REFERENCING OLD AS O NEW AS N
  FOR EACH ROW
DECLARE
  -- local variables here
BEGIN
   DECLARE
    CURSOR EMPL IS
      SELECT P.EMPL_LEGAJO      LEGAJO,
             P.EMPL_APE         APELLIDO,
             P.EMPL_NOMBRE      NOMBRE,
             P.EMPL_FEC_INGRESO INGRESO,
             P.EMPL_FEC_SALIDA  SALIDA,
             DECODE(PF.FORMA_CODIGO,5,2,PF.FORMA_CODIGO) TIP_PAGO,---------------------------------------------------10/03/2020
             DECODE(PF.FORMA_CODIGO,5,'MENSUAL',PF.FORMA_DESC)TIP_PAGO_DESC,-----------------------------------------A PARTIR DE ESTA FECHA INCLUYE LOS DE AHM
             EMPL_SITUACION 
        FROM PER_EMPLEADO P, PER_FORMA_PAGO PF, PER_EMPLEADO_DEPTO A------------------------------------------------PERO SU FORMA DE PAGO PASARIA A MENSUAL
       WHERE P.EMPL_EMPRESA = :N.LIB_SUJO_EMPR
         AND P.EMPL_EMPRESA = PF.FORMA_EMPR
         AND P.EMPL_FORMA_PAGO = PF.FORMA_CODIGO
         AND P.EMPL_LEGAJO = A.PEREMPDEP_EMPL
         AND P.EMPL_EMPRESA = A.PEREMPDEP_EMPR
         AND A.PEREMPDEP_SUC  = :N.LIB_SUJO_SUC
         AND P.EMPL_FEC_INGRESO IS NOT NULL
        -- AND (nvl(P.EMPL_NRO_SEG_SOCIAL,0) IS NOT NULL )
         and decode(P.EMPL_NRO_SEG_SOCIAL, null, (decode(:N.LIB_SUJO_EMPR,2,0)), 0 ) is not null
         AND P.EMPL_PAGA_IPS = 'S'
         AND P.EMPL_FORMA_PAGO IN (1,2,5)
       --  AND (P.EMPL_SUCURSAL = :N.LIB_SUJO_SUC OR :N.LIB_SUJO_SUC IS NULL)
        AND P.EMPL_LEGAJO IN
             (SELECT C.LEGAJO
                FROM (SELECT P.EMPL_LEGAJO      LEGAJO,
                             P.EMPL_FEC_INGRESO INGRESO,
                             P.EMPL_FEC_SALIDA  SALIDA
                        FROM PER_EMPLEADO P
                       WHERE P.EMPL_EMPRESA =:N.LIB_SUJO_EMPR

                        AND  TO_DATE(EMPL_FEC_INGRESO, 'DD/MM/YYYY') <= TO_DATE(LAST_DAY(TO_DATE('01/' ||
                                                                        TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                                                                        TO_CHAR(:N.LIB_SUJO_ANO),'DD/MM/YYYY')), 'DD/MM/YYYY')
                        AND (P.EMPL_FEC_SALIDA >= TO_DATE('01/' ||
                                         TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                                         TO_CHAR(:N.LIB_SUJO_ANO),'DD/MM/YYYY')
                             OR  TO_DATE(P.EMPL_FEC_SALIDA, 'DD/MM/YYYY') IS NULL)

                      UNION
                      SELECT S.EMPSOL_EMPL_LEG      LEGAJO,
                             S.EMPSOL_FECHA_ING_COP INGRESO,
                             S.EMPSOL_FECHA_SAL_COP SALIDA
                        FROM PER_EMPL_SOL_CAMBIO_ESTADO S
                       WHERE S.EMPSOL_EMPR =:N.LIB_SUJO_EMPR
                         AND S.EMPSOL_MARC_INGR_MOD = 'I'
                         AND S.EMPSOL_ESTADO_EMP_SOL  = 'I'
                         AND TO_DATE(S.EMPSOL_FECHA_ING_COP, 'DD/MM/YYYY') <= TO_DATE(LAST_DAY(TO_DATE('01/' ||
                                                                        TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                                                                        TO_CHAR(:N.LIB_SUJO_ANO),'DD/MM/YYYY')), 'DD/MM/YYYY')
                        -- AND EMPSOL_FECHA_SAL_COP IS NULL

                         AND (EMPSOL_FECHA_SAL_COP >= TO_DATE('01/' ||
                                                      TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                                                      TO_CHAR(:N.LIB_SUJO_ANO), 'DD/MM/YYYY') OR EMPSOL_FECHA_SAL_COP IS NULL )
                         ) C
             WHERE C.INGRESO IS NOT NULL)
              GROUP BY P.EMPL_LEGAJO ,
             P.EMPL_APE  ,
             P.EMPL_NOMBRE  ,
             P.EMPL_FEC_INGRESO ,
             P.EMPL_FEC_SALIDA  ,
             PF.FORMA_CODIGO    ,
             PF.FORMA_DESC,
             EMPL_SITUACION
          
             ORDER BY APELLIDO ;


    V_DIA_LAB      CHAR(1);
    V_TIP          CHAR(1);
    V_CONT_DIA     NUMBER;
    V_IMPORTE      NUMBER;
    V_VACACIONES   NUMBER;
    V_AGUINALDO    NUMBER;
    V_BON_FAM      NUMBER;
    V_CONT_DIA_LIC NUMBER;
    V_FECHA_SUC   DATE;
    V_SUCURSAL_EMPL NUMBER;
    V_HORAS_OBRERO_REA NUMBER;
    v_dias_trabajados number;
    V_CONT_DIA1 number;
  BEGIN
    
  
     

    --  FOR I IN 1..12 LOOP  --LOOP PARA ANALISIS DE TODOS LOS MESES

    DELETE FROM FIN_FINI003_TEMP;

    FOR E IN EMPL LOOP
      
    
  /* IF E.LEGAJO = 20436 THEN
     raise_application_error(-20001,'aca');
   END IF;*/

    V_FECHA_SUC := LAST_DAY(TO_DATE('01/' ||
                                TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                                        :N.LIB_SUJO_ANO,
                                        'DD/MM/YYYY'));
                                        
                                        
   IF E.EMPL_SITUACION = 'I' AND E.SALIDA IS NULL THEN
     RAISE_APPLICATION_ERROR (-20001, 'EL EMPLEADO '||E.NOMBRE||' ESTA INACTIVO Y NO TIENE FECHA SALIDA');
   
   END IF;
    ---SIRVE PARA VER SI EL PERSONAL AUN ESTA EN LA MISMA SUCURSAL
      BEGIN
          SELECT PEREMPDEP_SUC
            INTO V_SUCURSAL_EMPL
            FROM PER_EMPLEADO_DEPTO
            WHERE PEREMPDEP_EMPL = E.LEGAJO
            AND PEREMPDEP_EMPR = :N.LIB_SUJO_EMPR
            AND PEREMPDEP_FEC = (SELECT MAX(PEREMPDEP_FEC)
                                   FROM PER_EMPLEADO_DEPTO
                                  WHERE PEREMPDEP_EMPL = E.LEGAJO
                                    AND PEREMPDEP_FEC < V_FECHA_SUC
                                    AND PEREMPDEP_EMPR = :N.LIB_SUJO_EMPR );
                                    
                                    
         IF V_SUCURSAL_EMPL = 11 AND :N.LIB_SUJO_EMPR = 1 THEN
           V_SUCURSAL_EMPL := 1;
         END IF;
         EXCEPTION
           WHEN NO_DATA_FOUND THEN
               V_SUCURSAL_EMPL := :N.LIB_SUJO_SUC;
         END;


      IF V_SUCURSAL_EMPL IS NULL THEN
        V_SUCURSAL_EMPL := :N.LIB_SUJO_SUC;
      END IF;


  IF V_SUCURSAL_EMPL =:N.LIB_SUJO_SUC THEN

      ---LOOP DEL CURSOR

      /*IF E.INGRESO <= LAST_DAY(TO_DATE('01/' || TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                                       :N.LIB_SUJO_ANO,
                                       'DD/MM/YYYY')) THEN*/
        --LA FECHA DE ENTRADA TIENE QUE SER MENOR AL                                                                                                              --ULTIMO DIA DEL MES ANALIZADO

     /* IF E.SALIDA IS NULL OR
           E.SALIDA >= TO_DATE('01/' || TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                               :N.LIB_SUJO_ANO,
                               'DD/MM/YYYY') THEN */
          --LA FECHA DE SALIDA TIENE QUE SER NULO O
          --MAYOR AL PRIMER DIA DEL MES ANALIZADO
          V_CONT_DIA     := 0;
          V_CONT_DIA_LIC := 0;
          V_DIAS_TRABAJADOS := 0;
          INSERT INTO FIN_FINI003_TEMP (FINI003_CLI) VALUES (E.LEGAJO);

          FOR D IN 1 .. TO_NUMBER(TO_CHAR(LAST_DAY(TO_DATE('01/' ||
                                                           TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                                                           :N.LIB_SUJO_ANO,
                                                           'DD/MM/YYYY')),
                                          'DD')) LOOP




            ---RECORRE DIA A DIA PARA SABER SI TRABAJO


            IF E.TIP_PAGO = 1 THEN
              V_TIP := 'J';
            ELSE
              V_TIP := 'N';
            END IF;



            V_DIA_LAB := PER_VERIFICAR_DIA_LABORAL(E.LEGAJO,
                                                   :N.LIB_SUJO_EMPR,
                                                   TO_DATE(TO_CHAR(D) || '/' ||
                                                           TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                                                           :N.LIB_SUJO_ANO,
                                                           'DD/MM/YYYY'),
                                                   E.INGRESO,
                                                   E.SALIDA,
                                                   V_TIP);


           IF V_DIA_LAB = 'C'  THEN
             V_DIA_LAB := 'A';
           END IF;    
           
           
            IF V_DIA_LAB = 'W'  THEN
             V_DIA_LAB := 'S';
           END IF;    
            IF D = 1 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_1 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              
               IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;
              

            ELSIF D = 2 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_2 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 3 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_3 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 4 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_4 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 5 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_5 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 6 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_6 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 7 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_7 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 8 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_8 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;


            ELSIF D = 9 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_9 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 10 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_10 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 11 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_11 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 12 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_12 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 13 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_13 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 14 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_14 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 15 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_15 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 16 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_16 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 17 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_17 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 18 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_18 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 19 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_19 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 20 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_20 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 21 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_21 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 22 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_22 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 23 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_23 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 24 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_24 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;


            ELSIF D = 25 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_25 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 26 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_26 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 27 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_27 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 28 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_28 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 29 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_29 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 30 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_30 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

              IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            ELSIF D = 31 THEN

              UPDATE FIN_FINI003_TEMP
                 SET FINI003_DIA_31 = V_DIA_LAB
               WHERE FINI003_CLI = E.LEGAJO;

             IF V_DIA_LAB IN ('S', 'L', 'R', 'P','V','U','A','X') THEN
                V_CONT_DIA := V_CONT_DIA + 1;

                IF V_DIA_LAB IN ('L', 'R', 'P','V','U','A','X') THEN
                  V_CONT_DIA_LIC := V_CONT_DIA_LIC + 1;
                END IF;
              END IF;

              IF V_DIA_LAB IN ('D','F') AND V_TIP = 'N' THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_CONT_DIA := V_CONT_DIA + 1;
              END IF;
              ----------------------------***
              IF V_DIA_LAB IN ('D','F','S')  THEN                    ------SI ES MENSUALERO SE CUENTA LOS FERIADOS Y DOMINGOS
                   V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS + 1;
              END IF;

            END IF;

          END LOOP;

/*
          IF V_TIP <> 'J' OR V_CONT_DIA = 0 THEN

            IF E.SALIDA BETWEEN (TO_DATE('01/' || TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                                         :N.LIB_SUJO_ANO,
                                         'DD/MM/YYYY')) AND
               (LAST_DAY(TO_DATE('01/' || TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                                 :N.LIB_SUJO_ANO,
                                 'DD/MM/YYYY'))) THEN

              V_CONT_DIA := TO_NUMBER(TO_CHAR(E.SALIDA, 'DD'));

            ELSIF E.INGRESO BETWEEN
                  (TO_DATE('01/' || TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                           :N.LIB_SUJO_ANO,
                           'DD/MM/YYYY')) AND
                  (LAST_DAY(TO_DATE('01/' || TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                                    :N.LIB_SUJO_ANO,
                                    'DD/MM/YYYY'))) THEN

              V_CONT_DIA := (TO_NUMBER(TO_CHAR(LAST_DAY(TO_DATE('01/' ||
                                                                TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                                                                :N.LIB_SUJO_ANO,
                                                                'DD/MM/YYYY')),
                                               'DD'))) -
                            ((TO_NUMBER(TO_CHAR(E.INGRESO, 'DD'))) - 1);

            ELSE

              V_CONT_DIA := TO_NUMBER(TO_CHAR(LAST_DAY(TO_DATE('01/' ||
                                                               TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                                                               :N.LIB_SUJO_ANO,
                                                               'DD/MM/YYYY')),
                                              'DD'));

            END IF;

            -- V_CONT_DIA := V_CONT_DIA_LIC;
          END IF;*/
          
      IF V_CONT_DIA = 31 THEN   
            V_CONT_DIA := 30;
            V_CONT_DIA1 := 31;
      END IF;    
  IF :N.LIB_SUJO_EMPR = 1 THEN
       IF E.TIP_PAGO = 2 THEN
        /*IF V_DIAS_TRABAJADOS = 31 THEN 
                  V_CONT_DIA := V_DIAS_TRABAJADOS-1;
                ELSE
                  V_CONT_DIA := V_DIAS_TRABAJADOS;
          END IF;*/
       BEGIN
        SELECT PDOC_CANT_DIAS
         INTO V_CONT_DIA
          FROM PER_DOCUMENTO A
         WHERE  PDOC_EMPR = :N.LIB_SUJO_EMPR
           AND PDOC_EMPLEADO =E.LEGAJO-----V.EMPLEADO ;
           AND A.PDOC_CANT_DIAS IS NOT NULL
            and a.pdoc_form = 'PERI005'
           AND TO_CHAR(A.PDOC_FEC,'MM/YYYY') =  to_char (TO_DATE('01/' ||TO_CHAR(:N.LIB_SUJO_MES) || '/' ||TO_CHAR(:N.LIB_SUJO_ANO),'DD/MM/YYYY'), 'MM/YYYY')
           group by PDOC_CANT_DIAS;
         EXCEPTION WHEN NO_DATA_FOUND THEN   
         IF V_DIAS_TRABAJADOS = 31 THEN 
                  V_CONT_DIA := V_DIAS_TRABAJADOS-1;
                else
                  V_CONT_DIA := V_DIAS_TRABAJADOS;
                  END IF;
           WHEN TOO_MANY_ROWS THEN   
           IF V_DIAS_TRABAJADOS = 31 THEN 
                  V_CONT_DIA := V_DIAS_TRABAJADOS-1;
                else
                  V_CONT_DIA := V_DIAS_TRABAJADOS;
                  END IF;
         END;
         
         
          
        ELSE
                IF V_CONT_DIA = 31 THEN 
                  V_CONT_DIA := (V_CONT_DIA-1 )- V_CONT_DIA_LIC;
                else
                  V_CONT_DIA := (V_CONT_DIA)- V_CONT_DIA_LIC;
                
                END IF;
                
                
                
          END IF;
     ELSE  
       
     IF   E.TIP_PAGO = 1 THEN   
       IF V_CONT_DIA = 31 THEN 
                  V_CONT_DIA := (V_CONT_DIA-1 )- V_CONT_DIA_LIC;
                else
                  V_CONT_DIA := (V_CONT_DIA)- V_CONT_DIA_LIC;
                
                END IF;
          ELSE      
                IF V_CONT_DIA1 = 31 THEN 
                  V_CONT_DIA := V_DIAS_TRABAJADOS-1;
                ELSE
                  V_CONT_DIA := V_DIAS_TRABAJADOS;
                 END IF;
      END IF;
         ---- V_CONT_DIA := V_CONT_DIA - V_CONT_DIA_LIC;
      END IF;
      
       IF TO_CHAR (TO_DATE('01/' ||TO_CHAR(:N.LIB_SUJO_MES) || '/' ||TO_CHAR(:N.LIB_SUJO_ANO),'DD/MM/YYYY'), 'MM/YYYY') = '02/'||TO_CHAR(:N.LIB_SUJO_ANO) 
         AND( V_CONT_DIA = 28 OR V_CONT_DIA = 29 ) AND E.TIP_PAGO = 2 AND :N.LIB_SUJO_EMPR = 1 THEN
         V_CONT_DIA  := 30;
       ELSE
         V_CONT_DIA :=V_CONT_DIA;
       END IF;
       
       IF V_CONT_DIA = 31 THEN
         V_CONT_DIA := 30;
      ELSE
         V_CONT_DIA := V_CONT_DIA;
      
      END IF;
    /*  IF TO_CHAR(LAST_DAY (E.INGRESO),'DD') = 31 THEN
       
        IF TO_CHAR(E.INGRESO,'MM/YYYY') = TO_CHAR (TO_DATE('01/' ||TO_CHAR(:N.LIB_SUJO_MES) || '/' ||TO_CHAR(:N.LIB_SUJO_ANO),'DD/MM/YYYY'), 'MM/YYYY') THEN
          V_CONT_DIA := V_CONT_DIA-1;
         END IF;
      END IF;*/
      
          SELECT SUM(DECODE(PC.PCON_IND_SUM_LIB_LAB, 'S', CO."CREDITO", 0)) IMPORTE,
                 SUM(DECODE(CO."PCON_CLAVE", 6, CO."CREDITO", 0)) VACACIONES,
                 SUM(DECODE(CO."PCON_CLAVE", 3, CO."CREDITO", 0)) AGUINALDO,
                 SUM(DECODE(CO."PCON_CLAVE", 8, CO."CREDITO", 0)) BON_FAM
            INTO V_IMPORTE, V_VACACIONES, V_AGUINALDO, V_BON_FAM
            FROM CUBO_EMPLEADOS_PERL011_EMPR CO, PER_CONCEPTO PC
           WHERE CO."EMPL_EMPRESA" = :N.LIB_SUJO_EMPR
             AND CO."EMPL_EMPRESA" = PC.PCON_EMPR
             AND CO."PCON_CLAVE" = PC.PCON_CLAVE
             AND CO."EMPL_LEGAJO" = E.LEGAJO
             AND (SELECT decode(EMPL_NRO_SEG_SOCIAL, null, (decode(:N.LIB_SUJO_EMPR,2,0)), 0 )
             --decode(EMPL_EMPRESA,2,0,EMP.EMPL_NRO_SEG_SOCIAL) --EMP.EMPL_NRO_SEG_SOCIAL
                    FROM PER_EMPLEADO EMP
                   WHERE EMP.EMPL_EMPRESA = :N.LIB_SUJO_EMPR
                     AND EMP.EMPL_LEGAJO = CO."EMPL_LEGAJO") IS NOT NULL
            /* AND TO_DATE('01/' || TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                         :N.LIB_SUJO_ANO,
                         'DD/MM/YYYY') BETWEEN CO.PERI_FEC_INI AND
                 CO.PERI_FEC_FIN*/
             AND TO_CHAR(FILTRO_FECHA,'MM/YYYY') =  to_char (TO_DATE('01/' ||TO_CHAR(:N.LIB_SUJO_MES) || '/' ||TO_CHAR(:N.LIB_SUJO_ANO),'DD/MM/YYYY'), 'MM/YYYY')
             AND CO."PCON_IND_DBCR" = 'C';


             ---------calcula la hora total de horas trabajadas por los jornaleros, que mas adelante se utilizara en el resumen general
            IF V_TIP = 'J' THEN
               IF :N.LIB_SUJO_EMPR = 1 THEN
                 SELECT NVL(ROUND(SUM(HD) + SUM(HN) + SUM(HFD) + SUM(HFN)),0) HORAS_OBRERO
                  INTO V_HORAS_OBRERO_REA
                  FROM PER_PERL029_FINAL_TEMP T
                 WHERE EMPR = :N.LIB_SUJO_EMPR
                   AND FECHA BETWEEN TO_DATE('01/' ||
                                             TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                                             TO_CHAR(:N.LIB_SUJO_ANO),'DD/MM/YYYY')
                                 AND  TO_DATE(LAST_DAY(TO_DATE('01/' ||
                                             TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                                             TO_CHAR(:N.LIB_SUJO_ANO),'DD/MM/YYYY')), 'DD/MM/YYYY')
                   AND EMPL_LEGAJO =E.LEGAJO;
                ELSE
                  SELECT NVL(ROUND(SUM(HD) + SUM(HN) + SUM(HFD) + SUM(HFN)),0) HORAS_OBRERO
                  INTO V_HORAS_OBRERO_REA
                  FROM PER_PERL029_TEMP T
                 WHERE EMPR = :N.LIB_SUJO_EMPR
                   AND FECHA BETWEEN TO_DATE('01/' ||
                                             TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                                             TO_CHAR(:N.LIB_SUJO_ANO),'DD/MM/YYYY')
                                 AND  TO_DATE(LAST_DAY(TO_DATE('01/' ||
                                             TO_CHAR(:N.LIB_SUJO_MES) || '/' ||
                                             TO_CHAR(:N.LIB_SUJO_ANO),'DD/MM/YYYY')), 'DD/MM/YYYY')
                   AND EMPL_LEGAJO =E.LEGAJO;
                 END IF;

             ELSE
               V_HORAS_OBRERO_REA := 0;
             END IF;

          /*   19/08/2022 12:58:52 @PabloACespedes \(^-^)/
                ajustado por solicitud de RRHH Hilagro ticket #36115
               
          UPDATE FIN_FINI003_TEMP
             SET FINI003_FOR_PAG   = E.TIP_PAGO,
                 FINI003_IMP_UNIT  = ROUND(DECODE(V_TIP,
                                                  'J',
                                                  DECODE(V_CONT_DIA,
                                                         0,
                                                         0,
                                                         (V_IMPORTE /26)),
                                                  (V_IMPORTE / 30))),
                 FINI003_DIAS_TRAB = V_CONT_DIA,
                 FINI003_HS_TRAB  =
                 (V_CONT_DIA * 8),
                 FINI003_IMPORTE   = V_IMPORTE,
                 FINI003_VACAC     = V_VACACIONES,
                 FINI003_BF        = V_BON_FAM,
                 FINI003_AGUIN     = V_AGUINALDO,
                 FINI003_ARTICULO  = V_HORAS_OBRERO_REA
           WHERE FINI003_CLI = E.LEGAJO;
           */
           -- AHI ARRIBA ESTA LO QUE ANTERIORMENTE ESTABA \[�-�]
           UPDATE FIN_FINI003_TEMP
             SET FINI003_FOR_PAG   = E.TIP_PAGO,
                 FINI003_IMP_UNIT  = ROUND( V_IMPORTE / case when nvl(v_cont_dia, 0) = 0 then 1 else v_cont_dia end ),
                 FINI003_DIAS_TRAB = V_CONT_DIA,
                 FINI003_HS_TRAB   = (V_CONT_DIA * 8),
                 FINI003_IMPORTE   = V_IMPORTE,
                 FINI003_VACAC     = V_VACACIONES,
                 FINI003_BF        = V_BON_FAM,
                 FINI003_AGUIN     = V_AGUINALDO,
                 FINI003_ARTICULO  = V_HORAS_OBRERO_REA
           WHERE FINI003_CLI = E.LEGAJO;
   --     END IF;
     END IF;
    END LOOP;
    
     /*   
   IF gen_devuelve_user = 'JBENITEZ' THEN
     raise_application_error(-20001,'aca2');
   END IF;*/
   
    FOR X IN (SELECT T.FINI003_CLI,
                     T.FINI003_DIA_1,
                     T.FINI003_DIA_2,
                     T.FINI003_DIA_3,
                     T.FINI003_DIA_4,
                     T.FINI003_DIA_5,
                     T.FINI003_DIA_6,
                     T.FINI003_DIA_7,
                     T.FINI003_DIA_8,
                     T.FINI003_DIA_9,
                     T.FINI003_DIA_10,
                     T.FINI003_DIA_11,
                     T.FINI003_DIA_12,
                     T.FINI003_DIA_13,
                     T.FINI003_DIA_14,
                     T.FINI003_DIA_15,
                     T.FINI003_DIA_16,
                     T.FINI003_DIA_17,
                     T.FINI003_DIA_18,
                     T.FINI003_DIA_19,
                     T.FINI003_DIA_20,
                     T.FINI003_DIA_21,
                     T.FINI003_DIA_22,
                     T.FINI003_DIA_23,
                     T.FINI003_DIA_24,
                     T.FINI003_DIA_25,
                     T.FINI003_DIA_26,
                     T.FINI003_DIA_27,
                     T.FINI003_DIA_28,
                     T.FINI003_DIA_29,
                     T.FINI003_DIA_30,
                     T.FINI003_DIA_31,
                     T.FINI003_FOR_PAG,
                     T.FINI003_IMP_UNIT,
                     T.FINI003_DIAS_TRAB,
                     T.FINI003_HS_TRAB,
                     T.FINI003_IMPORTE,
                     T.FINI003_VACAC,
                     T.FINI003_BF,
                     T.FINI003_AGUIN,
                     T.FINI003_ARTICULO
                FROM FIN_FINI003_TEMP T) LOOP

      INSERT INTO PER_LIBRO_LAB_SUE_JORN_DET
        (LIB_SUDET_CLAVE,
         LIB_SUDET_EMPR,
         LIB_SUDET_LEGAJO,
         LIB_SUDET_MES,
         LIB_SUDET_FOR_PAGO,
         LIB_SUDET_IMP_UNIT,
         LIB_SUDET_DIAS_TRAB,
         LIB_SUDET_HORAS_TRAB,
         LIB_SUDET_IMPORTE,
         LIB_SUDET_VACACION,
         LIB_SUDET_BONIF_FAM,
         LIB_SUDET_AGUIN,
         LIB_SUDET_DIA_1,
         LIB_SUDET_DIA_2,
         LIB_SUDET_DIA_3,
         LIB_SUDET_DIA_4,
         LIB_SUDET_DIA_5,
         LIB_SUDET_DIA_6,
         LIB_SUDET_DIA_7,
         LIB_SUDET_DIA_8,
         LIB_SUDET_DIA_9,
         LIB_SUDET_DIA_10,
         LIB_SUDET_DIA_11,
         LIB_SUDET_DIA_12,
         LIB_SUDET_DIA_13,
         LIB_SUDET_DIA_14,
         LIB_SUDET_DIA_15,
         LIB_SUDET_DIA_16,
         LIB_SUDET_DIA_17,
         LIB_SUDET_DIA_18,
         LIB_SUDET_DIA_19,
         LIB_SUDET_DIA_20,
         LIB_SUDET_DIA_21,
         LIB_SUDET_DIA_22,
         LIB_SUDET_DIA_23,
         LIB_SUDET_DIA_24,
         LIB_SUDET_DIA_25,
         LIB_SUDET_DIA_26,
         LIB_SUDET_DIA_27,
         LIB_SUDET_DIA_28,
         LIB_SUDET_DIA_29,
         LIB_SUDET_DIA_30,
         LIB_SUDET_DIA_31,
         LIB_SUDET_HOR_OBR,
         LIB_SUDET_IMPO_IMP)

      VALUES
        (:N.LIB_SUJO_CLAVE,
         :N.LIB_SUJO_EMPR,
         X.FINI003_CLI,
         :N.LIB_SUJO_MES,
         X.FINI003_FOR_PAG,
         nvl(X.FINI003_IMP_UNIT,0),
         X.FINI003_DIAS_TRAB,
         X.FINI003_HS_TRAB,
         nvl(X.FINI003_IMPORTE,0),
         nvl(X.FINI003_VACAC,0),
         nvl(X.FINI003_BF,0),
         nvl(X.FINI003_AGUIN,0),
         X.FINI003_DIA_1,
         X.FINI003_DIA_2,
         X.FINI003_DIA_3,
         X.FINI003_DIA_4,
         X.FINI003_DIA_5,
         X.FINI003_DIA_6,
         X.FINI003_DIA_7,
         X.FINI003_DIA_8,
         X.FINI003_DIA_9,
         X.FINI003_DIA_10,
         X.FINI003_DIA_11,
         X.FINI003_DIA_12,
         X.FINI003_DIA_13,
         X.FINI003_DIA_14,
         X.FINI003_DIA_15,
         X.FINI003_DIA_16,
         X.FINI003_DIA_17,
         X.FINI003_DIA_18,
         X.FINI003_DIA_19,
         X.FINI003_DIA_20,
         X.FINI003_DIA_21,
         X.FINI003_DIA_22,
         X.FINI003_DIA_23,
         X.FINI003_DIA_24,
         X.FINI003_DIA_25,
         X.FINI003_DIA_26,
         X.FINI003_DIA_27,
         X.FINI003_DIA_28,
         X.FINI003_DIA_29,
         X.FINI003_DIA_30,
         X.FINI003_DIA_31,
         X.FINI003_ARTICULO,
         nvl(X.FINI003_IMPORTE,0));

    END LOOP;
    --  END LOOP;
  END;
  --Commit;
END PER_LIBRO_LAB_SUE_JORN_BU;
/
