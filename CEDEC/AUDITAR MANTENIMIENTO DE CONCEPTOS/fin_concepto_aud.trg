create or replace trigger fin_concepto_aud
  before insert or update or delete
  on FIN_CONCEPTO 
  for each row
  -- Author  : @PabloACespedes \(^u^)/
  -- Created : 18/08/2022 9:24:20
  -- TRG DE AUDITORIA
declare
  l_operacion varchar2(3);
  l_user      varchar2(200);
  l_terminal  varchar2(255);
begin
  <<get_terminal>>
  begin
    select rtrim(machine)||'.'||client_info
      into l_terminal
      from v$session
      where audsid = userenv('sessionid');
  exception
    when others then
      raise_application_error( -20000, sqlerrm );
  end get_terminal;

  l_user := nvl(sys_context('APEX$SESSION','APP_USER'),user);

  if inserting or updating then
    case when inserting then
       l_operacion := 'INS';
    else
       l_operacion := 'UPD';
    end case;

    insert into fin_concepto_jn(jn_operacion,
                                jn_datetime,
                                jn_user,
                                jn_terminal,
                                fcon_clave,
                                fcon_empr,
                                fcon_codigo,
                                fcon_desc,
                                fcon_tipo_saldo,
                                fcon_ind_clave_importacion,
                                fcon_clave_ctaco,
                                fcon_resum_libro_caja,
                                fcon_restringido,
                                fcon_tipo_gasto,
                                fcon_canal,
                                fcon_div_canal,
                                fcon_suc,
                                fcon_ind_canal_req,
                                fcon_movil,
                                fcon_rest,
                                fcon_grupo,
                                fcon_camion,
                                fcon_tra_ind_seguro,
                                fcon_tra_ind_combus,
                                fcon_destino_uso,
                                fcon_linea_neg,
                                fcon_categoria,
                                fcon_control_km,
                                fcon_estado,
                                fcon_ind_inact_func,
                                fcon_canal_beta,
                                fcon_carreta,
                                fcon_porc_ret,
                                fcon_ind_viatico_resp,
                                fcon_hab_baja_stk,
                                fcon_hab_ent_prod,
                                fcon_pais_export,
                                fcon_cli_export,
                                fcon_ind_afecta_linea)
                         values(l_operacion,
                                current_date,
                                l_user,
                                l_terminal,                
                                :new.fcon_clave,
                                :new.fcon_empr,
                                :new.fcon_codigo,
                                :new.fcon_desc,
                                :new.fcon_tipo_saldo,
                                :new.fcon_ind_clave_importacion,
                                :new.fcon_clave_ctaco,
                                :new.fcon_resum_libro_caja,
                                :new.fcon_restringido,
                                :new.fcon_tipo_gasto,
                                :new.fcon_canal,
                                :new.fcon_div_canal,
                                :new.fcon_suc,
                                :new.fcon_ind_canal_req,
                                :new.fcon_movil,
                                :new.fcon_rest,
                                :new.fcon_grupo,
                                :new.fcon_camion,
                                :new.fcon_tra_ind_seguro,
                                :new.fcon_tra_ind_combus,
                                :new.fcon_destino_uso,
                                :new.fcon_linea_neg,
                                :new.fcon_categoria,
                                :new.fcon_control_km,
                                :new.fcon_estado,
                                :new.fcon_ind_inact_func,
                                :new.fcon_canal_beta,
                                :new.fcon_carreta,
                                :new.fcon_porc_ret,
                                :new.fcon_ind_viatico_resp,
                                :new.fcon_hab_baja_stk,
                                :new.fcon_hab_ent_prod,
                                :new.fcon_pais_export,
                                :new.fcon_cli_export,
                                :new.fcon_ind_afecta_linea
                                );

   elsif deleting then
      l_operacion := 'DEL';
      
      insert into fin_concepto_jn(jn_operacion,
                                jn_datetime,
                                jn_user,
                                jn_terminal,
                                fcon_clave,
                                fcon_empr,
                                fcon_codigo,
                                fcon_desc,
                                fcon_tipo_saldo,
                                fcon_ind_clave_importacion,
                                fcon_clave_ctaco,
                                fcon_resum_libro_caja,
                                fcon_restringido,
                                fcon_tipo_gasto,
                                fcon_canal,
                                fcon_div_canal,
                                fcon_suc,
                                fcon_ind_canal_req,
                                fcon_movil,
                                fcon_rest,
                                fcon_grupo,
                                fcon_camion,
                                fcon_tra_ind_seguro,
                                fcon_tra_ind_combus,
                                fcon_destino_uso,
                                fcon_linea_neg,
                                fcon_categoria,
                                fcon_control_km,
                                fcon_estado,
                                fcon_ind_inact_func,
                                fcon_canal_beta,
                                fcon_carreta,
                                fcon_porc_ret,
                                fcon_ind_viatico_resp,
                                fcon_hab_baja_stk,
                                fcon_hab_ent_prod,
                                fcon_pais_export,
                                fcon_cli_export,
                                fcon_ind_afecta_linea)
                         values(l_operacion,
                                current_date,
                                l_user,
                                l_terminal,                
                                :old.fcon_clave,
                                :old.fcon_empr,
                                :old.fcon_codigo,
                                :old.fcon_desc,
                                :old.fcon_tipo_saldo,
                                :old.fcon_ind_clave_importacion,
                                :old.fcon_clave_ctaco,
                                :old.fcon_resum_libro_caja,
                                :old.fcon_restringido,
                                :old.fcon_tipo_gasto,
                                :old.fcon_canal,
                                :old.fcon_div_canal,
                                :old.fcon_suc,
                                :old.fcon_ind_canal_req,
                                :old.fcon_movil,
                                :old.fcon_rest,
                                :old.fcon_grupo,
                                :old.fcon_camion,
                                :old.fcon_tra_ind_seguro,
                                :old.fcon_tra_ind_combus,
                                :old.fcon_destino_uso,
                                :old.fcon_linea_neg,
                                :old.fcon_categoria,
                                :old.fcon_control_km,
                                :old.fcon_estado,
                                :old.fcon_ind_inact_func,
                                :old.fcon_canal_beta,
                                :old.fcon_carreta,
                                :old.fcon_porc_ret,
                                :old.fcon_ind_viatico_resp,
                                :old.fcon_hab_baja_stk,
                                :old.fcon_hab_ent_prod,
                                :old.fcon_pais_export,
                                :old.fcon_cli_export,
                                :old.fcon_ind_afecta_linea
                                );
   end if;
end fin_concepto_aud;
/
