prompt --application/set_environment
set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_200100 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2020.03.31'
,p_release=>'20.1.0.00.13'
,p_default_workspace_id=>2933390818164287
,p_default_application_id=>100
,p_default_id_offset=>7656439127971453890
,p_default_owner=>'ADCS'
);
end;
/
 
prompt APPLICATION 100 - FINANZAS
--
-- Application Export:
--   Application:     100
--   Name:            FINANZAS
--   Date and Time:   07:31 Thursday September 1, 2022
--   Exported By:     ADMIN
--   Flashback:       0
--   Export Type:     Page Export
--   Manifest
--     PAGE: 158
--   Manifest End
--   Version:         20.1.0.00.13
--   Instance ID:     218232033625678
--

begin
null;
end;
/
prompt --application/pages/delete_00158
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>158);
end;
/
prompt --application/pages/page_00158
begin
wwv_flow_api.create_page(
 p_id=>158
,p_user_interface_id=>wwv_flow_api.id(7567100396693658791)
,p_name=>'Documentos Externos'
,p_alias=>'DOCUMENTOS-EXTERNOS'
,p_step_title=>'Documentos Externos'
,p_autocomplete_on_off=>'OFF'
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.jQuery(function() {',
'  apex.theme42.util.configAPEXMsgs({',
'    autoDismiss: true,',
'    duration: 5000  // duration is optional (Default is 3000 milliseconds)',
'  });',
'});'))
,p_inline_css=>'.medioTime{font-size: 2.5rem !important;}'
,p_page_template_options=>'#DEFAULT#'
,p_required_role=>wwv_flow_api.id(7556625975688621252)
,p_last_updated_by=>'ADCS'
,p_last_upd_yyyymmddhh24miss=>'20220830153254'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(67689289565048901)
,p_name=>unistr('Configuraci\00F3n')
,p_template=>wwv_flow_api.id(7570304732549401240)
,p_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#:t-IRR-region--noBorders'
,p_component_template_options=>'#DEFAULT#:t-Report--stretch:t-Report--staticRowColors:t-Report--rowHighlightOff:t-Report--hideNoPagination'
,p_new_grid_row=>false
,p_display_point=>'BODY'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
unistr('select ''En ''||e.empr_razon_social ||'' la l\00EDnea de Negocio ''||l.lin_codigo ||''-''||l.lin_desc||'' utilizar\00E1 el concepto ''||fc.fcon_codigo||''-''||fc.fcon_desc ||'' de la empresa ''||'),
'       (select ex.empr_razon_social from gen_empresa ex where ex.empr_codigo = :P_EMPRESA) x,',
'       d.id editar',
'from FAC_LINEA_NEGOCIO_det_CAGRO d',
'inner join fac_linea_negocio_cagro l on ( l.lin_codigo = d.linea_neg_id and l.lin_empr = d.empresa_id)',
'inner join fin_concepto fc on ( fc.fcon_clave = d.concepto_id and fc.fcon_empr = :P_EMPRESA )',
'inner join gen_empresa e on (e.empr_codigo = d.empresa_id)',
'where empresa_grupo_id = case ',
'                           when :P_EMPRESA = ''1'' then ',
'                             22 --> @Cagro en Hilagro',
'                           else ',
'                             to_number(:P_EMPRESA)',
'                         end'))
,p_ajax_enabled=>'Y'
,p_ajax_items_to_submit=>'P_EMPRESA'
,p_query_row_template=>wwv_flow_api.id(7570326356984401251)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(67690738256048916)
,p_query_column_id=>1
,p_column_alias=>'X'
,p_column_display_sequence=>1
,p_column_heading=>unistr('Reglas para L\00EDneas de Negocio de otras Empresas')
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<h6>#X#</h6>'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(67690875505048917)
,p_query_column_id=>2
,p_column_alias=>'EDITAR'
,p_column_display_sequence=>2
,p_column_heading=>'Eliminar'
,p_use_as_row_header=>'N'
,p_column_link=>'#'
,p_column_linktext=>'<i class="fa fa-times u-danger-text medioTime"></i>'
,p_column_link_attr=>'onclick="$s(''P158_ID_DEL_DET_LINEA_NEG'',#EDITAR#)"'
,p_column_alignment=>'CENTER'
,p_derived_column=>'N'
,p_include_in_export=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(67689374485048902)
,p_plug_name=>'Lista de Documentos'
,p_region_template_options=>'#DEFAULT#:t-Region--noPadding:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(67692270299048931)
,p_plug_name=>'FIN DOCUMENTO CAGRO'
,p_parent_plug_id=>wwv_flow_api.id(67689374485048902)
,p_region_template_options=>'#DEFAULT#:t-IRR-region--noBorders'
,p_plug_template=>wwv_flow_api.id(7570304732549401240)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select f.doc_clave,',
'       f.doc_fec_doc fecha_documento,',
'       f.doc_fec_oper fecha_operacion,',
'       tm.tmov_codigo ||''-''||tm.tmov_desc tipo_movimiento,',
'       p.prov_codigo prov_cod,',
'       p.prov_razon_social proveedor,',
'       f.doc_obs observacion,',
'       f.doc_bruto_exen_loc monto,',
'       e.empl_legajo legajo,',
'       e.empl_nombre ||'' ''|| e.empl_ape funcionario,',
'       '''' detalle',
'from fin_documento_cagro f',
'inner join fin_proveedor p on (p.prov_codigo = f.doc_prov)',
'inner join per_empleado e on ( e.empl_legajo = f.doc_empleado and e.empl_empresa = p.prov_empr )',
'inner join gen_tipo_mov tm on ( tm.tmov_codigo = f.doc_tipo_mov and tm.tmov_empr = e.empl_empresa )',
'left  join fin_import_doc_ext_det ed on (ed.doc_clave_externa = f.doc_clave )',
'where p.prov_empr = to_number(:p_empresa)',
'and ed.id is null',
'and ((ed.id is not null and ed.empresa_id = to_number(:p_empresa) or ed.id is null))',
'and case when f.doc_empr = 22 then 1 else f.doc_empr end = to_number(:p_empresa);'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'MILLIMETERS'
,p_prn_paper_size=>'A4'
,p_prn_width=>297
,p_prn_height=>210
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header=>'FIN DOCUMENTO CAGRO'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#EEEEEE'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'bold'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#FFFFFF'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_prn_border_color=>'#666666'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(68915635767644245)
,p_max_row_count=>'1000000'
,p_no_data_found_message=>'Sin Registros'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:EMAIL:XLS:PDF:RTF'
,p_owner=>'ADMIN'
,p_internal_uid=>68915635767644245
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(68915794643644246)
,p_db_column_name=>'DOC_CLAVE'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Doc Clave'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(68955156334090147)
,p_db_column_name=>'FECHA_OPERACION'
,p_display_order=>20
,p_column_identifier=>'K'
,p_column_label=>unistr('Fecha Operaci\00F3n')
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd/mm/yyyy'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(68955019795090146)
,p_db_column_name=>'FECHA_DOCUMENTO'
,p_display_order=>30
,p_column_identifier=>'J'
,p_column_label=>'Fecha Documento'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd/mm/yyyy'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(68915811251644247)
,p_db_column_name=>'TIPO_MOVIMIENTO'
,p_display_order=>40
,p_column_identifier=>'B'
,p_column_label=>'Tipo Movimiento'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(68923164625662001)
,p_db_column_name=>'LEGAJO'
,p_display_order=>50
,p_column_identifier=>'F'
,p_column_label=>'Legajo'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(68923258404662002)
,p_db_column_name=>'FUNCIONARIO'
,p_display_order=>60
,p_column_identifier=>'G'
,p_column_label=>'Funcionario'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(68915958704644248)
,p_db_column_name=>'PROVEEDOR'
,p_display_order=>70
,p_column_identifier=>'C'
,p_column_label=>'Proveedor'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(68916057788644249)
,p_db_column_name=>'OBSERVACION'
,p_display_order=>80
,p_column_identifier=>'D'
,p_column_label=>'Observacion'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(68916150988644250)
,p_db_column_name=>'MONTO'
,p_display_order=>90
,p_column_identifier=>'E'
,p_column_label=>'Monto'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_format_mask=>'999G999G999G999G999G999G990'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(68923371105662003)
,p_db_column_name=>'DETALLE'
,p_display_order=>100
,p_column_identifier=>'H'
,p_column_label=>'Detalle'
,p_column_link=>'#'
,p_column_linktext=>'<span class="fa fa-search u-info-text medioTime" aria-hidden="true"></span>'
,p_column_link_attr=>'onclick="$s(''P158_FIN_DOC_CAGRO_DET'', #DOC_CLAVE#)"'
,p_allow_sorting=>'N'
,p_allow_filtering=>'N'
,p_allow_highlighting=>'N'
,p_allow_ctrl_breaks=>'N'
,p_allow_aggregations=>'N'
,p_allow_computations=>'N'
,p_allow_charting=>'N'
,p_allow_group_by=>'N'
,p_allow_pivot=>'N'
,p_allow_hide=>'N'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(68951639296090112)
,p_db_column_name=>'PROV_COD'
,p_display_order=>110
,p_column_identifier=>'I'
,p_column_label=>'Prov Cod'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(68933775555662380)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'689338'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'TIPO_MOVIMIENTO:FECHA_OPERACION:FECHA_DOCUMENTO:DOC_CLAVE:LEGAJO:FUNCIONARIO:PROV_COD:PROVEEDOR:OBSERVACION:MONTO:DETALLE:'
,p_break_on=>'TIPO_MOVIMIENTO'
,p_break_enabled_on=>'TIPO_MOVIMIENTO'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(67690971696048918)
,p_plug_name=>'Configurar Lineas de Negocio'
,p_region_template_options=>'#DEFAULT#:t-Region--accent6:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(67692320090048932)
,p_name=>'Detalle Concepto'
,p_template=>wwv_flow_api.id(7570302609627401239)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#:js-dialog-autoheight:js-dialog-size600x400'
,p_component_template_options=>'#DEFAULT#:t-AVPList--leftAligned'
,p_display_point=>'REGION_POSITION_04'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select cc.fcon_clave      concepto_clave,',
'       cc.fcon_codigo     concepto_cod,',
'       cc.fcon_desc       concepto,',
'       (select c.ctac_clave||''-''||c.ctac_desc x',
'        from fin_configuracion f',
'        inner join cnt_cuenta c on (c.ctac_clave = f.conf_cta_adel_prov and c.ctac_empr = f.conf_empr)       ',
'        where f.conf_empr = to_number(:p_empresa)',
'       ) cuenta,',
'       cc.fcon_tipo_saldo tipo_saldo,',
'       c.dcon_exen_loc    exento',
'from fin_doc_concepto_cagro c',
'inner join fin_concepto cc on ( cc.fcon_clave = c.dcon_clave_concepto )',
'where c.dcon_clave_doc = to_number(:P158_FIN_DOC_CAGRO_DET)',
'and   case when c.dcon_empr = 22 then 1 else c.dcon_empr end = to_number(:p_empresa)',
'and   cc.fcon_empr  = to_number(:p_empresa)'))
,p_ajax_enabled=>'Y'
,p_ajax_items_to_submit=>'P158_FIN_DOC_CAGRO_DET'
,p_query_row_template=>wwv_flow_api.id(7570329342594401252)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(68926910786662039)
,p_query_column_id=>1
,p_column_alias=>'CONCEPTO_CLAVE'
,p_column_display_sequence=>1
,p_column_heading=>'Concepto Clave'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_when_cond_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_display_when_condition=>'APP_USER'
,p_display_when_condition2=>'ADCS'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(68927010882662040)
,p_query_column_id=>2
,p_column_alias=>'CONCEPTO_COD'
,p_column_display_sequence=>2
,p_column_heading=>'Concepto Cod'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(68927117896662041)
,p_query_column_id=>3
,p_column_alias=>'CONCEPTO'
,p_column_display_sequence=>3
,p_column_heading=>'Concepto'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(68927506368662045)
,p_query_column_id=>4
,p_column_alias=>'CUENTA'
,p_column_display_sequence=>5
,p_column_heading=>'Cuenta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(68927247434662042)
,p_query_column_id=>5
,p_column_alias=>'TIPO_SALDO'
,p_column_display_sequence=>4
,p_column_heading=>'Tipo Saldo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(68927615057662046)
,p_query_column_id=>6
,p_column_alias=>'EXENTO'
,p_column_display_sequence=>6
,p_column_heading=>'Exento'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G999G999G990'
,p_column_alignment=>'RIGHT'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(68950805750090104)
,p_plug_name=>'Resumen'
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(7570305860100401241)
,p_plug_display_sequence=>35
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(68950990939090105)
,p_name=>'{viewData}'
,p_parent_plug_id=>wwv_flow_api.id(68950805750090104)
,p_template=>wwv_flow_api.id(7570284778490401231)
,p_display_sequence=>8
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#:t-MediaList--showBadges:u-colors:t-MediaList--horizontal:t-MediaList--large force-fa-lg:t-Report--hideNoPagination'
,p_display_point=>'BODY'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
unistr('select ''Res\00FAmen'' LIST_TITLE,'),
'       ''Total de Registros: ''|| count(1)  LIST_BADGE,',
'       ''Monto Total: '' ||to_char(sum(nvl(f.doc_bruto_exen_loc, 0 )), ''999G999G999G999G999G999G990'') LIST_TEXT,',
'       ''fa fa-download'' icon_class',
'from fin_documento_cagro f',
'inner join fin_proveedor p on (p.prov_codigo = f.doc_prov)',
'inner join per_empleado e on ( e.empl_legajo = f.doc_empleado and e.empl_empresa = p.prov_empr )',
'inner join gen_tipo_mov tm on ( tm.tmov_codigo = f.doc_tipo_mov and tm.tmov_empr = e.empl_empresa )',
'left  join fin_import_doc_ext_det ed on (ed.doc_clave_externa = f.doc_clave )',
'where p.prov_empr = to_number(:p_empresa)',
'and   ed.id is null',
'and   ((ed.id is not null and ed.empresa_id = to_number(:p_empresa) or ed.id is null))',
'and   case when f.doc_empr = 22 then 1 else f.doc_empr end = to_number(:p_empresa);'))
,p_ajax_enabled=>'Y'
,p_query_row_template=>wwv_flow_api.id(7642720688079957602)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(68951282662090108)
,p_query_column_id=>1
,p_column_alias=>'LIST_TITLE'
,p_column_display_sequence=>1
,p_column_heading=>'List Title'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(68951386051090109)
,p_query_column_id=>2
,p_column_alias=>'LIST_BADGE'
,p_column_display_sequence=>2
,p_column_heading=>'List Badge'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(68951495185090110)
,p_query_column_id=>3
,p_column_alias=>'LIST_TEXT'
,p_column_display_sequence=>3
,p_column_heading=>'List Text'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(68951517910090111)
,p_query_column_id=>4
,p_column_alias=>'ICON_CLASS'
,p_column_display_sequence=>4
,p_column_heading=>'Icon Class'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(68952379371090119)
,p_plug_name=>'Historial'
,p_region_template_options=>'#DEFAULT#:js-dialog-size600x400'
,p_plug_template=>wwv_flow_api.id(7570302609627401239)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'REGION_POSITION_04'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(68952410182090120)
,p_name=>'{listHist}'
,p_parent_plug_id=>wwv_flow_api.id(68952379371090119)
,p_template=>wwv_flow_api.id(7570284778490401231)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#:t-Report--stretch:t-Report--altRowsDefault:t-Report--rowHighlight'
,p_display_point=>'BODY'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select i.id,',
'       i.created fecha,',
'       i.created_by usuario,',
'       '''' detalle,',
'       '''' revertir',
'from fin_import_doc_ext_cab i',
'where i.empresa_id=to_number(:p_empresa)',
'order by i.created desc'))
,p_ajax_enabled=>'Y'
,p_query_row_template=>wwv_flow_api.id(7570326356984401251)
,p_query_num_rows=>5
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_no_data_found=>'Sin registros'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(68952757721090123)
,p_query_column_id=>1
,p_column_alias=>'ID'
,p_column_display_sequence=>1
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(68952870331090124)
,p_query_column_id=>2
,p_column_alias=>'FECHA'
,p_column_display_sequence=>2
,p_column_heading=>'Fecha'
,p_use_as_row_header=>'N'
,p_column_format=>'dd/mm/yyyy hh24:mi'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(68952946812090125)
,p_query_column_id=>3
,p_column_alias=>'USUARIO'
,p_column_display_sequence=>3
,p_column_heading=>'Usuario'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(68953095102090126)
,p_query_column_id=>4
,p_column_alias=>'DETALLE'
,p_column_display_sequence=>4
,p_column_heading=>'Detalle'
,p_use_as_row_header=>'N'
,p_column_link=>'#'
,p_column_linktext=>'<span class="fa fa-search u-info-text" aria-hidden="true"></span>'
,p_column_link_attr=>'onclick="$s(''P158_IMPORTACION_ID'',#ID#)"'
,p_column_alignment=>'CENTER'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(68953179824090127)
,p_query_column_id=>5
,p_column_alias=>'REVERTIR'
,p_column_display_sequence=>5
,p_column_heading=>'Revertir'
,p_use_as_row_header=>'N'
,p_column_link=>'#'
,p_column_linktext=>'<span class="fa fa-times-circle u-danger-text" aria-hidden="true"></span>'
,p_column_link_attr=>'onclick="$s(''P158_ID_IMPORT_REVERSE'',#ID#)"'
,p_column_alignment=>'CENTER'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(68953728102090133)
,p_plug_name=>unistr('Detalle de la importaci\00F3n')
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570284778490401231)
,p_plug_display_sequence=>50
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_source_type=>'PLUGIN_DEV.HARTENFELLER.SLIDEOVER'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>unistr('Detalle de la Importaci\00F3n')
,p_attribute_02=>'50%'
,p_attribute_03=>'left'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(68954124849090137)
,p_plug_name=>'{datosImportados}'
,p_parent_plug_id=>wwv_flow_api.id(68953728102090133)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(7570304732549401240)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select f.doc_clave,',
'       p.prov_codigo,',
'       p.prov_razon_social,',
'       e.empl_legajo,',
'       e.empl_nombre ||'' ''||e.empl_ape funcionario,',
'       f.doc_bruto_exen_loc monto',
'from fin_import_doc_ext_det d',
'inner join fin_documento f on (f.doc_clave = d.doc_clave_local and f.doc_empr = d.empresa_id)',
'inner join fin_proveedor p on (p.prov_codigo = f.doc_prov and p.prov_empr = f.doc_empr)',
'inner join per_empleado e on (e.empl_codigo_prov = p.prov_codigo and e.empl_empresa = p.prov_empr)',
'where d.import_id = to_number(:P158_IMPORTACION_ID)',
'and   d.empresa_id = to_number(:P_EMPRESA)'))
,p_plug_source_type=>'NATIVE_IR'
,p_ajax_items_to_submit=>'P158_IMPORTACION_ID,P_EMPRESA'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'MILLIMETERS'
,p_prn_paper_size=>'A4'
,p_prn_width=>297
,p_prn_height=>210
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header=>'{datosImportados}'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#EEEEEE'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'bold'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#FFFFFF'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_prn_border_color=>'#666666'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(68954254380090138)
,p_max_row_count=>'1000000'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:EMAIL:XLS:PDF:RTF'
,p_owner=>'ADMIN'
,p_internal_uid=>68954254380090138
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(68954375657090139)
,p_db_column_name=>'DOC_CLAVE'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Doc Clave'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(68954498671090140)
,p_db_column_name=>'PROV_CODIGO'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Prov Codigo'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(68954583566090141)
,p_db_column_name=>'PROV_RAZON_SOCIAL'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Prov Razon Social'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(68954651038090142)
,p_db_column_name=>'EMPL_LEGAJO'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Empl Legajo'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(68954765604090143)
,p_db_column_name=>'FUNCIONARIO'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Funcionario'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(68954893123090144)
,p_db_column_name=>'MONTO'
,p_display_order=>60
,p_column_identifier=>'F'
,p_column_label=>'Monto'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_format_mask=>'999G999G999G999G999G999G990'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(69037378360516968)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'690374'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'DOC_CLAVE:PROV_CODIGO:PROV_RAZON_SOCIAL:EMPL_LEGAJO:FUNCIONARIO:MONTO'
,p_sum_columns_on_break=>'MONTO'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(68950588603090101)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(68950990939090105)
,p_button_name=>'MIGRAR_DATOS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#:t-Button--success:t-Button--stretch'
,p_button_template_id=>wwv_flow_api.id(7570351310891401263)
,p_button_image_alt=>unistr('CLICK AQU\00CD PARA IMPORTAR DATOS A &P158_EMPRESA_DESC.')
,p_button_position=>'BELOW_BOX'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(67691483621048923)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_api.id(67690971696048918)
,p_button_name=>'AGREGAR_CONFIGURACION'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(7570351310891401263)
,p_button_is_hot=>'Y'
,p_button_image_alt=>unistr('Agregar Configuraci\00F3n')
,p_button_position=>'BODY'
,p_warn_on_unsaved_changes=>null
,p_button_css_classes=>'u-pullRight'
,p_grid_new_row=>'Y'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(68952270238090118)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(67689374485048902)
,p_button_name=>'HISTORY'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--warning'
,p_button_template_id=>wwv_flow_api.id(7570351310891401263)
,p_button_image_alt=>unistr('Historial de Exportaci\00F3n')
,p_button_position=>'REGION_TEMPLATE_EDIT'
,p_warn_on_unsaved_changes=>null
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(67691055736048919)
,p_name=>'P158_LN_COD_LINEA_NEG'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(67690971696048918)
,p_prompt=>unistr('L\00EDnea Negocio')
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select l.lin_codigo||''-''||l.lin_desc d,',
'       l.lin_codigo r',
'from fac_linea_negocio_cagro l      ',
'where l.lin_empr = :P158_LN_EMPRESA  '))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'%'
,p_lov_cascade_parent_items=>'P158_LN_EMPRESA'
,p_ajax_items_to_submit=>'P158_LN_EMPRESA'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(7570351031227401263)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'N'
,p_attribute_05=>'Y'
,p_attribute_06=>'0'
,p_attribute_08=>'650'
,p_attribute_09=>'650'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(67691171421048920)
,p_name=>'P158_LN_EMPRESA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(67690971696048918)
,p_prompt=>'Empresa'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select 1||''-''|| '' HILAGRO S.A'' d, 1 r from dual',
'union all',
'select e.empr_codigo||''-''||e.empr_razon_social d,',
'       e.empr_codigo r',
'from gen_empresa e',
'where e.empr_codigo not in (1, 22) --> HILAGRO Y CAGRO TIENEN MISMO ID'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'%'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(7570351031227401263)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'N'
,p_attribute_05=>'Y'
,p_attribute_06=>'0'
,p_attribute_08=>'650'
,p_attribute_09=>'650'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(67691215975048921)
,p_name=>'P158_LN_EMPRESA_GRUPO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(67690971696048918)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Empresa Grupo'
,p_source=>'P_EMPRESA'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_named_lov=>'LOV_EMPRESA'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
' SELECT DISTINCT EMPR_CODIGO || '' - '' ||EMPR_RAZON_SOCIAL as display_value, EMPR_CODIGO as return_value ',
'  FROM GEN_EMPRESA',
' WHERE EMPR_CODIGO IN',
'       (SELECT OPEM_EMPR',
'          FROM GEN_OPERADOR_EMPRESA',
'         WHERE OPEM_OPER =',
'               (SELECT OPER_CODIGO',
'                  FROM GEN_OPERADOR',
'                 WHERE RTRIM(UPPER(OPER_LOGIN)) = RTRIM(UPPER(:APP_USER))))',
' ORDER BY 1'))
,p_field_template=>wwv_flow_api.id(7570350881050401263)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
,p_attribute_02=>'LOV'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(67691316472048922)
,p_name=>'P158_LN_CONCEPTO_LOCAL'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(67690971696048918)
,p_prompt=>'Concepto'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select f.fcon_codigo||''-''||f.fcon_desc d,',
'       f.fcon_clave',
'from fin_concepto f      ',
'where f.fcon_empr=:p_empresa  '))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'%'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(7570351031227401263)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'N'
,p_attribute_05=>'N'
,p_attribute_08=>'650'
,p_attribute_09=>'650'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(67691886375048927)
,p_name=>'P158_ID_DEL_DET_LINEA_NEG'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(67689289565048901)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(68927729652662047)
,p_name=>'P158_FIN_DOC_CAGRO_DET'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(67692320090048932)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(68950698781090102)
,p_name=>'P158_EMPRESA_DESC'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(68950805750090104)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select e.empr_razon_social from gen_empresa e',
'where e.empr_codigo=:p_empresa'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
end;
/
begin
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(68953276865090128)
,p_name=>'P158_ID_IMPORT_REVERSE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(68952410182090120)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(68953808463090134)
,p_name=>'P158_IMPORTACION_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(68953728102090133)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(67691528303048924)
,p_name=>'Agregar configuracion'
,p_event_sequence=>10
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(67691483621048923)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(67691604761048925)
,p_event_id=>wwv_flow_api.id(67691528303048924)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'     l_empresa_grupo    number;',
'     l_empresa_remota   number;',
'     l_linea_neg_remota number;',
'     l_concepto_local   number;',
'begin',
'  l_empresa_grupo    := case when :p_empresa = 1 then 22 else to_number(:p_empresa) end; --> En Hilagro la empresa Consultagro es la 22',
'  l_empresa_remota   := :P158_LN_EMPRESA;',
'  l_linea_neg_remota := :P158_LN_COD_LINEA_NEG;',
'  l_concepto_local   := :P158_LN_CONCEPTO_LOCAL;',
'',
'  if l_empresa_remota is not null',
'  and l_linea_neg_remota is not null ',
'  and l_concepto_local is not null',
'  then',
'      delete from fac_linea_negocio_det_cagro',
'      where empresa_id       = l_empresa_remota',
'      and   linea_neg_id     = l_linea_neg_remota',
'      and   empresa_grupo_id = l_empresa_grupo;',
'',
'      insert into fac_linea_negocio_det_cagro(empresa_id, concepto_id, empresa_grupo_id, linea_neg_id)',
'      values(l_empresa_remota, l_concepto_local, l_empresa_grupo, l_linea_neg_remota);',
'  end if;',
'  ',
'end;'))
,p_attribute_02=>'P158_LN_EMPRESA,P158_LN_COD_LINEA_NEG,P158_LN_CONCEPTO_LOCAL'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(67691762159048926)
,p_event_id=>wwv_flow_api.id(67691528303048924)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(67689289565048901)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(67691918248048928)
,p_name=>'delete det linea negocio remote'
,p_event_sequence=>20
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P158_ID_DEL_DET_LINEA_NEG'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(67692046489048929)
,p_event_id=>wwv_flow_api.id(67691918248048928)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'delete from FAC_LINEA_NEGOCIO_det_cagro d',
'where d.id = :P158_ID_DEL_DET_LINEA_NEG;'))
,p_attribute_02=>'P158_ID_DEL_DET_LINEA_NEG'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(67692157368048930)
,p_event_id=>wwv_flow_api.id(67691918248048928)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(67689289565048901)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(68927845378662048)
,p_name=>'onChage open region'
,p_event_sequence=>30
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P158_FIN_DOC_CAGRO_DET'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(68927970531662049)
,p_event_id=>wwv_flow_api.id(68927845378662048)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_OPEN_REGION'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(67692320090048932)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(68928084434662050)
,p_event_id=>wwv_flow_api.id(68927845378662048)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(67692320090048932)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(68952543816090121)
,p_name=>'open region Hist'
,p_event_sequence=>40
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(68952270238090118)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(68952641898090122)
,p_event_id=>wwv_flow_api.id(68952543816090121)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_OPEN_REGION'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(68952379371090119)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(68953353692090129)
,p_name=>'Confirm reverse actioon'
,p_event_sequence=>50
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P158_ID_IMPORT_REVERSE'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(68953407465090130)
,p_event_id=>wwv_flow_api.id(68953353692090129)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_CONFIRM'
,p_attribute_01=>unistr('\00BFEst\00E1 seguro que quiere revertir la importaci\00F3n, \00E9sto eliminar\00E1 los registros de documentos ya generados? (NO HAY VUELTA ATR\00C1S)')
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(68953577704090131)
,p_event_id=>wwv_flow_api.id(68953353692090129)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'REVERSE_ACTION'
,p_attribute_02=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(68953936621090135)
,p_name=>'Open and refresh this region'
,p_event_sequence=>60
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P158_IMPORTACION_ID'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(68954084097090136)
,p_event_id=>wwv_flow_api.id(68953936621090135)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_OPEN_REGION'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(68953728102090133)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(68954903283090145)
,p_event_id=>wwv_flow_api.id(68953936621090135)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(68954124849090137)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(68952191705090117)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Importar Datos'
,p_process_sql_clob=>'fini238.importar_datos(in_empresa => to_number(:P_EMPRESA));'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'MIGRAR_DATOS'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'Datos Importados correctamente'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(68953632449090132)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'ReverseAction'
,p_process_sql_clob=>'fini238.revertir_importacion(in_importacion => to_number(:P158_ID_IMPORT_REVERSE));'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'REVERSE_ACTION'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>unistr('Ha revertido la importaci\00F3n')
);
end;
/
prompt --application/end_environment
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
