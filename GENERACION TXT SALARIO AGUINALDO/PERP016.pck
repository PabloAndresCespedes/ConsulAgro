CREATE OR REPLACE PACKAGE PERP016 AS

  PROCEDURE PP_CONSULTA (P_EMPRESA    IN NUMBER,
                                      P_FEC_DESDE  IN DATE,
                                      P_FEC_HASTA  IN DATE,
                                      P_FORMA_PAGO IN NUMBER,
                                      P_SUCURSAL   IN VARCHAR2,
                                      P_CTA_BCO    IN NUMBER,
                                      P_CONCEPTOS  IN VARCHAR2,
                                      P_BCO_PAGO   IN NUMBER);
                                      
  PROCEDURE PP_CREAR_ARCHIVO_SALARIOS(P_EMPRESA    IN NUMBER,
                                      P_FEC_DESDE  IN DATE,
                                      P_FEC_HASTA  IN DATE,
                                      P_FORMA_PAGO IN NUMBER,
                                      P_SUCURSAL   IN VARCHAR2,
                                      P_CTA_BCO    IN NUMBER,
                                      P_CONCEPTOS  IN VARCHAR2,
                                      P_BCO_PAGO   IN NUMBER);

  FUNCTION CLOB_TO_BLOB(P_CLOB IN CLOB) RETURN BLOB;
  
  
  PROCEDURE PP_EXCLUIR (P_VALOR IN VARCHAR2,
                        P_SEQ   IN NUMBER);

END;
/
CREATE OR REPLACE PACKAGE BODY PERP016 AS



  PROCEDURE PP_CONSULTA (P_EMPRESA    IN NUMBER,
                                      P_FEC_DESDE  IN DATE,
                                      P_FEC_HASTA  IN DATE,
                                      P_FORMA_PAGO IN NUMBER,
                                      P_SUCURSAL   IN VARCHAR2,
                                      P_CTA_BCO    IN NUMBER,
                                      P_CONCEPTOS  IN VARCHAR2,
                                      P_BCO_PAGO   IN NUMBER) IS
    
   CURSOR SALARIOS IS
      SELECT LAST_DAY(FECHA) FECHA_PAGO,
             CASE
               WHEN CONCEPTO <> 'AGUINALDO' THEN
                '000'
               ELSE
                '001'
             END CONCEPTO_PAGO,
             CASE
               WHEN CONCEPTO <> 'AGUINALDO' THEN
                'SALARIO'
               ELSE
                'AGUINALDO'
             END TIPO_PAGO,
             SUBSTR(NOMBRE, 0, 85) NOMBRE,
             A.EMPL_NRO_CTA NRO_CUENTA,
             EMPL_LEGAJO LEGAJO,
             SUM(IMPORTE) IMPORTE_PAGO,
             SUCURSAL,
             CAJA
        FROM PERP016_V A
       WHERE (A.SUCURSAL IN
             (SELECT REGEXP_SUBSTR(P_SUCURSAL, '[^:]+', 1, LEVEL) SUCURSAL
                 FROM DUAL
               CONNECT BY REGEXP_SUBSTR(P_SUCURSAL, '[^:]+', 1, LEVEL) IS NOT NULL) OR
             P_SUCURSAL IS NULL)
            
         AND FECHA BETWEEN P_FEC_DESDE AND P_FEC_HASTA
         AND A.EMPL_NRO_CTA IS NOT NULL
         AND (EMPL_FORMA_PAGO = P_FORMA_PAGO OR P_FORMA_PAGO IS NULL)
         AND (A.COD_CAJA = P_CTA_BCO OR P_CTA_BCO IS NULL)
         AND (A.COD_CONCEPTO IN
             (SELECT REGEXP_SUBSTR(P_CONCEPTOS, '[^:]+', 1, LEVEL) CONCEPTO
                 FROM DUAL
               CONNECT BY REGEXP_SUBSTR(P_CONCEPTOS, '[^:]+', 1, LEVEL) IS NOT NULL) OR
             P_CONCEPTOS IS NULL)
            AND EMPRESA = P_EMPRESA
            -- and empl_legajo <> 501
         AND EMPL_BCO_PAGO = P_BCO_PAGO
       GROUP BY LAST_DAY(FECHA),
                CASE
                  WHEN CONCEPTO <> 'AGUINALDO' THEN
                   '000'
                  ELSE
                   '001'
                END,
                CASE
                  WHEN CONCEPTO <> 'AGUINALDO' THEN
                   'SALARIO'
                  ELSE
                   'AGUINALDO'
                END,
                NOMBRE,
                EMPL_LEGAJO,
                A.EMPL_NRO_CTA,
                SUCURSAL,
                CAJA
       ORDER BY 4;
BEGIN
     APEX_COLLECTION.CREATE_OR_TRUNCATE_COLLECTION( P_COLLECTION_NAME => 'PERP016');
     FOR X IN SALARIOS LOOP
       
      ---  RAISE_APPLICATION_ERROR (-20001, 'ADFASDF');
             APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'PERP016',  
                                              P_C001      => X.FECHA_PAGO,
                                              P_C002      => X.CONCEPTO_PAGO,
                                              P_C003      => X.TIPO_PAGO,
                                              P_C004      => X.NOMBRE,
                                              P_C005      => X.NRO_CUENTA,
                                              P_C006      => X.LEGAJO,
                                              P_C007      => X.IMPORTE_PAGO,
                                              P_C008      => X.SUCURSAL,
                                              P_C009      => X.CAJA,
                                              P_C010      => 'N');
     
     
     
     END LOOP;   
  
  END PP_CONSULTA;
  
  
  PROCEDURE PP_CREAR_ARCHIVO_SALARIOS(P_EMPRESA    IN NUMBER,
                                      P_FEC_DESDE  IN DATE,
                                      P_FEC_HASTA  IN DATE,
                                      P_FORMA_PAGO IN NUMBER,
                                      P_SUCURSAL   IN VARCHAR2,
                                      P_CTA_BCO    IN NUMBER,
                                      P_CONCEPTOS  IN VARCHAR2,
                                      P_BCO_PAGO   IN NUMBER) IS
    V_CLAVE_EXPORT  NUMBER;
    V_TEXTO         CLOB;
    CANTIDAD        NUMBER;
    co_bco_regional constant number := 3;
    l_total_importe number;
    l_nro_cta_deb   varchar2(25 char);
    l_empresa_deb   gen_empresa.empr_razon_social%type;
    l_concepto      varchar2(5 char);
    CURSOR SALARIOS IS
     /* SELECT LAST_DAY(FECHA) FECHA_PAGO,
             CASE
               WHEN CONCEPTO <> 'AGUINALDO' THEN
                '000'
               ELSE
                '001'
             END CONCEPTO_PAGO,
             CASE
               WHEN CONCEPTO <> 'AGUINALDO' THEN
                'SALARIO'
               ELSE
                'AGUINALDO'
             END TIPO_PAGO,
             SUBSTR(NOMBRE, 0, 85) NOMBRE,
             A.EMPL_NRO_CTA NRO_CUENTA,
             EMPL_LEGAJO LEGAJO,
             SUM(IMPORTE) IMPORTE_PAGO
        FROM PERP016_V A
       WHERE (A.SUCURSAL IN
             (SELECT REGEXP_SUBSTR(P_SUCURSAL, '[^:]+', 1, LEVEL) SUCURSAL
                 FROM DUAL
               CONNECT BY REGEXP_SUBSTR(P_SUCURSAL, '[^:]+', 1, LEVEL) IS NOT NULL) OR
             P_SUCURSAL IS NULL)
            
         AND FECHA BETWEEN P_FEC_DESDE AND P_FEC_HASTA
         AND A.EMPL_NRO_CTA IS NOT NULL
         AND (EMPL_FORMA_PAGO = P_FORMA_PAGO OR P_FORMA_PAGO IS NULL)
         AND (A.COD_CAJA = P_CTA_BCO OR P_CTA_BCO IS NULL)
         AND (A.COD_CONCEPTO IN
             (SELECT REGEXP_SUBSTR(P_CONCEPTOS, '[^:]+', 1, LEVEL) CONCEPTO
                 FROM DUAL
               CONNECT BY REGEXP_SUBSTR(P_CONCEPTOS, '[^:]+', 1, LEVEL) IS NOT NULL) OR
             P_CONCEPTOS IS NULL)
            AND EMPRESA = P_EMPRESA
            -- and empl_legajo <> 501
         AND EMPL_BCO_PAGO = P_BCO_PAGO
       GROUP BY LAST_DAY(FECHA),
                CASE
                  WHEN CONCEPTO <> 'AGUINALDO' THEN
                   '000'
                  ELSE
                   '001'
                END,
                CASE
                  WHEN CONCEPTO <> 'AGUINALDO' THEN
                   'SALARIO'
                  ELSE
                   'AGUINALDO'
                END,
                NOMBRE,
                EMPL_LEGAJO,
                A.EMPL_NRO_CTA
       ORDER BY 4;*/
 SELECT C001 FECHA_PAGO,
        C002 CONCEPTO_PAGO,
        C003 TIPO_PAGO,
        C004 NOMBRE,
        C005 NRO_CUENTA,
        C006 LEGAJO,
        TO_NUMBER(C007) IMPORTE_PAGO,
        CASE
          WHEN C010 = 'N' THEN
           TO_NUMBER(C007)
          ELSE
           0
        END IMPORTE_EXPORTAR,
        C008 SUCURSAL,
        C009 CAJA
   FROM APEX_COLLECTIONS
  WHERE COLLECTION_NAME = 'PERP016'
    AND C010 = 'N'
  ORDER BY 4;

       
  
  BEGIN
  
    DELETE PER_EXPOR_SALARIO_AUX A
     WHERE A.PES_EMPR = P_EMPRESA
       AND A.PES_FECHA_DESDE BETWEEN P_FEC_DESDE AND P_FEC_HASTA;
  
    ----**** banco regional
  
    --nro cuenta longitud 15 llenar con ceros a la izquierda
    --importe pago 13 enteros y dos decimales llenar con 0 a la izquierda
    --tipo de movimiento D= debito C= credito
    --Fecha de Envio fecha de envio formato AAAAMMDD
    --FECHA DE PROCESO formato AAAAMMDD
    --MONEDA 000=GUARANIES 001=DOLARES
    --CODIGO BANCO DEFAULT 0028
    --PLAZA DEFAULT L
    --TIPO CHEQUE GERENCIA DEFAULT NULL
    --NOMBRE DE BENEFICIARIO 85 CARACTERES LLENAR CON ESPACIOS A LA DERECHA
    --PRODUCTO DEFAULT 010
    --SUB PRODUCTO SUELDO=000 ,AGUINALDO=001
    --SUCURSAL DE ENTREGA DEFAULT 001
    --CUENTA NOMINA LEGAJO DEL EMPLEADO 15 NUMEROS LLENAR CON 0 A LA IZQUIERDA
  
    ----**** banco regional Cta DEBITO
    -- 01/09/2022 11:29:03 @PabloACespedes \(^-^)/
    -- se_agrega esto para que tome el primer registro la cuenta a debitar
    if P_BCO_PAGO = co_bco_regional then
      <<total_salario>>
      begin
         select sum(TO_NUMBER(C007)) total
         into l_total_importe
         FROM APEX_COLLECTIONS
         WHERE COLLECTION_NAME = 'PERP016'
         AND C010 = 'N';   
      exception
        when no_data_found then
          Raise_application_error(-20000, 'No se pudo totalizar los salarios');
      end total_salario;
      
      l_empresa_deb := trim(ap.v(p_item => 'P3183_EMPRESA'));
      l_nro_cta_deb := ap.v(p_item => 'P3183_NRO_CUENTA_DEBITO');
      l_concepto    := case when ap.v(p_item => 'P3183_CONCEPTO_PAGO') = 'SUELDO' then '000' else '001' end;
      
      if l_nro_cta_deb is null then
        Raise_application_error(-20000, 'Complete el nro de cuenta a debitar');
      end if;
      
      if l_empresa_deb is null then
        Raise_application_error(-20000, 'Complete el nombre de la empresa');
      end if;
            
      V_TEXTO := V_TEXTO || LPAD(l_nro_cta_deb, 15, '0') ||
                 RPAD(LPAD(nvl(l_total_importe, 0), 13, '0'), 15, '0') || 'D' ||
                 TO_CHAR(sysdate, 'YYYYMMDD') ||
                 TO_CHAR(sysdate, 'YYYYMMDD') || '0000' || '0028' || 'L' || ' ' ||
                 RPAD(l_empresa_deb, 85, ' ') || '010' ||l_concepto ||
                 '001' || LPAD( P_EMPRESA , 15, '0') || CHR(13) || CHR(10);
    end if;
    ---- ****** fin bco regional Cta DEBITO
  
    FOR R IN SALARIOS LOOP
      ---** BANCO REGIONAL
      IF R.IMPORTE_PAGO > 0 AND P_BCO_PAGO = co_bco_regional THEN
        V_TEXTO := V_TEXTO || LPAD(R.NRO_CUENTA, 15, '0') ||
                   RPAD(LPAD(R.IMPORTE_PAGO, 13, '0'), 15, '0') || 'C' ||
                   TO_CHAR(sysdate, 'YYYYMMDD') ||
                   TO_CHAR(sysdate, 'YYYYMMDD') || '0000' || '0028' || 'L' || ' ' ||
                   RPAD(R.NOMBRE, 85, ' ') || '010' || R.CONCEPTO_PAGO ||
                   '001' || LPAD(R.LEGAJO, 15, '0') || CHR(13) || CHR(10);
      END IF;
      
       IF R.IMPORTE_PAGO > 0 AND P_BCO_PAGO = 7 AND P_EMPRESA = 2 THEN
        V_TEXTO := V_TEXTO || LPAD(R.NRO_CUENTA, 15, '0') ||
                   RPAD(LPAD(R.IMPORTE_PAGO, 15, '0'), 15, '0') || 'C' ||
                   TO_CHAR(SYSDATE, 'YYYYMMDD') ||
                   TO_CHAR(SYSDATE, 'YYYYMMDD') || '0000' || '0028' || 'L' || ' ' ||
                   RPAD(R.NOMBRE, 85, ' ') || '010' || R.CONCEPTO_PAGO ||
                   '001' || LPAD(R.LEGAJO, 15, '0') || CHR(13) || CHR(10);
      END IF;
    
      ---*** BANCO ATLAS
      IF R.IMPORTE_PAGO > 0 AND P_BCO_PAGO = 11 THEN
        V_TEXTO := V_TEXTO || RPAD(R.NRO_CUENTA, 15) ||
                   RPAD(R.IMPORTE_PAGO, 12) || 'C' ||
                   RPAD(TO_CHAR(SYSDATE, 'DD/MM/YYYY'), 10) ||CHR(13) || CHR(10);
      END IF;

    END LOOP;
  
    IF V_TEXTO IS NOT NULL THEN
    
      INSERT INTO PER_EXPOR_SALARIO_AUX
        (PES_EMPR,
         PES_FECHA_EXPOR,
         PES_LOGIN,
         PES_FECHA_DESDE,
         PES_FECHA_HASTA,
         PES_FORMA_PAGO,
         PES_ARCHIVO)
      VALUES
        (P_EMPRESA,
         SYSDATE,
         GEN_DEVUELVE_USER,
         P_FEC_DESDE,
         P_FEC_HASTA,
         NULL,
         PERP016.CLOB_TO_BLOB(P_CLOB => V_TEXTO));
    ELSE
      RAISE_APPLICATION_ERROR(-20001,
                              'No hay Datos para Exportar Verifique la Grilla de Salarios!');
    END IF;
  
  END PP_CREAR_ARCHIVO_SALARIOS;

  FUNCTION CLOB_TO_BLOB(P_CLOB IN CLOB) RETURN BLOB IS
    V_BLOB        BLOB;
    V_OFFSET      NUMBER DEFAULT 1;
    V_AMOUNT      NUMBER DEFAULT 4096;
    V_OFFSETWRITE NUMBER DEFAULT 1;
    V_AMOUNTWRITE NUMBER;
    V_BUFFER      VARCHAR2(4096 CHAR);
  BEGIN
    DBMS_LOB.CREATETEMPORARY(V_BLOB, TRUE);
    BEGIN
      LOOP
        DBMS_LOB.READ(LOB_LOC => P_CLOB,
                      AMOUNT  => V_AMOUNT,
                      OFFSET  => V_OFFSET,
                      BUFFER  => V_BUFFER);
      
        V_AMOUNTWRITE := UTL_RAW.LENGTH(R => UTL_RAW.CAST_TO_RAW(C => V_BUFFER));
      
        DBMS_LOB.WRITE(LOB_LOC => V_BLOB,
                       AMOUNT  => V_AMOUNTWRITE,
                       OFFSET  => V_OFFSETWRITE,
                       BUFFER  => UTL_RAW.CAST_TO_RAW(V_BUFFER));
      
        V_OFFSETWRITE := V_OFFSETWRITE + V_AMOUNTWRITE;
      
        V_OFFSET := V_OFFSET + V_AMOUNT;
        V_AMOUNT := 4096;
      END LOOP;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
    RETURN V_BLOB;
  END CLOB_TO_BLOB;
 
PROCEDURE PP_EXCLUIR (P_VALOR IN VARCHAR2,
                      P_SEQ   IN NUMBER) IS 
  
BEGIN
  
 IF P_VALOR  = 'true' THEN
   APEX_COLLECTION.UPDATE_MEMBER_ATTRIBUTE (P_COLLECTION_NAME => 'PERP016',
                                            P_SEQ             => P_SEQ,
                                            P_ATTR_NUMBER     => 10,
                                            P_ATTR_VALUE      => 'S'); 
                  
  ELSE
   APEX_COLLECTION.UPDATE_MEMBER_ATTRIBUTE (P_COLLECTION_NAME => 'PERP016',
                                            P_SEQ             => P_SEQ,
                                            P_ATTR_NUMBER     => 10,
                                            P_ATTR_VALUE      => 'N'); 
  
  END IF;

END PP_EXCLUIR;

END;
/
