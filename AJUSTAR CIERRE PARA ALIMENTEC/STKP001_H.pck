CREATE OR REPLACE PACKAGE STKP001_H AS
  PROCEDURE PP_VERIF_IMP_PROVISORIA(P_EMPRESA IN NUMBER,
                                    P_PERI_ACT_FEC_INI IN DATE, P_PERI_ACT_FEC_FIN IN DATE);
 PROCEDURE PP_CIERRE_PERIODO(P_EMPRESA IN NUMBER,
                             P_PERI_ACT IN NUMBER,
                             P_PERI_ACT_FEC_INI IN DATE,
                             P_PERI_ACT_FEC_FIN IN DATE,
                             P_PERI_SGTE IN NUMBER,
                             P_PERI_SGTE_FEC_INI IN DATE,
                             P_PERI_SGTE_FEC_FIN IN DATE,
                             O_RETURN            OUT VARCHAR2) ;
 PROCEDURE PP_ACT_ART_DEP(P_EMPRESA IN NUMBER,
                         P_PERI_SGTE_FEC_INI IN DATE,
                         P_PERI_SGTE_FEC_FIN IN DATE);

PROCEDURE PP_ACT_ART_EMPR_PERI(P_EMPRESA IN NUMBER,
                               P_PERI_ACT IN NUMBER,
                               P_PERI_SGTE IN NUMBER);
END STKP001_H;
/
CREATE OR REPLACE PACKAGE BODY STKP001_H AS


  PROCEDURE PP_VERIF_IMP_PROVISORIA(P_EMPRESA IN NUMBER,
                                    P_PERI_ACT_FEC_INI IN DATE,
                                    P_PERI_ACT_FEC_FIN IN DATE) IS
  V_DOC NUMBER;
BEGIN
  SELECT DOCU_NRO_DOC
    INTO V_DOC
    FROM STK_DOCUMENTO, STK_CONFIGURACION
   WHERE DOCU_TIPO_MOV = CONF_IMP_REC
     AND DOCU_FEC_EMIS BETWEEN P_PERI_ACT_FEC_INI
                           AND P_PERI_ACT_FEC_FIN
     AND DOCU_EMPR = P_EMPRESA
     AND DOCU_EMPR = CONF_EMPR;

  RAISE_APPLICATION_ERROR(-20001, 'El despacho '||TO_CHAR(V_DOC)||' esta ingresado de forma provisoria, no se puede realizar el cierre!.');

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    NULL;
  WHEN TOO_MANY_ROWS THEN
      ROLLBACK;
      RAISE_APPLICATION_ERROR(-20001,'Hay despachos ingresados en forma provisoria y no se puede realizar el cierre!.');

END PP_VERIF_IMP_PROVISORIA;


PROCEDURE PP_ACT_ART_EMPR_PERI(P_EMPRESA IN NUMBER,
                               P_PERI_ACT IN NUMBER,
                               P_PERI_SGTE IN NUMBER) IS
  /* OJO que PARAMETER.P_PERI_ACT ya se actualizo */
  CURSOR AEP_CUR IS
    SELECT *
      FROM STK_ART_EMPR_PERI
     WHERE AEP_EMPR = P_EMPRESA
       AND AEP_PERIODO = P_PERI_ACT;

  V_EMPR NUMBER := 0;
  V_ART NUMBER := 0;
  V_COSTO_INI_LOC NUMBER;
  V_COSTO_INI_MON NUMBER;
  V_COSTO_INI_LOC_OPER NUMBER;
  V_COSTO_INI_MON_OPER NUMBER;

BEGIN

  /* para cada registro insertar uno para el sgte.periodo */
  FOR RAEP IN AEP_CUR LOOP
  
    /* calculo de costo inicial expresado en moneda local */
    V_COSTO_INI_LOC := 0;
    /* modificado por luis fedriani el 21/11/1997 */
    /* le puse diferente de cero para que tambien actualize el costo*/
    /* si es negativa la existencia(antes tenia mayor que)*/
    IF RAEP.AEP_TOT_EXIST <> 0 THEN
       V_COSTO_INI_LOC := RAEP.AEP_TOT_EXIST
                          * RAEP.AEP_COSTO_PROM_LOC;
    END IF;
    
    IF RAEP.AEP_TOT_EXIST_OPER <> 0 THEN
       V_COSTO_INI_LOC_OPER := RAEP.AEP_TOT_EXIST_OPER
                          * RAEP.AEP_COSTO_PROM_LOC_OPER;
    END IF;
    
    /* calculo de costo inicial expresado en moneda de referencia (U$) */
    V_COSTO_INI_MON := 0;
    /* modificado por luis fedriani el 21/11/1997 */
    /* le puse diferente de cero para que tambien actualize el costo*/
    /* si es negativa la existencia (antes tenia mayor que)*/

    IF RAEP.AEP_TOT_EXIST <> 0 THEN
       V_COSTO_INI_MON := RAEP.AEP_TOT_EXIST
                          * RAEP.AEP_COSTO_PROM_MON;
    END IF;
    
    IF RAEP.AEP_TOT_EXIST_OPER <> 0 THEN
       V_COSTO_INI_MON_OPER := RAEP.AEP_TOT_EXIST_OPER
                          * RAEP.AEP_COSTO_PROM_MON_OPER;
    END IF;

    INSERT INTO STK_ART_EMPR_PERI
       (AEP_PERIODO, AEP_EMPR, AEP_ART,
        AEP_TOT_EXIST, AEP_EXIST_INI,
        AEP_COSTO_INI_LOC, AEP_COSTO_INI_MON,
        AEP_COSTO_PROM_INI_LOC, AEP_COSTO_PROM_INI_MON,
        AEP_COSTO_PROM_LOC, AEP_COSTO_PROM_MON,
        AEP_EC_COMPRA, AEP_EL_COMPRA, AEP_EM_COMPRA, AEP_SC_DEV_COMPRA,
        AEP_SL_DEV_COMPRA, AEP_SM_DEV_COMPRA, AEP_SC_VTA, AEP_SL_VTA,
        AEP_SM_VTA, AEP_EC_DEV_VTA, AEP_EL_DEV_VTA, AEP_EM_DEV_VTA,
        AEP_EC_PROD, AEP_EL_PROD, AEP_EM_PROD, AEP_SC_PROD,
        AEP_EC_KIT, AEP_EL_KIT, AEP_EM_KIT, AEP_SC_KIT, 
        AEP_SL_KIT, AEP_SM_KIT, AEP_SC_PERDIDA, AEP_SC_CONS,
        AEP_C_DIF_INV, AEP_C_TRANSF, AEP_C_REM, AEP_EC_ARM_DES,
        AEP_EL_ARM_DES, AEP_EM_ARM_DES, AEP_SC_ARM_DES,
        AEP_SL_ARM_DES, AEP_SM_ARM_DES, AEP_EL_DEV_PROD,
        AEP_EM_DEV_PROD, AEP_EC_DEV_PROD,
        AEP_TOT_EXIST_OPER, AEP_EXIST_INI_OPER,  AEP_COSTO_INI_LOC_OPER,
        AEP_COSTO_INI_MON_OPER, AEP_COSTO_PROM_INI_LOC_OPER, AEP_COSTO_PROM_INI_MON_OPER,
        AEP_COSTO_PROM_LOC_OPER, AEP_COSTO_PROM_MON_OPER 
        ) VALUES
       (P_PERI_SGTE, RAEP.AEP_EMPR, RAEP.AEP_ART,
        RAEP.AEP_TOT_EXIST, RAEP.AEP_TOT_EXIST,
        V_COSTO_INI_LOC, V_COSTO_INI_MON,
        RAEP.AEP_COSTO_PROM_LOC, RAEP.AEP_COSTO_PROM_MON,
        RAEP.AEP_COSTO_PROM_LOC, RAEP.AEP_COSTO_PROM_MON,
        0,0,0,0,
        0,0,0,0,
        0,0,0,0,
        0,0,0,0,
        0,0,0,0,
        0,0,0,0,
        0,0,0,0,
        0,0,0,
        0,0,0,
        0,0,RAEP.AEP_TOT_EXIST_OPER,
        RAEP.AEP_TOT_EXIST_OPER,
        V_COSTO_INI_LOC_OPER,
        V_COSTO_INI_MON_OPER,
        RAEP.AEP_COSTO_PROM_LOC_OPER,
        RAEP.AEP_COSTO_PROM_MON_OPER,
        RAEP.AEP_COSTO_PROM_LOC_OPER,
        RAEP.AEP_COSTO_PROM_MON_OPER);
   --- COMMIT;
  END LOOP;
  
  exception
        when others then 
              ROLLBACK;
              RAISE_APPLICATION_ERROR(-20001,'Error en PP_ACT_ART_EMPR_PERI'||SQLCODE||'-'||SQLERRM);
END PP_ACT_ART_EMPR_PERI;

PROCEDURE PP_ACT_ART_DEP(P_EMPRESA IN NUMBER,
                         P_PERI_SGTE_FEC_INI IN DATE,
                         P_PERI_SGTE_FEC_FIN IN DATE) IS
  /* cursor para actualizar ARDE_CANT_INI, ARDE_CANT_ENT y ARDE_CANT_SAL */
  CURSOR ENSA_CUR IS
    SELECT DOCU_EMPR, DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART,
           SUM(DECODE(OPER_ENT_SAL,'E',DETA_CANT,0)) TOT_ENT,
           SUM(DECODE(OPER_ENT_SAL,'S',DETA_CANT,0)) TOT_SAL
      FROM STK_OPERACION, STK_DOCUMENTO, STK_DOCUMENTO_DET
        WHERE         OPER_CODIGO     = DOCU_CODIGO_OPER
        AND         OPER_EMPR     = DOCU_EMPR
        
                 AND DOCU_CLAVE     = DETA_CLAVE_DOC
                 AND DOCU_EMPR    = DETA_EMPR


         AND DOCU_EMPR = P_EMPRESA
         AND DOCU_FEC_EMIS BETWEEN P_PERI_SGTE_FEC_INI
                              AND P_PERI_SGTE_FEC_FIN
      GROUP BY DOCU_EMPR, DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART
      ORDER BY DOCU_EMPR, DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART;

  V_EMPR NUMBER := 0;
  V_SUC NUMBER := 0;
  V_DEP NUMBER := 0;
  V_ART NUMBER := 0;

BEGIN

  /* cerar ARDE_CANT_ENT y ARDE_CANT_SAL en STK_ARTICULO_DEPOSITO */
  UPDATE STK_ARTICULO_DEPOSITO SET
         ARDE_CANT_INI = ARDE_CANT_ACT,
         ARDE_CANT_ENT = 0,
         ARDE_CANT_SAL = 0
   WHERE ARDE_EMPR = P_EMPRESA;
 -- COMMIT;

  FOR RENSA IN ENSA_CUR LOOP



    UPDATE STK_ARTICULO_DEPOSITO SET
         ARDE_CANT_INI = ARDE_CANT_ACT - RENSA.TOT_ENT + RENSA.TOT_SAL,
         ARDE_CANT_ENT = RENSA.TOT_ENT,
         ARDE_CANT_SAL = RENSA.TOT_SAL
      WHERE ARDE_EMPR = RENSA.DOCU_EMPR
        AND ARDE_SUC = RENSA.DOCU_SUC_ORIG
        AND ARDE_DEP = RENSA.DOCU_DEP_ORIG
        AND ARDE_ART = RENSA.DETA_ART;
  
  --  COMMIT;

  END LOOP;
 
EXCEPTION
  WHEN others THEN
    ROLLBACK;
                 
END PP_ACT_ART_DEP;


 PROCEDURE PP_CIERRE_PERIODO(P_EMPRESA IN NUMBER,
                             P_PERI_ACT IN NUMBER,
                             P_PERI_ACT_FEC_INI IN DATE,
                             P_PERI_ACT_FEC_FIN IN DATE,
                             P_PERI_SGTE IN NUMBER,
                             P_PERI_SGTE_FEC_INI IN DATE,
                             P_PERI_SGTE_FEC_FIN IN DATE,
                             O_RETURN            OUT VARCHAR2) IS
  V_VARIABLE VARCHAR2(1);
  V_PERI_ACT_FEC_INI DATE;

  V_PERI_ACT_FEC_FIN  DATE;
  V_PERI_SGTE_FEC_INI DATE;
  V_PERI_SGTE_FEC_FIN DATE;
  V_PERI_ACT NUMBER;
  V_PERI_SGTE NUMBER;
BEGIN
BEGIN
  INSERT INTO GEN_CONTROL_CIERRE
      (COCI_EMPR,
       COCI_SUC,
       COCI_SISTEMA,
       COCI_PERIODO,
       COCI_IND_PRE_CERRADO,
       COCI_LUGAR_ORIGEN,
       COCI_FECHA,
       COCI_LOGIN)
    VALUES
      (P_EMPRESA, 1, 1, P_PERI_ACT, 'S', GEN_LUGAR_ORIGEN, SYSDATE, gen_devuelve_user);
EXCEPTION
  WHEN OTHERS THEN
    NULL;
END; 

  PP_VERIF_IMP_PROVISORIA(P_EMPRESA ,
                          P_PERI_ACT_FEC_INI,
                          P_PERI_ACT_FEC_FIN  );
  STK_ACT_ART_DEP_HIST(P_EMPRESA,
                       P_PERI_ACT,
                       P_PERI_ACT_FEC_INI, 
                       P_PERI_ACT_FEC_FIN);
                       
  PP_ACT_ART_DEP(P_EMPRESA           ,
                 P_PERI_SGTE_FEC_INI,
                 P_PERI_SGTE_FEC_FIN );
                 
                 

--------------------- -------------
--PP_STK_ACTUALIZAR_PERIODO
-----------------------
  V_PERI_ACT_FEC_INI := P_PERI_SGTE_FEC_INI;
  V_PERI_ACT_FEC_FIN := P_PERI_SGTE_FEC_FIN;
  V_PERI_SGTE_FEC_INI := ADD_MONTHS(P_PERI_SGTE_FEC_INI, 1);
  V_PERI_SGTE_FEC_FIN := LAST_DAY(ADD_MONTHS(P_PERI_SGTE_FEC_FIN, 1));
  V_PERI_ACT := P_PERI_SGTE;
  V_PERI_SGTE := P_PERI_SGTE + 1;
  
  
  --------------------------------------------------------------
  ---DE AQUI PARA ABAJO USAR TODO LO QUE SEA VARIABLES YA NO
  ---LOS PARAMETROS RECIBIDOS EXCEPTO EL DE EMPRESA
  ----------------------------------------------------------
BEGIN
  SELECT 'X'
    INTO V_VARIABLE
    FROM STK_PERIODO
   WHERE PERI_CODIGO = V_PERI_SGTE
     AND PERI_EMPR = P_EMPRESA;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    INSERT INTO STK_PERIODO
              (PERI_CODIGO,
               PERI_FEC_INI,
               PERI_FEC_FIN,
               PERI_EMPR)
       VALUES (V_PERI_SGTE,
               V_PERI_SGTE_FEC_INI,
               V_PERI_SGTE_FEC_FIN,
               P_EMPRESA);
  when others then 
          ROLLBACK;
          raise_application_error(-20001,'Error en PP_ACT_STK_PERIODO'||SQLCODE||'-'||SQLERRM);
END;
                     
----------------------------------
---PP_ACTUALIZAR_CONF
-------------------------------------                      
BEGIN
  UPDATE STK_CONFIGURACION SET
     CONF_PERIODO_ACT = V_PERI_ACT,
     CONF_PERIODO_SGTE = V_PERI_SGTE
    WHERE CONF_EMPR = P_EMPRESA;
exception
when others then 
          ROLLBACK;
          raise_application_error(-20001,'Error en PP_ACT_STK_CONFIG'||SQLCODE||'-'||SQLERRM);
END;


-----------------------------
---PP_INSETAR_CONTROL_SERIE
------------------------------

BEGIN
  --Insertar nuevo registro de pre-cierre;
  --aqui :PARAMETER.P_PERI_ACT ya esta actualizado
  INSERT INTO GEN_CONTROL_CIERRE (
          COCI_EMPR, COCI_SUC, COCI_SISTEMA, COCI_PERIODO,
          COCI_IND_PRE_CERRADO, COCI_LUGAR_ORIGEN, 
          COCI_FECHA, COCI_LOGIN)
  VALUES (P_EMPRESA, 
  1,---SUCURSAL 
  1,---SISTEMA
  V_PERI_ACT, 'N', GEN_LUGAR_ORIGEN,
  SYSDATE, GEN_DEVUELVE_USER);
---  COMMIT;
  O_RETURN := 'FINANZA';

EXCEPTION
     WHEN OTHERS THEN 
       ROLLBACK;
          raise_application_error(-20001,'Error en PP_INSERTAR_CONTROL_CIERRE'||SQLCODE||'-'||SQLERRM);

END;



--------------------

PP_ACT_ART_EMPR_PERI(P_EMPRESA,
                     V_PERI_ACT ,
                     V_PERI_SGTE );



-----------------------------
-----PP_HABILITAR_MES ACTUAL
-----------------------------
  UPDATE STK_CONFIGURACION
     SET CONF_IND_HAB_MES_ACT = 'S'
   WHERE CONF_EMPR = P_EMPRESA;


------------------   
 if P_EMPRESA in( 1 , 20 ) then
  
  act_hist_client_h(in_empresa => p_empresa);
  
 end if;

 update GEN_CIERRE_GENERAL SET
 CGEN_STOCK = 'S',
 CGEN_STOCK_PERI = P_PERI_ACT
 WHERE CGEN_EMPR = P_EMPRESA;

 O_RETURN := 'FINANZA';

END PP_CIERRE_PERIODO;
END STKP001_H;
/
