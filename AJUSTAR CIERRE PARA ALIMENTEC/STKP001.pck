CREATE OR REPLACE PACKAGE STKP001 AS
  PROCEDURE PP_CERRAR_PERIODO_STK(P_EMPRESA IN NUMBER);
  PROCEDURE PP_PRE_CERRAR_PERIODO(P_EMPRESA IN NUMBER, P_PERIODO IN NUMBER);

  PROCEDURE PP_CERRAR_PERIODO(P_EMPRESA IN NUMBER, P_PERIODO IN NUMBER);
  PROCEDURE PP_VERIF_IMP_PROVISORIA(P_EMPRESA          IN NUMBER,
                                    P_PERI_ACT_FEC_INI IN DATE,
                                    P_PERI_ACT_FEC_FIN IN DATE);

  PROCEDURE PP_ALTER_TRIGGERS(OPERACION VARCHAR2);

  PROCEDURE PP_ACT_ART_DEP(P_EMPRESA           IN NUMBER,
                           P_PERIODO           IN NUMBER,
                           P_PERI_SGTE_FEC_INI IN DATE,
                           P_PERI_SGTE_FEC_FIN IN DATE);

  PROCEDURE PP_ACT_STK_PERIODO(P_EMPRESA           IN NUMBER,
                               P_PERIODO_SGTE      IN NUMBER,
                               P_PERI_SGTE_FEC_INI IN DATE,
                               P_PERI_SGTE_FEC_FIN IN DATE);
  PROCEDURE PP_ACT_STK_CONFIG(P_EMPRESA      IN NUMBER,
                              P_PERIODO      IN NUMBER,
                              P_PERIODO_SGTE IN NUMBER);

  PROCEDURE PP_INSERTAR_CONTROL_CIERRE(P_EMPRESA IN NUMBER,
                                       P_PERIODO IN NUMBER);
  PROCEDURE PP_ACT_ART_EMPR_PERI(P_EMPRESA      IN NUMBER,
                                 P_PERIODO      IN NUMBER,
                                 P_PERIODO_SGTE IN NUMBER);

  PROCEDURE PP_HABILITAR_MES_ACTUAL(P_EMPRESA IN NUMBER);
END;
/
CREATE OR REPLACE PACKAGE BODY STKP001 AS

  PROCEDURE PP_CERRAR_PERIODO_STK(P_EMPRESA IN NUMBER) IS
    v_periodo_act number;
  BEGIN
    select conf_periodo_act
      into V_PERIODO_act
      from stk_configuracion a, stk_periodo b
     where a.conf_empr = p_empresa
       and a.conf_periodo_act = peri_codigo
       and conf_empr = peri_empr;
  
    PP_PRE_CERRAR_PERIODO(P_EMPRESA, V_PERIODO_act);
  
    PP_CERRAR_PERIODO(P_EMPRESA, V_PERIODO_act);
    dbms_output.put_line('PERIODO:' || V_PERIODO_ACT);
  END;

  PROCEDURE PP_PRE_CERRAR_PERIODO(P_EMPRESA IN NUMBER, P_PERIODO IN NUMBER) IS
  BEGIN
    INSERT INTO GEN_CONTROL_CIERRE
      (COCI_EMPR,
       COCI_SUC,
       COCI_SISTEMA,
       COCI_PERIODO,
       COCI_IND_PRE_CERRADO,
       COCI_LUGAR_ORIGEN,
       COCI_FECHA,
       COCI_LOGIN)
    VALUES
      (P_EMPRESA, 1, 1, P_PERIODO, 'S', GEN_LUGAR_ORIGEN, SYSDATE, USER);
  
  EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
      --SI YA EXISTE UN REGISTRO ENTONCES
      --ACTUALIZAR EL DE LA SUCURSAL DONDE SE CORRE EL PROGRAMA
      UPDATE GEN_CONTROL_CIERRE
         SET COCI_IND_PRE_CERRADO = 'S',
             COCI_LUGAR_ORIGEN    = GEN_LUGAR_ORIGEN,
             COCI_FECHA           = SYSDATE,
             COCI_LOGIN           = USER
       WHERE COCI_EMPR = P_EMPRESA
         AND COCI_SUC = 1
         AND COCI_SISTEMA = 1
         AND COCI_PERIODO = P_PERIODO;
    
  END;

  PROCEDURE PP_CERRAR_PERIODO(P_EMPRESA IN NUMBER, P_PERIODO IN NUMBER) IS
  
    V_PERI_ACT_FEC_INI  DATE;
    V_PERI_ACT_FEC_FIN  DATE;
    V_PERIODO_SGTE      NUMBER;
    V_PERI_SGTE_FEC_INI DATE;
    V_PERI_SGTE_FEC_FIN DATE;
  BEGIN
  
    select conf_periodo_sgte,
           peri_fec_ini peri_fec_ini_act,
           peri_fec_fin peri_fec_fin_act,
           add_months(peri_fec_ini, 1) peri_fec_ini_sgte,
           add_months(peri_fec_fin, 1) peri_fec_fin_sgte
      into V_PERIODO_SGTE,
           V_PERI_ACT_FEC_INI,
           V_PERI_ACT_FEC_FIN,
           V_PERI_SGTE_FEC_INI,
           V_PERI_SGTE_FEC_FIN
      from stk_configuracion a, stk_periodo b
     where a.conf_empr = p_empresa
       and a.conf_periodo_act = peri_codigo
       and conf_empr = peri_empr;
  
    PP_VERIF_IMP_PROVISORIA(P_EMPRESA,
                            V_PERI_ACT_FEC_INI,
                            V_PERI_ACT_FEC_FIN);
  
    PP_ALTER_TRIGGERS('DISABLE');
  
    STK_ACT_ART_DEP_HIST(P_EMPRESA,
                         P_PERIODO,
                         V_PERI_ACT_FEC_INI,
                         V_PERI_ACT_FEC_FIN);
  
    PP_ACT_ART_DEP(P_EMPRESA,
                   P_PERIODO,
                   V_PERI_SGTE_FEC_INI,
                   V_PERI_SGTE_FEC_FIN);
  
    PP_ACT_STK_PERIODO(P_EMPRESA,
                       V_PERIODO_SGTE,
                       V_PERI_SGTE_FEC_INI,
                       V_PERI_SGTE_FEC_FIN);
  
    PP_ACT_STK_CONFIG(P_EMPRESA, P_PERIODO, V_PERIODO_SGTE);
  
    PP_INSERTAR_CONTROL_CIERRE(P_EMPRESA, V_PERIODO_SGTE);
  
    PP_ACT_ART_EMPR_PERI(P_EMPRESA, P_PERIODO, V_PERIODO_SGTE);
  
    /* PARA HABILITAR EL MES ACTUAL DE STOCK */
  
    PP_HABILITAR_MES_ACTUAL(P_EMPRESA);
    --ELIMINAR PEDIDOS ANULADOS
  
    IF P_EMPRESA in( 1 , 20 ) THEN
    
      ACT_HIST_CLIENT_H(in_empresa => p_empresa);
    
    END IF;
  
  END;

  PROCEDURE PP_ALTER_TRIGGERS(OPERACION VARCHAR2) IS
    CURSOR TABLE_CUR IS
      SELECT OWNER, OBJECT_NAME TABLE_NAME
        FROM ALL_OBJECTS
       WHERE OBJECT_NAME LIKE 'STK%'
         AND OBJECT_TYPE = 'TABLE'
         AND OWNER = 'ADCS';
  
    V_RESULTADO INTEGER;
    V_SQL       VARCHAR2(100);
    V_OWNER     VARCHAR2(40);
  BEGIN
    SELECT DISTINCT TABLE_OWNER
      INTO V_OWNER
      FROM ALL_SYNONYMS
     WHERE SYNONYM_NAME LIKE 'STK%';
  
    FOR RTABLE IN TABLE_CUR LOOP
      V_SQL       := 'ALTER TABLE ' || V_OWNER || '.' || RTABLE.TABLE_NAME || ' ' ||
                     OPERACION || ' ALL TRIGGERS';
      V_RESULTADO := GEN_DDL(V_SQL);
    END LOOP;
  
  EXCEPTION
    WHEN TOO_MANY_ROWS THEN
      RAISE_APPLICATION_ERROR(-20001,
                              'Avise al Dpto. de informatica. Se encontro mas de un esquema con sinonimos publicos!.');
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20001,
                              'Primero debe crear sinonimos publicos para los objetos (tablas, vistas, funciones, procedimientos, paquetes, etc.)');
  END;

  PROCEDURE PP_VERIF_IMP_PROVISORIA(P_EMPRESA          IN NUMBER,
                                    P_PERI_ACT_FEC_INI IN DATE,
                                    P_PERI_ACT_FEC_FIN IN DATE) IS
    V_DOC NUMBER;
  BEGIN
    SELECT DOCU_NRO_DOC
      INTO V_DOC
      FROM STK_DOCUMENTO, STK_CONFIGURACION
     WHERE DOCU_TIPO_MOV = CONF_IMP_REC
       AND DOCU_FEC_EMIS BETWEEN P_PERI_ACT_FEC_INI AND P_PERI_ACT_FEC_FIN
       AND DOCU_EMPR = P_EMPRESA
       AND DOCU_EMPR = CONF_EMPR;
  
    RAISE_APPLICATION_ERROR(-20001,
                            'El despacho ' || TO_CHAR(V_DOC) ||
                            ' esta ingresado de forma provisoria, no se puede realizar el cierre!.');
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
    WHEN TOO_MANY_ROWS THEN
      RAISE_APPLICATION_ERROR(-20001,
                              'Hay despachos ingresados en forma provisoria y no se puede realizar el cierre!.');
  END;

  PROCEDURE PP_ACT_ART_DEP(P_EMPRESA           IN NUMBER,
                           P_PERIODO           IN NUMBER,
                           P_PERI_SGTE_FEC_INI IN DATE,
                           P_PERI_SGTE_FEC_FIN IN DATE) IS
    /* CURSOR PARA ACTUALIZAR ARDE_CANT_INI, ARDE_CANT_ENT Y ARDE_CANT_SAL */
    CURSOR ENSA_CUR IS
      SELECT DOCU_EMPR,
             DOCU_SUC_ORIG,
             DOCU_DEP_ORIG,
             DETA_ART,
             SUM(DECODE(OPER_ENT_SAL, 'E', DETA_CANT, 0)) TOT_ENT,
             SUM(DECODE(OPER_ENT_SAL, 'S', DETA_CANT, 0)) TOT_SAL
        FROM STK_OPERACION, STK_DOCUMENTO, STK_DOCUMENTO_DET
       WHERE OPER_CODIGO = DOCU_CODIGO_OPER
         AND OPER_EMPR = DOCU_EMPR
            
         AND DOCU_CLAVE = DETA_CLAVE_DOC
         AND DOCU_EMPR = DETA_EMPR
            
         AND DOCU_EMPR = P_EMPRESA
         AND DOCU_FEC_EMIS BETWEEN P_PERI_SGTE_FEC_INI AND
             P_PERI_SGTE_FEC_FIN
       GROUP BY DOCU_EMPR, DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART
       ORDER BY DOCU_EMPR, DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART;
  
    V_EMPR NUMBER := 0;
    V_SUC  NUMBER := 0;
    V_DEP  NUMBER := 0;
    V_ART  NUMBER := 0;
  
  BEGIN
  
    /* CERAR ARDE_CANT_ENT Y ARDE_CANT_SAL EN STK_ARTICULO_DEPOSITO */
    UPDATE STK_ARTICULO_DEPOSITO
       SET ARDE_CANT_INI = ARDE_CANT_ACT,
           ARDE_CANT_ENT = 0,
           ARDE_CANT_SAL = 0
     WHERE ARDE_EMPR = P_EMPRESA;
  
    FOR RENSA IN ENSA_CUR LOOP
      UPDATE STK_ARTICULO_DEPOSITO
         SET ARDE_CANT_INI = ARDE_CANT_ACT - RENSA.TOT_ENT + RENSA.TOT_SAL,
             ARDE_CANT_ENT = RENSA.TOT_ENT,
             ARDE_CANT_SAL = RENSA.TOT_SAL
       WHERE ARDE_EMPR = RENSA.DOCU_EMPR
         AND ARDE_SUC = RENSA.DOCU_SUC_ORIG
         AND ARDE_DEP = RENSA.DOCU_DEP_ORIG
         AND ARDE_ART = RENSA.DETA_ART;
    
    END LOOP;
  
  END;

  PROCEDURE PP_ACT_STK_PERIODO(P_EMPRESA           IN NUMBER,
                               P_PERIODO_SGTE      IN NUMBER,
                               P_PERI_SGTE_FEC_INI IN DATE,
                               P_PERI_SGTE_FEC_FIN IN DATE) IS
    V_VARIABLE VARCHAR2(1);
  BEGIN
  
    SELECT 'X'
      INTO V_VARIABLE
      FROM STK_PERIODO
     WHERE PERI_CODIGO = P_PERIODO_SGTE
       AND PERI_EMPR = P_EMPRESA;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      INSERT INTO STK_PERIODO
        (PERI_CODIGO, PERI_FEC_INI, PERI_FEC_FIN, PERI_EMPR)
      VALUES
        (P_PERIODO_SGTE,
         P_PERI_SGTE_FEC_INI,
         P_PERI_SGTE_FEC_FIN,
         P_EMPRESA);
  END;

  PROCEDURE PP_ACT_STK_CONFIG(P_EMPRESA      IN NUMBER,
                              P_PERIODO      IN NUMBER,
                              P_PERIODO_SGTE IN NUMBER) IS
  BEGIN
    UPDATE STK_CONFIGURACION
       SET CONF_PERIODO_ACT = P_PERIODO, CONF_PERIODO_SGTE = P_PERIODO_SGTE
     WHERE CONF_EMPR = P_EMPRESA;
  END;

  PROCEDURE PP_INSERTAR_CONTROL_CIERRE(P_EMPRESA IN NUMBER,
                                       P_PERIODO IN NUMBER) IS
    V_LUGAR_ORIGEN NUMBER := GEN_LUGAR_ORIGEN;
  BEGIN
    --INSERTAR NUEVO REGISTRO DE PRE-CIERRE;
    --AQUI :PARAMETER.P_PERI_ACT YA ESTA ACTUALIZADO
    INSERT INTO GEN_CONTROL_CIERRE
      (COCI_EMPR,
       COCI_SUC,
       COCI_SISTEMA,
       COCI_PERIODO,
       COCI_IND_PRE_CERRADO,
       COCI_LUGAR_ORIGEN,
       COCI_FECHA,
       COCI_LOGIN)
    VALUES
      (P_EMPRESA, 1, 1, P_PERIODO, 'N', V_LUGAR_ORIGEN, SYSDATE, USER);
  exception 
    when others then 
       null;
  
  END;

  PROCEDURE PP_ACT_ART_EMPR_PERI(P_EMPRESA      IN NUMBER,
                                 P_PERIODO      IN NUMBER,
                                 P_PERIODO_SGTE IN NUMBER) IS
    /* OJO QUE PARAMETER.P_PERI_ACT YA SE ACTUALIZO */
    CURSOR AEP_CUR IS
      SELECT *
        FROM STK_ART_EMPR_PERI
       WHERE AEP_EMPR = P_EMPRESA
         AND AEP_PERIODO = P_PERIODO;
  
    V_EMPR               NUMBER := 0;
    V_ART                NUMBER := 0;
    V_COSTO_INI_LOC      NUMBER;
    V_COSTO_INI_MON      NUMBER;
    V_COSTO_INI_LOC_OPER NUMBER;
    V_COSTO_INI_MON_OPER NUMBER;
  
  BEGIN
  
   delete STK_ART_EMPR_PERI
       WHERE AEP_EMPR = P_EMPRESA
         AND AEP_PERIODO = P_PERIODO_SGTE;
  
  
    /* PARA CADA REGISTRO INSERTAR UNO PARA EL SGTE.PERIODO */
    FOR RAEP IN AEP_CUR LOOP
    
      /* CALCULO DE COSTO INICIAL EXPRESADO EN MONEDA LOCAL */
      V_COSTO_INI_LOC := 0;
      /* MODIFICADO POR LUIS FEDRIANI EL 21/11/1997 */
      /* LE PUSE DIFERENTE DE CERO PARA QUE TAMBIEN ACTUALIZE EL COSTO*/
      /* SI ES NEGATIVA LA EXISTENCIA(ANTES TENIA MAYOR QUE)*/
      IF RAEP.AEP_TOT_EXIST <> 0 THEN
        V_COSTO_INI_LOC := RAEP.AEP_TOT_EXIST * RAEP.AEP_COSTO_PROM_LOC;
      END IF;
    
      IF RAEP.AEP_TOT_EXIST_OPER <> 0 THEN
        V_COSTO_INI_LOC_OPER := RAEP.AEP_TOT_EXIST_OPER *
                                RAEP.AEP_COSTO_PROM_LOC_OPER;
      END IF;
    
      /* CALCULO DE COSTO INICIAL EXPRESADO EN MONEDA DE REFERENCIA (U$) */
      V_COSTO_INI_MON := 0;
      /* MODIFICADO POR LUIS FEDRIANI EL 21/11/1997 */
      /* LE PUSE DIFERENTE DE CERO PARA QUE TAMBIEN ACTUALIZE EL COSTO*/
      /* SI ES NEGATIVA LA EXISTENCIA (ANTES TENIA MAYOR QUE)*/
    
      IF RAEP.AEP_TOT_EXIST <> 0 THEN
        V_COSTO_INI_MON := RAEP.AEP_TOT_EXIST * RAEP.AEP_COSTO_PROM_MON;
      END IF;
    
      IF RAEP.AEP_TOT_EXIST_OPER <> 0 THEN
        V_COSTO_INI_MON_OPER := RAEP.AEP_TOT_EXIST_OPER *
                                RAEP.AEP_COSTO_PROM_MON_OPER;
      END IF;
    
      INSERT INTO STK_ART_EMPR_PERI
        (AEP_PERIODO,
         AEP_EMPR,
         AEP_ART,
         AEP_TOT_EXIST,
         AEP_EXIST_INI,
         AEP_COSTO_INI_LOC,
         AEP_COSTO_INI_MON,
         AEP_COSTO_PROM_INI_LOC,
         AEP_COSTO_PROM_INI_MON,
         AEP_COSTO_PROM_LOC,
         AEP_COSTO_PROM_MON,
         AEP_EC_COMPRA,
         AEP_EL_COMPRA,
         AEP_EM_COMPRA,
         AEP_SC_DEV_COMPRA,
         AEP_SL_DEV_COMPRA,
         AEP_SM_DEV_COMPRA,
         AEP_SC_VTA,
         AEP_SL_VTA,
         AEP_SM_VTA,
         AEP_EC_DEV_VTA,
         AEP_EL_DEV_VTA,
         AEP_EM_DEV_VTA,
         AEP_EC_PROD,
         AEP_EL_PROD,
         AEP_EM_PROD,
         AEP_SC_PROD,
         AEP_EC_KIT,
         AEP_EL_KIT,
         AEP_EM_KIT,
         AEP_SC_KIT,
         AEP_SL_KIT,
         AEP_SM_KIT,
         AEP_SC_PERDIDA,
         AEP_SC_CONS,
         AEP_C_DIF_INV,
         AEP_C_TRANSF,
         AEP_C_REM,
         AEP_EC_ARM_DES,
         AEP_EL_ARM_DES,
         AEP_EM_ARM_DES,
         AEP_SC_ARM_DES,
         AEP_SL_ARM_DES,
         AEP_SM_ARM_DES,
         AEP_EL_DEV_PROD,
         AEP_EM_DEV_PROD,
         AEP_EC_DEV_PROD,
         AEP_TOT_EXIST_OPER,
         AEP_EXIST_INI_OPER,
         AEP_COSTO_INI_LOC_OPER,
         AEP_COSTO_INI_MON_OPER,
         AEP_COSTO_PROM_INI_LOC_OPER,
         AEP_COSTO_PROM_INI_MON_OPER,
         AEP_COSTO_PROM_LOC_OPER,
         AEP_COSTO_PROM_MON_OPER)
      VALUES
        (P_PERIODO_SGTE,
         RAEP.AEP_EMPR,
         RAEP.AEP_ART,
         RAEP.AEP_TOT_EXIST,
         RAEP.AEP_TOT_EXIST,
         V_COSTO_INI_LOC,
         V_COSTO_INI_MON,
         RAEP.AEP_COSTO_PROM_LOC,
         RAEP.AEP_COSTO_PROM_MON,
         RAEP.AEP_COSTO_PROM_LOC,
         RAEP.AEP_COSTO_PROM_MON,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         RAEP.AEP_TOT_EXIST_OPER,
         RAEP.AEP_TOT_EXIST_OPER,
         V_COSTO_INI_LOC_OPER,
         V_COSTO_INI_MON_OPER,
         RAEP.AEP_COSTO_PROM_LOC_OPER,
         RAEP.AEP_COSTO_PROM_MON_OPER,
         RAEP.AEP_COSTO_PROM_LOC_OPER,
         RAEP.AEP_COSTO_PROM_MON_OPER);
    
    END LOOP;
  
  END;

  PROCEDURE PP_HABILITAR_MES_ACTUAL(P_EMPRESA IN NUMBER) IS
  BEGIN
    UPDATE STK_CONFIGURACION
       SET CONF_IND_HAB_MES_ACT = 'S'
     WHERE CONF_EMPR = P_EMPRESA;
  END;

END;
/
