create or replace procedure act_hist_client_h(in_empresa in number := null) is
  --Este script, se usara para automatizar los procesos de copia de la tabla clientes, al historial y tambien, la captura del codigo del periodo de finanzas
  --Se ejecutara en la tarea programada
  v_peri_act number;

begin

  --Hace la copia y lo vuelca a la tabla historial
  select stk_configuracion.conf_periodo_act
    into v_peri_act
    from stk_configuracion
   where conf_empr = in_empresa;

  insert into fin_cli_vend_hist
    (cliv_codigo,
     cliv_vendedor,
     cliv_periodo,
     cliv_lunes,
     cliv_martes,
     cliv_miercoles,
     cliv_jueves,
     cliv_viernes,
     cliv_sabado,
     cliv_est_cli,
     cliv_empr)
    select cli_codigo,
           cli_vendedor,
           v_peri_act,
           Cli_Dia_Lunes,
           Cli_Dia_Martes,
           Cli_Dia_Miercoles,
           Cli_Dia_Jueves,
           Cli_Dia_Viernes,
           Cli_Dia_Sabado,
           cli_est_cli,
           cli_empr
      from FIN_CLIENTE
     where cli_empr = in_empresa;

  commit;

exception
  when others then
     null;
end act_hist_client_h;

 

 
 
 
/
