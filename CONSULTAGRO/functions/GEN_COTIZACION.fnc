CREATE OR REPLACE FUNCTION GEN_COTIZACION(P_EMPRESA   IN NUMBER,
                                          P_MONEDA    IN NUMBER,
                                          P_FECHA     IN DATE,
                                          P_TIPO_TASA IN VARCHAR,
                                          P_MAX_COT   IN VARCHAR2 DEFAULT NULL --CON ESTO SE PODRA TRAER LA ULTIMA COTIZACION CARGADA
                                          ) RETURN NUMBER IS
  V_COT_COMPRA NUMBER;
  V_COT_VENTA  NUMBER;
BEGIN

  IF P_FECHA <= TO_DATE('31/12/2021') THEN
    --======EN EL HISTORICO SE USA ESTA FUNCION ---APERALTA
    RETURN FIN_BUSCAR_COTIZACION_FEC(FEC  => P_FECHA,
                                     MON  => P_MONEDA,
                                     EMPR => P_EMPRESA);
  
  ELSE
  
    --====ESTO EMPIEZA FUNCIONAR RECIEN APARTIR  DE ENERO 2022 
    BEGIN
      IF P_MONEDA NOT IN (4, 5, 6) THEN
        SELECT CT.COT_COMPRA, CT.COT_VENTA
          INTO V_COT_COMPRA, V_COT_VENTA
          FROM STK_COTIZACION CT
         WHERE COT_FEC = NVL(TO_CHAR(TRUNC(TO_DATE(P_FECHA)), 'DD/MM/YYYY'),
                             TRUNC(SYSDATE))
           AND COT_MON = P_MONEDA
           AND COT_EMPR = 1; ---SE DEFINIO QUE TODA LAS COTIZACIONES SOLO VA A ESTIRAR DE LA EMPRESA 1
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        IF P_MONEDA = 1 THEN
          RETURN 1;
        ELSE
        
          IF NVL(P_MAX_COT, 'N') = 'S' THEN
            SELECT CT.COT_COMPRA, CT.COT_VENTA
              INTO V_COT_COMPRA, V_COT_VENTA
              FROM STK_COTIZACION CT
             WHERE COT_FEC =
                   (SELECT MAX(COT_FEC)
                      FROM STK_COTIZACION CT
                     WHERE COT_FEC <=
                           NVL(TO_CHAR(TRUNC(TO_DATE(P_FECHA)), 'DD/MM/YYYY'),
                               TRUNC(SYSDATE))
                       AND COT_MON = P_MONEDA
                       AND COT_EMPR = 1)
               AND COT_MON = P_MONEDA
               AND COT_EMPR = 1;
          
          ELSE
          
            RAISE_APPLICATION_ERROR(-20001,
                                    'Primero debe ingresar la cotizacion del dia ' ||
                                    TO_CHAR(P_FECHA, 'DD/MM/YYYY') ||
                                    ' para la moneda ' || P_MONEDA || '.!');
          END IF;
        
        END IF;
    END;
  
    IF P_TIPO_TASA = 'C' THEN
      RETURN V_COT_COMPRA;
    ELSIF P_TIPO_TASA = 'V' THEN
      RETURN V_COT_VENTA;
    END IF;
  
    --------------------==========================================================
  
  END IF;

END GEN_COTIZACION;
/
