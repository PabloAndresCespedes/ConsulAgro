CREATE OR REPLACE PACKAGE CNTP103 IS

  PROCEDURE PP_ACTUALIZAR_REGISTRO(P_EMPRESA            IN NUMBER,
                                   P_FECHA_INICIO       IN DATE,
                                   P_FECHA_FIN          IN DATE,
                                   P_PERIODO            IN NUMBER,
                                   P_MENSAJE            OUT NUMBER,
                                   P_APP_USER           IN VARCHAR2,
                                   P_DIFERENCIA         IN VARCHAR2,
                                   P_TOTAL_IPS_PATRONAL IN NUMBER,
                                   P_APP_SESSION        IN NUMBER,
                                   P_IRASIS             IN VARCHAR2,
                                   P_CLAVE_ASIENTO_AUX  OUT NUMBER,
                                   P_PRIMER_ASIENTO     OUT NUMBER,
                                   P_ULTIMO_ASIENTO     OUT NUMBER);

  PROCEDURE PP_CARGAR_DATOS(P_EMPRESA           IN NUMBER,
                            P_FEC_ASI_INI       OUT DATE,
                            P_FEC_ASI_FIN       OUT DATE,
                            P_TIPO              OUT VARCHAR2,
                            P_FEC_TRANSFERENCIA OUT DATE,
                            P_USUARIO           OUT VARCHAR2);

  PROCEDURE PP_CARGAR_DETALLE_IP(P_EMPRESA IN NUMBER,
                                 P_FECHA   IN DATE,
                                 P_SESSION IN NUMBER,
                                 P_USUARIO IN VARCHAR2);

  PROCEDURE PP_VALIDAR_PERIODO(P_EMPRESA      IN NUMBER,
                               P_FECHA_INICIO IN DATE,
                               P_FECHA_FIN    IN DATE);

  PROCEDURE PP_VALIDAR_ASI_EXISTENTE(P_EMPRESA        IN NUMBER,
                                     P_OPERACION      IN VARCHAR2,
                                     P_PERI_FECHA_INI IN DATE);

  PROCEDURE PP_VALIDAR_CANALES_IP(P_CCO_USA_CANAL IN VARCHAR2,
                                  P_CANAL         IN VARCHAR2);

  PROCEDURE PP_GENERAR_ASI_PRCLI(P_EMPRESA      IN NUMBER,
                                 P_FECHA_INICIO IN DATE,
                                 P_FECHA_FIN    IN DATE,
                                 P_MENSAJE      OUT NUMBER);

  PROCEDURE PP_GENERAR_ASI_INT_PMO(P_EMPRESA      IN NUMBER,
                                   P_FECHA_INICIO IN DATE,
                                   P_FECHA_FIN    IN DATE,
                                   P_MENSAJE      OUT NUMBER);

  PROCEDURE PP_GENERAR_ASI_DEV_IVA(P_EMPRESA      IN NUMBER,
                                   P_FECHA_INICIO IN DATE,
                                   P_FECHA_FIN    IN DATE,
                                   P_MENSAJE      OUT NUMBER);

  PROCEDURE PP_GENERAR_ASI_BCO(P_EMPRESA      IN NUMBER,
                               P_FECHA_INICIO IN DATE,
                               P_FECHA_FIN    IN DATE,
                               P_MENSAJE      OUT NUMBER);

  PROCEDURE PP_GENERAR_ASI_COMBUSTIBLE(P_EMPRESA      IN NUMBER,
                                       P_FECHA_INICIO IN DATE,
                                       P_FECHA_FIN    IN DATE,
                                       P_MENSAJE      OUT NUMBER);

  PROCEDURE FIN_ASI_PREV_INCOBRABLES(P_EMPRESA     IN NUMBER,
                                     P_FECHA_DESDE IN DATE,
                                     P_FECHA_HASTA IN DATE,
                                     P_COD_PERIODO NUMBER,
                                     P_LOGIN       IN VARCHAR2);

  PROCEDURE PP_GENERAR_ASI_AGUINALDO(P_EMPRESA      IN NUMBER,
                                     P_FECHA_INICIO IN DATE,
                                     P_FECHA_FIN    IN DATE,
                                     P_MENSAJE      OUT NUMBER);
                                     
                                     
   PROCEDURE PP_GENERAR_ASI_INDEMNIZACION(P_EMPRESA      IN NUMBER,
                                       P_FECHA_INICIO IN DATE,
                                       P_FECHA_FIN    IN DATE,
                                       P_MENSAJE      OUT NUMBER)  ;                                  
                                     

  PROCEDURE PP_GENERAR_ASI_COSTO_EXPORT(P_EMPRESA      IN NUMBER,
                                        P_FECHA_INICIO IN DATE,
                                        P_FECHA_FIN    IN DATE,
                                        P_MENSAJE      OUT NUMBER);

  PROCEDURE PP_ACTUALIZAR_CNT_ASIENTO_IP(P_EMPRESA      IN NUMBER,
                                         P_FECHA_INICIO IN DATE,
                                         P_FECHA_FIN    IN DATE,
                                         P_DIFERENCIA   IN VARCHAR2,
                                         P_IPS_PATRONAL IN NUMBER,
                                         P_SESSION      IN NUMBER);

  PROCEDURE STK_ASIENTO_DIF_INVENTARIO(P_EMPRESA     IN NUMBER,
                                       P_FECHA_DESDE IN DATE,
                                       P_FECHA_HASTA IN DATE,
                                       P_COD_PERIODO IN NUMBER,
                                       P_LOGIN       IN VARCHAR2);

  PROCEDURE STK_ASIENTO_COSTO_VENTA(P_EMPRESA     IN NUMBER,
                                    P_FECHA_DESDE IN DATE,
                                    P_FECHA_HASTA IN DATE,
                                    P_COD_PERIODO IN NUMBER,
                                    P_LOGIN       IN VARCHAR2);

  PROCEDURE STK_ASIENTO_MOVIMIENTO_STK(P_EMPRESA     IN NUMBER,
                                       P_FECHA_DESDE IN DATE,
                                       P_FECHA_HASTA IN DATE,
                                       P_COD_PERIODO IN NUMBER,
                                       P_LOGIN       IN VARCHAR2);

  PROCEDURE STK_ASIENTO_IMPORTACION(P_EMPRESA     IN NUMBER,
                                    P_FECHA_DESDE IN DATE,
                                    P_FECHA_HASTA IN DATE,
                                    P_COD_PERIODO IN NUMBER,
                                    P_LOGIN       IN VARCHAR2);

  PROCEDURE PP_ACTUALIZAR_CNT_CONFI(P_EMPRESA      IN NUMBER,
                                    P_FECHA_INICIO IN DATE,
                                    P_FECHA_FIN    IN DATE);

  PROCEDURE PP_ACTUALIZAR_FIN_CONFI(P_EMPRESA      IN NUMBER,
                                    P_FECHA_INICIO IN DATE,
                                    P_FECHA_FIN    IN DATE);

  PROCEDURE PP_BORRAR_ASIENTOS_I(P_EMPRESA      IN NUMBER,
                                 P_FECHA_INICIO IN DATE,
                                 P_FECHA_FIN    IN DATE);

  PROCEDURE PP_BORRAR_REGISTRO(P_EMPRESA      IN NUMBER,
                               P_FECHA_INICIO IN DATE,
                               P_FECHA_FIN    IN DATE);

  PROCEDURE PP_ASIENTO_IRACIS(P_EMPRESA       IN NUMBER,
                              P_COD_PERIODO   IN NUMBER,
                              P_CLAVE_ASIENTO OUT NUMBER);

  PROCEDURE PP_CREAR_DEV_PREST_BANC(P_EMPRESA     IN NUMBER,
                                    P_FECHA_DESDE IN DATE,
                                    P_FECHA_HASTA IN DATE,
                                    P_COD_PERIODO IN NUMBER);

  PROCEDURE PP_MENSAJE_FINAL(P_EMPRESA        IN NUMBER,
                             P_FECHA_INICIO   IN DATE,
                             P_FECHA_FIN      IN DATE,
                             P_PRIMER_ASIENTO OUT NUMBER,
                             P_ULTIMO_ASIENTO OUT NUMBER);

  FUNCTION FP_VALIDAR_AGUINALDO_PROV(P_EMPRESA IN NUMBER) RETURN VARCHAR2;
  
  PROCEDURE PP_PREVISION_PREMIOS_GERENCIA(P_EMPRESA       IN NUMBER,
                                          P_FECHA_DESDE   IN DATE,
                                          P_FECHA_HASTA   IN DATE,
                                          P_COD_PERIODO   IN NUMBER,
                                          P_LOGIN         IN VARCHAR2,
                                          P_CLAVE_ASIENTO OUT NUMBER);
                                          
   PROCEDURE STK_ASIENTO_MOVIMIENTO_PRD(P_EMPRESA     IN NUMBER,
                                       P_FECHA_DESDE IN DATE,
                                       P_FECHA_HASTA IN DATE,
                                       P_COD_PERIODO IN NUMBER,
                                       P_LOGIN       IN VARCHAR2);
END CNTP103;
/
CREATE OR REPLACE PACKAGE BODY CNTP103 IS

  PROCEDURE PP_ACTUALIZAR_REGISTRO(P_EMPRESA            IN NUMBER,
                                   P_FECHA_INICIO       IN DATE,
                                   P_FECHA_FIN          IN DATE,
                                   P_PERIODO            IN NUMBER,
                                   P_MENSAJE            OUT NUMBER,
                                   P_APP_USER           IN VARCHAR2,
                                   P_DIFERENCIA         IN VARCHAR2,
                                   P_TOTAL_IPS_PATRONAL IN NUMBER,
                                   P_APP_SESSION        IN NUMBER,
                                   P_IRASIS             IN VARCHAR2,
                                   P_CLAVE_ASIENTO_AUX  OUT NUMBER,
                                   P_PRIMER_ASIENTO     OUT NUMBER,
                                   P_ULTIMO_ASIENTO     OUT NUMBER)
  
   IS
  BEGIN
  
    CNTP103.PP_VALIDAR_ASI_EXISTENTE(P_EMPRESA        => P_EMPRESA,
                                     P_OPERACION      => 'INSERTAR',
                                     P_PERI_FECHA_INI => P_FECHA_INICIO);
  
    
    --GENERAR ASIENTOS DE DEUDORES Y ACREEDORES....
    CNTP103.PP_GENERAR_ASI_PRCLI(P_EMPRESA      => P_EMPRESA,
                                 P_FECHA_INICIO => P_FECHA_INICIO,
                                 P_FECHA_FIN    => P_FECHA_FIN,
                                 P_MENSAJE      => P_MENSAJE);
  
    CNTP103.PP_GENERAR_ASI_INT_PMO(P_EMPRESA      => P_EMPRESA,
                                   P_FECHA_INICIO => P_FECHA_INICIO,
                                   P_FECHA_FIN    => P_FECHA_FIN,
                                   P_MENSAJE      => P_MENSAJE);
  
    CNTP103.PP_GENERAR_ASI_DEV_IVA(P_EMPRESA      => P_EMPRESA,
                                   P_FECHA_INICIO => P_FECHA_INICIO,
                                   P_FECHA_FIN    => P_FECHA_FIN,
                                   P_MENSAJE      => P_MENSAJE);
  
    CNTP103.PP_GENERAR_ASI_COMBUSTIBLE(P_EMPRESA      => P_EMPRESA,
                                       P_FECHA_INICIO => P_FECHA_INICIO,
                                       P_FECHA_FIN    => P_FECHA_FIN,
                                       P_MENSAJE      => P_MENSAJE);
 
    /*
    -- 11/08/2022 14:58:29 @PabloACespedes \(^-^)/
    -- LAURA AYALA CONTABILIDAD:
    Y no se puede sacar no eso? Consultagro no creo que tenga incobrable, ya que los clientes son las mismas empresas del grupo no mas
    CNTP103.FIN_ASI_PREV_INCOBRABLES(P_EMPRESA     => P_EMPRESA,
                                     P_FECHA_DESDE => NULL,
                                     P_FECHA_HASTA => NULL,
                                     P_COD_PERIODO => P_PERIODO,
                                     P_LOGIN       => P_APP_USER);
    
    */
    --GENERAR ASIENTOS DE COSTO DE VENTAS....

    IF P_FECHA_FIN >= '01/01/2018' THEN
    
      CNTP103.STK_ASIENTO_COSTO_VENTA(P_EMPRESA     => P_EMPRESA,
                                      P_FECHA_DESDE => NULL,
                                      P_FECHA_HASTA => NULL,
                                      P_COD_PERIODO => P_PERIODO,
                                      P_LOGIN       => P_APP_USER);
    
      
    END IF;
  
    --GENERAR ASIENTOS DE DIF. INVENTARIO....
    IF P_FECHA_FIN >= '01/01/2018' THEN
    
      CNTP103.STK_ASIENTO_DIF_INVENTARIO(P_EMPRESA     => P_EMPRESA,
                                         P_FECHA_DESDE => NULL,
                                         P_FECHA_HASTA => NULL,
                                         P_COD_PERIODO => P_PERIODO,
                                         P_LOGIN       => P_APP_USER);
    
    
    END IF;
  
    IF P_FECHA_FIN >= '01/02/2018' THEN
    
      CNTP103.STK_ASIENTO_MOVIMIENTO_STK(P_EMPRESA     => P_EMPRESA,
                                         P_FECHA_DESDE => NULL,
                                         P_FECHA_HASTA => NULL,
                                         P_COD_PERIODO => P_PERIODO,
                                         P_LOGIN       => P_APP_USER);
    
    END IF;
    
    
    IF P_FECHA_FIN >= '01/02/2018' THEN
    
      CNTP103.STK_ASIENTO_IMPORTACION(P_EMPRESA     => P_EMPRESA,
                                      P_FECHA_DESDE => NULL,
                                      P_FECHA_HASTA => NULL,
                                      P_COD_PERIODO => P_PERIODO,
                                      P_LOGIN       => P_APP_USER);
    
    
    
    END IF;
    
     CNTP103.STK_ASIENTO_MOVIMIENTO_PRD(P_EMPRESA     => P_EMPRESA,
                                      P_FECHA_DESDE => NULL,
                                      P_FECHA_HASTA => NULL,
                                      P_COD_PERIODO => P_PERIODO,
                                      P_LOGIN       => P_APP_USER);
    
  
    --GENERAR ASIENTOS DE DEVENGAMIENTOS DE PRESTAMOS....
  
    CNTP103.PP_CREAR_DEV_PREST_BANC(P_EMPRESA     => P_EMPRESA,
                                    P_FECHA_DESDE => NULL,
                                    P_FECHA_HASTA => NULL,
                                    P_COD_PERIODO => P_PERIODO);
  
    ---GENERAR ASIENTOS CAJA Y BANCOS....
    CNTP103.PP_GENERAR_ASI_BCO(P_EMPRESA      => P_EMPRESA,
                               P_FECHA_INICIO => P_FECHA_INICIO,
                               P_FECHA_FIN    => P_FECHA_FIN,
                               P_MENSAJE      => P_MENSAJE);
  
    CNTP103.PP_ACTUALIZAR_CNT_CONFI(P_EMPRESA      => P_EMPRESA,
                                    P_FECHA_INICIO => P_FECHA_INICIO,
                                    P_FECHA_FIN    => P_FECHA_FIN);
  
    --GENERAR ASIENTOS DE INTERESES DEVENGADOS Y AGUINALDO
    CNTP103.PP_GENERAR_ASI_AGUINALDO(P_EMPRESA      => P_EMPRESA,
                                     P_FECHA_INICIO => P_FECHA_INICIO,
                                     P_FECHA_FIN    => P_FECHA_FIN,
                                     P_MENSAJE      => P_MENSAJE);
                                     
                                    
                                     
   --GENERAR ASIENTOS DE INDEMNIZACION Y PRE AVISO
    CNTP103.PP_GENERAR_ASI_INDEMNIZACION(P_EMPRESA      => P_EMPRESA,
                                         P_FECHA_INICIO => P_FECHA_INICIO,
                                         P_FECHA_FIN    => P_FECHA_FIN,
                                         P_MENSAJE      => P_MENSAJE);                                  
                                     
  
    --GENERAR ASIENTOS DE EXPORTACIONES....
  
    CNTP103.PP_GENERAR_ASI_COSTO_EXPORT(P_EMPRESA      => P_EMPRESA,
                                        P_FECHA_INICIO => P_FECHA_INICIO,
                                        P_FECHA_FIN    => P_FECHA_FIN,
                                        P_MENSAJE      => P_MENSAJE);
  
    --GENERAR ASIENTOS DE APORTE OBRERO PATRONAL....
  
    CNTP103.PP_ACTUALIZAR_CNT_ASIENTO_IP(P_EMPRESA      => P_EMPRESA,
                                         P_FECHA_INICIO => P_FECHA_INICIO,
                                         P_FECHA_FIN    => P_FECHA_FIN,
                                         P_DIFERENCIA   => P_DIFERENCIA,
                                         P_IPS_PATRONAL => P_TOTAL_IPS_PATRONAL,
                                         P_SESSION      => P_APP_SESSION);
  
    CNTP103.PP_ACTUALIZAR_FIN_CONFI(P_EMPRESA      => P_EMPRESA,
                                    P_FECHA_INICIO => P_FECHA_INICIO,
                                    P_FECHA_FIN    => P_FECHA_FIN);
  
    --GENERAR ASIENTO IRASIS
    
    IF P_IRASIS IS NOT NULL THEN
   
      CNTP103.PP_ASIENTO_IRACIS(P_EMPRESA       => P_EMPRESA,
                                P_COD_PERIODO   => P_PERIODO,
                                P_CLAVE_ASIENTO => P_CLAVE_ASIENTO_AUX);
    
    END IF;

    IF P_FECHA_FIN > TO_DATE('31/05/2021') THEN
      cntp103.pp_prevision_premios_gerencia(p_empresa       => P_EMPRESA,
                                            p_fecha_desde   => P_FECHA_INICIO,
                                            p_fecha_hasta   => P_FECHA_FIN,
                                            p_cod_periodo   => NULL,
                                            p_login         => gen_devuelve_user,
                                            P_CLAVE_ASIENTO => P_CLAVE_ASIENTO_AUX);
    END IF;
  
    --RAISE_APPLICATION_ERROR(-20001,'prueba7');
    
    CNTP103.PP_MENSAJE_FINAL(P_EMPRESA        => P_EMPRESA,
                             P_FECHA_INICIO   => P_FECHA_INICIO,
                             P_FECHA_FIN      => P_FECHA_FIN,
                             P_PRIMER_ASIENTO => P_PRIMER_ASIENTO,
                             P_ULTIMO_ASIENTO => P_ULTIMO_ASIENTO);
  
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      RAISE_APPLICATION_ERROR(-20001, SQLERRM);
    
  END PP_ACTUALIZAR_REGISTRO;

  PROCEDURE PP_CARGAR_DATOS(P_EMPRESA           IN NUMBER,
                            P_FEC_ASI_INI       OUT DATE,
                            P_FEC_ASI_FIN       OUT DATE,
                            P_TIPO              OUT VARCHAR2,
                            P_FEC_TRANSFERENCIA OUT DATE,
                            P_USUARIO           OUT VARCHAR2) AS
  
  BEGIN
  
    SELECT P.PERI_FEC_INI,
           P.PERI_FEC_FIN,
           DECODE(D.AFD_CNT_CLAVE_ASI, NULL, 'MANUAL', 'AUTOMATICO'),
           ASI_FEC_GRAB,
           ASI_LOGIN
    
      INTO P_FEC_ASI_INI,
           P_FEC_ASI_FIN,
           P_TIPO,
           P_FEC_TRANSFERENCIA,
           P_USUARIO
      FROM FIN_PERIODO P,
           CNT_ASIENTO_FIN_DETALLE D,
           CNT_ASIENTO,
           (SELECT MAX(ASI_FEC) ULT_FECHA
              FROM CNT_ASIENTO A
             WHERE ASI_EMPR = P_EMPRESA
               AND A.ASI_SIST = 'FIN') MAX_FEC
     WHERE (ULT_FECHA) BETWEEN PERI_FEC_INI AND PERI_FEC_FIN
       AND P.PERI_EMPR = P_EMPRESA
       AND D.AFD_CNT_CLAVE_ASI(+) BETWEEN P.PERI_CLAVE_FIN_ASI_INI AND
           P.PERI_CLAVE_FIN_ASI_FIN
       AND P.PERI_EMPR = D.AFD_EMPR(+)
       AND PERI_CLAVE_FIN_ASI_INI = ASI_CLAVE
       AND PERI_EMPR = ASI_EMPR
     GROUP BY P.PERI_FEC_INI,
              ASI_LOGIN,
              P.PERI_FEC_FIN,
              ASI_FEC_GRAB,
              P.PERI_CLAVE_FIN_ASI_FIN,
              DECODE(D.AFD_CNT_CLAVE_ASI, NULL, 'MANUAL', 'AUTOMATICO');
  
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END PP_CARGAR_DATOS;
  PROCEDURE PP_CARGAR_DETALLE_IP(P_EMPRESA IN NUMBER,
                                 P_FECHA   IN DATE,
                                 P_SESSION IN NUMBER,
                                 P_USUARIO IN VARCHAR2) AS
  
    TYPE CURTYP IS REF CURSOR;
    C_CNTP103 CURTYP;
    V_SQL_1   VARCHAR2(3000);
    V_MES     NUMBER;
    V_ANHO    NUMBER;
    V_WHERE   VARCHAR2(3000);
    TYPE CUR_TYPE IS RECORD(
      SUC_DESC               VARCHAR2(25),
      DPTO_DESC              VARCHAR2(25),
      CAN_CODIGO             NUMBER,
      CANAL                  VARCHAR2(25),
      CUENTA_CLAVE           NUMBER,
      CUENTA_NRO             NUMBER,
      CUENTA_DESC            VARCHAR2(25),
      IMP_CANAL_IPS_OBRERO   NUMBER,
      IMP_CANAL_IPS_PATRONAL NUMBER,
      CCO_USA_CANAL          VARCHAR2(5),
      CCOSTO_COD             NUMBER);
  
    C CUR_TYPE;
  
  BEGIN
    BEGIN
      SELECT EXTRACT(MONTH FROM TO_DATE(P_FECHA)) INTO V_MES FROM DUAL;
    
      V_WHERE := V_WHERE || ' A.MES =' || V_MES || ' ';
    
    END;
  
    BEGIN
      SELECT EXTRACT(YEAR FROM TO_DATE(P_FECHA)) INTO V_ANHO FROM DUAL;
    
    END;
  
    V_SQL_1 := 'SELECT SUC_DESC,
       DPTO_DESC,
       A.CANB_CODIGO,
       A.CANAL_BETA,
       CUENTA_CLAVE,
       CUENTA_NRO,
       CUENTA_DESC,
       ROUND(SUM(A.IMP_CANAL_IPS_OBRERO)) IMP_CANAL_IPS_OBRERO,
       ROUND(SUM(A.IMP_CANAL_IPS_PATRONAL)) IMP_CANAL_IPS_PATRONAL,
       CCO_USA_CANAL,
       CCOSTO_COD
  FROM PER_PERP013_V A
 WHERE ' || V_WHERE || '
   AND A.ANHO = ' || V_ANHO || '
   AND PDOC_EMPR = ' || P_EMPRESA || '
 GROUP BY SUC_DESC,
          DPTO_DESC,
          A.CANB_CODIGO,
          A.CANAL_BETA,
          CUENTA_CLAVE,
          CUENTA_NRO,
          CUENTA_DESC,
          CCO_USA_CANAL,
          CCOSTO_COD
       ORDER BY 1, 2, 3';
  
    ---INSERT INTO X (CAMPO1, OTRO) VALUES (V_SQL_1, 'CNTP103');
    -- COMMIT;
  
    OPEN C_CNTP103 FOR V_SQL_1;
  
    BEGIN
      DELETE CNT103_TEMP
       WHERE SESION_ID = P_SESSION
         AND USUARIO = P_USUARIO;
      COMMIT;
    END;
  
    LOOP
    
      FETCH C_CNTP103
      
        INTO C;
      EXIT WHEN C_CNTP103%NOTFOUND;
    
      INSERT INTO CNT103_TEMP
        (SUC_DESC,
         DPTO_DESC,
         CANB_CODIGO,
         CANAL_BETA,
         CUENTA_CLAVE,
         CUENTA_NRO,
         CUENTA_DESC,
         IMP_CANAL_IPS_OBRERO,
         IMP_CANAL_IPS_PATRONAL,
         CCO_USA_CANAL,
         SESION_ID,
         USUARIO,
         EMPR)
      VALUES
        (C.SUC_DESC,
         C.DPTO_DESC,
         C.CAN_CODIGO,
         C.CANAL,
         C.CUENTA_CLAVE,
         C.CUENTA_NRO,
         C.CUENTA_DESC,
         C.IMP_CANAL_IPS_OBRERO,
         C.IMP_CANAL_IPS_PATRONAL,
         C.CCO_USA_CANAL,
         P_SESSION,
         P_USUARIO,
         P_EMPRESA);
    
    END LOOP;
  
  END PP_CARGAR_DETALLE_IP;

  PROCEDURE PP_VALIDAR_PERIODO(P_EMPRESA      IN NUMBER,
                               P_FECHA_INICIO IN DATE,
                               P_FECHA_FIN    IN DATE) AS
  
    V_CODIGO NUMBER;
  BEGIN
  
    SELECT PERI_CODIGO
      INTO V_CODIGO
      FROM FIN_PERIODO
     WHERE PERI_FEC_INI = P_FECHA_INICIO
       AND PERI_FEC_FIN = P_FECHA_FIN
       AND PERI_EMPR = P_EMPRESA
     ORDER BY 1;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20016,
                              'La peri?do escogido no pertenece a ninguno de los peri?dos anteriores o actual de Finanzas!.');
    
  END PP_VALIDAR_PERIODO;

  PROCEDURE PP_VALIDAR_ASI_EXISTENTE(P_EMPRESA        IN NUMBER,
                                     P_OPERACION      IN VARCHAR2,
                                     P_PERI_FECHA_INI IN DATE) AS
  
    V_VARIABLE     VARCHAR2(1);
    V_TIPO_ASIENTO VARCHAR2(2);
  
  BEGIN
  
    --SI EXISTEN ASIENTOS GENERADOS POR FINANZAS
    --ENTONCES SE GENERAN EN FIN_PERI?DO ESTAR?N
    --SI O SI-
    V_TIPO_ASIENTO := FP_VALIDA_EXIS_ASIENTO(P_EMPRESA, P_PERI_FECHA_INI); --FUNCION QUE VALIDA SI YA EXISTE EL ASIENTO
  
    IF P_OPERACION = 'INSERTAR' AND V_TIPO_ASIENTO IN ('M', 'A') THEN
      RAISE_APPLICATION_ERROR(-20016,
                              'Los asientos correspondientes a este periodo de finanzas ya han sido generados!.');
    END IF;
  
    IF P_OPERACION = 'BORRAR' AND V_TIPO_ASIENTO IN ('N') THEN
      RAISE_APPLICATION_ERROR(-20016,
                              'Los asientos no corresponden al periodo de finanzas todavia no se generaron!.');
    END IF;
  
  END PP_VALIDAR_ASI_EXISTENTE;

  PROCEDURE PP_VALIDAR_CANALES_IP(P_CCO_USA_CANAL IN VARCHAR2,
                                  P_CANAL         IN VARCHAR2) AS
  BEGIN
  
    IF P_CCO_USA_CANAL = 'S' AND P_CANAL LIKE '%SIN CANAL%' THEN
      RAISE_APPLICATION_ERROR(-20016, 'El centro de costo requiere canal!');
    END IF;
  
  END PP_VALIDAR_CANALES_IP;

  PROCEDURE PP_GENERAR_ASI_PRCLI(P_EMPRESA      IN NUMBER,
                                 P_FECHA_INICIO IN DATE,
                                 P_FECHA_FIN    IN DATE,
                                 P_MENSAJE      OUT NUMBER) AS
  
    CURSOR PROV_CLI_ENCA_CUR IS
      SELECT DECODE(DOC_CLI, NULL, 'P', 'C') PR_CL,
             DECODE(DCON_TIPO_SALDO, 'D', 'C', 'D') FCON_TIPO_SALDO
        FROM FIN_CONCEPTO,
             FIN_DOCUMENTO,
             FIN_DOC_CONCEPTO,
             FIN_CLIENTE,
             FIN_PROVEEDOR,
             FIN_CONFIGURACION,
             CNT_CUENTA        C, --PROVEEDORES
             CNT_CUENTA        E, --PROVEEDORES
             CNT_CUENTA        F --CLIENTES
       WHERE FCON_CLAVE = DCON_CLAVE_CONCEPTO
         AND FCON_EMPR = DCON_EMPR
            
         AND DOC_CLAVE = DCON_CLAVE_DOC
         AND DOC_EMPR = DCON_EMPR
            
         AND DOC_EMPR = CONF_EMPR
            
         AND DOC_EMPR = P_EMPRESA
         AND DOC_FEC_DOC BETWEEN P_FECHA_INICIO AND P_FECHA_FIN
         AND DOC_CTA_BCO IS NULL
         AND (DOC_CLI IS NOT NULL OR DOC_PROV IS NOT NULL OR
             DOC_CTACO IS NOT NULL)
            --     AND DOC_SIST <> 'ACO' --NO INCLUIR LOS ASIENTOS DE ACOPIO --SACADO A PEDIDO DE LEANDRO EL 20110714
         AND DOC_TIPO_MOV <> 38 --NO INCLUIR EL TIPO DE MOVIMIENTO 38 RECEPCION DE PRODUCTO CONDICIONAL
         AND DOC_TIPO_MOV <> 72
         AND DOC_SIST <> 'ACL' --NO INCLUIR LOS ASIENTOS DE ACOPIO DE LECHE
         AND DOC_SIST <> 'PMO' --NO INCLUIR LOS ASIENTOS DE PRESTAMOS
         AND DOC_CLI = CLI_CODIGO(+)
         AND DOC_EMPR = CLI_EMPR(+)
            
         AND DOC_PROV = PROV_CODIGO(+)
         AND DOC_EMPR = PROV_EMPR(+)
            
         AND E.CTAC_CLAVE(+) = CONF_CLAVE_CTACO_ACREEDORES --PROVEEDORES
         AND E.CTAC_EMPR(+) = CONF_EMPR --PROVEEDORES
            
         AND F.CTAC_CLAVE(+) = CONF_CLAVE_CTACO_DEUDORES --CLIENTES
         AND F.CTAC_EMPR(+) = CONF_EMPR --CLIENTES
            
         AND C.CTAC_CLAVE(+) = PROV_CTA_CONTABLE --PROVEEDORES
         AND C.CTAC_EMPR(+) = PROV_EMPR --PROVEEDORES
            
         AND DOC_TIPO_MOV NOT IN (81) --COMBUSTIBLES
      
       GROUP BY DECODE(DOC_CLI, NULL, 'P', 'C'),
                DECODE(DCON_TIPO_SALDO, 'D', 'C', 'D')
       ORDER BY 1, 2;
  
    -- CURSOR PARA TRAER LOS TOTALES POR CUENTA
    CURSOR PROV_CLI_DET_CUR(V_PRCL VARCHAR2, V_TIPO_SALDO VARCHAR2) IS
      SELECT DCON_CLAVE_CTACO,
             SUM(DCON_EXEN_LOC + DCON_GRAV_LOC + DCON_IVA_LOC) IMP_TOTAL,
             DECODE(DOC_CTACO,
                    NULL,
                    DECODE(DOC_PROV,
                           NULL,
                           F.CONF_CLAVE_CTACO_DEUDORES,
                           F.CONF_CLAVE_CTACO_ACREEDORES),
                    DOC_CTACO) CONTRA_CUENTA,
             G.CTAC_DESC CUENTA_A,
             H.CTAC_DESC CUETA_B,
             G.CTAC_DESC || ' a ' || H.CTAC_DESC DESCRIPCION
        FROM FIN_CONCEPTO,
             FIN_DOCUMENTO,
             FIN_DOC_CONCEPTO,
             FIN_CONFIGURACION F,
             CNT_CUENTA        G,
             CNT_CUENTA        H
       WHERE FCON_CLAVE = DCON_CLAVE_CONCEPTO
         AND FCON_EMPR = DCON_EMPR
            
         AND DOC_CLAVE = DCON_CLAVE_DOC
         AND DOC_EMPR = DCON_EMPR
            
         AND DOC_EMPR = CONF_EMPR
            
         AND DOC_EMPR = P_EMPRESA
            
         AND DOC_FEC_DOC BETWEEN P_FECHA_INICIO AND P_FECHA_FIN
         AND DOC_CTA_BCO IS NULL
         AND DECODE(DOC_CLI, NULL, 'P', 'C') = V_PRCL
         AND DECODE(DCON_TIPO_SALDO, 'D', 'C', 'D') = V_TIPO_SALDO
            --     AND DOC_SIST <> 'ACO' --NO INCLUIR LOS ASIENTOS DE ACOPIO  --SACADO A PEDIDO DE LEANDRO EL 20110714
         AND DOC_TIPO_MOV NOT IN (38, 78, 81) --NO INCLUIR EL TIPO DE MOVIMIENTO 38 RECEPCION DE PRODUCTO CONDICIONAL,81 COMBUSTIBLES
            
         AND DOC_SIST <> 'ACL' --NO INCLUIR LOS ASIENTOS DE ACOPIO DE LECHE
         AND DOC_SIST <> 'PMO' --NO INCLUIR LOS ASIENTOS DE PRESTAMOS
            --    AND DOC_NRO_DOC = 20090034684
            -- AND  CNT_VALIDA_TIPOMOV_CTA(DOC_TIPO_MOV,DCON_CLAVE_CTACO,:PARAMETER.P_EMPRESA)='S'
            
         AND DOC_EMPR = CONF_EMPR
         AND DCON_CLAVE_CTACO = G.CTAC_CLAVE
         AND DCON_EMPR = G.CTAC_EMPR
            
         AND DECODE(DOC_CTACO,
                    NULL,
                    DECODE(DOC_PROV,
                           NULL,
                           F.CONF_CLAVE_CTACO_DEUDORES,
                           F.CONF_CLAVE_CTACO_ACREEDORES),
                    DOC_CTACO) = H.CTAC_CLAVE
         AND DOC_EMPR = H.CTAC_EMPR
      
       GROUP BY DCON_CLAVE_CTACO,
                DECODE(DOC_CTACO,
                       NULL,
                       DECODE(DOC_PROV,
                              NULL,
                              F.CONF_CLAVE_CTACO_DEUDORES,
                              F.CONF_CLAVE_CTACO_ACREEDORES),
                       DOC_CTACO),
                G.CTAC_DESC,
                H.CTAC_DESC
       ORDER BY DCON_CLAVE_CTACO;
  
    TABLALLAVEADA EXCEPTION;
    PRAGMA EXCEPTION_INIT(TABLALLAVEADA, -54);
  
    -- VARIABLES AUXILIARES
    V_TOTAL       NUMBER;
    V_CLAVE_ASI   NUMBER;
    V_EJERCICIO   NUMBER;
    V_NRO_ASIENTO NUMBER;
    V_ASID_ITEM   NUMBER;
    V_CLAVE_TOTAL NUMBER;
    V_TIPO_SALDO  VARCHAR2(1);
    V_NRO_DOC     NUMBER;
    V_TIPO_MOV    NUMBER;
    ---------
    V_NRO_DOC_I   NUMBER;
    V_TIPO_MOV_I  NUMBER;
    V_DOC_CTA_CO  NUMBER;
    V_CTACO_ERROR NUMBER;
    V_DOC_CLAVE   NUMBER := 0;
    V_MESAJE      NUMBER;
  BEGIN
  
    -- SELECCIONAR EJERCICIO CORRESPONDIENTE AL PERIODO DE FINANZAS
  
    SELECT MIN(EJ_CODIGO)
      INTO V_EJERCICIO
      FROM CNT_EJERCICIO
    --WHERE TO_DATE(TO_CHAR(EJ_FEC_INICIAL,'YYYY'),'YYYY') = TO_DATE(TO_CHAR(:BASI.CONF_PER_ACT_INI,'YYYY'),'YYYY');
     WHERE P_FECHA_INICIO BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
       AND EJ_EMPR = P_EMPRESA;
  
    -- SE LLAVEA LA TABLA PARA QUE NADIE PUEDA INGRESAR ASIENTOS ENTRE MEDIO DE LOS
    -- ASIENTOS GENERADOS.
  
    LOCK TABLE CNT_ASIENTO IN EXCLUSIVE MODE NOWAIT;
  
    FOR PCENCA IN PROV_CLI_ENCA_CUR LOOP
    
      -- SELECCIONAR CLAVE PARA EL ASIENTO
    
      SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_CLAVE_ASI FROM DUAL;
    
      -- OBTENER EL MAXIMO NUMERO DE ASIENTO QUE ESTA GRABADO.
    
      SELECT MAX(ASI_NRO)
        INTO V_NRO_ASIENTO
        FROM CNT_ASIENTO
       WHERE ASI_EMPR = P_EMPRESA
         AND ASI_EJERCICIO = V_EJERCICIO;
    
      -- SE INSERTA LA CABECERA DE ASIENTO CON NUMERO DE ASIENTO IGUAL A MAX(ASI_NRO)
      -- MAS UNO.
    
      INSERT INTO CNT_ASIENTO
        (ASI_CLAVE,
         ASI_EMPR,
         ASI_EJERCICIO,
         ASI_NRO,
         ASI_FEC,
         ASI_OBS,
         ASI_FEC_GRAB,
         ASI_LOGIN,
         ASI_SIST,
         ASI_NRO_LISTADO_AUD)
      VALUES
        (V_CLAVE_ASI,
         P_EMPRESA,
         V_EJERCICIO,
         NVL(V_NRO_ASIENTO, 0) + 1,
         P_FECHA_FIN,
         'ASIENTO DE FINANZAS',
         SYSDATE,
         GEN_DEVUELVE_USER,
         'FIN',
         NULL);
      V_ASID_ITEM := 0;
      V_TOTAL     := 0;
    
      FOR PCDET IN PROV_CLI_DET_CUR(PCENCA.PR_CL, PCENCA.FCON_TIPO_SALDO) LOOP
        --   IF PCDET.DOC_TIPO_MOV!=83 AND PCDET.DCON_CLAVE_CTACO!=24 THEN  --QUE SOLO HAGA SI EL TIPO DE MOVIMIENTO ES DINTINTO A 83 Y DISTINTO A LA CUENTA CONTABLE 24
        V_MESAJE := TO_CHAR(PCDET.DCON_CLAVE_CTACO);
      
        V_ASID_ITEM := V_ASID_ITEM + 1;
        V_TOTAL     := V_TOTAL + PCDET.IMP_TOTAL;
      
        V_CTACO_ERROR := PCDET.DCON_CLAVE_CTACO;
      
        --///// DETALLES /////
        INSERT INTO CNT_ASIENTO_DET
          (ASID_CLAVE_ASI,
           ASID_ITEM,
           ASID_CLAVE_CTACO,
           ASID_IND_DB_CR,
           ASID_IMPORTE,
           ASID_TIPO_MOV,
           ASID_NRO_DOC,
           ASID_DESC,
           ASID_EMPR)
        VALUES
          (V_CLAVE_ASI,
           V_ASID_ITEM,
           PCDET.DCON_CLAVE_CTACO,
           PCENCA.FCON_TIPO_SALDO,
           PCDET.IMP_TOTAL,
           NULL,
           NULL,
           PCDET.DESCRIPCION,
           P_EMPRESA);
      
        --CONTRA ASIENTO
        V_ASID_ITEM := V_ASID_ITEM + 1;
      
        IF PCENCA.FCON_TIPO_SALDO = 'D' THEN
          V_TIPO_SALDO := 'C';
        ELSE
          V_TIPO_SALDO := 'D';
        END IF;
      
        INSERT INTO CNT_ASIENTO_DET
          (ASID_CLAVE_ASI,
           ASID_ITEM,
           ASID_CLAVE_CTACO,
           ASID_IND_DB_CR,
           ASID_IMPORTE,
           ASID_TIPO_MOV,
           ASID_NRO_DOC,
           ASID_DESC,
           ASID_EMPR)
        VALUES
          (V_CLAVE_ASI,
           V_ASID_ITEM,
           PCDET.CONTRA_CUENTA,
           V_TIPO_SALDO,
           PCDET.IMP_TOTAL,
           NULL,
           NULL,
           PCDET.DESCRIPCION,
           P_EMPRESA);
      
      END LOOP;
    END LOOP;
  
    P_MENSAJE := V_MESAJE;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20016,
                              'No se han encontrado datos al generar deudores/acreedores!.');
    WHEN TABLALLAVEADA THEN
      RAISE_APPLICATION_ERROR(-20016,
                              'Tabla de asientos llaveada por otro usuario');
    
  END PP_GENERAR_ASI_PRCLI;

  PROCEDURE PP_GENERAR_ASI_INT_PMO(P_EMPRESA      IN NUMBER,
                                   P_FECHA_INICIO IN DATE,
                                   P_FECHA_FIN    IN DATE,
                                   P_MENSAJE      OUT NUMBER) AS
  
    -- CURSOR PARA DETECTAR LOS TIPOS DE ASIENTOS.
    -- DEUDORES(DEBITO - CREDITO)
    -- ACREEDORES(DEBITO - CREDITO)
    CURSOR PROV_CLI_ENCA_CUR_PMO IS
      SELECT DECODE(DOC_CLI, NULL, 'P', 'C') PR_CL,
             DECODE(FCON_TIPO_SALDO, 'D', 'C', 'D') FCON_TIPO_SALDO
        FROM FIN_CONCEPTO,
             FIN_DOCUMENTO,
             FIN_DOC_CONCEPTO,
             FIN_CLIENTE,
             FIN_PROVEEDOR,
             FIN_CONFIGURACION,
             CNT_CUENTA        C, --PROVEEDORES
             CNT_CUENTA        E, --PROVEEDORES
             CNT_CUENTA        F --CLIENTES
       WHERE FCON_CLAVE = DCON_CLAVE_CONCEPTO
         AND FCON_EMPR = DCON_EMPR
            
         AND DOC_CLAVE = DCON_CLAVE_DOC
         AND DOC_EMPR = DCON_EMPR
            
         AND DOC_EMPR = CONF_EMPR
         AND DOC_EMPR = P_EMPRESA
            
         AND DOC_FEC_DOC BETWEEN P_FECHA_INICIO AND P_FECHA_FIN
         AND DOC_CTA_BCO IS NULL
         AND (DOC_CLI IS NOT NULL OR DOC_PROV IS NOT NULL OR
             DOC_CTACO IS NOT NULL)
            --     AND DOC_SIST <> 'ACO' --NO INCLUIR LOS ASIENTOS DE ACOPIO --SACADO A PEDIDO DE LEANDRO EL 20110714
         AND DOC_TIPO_MOV <> 38 --NO INCLUIR EL TIPO DE MOVIMIENTO 38 RECEPCION DE PRODUCTO CONDICIONAL
         AND DOC_TIPO_MOV = 72
         AND DOC_SIST <> 'ACL' --NO INCLUIR LOS ASIENTOS DE ACOPIO DE LECHE
         AND DOC_SIST <> 'PMO' --NO INCLUIR LOS ASIENTOS DE PRESTAMOS
         AND DOC_CLI = CLI_CODIGO(+)
         AND DOC_EMPR = CLI_EMPR(+)
            
         AND DOC_PROV = PROV_CODIGO(+)
         AND DOC_EMPR = PROV_EMPR(+)
            
         AND E.CTAC_CLAVE(+) = CONF_CLAVE_CTACO_ACREEDORES --PROVEEDORES
         AND E.CTAC_EMPR(+) = CONF_EMPR --PROVEEDORES
            
         AND F.CTAC_CLAVE(+) = CONF_CLAVE_CTACO_DEUDORES --CLIENTES
         AND F.CTAC_EMPR(+) = CONF_EMPR --CLIENTES
            
         AND C.CTAC_CLAVE(+) = PROV_CTA_CONTABLE --PROVEEDORES
         AND C.CTAC_EMPR(+) = PROV_EMPR
      
       GROUP BY DECODE(DOC_CLI, NULL, 'P', 'C'),
                DECODE(FCON_TIPO_SALDO, 'D', 'C', 'D');
  
    -- CURSOR PARA TRAER LOS TOTALES POR CUENTA
    CURSOR PROV_CLI_DET_CUR(V_PRCL VARCHAR2, V_TIPO_SALDO VARCHAR2) IS
      SELECT DOC_CLAVE,
             DCON_CLAVE_CTACO,
             SUM(DCON_EXEN_LOC + DCON_GRAV_LOC + DCON_IVA_LOC) IMP_TOTAL,
             DOC_NRO_DOC,
             DOC_TIPO_MOV,
             DOC_CTACO
        FROM FIN_CONCEPTO, FIN_DOCUMENTO, FIN_DOC_CONCEPTO
       WHERE FCON_CLAVE = DCON_CLAVE_CONCEPTO
         AND FCON_EMPR = DCON_EMPR
            
         AND DOC_CLAVE = DCON_CLAVE_DOC
         AND DOC_EMPR = DCON_EMPR
            
         AND DOC_EMPR = P_EMPRESA
            
         AND DOC_FEC_DOC BETWEEN P_FECHA_INICIO AND P_FECHA_FIN
         AND DOC_CTA_BCO IS NULL
         AND DECODE(DOC_CLI, NULL, 'P', 'C') = V_PRCL
         AND DECODE(FCON_TIPO_SALDO, 'D', 'C', 'D') = V_TIPO_SALDO
            --     AND DOC_SIST <> 'ACO' --NO INCLUIR LOS ASIENTOS DE ACOPIO  --SACADO A PEDIDO DE LEANDRO EL 20110714
         AND DOC_TIPO_MOV <> 38 --NO INCLUIR EL TIPO DE MOVIMIENTO 38 RECEPCION DE PRODUCTO CONDICIONAL
         AND DOC_TIPO_MOV = 72
         AND DOC_SIST <> 'ACL' --NO INCLUIR LOS ASIENTOS DE ACOPIO DE LECHE
         AND DOC_SIST <> 'PMO' --NO INCLUIR LOS ASIENTOS DE PRESTAMOS
       GROUP BY DOC_CLAVE,
                DCON_CLAVE_CTACO,
                DOC_NRO_DOC,
                DOC_TIPO_MOV,
                DOC_CTACO;
  
    CURSOR PROV_CLI_DOC(V_PRCL VARCHAR2, V_TIPO_SALDO VARCHAR2) IS
      SELECT DOC_CLAVE,
             DOC_TIPO_MOV,
             DOC_NRO_DOC,
             DCON_CLAVE_CTACO,
             SUM(DCON_EXEN_LOC + DCON_GRAV_LOC + DCON_IVA_LOC) IMP_TOTAL
        FROM FIN_CONCEPTO, FIN_DOCUMENTO, FIN_DOC_CONCEPTO
       WHERE FCON_CLAVE = DCON_CLAVE_CONCEPTO
         AND FCON_EMPR = DCON_EMPR
            
         AND DOC_CLAVE = DCON_CLAVE_DOC
         AND DOC_EMPR = DCON_EMPR
            
         AND DOC_EMPR = P_EMPRESA
         AND DOC_FEC_DOC BETWEEN P_FECHA_INICIO AND P_FECHA_FIN
         AND DOC_CTA_BCO IS NULL
         AND DECODE(DOC_CLI, NULL, 'P', 'C') = V_PRCL
         AND DECODE(FCON_TIPO_SALDO, 'D', 'C', 'D') = V_TIPO_SALDO
            --     AND DOC_SIST <> 'ACO' --NO INCLUIR LOS ASIENTOS DE ACOPIO  --SACADO A PEDIDO DE LEANDRO EL 20110714
         AND DOC_TIPO_MOV <> 38 --NO INCLUIR EL TIPO DE MOVIMIENTO 38 RECEPCION DE PRODUCTO CONDICIONAL
         AND DOC_TIPO_MOV = 72
         AND DOC_SIST <> 'ACL' --NO INCLUIR LOS ASIENTOS DE ACOPIO DE LECHE
         AND DOC_SIST <> 'PMO' --NO INCLUIR LOS ASIENTOS DE PRESTAMOS
       GROUP BY DOC_CLAVE, DOC_TIPO_MOV, DOC_NRO_DOC, DCON_CLAVE_CTACO;
  
    TABLALLAVEADA EXCEPTION;
    PRAGMA EXCEPTION_INIT(TABLALLAVEADA, -54);
  
    -- VARIABLES AUXILIARES
    V_TOTAL       NUMBER;
    V_CLAVE_ASI   NUMBER;
    V_EJERCICIO   NUMBER;
    V_NRO_ASIENTO NUMBER;
    V_ASID_ITEM   NUMBER;
    V_CLAVE_TOTAL NUMBER;
    V_TIPO_SALDO  VARCHAR2(1);
    V_NRO_DOC     NUMBER;
    V_TIPO_MOV    NUMBER;
    ---------
    V_NRO_DOC_I        NUMBER;
    V_TIPO_MOV_I       NUMBER;
    V_DOC_CTA_CO       NUMBER;
    V_CTACO_ACREEDORES NUMBER;
    V_MENSAJE          NUMBER;
  BEGIN
  
    -- SELECCIONAR EJERCICIO CORRESPONDIENTE AL PERIODO DE FINANZAS
  
    SELECT MIN(EJ_CODIGO)
      INTO V_EJERCICIO
      FROM CNT_EJERCICIO
    --WHERE TO_DATE(TO_CHAR(EJ_FEC_INICIAL,'YYYY'),'YYYY') = TO_DATE(TO_CHAR(:BASI.CONF_PER_ACT_INI,'YYYY'),'YYYY');
     WHERE P_FECHA_INICIO BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
       AND EJ_EMPR = P_EMPRESA;
  
    -- SELECCCIONAR  CLAVE CTA. CONTABLE ACREDEORES ACTUAL ---
  
    SELECT CONF_CLAVE_CTACO_ACREEDORES
      INTO V_CTACO_ACREEDORES
      FROM FIN_CONFIGURACION
     WHERE CONF_EMPR = P_EMPRESA;
  
    -- SE LLAVEA LA TABLA PARA QUE NADIE PUEDA INGRESAR ASIENTOS ENTRE MEDIO DE LOS
    -- ASIENTOS GENERADOS.
  
    LOCK TABLE CNT_ASIENTO IN EXCLUSIVE MODE NOWAIT;
  
    FOR PCENCA IN PROV_CLI_ENCA_CUR_PMO LOOP
    
      -- SELECCIONAR CLAVE PARA EL ASIENTO
    
      SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_CLAVE_ASI FROM DUAL;
    
      -- OBTENER EL MAXIMO NUMERO DE ASIENTO QUE ESTA GRABADO.
    
      SELECT MAX(ASI_NRO)
        INTO V_NRO_ASIENTO
        FROM CNT_ASIENTO
       WHERE ASI_EMPR = P_EMPRESA
         AND ASI_EJERCICIO = V_EJERCICIO;
    
      -- SE INSERTA LA CABECERA DE ASIENTO CON NUMERO DE ASIENTO IGUAL A MAX(ASI_NRO)
      -- MAS UNO.
    
      INSERT INTO CNT_ASIENTO
        (ASI_CLAVE,
         ASI_EMPR,
         ASI_EJERCICIO,
         ASI_NRO,
         ASI_FEC,
         ASI_OBS,
         ASI_FEC_GRAB,
         ASI_LOGIN,
         ASI_SIST,
         ASI_NRO_LISTADO_AUD)
      VALUES
        (V_CLAVE_ASI,
         P_EMPRESA,
         V_EJERCICIO,
         NVL(V_NRO_ASIENTO, 0) + 1,
         P_FECHA_FIN,
         'ASIENTO DE FINANZAS',
         SYSDATE,
         GEN_DEVUELVE_USER,
         'FIN',
         NULL);
      V_ASID_ITEM := 0;
      V_TOTAL     := 0;
    
      FOR PCDET IN PROV_CLI_DET_CUR(PCENCA.PR_CL, PCENCA.FCON_TIPO_SALDO) LOOP
        V_MENSAJE := TO_CHAR(PCDET.DCON_CLAVE_CTACO);
      
        V_ASID_ITEM  := V_ASID_ITEM + 1;
        V_TOTAL      := V_TOTAL + PCDET.IMP_TOTAL;
        V_NRO_DOC    := PCDET.DOC_NRO_DOC;
        V_TIPO_MOV   := PCDET.DOC_TIPO_MOV;
        V_DOC_CTA_CO := PCDET.DOC_CTACO;
      
        INSERT INTO CNT_ASIENTO_DET
          (ASID_CLAVE_ASI,
           ASID_ITEM,
           ASID_CLAVE_CTACO,
           ASID_IND_DB_CR,
           ASID_IMPORTE,
           ASID_TIPO_MOV,
           ASID_NRO_DOC,
           ASID_DESC,
           ASID_EMPR)
        VALUES
          (V_CLAVE_ASI,
           V_ASID_ITEM,
           PCDET.DCON_CLAVE_CTACO,
           PCENCA.FCON_TIPO_SALDO,
           PCDET.IMP_TOTAL,
           PCDET.DOC_TIPO_MOV,
           PCDET.DOC_NRO_DOC,
           NULL,
           P_EMPRESA);
      
      END LOOP;
    
      FOR RDOC IN PROV_CLI_DOC(PCENCA.PR_CL, PCENCA.FCON_TIPO_SALDO) LOOP
        V_NRO_DOC_I  := RDOC.DOC_NRO_DOC;
        V_TIPO_MOV_I := RDOC.DOC_TIPO_MOV;
      
        INSERT INTO CNT_ASIENTO_DOC_DET
          (ASIDD_CLAVE_ASI,
           ASIDD_CLAVE_DOC,
           ASIDD_ITEM,
           ASIDD_CLAVE_CTACO,
           ASIDD_IND_DB_CR,
           ASIDD_IMPORTE,
           ASIDD_TIPO_MOV,
           ASIDD_NRO_DOC,
           ASIDD_DESC,
           ASIDD_EMPR)
        VALUES
          (V_CLAVE_ASI,
           RDOC.DOC_CLAVE,
           V_ASID_ITEM,
           RDOC.DCON_CLAVE_CTACO,
           PCENCA.FCON_TIPO_SALDO,
           RDOC.IMP_TOTAL,
           RDOC.DOC_TIPO_MOV,
           RDOC.DOC_NRO_DOC,
           NULL,
           P_EMPRESA);
      END LOOP;
    
      -- AL SALIR DEL DETALLE INSERTAR UNO MAS PARA BALANCEAR EL ASIENTO CON LA CUENTA
      -- DE DEUDORES O ACREEDORES
    
      V_ASID_ITEM := V_ASID_ITEM + 1;
      --IND
    
      IF PCENCA.PR_CL = 'C' THEN
      
        V_CLAVE_TOTAL := V_DOC_CTA_CO; --2317 CLAVE CTACO DE INTERESES A COBRAR CARTERA DE CLIENTES.
      
      ELSE
        V_CLAVE_TOTAL := V_CTACO_ACREEDORES;
      END IF;
    
      /*IF PCENCA.PR_CL = 'C' THEN
        V_CLAVE_TOTAL := :BASI.CONF_CLAVE_CTACO_DEUDORES;
      ELSE
        V_CLAVE_TOTAL := :BASI.CONF_CLAVE_CTACO_ACREEDORES;
      END IF;*/
      IF PCENCA.FCON_TIPO_SALDO = 'D' THEN
        V_TIPO_SALDO := 'C';
      ELSE
        V_TIPO_SALDO := 'D';
      END IF;
    
      INSERT INTO CNT_ASIENTO_DET
        (ASID_CLAVE_ASI,
         ASID_ITEM,
         ASID_CLAVE_CTACO,
         ASID_IND_DB_CR,
         ASID_IMPORTE,
         ASID_TIPO_MOV,
         ASID_NRO_DOC,
         ASID_DESC,
         ASID_EMPR)
      VALUES
        (V_CLAVE_ASI,
         V_ASID_ITEM,
         V_CLAVE_TOTAL,
         V_TIPO_SALDO,
         V_TOTAL,
         V_TIPO_MOV,
         V_NRO_DOC,
         NULL,
         P_EMPRESA);
    
      INSERT INTO CNT_ASIENTO_DOC_DET
        (ASIDD_CLAVE_ASI,
         ASIDD_CLAVE_DOC,
         ASIDD_ITEM,
         ASIDD_CLAVE_CTACO,
         ASIDD_IND_DB_CR,
         ASIDD_IMPORTE,
         ASIDD_TIPO_MOV,
         ASIDD_NRO_DOC,
         ASIDD_DESC,
         ASIDD_EMPR)
      VALUES
        (V_CLAVE_ASI,
         NULL,
         V_ASID_ITEM,
         V_CLAVE_TOTAL,
         V_TIPO_SALDO,
         V_TOTAL,
         V_TIPO_MOV_I,
         V_NRO_DOC_I,
         NULL,
         P_EMPRESA);
    END LOOP;
  
    P_MENSAJE := V_MENSAJE;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20016,
                              'No se han encontrado datos al generar deudores/acreedores!.');
    WHEN TABLALLAVEADA THEN
      RAISE_APPLICATION_ERROR(-20016,
                              'Tabla de asientos llaveada por otro usuario');
  END;

  PROCEDURE PP_GENERAR_ASI_DEV_IVA(P_EMPRESA      IN NUMBER,
                                   P_FECHA_INICIO IN DATE,
                                   P_FECHA_FIN    IN DATE,
                                   P_MENSAJE      OUT NUMBER) AS
  
    -- CURSOR PARA DETECTAR LOS TIPOS DE ASIENTOS.
    -- DEUDORES(DEBITO - CREDITO)
    -- ACREEDORES(DEBITO - CREDITO)
    CURSOR PROV_CLI_ENCA_CUR IS
      SELECT DECODE(DOC_CLI, NULL, 'P', 'C') PR_CL,
             DECODE(FCON_TIPO_SALDO, 'C', 'D', 'C') FCON_TIPO_SALDO
        FROM FIN_CONCEPTO,
             FIN_DOCUMENTO,
             FIN_DOC_CONCEPTO,
             FIN_CLIENTE,
             FIN_PROVEEDOR,
             FIN_CONFIGURACION,
             CNT_CUENTA        C, --PROVEEDORES
             CNT_CUENTA        E, --PROVEEDORES
             CNT_CUENTA        F --CLIENTES
       WHERE FCON_CLAVE = DCON_CLAVE_CONCEPTO
         AND FCON_EMPR = DCON_EMPR
            
         AND DOC_CLAVE = DCON_CLAVE_DOC
         AND DOC_EMPR = DCON_EMPR
            
         AND DOC_EMPR = P_EMPRESA
         AND DOC_FEC_DOC BETWEEN P_FECHA_INICIO AND P_FECHA_FIN
         AND DOC_CTA_BCO IS NULL
         AND (DOC_CLI IS NOT NULL OR DOC_PROV IS NOT NULL OR
             DOC_CTACO IS NOT NULL)
            --     AND DOC_SIST <> 'ACO' --NO INCLUIR LOS ASIENTOS DE ACOPIO --SACADO A PEDIDO DE LEANDRO EL 20110714
         AND DOC_TIPO_MOV <> 38 --NO INCLUIR EL TIPO DE MOVIMIENTO 38 RECEPCION DE PRODUCTO CONDICIONAL
         AND DOC_TIPO_MOV = 75
         AND DOC_SIST <> 'ACL' --NO INCLUIR LOS ASIENTOS DE ACOPIO DE LECHE
         AND DOC_SIST <> 'PMO' --NO INCLUIR LOS ASIENTOS DE PRESTAMOS
            
         AND DOC_CLI = CLI_CODIGO(+)
         AND DOC_EMPR = CLI_EMPR(+)
            
         AND DOC_PROV = PROV_CODIGO(+)
         AND DOC_EMPR = PROV_EMPR(+)
            
         AND E.CTAC_CLAVE(+) = CONF_CLAVE_CTACO_ACREEDORES --PROVEEDORES
         AND E.CTAC_EMPR(+) = CONF_EMPR --PROVEEDORES
            
         AND DOC_EMPR = CONF_EMPR
         AND DOC_EMPR = P_EMPRESA
            
         AND F.CTAC_CLAVE(+) = CONF_CLAVE_CTACO_DEUDORES --CLIENTES
         AND F.CTAC_EMPR(+) = CONF_EMPR --CLIENTES
            
         AND C.CTAC_CLAVE(+) = PROV_CTA_CONTABLE --PROVEEDORES
         AND C.CTAC_EMPR(+) = PROV_EMPR --PROVEEDORES
      
       GROUP BY DECODE(DOC_CLI, NULL, 'P', 'C'),
                DECODE(FCON_TIPO_SALDO, 'C', 'D', 'C');
  
    -- CURSOR PARA TRAER LOS TOTALES POR CUENTA
    CURSOR PROV_CLI_DET_CUR(V_PRCL VARCHAR2, V_TIPO_SALDO VARCHAR2) IS
      SELECT DOC_CLAVE,
             DCON_CLAVE_CTACO,
             SUM(DCON_EXEN_LOC + DCON_GRAV_LOC + DCON_IVA_LOC) IMP_TOTAL,
             DOC_NRO_DOC,
             DOC_TIPO_MOV,
             DOC_CTACO
        FROM FIN_CONCEPTO, FIN_DOCUMENTO, FIN_DOC_CONCEPTO
       WHERE FCON_CLAVE = DCON_CLAVE_CONCEPTO
         AND FCON_EMPR = DCON_EMPR
            
         AND DOC_CLAVE = DCON_CLAVE_DOC
         AND DOC_EMPR = DCON_EMPR
            
         AND DOC_EMPR = P_EMPRESA
         AND DOC_FEC_DOC BETWEEN P_FECHA_INICIO AND P_FECHA_FIN
         AND DOC_CTA_BCO IS NULL
         AND DECODE(DOC_CLI, NULL, 'P', 'C') = V_PRCL
         AND DECODE(FCON_TIPO_SALDO, 'C', 'D', 'C') = V_TIPO_SALDO
            --     AND DOC_SIST <> 'ACO' --NO INCLUIR LOS ASIENTOS DE ACOPIO  --SACADO A PEDIDO DE LEANDRO EL 20110714
         AND DOC_TIPO_MOV <> 38 --NO INCLUIR EL TIPO DE MOVIMIENTO 38 RECEPCION DE PRODUCTO CONDICIONAL
         AND DOC_TIPO_MOV = 75
         AND DOC_SIST <> 'ACL' --NO INCLUIR LOS ASIENTOS DE ACOPIO DE LECHE
         AND DOC_SIST <> 'PMO' --NO INCLUIR LOS ASIENTOS DE PRESTAMOS
       GROUP BY DOC_CLAVE,
                DCON_CLAVE_CTACO,
                DOC_NRO_DOC,
                DOC_TIPO_MOV,
                DOC_CTACO;
  
    CURSOR PROV_CLI_DOC(V_PRCL VARCHAR2, V_TIPO_SALDO VARCHAR2) IS
      SELECT DOC_CLAVE,
             DOC_TIPO_MOV,
             DOC_NRO_DOC,
             DCON_CLAVE_CTACO,
             SUM(DCON_EXEN_LOC + DCON_GRAV_LOC + DCON_IVA_LOC) IMP_TOTAL
        FROM FIN_CONCEPTO, FIN_DOCUMENTO, FIN_DOC_CONCEPTO
       WHERE FCON_CLAVE = DCON_CLAVE_CONCEPTO
         AND FCON_EMPR = DCON_EMPR
            
         AND DOC_CLAVE = DCON_CLAVE_DOC
         AND DOC_EMPR = DCON_EMPR
            
         AND DOC_EMPR = P_EMPRESA
         AND DOC_FEC_DOC BETWEEN P_FECHA_INICIO AND P_FECHA_FIN
         AND DOC_CTA_BCO IS NULL
         AND DECODE(DOC_CLI, NULL, 'P', 'C') = V_PRCL
         AND DECODE(FCON_TIPO_SALDO, 'C', 'D', 'C') = V_TIPO_SALDO
            --     AND DOC_SIST <> 'ACO' --NO INCLUIR LOS ASIENTOS DE ACOPIO  --SACADO A PEDIDO DE LEANDRO EL 20110714
         AND DOC_TIPO_MOV <> 38 --NO INCLUIR EL TIPO DE MOVIMIENTO 38 RECEPCION DE PRODUCTO CONDICIONAL
         AND DOC_TIPO_MOV = 75
         AND DOC_SIST <> 'ACL' --NO INCLUIR LOS ASIENTOS DE ACOPIO DE LECHE
         AND DOC_SIST <> 'PMO' --NO INCLUIR LOS ASIENTOS DE PRESTAMOS
       GROUP BY DOC_CLAVE, DOC_TIPO_MOV, DOC_NRO_DOC, DCON_CLAVE_CTACO;
  
    TABLALLAVEADA EXCEPTION;
    PRAGMA EXCEPTION_INIT(TABLALLAVEADA, -54);
  
    -- VARIABLES AUXILIARES
    V_TOTAL       NUMBER;
    V_CLAVE_ASI   NUMBER;
    V_EJERCICIO   NUMBER;
    V_NRO_ASIENTO NUMBER;
    V_ASID_ITEM   NUMBER;
    V_CLAVE_TOTAL NUMBER;
    V_TIPO_SALDO  VARCHAR2(1);
    V_NRO_DOC     NUMBER;
    V_TIPO_MOV    NUMBER;
    ---------
    V_NRO_DOC_I        NUMBER;
    V_TIPO_MOV_I       NUMBER;
    V_DOC_CTA_CO       NUMBER;
    V_MENSAJE          NUMBER;
    V_CTACO_ACREEDORES NUMBER;
  BEGIN
  
    -- SELECCIONAR EJERCICIO CORRESPONDIENTE AL PERIODO DE FINANZAS
  
    SELECT MIN(EJ_CODIGO)
      INTO V_EJERCICIO
      FROM CNT_EJERCICIO
    --WHERE TO_DATE(TO_CHAR(EJ_FEC_INICIAL,'YYYY'),'YYYY') = TO_DATE(TO_CHAR(:BASI.CONF_PER_ACT_INI,'YYYY'),'YYYY');
     WHERE P_FECHA_INICIO BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
       AND EJ_EMPR = P_EMPRESA;
    SELECT CONF_CLAVE_CTACO_ACREEDORES
      INTO V_CTACO_ACREEDORES
      FROM FIN_CONFIGURACION
     WHERE CONF_EMPR = P_EMPRESA;
  
    -- SE LLAVEA LA TABLA PARA QUE NADIE PUEDA INGRESAR ASIENTOS ENTRE MEDIO DE LOS
    -- ASIENTOS GENERADOS.
  
    LOCK TABLE CNT_ASIENTO IN EXCLUSIVE MODE NOWAIT;
  
    FOR PCENCA IN PROV_CLI_ENCA_CUR LOOP
    
      -- SELECCIONAR CLAVE PARA EL ASIENTO
    
      SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_CLAVE_ASI FROM DUAL;
    
      -- OBTENER EL MAXIMO NUMERO DE ASIENTO QUE ESTA GRABADO.
    
      SELECT MAX(ASI_NRO)
        INTO V_NRO_ASIENTO
        FROM CNT_ASIENTO
       WHERE ASI_EMPR = P_EMPRESA
         AND ASI_EJERCICIO = V_EJERCICIO;
    
      -- SE INSERTA LA CABECERA DE ASIENTO CON NUMERO DE ASIENTO IGUAL A MAX(ASI_NRO)
      -- MAS UNO.
    
      INSERT INTO CNT_ASIENTO
        (ASI_CLAVE,
         ASI_EMPR,
         ASI_EJERCICIO,
         ASI_NRO,
         ASI_FEC,
         ASI_OBS,
         ASI_FEC_GRAB,
         ASI_LOGIN,
         ASI_SIST,
         ASI_NRO_LISTADO_AUD)
      VALUES
        (V_CLAVE_ASI,
         P_EMPRESA,
         V_EJERCICIO,
         NVL(V_NRO_ASIENTO, 0) + 1,
         P_FECHA_FIN,
         'ASIENTO DE FINANZAS',
         SYSDATE,
         GEN_DEVUELVE_USER,
         'FIN',
         NULL);
      V_ASID_ITEM := 0;
      V_TOTAL     := 0;
    
      FOR PCDET IN PROV_CLI_DET_CUR(PCENCA.PR_CL, PCENCA.FCON_TIPO_SALDO) LOOP
        V_MENSAJE := TO_CHAR(PCDET.DCON_CLAVE_CTACO);
      
        V_ASID_ITEM  := V_ASID_ITEM + 1;
        V_TOTAL      := V_TOTAL + PCDET.IMP_TOTAL;
        V_NRO_DOC    := PCDET.DOC_NRO_DOC;
        V_TIPO_MOV   := PCDET.DOC_TIPO_MOV;
        V_DOC_CTA_CO := PCDET.DOC_CTACO;
      
        INSERT INTO CNT_ASIENTO_DET
          (ASID_CLAVE_ASI,
           ASID_ITEM,
           ASID_CLAVE_CTACO,
           ASID_IND_DB_CR,
           ASID_IMPORTE,
           ASID_TIPO_MOV,
           ASID_NRO_DOC,
           ASID_DESC,
           ASID_EMPR)
        VALUES
          (V_CLAVE_ASI,
           V_ASID_ITEM,
           PCDET.DCON_CLAVE_CTACO,
           PCENCA.FCON_TIPO_SALDO,
           PCDET.IMP_TOTAL,
           PCDET.DOC_TIPO_MOV,
           PCDET.DOC_NRO_DOC,
           NULL,
           P_EMPRESA);
      
      END LOOP;
    
      FOR RDOC IN PROV_CLI_DOC(PCENCA.PR_CL, PCENCA.FCON_TIPO_SALDO) LOOP
        V_NRO_DOC_I  := RDOC.DOC_NRO_DOC;
        V_TIPO_MOV_I := RDOC.DOC_TIPO_MOV;
      
        INSERT INTO CNT_ASIENTO_DOC_DET
          (ASIDD_CLAVE_ASI,
           ASIDD_CLAVE_DOC,
           ASIDD_ITEM,
           ASIDD_CLAVE_CTACO,
           ASIDD_IND_DB_CR,
           ASIDD_IMPORTE,
           ASIDD_TIPO_MOV,
           ASIDD_NRO_DOC,
           ASIDD_DESC,
           ASIDD_EMPR)
        VALUES
          (V_CLAVE_ASI,
           RDOC.DOC_CLAVE,
           V_ASID_ITEM,
           RDOC.DCON_CLAVE_CTACO,
           PCENCA.FCON_TIPO_SALDO,
           RDOC.IMP_TOTAL,
           RDOC.DOC_TIPO_MOV,
           RDOC.DOC_NRO_DOC,
           NULL,
           P_EMPRESA);
      END LOOP;
    
      -- AL SALIR DEL DETALLE INSERTAR UNO MAS PARA BALANCEAR EL ASIENTO CON LA CUENTA
      -- DE DEUDORES O ACREEDORES
    
      V_ASID_ITEM := V_ASID_ITEM + 1;
      --IND
    
      IF PCENCA.PR_CL = 'C' THEN
      
        V_CLAVE_TOTAL := V_DOC_CTA_CO; --2317 CLAVE CTACO DE INTERESES A COBRAR CARTERA DE CLIENTES.
      
      ELSE
        V_CLAVE_TOTAL := V_CTACO_ACREEDORES;
      END IF;
    
      /*IF PCENCA.PR_CL = 'C' THEN
        V_CLAVE_TOTAL := :BASI.CONF_CLAVE_CTACO_DEUDORES;
      ELSE
        V_CLAVE_TOTAL := :BASI.CONF_CLAVE_CTACO_ACREEDORES;
      END IF;*/
    
      IF PCENCA.FCON_TIPO_SALDO = 'D' THEN
        V_TIPO_SALDO := 'C';
      ELSE
        V_TIPO_SALDO := 'D';
      END IF;
    
      INSERT INTO CNT_ASIENTO_DET
        (ASID_CLAVE_ASI,
         ASID_ITEM,
         ASID_CLAVE_CTACO,
         ASID_IND_DB_CR,
         ASID_IMPORTE,
         ASID_TIPO_MOV,
         ASID_NRO_DOC,
         ASID_DESC,
         ASID_EMPR)
      VALUES
        (V_CLAVE_ASI,
         V_ASID_ITEM,
         V_CLAVE_TOTAL,
         V_TIPO_SALDO,
         V_TOTAL,
         V_TIPO_MOV,
         V_NRO_DOC,
         NULL,
         P_EMPRESA);
    
      INSERT INTO CNT_ASIENTO_DOC_DET
        (ASIDD_CLAVE_ASI,
         ASIDD_CLAVE_DOC,
         ASIDD_ITEM,
         ASIDD_CLAVE_CTACO,
         ASIDD_IND_DB_CR,
         ASIDD_IMPORTE,
         ASIDD_TIPO_MOV,
         ASIDD_NRO_DOC,
         ASIDD_DESC,
         ASIDD_EMPR)
      VALUES
        (V_CLAVE_ASI,
         NULL,
         V_ASID_ITEM,
         V_CLAVE_TOTAL,
         V_TIPO_SALDO,
         V_TOTAL,
         V_TIPO_MOV_I,
         V_NRO_DOC_I,
         NULL,
         P_EMPRESA);
    END LOOP;
  
    P_MENSAJE := V_MENSAJE;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20016,
                              'No se han encontrado datos al generar deudores/acreedores!.');
    WHEN TABLALLAVEADA THEN
      RAISE_APPLICATION_ERROR(-20016,
                              'Tabla de asientos llaveada por otro usuario');
  END;

  PROCEDURE PP_GENERAR_ASI_BCO(P_EMPRESA      IN NUMBER,
                               P_FECHA_INICIO IN DATE,
                               P_FECHA_FIN    IN DATE,
                               P_MENSAJE      OUT NUMBER) AS
    CURSOR BCO_ENCA_CUR IS
      SELECT DOC_CTA_BCO, /*CTA_CLAVE_CTACO,*/
             DECODE(DOC_CTACO, NULL, CTA_CLAVE_CTACO, DOC_CTACO) CTA_CLAVE_CTACO,
             DECODE(DCON_TIPO_SALDO, 'D', 'C', 'D') TIPO_SALDO
        FROM FIN_CONCEPTO,
             FIN_CUENTA_BANCARIA,
             FIN_DOCUMENTO,
             FIN_DOC_CONCEPTO
       WHERE FCON_CLAVE = DCON_CLAVE_CONCEPTO
         AND FCON_EMPR = DCON_EMPR
            
         AND CTA_CODIGO = DOC_CTA_BCO
         AND CTA_EMPR = DOC_EMPR
            
         AND DOC_CLAVE = DCON_CLAVE_DOC
         AND DOC_EMPR = DCON_EMPR
            
         AND DCON_CLAVE_CTACO IS NOT NULL
         AND DOC_EMPR = P_EMPRESA
         AND DOC_FEC_DOC BETWEEN P_FECHA_INICIO AND P_FECHA_FIN
       GROUP BY DOC_CTA_BCO,
                DECODE(DOC_CTACO, NULL, CTA_CLAVE_CTACO, DOC_CTACO),
                DECODE(DCON_TIPO_SALDO, 'D', 'C', 'D')
      
       ORDER BY 1, 2, 3;
  
    CURSOR BCO_DET_CUR(V_CTA_BCO    NUMBER,
                       V_CTA_CTACO  NUMBER,
                       V_TIPO_SALDO VARCHAR2) IS
      SELECT DOC_CTA_BCO,
             CTA_CLAVE_CTACO,
             DCON_CLAVE_CTACO,
             DECODE(DCON_TIPO_SALDO, 'D', 'C', 'D') TIPO_SALDO,
             SUM(DCON_EXEN_LOC + DCON_GRAV_LOC + DCON_IVA_LOC) IMP_TOTAL
        FROM FIN_CONCEPTO,
             FIN_CUENTA_BANCARIA,
             FIN_DOCUMENTO,
             FIN_DOC_CONCEPTO
       WHERE FCON_CLAVE = DCON_CLAVE_CONCEPTO
         AND FCON_EMPR = DCON_EMPR
            
         AND CTA_CODIGO = DOC_CTA_BCO
         AND CTA_EMPR = DOC_EMPR
            
         AND DOC_CLAVE = DCON_CLAVE_DOC
         AND DOC_EMPR = DCON_EMPR
            
         AND DCON_CLAVE_CTACO IS NOT NULL
         AND DOC_EMPR = P_EMPRESA
            
         AND DOC_FEC_DOC BETWEEN P_FECHA_INICIO AND P_FECHA_FIN
            
         AND DOC_CTA_BCO = V_CTA_BCO
         AND CTA_CLAVE_CTACO = V_CTA_CTACO
         AND DECODE(DCON_TIPO_SALDO, 'D', 'C', 'D') = V_TIPO_SALDO
      
       GROUP BY DOC_CTA_BCO,
                CTA_CLAVE_CTACO,
                DCON_CLAVE_CTACO,
                DECODE(DCON_TIPO_SALDO, 'D', 'C', 'D')
       ORDER BY 1, 2, 4;
  
    -- DEFINICION DE VARIABLES AUXILIARES
    V_EJERCICIO   NUMBER;
    V_CLAVE_ASI   NUMBER;
    V_NRO_ASIENTO NUMBER;
    V_ASID_ITEM   NUMBER;
    V_TOTAL       NUMBER;
    V_TIPO_SALDO  VARCHAR2(1);
    V_MENSAJE     NUMBER;
    --V_PAUSE VARCHAR2(10) := 'OFF';
  
  BEGIN
    -- SELECCIONAR EJERCICIO CORRESPONDIENTE AL PERIODO DE FINANZAS
  
    SELECT MIN(EJ_CODIGO)
      INTO V_EJERCICIO
      FROM CNT_EJERCICIO
    --WHERE TO_DATE(TO_CHAR(EJ_FEC_INICIAL,'YYYY'),'YYYY') = TO_DATE(TO_CHAR(:BASI.CONF_PER_ACT_INI,'YYYY'),'YYYY');
     WHERE P_FECHA_INICIO BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
       AND EJ_EMPR = P_EMPRESA;
  
    FOR RBCOE IN BCO_ENCA_CUR LOOP
      --VALIDAR QUE LA CUENTA CONTABLE DE LA CUENTA BANCARIA NO SEA NULL
      IF RBCOE.CTA_CLAVE_CTACO IS NULL THEN
      
        RAISE_APPLICATION_ERROR(-20016,
                                'Primero debe asignar una cuenta contable a la cuenta bancaria nro:' ||
                                RBCOE.DOC_CTA_BCO || '.');
        --DO_KEY('EXIT');
      END IF;
      -- SELECCIONAR CLAVE PARA EL ASIENTO
      SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_CLAVE_ASI FROM DUAL;
    
      -- OBTENER EL MAXIMO NUMERO DE ASIENTO QUE ESTA GRABADO.
      SELECT MAX(ASI_NRO)
        INTO V_NRO_ASIENTO
        FROM CNT_ASIENTO
       WHERE ASI_EMPR = P_EMPRESA
         AND ASI_EJERCICIO = V_EJERCICIO;
    
      -- SE INSERTA LA CABECERA DE ASIENTO CON NUMERO DE ASIENTO IGUAL A MAX(ASI_NRO)
      -- MAS UNO.
    
      INSERT INTO CNT_ASIENTO
        (ASI_CLAVE,
         ASI_EMPR,
         ASI_EJERCICIO,
         ASI_NRO,
         ASI_FEC,
         ASI_OBS,
         ASI_FEC_GRAB,
         ASI_LOGIN,
         ASI_SIST,
         ASI_NRO_LISTADO_AUD)
      VALUES
        (V_CLAVE_ASI,
         P_EMPRESA,
         V_EJERCICIO,
         NVL(V_NRO_ASIENTO, 0) + 1,
         P_FECHA_FIN,
         'ASIENTO DE FINANZAS',
         SYSDATE,
         GEN_DEVUELVE_USER,
         'FIN',
         NULL);
      V_ASID_ITEM := 0;
      V_TOTAL     := 0;
    
      FOR RBCOD IN BCO_DET_CUR(RBCOE.DOC_CTA_BCO,
                               RBCOE.CTA_CLAVE_CTACO,
                               RBCOE.TIPO_SALDO) LOOP
      
        V_MENSAJE := TO_CHAR(RBCOD.DCON_CLAVE_CTACO);
        --   SYNCHRONIZE;
        V_ASID_ITEM := V_ASID_ITEM + 1;
        V_TOTAL     := V_TOTAL + RBCOD.IMP_TOTAL;
        -- INSERTAR LOS DETALLES DEL ASIENTO.
        INSERT INTO CNT_ASIENTO_DET
          (ASID_CLAVE_ASI,
           ASID_ITEM,
           ASID_CLAVE_CTACO,
           ASID_IND_DB_CR,
           ASID_IMPORTE,
           ASID_TIPO_MOV,
           ASID_NRO_DOC,
           ASID_DESC,
           ASID_EMPR)
        VALUES
          (V_CLAVE_ASI,
           V_ASID_ITEM,
           RBCOD.DCON_CLAVE_CTACO,
           RBCOE.TIPO_SALDO,
           RBCOD.IMP_TOTAL,
           NULL,
           NULL,
           'CTA BANCARIA:' || RBCOE.DOC_CTA_BCO,
           P_EMPRESA);
      
      END LOOP;
    
      -- AL SALIR DEL DETALLE INSERTAR UNO MAS PARA BALANCEAR EL ASIENTO CON LA CUENTA
      --   IF RBCOE.INDICADOR ='S' THEN
      V_ASID_ITEM := V_ASID_ITEM + 1;
      IF RBCOE.TIPO_SALDO = 'D' THEN
        V_TIPO_SALDO := 'C';
      ELSE
        V_TIPO_SALDO := 'D';
      END IF;
      INSERT INTO CNT_ASIENTO_DET
        (ASID_CLAVE_ASI,
         ASID_ITEM,
         ASID_CLAVE_CTACO,
         ASID_IND_DB_CR,
         ASID_IMPORTE,
         ASID_TIPO_MOV,
         ASID_NRO_DOC,
         ASID_DESC,
         ASID_EMPR)
      VALUES
        (V_CLAVE_ASI,
         V_ASID_ITEM,
         RBCOE.CTA_CLAVE_CTACO,
         V_TIPO_SALDO,
         V_TOTAL,
         NULL,
         NULL,
         'CTA BANCARIA:' || RBCOE.DOC_CTA_BCO,
         P_EMPRESA);
    
    END LOOP;
  
  END;

  PROCEDURE PP_GENERAR_ASI_COMBUSTIBLE(P_EMPRESA      IN NUMBER,
                                       P_FECHA_INICIO IN DATE,
                                       P_FECHA_FIN    IN DATE,
                                       P_MENSAJE      OUT NUMBER) AS
  
    CURSOR PROV_CLI_ENCA_CUR IS
      SELECT DECODE(DOC_CLI, NULL, 'P', 'C') PR_CL,
             DECODE(FCON_TIPO_SALDO, 'D', 'C', 'D') FCON_TIPO_SALDO
        FROM FIN_CONCEPTO,
             FIN_DOCUMENTO,
             FIN_DOC_CONCEPTO,
             FIN_CLIENTE,
             FIN_PROVEEDOR,
             FIN_CONFIGURACION,
             CNT_CUENTA        C, --PROVEEDORES
             CNT_CUENTA        E, --PROVEEDORES
             CNT_CUENTA        F --CLIENTES
       WHERE FCON_CLAVE = DCON_CLAVE_CONCEPTO
         AND FCON_EMPR = DCON_EMPR
            
         AND DOC_EMPR = CONF_EMPR
            
         AND CONF_EMPR = P_EMPRESA
            
         AND DOC_CLAVE = DCON_CLAVE_DOC
         AND DOC_EMPR = DCON_EMPR
            
         AND DOC_EMPR = P_EMPRESA
         AND DOC_FEC_DOC BETWEEN P_FECHA_INICIO AND P_FECHA_FIN
         AND DOC_CTA_BCO IS NULL
         AND (DOC_CLI IS NOT NULL OR DOC_PROV IS NOT NULL OR
             DOC_CTACO IS NOT NULL)
            --     AND DOC_SIST <> 'ACO' --NO INCLUIR LOS ASIENTOS DE ACOPIO --SACADO A PEDIDO DE LEANDRO EL 20110714
         AND DOC_TIPO_MOV = 81
         AND DOC_SIST <> 'ACL' --NO INCLUIR LOS ASIENTOS DE ACOPIO DE LECHE
         AND DOC_SIST <> 'PMO' --NO INCLUIR LOS ASIENTOS DE PRESTAMOS
         AND DOC_CLI = CLI_CODIGO(+)
         AND DOC_EMPR = CLI_EMPR(+)
            
         AND DOC_PROV = PROV_CODIGO(+)
         AND DOC_EMPR = PROV_EMPR(+)
            
         AND E.CTAC_CLAVE(+) = CONF_CLAVE_CTACO_ACREEDORES --PROVEEDORES
         AND E.CTAC_EMPR(+) = CONF_EMPR --PROVEEDORES
            
         AND F.CTAC_CLAVE(+) = CONF_CLAVE_CTACO_DEUDORES --CLIENTES
         AND F.CTAC_EMPR(+) = CONF_EMPR --CLIENTES
            
         AND C.CTAC_CLAVE(+) = PROV_CTA_CONTABLE --PROVEEDORES
         AND C.CTAC_EMPR(+) = PROV_EMPR --PROVEEDORES
      
       GROUP BY DECODE(DOC_CLI, NULL, 'P', 'C'),
                DECODE(FCON_TIPO_SALDO, 'D', 'C', 'D');
  
    -- CURSOR PARA TRAER LOS TOTALES POR CUENTA
    CURSOR PROV_CLI_DET_CUR(V_PRCL VARCHAR2, V_TIPO_SALDO VARCHAR2) IS
      SELECT DOC_CLAVE,
             DCON_CLAVE_CTACO,
             SUM(DCON_EXEN_LOC + DCON_GRAV_LOC + DCON_IVA_LOC) IMP_TOTAL,
             DOC_NRO_DOC,
             DOC_TIPO_MOV,
             DOC_CTACO
        FROM FIN_CONCEPTO, FIN_DOCUMENTO, FIN_DOC_CONCEPTO
       WHERE FCON_CLAVE = DCON_CLAVE_CONCEPTO
         AND FCON_EMPR = DCON_EMPR
            
         AND DOC_CLAVE = DCON_CLAVE_DOC
         AND DOC_EMPR = DCON_EMPR
            
         AND DOC_EMPR = P_EMPRESA
         AND DOC_FEC_DOC BETWEEN P_FECHA_INICIO AND P_FECHA_FIN
            
         AND DOC_CTA_BCO IS NULL
         AND DECODE(DOC_CLI, NULL, 'P', 'C') = V_PRCL
         AND DECODE(FCON_TIPO_SALDO, 'D', 'C', 'D') = V_TIPO_SALDO
            --     AND DOC_SIST <> 'ACO' --NO INCLUIR LOS ASIENTOS DE ACOPIO  --SACADO A PEDIDO DE LEANDRO EL 20110714
         AND DOC_TIPO_MOV = 81
         AND DOC_SIST <> 'ACL' --NO INCLUIR LOS ASIENTOS DE ACOPIO DE LECHE
         AND DOC_SIST <> 'PMO' --NO INCLUIR LOS ASIENTOS DE PRESTAMOS
       GROUP BY DOC_CLAVE,
                DCON_CLAVE_CTACO,
                DOC_NRO_DOC,
                DOC_TIPO_MOV,
                DOC_CTACO;
  
    CURSOR PROV_CLI_DOC(V_PRCL VARCHAR2, V_TIPO_SALDO VARCHAR2) IS
      SELECT DOC_CLAVE,
             DOC_TIPO_MOV,
             DOC_NRO_DOC,
             DCON_CLAVE_CTACO,
             SUM(DCON_EXEN_LOC + DCON_GRAV_LOC + DCON_IVA_LOC) IMP_TOTAL
        FROM FIN_CONCEPTO, FIN_DOCUMENTO, FIN_DOC_CONCEPTO
       WHERE FCON_CLAVE = DCON_CLAVE_CONCEPTO
         AND FCON_EMPR = DCON_EMPR
            
         AND DOC_CLAVE = DCON_CLAVE_DOC
         AND DOC_EMPR = DCON_EMPR
            
         AND DOC_EMPR = P_EMPRESA
         AND DOC_FEC_DOC BETWEEN P_FECHA_INICIO AND P_FECHA_FIN
         AND DOC_CTA_BCO IS NULL
         AND DECODE(DOC_CLI, NULL, 'P', 'C') = V_PRCL
         AND DECODE(FCON_TIPO_SALDO, 'D', 'C', 'D') = V_TIPO_SALDO
            --     AND DOC_SIST <> 'ACO' --NO INCLUIR LOS ASIENTOS DE ACOPIO  --SACADO A PEDIDO DE LEANDRO EL 20110714
         AND DOC_TIPO_MOV = 81
         AND DOC_SIST <> 'ACL' --NO INCLUIR LOS ASIENTOS DE ACOPIO DE LECHE
         AND DOC_SIST <> 'PMO' --NO INCLUIR LOS ASIENTOS DE PRESTAMOS
       GROUP BY DOC_CLAVE, DOC_TIPO_MOV, DOC_NRO_DOC, DCON_CLAVE_CTACO;
  
    TABLALLAVEADA EXCEPTION;
    PRAGMA EXCEPTION_INIT(TABLALLAVEADA, -54);
  
    -- VARIABLES AUXILIARES
    V_TOTAL       NUMBER;
    V_CLAVE_ASI   NUMBER;
    V_EJERCICIO   NUMBER;
    V_NRO_ASIENTO NUMBER;
    V_ASID_ITEM   NUMBER;
    V_CLAVE_TOTAL NUMBER;
    V_TIPO_SALDO  VARCHAR2(1);
    V_NRO_DOC     NUMBER;
    V_TIPO_MOV    NUMBER;
    ---------
    V_NRO_DOC_I   NUMBER;
    V_TIPO_MOV_I  NUMBER;
    V_DOC_CTA_CO  NUMBER;
    V_MENSAJE     NUMBER;
    V_COD_PERIODO NUMBER;
  
  BEGIN
  
    SELECT PERI_CODIGO
      INTO V_COD_PERIODO
      FROM FIN_PERIODO P
     WHERE P.PERI_EMPR = P_EMPRESA
       AND PERI_FEC_INI BETWEEN TO_DATE(P_FECHA_INICIO, 'DD/MM/YYYY') AND
           TO_DATE(P_FECHA_INICIO, 'DD/MM/YYYY');
  
    -- SELECCIONAR EJERCICIO CORRESPONDIENTE AL PERIODO DE FINANZAS
  
    SELECT MIN(EJ_CODIGO)
      INTO V_EJERCICIO
      FROM CNT_EJERCICIO
    --WHERE TO_DATE(TO_CHAR(EJ_FEC_INICIAL,'YYYY'),'YYYY') = TO_DATE(TO_CHAR(:BASI.CONF_PER_ACT_INI,'YYYY'),'YYYY');
     WHERE P_FECHA_INICIO BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
       AND EJ_EMPR = P_EMPRESA;
  
    -- SE LLAVEA LA TABLA PARA QUE NADIE PUEDA INGRESAR ASIENTOS ENTRE MEDIO DE LOS
    -- ASIENTOS GENERADOS.
  
    LOCK TABLE CNT_ASIENTO IN EXCLUSIVE MODE NOWAIT;
  
    FOR PCENCA IN PROV_CLI_ENCA_CUR LOOP
    
      -- SELECCIONAR CLAVE PARA EL ASIENTO
    
      SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_CLAVE_ASI FROM DUAL;
    
      -- OBTENER EL MAXIMO NUMERO DE ASIENTO QUE ESTA GRABADO.
    
      SELECT MAX(ASI_NRO)
        INTO V_NRO_ASIENTO
        FROM CNT_ASIENTO
       WHERE ASI_EMPR = P_EMPRESA
         AND ASI_EJERCICIO = V_EJERCICIO;
    
      -- SE INSERTA LA CABECERA DE ASIENTO CON NUMERO DE ASIENTO IGUAL A MAX(ASI_NRO)
      -- MAS UNO.
    
      INSERT INTO CNT_ASIENTO
        (ASI_CLAVE,
         ASI_EMPR,
         ASI_EJERCICIO,
         ASI_NRO,
         ASI_FEC,
         ASI_OBS,
         ASI_FEC_GRAB,
         ASI_LOGIN,
         ASI_SIST,
         ASI_NRO_LISTADO_AUD)
      VALUES
        (V_CLAVE_ASI,
         P_EMPRESA,
         V_EJERCICIO,
         NVL(V_NRO_ASIENTO, 0) + 1,
         P_FECHA_FIN,
         'COMBUSTIBLES',
         SYSDATE,
         GEN_DEVUELVE_USER,
         'FIN',
         NULL);
      V_ASID_ITEM := 0;
      V_TOTAL     := 0;
    
      FOR PCDET IN PROV_CLI_DET_CUR(PCENCA.PR_CL, PCENCA.FCON_TIPO_SALDO) LOOP
        V_MENSAJE := TO_CHAR(PCDET.DCON_CLAVE_CTACO);
      
        V_ASID_ITEM  := V_ASID_ITEM + 1;
        V_TOTAL      := V_TOTAL + PCDET.IMP_TOTAL;
        V_NRO_DOC    := PCDET.DOC_NRO_DOC;
        V_TIPO_MOV   := PCDET.DOC_TIPO_MOV;
        V_DOC_CTA_CO := PCDET.DOC_CTACO;
      
        INSERT INTO CNT_ASIENTO_DET
          (ASID_CLAVE_ASI,
           ASID_ITEM,
           ASID_CLAVE_CTACO,
           ASID_IND_DB_CR,
           ASID_IMPORTE,
           ASID_TIPO_MOV,
           ASID_NRO_DOC,
           ASID_DESC,
           ASID_EMPR)
        VALUES
          (V_CLAVE_ASI,
           V_ASID_ITEM,
           PCDET.DCON_CLAVE_CTACO,
           PCENCA.FCON_TIPO_SALDO,
           PCDET.IMP_TOTAL,
           PCDET.DOC_TIPO_MOV,
           PCDET.DOC_NRO_DOC,
           'Combustibles',
           P_EMPRESA);
      
        ---DESPUES DE CADA ASIENTO DE ANTICIPO QUE HAGA TAMBIEN CONTRA COMBUSTIBLES
        V_ASID_ITEM   := V_ASID_ITEM + 1;
        V_CLAVE_TOTAL := V_DOC_CTA_CO; --2487 CLAVE CTACO DE COMBUSTIBLES
        IF PCENCA.FCON_TIPO_SALDO = 'D' THEN
          V_TIPO_SALDO := 'C';
        ELSE
          V_TIPO_SALDO := 'D';
        END IF;
      
        INSERT INTO CNT_ASIENTO_DET
          (ASID_CLAVE_ASI,
           ASID_ITEM,
           ASID_CLAVE_CTACO,
           ASID_IND_DB_CR,
           ASID_IMPORTE,
           ASID_TIPO_MOV,
           ASID_NRO_DOC,
           ASID_DESC,
           ASID_EMPR)
        VALUES
          (V_CLAVE_ASI,
           V_ASID_ITEM,
           V_CLAVE_TOTAL,
           V_TIPO_SALDO,
           PCDET.IMP_TOTAL,
           PCDET.DOC_TIPO_MOV,
           PCDET.DOC_NRO_DOC,
           'Combustibles',
           P_EMPRESA);
      
      END LOOP;
    
      FOR RDOC IN PROV_CLI_DOC(PCENCA.PR_CL, PCENCA.FCON_TIPO_SALDO) LOOP
        V_NRO_DOC_I  := RDOC.DOC_NRO_DOC;
        V_TIPO_MOV_I := RDOC.DOC_TIPO_MOV;
      
        INSERT INTO CNT_ASIENTO_DOC_DET
          (ASIDD_CLAVE_ASI,
           ASIDD_CLAVE_DOC,
           ASIDD_ITEM,
           ASIDD_CLAVE_CTACO,
           ASIDD_IND_DB_CR,
           ASIDD_IMPORTE,
           ASIDD_TIPO_MOV,
           ASIDD_NRO_DOC,
           ASIDD_DESC,
           ASIDD_EMPR)
        VALUES
          (V_CLAVE_ASI,
           RDOC.DOC_CLAVE,
           V_ASID_ITEM,
           RDOC.DCON_CLAVE_CTACO,
           PCENCA.FCON_TIPO_SALDO,
           RDOC.IMP_TOTAL,
           RDOC.DOC_TIPO_MOV,
           RDOC.DOC_NRO_DOC,
           'Combustibles',
           P_EMPRESA);
      END LOOP;
    
      -- AL SALIR DEL DETALLE INSERTAR UNO MAS PARA BALANCEAR EL ASIENTO CON LA CUENTA
      -- DE DEUDORES O ACREEDORES
    
      --IND
    
      --IF PCENCA.PR_CL = 'C' THEN
    
      --ELSE
      --  V_CLAVE_TOTAL := :BASI.CONF_CLAVE_CTACO_ACREEDORES;
      --END IF;
    
      /*IF PCENCA.PR_CL = 'C' THEN
        V_CLAVE_TOTAL := :BASI.CONF_CLAVE_CTACO_DEUDORES;
      ELSE
        V_CLAVE_TOTAL := :BASI.CONF_CLAVE_CTACO_ACREEDORES;
      END IF;*/
    
      INSERT INTO CNT_ASIENTO_DOC_DET
        (ASIDD_CLAVE_ASI,
         ASIDD_CLAVE_DOC,
         ASIDD_ITEM,
         ASIDD_CLAVE_CTACO,
         ASIDD_IND_DB_CR,
         ASIDD_IMPORTE,
         ASIDD_TIPO_MOV,
         ASIDD_NRO_DOC,
         ASIDD_DESC,
         ASIDD_EMPR)
      VALUES
        (V_CLAVE_ASI,
         NULL,
         V_ASID_ITEM,
         V_CLAVE_TOTAL,
         V_TIPO_SALDO,
         V_TOTAL,
         V_TIPO_MOV_I,
         V_NRO_DOC_I,
         NULL,
         P_EMPRESA);
    END LOOP;
  
    P_MENSAJE := V_MENSAJE;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20016,
                              '6 No se han encontrado datos al generar deudores/acreedores!.');
    WHEN TABLALLAVEADA THEN
      RAISE_APPLICATION_ERROR(-20016,
                              'Tabla de asientos llaveada por otro usuario');
    
  END;

  PROCEDURE FIN_ASI_PREV_INCOBRABLES(P_EMPRESA     IN NUMBER,
                                     P_FECHA_DESDE IN DATE,
                                     P_FECHA_HASTA IN DATE,
                                     P_COD_PERIODO NUMBER,
                                     P_LOGIN       IN VARCHAR2) AS
  
    V_ASI_CLAVE     NUMBER;
    V_ASI_EJERCICIO NUMBER;
    V_ASI_NRO       NUMBER;
    V_NRO_ITEM      NUMBER := 0;
    SALIR EXCEPTION;
    V_FEC_INI            DATE;
    V_FEC_FIN            DATE;
    V_IMPORTE_TOTAL_GS   NUMBER := 0;
    V_PORC_MOROSIDAD     NUMBER := 0;
    V_IMPORTE_INCOBRABLE NUMBER := 0;
  
  BEGIN
  
    --==ESTO AGREGUE DE NUEVO POR QUE TENIA ALGUNOS PROBLEMA CON LA FECHA
    SELECT PERI_FEC_INI, PERI_FEC_FIN, C.CONF_PORC_MOROS_CLI
      INTO V_FEC_INI, V_FEC_FIN, V_PORC_MOROSIDAD
      FROM FIN_PERIODO P, FIN_CONFIGURACION C
     WHERE P.PERI_EMPR = P_EMPRESA
       AND PERI_CODIGO = P_COD_PERIODO
       AND PERI_EMPR = CONF_EMPR;
    --=========================================================
    IF V_FEC_INI < '01/07/2018' THEN
      RAISE SALIR;
    END IF;
  
    IF GEN_PACK_SUC.GEN_CONF_IND_SUC = 'S' THEN
      RAISE SALIR;
    END IF;
  
    DELETE CNT_ASIENTO_DET
     WHERE ASID_EMPR = P_EMPRESA
       AND ASID_CLAVE_ASI IN
           (SELECT ASI_CLAVE
              FROM CNT_ASIENTO
             WHERE CNT_ASIENTO.ASI_SIST = 'FIN'
               AND ASI_FEC BETWEEN V_FEC_INI AND V_FEC_FIN
               AND UPPER(ASI_OBS) LIKE '%ASIENTO DE PREV. INCOBRA.%'
               AND ASI_EMPR = P_EMPRESA);
    DELETE FROM CNT_ASIENTO
     WHERE CNT_ASIENTO.ASI_SIST = 'FIN'
       AND ASI_FEC BETWEEN V_FEC_INI AND V_FEC_FIN
       AND UPPER(ASI_OBS) LIKE '%ASIENTO DE PREV. INCOBRA.%'
       AND ASI_EMPR = P_EMPRESA;
  
    BEGIN
      SELECT ROUND(SUM(DECODE(OPER_DESC,
                              'DEV_VENTA',
                              DETA_IMP_NETO_LOC * -1,
                              'ANUL_VENTA',
                              DETA_IMP_NETO_LOC * -1,
                              DETA_IMP_NETO_LOC)),
                   0) VENTA_GS
        INTO V_IMPORTE_TOTAL_GS
        FROM STK_ARTICULO,
             GEN_EMPRESA,
             STK_OPERACION,
             STK_DOCUMENTO,
             STK_DOCUMENTO_DET,
             (SELECT DOCU_CLAVE_PADRE ANUL_CLAVE, DOCU_EMPR ANUL_EMPR
                FROM STK_DOCUMENTO
               WHERE DOCU_EMPR = P_EMPRESA
                 AND DOCU_CODIGO_OPER IN (14, 34))
       WHERE DOCU_CLAVE = DETA_CLAVE_DOC
         AND DOCU_EMPR = DETA_EMPR
            
         AND EMPR_CODIGO = DOCU_EMPR
            
         AND ART_CODIGO = DETA_ART
         AND ART_EMPR = DETA_EMPR
            
         AND OPER_CODIGO = DOCU_CODIGO_OPER
         AND OPER_EMPR = DOCU_EMPR
            
         AND (OPER_DESC IN ('VENTA', 'DEV_VENTA'))
         AND DOCU_CLAVE = ANUL_CLAVE(+)
         AND DOCU_EMPR = ANUL_EMPR(+)
         AND ANUL_CLAVE IS NULL
         AND DOCU_EMPR = P_EMPRESA
         AND TO_CHAR(DOCU_FEC_EMIS, 'MM/YYYY') =
             TO_CHAR(V_FEC_INI, 'MM/YYYY')
       GROUP BY DOCU_EMPR, EMPR_RAZON_SOCIAL;
    exception
      when no_Data_found then
        V_IMPORTE_TOTAL_GS := 0;
    END;
    
    V_IMPORTE_INCOBRABLE := ROUND((V_IMPORTE_TOTAL_GS * V_PORC_MOROSIDAD) / 100,
                                  0); --CALCULO SEGUN EL PORCENTAJE DE FIN CONFIGURACION
  
    ---------------------------------------------------------------------------------------------------
    --ASIENTO EN MONEDA LOCAL (GS)
    ---------------------------------------------------------------------------------------------------
    SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_ASI_CLAVE FROM DUAL;
  
    SELECT EJ_CODIGO
      INTO V_ASI_EJERCICIO
      FROM CNT_EJERCICIO
     WHERE V_FEC_INI BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
       AND EJ_EMPR = P_EMPRESA;
  
    SELECT NVL(MAX(A.ASI_NRO), 0) + 1
      INTO V_ASI_NRO
      FROM CNT_ASIENTO A
     WHERE A.ASI_EJERCICIO = V_ASI_EJERCICIO
       AND ASI_EMPR = P_EMPRESA;
  
    INSERT INTO CNT_ASIENTO
      (ASI_CLAVE,
       ASI_EMPR,
       ASI_EJERCICIO,
       ASI_NRO,
       ASI_FEC,
       ASI_OBS,
       ASI_FEC_GRAB,
       ASI_LOGIN,
       ASI_SIST,
       ASI_NRO_LISTADO_AUD)
    VALUES
      (V_ASI_CLAVE,
       P_EMPRESA,
       V_ASI_EJERCICIO,
       V_ASI_NRO,
       V_FEC_FIN,
       'ASIENTO DE PREV. INCOBRA. ' ||
       UPPER(REPLACE(TO_CHAR(V_FEC_INI, 'Month-YYYY'), ' ', '')),
       SYSDATE,
       P_LOGIN,
       'FIN',
       NULL);
    ---
    V_NRO_ITEM := NVL(V_NRO_ITEM, 0) + 1;
  
    BEGIN
      INSERT INTO CNT_ASIENTO_DET
        (ASID_CLAVE_ASI,
         ASID_ITEM,
         ASID_CLAVE_CTACO,
         ASID_IND_DB_CR,
         ASID_IMPORTE,
         ASID_TIPO_MOV,
         ASID_NRO_DOC,
         ASID_DESC,
         ASID_EMPR)
      VALUES
        (V_ASI_CLAVE,
         V_NRO_ITEM,
         1988,
         'D',
         V_IMPORTE_INCOBRABLE,
         NULL,
         NULL,
         NULL,
         P_EMPRESA);
    
      V_NRO_ITEM := V_NRO_ITEM + 1;
      INSERT INTO CNT_ASIENTO_DET
        (ASID_CLAVE_ASI,
         ASID_ITEM,
         ASID_CLAVE_CTACO,
         ASID_IND_DB_CR,
         ASID_IMPORTE,
         ASID_TIPO_MOV,
         ASID_NRO_DOC,
         ASID_DESC,
         ASID_EMPR)
      VALUES
        (V_ASI_CLAVE,
         V_NRO_ITEM,
         2004,
         'C',
         V_IMPORTE_INCOBRABLE,
         NULL,
         NULL,
         NULL,
         P_EMPRESA);
    
    EXCEPTION
      WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20000, SQLERRM);
    END;
  
    --COMMIT;
  
  EXCEPTION
    WHEN SALIR THEN
      NULL;
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20000, SQLERRM);
  END FIN_ASI_PREV_INCOBRABLES;

  PROCEDURE PP_GENERAR_ASI_AGUINALDO(P_EMPRESA      IN NUMBER,
                                     P_FECHA_INICIO IN DATE,
                                     P_FECHA_FIN    IN DATE,
                                     P_MENSAJE      OUT NUMBER) AS
    V_CLAVE                     NUMBER;
    V_ITEM                      NUMBER := 0;
    V_NRO                       NUMBER;
    V_EJ_CODIGO                 NUMBER;
    V_TOT_AGUINALDO             NUMBER := 0;
    V_CONF_CTACO_PROV_AGUINALDO NUMBER;
  
    CURSOR CUR_AGUINALDO IS
      SELECT PROVA_ITEM,
             PROVA_CCOSTO,
             B.CTAC_CLAVE,
             B.CTAC_NRO CUENTA_PROV,
             AGUINALDO,
             CANB_CODIGO,
             CANAL_BETA_DOC
        FROM CNT_CONF_PROV_AGUINALDO,
             CNT_CUENTA A,
             CNT_CUENTA B,
             (SELECT G.EMPR_CODIGO,
                     CUENTA_NRO,
                     ROUND(SUM(G.DET_TOTAL_GS / 12)) AGUINALDO,
                     CANAL.CANB_CODIGO,
                     CANAL_BETA_DOC
                FROM CUBO_MOV_GASTOS_XCANAL_GRAL_V G,
                     FAC_CANAL_VENTA_BETA          CANAL
               WHERE MES = TO_CHAR(P_FECHA_INICIO, 'YYYY-MM')
                 AND CANAL_BETA_DOC = CANAL.CANB_DESC(+)
                 AND G.EMPR_CODIGO = CANAL.CANB_EMPR(+)
                 AND G.EMPR_CODIGO = P_EMPRESA
                 AND CUENTA_NRO <> 312080000
               GROUP BY G.EMPR_CODIGO,
                        CUENTA_NRO,
                        CANAL.CANB_CODIGO,
                        CANAL_BETA_DOC) GASTOS
      
       WHERE PROVA_CUENTA = A.CTAC_CLAVE
         AND PROVA_EMPR = A.CTAC_EMPR
            
         AND PROVA_CUENTA_PROV = B.CTAC_CLAVE
         AND PROVA_EMPR = B.CTAC_EMPR
            
         AND A.CTAC_NRO = GASTOS.CUENTA_NRO
         AND A.CTAC_EMPR = GASTOS.EMPR_CODIGO
            
         AND A.CTAC_EMPR = P_EMPRESA
      -- -- -- -- -- -- -- -- -- --
      --AND CUENTA_NRO IN ('416030000') --COMENTAR, CUENTA DE PRUEBA.
      -- -- -- -- -- -- -- -- -- --
       ORDER BY B.CTAC_CLAVE;
  
  BEGIN
  
    SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_CLAVE FROM DUAL;
  
    SELECT CONF_CTACO_PROV_AGUINALDO
      INTO V_CONF_CTACO_PROV_AGUINALDO
      FROM FIN_CONFIGURACION
     WHERE CONF_EMPR = P_EMPRESA;
  
    SELECT EJ_CODIGO
      INTO V_EJ_CODIGO
      FROM CNT_EJERCICIO
     WHERE P_FECHA_INICIO BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
       AND EJ_EMPR = P_EMPRESA;
  
    SELECT NVL(MAX(ASI_NRO), 0) + 1
      INTO V_NRO
      FROM CNT_ASIENTO
     WHERE ASI_EJERCICIO = V_EJ_CODIGO
       AND ASI_EMPR = P_EMPRESA;
  
    INSERT INTO CNT_ASIENTO
      (ASI_CLAVE,
       ASI_EMPR,
       ASI_EJERCICIO,
       ASI_NRO,
       ASI_FEC,
       ASI_OBS,
       ASI_FEC_GRAB,
       ASI_LOGIN,
       ASI_SIST,
       ASI_NRO_LISTADO_AUD)
    VALUES
      (V_CLAVE,
       P_EMPRESA,
       V_EJ_CODIGO,
       V_NRO,
       P_FECHA_FIN,
       'PROV.AGUINALDO-' || TO_CHAR(P_FECHA_FIN, 'MONTH'),
       SYSDATE,
       GEN_DEVUELVE_USER,
       'SIG',
       NULL);
  
    FOR A IN CUR_AGUINALDO LOOP
      V_ITEM := V_ITEM + 1;
      INSERT INTO CNT_ASIENTO_DET
        (ASID_CLAVE_ASI,
         ASID_ITEM,
         ASID_CLAVE_CTACO,
         ASID_IND_DB_CR,
         ASID_IMPORTE,
         ASID_TIPO_MOV,
         ASID_NRO_DOC,
         ASID_DESC,
         ASID_CANAL_BETA,
         ASID_EMPR)
      VALUES
        (V_CLAVE,
         V_ITEM,
         A.CTAC_CLAVE,
         'D',
         A.AGUINALDO,
         NULL,
         NULL,
         NULL,
         A.CANB_CODIGO,
         P_EMPRESA);
      V_TOT_AGUINALDO := V_TOT_AGUINALDO + A.AGUINALDO;
    END LOOP;
  
    V_ITEM := V_ITEM + 1;
    INSERT INTO CNT_ASIENTO_DET
      (ASID_CLAVE_ASI,
       ASID_ITEM,
       ASID_CLAVE_CTACO,
       ASID_IND_DB_CR,
       ASID_IMPORTE,
       ASID_TIPO_MOV,
       ASID_NRO_DOC,
       ASID_DESC,
       ASID_CANAL_BETA,
       ASID_EMPR)
    VALUES
      (V_CLAVE,
       V_ITEM,
       V_CONF_CTACO_PROV_AGUINALDO,
       'C',
       V_TOT_AGUINALDO,
       NULL,
       NULL,
       NULL,
       NULL,
       P_EMPRESA);
  
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20000, SQLERRM);
  END;

----------------------provision indernizacion y preaviso       lv 01/02/2021


 PROCEDURE PP_GENERAR_ASI_INDEMNIZACION(P_EMPRESA      IN NUMBER,
                                       P_FECHA_INICIO IN DATE,
                                       P_FECHA_FIN    IN DATE,
                                       P_MENSAJE      OUT NUMBER) AS
    V_CLAVE                     NUMBER;
    V_ITEM                      NUMBER := 0;
    V_NRO                       NUMBER;
    V_EJ_CODIGO                 NUMBER;
    V_TOT_INDERNIZACION             NUMBER := 0;
    V_CONF_CTACO_PROV_IND NUMBER;
  
    CURSOR CUR_INDEMNIZACION IS
     /*       SELECT 1 ITEM,
             T.CCO_CODIGO,
             T.CCO_DESC,
             H.CTAC_CLAVE,
             T.CTAC_NRO,
             T.CTAC_DESC,
             ROUND(SUM(T.PGTO_IMPORTE) / 12) IMPORTE_MENSUAL,
             T.CANB_CODIGO,
             T.CANB_DESC,
             T.PGTO_EMPR,
              COUNT (CCO_CODIGO) OVER (PARTITION BY  CCO_CODIGO) CAT
        FROM FAC_LONDON_PRES_GASTO_AN_V T, CNT_CUENTA H
       WHERE T.CTAC_NRO = H.CTAC_NRO
         AND T.PGTO_EMPR = H.CTAC_EMPR
         AND T.PGTO_ANHO = TO_CHAR(P_FECHA_FIN,'YYYY')
         AND UPPER(T.CTAC_DESC) LIKE '%INDEM%'--'%INDEM%'
       GROUP BY 1,
                 T.CCO_CODIGO,
                 T.CCO_DESC,
                 H.CTAC_CLAVE,
                 T.CTAC_NRO,
                 T.CTAC_DESC,
                 T.CANB_CODIGO,
                 T.CANB_DESC,
                 T.PGTO_EMPR
       ORDER BY 1, 5;*/
       
       SELECT CCO_CODIGO,
       CCO_DESC,
       CTAC_CLAVE,
       CANB_CODIGO,
       ROUND(SUM(PGTO_IMPORTE) / 12) IMPORTE_MENSUAL
  FROM (SELECT case
                 when T.CCO_CODIGO = 32 AND T.pgto_mes > 3 AND T.pgto_anho = 2022 then
                  1
                 else
                  T.CCO_CODIGO
               end CCO_CODIGO,
               case
                 when T.CCO_CODIGO = 32 AND T.pgto_mes > 3 AND T.pgto_anho = 2022 then
                  'ADMINISTRACION'
                 else
                  t.CCO_DESC
               end CCO_DESC,
               Case
                 when T.CCO_CODIGO = 32 AND T.pgto_mes > 3 AND T.pgto_anho = 2022 then
                  2001
                 else
                  CTAC_CLAVE
               end CTAC_CLAVE,
               
               T.PGTO_IMPORTE,
               Case
                 when T.CCO_CODIGO = 32 AND T.pgto_mes > 3 AND T.pgto_anho = 2022 then
                  NULL
                 else
                  T.CANB_CODIGO
               end CANB_CODIGO
          FROM FAC_LONDON_PRES_GASTO_AN_V T, CNT_CUENTA H
         WHERE T.CTAC_NRO = H.CTAC_NRO
           AND T.PGTO_EMPR = H.CTAC_EMPR
           AND T.PGTO_ANHO = TO_CHAR(P_FECHA_FIN, 'YYYY')
           AND UPPER(T.CTAC_DESC) LIKE '%INDEM%')
 GROUP BY CCO_CODIGO, CCO_DESC, CTAC_CLAVE, CANB_CODIGO
 ORDER BY 1,3;
  
  BEGIN
  
    SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_CLAVE FROM DUAL;
  
    SELECT CONF_CTACO_PROV_INDERNIZACION
      INTO V_CONF_CTACO_PROV_IND
      FROM FIN_CONFIGURACION
     WHERE CONF_EMPR = P_EMPRESA;
  
    SELECT EJ_CODIGO
      INTO V_EJ_CODIGO
      FROM CNT_EJERCICIO
     WHERE P_FECHA_INICIO BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
       AND EJ_EMPR = P_EMPRESA;
  
    SELECT NVL(MAX(ASI_NRO), 0) + 1
      INTO V_NRO
      FROM CNT_ASIENTO
     WHERE ASI_EJERCICIO = V_EJ_CODIGO
       AND ASI_EMPR = P_EMPRESA;
  
    INSERT INTO CNT_ASIENTO
      (ASI_CLAVE,
       ASI_EMPR,
       ASI_EJERCICIO,
       ASI_NRO,
       ASI_FEC,
       ASI_OBS,
       ASI_FEC_GRAB,
       ASI_LOGIN,
       ASI_SIST,
       ASI_NRO_LISTADO_AUD)
    VALUES
      (V_CLAVE,
       P_EMPRESA,
       V_EJ_CODIGO,
       V_NRO,
       P_FECHA_FIN,
       'PROV.INDERNIZACION-' || TO_CHAR(P_FECHA_FIN, 'MONTH'),
       SYSDATE,
       GEN_DEVUELVE_USER,
       'SIG',
       NULL);
  
    FOR A IN CUR_INDEMNIZACION LOOP
      V_ITEM := V_ITEM + 1;
      INSERT INTO CNT_ASIENTO_DET
        (ASID_CLAVE_ASI,
         ASID_ITEM,
         ASID_CLAVE_CTACO,
         ASID_IND_DB_CR,
         ASID_IMPORTE,
         ASID_TIPO_MOV,
         ASID_NRO_DOC,
         ASID_DESC,
         ASID_CANAL_BETA,
         ASID_EMPR)
      VALUES
        (V_CLAVE,
         V_ITEM,
         A.CTAC_CLAVE,
         'D',
         A.IMPORTE_MENSUAL,
         NULL,
         NULL,
         NULL,
         A.CANB_CODIGO,
         P_EMPRESA);
      V_TOT_INDERNIZACION := V_TOT_INDERNIZACION + A.IMPORTE_MENSUAL;
    END LOOP;
  
    V_ITEM := V_ITEM + 1;
    INSERT INTO CNT_ASIENTO_DET
      (ASID_CLAVE_ASI,
       ASID_ITEM,
       ASID_CLAVE_CTACO,
       ASID_IND_DB_CR,
       ASID_IMPORTE,
       ASID_TIPO_MOV,
       ASID_NRO_DOC,
       ASID_DESC,
       ASID_CANAL_BETA,
       ASID_EMPR)
    VALUES
      (V_CLAVE,
       V_ITEM,
       V_CONF_CTACO_PROV_IND,
       'C',
       V_TOT_INDERNIZACION,
       NULL,
       NULL,
       NULL,
       NULL,
       P_EMPRESA);
  
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20000, SQLERRM);
  END;


-------------------------------------------------------------------------------LV




  PROCEDURE PP_GENERAR_ASI_COSTO_EXPORT(P_EMPRESA      IN NUMBER,
                                        P_FECHA_INICIO IN DATE,
                                        P_FECHA_FIN    IN DATE,
                                        P_MENSAJE      OUT NUMBER) AS
    V_ASI_CLAVE     NUMBER;
    V_ASI_EJERCICIO NUMBER;
    V_ASI_NRO       NUMBER;
    V_ASID_ITEM     NUMBER := 0;
    BAND            NUMBER := 0;
    CURSOR C IS
      SELECT CASE
               WHEN P_FECHA_INICIO <= '31/07/2021' THEN
                CLAS.CLAS_CTACO_MERCADERIA
               ELSE
                E.MARC_CTA_MERC
             END CLAS_CTACO_MERCADERIA,
             CLAS.CLAS_CTACO_EXPORTACION,
             CLAS.CLAS_CTACO_COSTO_EXPORTACION,
             CLAS.CLAS_CTACO_FACT_A_EMBARCAR,
             SUM(STDD.DETA_IMP_NETO_LOC) IMP_VENTA,
             SUM(STCD.CD_COSTO_PROM_LOC * STDD.DETA_CANT) IMP_COSTO,
             'Tmov:' || D.DOC_TIPO_MOV || ',Nro:' || D.DOC_NRO_DOC OBS,
             D.DOC_CLAVE,
             D.DOC_NRO_DOC
        FROM STK_DOCUMENTO     STDE,
             STK_DOCUMENTO_DET STDD,
             STK_COSTO_DIARIO  STCD,
             STK_ARTICULO      ART,
             STK_CLASIFICACION CLAS,
             FIN_DOCUMENTO     D,
             STK_MARCA         E
       WHERE STDE.DOCU_CLAVE = STDD.DETA_CLAVE_DOC
         AND STDE.DOCU_EMPR = STDD.DETA_EMPR
            
         AND ART_MARCA = MARC_CODIGO
         AND ART_EMPR = MARC_EMPR
            
         AND STDE.DOCU_FEC_EMIS = STCD.CD_FEC
         AND STDE.DOCU_EMPR = STCD.CD_EMPR
            
         AND STDD.DETA_ART = STCD.CD_ART
         AND STDD.DETA_EMPR = STCD.CD_EMPR
            
         AND STDD.DETA_ART = ART.ART_CODIGO
         AND STDD.DETA_EMPR = ART.ART_EMPR
            
         AND ART.ART_CLASIFICACION = CLAS.CLAS_CODIGO
         AND ART.ART_EMPR = CLAS.CLAS_EMPR
            
         AND STDE.DOCU_CLAVE_FIN = D.DOC_CLAVE
         AND STDE.DOCU_EMPR = D.DOC_EMPR
            
         AND DOCU_EMPR = P_EMPRESA
            
         AND D.DOC_IND_EXPORT = 'S'
         AND STDE.DOCU_FEC_EMIS BETWEEN P_FECHA_INICIO AND P_FECHA_FIN
       GROUP BY E.MARC_CTA_MERC,
                CLAS.CLAS_CTACO_EXPORTACION,
                CLAS.CLAS_CTACO_MERCADERIA,
                CLAS.CLAS_CTACO_COSTO_EXPORTACION,
                CLAS.CLAS_CTACO_FACT_A_EMBARCAR,
                'Tmov:' || D.DOC_TIPO_MOV || ',Nro:' || D.DOC_NRO_DOC,
                D.DOC_CLAVE,
                D.DOC_NRO_DOC
       ORDER BY D.DOC_NRO_DOC;
  
  BEGIN
  
    FOR V IN C LOOP
      IF BAND = 0 THEN
        SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_ASI_CLAVE FROM DUAL;
      
        SELECT MIN(EJ_CODIGO)
          INTO V_ASI_EJERCICIO
          FROM CNT_EJERCICIO
        --WHERE TO_DATE(TO_CHAR(EJ_FEC_INICIAL,'YYYY'),'YYYY') = TO_DATE(TO_CHAR(:BASI.CONF_PER_ACT_INI,'YYYY'),'YYYY');
         WHERE P_FECHA_INICIO BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
           AND EJ_EMPR = P_EMPRESA;
      
        SELECT MAX(ASI_NRO)
          INTO V_ASI_NRO
          FROM CNT_ASIENTO
         WHERE ASI_EMPR = P_EMPRESA
           AND ASI_EJERCICIO = V_ASI_EJERCICIO;
      
        INSERT INTO CNT_ASIENTO
          (ASI_CLAVE,
           ASI_EMPR,
           ASI_EJERCICIO,
           ASI_NRO,
           ASI_FEC,
           ASI_OBS,
           ASI_FEC_GRAB,
           ASI_LOGIN,
           ASI_SIST,
           ASI_NRO_LISTADO_AUD)
        VALUES
          (V_ASI_CLAVE,
           P_EMPRESA,
           V_ASI_EJERCICIO,
           NVL(V_ASI_NRO, 0) + 1,
           P_FECHA_FIN,
           'ASIENTO DE FINAN. COSTO EXPORT',
           SYSDATE,
           GEN_DEVUELVE_USER,
           'FIN',
           NULL);
      
        BAND := 1;
      END IF;
    
      --EMBARQUE PRECIO DE FACTURA
      V_ASID_ITEM := V_ASID_ITEM + 1;
      INSERT INTO CNT_ASIENTO_DET
        (ASID_CLAVE_ASI,
         ASID_ITEM,
         ASID_CLAVE_CTACO,
         ASID_IND_DB_CR,
         ASID_IMPORTE,
         ASID_TIPO_MOV,
         ASID_NRO_DOC,
         ASID_DESC,
         ASID_CANAL_BETA,
         ASID_EMPR)
      VALUES
        (V_ASI_CLAVE,
         V_ASID_ITEM,
         V.CLAS_CTACO_FACT_A_EMBARCAR,
         'D',
         V.IMP_VENTA,
         NULL,
         NULL,
         V.OBS,
         NULL,
         P_EMPRESA);
    
      V_ASID_ITEM := V_ASID_ITEM + 1;
      INSERT INTO CNT_ASIENTO_DET
        (ASID_CLAVE_ASI,
         ASID_ITEM,
         ASID_CLAVE_CTACO,
         ASID_IND_DB_CR,
         ASID_IMPORTE,
         ASID_TIPO_MOV,
         ASID_NRO_DOC,
         ASID_DESC,
         ASID_CANAL_BETA,
         ASID_EMPR)
      VALUES
        (V_ASI_CLAVE,
         V_ASID_ITEM,
         V.CLAS_CTACO_EXPORTACION,
         'C',
         V.IMP_VENTA,
         NULL,
         NULL,
         V.OBS,
         NULL,
         P_EMPRESA);
      --EMBARQUE COSTO
      V_ASID_ITEM := V_ASID_ITEM + 1;
      INSERT INTO CNT_ASIENTO_DET
        (ASID_CLAVE_ASI,
         ASID_ITEM,
         ASID_CLAVE_CTACO,
         ASID_IND_DB_CR,
         ASID_IMPORTE,
         ASID_TIPO_MOV,
         ASID_NRO_DOC,
         ASID_DESC,
         ASID_CANAL_BETA,
         ASID_EMPR)
      VALUES
        (V_ASI_CLAVE,
         V_ASID_ITEM,
         V.CLAS_CTACO_COSTO_EXPORTACION,
         'D',
         V.IMP_COSTO,
         NULL,
         NULL,
         V.OBS,
         NULL,
         P_EMPRESA);
    
      V_ASID_ITEM := V_ASID_ITEM + 1;
      INSERT INTO CNT_ASIENTO_DET
        (ASID_CLAVE_ASI,
         ASID_ITEM,
         ASID_CLAVE_CTACO,
         ASID_IND_DB_CR,
         ASID_IMPORTE,
         ASID_TIPO_MOV,
         ASID_NRO_DOC,
         ASID_DESC,
         ASID_CANAL_BETA,
         ASID_EMPR)
      VALUES
        (V_ASI_CLAVE,
         V_ASID_ITEM,
         V.CLAS_CTACO_MERCADERIA,
         'C',
         V.IMP_COSTO,
         NULL,
         NULL,
         V.OBS,
         NULL,
         P_EMPRESA);
    
    END LOOP;
  
  END;

  PROCEDURE PP_ACTUALIZAR_CNT_ASIENTO_IP(P_EMPRESA      IN NUMBER,
                                         P_FECHA_INICIO IN DATE,
                                         P_FECHA_FIN    IN DATE,
                                         P_DIFERENCIA   IN VARCHAR2,
                                         P_IPS_PATRONAL IN NUMBER,
                                         
                                         P_SESSION IN NUMBER) AS
  
    V_SQL                VARCHAR2(3000);
    V_CLAVE              NUMBER;
    V_ITEM               NUMBER := 0;
    V_EJ_CODIGO          NUMBER;
    V_NRO                NUMBER;
    V_IPS_PATRONAL_FINAL NUMBER;
    TYPE CURTYP IS REF CURSOR;
    C_CNT_ASIENTO_IP CURTYP;
    TYPE CUR_TYPE IS RECORD(
      ASID_CLAVE_CTACO NUMBER(10),
      ASID_IMPORTE     NUMBER(18, 2),
      ASID_CANAL_BETA  NUMBER(6));
  
    C CUR_TYPE;
  
  BEGIN
  
    SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_CLAVE FROM DUAL;
  
    SELECT EJ_CODIGO
      INTO V_EJ_CODIGO
      FROM CNT_EJERCICIO
     WHERE P_FECHA_FIN BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
       AND EJ_EMPR = P_EMPRESA;
  
    SELECT NVL(MAX(ASI_NRO), 0) + 1
      INTO V_NRO
      FROM CNT_ASIENTO
     WHERE ASI_EJERCICIO = V_EJ_CODIGO
       AND ASI_EMPR = P_EMPRESA;
  
    INSERT INTO CNT_ASIENTO
      (ASI_CLAVE,
       ASI_EMPR,
       ASI_EJERCICIO,
       ASI_NRO,
       ASI_FEC,
       ASI_OBS,
       ASI_FEC_GRAB,
       ASI_LOGIN,
       ASI_SIST,
       ASI_NRO_LISTADO_AUD)
    VALUES
      (V_CLAVE,
       P_EMPRESA,
       V_EJ_CODIGO,
       V_NRO,
       P_FECHA_FIN,
       'APORTE PATRONAL IPS-' || TO_CHAR(P_FECHA_FIN, 'MONTH'),
       SYSDATE,
       GEN_DEVUELVE_USER,
       'SIG',
       NULL);
  
    V_SQL := 'SELECT
                 CUENTA_CLAVE,
                 IMP_CANAL_IPS_PATRONAL  +  ((IMP_CANAL_IPS_PATRONAL /' ||
             P_IPS_PATRONAL || ' )  * 100) /100 * ' ||
             NVL(P_DIFERENCIA, 'null') ||
             ' TOTAL ,
                 CANB_CODIGO
              FROM CNT103_TEMP T
              WHERE  T.SESION_ID =  ' || P_SESSION || '
              AND T.EMPR = ' || P_EMPRESA || '';
  
    --INSERT INTO X (CAMPO1, OTRO) VALUES (V_SQL, 'marisa prueba');
    --COMMIT;
  
    OPEN C_CNT_ASIENTO_IP FOR V_SQL;
  
    LOOP
      ---
      FETCH C_CNT_ASIENTO_IP
      
        INTO C;
      EXIT WHEN C_CNT_ASIENTO_IP%NOTFOUND;
    
      IF C.ASID_IMPORTE > 0 THEN
        V_ITEM := V_ITEM + 1;
        INSERT INTO CNT_ASIENTO_DET
          (ASID_CLAVE_ASI,
           ASID_ITEM,
           ASID_CLAVE_CTACO,
           ASID_IND_DB_CR,
           ASID_IMPORTE,
           ASID_TIPO_MOV,
           ASID_NRO_DOC,
           ASID_DESC,
           ASID_CANAL_BETA,
           ASID_EMPR)
        VALUES
          (V_CLAVE,
           V_ITEM,
           C.ASID_CLAVE_CTACO,
           'D',
           C.ASID_IMPORTE,
           NULL,
           NULL,
           NULL,
           C.ASID_CANAL_BETA,
           P_EMPRESA);
      END IF;
    
    END LOOP;
  
    BEGIN
      SELECT SUM(IMP_CANAL_IPS_PATRONAL +
                 ((IMP_CANAL_IPS_PATRONAL / 439963579) * 100) / 100 *
                 P_DIFERENCIA) TOTAL
        INTO V_IPS_PATRONAL_FINAL
        FROM CNT103_TEMP T
       WHERE T.SESION_ID = P_SESSION
         AND T.EMPR = P_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_IPS_PATRONAL_FINAL := NULL;
    END;
  
    V_ITEM := V_ITEM + 1;
    INSERT INTO CNT_ASIENTO_DET
      (ASID_CLAVE_ASI,
       ASID_ITEM,
       ASID_CLAVE_CTACO,
       ASID_IND_DB_CR,
       ASID_IMPORTE,
       ASID_TIPO_MOV,
       ASID_NRO_DOC,
       ASID_DESC,
       ASID_CANAL_BETA,
       ASID_EMPR)
    VALUES
      (V_CLAVE,
       V_ITEM,
       120,
       'C',
       V_IPS_PATRONAL_FINAL,
       NULL,
       NULL,
       NULL,
       NULL,
       P_EMPRESA);
  
    -- INSERT INTO X (CAMPO1, OTRO) VALUES (V_SQL, 'CTA_BCO');
    --COMMIT;
  END;

  PROCEDURE STK_ASIENTO_DIF_INVENTARIO(P_EMPRESA     IN NUMBER,
                                       P_FECHA_DESDE IN DATE,
                                       P_FECHA_HASTA IN DATE,
                                       P_COD_PERIODO IN NUMBER,
                                       P_LOGIN       IN VARCHAR2) AS
  
    V_ASI_CLAVE     NUMBER;
    V_ASI_EJERCICIO NUMBER;
    V_ASI_NRO       NUMBER;
    V_NRO_ITEM      NUMBER := 0;
    SALIR EXCEPTION;
    V_FEC_INI      DATE;
    V_FEC_FIN      DATE;
    V_IND_SIN_CLAS NUMBER := 0;
  
    CURSOR OPERACIONES IS
      SELECT COUNT(OPER_CODIGO) EXISTE
        FROM STK_OPERACION A, STK_DOCUMENTO B
       WHERE A.OPER_EMPR = P_EMPRESA
         AND OPER_CODIGO = DOCU_CODIGO_OPER
         AND OPER_EMPR = DOCU_EMPR
         AND DOCU_FEC_EMIS BETWEEN V_FEC_INI AND V_FEC_FIN
         AND OPER_DESC IN ('DIF_MEN', 'DIF_MAS');
  
    CURSOR MOVIMIENTO_STK IS
      SELECT A.OPER_CTACO_MOVIMIENTO CTA_GAN_PERD,
             CASE
               WHEN V_FEC_INI <= '31/07/2021' THEN
                CLAS_CTACO_MERCADERIA
               ELSE
                MARC_CTA_MERC
             END CTA_MERCADERIA,
             CLAS_DESC CLASIFICACION,
             CASE
               WHEN OPER_DESC = 'DIF_MAS' THEN
                'C'
               WHEN OPER_DESC = 'DIF_MEN' THEN
                'D'
             END TIPO_DB_CR,
             OPER_DESC,
             ROUND(SUM(CD_COSTO_PROM_LOC * DETA_CANT), 0) COSTO_LOC
        FROM STK_OPERACION     A,
             STK_COSTO_DIARIO  B,
             STK_DOCUMENTO     C,
             STK_DOCUMENTO_DET D,
             STK_ARTICULO      E,
             STK_CLASIFICACION F,
             CNT_CUENTA        G,
             STK_LINEA         H,
             stk_marca         j
       WHERE OPER_CODIGO = DOCU_CODIGO_OPER
         AND OPER_EMPR = DOCU_EMPR
            
         AND ART_LINEA = LIN_CODIGO
         AND ART_EMPR = LIN_EMPR
            
         and art_marca = marc_codigo
         and art_empr = marc_empr
            
         AND E.ART_CLASIFICACION = CLAS_CODIGO(+)
         AND E.ART_EMPR = CLAS_EMPR(+)
            
         AND F.CLAS_CTACO_COSTO = CTAC_CLAVE(+)
         AND F.CLAS_EMPR = CTAC_EMPR(+)
            
         AND DETA_ART = CD_ART
         AND DETA_EMPR = CD_EMPR
            
         AND DOCU_FEC_EMIS = CD_FEC
         AND DOCU_EMPR = CD_EMPR
            
         AND DOCU_CLAVE = DETA_CLAVE_DOC
         AND DOCU_EMPR = DETA_EMPR
            
         AND DETA_ART = ART_CODIGO
         AND DETA_EMPR = ART_EMPR
            
         AND DOCU_CLAVE_FIN IS NULL
            
         AND ART_TIPO <> 4
         AND NVL(OPER_ENT_SAL, 'N') <> 'N'
            
         AND DOCU_FEC_EMIS BETWEEN V_FEC_INI AND V_FEC_FIN
         AND DOCU_EMPR = 1
            
         AND A.OPER_DESC IN ('DIF_MAS', 'DIF_MEN')
       GROUP BY F.CLAS_CTACO_COSTO,
                CLAS_CTACO_MERCADERIA,
                j.marc_cta_merc,
                CLAS_DESC,
                OPER_DESC,
                A.OPER_CTACO_MOVIMIENTO;
  
    CURSOR ART_SIN_CLAS IS
    
      SELECT A.OPER_CTACO_MOVIMIENTO CTA_GAN_PERD,
             CASE
               WHEN V_FEC_INI <= '31/07/2021' THEN
                CLAS_CTACO_MERCADERIA
               ELSE
                MARC_CTA_MERC
             END CTA_MERCADERIA,
             CLAS_DESC CLASIFICACION,
             CASE
               WHEN OPER_DESC = 'DIF_MAS' THEN
                'C'
               WHEN OPER_DESC = 'DIF_MEN' THEN
                'D'
             END TIPO_DB_CR,
             OPER_DESC,
             SUM(CD_COSTO_PROM_LOC * DETA_CANT) COSTO_LOC
        FROM STK_OPERACION     A,
             STK_COSTO_DIARIO  B,
             STK_DOCUMENTO     C,
             STK_DOCUMENTO_DET D,
             STK_ARTICULO      E,
             STK_CLASIFICACION F,
             CNT_CUENTA        G,
             STK_LINEA         H,
             stk_marca         i
       WHERE OPER_CODIGO = DOCU_CODIGO_OPER
         AND OPER_EMPR = DOCU_EMPR
            
         AND ART_LINEA = LIN_CODIGO
         AND ART_EMPR = LIN_EMPR
            
         and art_marca = marc_codigo
         and art_empr = marc_empr
            
         AND E.ART_CLASIFICACION = CLAS_CODIGO(+)
         AND E.ART_EMPR = CLAS_EMPR(+)
            
         AND F.CLAS_CTACO_COSTO = CTAC_CLAVE(+)
         AND F.CLAS_EMPR = CTAC_EMPR(+)
            
         AND DETA_ART = CD_ART
         AND DETA_EMPR = CD_EMPR
            
         AND DOCU_FEC_EMIS = CD_FEC
         AND DOCU_EMPR = CD_EMPR
            
         AND DOCU_CLAVE = DETA_CLAVE_DOC
         AND DOCU_EMPR = DETA_EMPR
            
         AND DETA_ART = ART_CODIGO
         AND DETA_EMPR = ART_EMPR
            
         AND DOCU_CLAVE_FIN IS NULL
            
         AND ART_TIPO <> 4
         AND NVL(OPER_ENT_SAL, 'N') <> 'N'
            
         AND CLAS_CODIGO IS NULL
         AND DOCU_FEC_EMIS BETWEEN V_FEC_INI AND V_FEC_FIN
         AND DOCU_EMPR = 1
            
         AND A.OPER_DESC IN ('DIF_MAS', 'DIF_MEN')
       GROUP BY F.CLAS_CTACO_COSTO,
                CLAS_CTACO_MERCADERIA,
                marc_cta_merc,
                CLAS_DESC,
                OPER_DESC,
                A.OPER_CTACO_MOVIMIENTO;
  
  BEGIN
  
    --==ESTO AGREGUE DE NUEVO POR QUE TENIA ALGUNOS PROBLEMA CON LA FECHA
    SELECT PERI_FEC_INI, PERI_FEC_FIN
      INTO V_FEC_INI, V_FEC_FIN
      FROM FIN_PERIODO P
     WHERE P.PERI_EMPR = P_EMPRESA
       AND PERI_CODIGO = P_COD_PERIODO;
    --=========================================================
    FOR V IN ART_SIN_CLAS LOOP
      V_IND_SIN_CLAS := V_IND_SIN_CLAS + 1;
    END LOOP;
    IF V_IND_SIN_CLAS > 0 THEN
      RAISE_APPLICATION_ERROR(-20000,
                              'Existen: ' || V_IND_SIN_CLAS ||
                              ' articulos sin clasificacion, debe asignar primero!');
    END IF;
  
    IF GEN_PACK_SUC.GEN_CONF_IND_SUC = 'S' THEN
      RAISE SALIR;
    END IF;
  
    ---------------------------------------------------------------------------------------------------
    --ASIENTO EN MONEDA LOCAL (GS)
    ---------------------------------------------------------------------------------------------------
  
    SELECT EJ_CODIGO
      INTO V_ASI_EJERCICIO
      FROM CNT_EJERCICIO
     WHERE V_FEC_INI BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
       AND EJ_EMPR = P_EMPRESA;
  
    FOR R IN OPERACIONES LOOP
    
      DELETE CNT_ASIENTO_DET
       WHERE ASID_EMPR = P_EMPRESA
         AND ASID_CLAVE_ASI IN
             (SELECT ASI_CLAVE
                FROM CNT_ASIENTO
               WHERE CNT_ASIENTO.ASI_SIST = 'STK'
                 AND ASI_FEC BETWEEN V_FEC_INI AND V_FEC_FIN
                 AND UPPER(ASI_OBS) LIKE '%ASIENTO DE DIF. INVENTARIO%'
                 AND ASI_EMPR = P_EMPRESA);
      DELETE FROM CNT_ASIENTO
       WHERE CNT_ASIENTO.ASI_SIST = 'STK'
         AND ASI_FEC BETWEEN V_FEC_INI AND V_FEC_FIN
         AND UPPER(ASI_OBS) LIKE '%ASIENTO DE DIF. INVENTARIO%'
         AND ASI_EMPR = P_EMPRESA;
    
      SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_ASI_CLAVE FROM DUAL;
      SELECT NVL(MAX(A.ASI_NRO), 0) + 1
        INTO V_ASI_NRO
        FROM CNT_ASIENTO A
       WHERE A.ASI_EJERCICIO = V_ASI_EJERCICIO
         AND ASI_EMPR = P_EMPRESA;
    
      INSERT INTO CNT_ASIENTO --COSTOS EN MONEDA LOCAL.
        (ASI_CLAVE,
         ASI_EMPR,
         ASI_EJERCICIO,
         ASI_NRO,
         ASI_FEC,
         ASI_OBS,
         ASI_FEC_GRAB,
         ASI_LOGIN,
         ASI_SIST,
         ASI_NRO_LISTADO_AUD)
      VALUES
        (V_ASI_CLAVE,
         P_EMPRESA,
         V_ASI_EJERCICIO,
         V_ASI_NRO,
         V_FEC_FIN,
         'ASIENTO DE DIF. INVENTARIO ' ||
         UPPER(REPLACE(TO_CHAR(V_FEC_INI, 'Month-YYYY'), ' ', '')),
         SYSDATE,
         P_LOGIN,
         'STK',
         NULL);
      ---
      V_NRO_ITEM := 0;
    
      FOR V IN MOVIMIENTO_STK LOOP
      
        V_NRO_ITEM := V_NRO_ITEM + 1;
      
        BEGIN
          INSERT INTO CNT_ASIENTO_DET
            (ASID_CLAVE_ASI,
             ASID_ITEM,
             ASID_CLAVE_CTACO,
             ASID_IND_DB_CR,
             ASID_IMPORTE,
             ASID_TIPO_MOV,
             ASID_NRO_DOC,
             ASID_DESC,
             ASID_EMPR)
          VALUES
            (V_ASI_CLAVE,
             V_NRO_ITEM,
             V.CTA_GAN_PERD,
             V.TIPO_DB_CR,
             V.COSTO_LOC,
             NULL,
             NULL,
             CASE WHEN V.OPER_DESC = 'DIF_MAS' THEN
             'SOBRANTE DE ' || V.CLASIFICACION ELSE
             'PERDIDA DE ' || V.CLASIFICACION END,
             P_EMPRESA);
        
          IF V.COSTO_LOC >= 0 THEN
            V_NRO_ITEM := V_NRO_ITEM + 1;
            INSERT INTO CNT_ASIENTO_DET
              (ASID_CLAVE_ASI,
               ASID_ITEM,
               ASID_CLAVE_CTACO,
               ASID_IND_DB_CR,
               ASID_IMPORTE,
               ASID_TIPO_MOV,
               ASID_NRO_DOC,
               ASID_DESC,
               ASID_EMPR)
            VALUES
              (V_ASI_CLAVE,
               V_NRO_ITEM,
               V.CTA_MERCADERIA,
               CASE WHEN V.TIPO_DB_CR = 'C' THEN 'D' ELSE 'C' END,
               V.COSTO_LOC,
               NULL,
               NULL,
               NULL,
               P_EMPRESA);
          
          END IF;
        EXCEPTION
          WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR(-20000, SQLERRM);
        END;
      END LOOP;
    
    END LOOP;
    --   COMMIT;
  EXCEPTION
    WHEN SALIR THEN
      NULL;
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20000, SQLERRM);
  END;

  PROCEDURE STK_ASIENTO_COSTO_VENTA(P_EMPRESA     IN NUMBER,
                                    P_FECHA_DESDE IN DATE,
                                    P_FECHA_HASTA IN DATE,
                                    P_COD_PERIODO IN NUMBER,
                                    P_LOGIN       IN VARCHAR2) AS
  
    V_ASI_CLAVE     NUMBER;
    V_ASI_EJERCICIO NUMBER;
    V_ASI_NRO       NUMBER;
    V_NRO_ITEM      NUMBER := 0;
    SALIR EXCEPTION;
    V_FEC_INI DATE;
    V_FEC_FIN DATE;
    CURSOR COSTO_VENTAS_TAGRO IS
      SELECT H.ART_MARCA,
             H.MARC_DESC,
             SUM(ROUND(H.COSTO_GS)) COSTO_LOC,
             SUM(ROUND(H.COSTO_US, 2)) COSTO_MON,
             MARC_CTA_COSTO CUENTA_DB,
             MARC_CTA_MERC CUENTA_CR
        FROM STK_ARTICULO J,
             STK_ART_EMPR_PERI,
             (SELECT DOCU_EMPR,
                     EMPR_RAZON_SOCIAL,
                     ART_MARCA,
                     MARC_DESC,
                     MARC_CTA_COSTO,
                     MARC_CTA_MERC,
                     ART_LINEA,
                     SL.LIN_DESC,
                     ART_LINEA_NEGOCIO,
                     FL.LIN_DESC LIN_NEG_DESC,
                     ART_GRUPO,
                     GRUP_DESC,
                     DETA_ART,
                     DETA_ART ARTICULO,
                     ART_DESC,
                     ART_UNID_MED,
                     DOCU_FEC_EMIS,
                     DETA_CLAVE_DOC, /*ST.CDET_CLAVE_DOC*/
                     SUM(DECODE(OPER_DESC,
                                'DEV_VENTA',
                                (DETA_CANT * -1),
                                DETA_CANT)) CANTIDAD,
                     SUM(DECODE(OPER_DESC,
                                'DEV_VENTA',
                                DETA_CANT * AEP_COSTO_PROM_LOC /*ST.CDET_COSTO_PROM_LOC*/
                                * -1,
                                DETA_CANT * AEP_COSTO_PROM_LOC /*ST.CDET_COSTO_PROM_LOC*/)) COSTO_GS,
                     SUM(AEP_COSTO_PROM_LOC /*ST.CDET_COSTO_PROM_LOC*/) COSTO_UNIT_GS,
                     SUM(AEP_COSTO_PROM_MON /*ST.CDET_COSTO_PROM_MON*/) COSTO_UNIT_US,
                     SUM(DECODE(OPER_DESC,
                                'DEV_VENTA',
                                DETA_CANT * AEP_COSTO_PROM_MON /*ST.CDET_COSTO_PROM_MON*/
                                * -1,
                                DETA_CANT * AEP_COSTO_PROM_MON /*ST.CDET_COSTO_PROM_MON*/)) COSTO_US,
                     SUM(DECODE(OPER_DESC,
                                'DEV_VENTA',
                                DETA_IMP_NETO_LOC * -1,
                                DETA_IMP_NETO_LOC)) VENTA_GS,
                     SUM(DECODE(OPER_DESC,
                                'DEV_VENTA',
                                (DETA_IMP_NETO_LOC / DOCU_TASA_US) * -1,
                                DETA_IMP_NETO_LOC / DOCU_TASA_US)) VENTA_US
                FROM STK_MARCA,
                     STK_LINEA         SL,
                     STK_GRUPO,
                     STK_ARTICULO,
                     GEN_EMPRESA,
                     STK_ART_EMPR_PERI,
                     STK_OPERACION,
                     STK_DOCUMENTO,
                     STK_DOCUMENTO_DET,
                     FAC_LINEA_NEGOCIO FL /*,
                                                                                                                                                                                          STK_COSTOS_DET ST*/
               WHERE DOCU_CLAVE = DETA_CLAVE_DOC
                 AND ART_EMPR = DETA_EMPR
                 AND OPER_EMPR = DOCU_EMPR
                 AND FL.LIN_EMPR(+) = ART_EMPR
                 AND EMPR_CODIGO = DOCU_EMPR
                 AND ART_CODIGO = DETA_ART
                 AND MARC_CODIGO = ART_MARCA
                 AND SL.LIN_CODIGO = ART_LINEA
                 AND GRUP_EMPR = ART_EMPR
                 AND SL.LIN_EMPR = ART_EMPR
                 AND SL.LIN_EMPR = GRUP_EMPR
                 AND MARC_EMPR = ART_EMPR
                 AND FL.LIN_CODIGO(+) = ART_LINEA_NEGOCIO
                 AND GRUP_LINEA = ART_LINEA
                 AND GRUP_CODIGO = ART_GRUPO
                    /*AND ST.CDET_CLAVE_DOC = DETA_CLAVE_DOC
                    AND ST.CDET_ART = DETA_ART*/
                 AND AEP_PERIODO = P_COD_PERIODO
                 AND AEP_EMPR = DETA_EMPR
                 AND DOCU_EMPR = P_EMPRESA
                 AND AEP_ART = DETA_ART
                 AND OPER_CODIGO = DOCU_CODIGO_OPER
                 AND (OPER_DESC = 'VENTA')
                 AND ART_TIPO <> 4
                 AND DOCU_FEC_EMIS BETWEEN P_FECHA_DESDE AND P_FECHA_HASTA
               GROUP BY DOCU_EMPR,
                        EMPR_RAZON_SOCIAL,
                        ART_MARCA,
                        MARC_DESC,
                        ART_LINEA,
                        SL.LIN_DESC,
                        ART_GRUPO,
                        GRUP_DESC,
                        DETA_ART,
                        ART_DESC,
                        ART_UNID_MED,
                        DOCU_FEC_EMIS,
                        DETA_CLAVE_DOC,
                        ART_LINEA_NEGOCIO,
                        FL.LIN_DESC,
                        MARC_CTA_COSTO,
                        MARC_CTA_MERC
              
              UNION
              
              SELECT DOCU_EMPR,
                     EMPR_RAZON_SOCIAL,
                     ART_MARCA,
                     MARC_DESC,
                     MARC_CTA_COSTO,
                     MARC_CTA_MERC,
                     ART_LINEA,
                     SL.LIN_DESC,
                     ART_LINEA_NEGOCIO,
                     FL.LIN_DESC LIN_NEG_DESC,
                     ART_GRUPO,
                     GRUP_DESC,
                     DETA_ART,
                     DETA_ART ARTICULO,
                     ART_DESC,
                     ART_UNID_MED,
                     DOCU_FEC_EMIS,
                     DETA_CLAVE_DOC, /*ST.CDET_CLAVE_DOC*/
                     SUM(DECODE(OPER_DESC,
                                'DEV_VENTA',
                                (DETA_CANT * -1),
                                DETA_CANT)) CANTIDAD,
                     SUM(DECODE(OPER_DESC,
                                'DEV_VENTA',
                                DETA_CANT * AEP_COSTO_PROM_LOC /*ST.CDET_COSTO_PROM_LOC*/
                                * -1,
                                DETA_CANT * AEP_COSTO_PROM_LOC /*ST.CDET_COSTO_PROM_LOC*/)) COSTO_GS,
                     SUM(AEP_COSTO_PROM_LOC /*ST.CDET_COSTO_PROM_LOC*/) COSTO_UNIT_GS,
                     SUM(AEP_COSTO_PROM_MON /*ST.CDET_COSTO_PROM_MON*/) COSTO_UNIT_US,
                     SUM(DECODE(OPER_DESC,
                                'DEV_VENTA',
                                DETA_CANT * AEP_COSTO_PROM_MON /*ST.CDET_COSTO_PROM_MON*/
                                * -1,
                                DETA_CANT * AEP_COSTO_PROM_MON /*ST.CDET_COSTO_PROM_MON*/)) COSTO_US,
                     SUM(DECODE(OPER_DESC,
                                'DEV_VENTA',
                                DETA_IMP_NETO_LOC * -1,
                                DETA_IMP_NETO_LOC)) VENTA_GS,
                     SUM(DECODE(OPER_DESC,
                                'DEV_VENTA',
                                (DETA_IMP_NETO_LOC / DOCU_TASA_US) * -1,
                                DETA_IMP_NETO_LOC / DOCU_TASA_US)) VENTA_US
                FROM STK_MARCA,
                     STK_LINEA         SL,
                     STK_GRUPO,
                     STK_ARTICULO,
                     GEN_EMPRESA,
                     STK_ART_EMPR_PERI,
                     FAC_LINEA_NEGOCIO FL,
                     STK_OPERACION,
                     STK_DOCUMENTO,
                     STK_DOCUMENTO_DET /*,
                                                                                                                                                                                          STK_COSTOS_DET ST*/
               WHERE DOCU_CLAVE = DETA_CLAVE_DOC
                 AND EMPR_CODIGO = DOCU_EMPR
                 AND ART_EMPR = DETA_EMPR
                 AND GRUP_EMPR = ART_EMPR
                 AND SL.LIN_EMPR = ART_EMPR
                 AND SL.LIN_EMPR = GRUP_EMPR
                 AND OPER_EMPR = DOCU_EMPR
                 AND FL.LIN_EMPR(+) = ART_EMPR
                 AND EMPR_CODIGO = DOCU_EMPR
                 AND ART_CODIGO = DETA_ART
                 AND MARC_CODIGO = ART_MARCA
                 AND SL.LIN_CODIGO = ART_LINEA
                 AND GRUP_LINEA = ART_LINEA
                 AND GRUP_CODIGO = ART_GRUPO
                 AND DOCU_EMPR = P_EMPRESA
                 AND FL.LIN_CODIGO(+) = ART_LINEA_NEGOCIO
                    /*AND ST.CDET_CLAVE_DOC = DETA_CLAVE_DOC
                    AND ST.CDET_ART = DETA_ART*/
                 AND AEP_PERIODO = P_COD_PERIODO
                 AND AEP_EMPR = DETA_EMPR
                 AND AEP_ART = DETA_ART
                 AND OPER_CODIGO = DOCU_CODIGO_OPER
                 AND (OPER_DESC = 'DEV_VENTA')
                    
                 AND DOCU_FEC_EMIS BETWEEN P_FECHA_DESDE AND P_FECHA_HASTA
               GROUP BY DOCU_EMPR,
                        EMPR_RAZON_SOCIAL,
                        ART_MARCA,
                        MARC_DESC,
                        MARC_CTA_COSTO,
                        MARC_CTA_MERC,
                        ART_LINEA,
                        SL.LIN_DESC,
                        ART_GRUPO,
                        GRUP_DESC,
                        DETA_ART,
                        ART_DESC,
                        ART_UNID_MED,
                        DOCU_FEC_EMIS,
                        DETA_CLAVE_DOC,
                        ART_LINEA_NEGOCIO,
                        FL.LIN_DESC,
                        MARC_CTA_COSTO,
                        MARC_CTA_MERC) H
       WHERE J.ART_CODIGO = H.ARTICULO
         AND AEP_ART = J.ART_CODIGO
         AND AEP_PERIODO = P_COD_PERIODO
         AND H.DOCU_EMPR = J.ART_EMPR
         AND J.ART_EMPR = AEP_EMPR
         AND (MARC_CTA_COSTO IS NOT NULL AND MARC_CTA_MERC IS NOT NULL)
       GROUP BY H.ART_MARCA, H.MARC_DESC, MARC_CTA_COSTO, MARC_CTA_MERC;
  
    CURSOR COSTO_VENTAS IS
      SELECT F.CLAS_CTACO_COSTO CTA_COSTO,
             CASE
               WHEN V_FEC_INI <= '31/07/2021' THEN
                CLAS_CTACO_MERCADERIA
               ELSE
                MARC_CTA_MERC
             END CTA_MERCADERIA,
             CLAS_DESC CLASIFICACION,
             ROUND(SUM(DECODE(OPER_DESC,
                              'VENTA',
                              (CD_COSTO_PROM_LOC * DETA_CANT),
                              'DEV_VENTA',
                              ((CD_COSTO_PROM_LOC * DETA_CANT) * (-1)),
                              'ANUL_VENTA',
                              (CD_COSTO_PROM_LOC * DETA_CANT) * (-1))) /*+                                                                                                                 END),
                                                                                                                                                                                                                                                   0)*/,
                   0) COSTO_LOC
        FROM STK_OPERACION A,
             STK_COSTO_DIARIO B,
             STK_DOCUMENTO C,
             STK_DOCUMENTO_DET D,
             STK_ARTICULO E,
             STK_CLASIFICACION F,
             CNT_CUENTA G,
             STK_LINEA H,
             (SELECT DOCU_CLAVE_PADRE ANUL_CLAVE, DOCU_EMPR ANUL_EMPR
                FROM STK_DOCUMENTO
               WHERE DOCU_CODIGO_OPER IN (14, 34)) ANUL,
             STK_MARCA J
       WHERE OPER_CODIGO = DOCU_CODIGO_OPER
         AND OPER_EMPR = DOCU_EMPR
            
         AND ART_MARCA = MARC_CODIGO
         AND ART_EMPR = MARC_EMPR
            
         AND ART_LINEA = LIN_CODIGO
         AND ART_EMPR = LIN_EMPR
            
         AND E.ART_CLASIFICACION = CLAS_CODIGO(+)
         AND E.ART_EMPR = CLAS_EMPR(+)
            
         AND F.CLAS_CTACO_COSTO = CTAC_CLAVE(+)
         AND F.CLAS_EMPR = CTAC_EMPR(+)
            
         AND DETA_ART = CD_ART
         AND DETA_EMPR = CD_EMPR
            
         AND DOCU_FEC_EMIS = CD_FEC
         AND DOCU_EMPR = CD_EMPR
            
         AND DOCU_CLAVE = DETA_CLAVE_DOC
         AND DOCU_EMPR = DETA_EMPR
            
         AND DETA_ART = ART_CODIGO
         AND DETA_EMPR = ART_EMPR
            
         AND DOCU_CLAVE_FIN IS NULL
            
         AND DOCU_CLAVE = ANUL_CLAVE(+)
         AND DOCU_EMPR = ANUL_EMPR(+)
         AND ANUL_CLAVE IS NULL
            
         AND ART_TIPO <> 4
         AND NVL(OPER_ENT_SAL, 'N') <> 'N'
            
         AND DOCU_FEC_EMIS BETWEEN V_FEC_INI AND V_FEC_FIN
         AND DOCU_EMPR = P_EMPRESA
            
         AND A.OPER_DESC IN ('VENTA', 'DEV_VENTA')
       GROUP BY F.CLAS_CTACO_COSTO,
                marc_cta_merc,
                CLAS_CTACO_MERCADERIA,
                CLAS_DESC,
                CLAS_CODIGO,
                CLAS_CODIGO,
                TO_CHAR(DOCU_FEC_EMIS, 'MM/YYYY'),
                TO_CHAR(DOCU_FEC_EMIS, 'MM'),
                TO_CHAR(DOCU_FEC_EMIS, 'YYYY');
  
  BEGIN
  
    --==ESTO AGREGUE DE NUEVO POR QUE TENIA ALGUNOS PROBLEMA CON LA FECHA
    SELECT PERI_FEC_INI, PERI_FEC_FIN
      INTO V_FEC_INI, V_FEC_FIN
      FROM FIN_PERIODO P
     WHERE P.PERI_EMPR = P_EMPRESA
       AND PERI_CODIGO = P_COD_PERIODO;
    --=========================================================
  
    IF GEN_PACK_SUC.GEN_CONF_IND_SUC = 'S' THEN
      RAISE SALIR;
    END IF;
  
    IF P_EMPRESA = 2 THEN
    
      DELETE CNT_ASIENTO_DET
       WHERE ASID_EMPR = P_EMPRESA
         AND ASID_CLAVE_ASI IN
             (SELECT ASI_CLAVE
                FROM CNT_ASIENTO
               WHERE CNT_ASIENTO.ASI_SIST = 'STK'
                 AND ASI_FEC BETWEEN P_FECHA_DESDE AND P_FECHA_HASTA
                 AND UPPER(ASI_OBS) LIKE '%ASIENTO DE COSTOS%'
                 AND ASI_EMPR = P_EMPRESA);
    
      DELETE FROM CNT_ASIENTO
       WHERE CNT_ASIENTO.ASI_SIST = 'STK'
         AND ASI_FEC BETWEEN P_FECHA_DESDE AND P_FECHA_HASTA
         AND UPPER(ASI_OBS) LIKE '%ASIENTO DE COSTOS%'
         AND ASI_EMPR = P_EMPRESA;
      ---------------------------------------------------------------------------------------------------
      --ASIENTO EN MONEDA LOCAL (GS)
      ---------------------------------------------------------------------------------------------------
      SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_ASI_CLAVE FROM DUAL;
    
      SELECT EJ_CODIGO
        INTO V_ASI_EJERCICIO
        FROM CNT_EJERCICIO
       WHERE P_FECHA_HASTA BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
         AND EJ_EMPR = P_EMPRESA;
    
      SELECT NVL(MAX(A.ASI_NRO), 0) + 1
        INTO V_ASI_NRO
        FROM CNT_ASIENTO A
       WHERE A.ASI_EJERCICIO = V_ASI_EJERCICIO
         AND ASI_EMPR = P_EMPRESA;
    
      INSERT INTO CNT_ASIENTO --COSTOS EN MONEDA LOCAL.
        (ASI_CLAVE,
         ASI_EMPR,
         ASI_EJERCICIO,
         ASI_NRO,
         ASI_FEC,
         ASI_OBS,
         ASI_FEC_GRAB,
         ASI_LOGIN,
         ASI_SIST,
         ASI_NRO_LISTADO_AUD,
         ASI_MON)
      VALUES
        (V_ASI_CLAVE,
         P_EMPRESA,
         V_ASI_EJERCICIO,
         V_ASI_NRO,
         P_FECHA_HASTA,
         'ASIENTO DE COSTOS ' ||
         UPPER(REPLACE(TO_CHAR(P_FECHA_HASTA, 'Month-YYYY'), ' ', '')) ||
         ' GS',
         SYSDATE,
         GEN_DEVUELVE_USER,
         'STK',
         NULL,
         1);
      ---
      V_NRO_ITEM := 0;
      FOR V IN COSTO_VENTAS_TAGRO LOOP
      
        V_NRO_ITEM := V_NRO_ITEM + 1;
      
        BEGIN
          INSERT INTO CNT_ASIENTO_DET
            (ASID_CLAVE_ASI,
             ASID_ITEM,
             ASID_CLAVE_CTACO,
             ASID_IND_DB_CR,
             ASID_IMPORTE,
             ASID_TIPO_MOV,
             ASID_NRO_DOC,
             ASID_DESC,
             ASID_IMPORTE_MON,
             ASID_EMPR)
          VALUES
            (V_ASI_CLAVE,
             V_NRO_ITEM,
             V.CUENTA_DB,
             'D',
             V.COSTO_LOC,
             NULL,
             NULL,
             NULL,
             NULL,
             P_EMPRESA);
        
          V_NRO_ITEM := V_NRO_ITEM + 1;
          INSERT INTO CNT_ASIENTO_DET
            (ASID_CLAVE_ASI,
             ASID_ITEM,
             ASID_CLAVE_CTACO,
             ASID_IND_DB_CR,
             ASID_IMPORTE,
             ASID_TIPO_MOV,
             ASID_NRO_DOC,
             ASID_DESC,
             ASID_IMPORTE_MON,
             ASID_EMPR)
          VALUES
            (V_ASI_CLAVE,
             V_NRO_ITEM,
             V.CUENTA_CR,
             'C',
             V.COSTO_LOC,
             NULL,
             NULL,
             NULL,
             NULL,
             P_EMPRESA);
        EXCEPTION
          WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR(-20000, SQLERRM);
        END;
      END LOOP;
    
      ---------------------------------------------------------------------------------------------------
      --ASIENTO EN MONEDA EXTRANJERA (USD)
      ---------------------------------------------------------------------------------------------------
      SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_ASI_CLAVE FROM DUAL;
    
      SELECT EJ_CODIGO
        INTO V_ASI_EJERCICIO
        FROM CNT_EJERCICIO
       WHERE P_FECHA_HASTA BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
         AND EJ_EMPR = P_EMPRESA;
    
      SELECT NVL(MAX(A.ASI_NRO), 0) + 1
        INTO V_ASI_NRO
        FROM CNT_ASIENTO A
       WHERE A.ASI_EJERCICIO = V_ASI_EJERCICIO
         AND ASI_EMPR = P_EMPRESA;
    
      INSERT INTO CNT_ASIENTO --COSTOS EN MONEDA EXTRANJERA.
        (ASI_CLAVE,
         ASI_EMPR,
         ASI_EJERCICIO,
         ASI_NRO,
         ASI_FEC,
         ASI_OBS,
         ASI_FEC_GRAB,
         ASI_LOGIN,
         ASI_SIST,
         ASI_NRO_LISTADO_AUD,
         ASI_MON)
      VALUES
        (V_ASI_CLAVE,
         P_EMPRESA,
         V_ASI_EJERCICIO,
         V_ASI_NRO,
         P_FECHA_HASTA,
         'ASIENTO DE COSTOS ' ||
         UPPER(REPLACE(TO_CHAR(P_FECHA_HASTA, 'Month-YYYY') || ' USD',
                       ' ',
                       '')),
         SYSDATE,
         GEN_DEVUELVE_USER,
         'STK',
         NULL,
         2);
      ---
      V_NRO_ITEM := 0;
      FOR V IN COSTO_VENTAS_TAGRO LOOP
      
        V_NRO_ITEM := V_NRO_ITEM + 1;
      
        BEGIN
          INSERT INTO CNT_ASIENTO_DET
            (ASID_CLAVE_ASI,
             ASID_ITEM,
             ASID_CLAVE_CTACO,
             ASID_IND_DB_CR,
             ASID_IMPORTE,
             ASID_TIPO_MOV,
             ASID_NRO_DOC,
             ASID_DESC,
             ASID_IMPORTE_MON,
             ASID_EMPR)
          VALUES
            (V_ASI_CLAVE,
             V_NRO_ITEM,
             V.CUENTA_DB,
             'D',
             0,
             NULL,
             NULL,
             NULL,
             V.COSTO_MON,
             P_EMPRESA);
        
          V_NRO_ITEM := V_NRO_ITEM + 1;
          INSERT INTO CNT_ASIENTO_DET
            (ASID_CLAVE_ASI,
             ASID_ITEM,
             ASID_CLAVE_CTACO,
             ASID_IND_DB_CR,
             ASID_IMPORTE,
             ASID_TIPO_MOV,
             ASID_NRO_DOC,
             ASID_DESC,
             ASID_IMPORTE_MON,
             ASID_EMPR)
          VALUES
            (V_ASI_CLAVE,
             V_NRO_ITEM,
             V.CUENTA_CR,
             'C',
             0,
             NULL,
             NULL,
             NULL,
             V.COSTO_MON,
             P_EMPRESA);
        EXCEPTION
          WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR(-20000, SQLERRM);
        END;
      END LOOP;
      -- COMMIT;
      ----FIN TRANSAGRO
    ELSE
    
      DELETE CNT_ASIENTO_DET
       WHERE ASID_EMPR = P_EMPRESA
         AND ASID_CLAVE_ASI IN
             (SELECT ASI_CLAVE
                FROM CNT_ASIENTO
               WHERE CNT_ASIENTO.ASI_SIST = 'STK'
                 AND ASI_FEC BETWEEN V_FEC_INI AND V_FEC_FIN
                 AND UPPER(ASI_OBS) LIKE '%ASIENTO DE COSTOS DE VENTAS%'
                 AND ASI_EMPR = P_EMPRESA);
      DELETE FROM CNT_ASIENTO
       WHERE CNT_ASIENTO.ASI_SIST = 'STK'
         AND ASI_FEC BETWEEN V_FEC_INI AND V_FEC_FIN
         AND UPPER(ASI_OBS) LIKE '%ASIENTO DE COSTOS DE VENTAS%'
         AND ASI_EMPR = P_EMPRESA;
      ---------------------------------------------------------------------------------------------------
      --ASIENTO EN MONEDA LOCAL (GS)
      ---------------------------------------------------------------------------------------------------
      SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_ASI_CLAVE FROM DUAL;
    
      SELECT EJ_CODIGO
        INTO V_ASI_EJERCICIO
        FROM CNT_EJERCICIO
       WHERE V_FEC_INI BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
         AND EJ_EMPR = P_EMPRESA;
    
      SELECT NVL(MAX(A.ASI_NRO), 0) + 1
        INTO V_ASI_NRO
        FROM CNT_ASIENTO A
       WHERE A.ASI_EJERCICIO = V_ASI_EJERCICIO
         AND ASI_EMPR = P_EMPRESA;
    
      INSERT INTO CNT_ASIENTO --COSTOS EN MONEDA LOCAL.
        (ASI_CLAVE,
         ASI_EMPR,
         ASI_EJERCICIO,
         ASI_NRO,
         ASI_FEC,
         ASI_OBS,
         ASI_FEC_GRAB,
         ASI_LOGIN,
         ASI_SIST,
         ASI_NRO_LISTADO_AUD)
      VALUES
        (V_ASI_CLAVE,
         P_EMPRESA,
         V_ASI_EJERCICIO,
         V_ASI_NRO,
         V_FEC_FIN,
         'ASIENTO DE COSTOS DE VENTAS ' ||
         UPPER(REPLACE(TO_CHAR(V_FEC_INI, 'Month-YYYY'), ' ', '')),
         SYSDATE,
         P_LOGIN,
         'STK',
         NULL);
      ---
    
      V_NRO_ITEM := 0;
      FOR V IN COSTO_VENTAS LOOP
      
        V_NRO_ITEM := V_NRO_ITEM + 1;
      
        BEGIN
          INSERT INTO CNT_ASIENTO_DET
            (ASID_CLAVE_ASI,
             ASID_ITEM,
             ASID_CLAVE_CTACO,
             ASID_IND_DB_CR,
             ASID_IMPORTE,
             ASID_TIPO_MOV,
             ASID_NRO_DOC,
             ASID_DESC,
             ASID_EMPR)
          VALUES
            (V_ASI_CLAVE,
             V_NRO_ITEM,
             V.CTA_COSTO,
             CASE WHEN V.COSTO_LOC >= 0 THEN 'D' ELSE 'C' END,
             CASE WHEN V.COSTO_LOC >= 0 THEN V.COSTO_LOC ELSE
             V.COSTO_LOC * (-1) END,
             NULL,
             NULL,
             'COSTO ' || V.CLASIFICACION || CASE WHEN
             V.CLASIFICACION = 'AFRECHO Y TRIGUILLO' THEN '(+COSTO FLETE)' END,
             P_EMPRESA);
        
          --   IF V.COSTO_LOC >= 0 THEN
          V_NRO_ITEM := V_NRO_ITEM + 1;
          INSERT INTO CNT_ASIENTO_DET
            (ASID_CLAVE_ASI,
             ASID_ITEM,
             ASID_CLAVE_CTACO,
             ASID_IND_DB_CR,
             ASID_IMPORTE,
             ASID_TIPO_MOV,
             ASID_NRO_DOC,
             ASID_DESC,
             ASID_EMPR)
          VALUES
            (V_ASI_CLAVE,
             V_NRO_ITEM,
             V.CTA_MERCADERIA,
             CASE WHEN V.COSTO_LOC >= 0 THEN 'C' ELSE 'D' END,
             CASE WHEN V.COSTO_LOC >= 0 THEN V.COSTO_LOC ELSE
             V.COSTO_LOC * (-1) END,
             NULL,
             NULL,
             NULL,
             P_EMPRESA);
        
          --  END IF;
        EXCEPTION
          WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR(-20000, SQLERRM);
        END;
      END LOOP;
    END IF;
    -- COMMIT;
  
  EXCEPTION
    WHEN SALIR THEN
      NULL;
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20000, SQLERRM);
  END;

  PROCEDURE STK_ASIENTO_MOVIMIENTO_STK(P_EMPRESA     IN NUMBER,
                                       P_FECHA_DESDE IN DATE,
                                       P_FECHA_HASTA IN DATE,
                                       P_COD_PERIODO IN NUMBER,
                                       P_LOGIN       IN VARCHAR2) AS
  
    V_ASI_CLAVE     NUMBER;
    V_ASI_EJERCICIO NUMBER;
    V_ASI_NRO       NUMBER;
    V_NRO_ITEM      NUMBER := 0;
    SALIR EXCEPTION;
    V_FEC_INI DATE;
    V_FEC_FIN DATE;
  
    CURSOR OPERACION_SIN_CONCEPTO IS
      SELECT DISTINCT OPER_CODIGO, OPER_DESC
        FROM STK_OPERACION A, STK_DOCUMENTO B, STK_DOCUMENTO_DET C
       WHERE A.OPER_EMPR = P_EMPRESA
         AND OPER_CODIGO = DOCU_CODIGO_OPER
         AND OPER_EMPR = DOCU_EMPR
         AND DOCU_FEC_EMIS BETWEEN V_FEC_INI AND V_FEC_FIN
         AND DOCU_CLAVE = DETA_CLAVE_DOC
         AND DOCU_EMPR = DETA_EMPR
         AND C.DETA_CLAVE_CONCEPTO IS NULL
         AND OPER_DESC IN ('INSUMOS_DE_FABRICA',
                           --  'GASTOS_DE_SEGURIDAD',  COMENTADO POR EL NUEVO PROCEDIMIENTOS QUE SE HIZO EN AGOSTO2019
                           'SAL_AVER_VENCIDOS');
  
    CURSOR OPERACIONES IS
      SELECT DISTINCT OPER_CODIGO, OPER_DESC
        FROM STK_OPERACION A, STK_DOCUMENTO B, STK_DOCUMENTO_DET C
       WHERE A.OPER_EMPR = P_EMPRESA
         AND OPER_CODIGO = DOCU_CODIGO_OPER
         AND OPER_EMPR = DOCU_EMPR
         AND DOCU_CLAVE = DETA_CLAVE_DOC
         AND DOCU_EMPR = DETA_EMPR
         AND C.DETA_CLAVE_CONCEPTO IS NOT NULL
         AND DOCU_FEC_EMIS BETWEEN V_FEC_INI AND V_FEC_FIN
         AND OPER_DESC IN ('INSUMOS_DE_FABRICA',
                           'GASTOS_DE_SEGURIDAD',
                           'SAL_AVER_VENCIDOS');
  
    CURSOR MOVIMIENTO_STK(I_OPERACION IN NUMBER) IS
      SELECT CASE
               WHEN OPER_DESC = 'SAL_AVER_VENCIDOS' THEN
                NVL(G.CTAC_CLAVE, A.OPER_CTACO_MOVIMIENTO)
               WHEN OPER_DESC = 'GASTOS_DE_SEGURIDAD' THEN
                G.CTAC_CLAVE
               ELSE
                G.CTAC_CLAVE
             END CTA_CONTABLE,
             CASE
               WHEN V_FEC_INI <= '31/07/2021' THEN
                NVL(F.CLAS_CTACO_MERCADERIA, 36)
               ELSE
                MARC_CTA_MERC
             END CTA_MERCADERIA,
             NVL(CLAS_DESC, LIN_DESC) CLASIFICACION,
             'D' TIPO_DB_CR,
             ROUND(SUM(CD_COSTO_PROM_LOC * DETA_CANT), 0) COSTO_LOC
        FROM STK_OPERACION     A,
             STK_COSTO_DIARIO  B,
             STK_DOCUMENTO     C,
             STK_DOCUMENTO_DET D,
             STK_ARTICULO      E,
             STK_CLASIFICACION F,
             CNT_CUENTA        G,
             STK_LINEA         H,
             FIN_CONCEPTO      I,
             STK_MARCA         J
       WHERE OPER_CODIGO = DOCU_CODIGO_OPER
         AND OPER_EMPR = DOCU_EMPR
            
         AND ART_LINEA = LIN_CODIGO
         AND ART_EMPR = LIN_EMPR
         AND ART_MARCA = MARC_CODIGO
         AND ART_EMPR = MARC_EMPR
            
         AND E.ART_CLASIFICACION = CLAS_CODIGO(+)
         AND E.ART_EMPR = CLAS_EMPR(+)
            
         AND D.DETA_CLAVE_CONCEPTO = I.FCON_CLAVE(+)
         AND D.DETA_EMPR = I.FCON_EMPR(+)
            
         AND FCON_CLAVE_CTACO = CTAC_CLAVE(+)
         AND FCON_EMPR = CTAC_EMPR(+)
            
         AND DETA_ART = CD_ART
         AND DETA_EMPR = CD_EMPR
            
         AND DOCU_FEC_EMIS = CD_FEC
         AND DOCU_EMPR = CD_EMPR
            
         AND DOCU_CLAVE = DETA_CLAVE_DOC
         AND DOCU_EMPR = DETA_EMPR
            
         AND DETA_ART = ART_CODIGO
         AND DETA_EMPR = ART_EMPR
            
         AND DOCU_CLAVE_FIN IS NULL
            
         AND ART_TIPO <> 4
         AND NVL(OPER_ENT_SAL, 'N') <> 'N'
            
         AND DOCU_FEC_EMIS BETWEEN V_FEC_INI AND V_FEC_FIN
         AND DOCU_EMPR = P_EMPRESA
         AND DETA_CLAVE_CONCEPTO IS NOT NULL
            
         AND CD_COSTO_PROM_LOC > 0
         AND A.OPER_CODIGO IN (I_OPERACION)
       GROUP BY marc_cta_merc,
                CLAS_CTACO_MERCADERIA,
                CLAS_DESC,
                G.CTAC_CLAVE,
                NVL(CLAS_DESC, LIN_DESC),
                
                CASE
                  WHEN OPER_DESC = 'SAL_AVER_VENCIDOS' THEN
                   NVL(G.CTAC_CLAVE, A.OPER_CTACO_MOVIMIENTO)
                  WHEN OPER_DESC = 'GASTOS_DE_SEGURIDAD' THEN
                   G.CTAC_CLAVE
                  ELSE
                   G.CTAC_CLAVE
                END;
  
  BEGIN
    --==ESTO AGREGUE DE NUEVO POR QUE TENIA ALGUNOS PROBLEMA CON LA FECHA
    SELECT PERI_FEC_INI, PERI_FEC_FIN
      INTO V_FEC_INI, V_FEC_FIN
      FROM FIN_PERIODO P
     WHERE P.PERI_EMPR = P_EMPRESA
       AND PERI_CODIGO = P_COD_PERIODO;
    --=========================================================
  
    IF GEN_PACK_SUC.GEN_CONF_IND_SUC = 'S' THEN
      RAISE SALIR;
    END IF;
    ---------------------------------------------------------------------------------------------------
    --ASIENTO EN MONEDA LOCAL (GS)
    ---------------------------------------------------------------------------------------------------
  
    SELECT EJ_CODIGO
      INTO V_ASI_EJERCICIO
      FROM CNT_EJERCICIO
     WHERE V_FEC_INI BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
       AND EJ_EMPR = P_EMPRESA;
  
    FOR R IN OPERACION_SIN_CONCEPTO LOOP
      RAISE_APPLICATION_ERROR(-20001,
                              'Operacion:' || R.OPER_DESC ||
                              ' sin conceptos asignados, verificar!!');
    
    END LOOP;
  
    FOR R IN OPERACIONES LOOP
    
      DELETE CNT_ASIENTO_DET
       WHERE ASID_EMPR = P_EMPRESA
         AND ASID_CLAVE_ASI IN
             (SELECT ASI_CLAVE
                FROM CNT_ASIENTO
               WHERE CNT_ASIENTO.ASI_SIST = 'STK'
                 AND ASI_FEC BETWEEN V_FEC_INI AND V_FEC_FIN
                 AND UPPER(ASI_OBS) LIKE
                     '%' || 'ASIENTO DE ' || REPLACE(R.OPER_DESC, '_', ' ') || '%'
                 AND ASI_EMPR = P_EMPRESA);
      DELETE FROM CNT_ASIENTO
       WHERE CNT_ASIENTO.ASI_SIST = 'STK'
         AND ASI_FEC BETWEEN V_FEC_INI AND V_FEC_FIN
         AND UPPER(ASI_OBS) LIKE
             '%' || 'ASIENTO DE ' || REPLACE(R.OPER_DESC, '_', ' ') || '%'
         AND ASI_EMPR = P_EMPRESA;
    
      SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_ASI_CLAVE FROM DUAL;
    
      SELECT NVL(MAX(A.ASI_NRO), 0) + 1
        INTO V_ASI_NRO
        FROM CNT_ASIENTO A
       WHERE A.ASI_EJERCICIO = V_ASI_EJERCICIO
         AND ASI_EMPR = P_EMPRESA;
    
      INSERT INTO CNT_ASIENTO --COSTOS EN MONEDA LOCAL.
        (ASI_CLAVE,
         ASI_EMPR,
         ASI_EJERCICIO,
         ASI_NRO,
         ASI_FEC,
         ASI_OBS,
         ASI_FEC_GRAB,
         ASI_LOGIN,
         ASI_SIST,
         ASI_NRO_LISTADO_AUD)
      VALUES
        (V_ASI_CLAVE,
         P_EMPRESA,
         V_ASI_EJERCICIO,
         V_ASI_NRO,
         V_FEC_FIN,
         'ASIENTO DE ' || REPLACE(R.OPER_DESC, '_', ' ') || ' ' ||
         UPPER(REPLACE(TO_CHAR(V_FEC_INI, 'Month-YYYY'), ' ', '')),
         SYSDATE,
         P_LOGIN,
         'STK',
         NULL);
      ---
      V_NRO_ITEM := 0;
      FOR V IN MOVIMIENTO_STK(R.OPER_CODIGO) LOOP
      
        V_NRO_ITEM := V_NRO_ITEM + 1;
      
        BEGIN
          INSERT INTO CNT_ASIENTO_DET
            (ASID_CLAVE_ASI,
             ASID_ITEM,
             ASID_CLAVE_CTACO,
             ASID_IND_DB_CR,
             ASID_IMPORTE,
             ASID_TIPO_MOV,
             ASID_NRO_DOC,
             ASID_DESC,
             ASID_EMPR)
          VALUES
            (V_ASI_CLAVE,
             V_NRO_ITEM,
             V.CTA_CONTABLE,
             V.TIPO_DB_CR,
             V.COSTO_LOC,
             NULL,
             NULL,
             V.CLASIFICACION,
             P_EMPRESA);
        
          IF V.COSTO_LOC >= 0 THEN
            V_NRO_ITEM := V_NRO_ITEM + 1;
            INSERT INTO CNT_ASIENTO_DET
              (ASID_CLAVE_ASI,
               ASID_ITEM,
               ASID_CLAVE_CTACO,
               ASID_IND_DB_CR,
               ASID_IMPORTE,
               ASID_TIPO_MOV,
               ASID_NRO_DOC,
               ASID_DESC,
               ASID_EMPR)
            VALUES
              (V_ASI_CLAVE,
               V_NRO_ITEM,
               V.CTA_MERCADERIA,
               CASE WHEN V.TIPO_DB_CR = 'C' THEN 'D' ELSE 'C' END,
               V.COSTO_LOC,
               NULL,
               NULL,
               NULL,
               P_EMPRESA);
          
          END IF;
        EXCEPTION
          WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR(-20000, SQLERRM);
        END;
      END LOOP;
    
    END LOOP;
  
    --COMMIT;
  
  EXCEPTION
    WHEN SALIR THEN
      NULL;
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20000, SQLERRM);
  END;

  PROCEDURE STK_ASIENTO_IMPORTACION(P_EMPRESA     IN NUMBER,
                                    P_FECHA_DESDE IN DATE,
                                    P_FECHA_HASTA IN DATE,
                                    P_COD_PERIODO IN NUMBER,
                                    P_LOGIN       IN VARCHAR2) AS
  
    V_ASI_CLAVE     NUMBER;
    V_ASI_EJERCICIO NUMBER;
    V_ASI_NRO       NUMBER;
    V_NRO_ITEM      NUMBER := 0;
    SALIR EXCEPTION;
    V_FEC_INI      DATE;
    V_FEC_FIN      DATE;
    V_IND_SIN_CLAS NUMBER := 0;
  
    CURSOR OPERACIONES IS
      SELECT COUNT(OPER_DESC)
        FROM STK_OPERACION     A,
             STK_DOCUMENTO     C,
             STK_DOCUMENTO_DET D,
             STK_ARTICULO      E,
             IMP_FACTURA       I
       WHERE OPER_CODIGO = DOCU_CODIGO_OPER
         AND OPER_EMPR = DOCU_EMPR
            
         AND DOCU_CLAVE = DETA_CLAVE_DOC
         AND DOCU_EMPR = DETA_EMPR
            
         AND DETA_ART = ART_CODIGO
         AND DETA_EMPR = ART_EMPR
            
         AND DOCU_CLAVE_FIN IS NULL
            
         AND ART_TIPO <> 4
         AND NVL(OPER_ENT_SAL, 'N') <> 'N'
            
         AND DOCU_FEC_EMIS BETWEEN V_FEC_INI AND V_FEC_FIN
         AND DOCU_EMPR = P_EMPRESA
            
         AND A.OPER_DESC IN ('COMPRA')
         AND DOCU_CLAVE = I.FAC_CLAVE_STK
         AND DOCU_EMPR = FAC_EMPR;
  
    CURSOR MOVIMIENTO_STK IS
      SELECT NVL(F.CLAS_CTACO_IMPORT, 26) CTA_IMP,
             CASE
               WHEN V_FEC_INI <= '31/07/2021' THEN
                NVL(F.CLAS_CTACO_MERCADERIA, 36)
               ELSE
                MARC_CTA_MERC
             END CTA_MERCADERIA,
             NVL(CLAS_DESC, ART_DESC) CLASIFICACION,
             'C' TIPO_DB_CR,
             OPER_DESC,
             ROUND(SUM(D.DETA_IMP_NETO_LOC + D.DETA_IVA_LOC), 0) COSTO_LOC
        FROM STK_OPERACION     A,
             STK_COSTO_DIARIO  B,
             STK_DOCUMENTO     C,
             STK_DOCUMENTO_DET D,
             STK_ARTICULO      E,
             STK_CLASIFICACION F,
             CNT_CUENTA        G,
             STK_LINEA         H,
             IMP_FACTURA       I,
             stk_marca         j
       WHERE OPER_CODIGO = DOCU_CODIGO_OPER
         AND OPER_EMPR = DOCU_EMPR
            
         AND ART_LINEA = LIN_CODIGO
         AND ART_EMPR = LIN_EMPR
            
         and art_marca = marc_codigo
         and art_empr = marc_empr
            
         AND E.ART_CLASIFICACION = CLAS_CODIGO(+)
         AND E.ART_EMPR = CLAS_EMPR(+)
            
         AND F.CLAS_CTACO_COSTO = CTAC_CLAVE(+)
         AND F.CLAS_EMPR = CTAC_EMPR(+)
            
         AND DETA_ART = CD_ART
         AND DETA_EMPR = CD_EMPR
            
         AND DOCU_FEC_EMIS = CD_FEC
         AND DOCU_EMPR = CD_EMPR
            
         AND DOCU_CLAVE = DETA_CLAVE_DOC
         AND DOCU_EMPR = DETA_EMPR
            
         AND DETA_ART = ART_CODIGO
         AND DETA_EMPR = ART_EMPR
            
         AND DOCU_CLAVE_FIN IS NULL
            
         AND ART_TIPO <> 4
         AND NVL(OPER_ENT_SAL, 'N') <> 'N'
            
         AND DOCU_FEC_EMIS BETWEEN V_FEC_INI AND V_FEC_FIN
         AND DOCU_EMPR = P_EMPRESA
            
         AND A.OPER_DESC IN ('COMPRA')
            
         AND DOCU_CLAVE = I.FAC_CLAVE_STK
         AND DOCU_EMPR = FAC_EMPR
      
       GROUP BY marc_cta_merc,
                CLAS_CTACO_MERCADERIA,
                OPER_DESC,
                A.OPER_CTACO_MOVIMIENTO,
                NVL(CLAS_DESC, ART_DESC),
                F.CLAS_CTACO_IMPORT;
  
  BEGIN
  
    --==ESTO AGREGUE DE NUEVO POR QUE TENIA ALGUNOS PROBLEMA CON LA FECHA
    SELECT PERI_FEC_INI, PERI_FEC_FIN
      INTO V_FEC_INI, V_FEC_FIN
      FROM FIN_PERIODO P
     WHERE P.PERI_EMPR = P_EMPRESA
       AND PERI_CODIGO = P_COD_PERIODO;
    --=========================================================
  
    IF GEN_PACK_SUC.GEN_CONF_IND_SUC = 'S' THEN
      RAISE SALIR;
    END IF;
  
    ---------------------------------------------------------------------------------------------------
    --ASIENTO EN MONEDA LOCAL (GS)
    ---------------------------------------------------------------------------------------------------
  
    SELECT EJ_CODIGO
      INTO V_ASI_EJERCICIO
      FROM CNT_EJERCICIO
     WHERE V_FEC_INI BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
       AND EJ_EMPR = P_EMPRESA;
  
    FOR R IN OPERACIONES LOOP
    
      DELETE CNT_ASIENTO_DET
       WHERE ASID_EMPR = P_EMPRESA
         AND ASID_CLAVE_ASI IN
             (SELECT ASI_CLAVE
                FROM CNT_ASIENTO
               WHERE CNT_ASIENTO.ASI_SIST = 'STK'
                 AND ASI_FEC BETWEEN V_FEC_INI AND V_FEC_FIN
                 AND UPPER(ASI_OBS) LIKE '%ASIENTO DE IMP. EN CURSO%'
                 AND ASI_EMPR = P_EMPRESA);
      DELETE FROM CNT_ASIENTO
       WHERE CNT_ASIENTO.ASI_SIST = 'STK'
         AND ASI_FEC BETWEEN V_FEC_INI AND V_FEC_FIN
         AND UPPER(ASI_OBS) LIKE '%ASIENTO DE IMP. EN CURSO%'
         AND ASI_EMPR = P_EMPRESA;
    
      SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_ASI_CLAVE FROM DUAL;
      SELECT NVL(MAX(A.ASI_NRO), 0) + 1
        INTO V_ASI_NRO
        FROM CNT_ASIENTO A
       WHERE A.ASI_EJERCICIO = V_ASI_EJERCICIO
         AND ASI_EMPR = P_EMPRESA;
    
      INSERT INTO CNT_ASIENTO --COSTOS EN MONEDA LOCAL.
        (ASI_CLAVE,
         ASI_EMPR,
         ASI_EJERCICIO,
         ASI_NRO,
         ASI_FEC,
         ASI_OBS,
         ASI_FEC_GRAB,
         ASI_LOGIN,
         ASI_SIST,
         ASI_NRO_LISTADO_AUD)
      VALUES
        (V_ASI_CLAVE,
         P_EMPRESA,
         V_ASI_EJERCICIO,
         V_ASI_NRO,
         V_FEC_FIN,
         'ASIENTO DE IMP. EN CURSO ' ||
         UPPER(REPLACE(TO_CHAR(V_FEC_INI, 'Month-YYYY'), ' ', '')),
         SYSDATE,
         P_LOGIN,
         'STK',
         NULL);
      ---
      V_NRO_ITEM := 0;
    
      FOR V IN MOVIMIENTO_STK LOOP
      
        V_NRO_ITEM := V_NRO_ITEM + 1;
      
        BEGIN
          INSERT INTO CNT_ASIENTO_DET
            (ASID_CLAVE_ASI,
             ASID_ITEM,
             ASID_CLAVE_CTACO,
             ASID_IND_DB_CR,
             ASID_IMPORTE,
             ASID_TIPO_MOV,
             ASID_NRO_DOC,
             ASID_DESC,
             ASID_EMPR)
          VALUES
            (V_ASI_CLAVE,
             V_NRO_ITEM,
             V.CTA_IMP,
             V.TIPO_DB_CR,
             V.COSTO_LOC,
             NULL,
             NULL,
             'IMPORTACION DE ' || V.CLASIFICACION,
             P_EMPRESA);
        
          V_NRO_ITEM := V_NRO_ITEM + 1;
          INSERT INTO CNT_ASIENTO_DET
            (ASID_CLAVE_ASI,
             ASID_ITEM,
             ASID_CLAVE_CTACO,
             ASID_IND_DB_CR,
             ASID_IMPORTE,
             ASID_TIPO_MOV,
             ASID_NRO_DOC,
             ASID_DESC,
             ASID_EMPR)
          VALUES
            (V_ASI_CLAVE,
             V_NRO_ITEM,
             V.CTA_MERCADERIA,
             CASE WHEN V.TIPO_DB_CR = 'C' THEN 'D' ELSE 'C' END,
             V.COSTO_LOC,
             NULL,
             NULL,
             NULL,
             P_EMPRESA);
        
        EXCEPTION
          WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR(-20000, SQLERRM);
        END;
      END LOOP;
    
    END LOOP;
  
    -- COMMIT;
  EXCEPTION
    WHEN SALIR THEN
      NULL;
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20000, SQLERRM);
  END;

  PROCEDURE PP_ACTUALIZAR_CNT_CONFI(P_EMPRESA      IN NUMBER,
                                    P_FECHA_INICIO IN DATE,
                                    P_FECHA_FIN    IN DATE) AS
    V_CONF_PER_ACT_FIN DATE;
    V_CONF_PER_ACT_INI DATE;
    V_CLAVE_DESDE      NUMBER;
    V_CLAVE_HASTA      NUMBER;
  
  BEGIN
  
    SELECT CONF_PER_ACT_INI, CONF_PER_ACT_FIN
    
      INTO V_CONF_PER_ACT_INI, V_CONF_PER_ACT_FIN
    
      FROM FIN_CONFIGURACION
     WHERE CONF_EMPR = P_EMPRESA;
  
    BEGIN
      SELECT MIN(ASI_CLAVE)
      
        INTO V_CLAVE_DESDE
        FROM CNT_ASIENTO T
      
       WHERE T.ASI_FEC = P_FECHA_FIN
         AND T.ASI_SIST = 'FIN'
         AND T.ASI_EMPR = P_EMPRESA;
    
    END;
  
    BEGIN
      SELECT MAX(ASI_CLAVE)
        INTO V_CLAVE_HASTA
        FROM CNT_ASIENTO T
       WHERE T.ASI_FEC = P_FECHA_FIN
         AND T.ASI_SIST = 'FIN'
         AND T.ASI_EMPR = P_EMPRESA;
    
    END;
  
    --GUARDAR LOS NUMEROS DE ASIENTOS GENERADOS (EL INICIAL Y EL FINAL)
    --SOLO GUARDAR EN CASO QUE SEA EL PERI?DO ACTUAL.
    IF P_FECHA_INICIO = V_CONF_PER_ACT_INI AND
       P_FECHA_FIN = V_CONF_PER_ACT_FIN THEN
      UPDATE CNT_CONFIGURACION
         SET CONF_CLAVE_ASIENTO_INI = V_CLAVE_DESDE,
             CONF_CLAVE_ASIENTO_FIN = V_CLAVE_HASTA
       WHERE CONF_EMPR = P_EMPRESA;
    END IF;
  
  END;

  PROCEDURE PP_ACTUALIZAR_FIN_CONFI(P_EMPRESA      IN NUMBER,
                                    P_FECHA_INICIO IN DATE,
                                    P_FECHA_FIN    IN DATE) AS
  
    V_EMPR             NUMBER;
    V_CONF_PER_ACT_FIN DATE;
    V_CONF_PER_ACT_INI DATE;
    V_CLAVE_DESDE      NUMBER;
    V_CLAVE_HASTA      NUMBER;
  
  BEGIN
  
    SELECT CONF_PER_ACT_INI, CONF_PER_ACT_FIN
    
      INTO V_CONF_PER_ACT_INI, V_CONF_PER_ACT_FIN
    
      FROM FIN_CONFIGURACION
     WHERE CONF_EMPR = P_EMPRESA;
  
    BEGIN
      SELECT MIN(ASI_CLAVE)
      
        INTO V_CLAVE_DESDE
        FROM CNT_ASIENTO T
      
       WHERE T.ASI_FEC = P_FECHA_FIN
         AND T.ASI_SIST = 'FIN'
         AND T.ASI_EMPR = P_EMPRESA;
    
    END;
  
    BEGIN
      SELECT MAX(ASI_CLAVE)
        INTO V_CLAVE_HASTA
        FROM CNT_ASIENTO T
       WHERE T.ASI_FEC = P_FECHA_FIN
            --  AND T.ASI_SIST  = 'FIN'
         AND T.ASI_EMPR = P_EMPRESA;
    
    END;
  
    --ESTO SE HACE PARA QUE SE EJECUTE EL PACKAGE Y LEA FIN_CONFIGURACION, ASI EL
    --TRIGGER DE REPLICA SOBRE FIN_CONFIGURACION CUANDO LLAMA AL PACKAGE GEN_PACK_SUC, ESTE YA SE ENCUENTRA INICIALIZADO
    --SI NO SE HACE DA ERROR ORA-04091 TABLE STRING.STRING IS MUTATING, TRIGGER/FUNCTION MAY NOT SEE IT
    --PORQUE EL DISPARADOR DE REPLICA NECESITA LLAMAR A GEN_PACK_SUC Y AL SER
    --LA PRIMERA LLAMADA, ESTE PACKAGE LEE LA TABLA FIN_CONFIGURACION QUE EST? SIENDO MUTADA
  
    V_EMPR := GEN_PACK_SUC.GEN_CONF_EMPR;
  
    --BLOQUEAR EL SISTEMA DE FINANZAS PARA QUE NO MODIFIQUEN NADA Y
    --EVITAR INCONSISTENCIAS ENTRE FINANZAS Y CONTABILIDAD
  
    IF P_FECHA_INICIO = V_CONF_PER_ACT_INI AND
       P_FECHA_FIN = V_CONF_PER_ACT_FIN THEN
      UPDATE FIN_CONFIGURACION
         SET CONF_IND_HAB_MES_ACT = 'N'
       WHERE CONF_EMPR = P_EMPRESA;
    END IF;
  
    UPDATE FIN_PERIODO
       SET PERI_CLAVE_FIN_ASI_INI = V_CLAVE_DESDE,
           PERI_CLAVE_FIN_ASI_FIN = V_CLAVE_HASTA,
           PERI_EMPR              = P_EMPRESA
     WHERE TO_CHAR(PERI_FEC_INI, 'DD-MM-YYYY') = P_FECHA_INICIO
       AND TO_CHAR(PERI_FEC_FIN, 'DD-MM-YYYY') = P_FECHA_FIN
       AND PERI_EMPR = P_EMPRESA;
  
  END;

  PROCEDURE PP_BORRAR_ASIENTOS_I(P_EMPRESA      IN NUMBER,
                                 P_FECHA_INICIO IN DATE,
                                 P_FECHA_FIN    IN DATE) AS
  
    CURSOR ASIENTOI IS
      SELECT ASII_CLAVE
        FROM CNTI_ASIENTO
       WHERE ASII_EMPR = P_EMPRESA
         AND ASII_SIST = 'FIN'
         AND ASII_FEC BETWEEN P_FECHA_INICIO AND P_FECHA_INICIO;
  
  BEGIN
    FOR R IN ASIENTOI LOOP
      DELETE FROM CNTI_ASIENTO_DET
       WHERE ASIDI_CLAVE = R.ASII_CLAVE
         AND ASIDI_EMPR = P_EMPRESA;
    
      DELETE FROM CNTI_ASIENTO
       WHERE ASII_CLAVE = R.ASII_CLAVE
         AND ASII_EMPR = P_EMPRESA;
    END LOOP;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
  END;

  PROCEDURE PP_BORRAR_REGISTRO(P_EMPRESA      IN NUMBER,
                               P_FECHA_INICIO IN DATE,
                               P_FECHA_FIN    IN DATE) AS
  
    SALIR EXCEPTION;
    V_EMPR             NUMBER;
    V_VALIDA_ASIENTO   VARCHAR2(5);
    V_ASINETO_INICIAL  NUMBER;
    V_ASIENTO_FINAL    NUMBER;
    V_CONF_PER_ACT_FIN DATE;
    V_CONF_PER_ACT_INI DATE;
  
  BEGIN
  
    SELECT CONF_PER_ACT_INI, CONF_PER_ACT_FIN
    
      INTO V_CONF_PER_ACT_INI, V_CONF_PER_ACT_FIN
    
      FROM FIN_CONFIGURACION
     WHERE CONF_EMPR = P_EMPRESA;
  
    BEGIN
      -- CALL THE FUNCTION
      V_VALIDA_ASIENTO := FP_VALIDA_EXIS_ASIENTO(I_EMPRESA      => P_EMPRESA,
                                                 I_PERI_FEC_INI => P_FECHA_INICIO);
    END;
  
    IF V_VALIDA_ASIENTO = 'A' THEN
      --LOS AUTOMATICOS SE BORRARAN CON EL PROCEDIMIENTO
    
      PP_VALIDAR_ASIENTOS('S', P_FECHA_FIN, P_EMPRESA);
    
    ELSE
    
      CNTP103.PP_BORRAR_ASIENTOS_I(P_EMPRESA      => P_EMPRESA,
                                   P_FECHA_INICIO => P_FECHA_INICIO,
                                   P_FECHA_FIN    => P_FECHA_FIN);
    
      SELECT PERI_CLAVE_FIN_ASI_INI, PERI_CLAVE_FIN_ASI_FIN
        INTO V_ASINETO_INICIAL, V_ASIENTO_FINAL
        FROM FIN_PERIODO
       WHERE PERI_FEC_INI = P_FECHA_INICIO
         AND PERI_FEC_FIN = P_FECHA_FIN
         AND PERI_EMPR = P_EMPRESA;
    
      DELETE CNT_ASIENTO_DET
       WHERE ASID_CLAVE_ASI BETWEEN V_ASINETO_INICIAL AND V_ASIENTO_FINAL
         AND ASID_EMPR = P_EMPRESA;
    
      DELETE CNT_ASIENTO_DOC_DET
       WHERE ASIDD_CLAVE_ASI BETWEEN V_ASINETO_INICIAL AND V_ASIENTO_FINAL
         AND ASIDD_EMPR = P_EMPRESA;
    
      UPDATE FIN_PERIODO
         SET PERI_CLAVE_FIN_ASI_INI = NULL,
             PERI_CLAVE_FIN_ASI_FIN = NULL,
             PERI_EMPR              = P_EMPRESA
       WHERE TO_CHAR(PERI_FEC_INI, 'DD-MM-YYYY') = P_FECHA_INICIO
         AND TO_CHAR(PERI_FEC_FIN, 'DD-MM-YYYY') = P_FECHA_FIN
         AND PERI_EMPR = P_EMPRESA;
    
      DELETE CNT_ASIENTO
       WHERE ASI_CLAVE BETWEEN V_ASINETO_INICIAL AND V_ASIENTO_FINAL
         AND ASI_EMPR = P_EMPRESA;
    
      --SOLO SI ES EL PERI?DO ACTUAL DE FINANZAS
      IF P_FECHA_INICIO = V_CONF_PER_ACT_INI AND
         P_FECHA_FIN = V_CONF_PER_ACT_FIN THEN
        UPDATE CNT_CONFIGURACION
           SET CONF_CLAVE_ASIENTO_INI = NULL, CONF_CLAVE_ASIENTO_FIN = NULL
         WHERE CONF_EMPR = P_EMPRESA;
      END IF;
    
      --ESTO SE HACE PARA QUE SE EJECUTE EL PACKAGE Y LEA FIN_CONFIGURACION, ASI EL
      --TRIGGER DE REPLICA SOBRE FIN_CONFIGURACION CUANDO LLAMA AL PACKAGE GEN_PACK_SUC, ESTE YA SE ENCUENTRA INICIALIZADO
      --SI NO SE HACE DA ERROR ORA-04091 TABLE STRING.STRING IS MUTATING, TRIGGER/FUNCTION MAY NOT SEE IT
      --PORQUE EL DISPARADOR DE REPLICA NECESITA LLAMAR A GEN_PACK_SUC Y AL SER
      --LA PRIMERA LLAMADA, ESTE PACKAGE LEE LA TABLA FIN_CONFIGURACION QUE EST? SIENDO MUTADA
    
      V_EMPR := GEN_PACK_SUC.GEN_CONF_EMPR;
    
      --VOLVER A HABILITAR EL SISTEMA DE FINANZAS PARA QUE PUEDAN
      --HACER SUS MODIFICACIONES
      --SOLO SI ES PERI?DO ACTUAL
      IF P_FECHA_INICIO = V_CONF_PER_ACT_INI AND
         P_FECHA_FIN = V_CONF_PER_ACT_FIN THEN
        UPDATE FIN_CONFIGURACION
           SET CONF_IND_HAB_MES_ACT = 'S'
         WHERE CONF_EMPR = P_EMPRESA;
      END IF;
    
      --RESTAURAMOS LOS LOS VALORES DE LOS INTERESES QUE SE DEVENGARON.
      FIN_DEVENGAMIENTO(TO_DATE(P_FECHA_INICIO, 'dd/mm/yyyy'),
                        TO_DATE(P_FECHA_FIN, 'dd/mm/yyyy'),
                        'A',
                        P_EMPRESA);
    
    END IF;
  EXCEPTION
    WHEN SALIR THEN
      ROLLBACK;
    
  END;

  PROCEDURE PP_ASIENTO_IRACIS(P_EMPRESA       IN NUMBER,
                              P_COD_PERIODO   IN NUMBER,
                              P_CLAVE_ASIENTO OUT NUMBER) IS
    V_FEC_INI DATE;
    V_FEC_FIN DATE;
    SALIR EXCEPTION;
    V_ASI_EJERCICIO  NUMBER;
    V_IMPORTE_IRACIS NUMBER;
    V_NRO_ITEM       NUMBER := 0;
    V_ASI_CLAVE      NUMBER;
    V_ASI_NRO        NUMBER;
  BEGIN
  
    --==  FECHA
    SELECT PERI_FEC_INI, PERI_FEC_FIN
      INTO V_FEC_INI, V_FEC_FIN
      FROM FIN_PERIODO P
     WHERE P.PERI_EMPR = P_EMPRESA
       AND PERI_CODIGO = P_COD_PERIODO;
    --=========================================================
  
    IF GEN_PACK_SUC.GEN_CONF_IND_SUC = 'S' THEN
      RAISE SALIR;
    END IF;
  
    IF V_FEC_INI < '01/01/2018' THEN
      RAISE SALIR;
    END IF;
  
    ---------------------------------------------------------------------------------------------------
    --ASIENTO EN MONEDA LOCAL (GS)
    ---------------------------------------------------------------------------------------------------
  
    SELECT EJ_CODIGO
      INTO V_ASI_EJERCICIO
      FROM CNT_EJERCICIO
     WHERE V_FEC_INI BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
       AND EJ_EMPR = P_EMPRESA;
  
    --=========================================================
  
    BEGIN
      SELECT nvl(IMPORTE, 0)
        INTO V_IMPORTE_IRACIS
        FROM (SELECT ROUND(SUM((IMPORTE * B.CONF_PORC_IRACIS) / 100), 0) IMPORTE
              
                FROM CNTL116_V A, FIN_CONFIGURACION B
               WHERE A.ASI_EMPR = P_EMPRESA
                 AND A.ASI_EMPR = CONF_EMPR
                 AND TO_CHAR(A.MES, 'mm/yyyy') =
                     TO_CHAR(V_FEC_FIN, 'mm/yyyy'))
       WHERE IMPORTE > 0;
    
    EXCEPTION
      WHEN OTHERS THEN
        RAISE SALIR;
      
    END;
  
    IF NVL(V_IMPORTE_IRACIS, 0) = 0 THEN
      RAISE SALIR;
    END IF;
  
    SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_ASI_CLAVE FROM DUAL;
  
    SELECT NVL(MAX(A.ASI_NRO), 0) + 1
      INTO V_ASI_NRO
      FROM CNT_ASIENTO A
     WHERE A.ASI_EJERCICIO = V_ASI_EJERCICIO
       AND ASI_EMPR = P_EMPRESA;
  
    INSERT INTO CNT_ASIENTO
      (ASI_CLAVE,
       ASI_EMPR,
       ASI_EJERCICIO,
       ASI_NRO,
       ASI_FEC,
       ASI_OBS,
       ASI_FEC_GRAB,
       ASI_LOGIN,
       ASI_SIST,
       ASI_NRO_LISTADO_AUD)
    VALUES
      (V_ASI_CLAVE,
       P_EMPRESA,
       V_ASI_EJERCICIO,
       V_ASI_NRO,
       V_FEC_FIN,
       'ASIENTO DE PROV. (IRACIS) ' ||
       UPPER(REPLACE(TO_CHAR(V_FEC_INI, 'Month-YYYY'), ' ', '')),
       SYSDATE,
       GEN_DEVUELVE_USER,
       'SIG',
       NULL);
    ---
  
    V_NRO_ITEM := V_NRO_ITEM + 1;
  
    BEGIN
      INSERT INTO CNT_ASIENTO_DET
        (ASID_CLAVE_ASI,
         ASID_ITEM,
         ASID_CLAVE_CTACO,
         ASID_IND_DB_CR,
         ASID_IMPORTE,
         ASID_TIPO_MOV,
         ASID_NRO_DOC,
         ASID_DESC,
         ASID_EMPR)
      VALUES
        (V_ASI_CLAVE,
         V_NRO_ITEM,
         117, --ESTATICO POR EL MOMENTO
         CASE WHEN V_IMPORTE_IRACIS > 0 THEN 'C' WHEN V_IMPORTE_IRACIS < 0 THEN 'D' ELSE 'C' END,
         V_IMPORTE_IRACIS,
         NULL,
         NULL,
         NULL,
         P_EMPRESA);
    
      V_NRO_ITEM := V_NRO_ITEM + 1;
      INSERT INTO CNT_ASIENTO_DET
        (ASID_CLAVE_ASI,
         ASID_ITEM,
         ASID_CLAVE_CTACO,
         ASID_IND_DB_CR,
         ASID_IMPORTE,
         ASID_TIPO_MOV,
         ASID_NRO_DOC,
         ASID_DESC,
         ASID_EMPR)
      VALUES
        (V_ASI_CLAVE,
         V_NRO_ITEM,
         10258,  --@PabloACespedes, en duro porque tio UGO se_apura, igual reventara PAPU 725,
         CASE WHEN V_IMPORTE_IRACIS > 0 THEN 'D' WHEN V_IMPORTE_IRACIS < 0 THEN 'C' ELSE 'D' END,
         V_IMPORTE_IRACIS,
         NULL,
         NULL,
         NULL,
         P_EMPRESA);
    
      P_CLAVE_ASIENTO := V_ASI_CLAVE;
    
      UPDATE FIN_PERIODO
         SET PERI_CLAVE_FIN_ASI_FIN = V_ASI_CLAVE
       WHERE PERI_FEC_INI = V_FEC_INI
         AND PERI_FEC_FIN = V_FEC_FIN
         AND PERI_EMPR = P_EMPRESA;
      -- COMMIT;
    EXCEPTION
      WHEN salir THEN
        null;
      when others then
      
        RAISE_APPLICATION_ERROR(-20000, SQLERRM);
      
    END;
  
  END PP_ASIENTO_IRACIS;

  PROCEDURE PP_CREAR_DEV_PREST_BANC(P_EMPRESA     IN NUMBER,
                                    P_FECHA_DESDE IN DATE,
                                    P_FECHA_HASTA IN DATE,
                                    P_COD_PERIODO IN NUMBER) IS
  
    V_COTIZACION    NUMBER;
    V_ASI_CLAVE     NUMBER;
    V_ASI_EJERCICIO NUMBER;
    V_ASI_NRO       NUMBER;
    V_USUARIO       VARCHAR2(100) := gen_devuelve_user;
    V_NRO_ITEM      NUMBER;
    V_CTA_CREDITO   NUMBER;
    V_CTA_DEBITO    NUMBER;
    V_FEC_INI       DATE;
    V_FEC_FIN       DATE;
    SALIR EXCEPTION;
  
    CURSOR DEVENGAMIENTO IS
      SELECT EMPRESA,
             TASA_COT,
             CASE
               WHEN COD_MONEDA = 1 THEN
                'PRESTAMOS EN ' || DESC_MONEDA
               ELSE
                'PRESTAMOS EN ' || DESC_MONEDA || ', COTIZACION: ' ||
                TO_CHAR(TASA_COT)
             END DESC_ASIENTO,
             SUM(IMP_INT_A_DEVENGAR) TOTAL_A_DEVENGAR
        FROM FIN_PREST_BANCARIO_AUX A
       WHERE A.EMPRESA = P_EMPRESA
         AND USUARIO = V_USUARIO
         AND MES_A_DEVENGAR BETWEEN V_FEC_INI AND V_FEC_FIN
       GROUP BY EMPRESA, DESC_MONEDA, TASA_COT, A.COD_MONEDA
       ORDER BY COD_MONEDA;
  
  BEGIN
  
    --==  FECHA
    SELECT PERI_FEC_INI, PERI_FEC_FIN
      INTO V_FEC_INI, V_FEC_FIN
      FROM FIN_PERIODO P
     WHERE P.PERI_EMPR = P_EMPRESA
       AND PERI_CODIGO = P_COD_PERIODO;
  
    IF V_FEC_INI < TO_DATE('01/08/2019') THEN
      RAISE SALIR;
    END IF;
  
    V_COTIZACION := FIN_BUSCAR_COTIZACION_FEC(FEC  => V_FEC_FIN,
                                              MON  => 2,
                                              EMPR => P_EMPRESA);
  
    --  RAISE_APPLICATION_ERROR(-20001, P_COD_PERIODO||'-'||);
  
    FINC019.PP_DEVENGAR_PRESTAMOS(P_EMPRESA     => P_EMPRESA,
                                  P_MONEDA      => NULL,
                                  P_PRESTAMO    => NULL,
                                  P_FECHA_DESDE => NULL,
                                  P_FECHA_HASTA => V_FEC_FIN,
                                  P_COTIZACION  => V_COTIZACION,
                                  P_PROVEEDOR   => NULL,
                                  P_SESSION     => NULL);
  
    SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_ASI_CLAVE FROM DUAL;
  
    SELECT EJ_CODIGO
      INTO V_ASI_EJERCICIO
      FROM CNT_EJERCICIO
     WHERE V_FEC_INI BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
       AND EJ_EMPR = P_EMPRESA;
  
    BEGIN
      SELECT NVL(MAX(A.ASI_NRO), 0) + 1
        INTO V_ASI_NRO
        FROM CNT_ASIENTO A
       WHERE A.ASI_EJERCICIO = V_ASI_EJERCICIO
         AND ASI_EMPR = P_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_ASI_NRO := 1;
    END;
  
    FOR R IN DEVENGAMIENTO LOOP
    
      V_NRO_ITEM := NVL(V_NRO_ITEM, 0) + 1;
      IF V_NRO_ITEM = 1 THEN
        -- PARA QUE GUARDE SOLO UNA LA CABECERA
        INSERT INTO CNT_ASIENTO
          (ASI_CLAVE,
           ASI_EMPR,
           ASI_EJERCICIO,
           ASI_NRO,
           ASI_FEC,
           ASI_OBS,
           ASI_FEC_GRAB,
           ASI_LOGIN,
           ASI_SIST,
           ASI_NRO_LISTADO_AUD)
        VALUES
          (V_ASI_CLAVE,
           P_EMPRESA,
           V_ASI_EJERCICIO,
           V_ASI_NRO,
           V_FEC_FIN,
           'ASIENTO DE DEV. DE PRESTAMOS BANC. ' ||
           UPPER(REPLACE(TO_CHAR(V_FEC_INI, 'Month-YYYY'), ' ', '')),
           SYSDATE,
           GEN_DEVUELVE_USER,
           'SIG',
           NULL);
        ---
      END IF;
      V_CTA_CREDITO := 2280;
      V_CTA_DEBITO  := 266;
    
      INSERT INTO CNT_ASIENTO_DET
        (ASID_CLAVE_ASI,
         ASID_ITEM,
         ASID_CLAVE_CTACO,
         ASID_IND_DB_CR,
         ASID_IMPORTE,
         ASID_TIPO_MOV,
         ASID_NRO_DOC,
         ASID_DESC,
         ASID_EMPR)
      VALUES
        (V_ASI_CLAVE,
         V_NRO_ITEM,
         V_CTA_CREDITO, --ESTATICO POR EL MOMENTO
         'C',
         R.TOTAL_A_DEVENGAR,
         NULL,
         NULL,
         R.DESC_ASIENTO,
         P_EMPRESA);
    
      V_NRO_ITEM := V_NRO_ITEM + 1;
    
      INSERT INTO CNT_ASIENTO_DET
        (ASID_CLAVE_ASI,
         ASID_ITEM,
         ASID_CLAVE_CTACO,
         ASID_IND_DB_CR,
         ASID_IMPORTE,
         ASID_TIPO_MOV,
         ASID_NRO_DOC,
         ASID_DESC,
         ASID_EMPR)
      VALUES
        (V_ASI_CLAVE,
         V_NRO_ITEM,
         V_CTA_DEBITO, --ESTATICO POR EL MOMENTO
         'D',
         R.TOTAL_A_DEVENGAR,
         NULL,
         NULL,
         R.DESC_ASIENTO,
         P_EMPRESA);
    
    END LOOP;
  
    --  P_CLAVE_ASIENTO := V_ASI_CLAVE;
    /*
      UPDATE FIN_PERIODO
         SET PERI_CLAVE_FIN_ASI_FIN = V_ASI_CLAVE
       WHERE PERI_FEC_INI = V_FEC_INI
         AND PERI_FEC_FIN = V_FEC_FIN
         AND PERI_EMPR = P_EMPRESA;
    */
  EXCEPTION
    WHEN SALIR THEN
      NULL;
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20000, SQLERRM);
  END PP_CREAR_DEV_PREST_BANC;

  PROCEDURE PP_MENSAJE_FINAL(P_EMPRESA        IN NUMBER,
                             P_FECHA_INICIO   IN DATE,
                             P_FECHA_FIN      IN DATE,
                             P_PRIMER_ASIENTO OUT NUMBER,
                             P_ULTIMO_ASIENTO OUT NUMBER) AS
  
  BEGIN
    SELECT PRIMER.ASI_NRO, ULTIMO.ASI_NRO
      INTO P_PRIMER_ASIENTO, P_ULTIMO_ASIENTO
      FROM CNT_ASIENTO PRIMER, CNT_ASIENTO ULTIMO, FIN_PERIODO
     WHERE PRIMER.ASI_CLAVE = PERI_CLAVE_FIN_ASI_INI
       AND ULTIMO.ASI_CLAVE = PERI_CLAVE_FIN_ASI_FIN
       AND PERI_FEC_INI = P_FECHA_INICIO
       AND PERI_FEC_FIN = P_FECHA_FIN
       AND PRIMER.ASI_EMPR = ULTIMO.ASI_EMPR
       AND ULTIMO.ASI_EMPR = PERI_EMPR
       AND PERI_EMPR = P_EMPRESA;
  
  END PP_MENSAJE_FINAL;

  FUNCTION FP_VALIDAR_AGUINALDO_PROV(P_EMPRESA IN NUMBER) RETURN VARCHAR2 IS
  
    CURSOR C IS
      SELECT DISTINCT A.DPTOC_DPTO,
                      B.DPTO_DESC,
                      A.DPTOC_EMPR,
                      CNT.CTAC_NRO || ' - ' || CNT.CTAC_DESC CUENTA,
                      CNT.CTAC_NRO,
                      AG.PROVA_CUENTA
        FROM PER_DPTO_CONC           A,
             GEN_DEPARTAMENTO        B,
             PER_CONCEPTO            C,
             FIN_CONCEPTO            D,
             CNT_CUENTA              CNT,
             PER_EMPLEADO            EMP,
             PER_EMPLEADO_DEPTO      ED,
             CNT_CONF_PROV_AGUINALDO AG
       WHERE A.DPTOC_DPTO = B.DPTO_CODIGO
         AND A.DPTOC_EMPR = B.DPTO_EMPR
         AND A.DPTOC_PER_CONC = C.PCON_CLAVE
         AND A.DPTOC_EMPR = C.PCON_EMPR
         AND A.DPTOC_FIN_CONC = D.FCON_CLAVE
         AND A.DPTOC_EMPR = D.FCON_EMPR
         AND A.DPTOC_EMPR = P_EMPRESA
         AND C.PCON_IND_AGUINALDO = 'S'
         AND CNT.CTAC_CLAVE = D.FCON_CLAVE_CTACO
         AND CNT.CTAC_EMPR = D.FCON_EMPR
         AND AG.PROVA_CUENTA(+) = CNT.CTAC_CLAVE
         AND AG.PROVA_EMPR(+) = CNT.CTAC_EMPR
         AND B.DPTO_CODIGO = ED.PEREMPDEP_DEPTO
         AND B.DPTO_EMPR = ED.PEREMPDEP_EMPR
         AND AG.PROVA_CUENTA IS NULL
         AND EMP.EMPL_LEGAJO = ED.PEREMPDEP_EMPL
         AND EMP.EMPL_EMPRESA = ED.PEREMPDEP_EMPR
         AND EMP.EMPL_DEPARTAMENTO = ED.PEREMPDEP_DEPTO
         AND EMP.EMPL_SITUACION = 'A'
       ORDER BY A.DPTOC_DPTO;
  
    V_MENSAJE VARCHAR2(22323) := ' Falta cargar la configuracion de provision aguinaldo para el departamento';
    --  V_BANDERA      NUMBER;
    V_ESTADO       NUMBER := 0;
    V_CUENTA_NRO   NUMBER;
    V_CTA_ANTERIOR VARCHAR2(100);
  BEGIN
  
    FOR I IN C LOOP
    
      IF V_CTA_ANTERIOR <> I.DPTO_DESC OR V_CTA_ANTERIOR IS NULL THEN
        V_MENSAJE := V_MENSAJE || CHR(13) || ' ' || I.DPTOC_DPTO || '-' ||
                     I.DPTO_DESC;
      END IF;
    
      V_MENSAJE := V_MENSAJE || ' |cuenta: ' || I.CUENTA || '';
    
      V_ESTADO := V_ESTADO + 1;
    
      V_CTA_ANTERIOR := I.DPTO_DESC;
    
    END LOOP;
  
    IF V_ESTADO = 0 THEN
      V_MENSAJE := NULL;
    END IF;
  
    RETURN V_MENSAJE;
  
  END FP_VALIDAR_AGUINALDO_PROV;

  PROCEDURE PP_PREVISION_PREMIOS_GERENCIA(P_EMPRESA       IN NUMBER,
                                          P_FECHA_DESDE   IN DATE,
                                          P_FECHA_HASTA   IN DATE,
                                          P_COD_PERIODO   IN NUMBER,
                                          P_LOGIN         IN VARCHAR2,
                                          P_CLAVE_ASIENTO OUT NUMBER) IS
    V_ASI_NRO        NUMBER;
    V_CUENTA_CREDITO NUMBER := 8482;
    V_TOTAL_IMPORTE  NUMBER;
    V_NRO_ITEM       NUMBER;
    V_ASI_CLAVE      NUMBER;
    V_ASI_EJERCICIO  NUMBER;
    CURSOR PREVISION IS
    
      SELECT B.ADPG_CUENTA CTA_CONTABLE,
             ADPG_PORCENTAJE * 100 PORCENTAJE,
             RESULTADO_OPERATIVO,
             ROUND(A.RESULTADO_OPERATIVO * B.ADPG_PORCENTAJE) IMPORTE,
             C.CANB_DESC CANAL,
             CANB_CODIGO CANAL_CODIGO
        FROM CNT_RESULTADOS         A,
             CNT_CONF_DISTR_ASIENTO B,
             FAC_CANAL_VENTA_BETA   C,
             CNT_CUENTA             D
       WHERE MES = TO_CHAR(P_FECHA_HASTA, 'YYYY-MM')
         AND B.ADPG_EMPR = C.CANB_EMPR(+)
         AND B.ADPG_CANAL = C.CANB_CODIGO(+)
         AND B.ADPG_CUENTA = D.CTAC_CLAVE
         AND B.ADPG_EMPR = D.CTAC_EMPR
         AND B.ADPG_EMPR = P_EMPRESA
         AND B.ADPG_PORCENTAJE > 0;
  BEGIN
  
    SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_ASI_CLAVE FROM DUAL;
  
    SELECT EJ_CODIGO
      INTO V_ASI_EJERCICIO
      FROM CNT_EJERCICIO
     WHERE P_FECHA_HASTA BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
       AND EJ_EMPR = P_EMPRESA;
  
    SELECT NVL(MAX(A.ASI_NRO), 0) + 1
      INTO V_ASI_NRO
      FROM CNT_ASIENTO A
     WHERE A.ASI_EJERCICIO = V_ASI_EJERCICIO
       AND ASI_EMPR = P_EMPRESA;
  
    INSERT INTO CNT_ASIENTO
      (ASI_CLAVE,
       ASI_EMPR,
       ASI_EJERCICIO,
       ASI_NRO,
       ASI_FEC,
       ASI_OBS,
       ASI_FEC_GRAB,
       ASI_LOGIN,
       ASI_SIST,
       ASI_NRO_LISTADO_AUD)
    VALUES
      (V_ASI_CLAVE,
       P_EMPRESA,
       V_ASI_EJERCICIO,
       V_ASI_NRO,
       P_FECHA_HASTA,
       'ASIENTO DE PREVISION PREMIOS GERENCIA',
       SYSDATE,
       P_LOGIN,
       'SIG',
       NULL);
    ---
    V_NRO_ITEM := 0;
  
    FOR R IN PREVISION LOOP
      V_NRO_ITEM := V_NRO_ITEM + 1;
    
      INSERT INTO CNT_ASIENTO_DET
        (ASID_CLAVE_ASI,
         ASID_ITEM,
         ASID_CLAVE_CTACO,
         ASID_IND_DB_CR,
         ASID_IMPORTE,
         ASID_TIPO_MOV,
         ASID_NRO_DOC,
         ASID_DESC,
         ASID_EMPR,
         ASID_CANAL_BETA)
      VALUES
        (V_ASI_CLAVE,
         V_NRO_ITEM,
         R.CTA_CONTABLE,
         'D',
         R.IMPORTE,
         NULL,
         NULL,
         R.CANAL,
         P_EMPRESA,
         R.CANAL_CODIGO);
    
      V_TOTAL_IMPORTE := NVL(V_TOTAL_IMPORTE, 0) + R.IMPORTE;
    
    END LOOP;
  
    V_NRO_ITEM := V_NRO_ITEM + 1;
    INSERT INTO CNT_ASIENTO_DET
      (ASID_CLAVE_ASI,
       ASID_ITEM,
       ASID_CLAVE_CTACO,
       ASID_IND_DB_CR,
       ASID_IMPORTE,
       ASID_TIPO_MOV,
       ASID_NRO_DOC,
       ASID_DESC,
       ASID_EMPR)
    VALUES
      (V_ASI_CLAVE,
       V_NRO_ITEM,
       V_CUENTA_CREDITO,
       'C',
       V_TOTAL_IMPORTE,
       NULL,
       NULL,
       NULL,
       P_EMPRESA);
  
    P_CLAVE_ASIENTO := V_ASI_CLAVE;
  
    UPDATE FIN_PERIODO
       SET PERI_CLAVE_FIN_ASI_FIN = V_ASI_CLAVE
     WHERE PERI_FEC_FIN = P_FECHA_HASTA
       AND PERI_EMPR = P_EMPRESA;
  
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20000, SQLERRM);
  END;

  PROCEDURE STK_ASIENTO_MOVIMIENTO_PRD(P_EMPRESA     IN NUMBER,
                                       P_FECHA_DESDE IN DATE,
                                       P_FECHA_HASTA IN DATE,
                                       P_COD_PERIODO IN NUMBER,
                                       P_LOGIN       IN VARCHAR2) AS
  
    V_ASI_CLAVE     NUMBER;
    V_ASI_EJERCICIO NUMBER;
    V_ASI_NRO       NUMBER;
    V_NRO_ITEM      NUMBER := 0;
    SALIR EXCEPTION;
    V_FEC_INI DATE;
    V_FEC_FIN DATE;
  V_CLAVE_CTA_AJUSTE NUMBER;
  V_TIPO_AJUSTE VARCHAR2(1);
  
  
    CURSOR PRODUCCION IS
      SELECT OPER_DESC,
             MARC_CTA_MERC CTA_CONTABLE, 
             DECODE(OPER_ENT_SAL, 'E', 'D', 'S', 'C') TIPO_DB_CR,
             ROUND(SUM(D.DETA_IMP_NETO_LOC), 0) IMPORTE
        FROM STK_OPERACION     A,
             STK_COSTO_DIARIO  B,
             STK_DOCUMENTO     C,
             STK_DOCUMENTO_DET D,
             STK_ARTICULO      E,
             STK_CLASIFICACION F,
             CNT_CUENTA        G,
             STK_LINEA         H,
             FIN_CONCEPTO      I,
             STK_MARCA         J,
             STK_GRUPO         K
       WHERE OPER_CODIGO = DOCU_CODIGO_OPER
         AND OPER_EMPR = DOCU_EMPR
            
         AND ART_GRUPO = GRUP_CODIGO
         AND ART_LINEA = GRUP_LINEA
         AND ART_EMPR = GRUP_EMPR
            
         AND ART_LINEA = LIN_CODIGO
         AND ART_EMPR = LIN_EMPR
         AND ART_MARCA = MARC_CODIGO
         AND ART_EMPR = MARC_EMPR
            
         AND E.ART_CLASIFICACION = CLAS_CODIGO(+)
         AND E.ART_EMPR = CLAS_EMPR(+)
            
         AND D.DETA_CLAVE_CONCEPTO = I.FCON_CLAVE(+)
         AND D.DETA_EMPR = I.FCON_EMPR(+)
            
         AND FCON_CLAVE_CTACO = CTAC_CLAVE(+)
         AND FCON_EMPR = CTAC_EMPR(+)
            
         AND DETA_ART = CD_ART
         AND DETA_EMPR = CD_EMPR
            
         AND DOCU_FEC_EMIS = CD_FEC
         AND DOCU_EMPR = CD_EMPR
            
         AND OPER_TIPO_ASIENTO = 'PRODUCCION'
            
         AND DOCU_CLAVE = DETA_CLAVE_DOC
         AND DOCU_EMPR = DETA_EMPR
            
         AND DETA_ART = ART_CODIGO
         AND DETA_EMPR = ART_EMPR
            
         AND DOCU_CLAVE_FIN IS NULL
            
         AND ART_TIPO <> 4
         AND NVL(OPER_ENT_SAL, 'N') <> 'N'
            
         AND DOCU_FEC_EMIS BETWEEN V_FEC_INI AND V_FEC_FIN
         AND DOCU_EMPR = P_EMPRESA
       GROUP BY marc_cta_merc,  
                OPER_DESC,
                OPER_ENT_SAL  
       ORDER BY OPER_ENT_SAL DESC 
       ;
  
    CURSOR AJUSTES_DIF IS
    
SELECT SUM(DECODE(OPER_ENT_SAL, 'E', ROUND(D.DETA_IMP_NETO_LOC), 0)) -
       SUM(DECODE(OPER_ENT_SAL, 'S', ROUND(DETA_IMP_NETO_LOC), 0)) diferencia
  FROM STK_OPERACION     A,
       STK_COSTO_DIARIO  B,
       STK_DOCUMENTO     C,
       STK_DOCUMENTO_DET D,
       STK_ARTICULO      E,
       STK_CLASIFICACION F,
       CNT_CUENTA        G,
       STK_LINEA         H,
       FIN_CONCEPTO      I,
       STK_MARCA         J,
       STK_GRUPO         K
 WHERE OPER_CODIGO = DOCU_CODIGO_OPER
   AND OPER_EMPR = DOCU_EMPR
      
   AND ART_GRUPO = GRUP_CODIGO
   AND ART_LINEA = GRUP_LINEA
   AND ART_EMPR = GRUP_EMPR
      
   AND ART_LINEA = LIN_CODIGO
   AND ART_EMPR = LIN_EMPR
   AND ART_MARCA = MARC_CODIGO
   AND ART_EMPR = MARC_EMPR
      
   AND E.ART_CLASIFICACION = CLAS_CODIGO(+)
   AND E.ART_EMPR = CLAS_EMPR(+)
      
   AND D.DETA_CLAVE_CONCEPTO = I.FCON_CLAVE(+)
   AND D.DETA_EMPR = I.FCON_EMPR(+)
      
   AND FCON_CLAVE_CTACO = CTAC_CLAVE(+)
   AND FCON_EMPR = CTAC_EMPR(+)
      
   AND DETA_ART = CD_ART
   AND DETA_EMPR = CD_EMPR
      
   AND DOCU_FEC_EMIS = CD_FEC
   AND DOCU_EMPR = CD_EMPR
      
   AND OPER_TIPO_ASIENTO = 'PRODUCCION'
      
   AND DOCU_CLAVE = DETA_CLAVE_DOC
   AND DOCU_EMPR = DETA_EMPR
      
   AND DETA_ART = ART_CODIGO
   AND DETA_EMPR = ART_EMPR
      
   AND DOCU_CLAVE_FIN IS NULL
      
   AND ART_TIPO <> 4
   AND NVL(OPER_ENT_SAL, 'N') <> 'N'
      
   AND DOCU_FEC_EMIS BETWEEN V_FEC_INI AND V_FEC_FIN
   AND DOCU_EMPR = 1;
  
  BEGIN
    --==ESTO AGREGUE DE NUEVO POR QUE TENIA ALGUNOS PROBLEMA CON LA FECHA
    SELECT PERI_FEC_INI, PERI_FEC_FIN
      INTO V_FEC_INI, V_FEC_FIN
      FROM FIN_PERIODO P
     WHERE P.PERI_EMPR = P_EMPRESA
       AND PERI_CODIGO = P_COD_PERIODO;
    --=========================================================
  
    IF GEN_PACK_SUC.GEN_CONF_IND_SUC = 'S' THEN
      RAISE SALIR;
    END IF;
    ---------------------------------------------------------------------------------------------------
    --ASIENTO EN MONEDA LOCAL (GS)
    ---------------------------------------------------------------------------------------------------
  
    SELECT EJ_CODIGO
      INTO V_ASI_EJERCICIO
      FROM CNT_EJERCICIO
     WHERE V_FEC_INI BETWEEN EJ_FEC_INICIAL AND EJ_FEC_FINAL
       AND EJ_EMPR = P_EMPRESA;
  
    SELECT SEQ_CNT_ASIENTO.NEXTVAL INTO V_ASI_CLAVE FROM DUAL;
  
    SELECT NVL(MAX(A.ASI_NRO), 0) + 1
      INTO V_ASI_NRO
      FROM CNT_ASIENTO A
     WHERE A.ASI_EJERCICIO = V_ASI_EJERCICIO
       AND ASI_EMPR = P_EMPRESA;
    ---
    V_NRO_ITEM := 0;
    FOR V IN PRODUCCION LOOP
      V_NRO_ITEM := V_NRO_ITEM + 1;
    
      IF V_NRO_ITEM = 1 THEN
        --QUE GUARDE LA CABECERA SOLO SI ENTRA EN EL LOOP SOLO POR SI NO HAY PRODUCCION EN EL MES
        INSERT INTO CNT_ASIENTO
          (ASI_CLAVE,
           ASI_EMPR,
           ASI_EJERCICIO,
           ASI_NRO,
           ASI_FEC,
           ASI_OBS,
           ASI_FEC_GRAB,
           ASI_LOGIN,
           ASI_SIST,
           ASI_NRO_LISTADO_AUD)
        VALUES
          (V_ASI_CLAVE,
           P_EMPRESA,
           V_ASI_EJERCICIO,
           V_ASI_NRO,
           V_FEC_FIN,
           'ASIENTOS DE PRODUCCION ' ||
           UPPER(REPLACE(TO_CHAR(V_FEC_INI, 'Month-YYYY'), ' ', '')),
           SYSDATE,
           P_LOGIN,
           'STK',
           NULL);
      END IF;
    
      BEGIN
        INSERT INTO CNT_ASIENTO_DET
          (ASID_CLAVE_ASI,
           ASID_ITEM,
           ASID_CLAVE_CTACO,
           ASID_IND_DB_CR,
           ASID_IMPORTE,
           ASID_TIPO_MOV,
           ASID_NRO_DOC,
           ASID_DESC,
           ASID_EMPR)
        VALUES
          (V_ASI_CLAVE,
           V_NRO_ITEM,
           V.CTA_CONTABLE,
           V.TIPO_DB_CR,
           V.IMPORTE,
           NULL,
           null  ,
           V.OPER_DESC  
           ,
           P_EMPRESA);
      EXCEPTION
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20000, SQLERRM);
      END;
    END LOOP;
  
    FOR R IN AJUSTES_DIF LOOP

IF R.DIFERENCIA<>0   THEN
  
IF R.DIFERENCIA<0 THEN 
  V_TIPO_AJUSTE:='D' ;  --SI LA ENTRADA ES MENOS ENTONCES SE AJUSTA EN EL DEBITO
  ELSE
  V_TIPO_AJUSTE:='C' ; -- SI LA SALIDA ES MENOS ENTONCES SE AJUSTA EN EL CREDITO 
END IF;
 
  --AJUSTAR LA DIFERENCIA CONTRA EL ULTIMO ITEM   
 SELECT ASID_CLAVE_CTACO
   INTO V_CLAVE_CTA_AJUSTE
   FROM (select ASID_CLAVE_CTACO,
                ASID_ITEM,
                MAX(A.ASID_ITEM) OVER (PARTITION BY ASID_CLAVE_ASI) ULTIMO_ITEM
           from CNT_ASIENTO_DET a
          where a.asid_clave_asi = V_ASI_CLAVE
            and asid_empr = p_empresa
            and a.asid_ind_db_cr = V_TIPO_AJUSTE)  
            
  WHERE ASID_ITEM = ULTIMO_ITEM; 
  
      V_NRO_ITEM := V_NRO_ITEM + 1;
    
      BEGIN
        INSERT INTO CNT_ASIENTO_DET
          (ASID_CLAVE_ASI,
           ASID_ITEM,
           ASID_CLAVE_CTACO,
           ASID_IND_DB_CR,
           ASID_IMPORTE,
           ASID_TIPO_MOV,
           ASID_NRO_DOC,
           ASID_DESC,
           ASID_EMPR)
        VALUES
          (V_ASI_CLAVE,
           V_NRO_ITEM,
          V_CLAVE_CTA_AJUSTE,
           V_TIPO_AJUSTE,
           ABS(R.DIFERENCIA),
           NULL,
           NULL,
           'AJUSTES POR DIF. PRODUCCION',
           P_EMPRESA);
      EXCEPTION
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20000, SQLERRM);
      END;
      
  END IF;    
      
      
    END LOOP;
  
  EXCEPTION
    WHEN SALIR THEN
      NULL;
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20000, SQLERRM);
  END;

END CNTP103;
/
