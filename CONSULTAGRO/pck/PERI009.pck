CREATE OR REPLACE PACKAGE PERI009 IS

  -- AUTHOR  : PROGRAMACION9
  -- CREATED : 07/07/2020 15:32:52
  -- PURPOSE :

 FUNCTION FP_CLAVE_DOC           (I_EMPRESA           IN    NUMBER  ) RETURN NUMBER;

 FUNCTION PP_CARGAR_DOCUMENTO_FIN(I_FCON_CLAVE        IN    NUMBER  ,
                                  I_FCON_CLAVE_CTACO  IN    NUMBER  ,
                                  I_FCON_TIPO_SALDO   IN    VARCHAR2,
                                  S_IMPORTE           IN    NUMBER  ,
                                  S_TASA_OFIC         IN    NUMBER  ,
                                  I_VREC_OBS          IN    VARCHAR2,
                                  I_EMPRESA           IN    NUMBER  ,
                                  I_VREC_EMPLEADO     IN    NUMBER  ,
                                  I_VREC_CTABCO       IN    NUMBER  ,
                                  S_TIPO_MOV          IN    NUMBER  ,
                                  I_EMPL_CODIGO_PROV  IN    NUMBER  ,
                                  I_EMPL_NOMBRES      IN    VARCHAR2,
                                  I_VREC_FEC_OPER     IN    DATE    ,
                                  I_VREC_FEC_DOC      IN    DATE    ,
                                  I_VREC_CLAVE        IN    NUMBER  ,
                                  I_VREC_DEPTO         IN NUMBER) RETURN NUMBER;
PROCEDURE PP_ACT_PER_FIN         (I_EMPL_CODIGO_PROV  IN    NUMBER  ,
                                  S_IMPORTE           IN OUT NUMBER ,
                                  I_VREC_EMPLEADO     IN    NUMBER  ,
                                  I_EMPL_NOMBRES      IN    VARCHAR2,
                                  I_VREC_FEC_DOC      IN    DATE    ,
                                  I_EMPRESA           IN    NUMBER  ,
                                  S_VREC_FEC_DOC      IN    DATE    ,
                                  I_VREC_CLAVE        IN    NUMBER  ,
                                  I_FCON_CLAVE        OUT   NUMBER  ,
                                  I_FCON_CODIGO       OUT   NUMBER  ,
                                  I_FCON_CLAVE_CTACO  OUT   NUMBER  ,
                                  I_FCON_TIPO_SALDO   OUT   VARCHAR2,
                                  S_TIPO_MOV          OUT   NUMBER  ,
                                  I_PCON_IND_IPS      OUT   VARCHAR2,
                                  I_PCON_IND_SUMA_IPS OUT   VARCHAR2,
                                  S_IMP_IPS           IN  OUT NUMBER,
                                  S_TASA_OFIC         IN    NUMBER  ,
                                  S_MON               IN    NUMBER  ,
                                  I_EMPL_DEPARTAMENTO IN    NUMBER  ,
                                  W_MON_DEC_IMP       IN    NUMBER  ,
                                  I_VREC_NRO          IN    NUMBER  ,
                                  I_VREC_CLAVE_IPS    OUT   NUMBER  ,
                                  I_VREC_CLAVE_VAC    OUT   NUMBER  ,
                                  I_VREC_OBS          IN    VARCHAR2,
                                  I_VREC_CTABCO       IN    NUMBER  ,
                                  I_VREC_FEC_OPER     IN    DATE    );
PROCEDURE PP_INS_PER_DOC         (IPDOC_CLAVE         IN    NUMBER  ,
                                  IPDOC_QUINCENA      IN    NUMBER  ,
                                  IPDOC_EMPLEADO      IN    NUMBER  ,
                                  IVREC_FEC_DOC       IN    DATE    ,
                                  IPDOC_NRO_DOC       IN    NUMBER  ,
                                  IVREC_FEC_DOC_GRAB  IN    DATE    ,
                                  IPDOC_LOGIN         IN    VARCHAR2,
                                  IPDOC_FORM          IN    VARCHAR2,
                                  IPDOC_PERIODO       IN    NUMBER  ,
                                  IPDOC_CLAVE_FIN     IN    NUMBER  ,
                                  IPDOC_NRO_ITEM      IN    NUMBER  ,
                                  IPDOC_CONCEPTO      IN    NUMBER  ,
                                  IPDOC_IMPORTE       IN    NUMBER  ,
                                  IPDOC_IMPORTE_LOC   IN    NUMBER  ,
                                  IPDOC_MON           IN    NUMBER  ,
                                  IPDOC_DEPARTAMENTO  IN    NUMBER  ,
                                  IPEMPRESA           IN    NUMBER  ,
                                  IPCLAVE_PADRE       IN    NUMBER  );
PROCEDURE PP_BORRAR_FIN          (I_VREC_CLAVE_VAC    IN    NUMBER  ,
                                  I_EMPRESA           IN    NUMBER  ,
                                  I_VREC_CLAVE_IPS    IN    NUMBER  ,
                                  I_VREC_CLAVE        IN    NUMBER  );
PROCEDURE PP_BORRAR_REGISTRO     (I_VREC_EMPLEADO     IN    NUMBER  ,
                                  I_VREC_NRO          IN    NUMBER  ,
                                  I_EMPRESA           IN    NUMBER  ,
                                  I_VREC_CLAVE        IN    NUMBER  ,
                                  I_VREC_CLAVE_VAC    IN    NUMBER  ,
                                  I_VREC_CLAVE_IPS    IN    NUMBER  ,
                                  I_VREC_FEC_DOC      IN    DATE    );
PROCEDURE PP_BUSCAR_EMPLEADO     (I_EMPL_NOMBRES      OUT   VARCHAR2,
                                  I_EMPL_CODIGO_PROV  OUT   NUMBER  ,
                                  I_EMPL_DEPARTAMENTO OUT   NUMBER  ,
                                  I_VREC_EMPLEADO     IN    NUMBER  ,
                                  I_EMPRESA           IN    NUMBER  );
PROCEDURE PP_BUSCAR_DOC_ADEL     (I_DOC_FECHA         OUT   DATE    ,
                                  I_DOC_NRO_DOC       OUT   NUMBER  ,
                                  I_DOC_PROV          OUT   NUMBER  ,
                                  I_DOC_CTABCO        OUT   NUMBER  ,
                                  I_DOC_DIAS_VAC      OUT   NUMBER  ,
                                  I_DOC_IMPORTE       OUT   NUMBER  ,
                                  I_DOC_IMPORTE_IPS   OUT   NUMBER  ,
                                  I_DOC_IMPORTE_TOTAL OUT   NUMBER  ,
                                  I_DOC_OBS           OUT   VARCHAR2,
                                  I_EMPRESA           IN    NUMBER  ,
                                  I_DOC_BCO_DESC      OUT   VARCHAR2,
                                  I_DOC_CTA_DESC      OUT   VARCHAR ,
                                  I_VREC_CLAVE        IN    NUMBER  ,
                                  I_DOC_PROV_DESC     OUT   VARCHAR2);
PROCEDURE PP_BUSCAR_ITEM         (I_VREC_NRO          OUT   NUMBER  ,
                                  S_PERIODO           OUT   VARCHAR2,
                                  I_VREC_EMPLEADO     IN    NUMBER  ,
                                  I_VREC_FEC_DESDE    IN    DATE    ,
                                  I_VREC_FEC_HASTA    IN    DATE    ,
                                  I_EMPRESA           IN    NUMBER  );
PROCEDURE PP_BUSCAR_PLANIFICACION(S_VREC_FEC_DESDE    OUT   DATE    ,
                                  I_VREC_FEC_DESDE    OUT   DATE    ,
                                  I_S_VREC_FEC_HASTA  OUT   DATE    ,
                                  I_VREC_FEC_HASTA    OUT   DATE    ,
                                  I_VREC_DIAS         OUT   NUMBER  ,
                                  I_VREC_EMPLEADO     IN    NUMBER  ,
                                  I_EMPRESA           IN    NUMBER  ,
                                  I_VREC_ANHO         IN    NUMBER  );
PROCEDURE PP_CALC_DIAS_RECIBO    (I_VREC_DIAS_PAG     IN    NUMBER  ,
                                  S_IMPORTE           OUT   NUMBER  ,
                                  S_IMP_DIARIO        IN    NUMBER  );
PROCEDURE PP_CALC_IPS            (I_FCON_CLAVE        OUT   NUMBER  ,
                                  I_FCON_CODIGO       OUT   NUMBER  ,
                                  I_FCON_CLAVE_CTACO  OUT   NUMBER  ,
                                  I_FCON_TIPO_SALDO   OUT   VARCHAR2,
                                  I_S_TIPO_MOV        OUT   NUMBER  ,
                                  I_PCON_IND_IPS      OUT   VARCHAR2,
                                  I_PCON_IND_SUMA_IPS OUT   VARCHAR2,
                                  I_EMPRESA           IN    NUMBER  ,
                                  I_VREC_EMPLEADO     IN    NUMBER  ,
                                  I_S_IMP_IPS         OUT   NUMBER  ,
                                  W_MON_DEC_IMP       IN    NUMBER  ,
                                  I_S_IMPORTE        IN OUT NUMBER  );
PROCEDURE PP_GENERAR_ADEL_PROV   (I_DOC_IMPORTE_TOTAL IN OUT NUMBER ,
                                  I_EMPRESA           IN    NUMBER  ,
                                  I_VREC_FEC_HASTA    IN    DATE    ,
                                  I_DOC_FECHA         OUT   DATE    ,
                                  I_S_VREC_FEC_DOC    IN    DATE    ,
                                  I_DOC_NRO_DOC       OUT   NUMBER  ,
                                  I_VREC_CLAVE        IN    NUMBER  ,
                                  I_DOC_PROV          OUT   NUMBER  ,
                                  I_EMPL_CODIGO_PROV  IN    NUMBER  ,
                                  I_VREC_EMPLEADO     IN    NUMBER  ,
                                  I_DOC_CTABCO        OUT   NUMBER  ,
                                  I_VREC_CTABCO       IN    NUMBER  ,
                                  I_DOC_BCO_DESC      OUT   VARCHAR2,
                                  I_BCO_DESC          IN    VARCHAR2,
                                  I_DOC_CTA_DESC      OUT   VARCHAR2,
                                  I_CTA_DESC          IN    VARCHAR2,
                                  I_S_IMPORTE         IN OUT NUMBER ,
                                  I_VREC_DIAS_PAG     IN    NUMBER  ,
                                  I_DOC_DIAS_VAC      OUT   NUMBER  ,
                                  I_DOC_IMPORTE       OUT   NUMBER  ,
                                  I_DOC_IMPORTE_IPS   OUT   NUMBER  ,
                                  I_S_IMP_IPS         IN OUT NUMBER ,
                                  I_DOC_PROV_DESC     OUT   VARCHAR2,
                                  I_DOC_OBS           OUT   VARCHAR2,
                                  I_VREC_FEC_DESDE    IN DATE);
PROCEDURE PP_TRAER_MONEDA        (I_MON_DESC          OUT   VARCHAR2,
                                  I_W_MON_DEC_IMP     OUT   NUMBER  ,
                                  I_W_MON_DEC_PRECIO  OUT   NUMBER  ,
                                  I_EMPRESA           IN    NUMBER  ,
                                  S_MON               OUT   NUMBER  ,
                                  I_VREC_FEC_DOC      IN    DATE    ,
                                  I_VREC_EMPLEADO     IN    NUMBER  ,
                                  S_TASA_OFIC         OUT   NUMBER  );
PROCEDURE PP_VALIDAR_PERIODO     (FECHA               IN    DATE    ,
                                  I_EMPRESA           IN    NUMBER  ,
                                  W_PERIODO           OUT   NUMBER  );
PROCEDURE PP_VALIDAR_PERIODO_FIN (FECHA               IN    DATE    ,
                                  I_EMPRESA           IN    NUMBER  );
PROCEDURE PP_VERIF_MOSTRAR_CTA_BANC(I_BCO_DESC        OUT   VARCHAR2,
                                  I_CTA_DESC          OUT   VARCHAR2,
                                  S_MON               OUT   NUMBER  ,
                                  I_EMPRESA           IN    NUMBER  ,
                                  I_VREC_CTABCO       IN    NUMBER  );
PROCEDURE PP_CARGAR_CLAVE        (I_EMPRESA           IN    NUMBER  ,
                                  I_VREC_CLAVE        IN OUT NUMBER );
PROCEDURE PP_ACTUALIZAR_REGISTRO (I_VREC_EMPLEADO     IN    NUMBER  ,
                                  I_EMPRESA           IN    NUMBER  ,
                                  I_DOC_CTABCO        IN    NUMBER  ,
                                  I_DOC_FECHA         IN    DATE    ,
                                  I_DOC_IMPORTE       IN    NUMBER  ,
                                  I_DOC_OBS           IN    VARCHAR2,
                                  I_DOC_NRO_DOC       IN    NUMBER  ,
                                  I_DOC_DIAS_VAC      IN    NUMBER  ,
                                  I_DOC_IMPORTE_IPS   IN    NUMBER  ,
                                  I_DOC_IMPORTE_TOTAL IN    NUMBER  ,
                                  I_EMPL_CODIGO_PROV  IN    NUMBER  ,
                                  S_IMPORTE         IN OUT  NUMBER  ,
                                  I_EMPL_NOMBRES      IN    VARCHAR2,
                                  I_VREC_FEC_DOC      IN    DATE    ,
                                  S_VREC_FEC_DOC      IN    DATE    ,
                                  I_VREC_CLAVE        IN    NUMBER  ,
                                  I_FCON_CLAVE        OUT   NUMBER  ,
                                  I_FCON_CODIGO       OUT   NUMBER  ,
                                  I_FCON_CLAVE_CTACO  OUT   NUMBER  ,
                                  I_FCON_TIPO_SALDO   OUT   VARCHAR2,
                                  S_TIPO_MOV          OUT   NUMBER  ,
                                  I_PCON_IND_IPS      OUT   VARCHAR2,
                                  I_PCON_IND_SUMA_IPS OUT   VARCHAR2,
                                  S_IMP_IPS           IN  OUT NUMBER,
                                  S_TASA_OFIC         IN    NUMBER  ,
                                  S_MON               IN    NUMBER  ,
                                  I_EMPL_DEPARTAMENTO IN    NUMBER  ,
                                  W_MON_DEC_IMP       IN    NUMBER  ,
                                  I_VREC_NRO          IN    NUMBER  ,
                                  I_VREC_CLAVE_IPS    OUT   NUMBER  ,
                                  I_VREC_CLAVE_VAC    OUT   NUMBER  ,
                                  I_VREC_OBS          IN    VARCHAR2,
                                  I_VREC_CTABCO       IN    NUMBER  ,
                                  I_VREC_FEC_OPER     IN    DATE    ,
                                  I_VREC_DIAS_PAG     IN    NUMBER  ,
                                  I_VREC_DIAS         IN    NUMBER  ,
                                  I_VREC_FEC_DESDE    IN    DATE    ,
                                  I_VREC_FEC_HASTA    IN    DATE    ,
                                  I_VREC_ANHO         IN OUT NUMBER );
PROCEDURE PP_BORRAR_FIN_2        (I_VREC_CLAVE_VAC    IN    NUMBER  ,
                                  I_EMPRESA           IN    NUMBER  ,
                                  I_VREC_CLAVE_IPS    IN    NUMBER  ,
                                  I_VREC_CLAVE        IN    NUMBER  ,
                                  I_VREC_EMPLEADO     IN    NUMBER  ,
                                  I_VREC_FEC_DOC      IN    DATE    );
FUNCTION CF_RECIBIMOSFORMULA     (I_EMPRESA           IN    NUMBER  ) RETURN VARCHAR2;
FUNCTION CF_TOTAL_IMPORTEFORMULA (I_FORMA_PAGO        IN    NUMBER  ,
                                  I_IMPORTE_VAC       IN    NUMBER  ,
                                  I_IMPORTE_AMH       IN    NUMBER  ,
                                  I_IMPORTE_IPS       IN    NUMBER  ) RETURN NUMBER;
FUNCTION CF_COTIZACIONFORMULA    (I_PDOC_CLAVE_FIN    IN    NUMBER  ,
                                  I_EMPRESA           IN    NUMBER  ) RETURN NUMBER;
FUNCTION CF_SIMBOLO_MONFORMULA   (I_PDOC_MON          IN    NUMBER  ,
                                  I_EMPRESA           IN    NUMBER  ) RETURN VARCHAR2;
FUNCTION CF_DESCRIPCION_VACACIONESFORMU (I_DIAS_VACACIONES IN NUMBER) RETURN VARCHAR2;
PROCEDURE PP_LLAMAR_REPORT       (I_VREC_EMPLEADO     IN    NUMBER  ,
                                  I_EMPRESA           IN    NUMBER  ,
                                  I_VREC_NRO          IN    NUMBER  ,
                                  I_VREC_CLAVE_VAC    IN    NUMBER  ,
                                  I_VREC_CLAVE_IPS    IN    NUMBER  ,
                                  I_SUCURSAL          IN    NUMBER  ,
                                  I_DIAS_VACACIONES   IN    NUMBER  ,
                                  I_DESDE             IN    DATE    ,
                                  I_HASTA             IN    DATE    );

 PROCEDURE PP_BLOQ_PROV_CLI (P_EMPRESA            IN NUMBER,
                             P_LEGAJO             IN NUMBER);

END PERI009;
/
CREATE OR REPLACE PACKAGE BODY PERI009 IS

  PROCEDURE PP_CARGAR_CLAVE(I_EMPRESA    IN NUMBER,
                            I_VREC_CLAVE IN OUT NUMBER) IS
  
    CURSOR PERVACREC IS
      SELECT VREC_CLAVE
        FROM PER_VAC_REC
       WHERE VREC_EMPR = I_EMPRESA
       ORDER BY VREC_CLAVE;
    V_CONT NUMBER := 1;
  BEGIN
    IF I_VREC_CLAVE IS NOT NULL THEN
      RAISE_APPLICATION_ERROR(-20020,
                              'Primero debe aceptar o cancelar los cambios hechos!');
    END IF;
  
    FOR R IN PERVACREC LOOP
      IF R.VREC_CLAVE > V_CONT THEN
        EXIT;
      ELSIF R.VREC_CLAVE < V_CONT THEN
        NULL; --LEER SIG.CLIENTE HASTA EMPATAR CON V_CONT
      ELSE
        V_CONT := V_CONT + 1; --SI CLIENTE = V_CONT ENTONCES AUMENTAR 1 Y LEER SIG.CLIENTE
      END IF;
    END LOOP;
  
    I_VREC_CLAVE := V_CONT; --EL CODIGO V_CONT ESTA LIBRE Y SE PUEDE USAR
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      I_VREC_CLAVE := 0;
  END;

  FUNCTION FP_CLAVE_DOC(I_EMPRESA IN NUMBER) RETURN NUMBER IS
    V_CLAVE NUMBER;
  BEGIN
    SELECT NVL(MAX(PDOC_CLAVE), 0) + 1
      INTO V_CLAVE
      FROM PER_DOCUMENTO
     WHERE PDOC_EMPR = I_EMPRESA;
  
    RETURN V_CLAVE;
  
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001, SQLERRM);
  END;

  PROCEDURE PP_ACT_PER_FIN(I_EMPL_CODIGO_PROV  IN NUMBER,
                           S_IMPORTE           IN OUT NUMBER,
                           I_VREC_EMPLEADO     IN NUMBER,
                           I_EMPL_NOMBRES      IN VARCHAR2,
                           I_VREC_FEC_DOC      IN DATE,
                           I_EMPRESA           IN NUMBER,
                           S_VREC_FEC_DOC      IN DATE,
                           I_VREC_CLAVE        IN NUMBER,
                           I_FCON_CLAVE        OUT NUMBER,
                           I_FCON_CODIGO       OUT NUMBER,
                           I_FCON_CLAVE_CTACO  OUT NUMBER,
                           I_FCON_TIPO_SALDO   OUT VARCHAR2,
                           S_TIPO_MOV          OUT NUMBER,
                           I_PCON_IND_IPS      OUT VARCHAR2,
                           I_PCON_IND_SUMA_IPS OUT VARCHAR2,
                           S_IMP_IPS           IN OUT NUMBER,
                           S_TASA_OFIC         IN NUMBER,
                           S_MON               IN NUMBER,
                           I_EMPL_DEPARTAMENTO IN NUMBER,
                           W_MON_DEC_IMP       IN NUMBER,
                           I_VREC_NRO          IN NUMBER,
                           I_VREC_CLAVE_IPS    OUT NUMBER,
                           I_VREC_CLAVE_VAC    OUT NUMBER,
                           I_VREC_OBS          IN VARCHAR2,
                           I_VREC_CTABCO       IN NUMBER,
                           I_VREC_FEC_OPER     IN DATE) IS
    V_CLAVE NUMBER;
    --V_PRI_CLAVE     NUMBER;
    V_CLAVE_FIN     NUMBER;
    V_QUINCENA      NUMBER;
    V_PERIODO       NUMBER;
    V_CONCEPTO_RRHH NUMBER := 6; --VACACIONES.
    V_CLAVE_PADRE   NUMBER;
    V_FORMA_PAGO    NUMBER;
  BEGIN
    ------------REVISADO ----------------COMPLETO
    V_CLAVE := FP_CLAVE_DOC(I_EMPRESA);
    IF I_EMPL_CODIGO_PROV IS NULL AND S_IMPORTE > 0 THEN
      RAISE_APPLICATION_ERROR(-20002,
                              'El Empleado ' || I_VREC_EMPLEADO || '-' ||
                              I_EMPL_NOMBRES ||
                              ', debe tener un codigo de proveedor');
    END IF;
  
    SELECT PERI_CODIGO
      INTO V_PERIODO
      FROM PER_PERIODO
     WHERE I_VREC_FEC_DOC BETWEEN PERI_FEC_INI AND PERI_FEC_FIN
       AND PERI_EMPR = I_EMPRESA;
  
    V_QUINCENA := SUBSTR(S_VREC_FEC_DOC, 1, 2);
    IF V_QUINCENA <= 15 THEN
      V_QUINCENA := 1;
    ELSE
      V_QUINCENA := 2;
    END IF;
  
    SELECT FCON_CLAVE,
           FCON_CODIGO,
           FCON_CLAVE_CTACO,
           FCON_TIPO_SALDO,
           C.PCON_FIN_TMOV,
           C.PCON_IND_IPS,
           C.PCON_IND_SUMA_IPS
      INTO I_FCON_CLAVE,
           I_FCON_CODIGO,
           I_FCON_CLAVE_CTACO,
           I_FCON_TIPO_SALDO,
           S_TIPO_MOV,
           I_PCON_IND_IPS,
           I_PCON_IND_SUMA_IPS
      FROM PER_EMPLEADO  E,
           PER_CONCEPTO  C,
           PER_DPTO_CONC D,
           FIN_CONCEPTO  CF,
           GEN_EMPRESA   G
     WHERE E.EMPL_DEPARTAMENTO = D.DPTOC_DPTO
       AND C.PCON_CLAVE = D.DPTOC_PER_CONC
       AND D.DPTOC_FIN_CONC = CF.FCON_CLAVE
       AND C.PCON_CLAVE = 6 --VACACIONES
       AND E.EMPL_LEGAJO = I_VREC_EMPLEADO
       AND E.EMPL_EMPRESA = G.EMPR_CODIGO
       AND C.PCON_EMPR = G.EMPR_CODIGO
       AND D.DPTOC_EMPR = G.EMPR_CODIGO
       AND CF.FCON_EMPR = G.EMPR_CODIGO
       AND E.EMPL_EMPRESA = I_EMPRESA;
  
    V_CLAVE_FIN := PERI009.PP_CARGAR_DOCUMENTO_FIN(I_FCON_CLAVE       => I_FCON_CLAVE,
                                                   I_FCON_CLAVE_CTACO => I_FCON_CLAVE_CTACO,
                                                   I_FCON_TIPO_SALDO  => I_FCON_TIPO_SALDO,
                                                   S_IMPORTE          => S_IMPORTE,
                                                   S_TASA_OFIC        => S_TASA_OFIC,
                                                   I_VREC_OBS         => I_VREC_OBS,
                                                   I_EMPRESA          => I_EMPRESA,
                                                   I_VREC_EMPLEADO    => I_VREC_EMPLEADO,
                                                   I_VREC_CTABCO      => I_VREC_CTABCO,
                                                   S_TIPO_MOV         => S_TIPO_MOV,
                                                   I_EMPL_CODIGO_PROV => I_EMPL_CODIGO_PROV,
                                                   I_EMPL_NOMBRES     => I_EMPL_NOMBRES,
                                                   I_VREC_FEC_OPER    => I_VREC_FEC_OPER,
                                                   I_VREC_FEC_DOC     => I_VREC_FEC_DOC,
                                                   I_VREC_CLAVE       => I_VREC_CLAVE,
                                                   I_VREC_DEPTO       => I_EMPL_DEPARTAMENTO);
    --REVISADO
  
    V_CLAVE          := V_CLAVE;
    I_VREC_CLAVE_VAC := V_CLAVE;
    PP_INS_PER_DOC(V_CLAVE, --REVISADO
                   V_QUINCENA,
                   I_VREC_EMPLEADO,
                   I_VREC_FEC_DOC,
                   I_VREC_CLAVE,
                   SYSDATE,
                   GEN_DEVUELVE_USER,
                   'PERI053',
                   V_PERIODO,
                   V_CLAVE_FIN,
                   1,
                   V_CONCEPTO_RRHH,
                   S_IMPORTE,
                   S_IMPORTE * S_TASA_OFIC,
                   S_MON,
                   I_EMPL_DEPARTAMENTO,
                   I_EMPRESA,
                   NULL);
  
    --- HAY DOS TIPOS DE PAGO DE SEGURO MEDICO UNO ES IPS QUE ES EL (9% APORTE OBRERO) (PARA LA MAYORIA DE LOS PERSONALES)
    --- TAMBIEN SE ENCUENTRA EL DE AMH (5% APORTE OBRERO) QUE PERTENECE A LA POBLACION INDIGENA
    --- ENTONCES SE CONSULTA LA FORMA DE PAGO DEL PERSONAL SI ES 5 SE DEBE UTILIZAR  AMH SINO IPS..
    --- ESTOS CAMBIOS SON PEDIDOS DE RRHH - ANDREA (YA QUE CAUSA DIFERENCIA EN SU CAJA)
  
    --I.P.S.
  
    IF NVL(I_PCON_IND_IPS, 'N') = 'S' THEN
    
      --SE BUSCA LA FORMA DE PAGO DEL PERSONAL PARA VER SI EL APORTE SERA IPS O AMH
      SELECT EMPL_FORMA_PAGO
        INTO V_FORMA_PAGO
        FROM PER_EMPLEADO
       WHERE EMPL_EMPRESA = I_EMPRESA
         AND EMPL_LEGAJO = I_VREC_EMPLEADO;
    
      IF V_FORMA_PAGO <> 5 THEN
      
        SELECT FCON_CLAVE,
               FCON_CODIGO,
               FCON_CLAVE_CTACO,
               FCON_TIPO_SALDO,
               C.PCON_FIN_TMOV
          INTO I_FCON_CLAVE,
               I_FCON_CODIGO,
               I_FCON_CLAVE_CTACO,
               I_FCON_TIPO_SALDO,
               S_TIPO_MOV
          FROM PER_EMPLEADO  E,
               PER_CONCEPTO  C,
               PER_DPTO_CONC D,
               FIN_CONCEPTO  CF,
               GEN_EMPRESA   G
         WHERE E.EMPL_DEPARTAMENTO = D.DPTOC_DPTO
           AND C.PCON_CLAVE = D.DPTOC_PER_CONC
           AND D.DPTOC_FIN_CONC = CF.FCON_CLAVE
           AND C.PCON_CLAVE = 4 --IPS
           AND E.EMPL_LEGAJO = I_VREC_EMPLEADO
           AND E.EMPL_EMPRESA = G.EMPR_CODIGO
           AND C.PCON_EMPR = G.EMPR_CODIGO
           AND D.DPTOC_EMPR = G.EMPR_CODIGO
           AND CF.FCON_EMPR = G.EMPR_CODIGO
           AND E.EMPL_EMPRESA = I_EMPRESA;
      
        IF I_PCON_IND_SUMA_IPS = 'S' THEN
          S_IMPORTE := ROUND(S_IMPORTE / 0.91, W_MON_DEC_IMP);
        ELSE
          S_IMPORTE := S_IMPORTE;
        END IF;
      
        S_IMP_IPS := ROUND(S_IMPORTE * 0.09, W_MON_DEC_IMP);
      
        S_IMPORTE   := S_IMP_IPS;
        V_CLAVE_FIN := PERI009.PP_CARGAR_DOCUMENTO_FIN(I_FCON_CLAVE       => I_FCON_CLAVE,
                                                       I_FCON_CLAVE_CTACO => I_FCON_CLAVE_CTACO,
                                                       I_FCON_TIPO_SALDO  => I_FCON_TIPO_SALDO,
                                                       S_IMPORTE          => S_IMPORTE,
                                                       S_TASA_OFIC        => S_TASA_OFIC,
                                                       I_VREC_OBS         => I_VREC_OBS,
                                                       I_EMPRESA          => I_EMPRESA,
                                                       I_VREC_EMPLEADO    => I_VREC_EMPLEADO,
                                                       I_VREC_CTABCO      => I_VREC_CTABCO,
                                                       S_TIPO_MOV         => S_TIPO_MOV,
                                                       I_EMPL_CODIGO_PROV => I_EMPL_CODIGO_PROV,
                                                       I_EMPL_NOMBRES     => I_EMPL_NOMBRES,
                                                       I_VREC_FEC_OPER    => I_VREC_FEC_OPER,
                                                       I_VREC_FEC_DOC     => I_VREC_FEC_DOC,
                                                       I_VREC_CLAVE       => I_VREC_CLAVE,
                                                       I_VREC_DEPTO       => I_EMPL_DEPARTAMENTO );
      
        V_CLAVE_PADRE    := V_CLAVE;
        V_CLAVE          := V_CLAVE + 1;
        I_VREC_CLAVE_IPS := V_CLAVE;
      
        PP_INS_PER_DOC(V_CLAVE,
                       V_QUINCENA,
                       I_VREC_EMPLEADO,
                       I_VREC_FEC_DOC,
                       I_VREC_CLAVE,
                       SYSDATE,
                       GEN_DEVUELVE_USER,
                       'PERI053',
                       V_PERIODO,
                       V_CLAVE_FIN,
                       1,
                       4, --CLAVE DE I.P.S. OBRERO
                       S_IMP_IPS,
                       S_IMP_IPS * S_TASA_OFIC,
                       S_MON,
                       I_EMPL_DEPARTAMENTO,
                       I_EMPRESA,
                       V_CLAVE_PADRE);
      
      ELSE
        --- A PARTIR DE AQUI SI EL APORTE ES AMH
      
        SELECT FCON_CLAVE,
               FCON_CODIGO,
               FCON_CLAVE_CTACO,
               FCON_TIPO_SALDO,
               C.PCON_FIN_TMOV
          INTO I_FCON_CLAVE,
               I_FCON_CODIGO,
               I_FCON_CLAVE_CTACO,
               I_FCON_TIPO_SALDO,
               S_TIPO_MOV
          FROM PER_EMPLEADO  E,
               PER_CONCEPTO  C,
               PER_DPTO_CONC D,
               FIN_CONCEPTO  CF,
               GEN_EMPRESA   G
         WHERE E.EMPL_DEPARTAMENTO = D.DPTOC_DPTO
           AND C.PCON_CLAVE = D.DPTOC_PER_CONC
           AND D.DPTOC_FIN_CONC = CF.FCON_CLAVE
           AND C.PCON_CLAVE = 31 --IPS
           AND E.EMPL_LEGAJO = I_VREC_EMPLEADO
           AND E.EMPL_EMPRESA = G.EMPR_CODIGO
           AND C.PCON_EMPR = G.EMPR_CODIGO
           AND D.DPTOC_EMPR = G.EMPR_CODIGO
           AND CF.FCON_EMPR = G.EMPR_CODIGO
           AND E.EMPL_EMPRESA = I_EMPRESA;
        ----- ESTA PARTE PREGUNTAR A DIEGO COMO SERIA SI ES EL 5 %
        IF I_PCON_IND_SUMA_IPS = 'S' THEN
          S_IMPORTE := ROUND(S_IMPORTE / 0.91, W_MON_DEC_IMP);
        ELSE
          S_IMPORTE := S_IMPORTE;
        END IF;
        --HASTA AQUI
      
        S_IMP_IPS := ROUND(S_IMPORTE * 0.05, W_MON_DEC_IMP);
      
        S_IMPORTE   := S_IMP_IPS;
        V_CLAVE_FIN := PERI009.PP_CARGAR_DOCUMENTO_FIN(I_FCON_CLAVE       => I_FCON_CLAVE,
                                                       I_FCON_CLAVE_CTACO => I_FCON_CLAVE_CTACO,
                                                       I_FCON_TIPO_SALDO  => I_FCON_TIPO_SALDO,
                                                       S_IMPORTE          => S_IMPORTE,
                                                       S_TASA_OFIC        => S_TASA_OFIC,
                                                       I_VREC_OBS         => I_VREC_OBS,
                                                       I_EMPRESA          => I_EMPRESA,
                                                       I_VREC_EMPLEADO    => I_VREC_EMPLEADO,
                                                       I_VREC_CTABCO      => I_VREC_CTABCO,
                                                       S_TIPO_MOV         => S_TIPO_MOV,
                                                       I_EMPL_CODIGO_PROV => I_EMPL_CODIGO_PROV,
                                                       I_EMPL_NOMBRES     => I_EMPL_NOMBRES,
                                                       I_VREC_FEC_OPER    => I_VREC_FEC_OPER,
                                                       I_VREC_FEC_DOC     => I_VREC_FEC_DOC,
                                                       I_VREC_CLAVE       => I_VREC_CLAVE,
                                                       I_VREC_DEPTO       => I_EMPL_DEPARTAMENTO);
      
        V_CLAVE_PADRE    := V_CLAVE;
        V_CLAVE          := V_CLAVE + 1;
        I_VREC_CLAVE_IPS := V_CLAVE;
      
        PP_INS_PER_DOC(V_CLAVE,
                       V_QUINCENA,
                       I_VREC_EMPLEADO,
                       I_VREC_FEC_DOC,
                       I_VREC_CLAVE,
                       SYSDATE,
                       GEN_DEVUELVE_USER,
                       'PERI053',
                       V_PERIODO,
                       V_CLAVE_FIN,
                       1,
                       31, --CLAVE DE A.M.H OBRERO
                       S_IMP_IPS,
                       S_IMP_IPS * S_TASA_OFIC,
                       S_MON,
                       I_EMPL_DEPARTAMENTO,
                       I_EMPRESA,
                       V_CLAVE_PADRE);
      
      END IF;
    
    END IF;
  
    UPDATE PER_EMPLEADO_VACACION
       SET EMPLV_ESTADO = 'C' --CONFIRMADO
     WHERE EMPLV_LEGAJO = I_VREC_EMPLEADO
       AND EMPLV_ITEM = I_VREC_NRO
       AND EMPLV_EMPR = I_EMPRESA;
  
  END;

  FUNCTION PP_CARGAR_DOCUMENTO_FIN(I_FCON_CLAVE       IN NUMBER,
                                   I_FCON_CLAVE_CTACO IN NUMBER,
                                   I_FCON_TIPO_SALDO  IN VARCHAR2,
                                   S_IMPORTE          IN NUMBER,
                                   S_TASA_OFIC        IN NUMBER,
                                   I_VREC_OBS         IN VARCHAR2,
                                   I_EMPRESA          IN NUMBER,
                                   I_VREC_EMPLEADO    IN NUMBER,
                                   I_VREC_CTABCO      IN NUMBER,
                                   S_TIPO_MOV         IN NUMBER,
                                   I_EMPL_CODIGO_PROV IN NUMBER,
                                   I_EMPL_NOMBRES     IN VARCHAR2,
                                   I_VREC_FEC_OPER    IN DATE,
                                   I_VREC_FEC_DOC     IN DATE,
                                   I_VREC_CLAVE       IN NUMBER,
                                   I_VREC_DEPTO       IN NUMBER)
    RETURN NUMBER IS
    V_CLAVE NUMBER := FIN_SEQ_DOC_NEXTVAL;
    --V_ITEM  NUMBER := 1;
  
  BEGIN
    ------REVISADO----- LUEGO DE CARGAR EL APORTE POR VACACION
    ------------------- GUARDA IPS O AMH
  
    INSERT INTO FIN_DOCUMENTO
      (DOC_CLAVE,
       DOC_EMPLEADO,
       DOC_EMPR,
       DOC_CTA_BCO,
       DOC_SUC,
       DOC_TIPO_MOV,
       DOC_NRO_DOC,
       DOC_TIPO_SALDO,
       DOC_MON,
       DOC_PROV,
       DOC_CLI_NOM,
       DOC_FEC_OPER,
       DOC_FEC_DOC,
       DOC_BRUTO_EXEN_LOC,
       DOC_BRUTO_EXEN_MON,
       DOC_BRUTO_GRAV_LOC,
       DOC_BRUTO_GRAV_MON,
       DOC_NETO_EXEN_LOC,
       DOC_NETO_EXEN_MON,
       DOC_NETO_GRAV_LOC,
       DOC_NETO_GRAV_MON,
       DOC_IVA_LOC,
       DOC_IVA_MON,
       DOC_SALDO_INI_MON,
       DOC_SALDO_LOC,
       DOC_SALDO_MON,
       DOC_SALDO_PER_ACT_LOC,
       DOC_SALDO_PER_ACT_MON,
       DOC_OBS,
       DOC_BASE_IMPON_LOC,
       DOC_BASE_IMPON_MON,
       DOC_LOGIN,
       DOC_FEC_GRAB,
       DOC_SIST,
       DOC_OPERADOR)
    VALUES
      (V_CLAVE,
       I_VREC_EMPLEADO,
       I_EMPRESA,
       I_VREC_CTABCO,
       1,
       S_TIPO_MOV,
       I_VREC_CLAVE,
       'C',
       1, ---GUARANIES X DEFECTO
       I_EMPL_CODIGO_PROV,
       SUBSTR(I_EMPL_NOMBRES, 1, 40),
       I_VREC_FEC_OPER,
       I_VREC_FEC_DOC,
       NVL(S_IMPORTE * S_TASA_OFIC, 0),
       NVL(S_IMPORTE, 0),
       0,
       0,
       NVL(S_IMPORTE * S_TASA_OFIC, 0),
       NVL(S_IMPORTE, 0),
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       NULL, --'ANTIC. Personal-Modulo RRHH',
       0,
       0,
       GEN_DEVUELVE_USER,
       SYSDATE,
       'PER',
       2);
       

       
  
    /*IF FCON_TIPO_SALDO='C' THEN
    
      GO_BLOCK('PER_VAC_REC_CAN');
      FIRST_RECORD;
      V_ITEM:=1;
    
      LOOP
    
    
          INSERT INTO FIN_DOC_CONCEPTO
         (DCON_ITEM            ,
          DCON_CLAVE_DOC       ,
          DCON_CLAVE_CONCEPTO  ,
          DCON_CLAVE_CTACO     ,
          DCON_TIPO_SALDO      ,
          DCON_EXEN_LOC        ,
          DCON_EXEN_MON        ,
          DCON_GRAV_LOC        ,
          DCON_GRAV_MON        ,
          DCON_IVA_LOC         ,
          DCON_IVA_MON         ,
          DCON_IND_TIPO_IVA_COMPRA,
          DCON_OBS,
          DCON_CANAL  )
    
         VALUES
        (V_ITEM,
         V_CLAVE,
         FCON_CLAVE,
         FCON_CLAVE_CTACO,
         FCON_TIPO_SALDO ,
         :PER_VAC_REC_CAN.VACAN_IMPORTE*:S_TASA_OFIC,
         :PER_VAC_REC_CAN.VACAN_IMPORTE,
         0,
         0,
         0,
         0,
         1,
         VREC_OBS,
         :PER_VAC_REC_CAN.VACAN_CANAL);
         V_ITEM:=V_ITEM+1;
         EXIT WHEN :SYSTEM.LAST_RECORD='TRUE';
         NEXT_RECORD;
      END LOOP;
    
    ELSE*/
  
    BEGIN
      --  PL_EXHIBIR_ERROR(S_IMPORTE);
    
      INSERT INTO FIN_DOC_CONCEPTO
        (DCON_ITEM,
         DCON_CLAVE_DOC,
         DCON_CLAVE_CONCEPTO,
         DCON_CLAVE_CTACO,
         DCON_TIPO_SALDO,
         DCON_EXEN_LOC,
         DCON_EXEN_MON,
         DCON_GRAV_LOC,
         DCON_GRAV_MON,
         DCON_IVA_LOC,
         DCON_IVA_MON,
         DCON_IND_TIPO_IVA_COMPRA,
         DCON_OBS,
         DCON_EMPR)
      
      VALUES
        (1, --PARA IPS UN UNICO ITEM.
         V_CLAVE,
         I_FCON_CLAVE,
         I_FCON_CLAVE_CTACO,
         I_FCON_TIPO_SALDO,
         S_IMPORTE * S_TASA_OFIC,
         S_IMPORTE,
         0,
         0,
         0,
         0,
         1,
         I_VREC_OBS,
         I_EMPRESA);
         
         
                IF I_EMPRESA = 2 THEN
         
                     FOR R IN (SELECT DISTINCT PERI_CODIGO,
                                        PERI_FEC_INI,
                                        PERI_FEC_FIN,
                                        EMCP_LEGAJO  LEGAJO,
                                        EMCP_SUC     SUCURSAL,
                                        EMCP_DPTO    DPTO,
                                        EMCP_LINEA   LINEA,
                                        EMCP_PORC    PORCENTAJE
                          FROM PER_EMPL_LINEA_PERI A, PER_PERIODO B
                         WHERE A.EMCP_PERIODO = B.PERI_CODIGO
                           AND A.EMCP_EMPR = B.PERI_EMPR
                           AND EMCP_LEGAJO = I_VREC_EMPLEADO-----V_EMPLEADO
                           AND EMCP_DPTO = I_VREC_DEPTO----V_DPTO
                           AND EMCP_EMPR = I_EMPRESA
                           AND I_VREC_FEC_DOC BETWEEN PERI_FEC_INI AND PERI_FEC_FIN

                        ) LOOP
 ---RAISE_APPLICATION_ERROR(-20008, V_CLAVE);
                INSERT INTO FIN_DOC_LINEA_NEGOCIO
                  (DOCL_CLAVE,
                   DOCL_ITEM,
                   DOCL_LINEA_NEGOCIO,
                   DOCL_PORCENTAJE,
                   DOCL_EMPR)
                VALUES
                  (V_CLAVE,
                    1,
                    R.LINEA,
                    R.PORCENTAJE,
                    I_EMPRESA);

              END LOOP;
       
       END IF;
    EXCEPTION
      WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20007, SQLERRM);
    END;
  
    --END IF;
  
    RETURN V_CLAVE;
  
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20008, SQLERRM);
      -- NULL;
  END;

  PROCEDURE PP_INS_PER_DOC(IPDOC_CLAVE        IN NUMBER,
                           IPDOC_QUINCENA     IN NUMBER,
                           IPDOC_EMPLEADO     IN NUMBER,
                           IVREC_FEC_DOC      IN DATE,
                           IPDOC_NRO_DOC      IN NUMBER,
                           IVREC_FEC_DOC_GRAB IN DATE,
                           IPDOC_LOGIN        IN VARCHAR2,
                           IPDOC_FORM         IN VARCHAR2,
                           IPDOC_PERIODO      IN NUMBER,
                           IPDOC_CLAVE_FIN    IN NUMBER,
                           IPDOC_NRO_ITEM     IN NUMBER,
                           IPDOC_CONCEPTO     IN NUMBER,
                           IPDOC_IMPORTE      IN NUMBER,
                           IPDOC_IMPORTE_LOC  IN NUMBER,
                           IPDOC_MON          IN NUMBER,
                           IPDOC_DEPARTAMENTO IN NUMBER,
                           IPEMPRESA          IN NUMBER,
                           IPCLAVE_PADRE      IN NUMBER) IS
  BEGIN
  
    ---REVISADO ---  LUEGO DE GUARDAR APORTE POR VACACION, GUARDA APORTE POR IPS O AMH
    ---------------
  
    INSERT INTO PER_DOCUMENTO
      (PDOC_CLAVE,
       PDOC_QUINCENA,
       PDOC_EMPLEADO,
       PDOC_FEC,
       PDOC_NRO_DOC,
       PDOC_FEC_GRAB,
       PDOC_LOGIN,
       PDOC_FORM,
       PDOC_PERIODO,
       PDOC_CLAVE_FIN,
       PDOC_MON,
       PDOC_DEPARTAMENTO,
       PDOC_EMPR,
       PDOC_CLAVE_PADRE)
    VALUES
      (IPDOC_CLAVE,
       IPDOC_QUINCENA,
       IPDOC_EMPLEADO,
       IVREC_FEC_DOC,
       IPDOC_NRO_DOC,
       IVREC_FEC_DOC_GRAB,
       IPDOC_LOGIN,
       IPDOC_FORM,
       IPDOC_PERIODO,
       IPDOC_CLAVE_FIN,
       IPDOC_MON,
       IPDOC_DEPARTAMENTO,
       IPEMPRESA,
       IPCLAVE_PADRE);
  
    INSERT INTO PER_DOCUMENTO_DET
      (PDDET_CLAVE_DOC,
       PDDET_ITEM,
       PDDET_CLAVE_CONCEPTO,
       PDDET_IMP,
       PDDET_IMP_LOC,
       PDDET_CLAVE_FIN,
       PDDET_EMPR)
    VALUES
      (IPDOC_CLAVE,
       IPDOC_NRO_ITEM,
       IPDOC_CONCEPTO,
       IPDOC_IMPORTE,
       IPDOC_IMPORTE_LOC,
       IPDOC_CLAVE_FIN,
       IPEMPRESA);
  
  END;

  PROCEDURE PP_BORRAR_FIN(I_VREC_CLAVE_VAC IN NUMBER,
                          I_EMPRESA        IN NUMBER,
                          I_VREC_CLAVE_IPS IN NUMBER,
                          I_VREC_CLAVE     IN NUMBER) IS
    V_CLAVE_VAC NUMBER;
    V_CLAVE_IPS NUMBER;
  BEGIN
  
    DELETE FROM PER_RECIB_VAC_ADEL T
     WHERE T.VACADEL_EMPR = I_EMPRESA
       AND T.VACADEL_CLAVE_REC = I_VREC_CLAVE;
  
    --ELIMINAMOS EL MOVIMIENTO 26 CORRESPONDIENTES A LAS VACACIONES
    BEGIN
      SELECT PDOC_CLAVE_FIN
        INTO V_CLAVE_VAC
        FROM PER_DOCUMENTO
       WHERE PDOC_CLAVE = I_VREC_CLAVE_VAC
         AND PDOC_EMPR = I_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
  
    IF NVL(V_CLAVE_VAC, 0) > 0 THEN
      BEGIN
        DELETE FROM PER_DOCUMENTO_DET
         WHERE PDDET_CLAVE_DOC = I_VREC_CLAVE_VAC
           AND PDDET_EMPR = I_EMPRESA;
      
        DELETE FROM PER_DOCUMENTO
         WHERE PDOC_CLAVE = I_VREC_CLAVE_VAC
           AND PDOC_EMPR = I_EMPRESA;
      EXCEPTION
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001, SQLERRM);
      END;
      
       DELETE FROM FIN_DOC_LINEA_NEGOCIO
        WHERE DOCL_CLAVE = V_CLAVE_VAC
          AND DOCL_EMPR = I_EMPRESA;
    
    
      DELETE FROM FIN_DOC_CONCEPTO
       WHERE DCON_CLAVE_DOC = V_CLAVE_VAC
         AND DCON_EMPR = I_EMPRESA;
    
      DELETE FROM FIN_DOCUMENTO
       WHERE DOC_CLAVE = V_CLAVE_VAC
         AND DOC_EMPR = I_EMPRESA;
    
      --ELIMINAMOS EL MOVIMIENTO 49 CORRESPONDIENTE AL APORTE POR IPS O EL MOVIMIENTO 69 CORRESPONDIENTE A AMH
      SELECT PDOC_CLAVE_FIN
        INTO V_CLAVE_IPS
        FROM PER_DOCUMENTO
       WHERE PDOC_CLAVE = I_VREC_CLAVE_IPS
         AND PDOC_EMPR = I_EMPRESA;
    
      DELETE FROM PER_DOCUMENTO_DET
       WHERE PDDET_CLAVE_DOC = I_VREC_CLAVE_IPS
         AND PDDET_EMPR = I_EMPRESA;
    
      DELETE FROM PER_DOCUMENTO
       WHERE PDOC_CLAVE = I_VREC_CLAVE_IPS
         AND PDOC_EMPR = I_EMPRESA;
    
      DELETE FROM FIN_DOC_CONCEPTO
       WHERE DCON_CLAVE_DOC = V_CLAVE_IPS
         AND DCON_EMPR = I_EMPRESA;
    
      DELETE FROM FIN_DOCUMENTO
       WHERE DOC_CLAVE = V_CLAVE_IPS
         AND DOC_EMPR = I_EMPRESA;
    
      --ELIMINAMOS DE LA TABLA DE DE ADELANTOS
    
    END IF;
  
  END;

  PROCEDURE PP_BORRAR_FIN_2(I_VREC_CLAVE_VAC IN NUMBER,
                            I_EMPRESA        IN NUMBER,
                            I_VREC_CLAVE_IPS IN NUMBER,
                            I_VREC_CLAVE     IN NUMBER,
                            I_VREC_EMPLEADO  IN NUMBER,
                            I_VREC_FEC_DOC   IN DATE) IS
    V_CLAVE_VAC NUMBER;
    V_CLAVE_IPS NUMBER;
  
    --------------------------------------*** ADELANTO PROVEEDOR
    V_CLAVE_ADELANTO     NUMBER;
    V_CLAVE_DEV_ADELANTO NUMBER;
    FAC_CLAVE_IMAGEN     NUMBER;
    V_CLAVE_DOC_ADELANTO NUMBER;
    V_CLAVE_PER_ADELANTO NUMBER;
    V_CLAVE_DOC_IPS      NUMBER;
    V_CLAVE_PER_IPS      NUMBER;
    V_CLAVE_DOC_VAC      NUMBER;
    V_CLAVE_PER_VAC      NUMBER;
    V_PERIODO            NUMBER;
  BEGIN
    --   RAISE_APPLICATION_ERROR(-20010,I_VREC_CLAVE_VAC);
    BEGIN
      SELECT PDOC_CLAVE_FIN
        INTO V_CLAVE_VAC
        FROM PER_DOCUMENTO
       WHERE PDOC_CLAVE = I_VREC_CLAVE_VAC
         AND PDOC_EMPR = I_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
  
    IF NVL(V_CLAVE_VAC, 0) > 0 THEN
    
      BEGIN
        DELETE FROM PER_DOCUMENTO_DET
         WHERE PDDET_CLAVE_DOC = I_VREC_CLAVE_VAC
           AND PDDET_EMPR = I_EMPRESA;
      
        DELETE FROM PER_DOCUMENTO
         WHERE PDOC_CLAVE = I_VREC_CLAVE_VAC
           AND PDOC_EMPR = I_EMPRESA;
      EXCEPTION
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20102, SQLERRM);
          -- PL_EXHIBIR_ERROR('nooooooo');
      END;
    
     DELETE FROM FIN_DOC_LINEA_NEGOCIO
        WHERE DOCL_CLAVE = V_CLAVE_VAC
          AND DOCL_EMPR = I_EMPRESA;
    
      DELETE FROM FIN_DOC_CONCEPTO
       WHERE DCON_CLAVE_DOC = V_CLAVE_VAC
         AND DCON_EMPR = I_EMPRESA;
    
      DELETE FROM FIN_DOCUMENTO
       WHERE DOC_CLAVE = V_CLAVE_VAC
         AND DOC_EMPR = I_EMPRESA;
    
      --ELIMINAMOS EL MOVIMIENTO 49 CORRESPONDIENTE AL APORTE POR IPS.
      SELECT PDOC_CLAVE_FIN
        INTO V_CLAVE_IPS
        FROM PER_DOCUMENTO
       WHERE PDOC_CLAVE = I_VREC_CLAVE_IPS
         AND PDOC_EMPR = I_EMPRESA;
    
      DELETE FROM PER_DOCUMENTO_DET
       WHERE PDDET_CLAVE_DOC = I_VREC_CLAVE_IPS
         AND PDDET_EMPR = I_EMPRESA;
    
      DELETE FROM PER_DOCUMENTO
       WHERE PDOC_CLAVE = I_VREC_CLAVE_IPS
         AND PDOC_EMPR = I_EMPRESA;
    
    
    DELETE FROM FIN_DOC_LINEA_NEGOCIO
        WHERE DOCL_CLAVE = V_CLAVE_IPS
          AND DOCL_EMPR = I_EMPRESA;
          
          
      DELETE FROM FIN_DOC_CONCEPTO
       WHERE DCON_CLAVE_DOC = V_CLAVE_IPS
         AND DCON_EMPR = I_EMPRESA;
    
      DELETE FROM FIN_DOCUMENTO
       WHERE DOC_CLAVE = V_CLAVE_IPS
         AND DOC_EMPR = I_EMPRESA;
    
    END IF;
  
    -- VERIFICAMOS SI EL REGISTRO QUE SE VA A ELIMINAR TIENE  ADELANTO PROVEEDOR Y DEVOLUCION DE ADELANTO PROVEEDOR
  
    BEGIN
      SELECT DECODE(I_EMPRESA,
                    1,
                    T.VACADEL_CLAVE_DOC_ADEL,
                    T.VACADEL_CLA_FDOC_ADEL),
             T.VACADEL_CLAVE_DOC_DEV
        INTO V_CLAVE_ADELANTO, V_CLAVE_DEV_ADELANTO
        FROM PER_RECIB_VAC_ADEL T
       WHERE VACADEL_EMPR = I_EMPRESA
         AND VACADEL_CLAVE_REC = I_VREC_CLAVE
         AND T.VACADEL_EMPL_LEG = I_VREC_EMPLEADO;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
    begin
    SELECT PERI_CODIGO + 1
      INTO V_PERIODO
      FROM PER_PERIODO
     WHERE I_VREC_FEC_DOC BETWEEN PERI_FEC_INI AND PERI_FEC_FIN
       AND PERI_EMPR = I_EMPRESA; ---PARA ELIMINAR LOS DATOS DEL PRIODO SIGUENTE
   exception
       when no_data_found then 
         null;
          --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
   end;
  
    IF NVL(V_CLAVE_ADELANTO, 0) > 0 THEN
      ---PRIMERO ELIMINAMOS ESTE REGISTRO O SINO DESPUES NO DEJA ELIMINAR NADA;
      DELETE FROM PER_RECIB_VAC_ADEL
       WHERE VACADEL_CLAVE_REC = I_VREC_CLAVE
         AND VACADEL_EMPR = I_EMPRESA;
    END IF;
    -------------------PRIMERO ELIMINAMOS TODO LO QUE TENGA QUE VER CON DEVOLUCION DE ADELANTO--------
    IF NVL(V_CLAVE_DEV_ADELANTO, 0) > 0 THEN
    
      --------------------------------------APORTE IPS DEL SIGUIENTE PERIODO
      BEGIN
        SELECT PDOC_CLAVE, S.PDDET_CLAVE_FIN
          INTO V_CLAVE_PER_IPS, V_CLAVE_DOC_IPS
          FROM PER_DOCUMENTO T, PER_DOCUMENTO_DET S
         WHERE PDOC_EMPLEADO = I_VREC_EMPLEADO
           AND T.PDOC_EMPR = S.PDDET_EMPR
           AND T.PDOC_CLAVE = S.PDDET_CLAVE_DOC
           AND PDOC_NRO_DOC = I_VREC_CLAVE
           AND PDOC_EMPR = I_EMPRESA
           AND PDOC_PERIODO = V_PERIODO
           AND S.PDDET_CLAVE_CONCEPTO = 4;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
      END;
    
      IF NVL(V_CLAVE_DOC_IPS, 0) > 0 THEN
        DELETE FROM PER_DOCUMENTO_DET
         WHERE PDDET_CLAVE_DOC = V_CLAVE_PER_IPS
           AND PDDET_EMPR = I_EMPRESA;
      
        DELETE FROM PER_DOCUMENTO
         WHERE PDOC_CLAVE = V_CLAVE_PER_IPS
           AND PDOC_EMPR = I_EMPRESA;
      
      
        DELETE FROM FIN_DOC_LINEA_NEGOCIO
        WHERE DOCL_CLAVE = V_CLAVE_DOC_IPS
          AND DOCL_EMPR = I_EMPRESA;
          
        DELETE FROM FIN_DOC_CONCEPTO
         WHERE DCON_CLAVE_DOC = V_CLAVE_DOC_IPS
           AND DCON_EMPR = I_EMPRESA;
      
        DELETE FROM FIN_DOCUMENTO
         WHERE DOC_CLAVE = V_CLAVE_DOC_IPS
           AND DOC_EMPR = I_EMPRESA;
      END IF;
    
      --------------------------------------APORTE VACACION DEL SIGUIENTE PERIODO
      BEGIN
        SELECT PDOC_CLAVE, S.PDDET_CLAVE_FIN
          INTO V_CLAVE_PER_VAC, V_CLAVE_DOC_VAC
          FROM PER_DOCUMENTO T, PER_DOCUMENTO_DET S
         WHERE PDOC_EMPLEADO = I_VREC_EMPLEADO
           AND T.PDOC_EMPR = S.PDDET_EMPR
           AND T.PDOC_CLAVE = S.PDDET_CLAVE_DOC
           AND PDOC_NRO_DOC = I_VREC_CLAVE
           AND PDOC_EMPR = I_EMPRESA
           AND PDOC_PERIODO = V_PERIODO
           AND S.PDDET_CLAVE_CONCEPTO = 6;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
      END;
    
      IF NVL(V_CLAVE_DOC_VAC, 0) > 0 THEN
        DELETE FROM PER_DOCUMENTO_DET
         WHERE PDDET_CLAVE_DOC = V_CLAVE_PER_VAC
           AND PDDET_EMPR = I_EMPRESA;
      
        DELETE FROM PER_DOCUMENTO
         WHERE PDOC_CLAVE = V_CLAVE_PER_VAC
           AND PDOC_EMPR = I_EMPRESA;
      
         DELETE FROM FIN_DOC_LINEA_NEGOCIO
        WHERE DOCL_CLAVE = V_CLAVE_DOC_VAC
          AND DOCL_EMPR = I_EMPRESA;
          
        DELETE FROM FIN_DOC_CONCEPTO
         WHERE DCON_CLAVE_DOC = V_CLAVE_DOC_VAC
           AND DCON_EMPR = I_EMPRESA;
      
        DELETE FROM FIN_DOCUMENTO
         WHERE DOC_CLAVE = V_CLAVE_DOC_VAC
           AND DOC_EMPR = I_EMPRESA;
      
      END IF;
    
      DELETE FROM PER_RECIB_VAC_ADEL
       WHERE VACADEL_CLAVE_REC = I_VREC_CLAVE
         AND VACADEL_EMPR = I_EMPRESA;
    
      -----ELIMINAMOS TODO LO QUE TENGA QUE VER CON EL MOV 33
      DELETE FROM FIN_PAGO
       WHERE PAG_CLAVE_DOC = V_CLAVE_ADELANTO
         AND PAG_CLAVE_PAGO = V_CLAVE_DEV_ADELANTO
         AND PAG_EMPR = I_EMPRESA;
    
    
       
         DELETE FROM FIN_DOC_LINEA_NEGOCIO
        WHERE DOCL_CLAVE = V_CLAVE_DEV_ADELANTO
          AND DOCL_EMPR = I_EMPRESA;
          
          
      DELETE FROM FIN_DOC_CONCEPTO
       WHERE DCON_CLAVE_DOC = V_CLAVE_DEV_ADELANTO
         AND DCON_EMPR = I_EMPRESA;
    
      DELETE FROM FIN_DOCUMENTO
       WHERE DOC_CLAVE = V_CLAVE_DEV_ADELANTO
         AND DOC_EMPR = I_EMPRESA;
    
    END IF;
    -----------------ELIMINAMOS TODO LOS QUE TENGA QUE VER CON EL ADELANTO PROVEEDOR-------------------------------
  
    IF NVL(V_CLAVE_ADELANTO, 0) > 0 THEN
    
      BEGIN
        SELECT FAC_CLAVE
          INTO FAC_CLAVE_IMAGEN
          FROM COM_FACTURA_REC
         WHERE FAC_CLAVE_DOC_FIN = V_CLAVE_ADELANTO
           AND FAC_EMPR = I_EMPRESA;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
      END;
    
      IF NVL(FAC_CLAVE_IMAGEN, 0) > 0 THEN
        DELETE FROM COM_FACTURA_IMAGEN
         WHERE FAC_CLAVE = FAC_CLAVE_IMAGEN
           AND FAC_EMPR = I_EMPRESA;
      
        DELETE FROM COM_FACTURA_REC
         WHERE FAC_CLAVE = FAC_CLAVE_IMAGEN
           AND FAC_EMPR = I_EMPRESA;
      END IF;
    
      ---ELIMINAMOS TODO LO QUE SE CREO PARA EL ADELANTO A PROVEEDOR
      --------------------------------------ADELANTO VACACION
      BEGIN
        SELECT PDOC_CLAVE, NVL(S.PDDET_CLAVE_FIN, T.PDOC_CLAVE_FIN)
          INTO V_CLAVE_PER_ADELANTO, V_CLAVE_DOC_ADELANTO
          FROM PER_DOCUMENTO T, PER_DOCUMENTO_DET S
         WHERE PDOC_EMPLEADO = I_VREC_EMPLEADO
           AND T.PDOC_EMPR = S.PDDET_EMPR
           AND T.PDOC_CLAVE = S.PDDET_CLAVE_DOC
           AND PDOC_NRO_DOC = I_VREC_CLAVE
           AND PDOC_EMPR = I_EMPRESA
           AND S.PDDET_CLAVE_CONCEPTO = DECODE(I_EMPRESA, 2, 40, 35);
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
      END;
    
      IF NVL(V_CLAVE_PER_ADELANTO, 0) > 0 THEN
        ----ELIMINAMOES EL CONCEPTO 40 EN FINANZAS
        DELETE FROM PER_DOCUMENTO_DET
         WHERE PDDET_CLAVE_DOC = V_CLAVE_PER_ADELANTO
           AND PDDET_EMPR = I_EMPRESA;
      
        DELETE FROM PER_DOCUMENTO
         WHERE PDOC_CLAVE = V_CLAVE_PER_ADELANTO
           AND PDOC_EMPR = I_EMPRESA;
      END IF;
    
      DELETE FROM FIN_CUOTA
       WHERE CUO_CLAVE_DOC = V_CLAVE_ADELANTO
         AND CUO_EMPR = I_EMPRESA;
    
       DELETE FROM FIN_DOC_LINEA_NEGOCIO
        WHERE DOCL_CLAVE = V_CLAVE_ADELANTO
          AND DOCL_EMPR = I_EMPRESA;
          
      DELETE FROM FIN_DOC_CONCEPTO
       WHERE DCON_CLAVE_DOC = V_CLAVE_ADELANTO
         AND DCON_EMPR = I_EMPRESA;
    
      DELETE FROM FIN_DOCUMENTO
       WHERE DOC_CLAVE = V_CLAVE_ADELANTO
         AND DOC_EMPR = I_EMPRESA;
    
    END IF;
    COMMIT;
  END;

  PROCEDURE PP_BORRAR_REGISTRO(I_VREC_EMPLEADO  IN NUMBER,
                               I_VREC_NRO       IN NUMBER,
                               I_EMPRESA        IN NUMBER,
                               I_VREC_CLAVE     IN NUMBER,
                               I_VREC_CLAVE_VAC IN NUMBER,
                               I_VREC_CLAVE_IPS IN NUMBER,
                               I_VREC_FEC_DOC   IN DATE) IS
  BEGIN
  
    DECLARE
      --V_MESSAGE VARCHAR2(100) := '?BORRAR LOS DATOS?';
      SALIR EXCEPTION;
    
    BEGIN
    
      UPDATE PER_EMPLEADO_VACACION
         SET EMPLV_ESTADO = 'P' --VUELVE A PONER EN PENDIENTE.
       WHERE EMPLV_LEGAJO = I_VREC_EMPLEADO
         AND EMPLV_ITEM = I_VREC_NRO
         AND EMPLV_EMPR = I_EMPRESA;
      -- RAISE_APPLICATION_ERROR(-20010,I_VREC_CLAVE_VAC);
      /* PERI009.PP_BORRAR_FIN(I_VREC_CLAVE_VAC => I_VREC_CLAVE_VAC,
      I_EMPRESA        => I_EMPRESA,
      I_VREC_CLAVE_IPS => I_VREC_CLAVE_IPS,
      I_VREC_CLAVE     => I_VREC_CLAVE);*/
    
      BEGIN
        -- CALL THE PROCEDURE
        PERI009.PP_BORRAR_FIN_2(I_VREC_CLAVE_VAC => I_VREC_CLAVE_VAC,
                                I_EMPRESA        => I_EMPRESA,
                                I_VREC_CLAVE_IPS => I_VREC_CLAVE_IPS,
                                I_VREC_CLAVE     => I_VREC_CLAVE,
                                I_VREC_EMPLEADO  => I_VREC_EMPLEADO,
                                I_VREC_FEC_DOC   => I_VREC_FEC_DOC);
      END;
    
      DELETE PER_VAC_REC
       WHERE VREC_CLAVE = I_VREC_CLAVE
         AND VREC_EMPR = I_EMPRESA;
    
      COMMIT;
    EXCEPTION
      WHEN SALIR THEN
        NULL;
    END;
  END;

  PROCEDURE PP_BUSCAR_DOC_ADEL(I_DOC_FECHA         OUT DATE,
                               I_DOC_NRO_DOC       OUT NUMBER,
                               I_DOC_PROV          OUT NUMBER,
                               I_DOC_CTABCO        OUT NUMBER,
                               I_DOC_DIAS_VAC      OUT NUMBER,
                               I_DOC_IMPORTE       OUT NUMBER,
                               I_DOC_IMPORTE_IPS   OUT NUMBER,
                               I_DOC_IMPORTE_TOTAL OUT NUMBER,
                               I_DOC_OBS           OUT VARCHAR2,
                               I_EMPRESA           IN NUMBER,
                               I_DOC_BCO_DESC      OUT VARCHAR2,
                               I_DOC_CTA_DESC      OUT VARCHAR,
                               I_VREC_CLAVE        IN NUMBER,
                               I_DOC_PROV_DESC     OUT VARCHAR2) IS
  BEGIN
    BEGIN
      SELECT T.VACADEL_FECHA_DOC,
             T.VACADEL_NRO_DOC,
             T.VACADEL_COD_PROV,
             T.VACADEL_CAJA_BCO,
             T.VACADEL_DIAS,
             T.VACADEL_IMPORTE,
             T.VACADEL_IPS,
             T.VACADEL_TOTAL_IMP,
             T.VACADEL_OBS
        INTO I_DOC_FECHA,
             I_DOC_NRO_DOC,
             I_DOC_PROV,
             I_DOC_CTABCO,
             I_DOC_DIAS_VAC,
             I_DOC_IMPORTE,
             I_DOC_IMPORTE_IPS,
             I_DOC_IMPORTE_TOTAL,
             I_DOC_OBS
        FROM PER_RECIB_VAC_ADEL T
       WHERE T.VACADEL_EMPR = I_EMPRESA
         AND T.VACADEL_CLAVE_REC = I_VREC_CLAVE;
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        I_DOC_FECHA         := NULL;
        I_DOC_NRO_DOC       := NULL;
        I_DOC_PROV          := NULL;
        I_DOC_CTABCO        := NULL;
        I_DOC_DIAS_VAC      := NULL;
        I_DOC_IMPORTE       := NULL;
        I_DOC_IMPORTE_IPS   := NULL;
        I_DOC_IMPORTE_TOTAL := NULL;
        I_DOC_OBS           := NULL;
        NULL;
    END;
  
    BEGIN
      SELECT BCO_DESC, CTA_DESC
        INTO I_DOC_BCO_DESC, I_DOC_CTA_DESC
        FROM FIN_CUENTA_BANCARIA, FIN_BANCO, GEN_EMPRESA
       WHERE CTA_BCO = BCO_CODIGO(+)
         AND CTA_EMPR = BCO_EMPR(+)
         AND CTA_EMPR = I_EMPRESA
         AND CTA_EMPR = EMPR_CODIGO
         AND CTA_CODIGO = I_DOC_CTABCO;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        I_DOC_BCO_DESC := NULL;
        I_DOC_CTA_DESC := NULL;
        NULL;
    END;
  
    BEGIN
      SELECT P.PROV_RAZON_SOCIAL
        INTO I_DOC_PROV_DESC
        FROM FIN_PROVEEDOR P
       WHERE P.PROV_EMPR = I_EMPRESA
         AND P.PROV_CODIGO = I_DOC_PROV;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        I_DOC_PROV_DESC := NULL;
        NULL;
    END;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
  END;

  PROCEDURE PP_BUSCAR_EMPLEADO(I_EMPL_NOMBRES      OUT VARCHAR2,
                               I_EMPL_CODIGO_PROV  OUT NUMBER,
                               I_EMPL_DEPARTAMENTO OUT NUMBER,
                               I_VREC_EMPLEADO     IN NUMBER,
                               I_EMPRESA           IN NUMBER) IS
  BEGIN
    SELECT EMPL_NOMBRE || ' ' || EMPL_APE,
           EMPL_CODIGO_PROV,
           EMPL_DEPARTAMENTO
      INTO I_EMPL_NOMBRES, I_EMPL_CODIGO_PROV, I_EMPL_DEPARTAMENTO
      FROM PER_EMPLEADO
     WHERE EMPL_LEGAJO = I_VREC_EMPLEADO
       AND EMPL_EMPRESA = I_EMPRESA;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20005, 'No existe este empleado');
  END;

  PROCEDURE PP_BUSCAR_ITEM(I_VREC_NRO       OUT NUMBER,
                           S_PERIODO        OUT VARCHAR2,
                           I_VREC_EMPLEADO  IN NUMBER,
                           I_VREC_FEC_DESDE IN DATE,
                           I_VREC_FEC_HASTA IN DATE,
                           I_EMPRESA        IN NUMBER) IS
  BEGIN
  
    SELECT EMPLV_ITEM, EMPLV_PERIODO
      INTO I_VREC_NRO, S_PERIODO
      FROM PER_EMPLEADO_VACACION
     WHERE EMPLV_LEGAJO = I_VREC_EMPLEADO
       AND EMPLV_FEC_DESDE = TO_CHAR(I_VREC_FEC_DESDE, 'DD/MM/YYYY')
       AND EMPLV_FEC_HASTA = TO_CHAR(I_VREC_FEC_HASTA, 'DD/MM/YYYY')
       AND EMPLV_EMPR = I_EMPRESA;
    --COMMIT;
    --  RAISE_APPLICATION_ERROR(-20020,'aca esta la paa');
  END;

  PROCEDURE PP_BUSCAR_PLANIFICACION(S_VREC_FEC_DESDE   OUT DATE,
                                    I_VREC_FEC_DESDE   OUT DATE,
                                    I_S_VREC_FEC_HASTA OUT DATE,
                                    I_VREC_FEC_HASTA   OUT DATE,
                                    I_VREC_DIAS        OUT NUMBER,
                                    I_VREC_EMPLEADO    IN NUMBER,
                                    I_EMPRESA          IN NUMBER,
                                    I_VREC_ANHO        IN NUMBER) IS
  BEGIN
    SELECT TO_CHAR(PLVAC_FEC_DESDE, 'dd-mm-yyyy'),
           PLVAC_FEC_DESDE,
           TO_CHAR(PLVAC_FEC_HASTA, 'dd-mm-yyyy'),
           PLVAC_FEC_HASTA,
           PLVAC_DIAS
      INTO S_VREC_FEC_DESDE,
           I_VREC_FEC_DESDE,
           I_S_VREC_FEC_HASTA,
           I_VREC_FEC_HASTA,
           I_VREC_DIAS
      FROM PER_VACACIONES
     WHERE PLVAC_EMPLEADO = I_VREC_EMPLEADO
       AND PLVAC_ANO = I_VREC_ANHO
       AND TO_CHAR(PLVAC_FEC_DESDE, 'dd-mm-yyyy') =
           TO_CHAR(I_VREC_FEC_DESDE, 'dd-mm-yyyy')
       AND PLVAC_EMPR = I_EMPRESA;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20006,
                              'No existe planificacion para este empleado en este a?o y con esta fecha de inicio!');
  END;

  PROCEDURE PP_CALC_DIAS_RECIBO(I_VREC_DIAS_PAG IN NUMBER,
                                S_IMPORTE       OUT NUMBER,
                                S_IMP_DIARIO    IN NUMBER) IS
    V_DIAS_RECIBO NUMBER;
  BEGIN
  
    V_DIAS_RECIBO := I_VREC_DIAS_PAG;
    S_IMPORTE     := V_DIAS_RECIBO * NVL(S_IMP_DIARIO, 0);
  
  END;

  PROCEDURE PP_CALC_IPS(I_FCON_CLAVE        OUT NUMBER,
                        I_FCON_CODIGO       OUT NUMBER,
                        I_FCON_CLAVE_CTACO  OUT NUMBER,
                        I_FCON_TIPO_SALDO   OUT VARCHAR2,
                        I_S_TIPO_MOV        OUT NUMBER,
                        I_PCON_IND_IPS      OUT VARCHAR2,
                        I_PCON_IND_SUMA_IPS OUT VARCHAR2,
                        I_EMPRESA           IN NUMBER,
                        I_VREC_EMPLEADO     IN NUMBER,
                        I_S_IMP_IPS         OUT NUMBER,
                        W_MON_DEC_IMP       IN NUMBER,
                        I_S_IMPORTE         IN OUT NUMBER) IS
    P_EMPL_FOR_PAGO NUMBER;
  
    --- HAY DOS TIPOS DE PAGO DE SEGURO MEDICO UNO ES IPS QUE ES EL (9% APORTE OBRERO) (PARA LA MAYORIA DE LOS PERSONALES)
    --- TAMBIEN SE ENCUENTRA EL DE AMH (5% APORTE OBRERO) QUE PERTENECE A LA POBLACION INDIGENA
    --- ENTONCES SE CONSULTA LA FORMA DE PAGO DEL PERSONAL SI ES 5 SE DEBE UTILIZAR  AMH SINO IPS..
    --- ESTOS CAMBIOS SE PIDIO DE RRHH - ANDREA (YA QUE CAUSA DIFERENCIA EN SU CAJA)
  BEGIN
  
    SELECT FCON_CLAVE,
           FCON_CODIGO,
           FCON_CLAVE_CTACO,
           FCON_TIPO_SALDO,
           C.PCON_FIN_TMOV,
           C.PCON_IND_IPS,
           C.PCON_IND_SUMA_IPS,
           E.EMPL_FORMA_PAGO --(SE AGREGO ESTA COLUMNA PARA SABER LA FORMA DE PAGO)
      INTO I_FCON_CLAVE,
           I_FCON_CODIGO,
           I_FCON_CLAVE_CTACO,
           I_FCON_TIPO_SALDO,
           I_S_TIPO_MOV,
           I_PCON_IND_IPS,
           I_PCON_IND_SUMA_IPS,
           P_EMPL_FOR_PAGO
      FROM PER_EMPLEADO  E,
           PER_CONCEPTO  C,
           PER_DPTO_CONC D,
           FIN_CONCEPTO  CF,
           GEN_EMPRESA   G
     WHERE E.EMPL_DEPARTAMENTO = D.DPTOC_DPTO
       AND C.PCON_CLAVE = D.DPTOC_PER_CONC
       AND D.DPTOC_FIN_CONC = CF.FCON_CLAVE
       AND C.PCON_CLAVE = 6 --VACACIONES
       AND E.EMPL_LEGAJO = I_VREC_EMPLEADO
       AND E.EMPL_EMPRESA = G.EMPR_CODIGO
       AND C.PCON_EMPR = G.EMPR_CODIGO
       AND D.DPTOC_EMPR = G.EMPR_CODIGO
       AND CF.FCON_EMPR = G.EMPR_CODIGO
       AND G.EMPR_CODIGO = I_EMPRESA;
  
    IF P_EMPL_FOR_PAGO <> 5 THEN
    
      SELECT FCON_CLAVE,
             FCON_CODIGO,
             FCON_CLAVE_CTACO,
             FCON_TIPO_SALDO,
             C.PCON_FIN_TMOV
        INTO I_FCON_CLAVE,
             I_FCON_CODIGO,
             I_FCON_CLAVE_CTACO,
             I_FCON_TIPO_SALDO,
             I_S_TIPO_MOV
        FROM PER_EMPLEADO  E,
             PER_CONCEPTO  C,
             PER_DPTO_CONC D,
             FIN_CONCEPTO  CF,
             GEN_EMPRESA   G
       WHERE E.EMPL_DEPARTAMENTO = D.DPTOC_DPTO
         AND C.PCON_CLAVE = D.DPTOC_PER_CONC
         AND D.DPTOC_FIN_CONC = CF.FCON_CLAVE
         AND C.PCON_CLAVE = 4 --IPS
         AND E.EMPL_LEGAJO = I_VREC_EMPLEADO
         AND E.EMPL_EMPRESA = G.EMPR_CODIGO
         AND C.PCON_EMPR = G.EMPR_CODIGO
         AND D.DPTOC_EMPR = G.EMPR_CODIGO
         AND CF.FCON_EMPR = G.EMPR_CODIGO
         AND G.EMPR_CODIGO = I_EMPRESA;
    ELSE
    
      SELECT FCON_CLAVE,
             FCON_CODIGO,
             FCON_CLAVE_CTACO,
             FCON_TIPO_SALDO,
             C.PCON_FIN_TMOV
        INTO I_FCON_CLAVE,
             I_FCON_CODIGO,
             I_FCON_CLAVE_CTACO,
             I_FCON_TIPO_SALDO,
             I_S_TIPO_MOV
        FROM PER_EMPLEADO  E,
             PER_CONCEPTO  C,
             PER_DPTO_CONC D,
             FIN_CONCEPTO  CF,
             GEN_EMPRESA   G
       WHERE E.EMPL_DEPARTAMENTO = D.DPTOC_DPTO
         AND C.PCON_CLAVE = D.DPTOC_PER_CONC
         AND D.DPTOC_FIN_CONC = CF.FCON_CLAVE
         AND C.PCON_CLAVE = 31 --AHM
         AND E.EMPL_LEGAJO = I_VREC_EMPLEADO
         AND E.EMPL_EMPRESA = G.EMPR_CODIGO
         AND C.PCON_EMPR = G.EMPR_CODIGO
         AND D.DPTOC_EMPR = G.EMPR_CODIGO
         AND CF.FCON_EMPR = G.EMPR_CODIGO
         AND G.EMPR_CODIGO = I_EMPRESA;
    END IF;
    IF I_PCON_IND_SUMA_IPS = 'S' THEN
    
      IF P_EMPL_FOR_PAGO = 5 THEN
        I_S_IMPORTE := ROUND(I_S_IMPORTE / 0.95);
      ELSE
        I_S_IMPORTE := ROUND(I_S_IMPORTE / 0.91);
      END IF;
    ELSE
      I_S_IMPORTE := I_S_IMPORTE;
    END IF;
  
    IF P_EMPL_FOR_PAGO = 5 THEN
      I_S_IMP_IPS := ROUND(NVL(I_S_IMPORTE, 0) * 0.05,
                           NVL(W_MON_DEC_IMP, 0));
    
    ELSE
      I_S_IMP_IPS := ROUND(NVL(I_S_IMPORTE, 0) * 0.09,
                           NVL(W_MON_DEC_IMP, 0));
    END IF;
    DBMS_OUTPUT.PUT_LINE(I_S_IMP_IPS);
  END;

  PROCEDURE PP_GENERAR_ADEL_PROV(I_DOC_IMPORTE_TOTAL IN OUT NUMBER,
                                 I_EMPRESA           IN NUMBER,
                                 I_VREC_FEC_HASTA    IN DATE,
                                 I_DOC_FECHA         OUT DATE,
                                 I_S_VREC_FEC_DOC    IN DATE,
                                 I_DOC_NRO_DOC       OUT NUMBER,
                                 I_VREC_CLAVE        IN NUMBER,
                                 I_DOC_PROV          OUT NUMBER,
                                 I_EMPL_CODIGO_PROV  IN NUMBER,
                                 I_VREC_EMPLEADO     IN NUMBER,
                                 I_DOC_CTABCO        OUT NUMBER,
                                 I_VREC_CTABCO       IN NUMBER,
                                 I_DOC_BCO_DESC      OUT VARCHAR2,
                                 I_BCO_DESC          IN VARCHAR2,
                                 I_DOC_CTA_DESC      OUT VARCHAR2,
                                 I_CTA_DESC          IN VARCHAR2,
                                 I_S_IMPORTE         IN OUT NUMBER,
                                 I_VREC_DIAS_PAG     IN NUMBER,
                                 I_DOC_DIAS_VAC      OUT NUMBER,
                                 I_DOC_IMPORTE       OUT NUMBER,
                                 I_DOC_IMPORTE_IPS   OUT NUMBER,
                                 I_S_IMP_IPS         IN OUT NUMBER,
                                 I_DOC_PROV_DESC     OUT VARCHAR2,
                                 I_DOC_OBS           OUT VARCHAR2,
                                 I_VREC_FEC_DESDE    IN DATE) IS
    V_FEC_FIN       DATE;
    V_IMPORTE_X_DIA NUMBER;
    V_FORMA_PAGO    NUMBER;
    ------------REVISADO-----------------
  BEGIN
  
    IF I_DOC_IMPORTE_TOTAL IS NULL THEN
    
      SELECT PERI_FEC_FIN
        INTO V_FEC_FIN
        FROM PER_CONFIGURACION, PER_PERIODO, GEN_EMPRESA
       WHERE I_VREC_FEC_DESDE BETWEEN PERI_FEC_INI AND PERI_FEC_FIN
         AND CONF_EMPR = EMPR_CODIGO
         AND PERI_EMPR = EMPR_CODIGO
         AND CONF_EMPR = I_EMPRESA;
      ---RAISE_APPLICATION_ERROR(-20010, V_FEC_FIN);
      IF (I_VREC_FEC_HASTA - V_FEC_FIN) >= 1 THEN
      
        --CUANDO EL PERIODO TERMINA EL 30 Y EL EMPLEADO SALE DE VACACIONES HASTA EL 01 DEL SIGUENTE MES, NO HACE EL ADELANTO. 13/06/2019
      
        --
      
        I_DOC_FECHA   := I_S_VREC_FEC_DOC;
        I_DOC_NRO_DOC := I_VREC_CLAVE;
        I_DOC_PROV    := I_EMPL_CODIGO_PROV;
      
        SELECT FP.PROV_RAZON_SOCIAL
          INTO I_DOC_PROV_DESC
          FROM FIN_PROVEEDOR FP, PER_EMPLEADO PE
         WHERE FP.PROV_EMPR = I_EMPRESA
           AND FP.PROV_EMPR = PE.EMPL_EMPRESA
           AND PE.EMPL_CODIGO_PROV = FP.PROV_CODIGO
           AND PE.EMPL_LEGAJO = I_VREC_EMPLEADO;
      
        I_DOC_CTABCO   := I_VREC_CTABCO;
        I_DOC_BCO_DESC := I_BCO_DESC;
        I_DOC_CTA_DESC := I_CTA_DESC;
      
        --================================================
      
        --TRAER CANTIDAD DE DIAS HABILES DEL MES SIGUIENTE
        I_DOC_DIAS_VAC := GENERAL.FP_RETORNA_CANT_DIA_HABIL(I_FECHA_INI => TO_DATE('01/' ||
                                                                                   TO_CHAR(I_VREC_FEC_HASTA,
                                                                                           'MM/YYYY'),
                                                                                   'DD/MM/YYYY'),
                                                            I_FECHA_FIN => I_VREC_FEC_HASTA,
                                                            I_EMPRESA   => I_EMPRESA);
      
        V_IMPORTE_X_DIA := ROUND(I_S_IMPORTE / I_VREC_DIAS_PAG, 0);
      
        I_DOC_IMPORTE := ROUND(I_DOC_DIAS_VAC * V_IMPORTE_X_DIA, 0);
      
        --SE BUSCA LA FORMA DE PAGO DEL PERSONAL PARA VER SI EL APORTE SERA IPS O AMH
        SELECT EMPL_FORMA_PAGO
          INTO V_FORMA_PAGO
          FROM PER_EMPLEADO
         WHERE EMPL_EMPRESA = I_EMPRESA
           AND EMPL_LEGAJO = I_VREC_EMPLEADO;
      
        IF V_FORMA_PAGO = 5 THEN
          I_DOC_IMPORTE_IPS := ROUND(ROUND(I_DOC_DIAS_VAC * V_IMPORTE_X_DIA,
                                           0) * 0.05,
                                     0);
        ELSE
          I_DOC_IMPORTE_IPS := ROUND(ROUND(I_DOC_DIAS_VAC * V_IMPORTE_X_DIA,
                                           0) * 0.09,
                                     
                                     0);
        
        END IF;
      
        I_DOC_IMPORTE_TOTAL := (I_DOC_IMPORTE - I_DOC_IMPORTE_IPS); --TIENE QUE SER MENOS EL IPS.
        I_DOC_OBS           := 'Adelanto creado desde el PERI009 "Recibos de Vacaciones", para la conciliacion de caja.';
      
        -----------------PARA QUE LA CAJA PUEDA CUADRAR SE LE DESCUENTA AL IMPORTE EL ADELANTO
      
        I_S_IMPORTE := (I_S_IMPORTE - NVL(I_DOC_IMPORTE, 0));
        I_S_IMP_IPS := (I_S_IMP_IPS - NVL(I_DOC_IMPORTE_IPS, 0));
        --RAISE_APPLICATION_ERROR(-20027, 'HOLI');
      END IF;
    
    END IF;
  
    --  EXCEPTION
    --  WHEN OTHERS THEN
    --RAISE_APPLICATION_ERROR(-20009, SQLERRM);
    -- I_DOC_IMPORTE_TOTAL := NULL;
  END;

  PROCEDURE PP_TRAER_MONEDA(I_MON_DESC         OUT VARCHAR2,
                            I_W_MON_DEC_IMP    OUT NUMBER,
                            I_W_MON_DEC_PRECIO OUT NUMBER,
                            I_EMPRESA          IN NUMBER,
                            S_MON              OUT NUMBER,
                            I_VREC_FEC_DOC     IN DATE,
                            I_VREC_EMPLEADO    IN NUMBER,
                            S_TASA_OFIC        OUT NUMBER) IS
  BEGIN
  
    SELECT NVL(EMPL_MON_PAGO, 1)
      INTO S_MON
      FROM PER_EMPLEADO
     WHERE EMPL_LEGAJO = I_VREC_EMPLEADO
       AND EMPL_EMPRESA = I_EMPRESA;
  
    SELECT MON_DESC, MON_DEC_IMP, MON_DEC_PRECIO
      INTO I_MON_DESC, I_W_MON_DEC_IMP, I_W_MON_DEC_PRECIO
    
      FROM GEN_MONEDA
     WHERE MON_CODIGO = S_MON
       AND MON_EMPR = I_EMPRESA;
  
    S_TASA_OFIC :=gen_cotizacion(I_EMPRESA,S_MON,I_VREC_FEC_DOC,'V');

  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20010, 'Moneda inexistente!');
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20009, SQLERRM);
  END;

  PROCEDURE PP_VALIDAR_PERIODO(FECHA     IN DATE,
                               I_EMPRESA IN NUMBER,
                               W_PERIODO OUT NUMBER) IS
  
    V_FEC_INI DATE;
    V_FEC_FIN DATE;
  
    V_PERIODO NUMBER;
  
  BEGIN
    -- RAISE_APPLICATION_ERROR(-20012, FECHA);
    SELECT PERI_FEC_INI
      INTO V_FEC_INI
      FROM PER_CONFIGURACION, PER_PERIODO, GEN_EMPRESA
     WHERE CONF_PERI_ACT = PERI_CODIGO
       AND CONF_EMPR = EMPR_CODIGO
       AND PERI_EMPR = EMPR_CODIGO
       AND CONF_EMPR = I_EMPRESA;
  
    SELECT PERI_FEC_FIN
      INTO V_FEC_FIN
      FROM PER_CONFIGURACION, PER_PERIODO, GEN_EMPRESA
     WHERE CONF_PERI_SGTE = PERI_CODIGO
       AND CONF_EMPR = EMPR_CODIGO
       AND PERI_EMPR = EMPR_CODIGO
       AND CONF_EMPR = I_EMPRESA;
  
    IF FECHA NOT BETWEEN V_FEC_INI AND V_FEC_FIN THEN
      RAISE_APPLICATION_ERROR(-20011,
                              'La fecha de documento debe estar comprendida entre ' ||
                              TO_CHAR(V_FEC_INI, 'dd-mm-yyyy') || ' y ' ||
                              TO_CHAR(V_FEC_FIN, 'dd-mm-yyyy'));
    END IF;
  
    SELECT PERI_CODIGO
      INTO V_PERIODO
      FROM PER_PERIODO
     WHERE PERI_FEC_INI <= FECHA
       AND PERI_FEC_FIN >= FECHA
       AND PERI_EMPR = I_EMPRESA;
  
    W_PERIODO := V_PERIODO;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20012,
                              'No ha sido cargada la configuracion de Recursos Humanos!');
  END;

  PROCEDURE PP_VALIDAR_PERIODO_FIN(FECHA IN DATE, I_EMPRESA IN NUMBER) IS
  
    V_FEC_INI DATE;
    V_FEC_FIN DATE;
  
    V_PERIODO NUMBER;
  
  BEGIN
  
    SELECT PERI_FEC_INI
      INTO V_FEC_INI
      FROM FIN_CONFIGURACION, FIN_PERIODO, GEN_EMPRESA
     WHERE CONF_PERIODO_ACT = PERI_CODIGO
       AND CONF_EMPR = EMPR_CODIGO
       AND PERI_EMPR = EMPR_CODIGO
       AND CONF_EMPR = I_EMPRESA;
  
    SELECT PERI_FEC_FIN
      INTO V_FEC_FIN
      FROM FIN_CONFIGURACION, FIN_PERIODO, GEN_EMPRESA
     WHERE CONF_PERIODO_SGTE = PERI_CODIGO
       AND CONF_EMPR = EMPR_CODIGO
       AND PERI_EMPR = EMPR_CODIGO
       AND CONF_EMPR = I_EMPRESA;
  
    IF FECHA NOT BETWEEN V_FEC_INI AND V_FEC_FIN THEN
      RAISE_APPLICATION_ERROR(-20013,
                              'La fecha de documento debe estar comprendida entre ' ||
                              TO_CHAR(V_FEC_INI, 'dd-mm-yyyy') || ' y ' ||
                              TO_CHAR(V_FEC_FIN, 'dd-mm-yyyy') ||
                              ', si se esta relacionando con FINANZAS!');
    END IF;
  
    SELECT PERI_CODIGO
      INTO V_PERIODO
      FROM FIN_PERIODO
     WHERE PERI_FEC_INI <= FECHA
       AND PERI_FEC_FIN >= FECHA
       AND PERI_EMPR = I_EMPRESA;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20014,
                              'No ha sido cargada la configuracion de Finanzas!');
  END;

  PROCEDURE PP_VERIF_MOSTRAR_CTA_BANC(I_BCO_DESC    OUT VARCHAR2,
                                      I_CTA_DESC    OUT VARCHAR2,
                                      S_MON         OUT NUMBER,
                                      I_EMPRESA     IN NUMBER,
                                      I_VREC_CTABCO IN NUMBER) IS
    V_MON NUMBER;
  BEGIN
  
    SELECT CTA_MON, BCO_DESC, CTA_DESC, CTA_MON
      INTO V_MON, I_BCO_DESC, I_CTA_DESC, S_MON
      FROM FIN_CUENTA_BANCARIA, FIN_BANCO, GEN_EMPRESA
     WHERE CTA_BCO = BCO_CODIGO(+)
       AND CTA_EMPR = BCO_EMPR(+)
       AND CTA_EMPR = I_EMPRESA
       AND CTA_EMPR = EMPR_CODIGO
       AND CTA_CODIGO = I_VREC_CTABCO;
  
    -- PL_VALIDAR_OPCTA(I_VREC_CTABCO, I_EMPRESA, USER, I_CTA_DESC,'C') ;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      I_BCO_DESC := NULL;
      I_CTA_DESC := NULL;
      RAISE_APPLICATION_ERROR(-20015, 'Cuenta bancaria inexistente1!'||I_VREC_CTABCO);
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20016, SQLCODE || ' - ' || SQLERRM);
  END;
  PROCEDURE PP_ACTUALIZAR_REGISTRO(I_VREC_EMPLEADO     IN NUMBER,
                                   I_EMPRESA           IN NUMBER,
                                   I_DOC_CTABCO        IN NUMBER,
                                   I_DOC_FECHA         IN DATE,
                                   I_DOC_IMPORTE       IN NUMBER,
                                   I_DOC_OBS           IN VARCHAR2,
                                   I_DOC_NRO_DOC       IN NUMBER,
                                   I_DOC_DIAS_VAC      IN NUMBER,
                                   I_DOC_IMPORTE_IPS   IN NUMBER,
                                   I_DOC_IMPORTE_TOTAL IN NUMBER,
                                   I_EMPL_CODIGO_PROV  IN NUMBER,
                                   S_IMPORTE           IN OUT NUMBER,
                                   I_EMPL_NOMBRES      IN VARCHAR2,
                                   I_VREC_FEC_DOC      IN DATE,
                                   S_VREC_FEC_DOC      IN DATE,
                                   I_VREC_CLAVE        IN NUMBER,
                                   I_FCON_CLAVE        OUT NUMBER,
                                   I_FCON_CODIGO       OUT NUMBER,
                                   I_FCON_CLAVE_CTACO  OUT NUMBER,
                                   I_FCON_TIPO_SALDO   OUT VARCHAR2,
                                   S_TIPO_MOV          OUT NUMBER,
                                   I_PCON_IND_IPS      OUT VARCHAR2,
                                   I_PCON_IND_SUMA_IPS OUT VARCHAR2,
                                   S_IMP_IPS           IN OUT NUMBER,
                                   S_TASA_OFIC         IN NUMBER,
                                   S_MON               IN NUMBER,
                                   I_EMPL_DEPARTAMENTO IN NUMBER,
                                   W_MON_DEC_IMP       IN NUMBER,
                                   I_VREC_NRO          IN NUMBER,
                                   I_VREC_CLAVE_IPS    OUT NUMBER,
                                   I_VREC_CLAVE_VAC    OUT NUMBER,
                                   I_VREC_OBS          IN VARCHAR2,
                                   I_VREC_CTABCO       IN NUMBER,
                                   I_VREC_FEC_OPER     IN DATE,
                                   ----
                                   I_VREC_DIAS_PAG  IN NUMBER,
                                   I_VREC_DIAS      IN NUMBER,
                                   I_VREC_FEC_DESDE IN DATE,
                                   I_VREC_FEC_HASTA IN DATE,
                                   I_VREC_ANHO      IN OUT NUMBER) IS
    SALIR EXCEPTION;
    V_CLAVE NUMBER;
  BEGIN
    IF I_VREC_ANHO IS NULL THEN
      I_VREC_ANHO := TO_NUMBER(TO_CHAR(SYSDATE, 'YYYY'));
    
    END IF;
  
   IF I_VREC_CTABCO IS NULL THEN
      RAISE_APPLICATION_ERROR(-20001, 'Debe ingresar el banco');
    
    END IF;

    V_CLAVE := FP_CLAVE_DOC(I_EMPRESA);
 ---   RAISE_APPLICATION_ERROR(-20001, V_CLAVE);
    INSERT INTO PER_VAC_REC
      (VREC_CLAVE,
       VREC_EMPLEADO,
       VREC_FECHA_EMIS,
       VREC_ANHO,
       VREC_FEC_DESDE,
       VREC_FEC_HASTA,
       VREC_DIAS,
       VREC_OBS,
       VREC_NRO,
       VREC_FEC_DOC,
       VREC_FEC_OPER,
       VREC_CTABCO,
       VREC_CLAVE_VAC,
       VREC_CLAVE_IPS,
       VREC_DIAS_PAG,
       VREC_EMPR)
    VALUES
      (I_VREC_CLAVE,
       I_VREC_EMPLEADO,
       SYSDATE,
       I_VREC_ANHO,
       I_VREC_FEC_DESDE,
       I_VREC_FEC_HASTA,
       I_VREC_DIAS,
       I_VREC_OBS,
       I_VREC_NRO,
       I_VREC_FEC_DOC,
       I_VREC_FEC_OPER,
       I_VREC_CTABCO,
       V_CLAVE,
       (V_CLAVE + 1),
       I_VREC_DIAS_PAG,
       I_EMPRESA);
  
    COMMIT;
    --GRABAR EN RRHH Y FINANZAS
    BEGIN
      -- CALL THE PROCEDURE
      PERI009.PP_ACT_PER_FIN(I_EMPL_CODIGO_PROV  => I_EMPL_CODIGO_PROV,
                             S_IMPORTE           => S_IMPORTE,
                             I_VREC_EMPLEADO     => I_VREC_EMPLEADO,
                             I_EMPL_NOMBRES      => I_EMPL_NOMBRES,
                             I_VREC_FEC_DOC      => I_VREC_FEC_DOC,
                             I_EMPRESA           => I_EMPRESA,
                             S_VREC_FEC_DOC      => S_VREC_FEC_DOC,
                             I_VREC_CLAVE        => I_VREC_CLAVE,
                             I_FCON_CLAVE        => I_FCON_CLAVE,
                             I_FCON_CODIGO       => I_FCON_CODIGO,
                             I_FCON_CLAVE_CTACO  => I_FCON_CLAVE_CTACO,
                             I_FCON_TIPO_SALDO   => I_FCON_TIPO_SALDO,
                             S_TIPO_MOV          => S_TIPO_MOV,
                             I_PCON_IND_IPS      => I_PCON_IND_IPS,
                             I_PCON_IND_SUMA_IPS => I_PCON_IND_SUMA_IPS,
                             S_IMP_IPS           => S_IMP_IPS,
                             S_TASA_OFIC         => S_TASA_OFIC,
                             S_MON               => S_MON,
                             I_EMPL_DEPARTAMENTO => I_EMPL_DEPARTAMENTO,
                             W_MON_DEC_IMP       => W_MON_DEC_IMP,
                             I_VREC_NRO          => I_VREC_NRO,
                             I_VREC_CLAVE_IPS    => I_VREC_CLAVE_IPS,
                             I_VREC_CLAVE_VAC    => I_VREC_CLAVE_VAC,
                             I_VREC_OBS          => I_VREC_OBS,
                             I_VREC_CTABCO       => I_VREC_CTABCO,
                             I_VREC_FEC_OPER     => I_VREC_FEC_OPER);
    END;
    -- REVISADO
    --EN CASO QUE LAS VACACIONES TOMEN DOS MESES CREA UN ADELANTO A PROV. PARA EL CIERRE DE CAJA.
    IF I_DOC_FECHA IS NOT NULL AND I_DOC_NRO_DOC IS NOT NULL AND
       I_DOC_DIAS_VAC > 0 AND I_EMPRESA = 1 THEN
      --- RAISE_APPLICATION_ERROR (-20001, 'PASA PRO AQUI');
      PER_CREA_ADEL_EMPL_VAC(TO_NUMBER(I_VREC_EMPLEADO),
                             TO_NUMBER(I_EMPRESA),
                             TO_NUMBER(I_DOC_CTABCO),
                             TO_DATE(I_DOC_FECHA, 'DD/MM/YYYY'),
                             TO_NUMBER(I_DOC_IMPORTE),
                             I_DOC_OBS,
                             GEN_DEVUELVE_USER,
                             TO_NUMBER(I_DOC_NRO_DOC),
                             TO_NUMBER(I_DOC_DIAS_VAC),
                             TO_NUMBER(I_DOC_IMPORTE_IPS),
                             TO_NUMBER(I_DOC_IMPORTE_TOTAL),
                             I_DOC_OBS,
                             I_VREC_CLAVE);
    END IF;
  
    IF I_DOC_FECHA IS NOT NULL AND I_DOC_NRO_DOC IS NOT NULL AND --ADELANTO PARA TRANSAGRO
       I_DOC_DIAS_VAC > 0 AND I_EMPRESA = 2 THEN
      PER_CREA_ADEL_EMPL_VAC_TRA(TO_NUMBER(I_VREC_EMPLEADO),
                                 TO_NUMBER(I_EMPRESA),
                                 TO_NUMBER(I_DOC_CTABCO),
                                 TO_DATE(I_DOC_FECHA, 'DD/MM/YYYY'),
                                 TO_NUMBER(I_DOC_IMPORTE),
                                 I_DOC_OBS,
                                 GEN_DEVUELVE_USER,
                                 TO_NUMBER(I_DOC_NRO_DOC),
                                 TO_NUMBER(I_DOC_DIAS_VAC),
                                 TO_NUMBER(I_DOC_IMPORTE_IPS),
                                 TO_NUMBER(I_DOC_IMPORTE_TOTAL),
                                 I_DOC_OBS,
                                 I_VREC_CLAVE);
    END IF;
  
    -------------------------
  
    ----------------------------
  
  EXCEPTION
    WHEN SALIR THEN
      NULL;
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20017, SQLCODE || ' - ' || SQLERRM);
  END;

  FUNCTION CF_RECIBIMOSFORMULA(I_EMPRESA IN NUMBER) RETURN VARCHAR2 is
    l_name_empr gen_empresa.empr_razon_social%type;
  begin
    <<get_raz_soc_empr>>
      begin
        select upper(e.empr_razon_social)
        into l_name_empr
        from gen_empresa e
        where e.empr_codigo = i_empresa;
      exception
        when no_data_found then
          l_name_empr := 'Defina Razon Social en Empresa -> '||i_empresa;
      end get_raz_soc_empr;
      
      if l_name_empr is not null then
         RETURN 'Recibi(mos) de: '||l_name_empr;
      end if;
      /*
    IF I_EMPRESA = 1 then
      return 'Recibi(mos) de: CONSULTAGRO S.A';
    ELSIF I_EMPRESA = 2 THEN
      RETURN 'Recibi(mos) de: TRANSAGRO S.A.';
    ELSIF I_EMPRESA = 3 THEN
      RETURN 'Recibi(mos) de: CEDEC';
    ELSIF I_EMPRESA = 4 THEN
      RETURN 'Recibi(mos) de: MI TIERRA';
    ELSIF I_EMPRESA = 5 THEN
      RETURN 'Recibi(mos) de: AGROGANADERA PICO S.A';
    ELSIF I_EMPRESA = 6 THEN
      RETURN 'Recibi(mos) de: ALBERT HILDEBRAND';
    ELSIF I_EMPRESA = 7 THEN
      RETURN 'Recibi(mos) de: AGRO GUYRAUNGUA S.A.';
    ELSIF I_EMPRESA = 8 THEN
      RETURN 'Recibi(mos) de: HALTEN S.A.';
    ELSIF I_EMPRESA = 9 THEN
      RETURN 'Recibi(mos) de: JARDIN S.A.';
    ELSIF I_EMPRESA = 10 THEN
      RETURN 'Recibi(mos) de: JOHNNY HILDEBRAND';
    ELSIF I_EMPRESA = 11 THEN
      RETURN 'Recibi(mos) de: MONEGRO S.A.';
    ELSIF I_EMPRESA = 12 THEN
      RETURN 'Recibi(mos) de: MONTENEGRO S.A.';
    ELSIF I_EMPRESA = 13 THEN
      RETURN 'Recibi(mos) de: SUELO FERTIL S.A.';
    END IF;
    */
  END;

  FUNCTION CF_TOTAL_IMPORTEFORMULA(I_FORMA_PAGO  IN NUMBER,
                                   I_IMPORTE_VAC IN NUMBER,
                                   I_IMPORTE_AMH IN NUMBER,
                                   I_IMPORTE_IPS IN NUMBER) RETURN NUMBER IS
    IMPORTE_TOTAL NUMBER;
  
  BEGIN
    IF I_FORMA_PAGO = 5 THEN
      IMPORTE_TOTAL := ROUND(I_IMPORTE_VAC - I_IMPORTE_AMH);
    ELSE
      IMPORTE_TOTAL := ROUND(I_IMPORTE_VAC - I_IMPORTE_IPS);
    END IF;
    RETURN IMPORTE_TOTAL;
  END;

  FUNCTION CF_COTIZACIONFORMULA(I_PDOC_CLAVE_FIN IN NUMBER,
                                I_EMPRESA        IN NUMBER) RETURN NUMBER IS
    V_TASA NUMBER;
  BEGIN
  
    SELECT DOC_TASA
      INTO V_TASA
      FROM FIN_DOCUMENTO
     WHERE DOC_CLAVE = I_PDOC_CLAVE_FIN
       AND DOC_EMPR = I_EMPRESA;
  
    RETURN V_TASA;
  
  END;

  FUNCTION CF_SIMBOLO_MONFORMULA(I_PDOC_MON IN NUMBER, I_EMPRESA IN NUMBER)
    RETURN VARCHAR2 IS
    V_SIMBOLO VARCHAR2(10);
  BEGIN
    SELECT MON_SIMBOLO
      INTO V_SIMBOLO
      FROM GEN_MONEDA
     WHERE MON_CODIGO = NVL(I_PDOC_MON, 1)
       AND MON_EMPR = I_EMPRESA;
    RETURN V_SIMBOLO;
  END;

  FUNCTION CF_DESCRIPCION_VACACIONESFORMU(I_DIAS_VACACIONES IN NUMBER)
    RETURN VARCHAR2 IS
  BEGIN
    RETURN 'VACACIONES ' || I_DIAS_VACACIONES || ' DIAS';
  END;

  PROCEDURE PP_LLAMAR_REPORT(I_VREC_EMPLEADO   IN NUMBER,
                             I_EMPRESA         IN NUMBER,
                             I_VREC_NRO        IN NUMBER,
                             I_VREC_CLAVE_VAC  IN NUMBER,
                             I_VREC_CLAVE_IPS  IN NUMBER,
                             I_SUCURSAL        IN NUMBER,
                             I_DIAS_VACACIONES IN NUMBER,
                             I_DESDE           IN DATE,
                             I_HASTA           IN DATE) IS
  
    V_WHERE VARCHAR2(32767) := NULL;
    SALIR EXCEPTION;
    V_CLAVES_PER VARCHAR2(100) := NULL;
  
    V_IMPORTE_TOTAL NUMBER;
    V_IMPORTE_IPS   NUMBER;
    V_FORMA_PAGO    NUMBER;
    V_IMPORTE_AMH   NUMBER;
    V_DES_EMPRESA   VARCHAR2(100);
    V_SUCURSAL      VARCHAR2(100);
    V_PARAMETROS    VARCHAR2(32767);
    V_IDENTIFICADOR VARCHAR2(2) := '&';
    V_IPS_AMH       VARCHAR2(100);
    
        V_REPORTE VARCHAR2(10);
  BEGIN
  
    /*  RAISE_APPLICATION_ERROR(-20001,I_VREC_EMPLEADO   ||'   '||
    I_EMPRESA         ||'   '||
    I_VREC_NRO        ||'   '||
    I_VREC_CLAVE_VAC  ||'   '||
    I_VREC_CLAVE_IPS  ||'   '||
    I_SUCURSAL        ||'   '||
    I_DIAS_VACACIONES );*/
    BEGIN
      IF I_EMPRESA = 3 THEN --Cuando es CEDEC, el calculo debe hacer por el la cantidad de dias pagados, no por el total de dias. 28/01/2021
      SELECT EMPLV_VAC_PAG * EMPLV_IMP_DIA
        INTO V_IMPORTE_TOTAL
        FROM PER_EMPLEADO_VACACION
       WHERE EMPLV_LEGAJO = I_VREC_EMPLEADO
         AND EMPLV_EMPR = I_EMPRESA
         AND EMPLV_ITEM = I_VREC_NRO
         AND EMPLV_FEC_DESDE = I_DESDE
         AND EMPLV_FEC_HASTA = I_HASTA;
      else
        SELECT EMPLV_CANT_DIAS * EMPLV_IMP_DIA
        INTO V_IMPORTE_TOTAL
        FROM PER_EMPLEADO_VACACION
       WHERE EMPLV_LEGAJO = I_VREC_EMPLEADO
         AND EMPLV_EMPR = I_EMPRESA
         AND EMPLV_ITEM = I_VREC_NRO
         AND EMPLV_FEC_DESDE = I_DESDE
         AND EMPLV_FEC_HASTA = I_HASTA;
      end if;
        
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
  
    SELECT EMPL_FORMA_PAGO
      INTO V_FORMA_PAGO
      FROM PER_EMPLEADO
     WHERE EMPL_EMPRESA = I_EMPRESA
       AND EMPL_LEGAJO = I_VREC_EMPLEADO;
  
    IF V_FORMA_PAGO = 5 THEN
      V_IMPORTE_AMH := ROUND(V_IMPORTE_TOTAL * 0.05, 0);
    ELSE
      V_IMPORTE_IPS := ROUND(V_IMPORTE_TOTAL * 0.09, 0);
    END IF;
  
    BEGIN
      SELECT T.EMPR_RAZON_SOCIAL
        INTO V_DES_EMPRESA
        FROM GEN_EMPRESA T
       WHERE T.EMPR_CODIGO = I_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
        V_DES_EMPRESA := NULL;
    END;
    BEGIN
      SELECT SUC_DESC
        INTO V_SUCURSAL
        FROM GEN_SUCURSAL
       WHERE SUC_CODIGO = I_SUCURSAL
         AND SUC_EMPR = I_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
        V_SUCURSAL := 'Todas las Sucursales';
    END;
  
    V_WHERE := ' AND empl_legajo =' || I_VREC_EMPLEADO;
  
    IF I_VREC_CLAVE_VAC IS NOT NULL THEN
      V_CLAVES_PER := I_VREC_CLAVE_VAC;
    END IF;
    IF I_VREC_CLAVE_IPS IS NOT NULL THEN
      V_CLAVES_PER := V_CLAVES_PER || ',' || I_VREC_CLAVE_IPS;
    END IF;
  
    IF V_FORMA_PAGO = 5 THEN
      V_IPS_AMH := 'A.M.H. OBRERO:';
    ELSE
      V_IPS_AMH := 'I.P.S OBRERO:';
    END IF;
    V_WHERE := V_WHERE || ' AND pdoc_clave in (' || V_CLAVES_PER || ')';
  
    V_PARAMETROS := 'P_FORMATO=' || URL_ENCODE('pdf');
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_EMPRESA=' ||
                    URL_ENCODE(I_EMPRESA);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_DESC_EMPRESA=' ||
                    URL_ENCODE(V_DES_EMPRESA);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_PROGRAMA=' ||
                    URL_ENCODE('PERI209');
    -----------SUCURSALES
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_SUCU_DESC=' ||
                    URL_ENCODE(V_SUCURSAL);
    -----------
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_DESC_PROGRAMA=' ||
                    URL_ENCODE('RECIBOS DE VACACIONES');
  
    --------------CLIENTE
  
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_DIAS_VACACIONES=' ||
                    URL_ENCODE(I_DIAS_VACACIONES);
  
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_IMPORTE_VAC=' ||
                    URL_ENCODE(NVL(TRUNC(V_IMPORTE_TOTAL, 0), 0));
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_IMPORTE_IPS=' ||
                    URL_ENCODE(NVL(TRUNC(V_IMPORTE_IPS, 0), 0));
  
    IF V_FORMA_PAGO = 5 THEN
      V_IPS_AMH    := 'A.M.H. OBRERO:';
      V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_IMPORTE_AMH=' ||
                      URL_ENCODE(NVL(TRUNC(V_IMPORTE_AMH, 0), 0));
    ELSE
      V_IPS_AMH    := 'I.P.S OBRERO:';
      V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_IMPORTE_AMH=' ||
                      URL_ENCODE(NVL(TRUNC(V_IMPORTE_IPS, 0), 0));
    END IF;
  
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_FORMA_PAGO=' ||
                    URL_ENCODE(NVL(TRUNC(V_FORMA_PAGO, 0), 0));
  
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_WHERE=' ||
                    URL_ENCODE(V_WHERE);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_IPS_AMH=' ||
                    URL_ENCODE(V_IPS_AMH);
  
    INSERT INTO X (CAMPO1, OTRO) VALUES (V_PARAMETROS, 'PERI209');
    COMMIT;
   
   IF I_EMPRESA = 2 THEN
    V_REPORTE := 'PERI209_T';
  
   ELSE
     V_REPORTE := 'PERI209';
   
   END IF;
   
    DELETE FROM GEN_PARAMETROS_REPORT WHERE USUARIO = GEN_DEVUELVE_USER;
    INSERT INTO GEN_PARAMETROS_REPORT
      (PARAMETROS, USUARIO, NOMBRE_REPORTE, FORMATO_SALIDA)
    VALUES
      (V_PARAMETROS, GEN_DEVUELVE_USER, V_REPORTE, 'pdf');
    COMMIT;
  
  EXCEPTION
    WHEN SALIR THEN
      NULL;
  END;

  PROCEDURE PP_BLOQ_PROV_CLI(P_EMPRESA IN NUMBER, P_LEGAJO IN NUMBER) IS
  
    V_CANT_REG_PROV NUMBER;
    V_CANT_REG_CLI  NUMBER;
  
    V_PROVEEDOR NUMBER;
    V_CLIENTE   NUMBER;
  BEGIN
  
    --------PEDIDO DEL SR UGO, NO SE PUEDE REALIZAR NINGUNA TRANSACCION SI EL CLIENTE CUENTA CON
    --------ADELANTOS VENCIDOS.. EXECPTO EL PAGO DE LOS MISMO--06/08/2020 LV
    IF P_EMPRESA = 1 THEN
      SELECT E.EMPL_CODIGO_PROV, E.EMPL_COD_CLIENTE
        INTO V_PROVEEDOR, V_CLIENTE
        FROM PER_EMPLEADO E
       WHERE EMPL_EMPRESA = P_EMPRESA
         AND EMPL_LEGAJO = P_LEGAJO;
    
      SELECT COUNT(*)
        INTO V_CANT_REG_PROV
        FROM FIN_DOCUMENTO   A,
             FIN_UNION_CUOTA B,
             FIN_UNION_PAGO  C,
             FIN_PROVEEDOR   D
       WHERE A.DOC_TIPO_MOV IN (31)
         AND A.DOC_EMPR = 1
         AND A.DOC_CLAVE = B.CUO_CLAVE_DOC
         AND A.DOC_EMPR = B.CUO_EMPR
         AND A.DOC_PROV = D.PROV_CODIGO(+)
         AND A.DOC_EMPR = D.PROV_EMPR(+)
         AND (B.CUO_CLAVE_DOC = C.PAG_CLAVE_DOC(+) AND
             B.CUO_FEC_VTO = C.PAG_FEC_VTO(+) AND
             B.CUO_EMPR = C.PAG_EMPR(+))
         AND B.CUO_FEC_VTO < SYSDATE
         AND TRUNC(CUO_SALDO_MON) > 0
         AND TRUNC(SYSDATE) - CUO_FEC_VTO > 0
         AND DOC_PROV = V_PROVEEDOR;
    
      IF V_CANT_REG_PROV > 0 THEN
      
        RAISE_APPLICATION_ERROR(-20001,
                                'El empleado ' || P_LEGAJO ||
                                ' cuenta con ' || V_CANT_REG_PROV ||
                                ' adelanto/s proveedor vencido/s');
      
      END IF;
    
      SELECT COUNT(1)
        INTO V_CANT_REG_CLI
        FROM FIN_DOCUMENTO   A,
             FIN_UNION_CUOTA B,
             FIN_UNION_PAGO  C,
             FIN_CLIENTE     D
       WHERE A.DOC_TIPO_MOV IN (18)
         AND A.DOC_EMPR = 1
         AND A.DOC_CLAVE = B.CUO_CLAVE_DOC
         AND A.DOC_EMPR = B.CUO_EMPR
         AND A.DOC_CLI = D.CLI_CODIGO(+)
         AND A.DOC_EMPR = D.CLI_EMPR(+)
         AND (B.CUO_CLAVE_DOC = C.PAG_CLAVE_DOC(+) AND
             B.CUO_FEC_VTO = C.PAG_FEC_VTO(+) AND
             B.CUO_EMPR = C.PAG_EMPR(+))
         AND B.CUO_FEC_VTO < SYSDATE
         AND TRUNC(CUO_SALDO_MON) > 0
         AND TRUNC(SYSDATE) - CUO_FEC_VTO > 0
         AND CLI_CODIGO = V_CLIENTE;
    
      IF V_CANT_REG_CLI > 0 THEN
      
        RAISE_APPLICATION_ERROR(-20001,
                                'El empleado ' || P_LEGAJO ||
                                ' cuenta con ' || V_CANT_REG_CLI ||
                                ' adelanto/s cliente vencido/s');
      END IF;
    
    END IF;
  
  END PP_BLOQ_PROV_CLI;

END PERI009;
/
