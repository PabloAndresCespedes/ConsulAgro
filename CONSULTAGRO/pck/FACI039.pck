CREATE OR REPLACE PACKAGE FACI039 AS

  /* TODO ENTER PACKAGE DECLARATIONS (TYPES, EXCEPTIONS, METHODS ETC) HERE */
  FUNCTION FL_OBTENER_ULT_NRO(I_TIPO_MOV  IN NUMBER,
                              I_IMPRESORA IN NUMBER,
                              I_EMPRESA   IN NUMBER) RETURN CHAR;

  PROCEDURE PP_BUSCAR_NRO_FACTURA(P_EMPRESA      IN NUMBER,
                                  P_DOC_TIPO_MOV IN NUMBER,
                                  P_IMPRESORA    IN NUMBER,
                                  P_DOC_NRO_DOC  OUT NUMBER,
                                  P_DOC_TIMBRADO IN NUMBER);

  /*PROCEDURE AF_VALIDAR_FECHA(P_ACCION_EN IN VARCHAR2,
  P_CAMPO_US_EN IN VARCHAR2,
  P_CAMPO_DB_EN IN VARCHAR2,
  P_FECHA_INICIO_EN IN DATE);*/

  PROCEDURE PP_VALIDAR_FECHA(P_DOC_FEC_DOC           IN DATE,
                             P_CONF_PER_ACT_INI      IN DATE,
                             P_CONF_PER_SGTE_FIN     IN DATE,
                             P_CONF_STK_PER_ACT_INI  IN DATE,
                             P_CONF_STK_PER_SGTE_FIN IN DATE);

  FUNCTION FP_COTIZACION(P_MONEDA      IN NUMBER,
                         P_DOC_FEC_DOC IN DATE,
                         P_EMPRESA     IN NUMBER) RETURN NUMBER;

  PROCEDURE PL_VALIDAR_HAB_MES_FIN(P_FECHA   IN DATE,
                                   P_EMPRESA IN NUMBER,
                                   P_USUARIO IN VARCHAR2);

  PROCEDURE PL_VALIDAR_HAB_MES_STK(P_FECHA   IN DATE,
                                   P_EMPRESA IN NUMBER,
                                   P_USUARIO IN VARCHAR2);

  PROCEDURE PL_VAL_DOC_TIMBRADO(I_NRO_DOC   IN NUMBER,
                                I_CLASE_DOC IN NUMBER,
                                I_FECHA     IN DATE,
                                O_TIMBRADO  OUT NUMBER,
                                I_EMPRESA   IN NUMBER);

  PROCEDURE PP_TRAER_DESC_CLI(P_DOC_CLI                     IN NUMBER,
                              P_EMPRESA                     IN NUMBER,
                              P_DOC_CLI_TEL                 OUT VARCHAR2,
                              P_DOC_CLI_RUC                 OUT VARCHAR2,
                              P_CLI_PERS_REPRESENTANTE      OUT VARCHAR2,
                              P_CLI_DOC_IDENT_REPRESENTANTE OUT VARCHAR2,
                              P_CLI_LOCALIDAD               OUT VARCHAR2,
                              P_W_CLI_MAX_DIAS_ATRASO       OUT NUMBER,
                              P_DIAS_ATRASO                 OUT NUMBER,
                              P_CLI_NOM                     OUT VARCHAR2,
                              P_CLI_DIR                     OUT VARCHAR2,
                              P_DOC_CANAL                   OUT NUMBER,
                              P_CLI_IND_MOD_CANAL           OUT VARCHAR2,
                              P_OCASIONAL                   OUT NUMBER,
                              P_PLAZO_PAGO                  OUT NUMBER,
                              P_PORC_DTO                    OUT NUMBER,
                              P_RECARGO                     OUT NUMBER);

  PROCEDURE PP_VALIDAR_LISTA_NEGRA(P_DOC_CLI     IN NUMBER,
                                   P_EMPRESA     IN NUMBER,
                                   P_MARCA       OUT VARCHAR2,
                                   P_LISTA_NEGRA OUT VARCHAR2);

  PROCEDURE PP_VALIDAR_CLIENTE(P_EMPRESA      IN NUMBER,
                               P_DOC_CLI      IN NUMBER,
                               P_TIPO_FACTURA IN NUMBER,
                               P_DOC_FEC_DOC  IN DATE,
                               P_LIM_FAC_ADIC OUT VARCHAR2);

  PROCEDURE PP_CLI_LISTA_PRECIO(I_EMPRESA               IN NUMBER,
                                I_DOC_CLI               IN NUMBER,
                                I_DOC_MON               IN NUMBER,
                                I_SUCURSAL              IN NUMBER,
                                O_LIPR_NRO_LISTA_PRECIO OUT NUMBER);

  PROCEDURE PP_LISTA_PRECIOS(P_EMPRESA                   IN NUMBER,
                             P_DOC_CLI                   IN NUMBER,
                             P_LIPR_NRO_LISTA_PRECIO     OUT NUMBER,
                             P_PED_AUTO                  OUT VARCHAR2,
                             P_TIPO_FACTURA              IN NUMBER,
                             P_DOC_FEC_DOC               IN DATE,
                             P_W_IMP_LIM_CR_EMPR         IN OUT NUMBER,
                             P_W_IMP_LIM_CR_DISP_GRUPO   IN OUT NUMBER,
                             P_W_IMP_LIM_CR_DISP_EMPR    IN OUT NUMBER,
                             P_W_IMP_CHEQ_DIFERIDO       IN OUT NUMBER,
                             P_W_IMP_CHEQ_RECHAZADO      IN OUT NUMBER,
                             P_CATEG                     OUT NUMBER,
                             P_CONF_CATEG_CLI_ESPORADICO IN NUMBER,
                             P_VENDEDOR                  IN NUMBER,
                             I_DOC_MON                   IN NUMBER);

  PROCEDURE PP_VALIDAR_CLIENTE2(P_EMPRESA             IN NUMBER,
                                P_DOC_CLI             IN NUMBER,
                                P_TIPO_FACTURA        IN NUMBER,
                                P_W_CATEG             OUT NUMBER,
                                P_W_CLI_PORC_EXEN_IVA OUT NUMBER,
                                P_W_IMP_LIM_CR_GRUPO  OUT NUMBER,
                                P_DOC_LEGAJO          OUT NUMBER,
                                P_MENSAJE_CONFIR      OUT VARCHAR2);

  PROCEDURE PL_VALIDAR_OPCTA(P_CTA       IN NUMBER,
                             P_EMPRESA   IN NUMBER,
                             P_LOGIN     IN VARCHAR2,
                             P_CTA_DESC  IN VARCHAR2,
                             P_OPERACION IN VARCHAR2,
                             P_PROGRAMA  IN VARCHAR2);

  PROCEDURE PP_VAL_CTA_BCO_MES_ACTUAL(P_FECHA   IN DATE,
                                      P_CTA_BCO IN NUMBER,
                                      P_EMPRESA IN NUMBER,
                                      P_USUARIO IN VARCHAR2);

  PROCEDURE PP_VALIDAR_MONEDA(P_DOC_MON               IN NUMBER,
                              P_MON_LOC               IN NUMBER,
                              P_MON_US                IN NUMBER,
                              P_TIPO_FACTURA          IN NUMBER,
                              P_W_ANT_DOC_MON         IN NUMBER,
                              P_PLAZO_PAGO            IN OUT NUMBER,
                              P_DOC_CLI               IN NUMBER,
                              P_EMPRESA               IN NUMBER,
                              P_DOC_FEC_DOC           IN DATE,
                              P_CONF_COD_FAC_CRED     IN NUMBER,
                              P_USUARIO               IN VARCHAR2,
                              P_W_CLI_MAX_DIAS_ATRASO IN OUT NUMBER,
                              P_W_LIM_CR_ESPECIAL     OUT NUMBER,
                              P_CONFIRMAR_VENC        OUT VARCHAR2,
                              P_PORC_DTO              OUT NUMBER,
                              P_RECARGO               OUT NUMBER,
                              P_IND_BTN_TASA          OUT VARCHAR2,
                              P_W_DOC_TASA_US         OUT NUMBER,
                              P_CONFIRMAR_BLOQ        OUT VARCHAR2);

  PROCEDURE PP_CTRL_LIM_CR(P_EMPRESA IN NUMBER, P_DOC_CLI IN NUMBER);

  PROCEDURE PL_CONTROL_OPER_DEP(I_EMPRESA   NUMBER,
                                I_SUCURSAL  NUMBER,
                                I_DEPOSITO  IN NUMBER,
                                I_OPERACION VARCHAR2,
                                P_USUARIO   IN VARCHAR2,
                                I_CLAVE     OUT NUMBER);

  PROCEDURE PP_TRAER_CAMION(P_EMPRESA            IN NUMBER,
                            P_DOC_CAMION         IN NUMBER,
                            P_SUCURSAL           IN NUMBER,
                            P_TRANSPORTISTA      IN NUMBER,
                            P_W_CAPACIDAD_CAMION OUT NUMBER);

  PROCEDURE PP_VALIDAR_CARGA(P_DOC_CAMION        IN NUMBER,
                             P_DOC_OCARGA_LONDON IN OUT NUMBER,
                             P_EMPRESA           IN NUMBER,
                             P_SUCURSAL          IN NUMBER);

  PROCEDURE PP_VALIDAR_LISTA_PRECIO(P_EMPRESA          IN NUMBER,
                                    P_NRO_LISTA_PRECIO IN NUMBER,
                                    P_DOC_MON          IN NUMBER);

  PROCEDURE PP_VALIDAR_ENCABEZADO(P_DOC_CAMION        IN NUMBER,
                                  P_DOC_TRANSPORTISTA IN NUMBER,
                                  P_DOC_OCARGA_LONDON IN NUMBER,
                                  P_PORC_DTO          IN NUMBER,
                                  P_PLAZO_PAGO        IN NUMBER,
                                  P_DOC_LEGAJO        IN NUMBER,
                                  P_DOC_CLI_TEL       IN VARCHAR2,
                                  P_DEPOSITO          IN NUMBER,
                                  P_RECARGO           IN NUMBER,
                                  P_DOC_MON           IN NUMBER,
                                  P_W_DOC_TASA_US     IN NUMBER,
                                  P_DOC_CLI_RUC       IN VARCHAR2,
                                  P_DOC_CTA_BCO_FCON  IN NUMBER,
                                  P_DOC_CLI_DIR       IN VARCHAR2,
                                  P_DOC_CANAL         IN NUMBER,
                                  P_DOC_CLI           IN NUMBER,
                                  P_PED_AUTO          IN VARCHAR2,
                                  -- P_S_NRO_PEDIDO          IN NUMBER,
                                  P_DOC_TIMBRADO          IN NUMBER,
                                  P_DOC_NRO_DOC           IN NUMBER,
                                  P_DOC_SERIE             IN VARCHAR2,
                                  P_DOC_FEC_DOC           IN DATE,
                                  P_S_TIPO_FACTURA        IN NUMBER,
                                  P_EMPRESA               IN NUMBER,
                                  P_SUCURSAL              IN NUMBER,
                                  P_CONF_PER_ACT_INI      IN DATE,
                                  P_CONF_PER_SGTE_FIN     IN DATE,
                                  P_CONF_STK_PER_ACT_INI  IN DATE,
                                  P_CONF_STK_PER_SGTE_FIN IN DATE,
                                  P_USUARIO               IN VARCHAR2,
                                  P_CONF_CATEG_CLI_ESPOR  IN NUMBER,
                                  P_MON_LOC               IN NUMBER,
                                  P_MON_US                IN NUMBER,
                                  P_CONF_COD_FAC_CRED     IN NUMBER,
                                  P_NRO_LISTA_PRECIO      IN NUMBER,
                                  P_MENSAJE               OUT VARCHAR2,
                                  P_CONTINUAR             OUT VARCHAR2);

  PROCEDURE PP_ADICIONAR_ITEM(P_UM                       IN VARCHAR2,
                              P_ARTICULO                 IN NUMBER,
                              P_CANTIDAD                 IN NUMBER,
                              P_PRECIO                   IN NUMBER,
                              P_IMPORTE                  IN NUMBER,
                              P_ART_KG_CONTENIDO         IN NUMBER,
                              P_ART_FACTOR_CONVERSION    IN NUMBER,
                              P_AREM_PRECIO_VTA          IN NUMBER,
                              P_ART_UM                   IN VARCHAR2,
                              P_IMPU_PORCENTAJE          IN NUMBER,
                              P_IMPU_PORC_BASE_IMPONIBLE IN NUMBER,
                              I_CLAVE_CONTRATO           IN NUMBER,
                              V_TOTAL                    IN NUMBER DEFAULT NULL,
                              V_TASA                     IN NUMBER);

  PROCEDURE PP_FACTURA(P_EMPRESA             IN NUMBER,
                       P_TIPO_FACTURA        IN NUMBER,
                       P_MAX_ITEMS_FAC_CO    OUT NUMBER,
                       P_MAX_ITEMS_FAC_CR    OUT NUMBER,
                       P_DOC_TIPO_MOV        OUT NUMBER,
                       P_CONF_COD_FAC_CONT   IN NUMBER,
                       P_CONF_COD_FAC_CRED   IN NUMBER,
                       P_DOC_TIMBRADO        OUT NUMBER,
                       P_DOC_TIPO_SALDO      OUT VARCHAR2,
                       P_IND_CUOTA           OUT VARCHAR2,
                       P_CONF_IND_CUOTA_AUTO IN VARCHAR2);

  PROCEDURE PP_CONTROL_ARTICULO(P_ARTICULO                  IN NUMBER,
                                P_EMPRESA                   IN NUMBER,
                                P_DOC_OPERADOR              IN NUMBER,
                                P_DOC_MON                   IN NUMBER,
                                P_RECARGO                   IN NUMBER,
                                P_DOC_FEC_DOC               IN DATE,
                                P_LIPR_NRO_LISTA_PRECIO     IN NUMBER,
                                P_UNIDAD_VTA                IN VARCHAR2,
                                P_CONF_IND_MODIFICAR_PRECIO IN VARCHAR2,
                                P_MON_SIMBOLO               IN VARCHAR2,
                                P_MON_US                    IN NUMBER,
                                P_MON_LOC                   IN NUMBER,
                                P_DOC_TASA_US               IN NUMBER,
                                P_SUCURSAL                  IN NUMBER,
                                P_DEPOSITO                  IN NUMBER,
                                P_ART_CODIGO_FABRICA        OUT STK_ARTICULO.ART_CODIGO_FABRICA%TYPE,
                                P_ART_MARCA                 OUT STK_ARTICULO.ART_MARCA%TYPE,
                                P_ART_IMPU                  OUT STK_ARTICULO.ART_IMPU%TYPE,
                                P_ART_EST                   OUT STK_ARTICULO.ART_EST%TYPE,
                                P_ART_UNID_MED              OUT STK_ARTICULO.ART_UNID_MED%TYPE,
                                P_ART_TIPO_COMISION         OUT STK_ARTICULO.ART_TIPO_COMISION%TYPE,
                                P_ART_COD_ALFANUMERICO      OUT STK_ARTICULO.ART_COD_ALFANUMERICO%TYPE,
                                P_ART_FACTOR_CONVERSION     OUT STK_ARTICULO.ART_FACTOR_CONVERSION%TYPE,
                                P_IMPU_PORCENTAJE           OUT GEN_IMPUESTO.IMPU_PORCENTAJE%TYPE,
                                P_IMPU_PORC_BASE_IMPONIBLE  OUT GEN_IMPUESTO.IMPU_PORC_BASE_IMPONIBLE%TYPE,
                                P_ART_KG_CONTENIDO          OUT NUMBER, --STK_ARTICULO.ART_KG_CONTENIDO%TYPE,
                                P_ART_COD_BARRA             OUT STK_ARTICULO.ART_COD_BARRA%TYPE,
                                P_ENVA_IND_HA               OUT STK_ENVASES.ENVA_IND_HA%TYPE,
                                P_ART_IND_FACT_NEGATIVO     OUT STK_ARTICULO.ART_IND_FACT_NEGATIVO%TYPE,
                                P_ART_CONCEPTO              OUT FIN_CONCEPTO.FCON_CLAVE%TYPE,
                                P_ART_CTA_CONTABLE          OUT STK_CLASIFICACION.CLAS_CTACO_VENTA%TYPE,
                                P_CANT_REM                  OUT NUMBER,
                                P_IMPU_CODIGO               OUT NUMBER,
                                P_AREM_PRECIO_VTA           OUT NUMBER,
                                P_PRECIO                    OUT NUMBER,
                                P_ADICIONAR                 OUT NUMBER,
                                P_ART_DESC                  OUT STK_ARTICULO.ART_DESC%TYPE,
                                P_EXPORTACION               IN VARCHAR2,
                                P_CLAVE_PROF                NUMBER);

  PROCEDURE PP_RECALCULAR_DETALLES(I_UNIDAD_VTA               IN VARCHAR2,
                                   I_ART_UNID_MED             IN VARCHAR2,
                                   I_ART_FACTOR_CONVERSION    IN NUMBER,
                                   I_PRECIO                   IN NUMBER,
                                   I_DET_CANT                 IN NUMBER,
                                   I_AREM_PRECIO_VTA          IN NUMBER,
                                   I_PORC_DTO                 IN NUMBER,
                                   I_IMPU_PORCENTAJE          IN NUMBER,
                                   I_W_CLI_PORC_EXEN_IVA      IN NUMBER,
                                   I_IMPU_PORC_BASE_IMPONIBLE IN NUMBER,
                                   I_MON_LOC                  IN NUMBER,
                                   I_DOC_TASA_US              IN NUMBER,
                                   I_DOC_MON                  IN NUMBER,
                                   I_TIPO_FACTURA             IN NUMBER,
                                   I_PORC_IVA                 IN NUMBER,
                                   I_MON_DEC_IMP              IN NUMBER,
                                   O_DET_BRUTO_LOC            OUT NUMBER,
                                   O_DET_BRUTO_MON            OUT NUMBER,
                                   O_DET_NETO_LOC             OUT NUMBER,
                                   O_DET_NETO_MON             OUT NUMBER,
                                   O_DET_IVA_LOC              OUT NUMBER,
                                   O_DET_IVA_MON              OUT NUMBER,
                                   O_TOTAL                    OUT NUMBER,
                                   O_BRUTO_EXEN_PRECIO_MON    OUT NUMBER,
                                   O_BRUTO_GRAV_PRECIO_MON    OUT NUMBER,
                                   O_BRUTO_EXEN_MON           OUT NUMBER,
                                   O_BRUTO_GRAV_MON           OUT NUMBER,
                                   O_BRUTO_EXEN_LOC           OUT NUMBER,
                                   O_BRUTO_GRAV_LOC           OUT NUMBER,
                                   O_IVA_MON                  OUT NUMBER,
                                   O_IVA_LOC                  OUT NUMBER,
                                   O_NETO_EXEN_LOC            OUT NUMBER,
                                   O_NETO_EXEN_MON            OUT NUMBER,
                                   O_NETO_GRAV_LOC            OUT NUMBER,
                                   O_NETO_GRAV_MON            OUT NUMBER);

  PROCEDURE PP_CALCULAR_TOTALES(I_PORC_DTO IN NUMBER,
                                --I_IMPU_PORCENTAJE          IN NUMBER,
                                I_W_CLI_PORC_EXEN_IVA IN NUMBER,
                                --I_IMPU_PORC_BASE_IMPONIBLE IN NUMBER,
                                I_MON_LOC       IN NUMBER,
                                I_DOC_TASA_US   IN NUMBER,
                                I_DOC_MON       IN NUMBER,
                                I_TIPO_FACTURA  IN NUMBER,
                                I_PORC_IVA      IN NUMBER,
                                I_MON_DEC_IMP   IN NUMBER,
                                O_TOTAL_DTO     OUT NUMBER,
                                O_TOTAL_IVA     OUT NUMBER,
                                O_TOTAL         OUT NUMBER,
                                O_NETO_EXEN_MON OUT NUMBER,
                                O_NETO_GRAV_MON OUT NUMBER);

  PROCEDURE PP_VALIDAR_ITEM( --I_ARDE_CANT_ACT   IN NUMBER,
                            --I_W_SALDO_PED_SUC_IMP IN NUMBER,
                            --I_W_CANT_REM      IN NUMBER,
                            --I_ART_DESC        IN VARCHAR2,
                            --I_ART             IN VARCHAR2,
                            I_DET_CANT_PEDIDO       IN NUMBER,
                            I_DET_CANT              IN OUT NUMBER,
                            I_UNIDAD_VTA            IN VARCHAR2,
                            I_ART_FACTOR_CONVERSION IN NUMBER,
                            O_DET_CANT_GUARDADA     OUT NUMBER,
                            I_ART_IND_FACT_NEGATIVO IN VARCHAR2,
                            P_SUCURSAL              IN NUMBER,
                            P_EMPRESA               IN NUMBER,
                            P_DEPOSITO              IN NUMBER,
                            P_ARTICULO              IN NUMBER,
                            I_CONF_FACT_CERO        IN VARCHAR2,
                            I_W_IND_LISTVAL         IN VARCHAR2,
                            I_DET_NRO_REMIS         IN NUMBER,
                            O_CONFIRMAR             OUT VARCHAR2);

  PROCEDURE PP_INSERTAR_FACTURA_CABECERA(I_DOC_SUC               IN NUMBER,
                                         I_DOC_DEP               IN NUMBER,
                                         I_DOC_TIPO_MOV          IN NUMBER,
                                         I_DOC_CTA_BCO           IN NUMBER,
                                         I_DOC_MON               IN NUMBER,
                                         I_DOC_CLI               IN NUMBER,
                                         I_DOC_CLI_NOM           IN VARCHAR2,
                                         I_DOC_CLI_DIR           IN VARCHAR2,
                                         I_DOC_CLI_RUC           IN VARCHAR2,
                                         I_DOC_CLI_TEL           IN VARCHAR2,
                                         I_COD_VENDEDOR          IN NUMBER,
                                         I_DOC_FEC_DOC           IN DATE,
                                         I_DOC_FEC_OPER          IN DATE,
                                         I_DOC_NETO_GRAV_MON     IN NUMBER,
                                         I_DOC_NETO_EXEN_MON     IN NUMBER,
                                         I_DOC_IVA_MON           IN NUMBER,
                                         I_DOC_NRO_DOC           IN NUMBER,
                                         I_DOC_IND_IMPR_VENDEDOR IN VARCHAR2,
                                         I_DOC_IND_IMPR_CONYUGUE IN VARCHAR2,
                                         I_DOC_CANAL             IN NUMBER,
                                         I_DOC_TIMBRADO          IN NUMBER,
                                         I_OCARGA_LONDON         IN NUMBER,
                                         I_EMPR                  IN NUMBER,
                                         I_TASA                  IN NUMBER,
                                         I_OBS                   IN VARCHAR2,
                                         I_USER                  IN VARCHAR2,
                                         O_DOC_CLAVE_FIN         OUT NUMBER,
                                         O_DOC_CLAVE_STK         OUT NUMBER,
                                         I_EXPORTACION           IN VARCHAR2);

  PROCEDURE PP_INSERTAR_FACTURA_CUOTA(I_CLAVE_FIN IN NUMBER,
                                      I_FEC_VTO   IN DATE,
                                      I_IMP_MON   IN NUMBER,
                                      I_EMPR      IN NUMBER,
                                      I_TASA      IN NUMBER);

  PROCEDURE PP_INSERTAR_FACTURA_DETALLE(P_CLAVE_FIN      IN NUMBER,
                                        P_CLAVE_STK      IN NUMBER,
                                        P_NRO_ITEM       IN NUMBER,
                                        P_ART            IN NUMBER,
                                        P_EMPR           IN NUMBER,
                                        P_CANT           IN NUMBER,
                                        P_NETO_MON       IN NUMBER,
                                        P_CODIGO_IMPU    IN NUMBER,
                                        P_IVA_MON        IN NUMBER,
                                        P_EXPORTACION    IN VARCHAR2,
                                        P_TASA           IN NUMBER,
                                        P_CLAVE_CONTRATO IN NUMBER);

  PROCEDURE PP_INSERTAR_TARJETAS(I_DOC_SUC          IN NUMBER,
                                 I_DOC_CTA_BCO      IN NUMBER,
                                 I_DOC_MON          IN NUMBER,
                                 I_DOC_FEC_DOC      IN DATE,
                                 I_DOC_NRO_DOC      IN NUMBER,
                                 I_EMPR             IN NUMBER,
                                 I_TASA             IN NUMBER,
                                 I_USER             IN VARCHAR2,
                                 I_DOC_CLAVE_PADRE  IN NUMBER, --RECIBE LA CLAVE DE LA FACTURA
                                 I_TARJ_TARJETA     IN NUMBER,
                                 I_TARJ_NRO_TARJETA IN VARCHAR2,
                                 I_TARJ_FEC_VTO     IN DATE,
                                 I_TARJ_IMP_LOC     IN NUMBER);

  PROCEDURE PP_INSERTAR_CHEQUES(I_DOC_SUC            IN NUMBER,
                                I_DOC_CTA_BCO        IN NUMBER,
                                I_DOC_MON            IN NUMBER,
                                I_DOC_FEC_DOC        IN DATE,
                                I_DOC_NRO_DOC        IN NUMBER,
                                I_EMPR               IN NUMBER,
                                I_TASA               IN NUMBER,
                                I_USER               IN VARCHAR2,
                                I_DOC_CLAVE_PADRE    IN NUMBER, --RECIBE LA CLAVE DE LA FACTURA
                                I_CHEQ_NRO           IN VARCHAR2,
                                I_CHEQ_SERIE         IN VARCHAR2,
                                I_CHEQ_BCO           IN NUMBER,
                                I_CHEQ_MON           IN NUMBER,
                                I_CHEQ_IMPORTE       IN NUMBER,
                                I_CLI_CODIGO         IN NUMBER,
                                I_CLI_NOM            IN VARCHAR2,
                                I_CHEQ_FEC_DEPOSITAR IN DATE,
                                I_CHEQ_NRO_CTA_CHEQ  IN VARCHAR2);

  PROCEDURE PP_ADICIONAR_TARJETA(P_TARJ_TARJETA     IN NUMBER,
                                 P_TARJ_NRO_TARJETA IN VARCHAR2,
                                 P_TARJ_FEC_VTO     IN DATE,
                                 P_TARJ_IMP_LOC     IN NUMBER);

  PROCEDURE PP_ADICIONAR_CHEQUE(P_W_CHEQ_BCO          IN NUMBER,
                                P3_CHEQ_SERIE         IN VARCHAR2,
                                P3_CHEQ_NRO           IN VARCHAR2,
                                P3_CHEQ_MON           IN NUMBER,
                                P3_CHEQ_FEC_DEPOSITAR IN DATE,
                                P3_IMPORTE            IN NUMBER,
                                P3_TASA               IN NUMBER,
                                P3_CHEQ_IMPORTE_LOC   IN NUMBER,
                                P3_CHEQ_NRO_CTA_CHEQ  IN VARCHAR2);

  PROCEDURE PP_VALIDAR_CHEQUES(P_W_CHEQ_BCO          IN NUMBER,
                               P3_CHEQ_SERIE         IN VARCHAR2,
                               P3_CHEQ_NRO           IN VARCHAR2,
                               P3_CHEQ_MON           IN NUMBER,
                               P3_CHEQ_FEC_DEPOSITAR IN DATE,
                               P3_IMPORTE            IN NUMBER,
                               P3_TASA               IN OUT NUMBER,
                               P3_CHEQ_IMPORTE_LOC   IN OUT NUMBER,
                               P3_CHEQ_NRO_CTA_CHEQ  IN VARCHAR2,
                               P_EMPRESA             IN NUMBER,
                               P_FEC_DOC             IN DATE,
                               P_MON_LOC             IN NUMBER);

  PROCEDURE PP_VALIDAR_TARJETAS(P_TARJ_TARJETA      IN NUMBER,
                                P_TARJ_NRO_TARJETA  IN VARCHAR2,
                                P_TARJ_FEC_VTO      IN DATE,
                                P_TARJ_IMP_LOC      IN NUMBER,
                                P_CONF_PER_ACT_INI  IN DATE,
                                P_CONF_PER_SGTE_FIN IN DATE,
                                P_FEC_INIC_SISTEMA  IN DATE,
                                P_FEC_FIN_SISTEMA   IN DATE,
                                P_DOC_FEC_DOC       IN DATE);

  PROCEDURE PP_VALIDAR_CUOTAS(P_CANT_CUOTAS       IN NUMBER,
                              P_FEC_PRIM_VTO      IN DATE,
                              P_TIPO_VENCIMIENTO  IN VARCHAR2,
                              P_DIAS_ENTRE_CUOTAS IN NUMBER,
                              P_MON_DEC_IMP       IN NUMBER);

  PROCEDURE PP_GENERAR_CUOTAS(P_CANT_CUOTAS       IN NUMBER,
                              P_FEC_PRIM_VTO      IN DATE,
                              P_TIPO_VENCIMIENTO  IN VARCHAR2,
                              P_DIAS_ENTRE_CUOTAS IN NUMBER,
                              P_MON_DEC_IMP       IN NUMBER,
                              P_TOTAL             IN NUMBER,
                              P_FUNCIONARIO       IN NUMBER DEFAULT 0,
                              P_LIMITE_CREDITO    IN NUMBER DEFAULT NULL,
                              P_MONEDA            IN NUMBER DEFAULT 1,
                              P_FEC_DOC           IN DATE DEFAULT NULL);

  PROCEDURE PP_VAL_CUOTA_PAGO(P_TIPO_FACTURA IN NUMBER, P_TOTAL IN NUMBER);

  PROCEDURE PP_VALIDAR_CHEQ_DIF(P_DOC_FEC_DOC             IN DATE,
                                P_DOC_CLI                 IN NUMBER,
                                P_W_IMP_LIM_CR_EMPR       IN OUT NUMBER,
                                P_EMPRESA                 IN NUMBER,
                                P_W_IMP_LIM_CR_DISP_GRUPO IN OUT NUMBER,
                                P_W_IMP_LIM_CR_DISP_EMPR  IN OUT NUMBER,
                                P_W_IMP_CHEQ_DIFERIDO     IN OUT NUMBER,
                                P_W_IMP_CHEQ_RECHAZADO    IN OUT NUMBER);

  PROCEDURE PP_TERMINAR_PROCESO(I_DOC_OPERADOR IN NUMBER,
                                I_DOC_NRO_DOC  IN NUMBER,
                                P_EMPRESA      IN NUMBER,
                                P_IMPRESORA    IN NUMBER);

  PROCEDURE PP_ESTABLECER_IMPORTES_LOC(I_DOC_CLAVE IN NUMBER,
                                       I_EMPRESA   IN NUMBER,
                                       I_TASA      IN NUMBER);

  PROCEDURE PP_VALIDAR_PRECIO(IO_PRECIO                  IN OUT NUMBER,
                              I_ART_UNID_MED             IN VARCHAR2,
                              I_UNIDAD_VTA               IN VARCHAR2,
                              I_IMPU_PORC_BASE_IMPONIBLE IN NUMBER,
                              I_IMPU_PORCENTAJE          IN NUMBER,
                              I_ART_FACTOR_CONVERSION    IN NUMBER,
                              O_AREM_PRECIO_VTA          OUT NUMBER);

  PROCEDURE PP_ACTUALIZAR_AUTORIZ_ESPEC(P_EMPRESA     IN NUMBER,
                                        P_CLIENTE     IN NUMBER,
                                        P_DOC_FEC_DOC IN DATE,
                                        P_MONEDA      IN NUMBER,
                                        P_LIST_NEGRA  IN VARCHAR2 DEFAULT NULL);

  PROCEDURE PP_CONTROLAR_LIMITE(IMPORTE                IN NUMBER,
                                I_DOC_FEC_DOC          IN DATE,
                                I_DOC_CLI              IN NUMBER,
                                I_IMP_LIM_CR_DISP_EMPR IN NUMBER,
                                I_EMPRESA              IN NUMBER,
                                I_DOC_MON              IN NUMBER,
                                O_LIM_AUTORIZADO       OUT VARCHAR2);

  PROCEDURE PP_VAL_FACT_PEND_COBRO;

  PROCEDURE PP_AUTORIZACION_ESPECIAL(P_EMPRESA    IN NUMBER,
                                     P_CLIENTE    IN NUMBER,
                                     P_LISTA_NEG  IN VARCHAR2,
                                     P_SOL_ESTADO OUT NUMBER,
                                     P_TIPO_MV    IN NUMBER,
                                     P_MONEDA     IN NUMBER);

  PROCEDURE PP_HISTORICO_LISTA_NEGRA(P_DOC_CLI       IN NUMBER,
                                     P_EMPRESA       IN NUMBER,
                                     P_MARCA         OUT VARCHAR2,
                                     P_LISTA_NEGRA_H OUT VARCHAR2);

  PROCEDURE PP_BLOQ_PROV_CLI(P_EMPRESA IN NUMBER, P_CLIENTE IN NUMBER);

  FUNCTION FP_CLI_ES_EMPL(I_CLI IN NUMBER, I_EMPRESA IN NUMBER)
    RETURN BOOLEAN;

  PROCEDURE PP_VAL_MAX_ATRASO(I_EMPRESA      IN NUMBER,
                              I_DOC_CLI      IN NUMBER,
                              I_DOC_FEC_DOC  IN DATE,
                              I_MON          IN NUMBER,
                              I_TIPO_FACTURA IN NUMBER);

  PROCEDURE PP_BORRAR_DETALLE(I_SEQ IN NUMBER);

  PROCEDURE PP_VACIAR_COLLECTION;

  PROCEDURE PP_VALIDAR_DESCUENTO(I_EMPRESA     IN NUMBER,
                                 I_DOC_CLI     IN NUMBER,
                                 I_ART         IN NUMBER,
                                 I_DOC_FEC_DOC IN DATE,
                                 I_CLAVE_DESC  IN NUMBER);

  PROCEDURE PP_AUTORIZACION_LIMITE(P_EMPRESA    IN NUMBER,
                                   P_CLIENTE    IN NUMBER,
                                   P_MONEDA     IN NUMBER,
                                   P_SOL_ESTADO OUT NUMBER,
                                   P_IMP_SOL    OUT NUMBER);
END FACI039;
/
CREATE OR REPLACE PACKAGE BODY FACI039 AS

  FUNCTION FL_OBTENER_ULT_NRO(I_TIPO_MOV  IN NUMBER,
                              I_IMPRESORA IN NUMBER,
                              I_EMPRESA   IN NUMBER) RETURN CHAR IS
    /*
    RETORNA EL ULTIMO NRO GRABADO DE ACUERDO AL TIPO DE MOVIMIENTO POR IMPRESORA EN FORMTO DE 13 DIGITOS.
    */
    V_NRO NUMBER := 0;
  BEGIN
    IF I_TIPO_MOV IN (9, 10) THEN
      --FACTURAS, CONTADO Y CREDITO
      --GEN_EXEC_SELECT('SELECT IMP_ULT_FACT FROM GEN_IMPRESORA WHERE IMP_EMPR = '||I_EMPRESA||' AND IMP_IP='||OWA_UTIL.GET_CGI_ENV('REMOTE_ADDR'), V_NRO);-- SE PUEDE UTILIZAR PARA HACER HACKING
      SELECT IMP_ULT_FACT
        INTO V_NRO
        FROM GEN_IMPRESORA
       WHERE IMP_EMPR = I_EMPRESA
         AND IMPR_IP = OWA_UTIL.GET_CGI_ENV('REMOTE_ADDR');
    END IF;
  
    IF I_TIPO_MOV = 7 THEN
      --AUTOFACTURAS
      --GEN_EXEC_SELECT('SELECT IMP_ULT_FACT_COMPRA FROM GEN_IMPRESORA WHERE IMP_EMPR = '||I_EMPRESA||' AND IMP_CODIGO='||I_IMPRESORA, V_NRO);
      SELECT IMP_ULT_FACT_COMPRA
        INTO V_NRO
        FROM GEN_IMPRESORA
       WHERE IMP_EMPR = I_EMPRESA
         AND IMPR_IP = OWA_UTIL.GET_CGI_ENV('REMOTE_ADDR');
    END IF;
    IF I_TIPO_MOV = 16 THEN
      --NOTA DE CREDITO
      SELECT IMP_ULT_NOTA_CREDITO
        INTO V_NRO
        FROM GEN_IMPRESORA
       WHERE IMP_EMPR = I_EMPRESA
         AND IMPR_IP = OWA_UTIL.GET_CGI_ENV('REMOTE_ADDR');
    
    END IF;
    IF I_TIPO_MOV IN (61, 62, 64) THEN
      --RETENCIONES IVA Y RENTA
      SELECT IMP_ULT_RETENCION
        INTO V_NRO
        FROM GEN_IMPRESORA
       WHERE IMP_EMPR = I_EMPRESA
         AND IMPR_IP = OWA_UTIL.GET_CGI_ENV('REMOTE_ADDR');
    
    END IF;
  
    RETURN LPAD(NVL(V_NRO, 0) + 1, 13, '0');
    --EXCEPTION
    --  WHEN OTHERS THEN
    --     RAISE_APPLICATION_ERROR(-20010,'Error en la configuraci?n de la impresora, obtener ?ltimo n?mero');
    --NULL;
  END FL_OBTENER_ULT_NRO;

  PROCEDURE PP_BUSCAR_NRO_FACTURA(P_EMPRESA      IN NUMBER,
                                  P_DOC_TIPO_MOV IN NUMBER,
                                  P_IMPRESORA    IN NUMBER,
                                  P_DOC_NRO_DOC  OUT NUMBER,
                                  P_DOC_TIMBRADO IN NUMBER) AS
    V_EXISTE NUMBER;
    --V_TIPO_IMPRESION VARCHAR2(2);
  BEGIN
  
    P_DOC_NRO_DOC := FL_OBTENER_ULT_NRO(P_DOC_TIPO_MOV,
                                        P_IMPRESORA,
                                        P_EMPRESA);
  
    SELECT COUNT(*)
      INTO V_EXISTE
      FROM FIN_DOCUMENTO
     WHERE DOC_NRO_DOC = P_DOC_NRO_DOC
       AND DOC_TIMBRADO = P_DOC_TIMBRADO
       AND DOC_TIPO_MOV IN (9, 10)
       AND DOC_EMPR = P_EMPRESA;
  
    IF V_EXISTE > 1 THEN
      RAISE_APPLICATION_ERROR(-20002,
                              'Numero de factura y timbrado existente');
    END IF;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20002,
                              'Primero debe ingresar un registro para la impresora: ' ||
                              TO_CHAR(P_IMPRESORA));
  END PP_BUSCAR_NRO_FACTURA;

  /*
   PROCEDURE AF_VALIDAR_FECHA(P_ACCION_EN IN VARCHAR2,
                              P_CAMPO_US_EN IN VARCHAR2,
                              P_CAMPO_DB_EN IN VARCHAR2,
                              P_FECHA_INICIO_EN IN DATE)
  /*
   AUTOR: EITEL LAVALL
   FECHA: MAYO 1996
   RESUMEN   : SI ACCION_EN ES 'LOV' (LISTA DE VALORES) ENTONCES LLAMAR AL
               PROCEDIMIENTO AF_CALENDAR. SI ACCION_EN ES 'OVF' ENTONCES
               CONVERTIR LA ENTRADA A UNA FECHA CON EL PROCEDIMIENTO
               AF_CONVERTIR_FECHA. DESPUES DE OBTENIDO LOS RESULTADOS
               COPIARLOS A LOS RESPECTIVOS CAMPOS CON EL SUB PROCEDIMIENTO
               COPIAR_VALOR_FECHA.
  
  IS
  
     VALOR_US VARCHAR2(255) := NAME_IN (P_CAMPO_US_EN);
     CAMPO_DB VARCHAR2(80) := UPPER (P_CAMPO_DB_EN);
     VALOR_FECHA DATE;
  
     --
     PROCEDURE COPIAR_VALOR_FECHA (ACCION_EN IN VARCHAR2,
                                   CAMPO_DB_EN IN VARCHAR2,
                                   VALOR_FECHA_EN IN DATE)
     IS
     BEGIN
        IF VALOR_FECHA_EN IS NOT NULL
        THEN
           COPY (TO_CHAR(VALOR_FECHA_EN, NAME_IN('DD/MM/YYYY')), CAMPO_DB_EN);
           COPY (TO_CHAR(VALOR_FECHA_EN, NAME_IN('DD/MM/YYYY')), CAMPO_US_EN);
           IF ACCION_EN = 'LOV' THEN DNULL; END IF;
        ELSE
           IF ACCION_EN = 'OVF' THEN
             RAISE_APPLICATION_ERROR(-20002,'Fecha invalida!');
           END IF;
        END IF;
     END;
  
  
  --CUERPO PRINCIPAL DE AF_VALIDAR_FECHA
  BEGIN
  
    IF P_CAMPO_DB_EN IS NULL
    THEN
       P_CAMPO_DB := REPLACE (P_CAMPO_US_EN, '.'||NAME_IN('GLOBAL.AF_PREFIJO'), '.');
    ELSE
       CAMPO_DB := UPPER (CAMPO_DB_EN);
    END IF;
  
    -- SI EL CAMPO DE DESTINO NO ES DEL TIPO FECHA ENTONCES NO SE DEVUELVE NADA.
    IF GET_ITEM_PROPERTY(CAMPO_DB,DATATYPE) LIKE '%DATE%' THEN
      IF ACCION_EN = 'LOV' THEN -- SELECCIONAR UNA FECHA DEL CALENDARIO
        AF_CALENDARIO (VALOR_FECHA, FECHA_INICIO_EN);
        COPIAR_VALOR_FECHA ('LOV', CAMPO_DB, VALOR_FECHA);
      ELSIF ACCION_EN = 'OVF' AND NAME_IN('GLOBAL.AF_FLAG_VALIDACION') = 'ON' THEN
        IF VALOR_US IS NULL THEN
           IF GET_ITEM_PROPERTY (CAMPO_DB, REQUIRED) = 'TRUE' THEN
              MESSAGE ('No puede ser nulo!');BELL;
              RAISE FORM_TRIGGER_FAILURE;
           ELSE
              COPY (NULL, CAMPO_DB);
           END IF;
        ELSE
           COPIAR_VALOR_FECHA ('OVF', CAMPO_DB, AF_CONVERTIR_FECHA (VALOR_US));
        END IF;
      END IF;
    END IF;
  END;*/

  PROCEDURE PP_VALIDAR_FECHA(P_DOC_FEC_DOC           IN DATE,
                             P_CONF_PER_ACT_INI      IN DATE,
                             P_CONF_PER_SGTE_FIN     IN DATE,
                             P_CONF_STK_PER_ACT_INI  IN DATE,
                             P_CONF_STK_PER_SGTE_FIN IN DATE) AS
  BEGIN
    IF P_DOC_FEC_DOC IS NOT NULL THEN
      IF P_DOC_FEC_DOC NOT BETWEEN P_CONF_PER_ACT_INI AND
         P_CONF_PER_SGTE_FIN THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'Fecha debe estar comprendida en el rango de: ' ||
                                                   TO_CHAR(P_CONF_PER_ACT_INI,
                                                           'DD-MM-YYYY') ||
                                                   ' a: ' ||
                                                   TO_CHAR(P_CONF_PER_SGTE_FIN,
                                                           'DD-MM-YYYY') ||
                                                   ' para el sistema de FINANZAS!',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        RAISE_APPLICATION_ERROR(-20002,
                                'Fecha debe estar comprendida en el rango de: ' ||
                                TO_CHAR(P_CONF_PER_ACT_INI, 'DD-MM-YYYY') ||
                                ' a: ' ||
                                TO_CHAR(P_CONF_PER_SGTE_FIN, 'DD-MM-YYYY') ||
                                ' para el sistema de FINANZAS!');
      END IF;
      IF P_DOC_FEC_DOC NOT BETWEEN P_CONF_STK_PER_ACT_INI AND
         P_CONF_STK_PER_SGTE_FIN THEN
        RAISE_APPLICATION_ERROR(-20002,
                                'Fecha debe estar comprendida en el rango de: ' ||
                                TO_CHAR(P_CONF_STK_PER_ACT_INI,
                                        'DD-MM-YYYY') || ' a: ' ||
                                TO_CHAR(P_CONF_STK_PER_SGTE_FIN,
                                        'DD-MM-YYYY') ||
                                ' para el sistema de STOCK!');
      END IF;
    ELSE
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'Fecha no puede ser nulo!',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002, 'Fecha no puede ser nulo!');
    END IF;
  END PP_VALIDAR_FECHA;

  FUNCTION FP_COTIZACION(P_MONEDA      IN NUMBER,
                         P_DOC_FEC_DOC IN DATE,
                         P_EMPRESA     IN NUMBER) RETURN NUMBER IS
  
    V_TASA NUMBER;
  
  BEGIN
    IF P_MONEDA <> 1 THEN
      SELECT COT_TASA
        INTO V_TASA
        FROM STK_COTIZACION
       WHERE TRUNC(COT_FEC) = TO_CHAR(TRUNC(P_DOC_FEC_DOC), 'DD-MM-YYYY')
         AND COT_MON = P_MONEDA
         AND COT_EMPR = P_EMPRESA;
    ELSE
      V_TASA := 1;
    END IF;
    --MESSAGE(TO_CHAR(V_TASA));PAUSE;
    RETURN V_TASA;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20002,
                              'Primero debe ingresar la cotizacion del dia para la moneda ' ||
                              TO_CHAR(P_MONEDA) || '!');
  END FP_COTIZACION;
  PROCEDURE PL_VALIDAR_HAB_MES_FIN(P_FECHA   IN DATE,
                                   P_EMPRESA IN NUMBER,
                                   P_USUARIO IN VARCHAR2) IS
    V_VARIABLE             VARCHAR2(1);
    V_OPER_IND_HAB_MES_ACT VARCHAR2(1);
    V_FECHA_LIM            DATE;
    V_CONF_PER_ACT_INI     DATE;
    V_CONF_PER_SGTE_FIN    DATE;
    V_CONF_PER_SGTE_INI    DATE;
  
  BEGIN
    IF P_EMPRESA <> 2 THEN
      --VALIDAR SI EL PERIODO ACTUAL ESTA HABILITADO
      BEGIN
        SELECT 'X'
          INTO V_VARIABLE
          FROM FIN_CONFIGURACION
         WHERE P_FECHA BETWEEN CONF_FEC_LIM_MOD AND CONF_PER_ACT_FIN
              --AND CONF_IND_HAB_MES_ACT = 'N
           AND CONF_IND_HAB_MES_ACT IN ('N', 'S')
           AND CONF_EMPR = P_EMPRESA;
        --------------------------------------------------------------------------------------------------------------------
        IF V_VARIABLE = 'X' THEN
          BEGIN
            SELECT NVL(OPEM_IND_HAB_MES_ACT, 'N')
              INTO V_OPER_IND_HAB_MES_ACT
              FROM GEN_OPERADOR, GEN_OPERADOR_EMPRESA
             WHERE OPER_CODIGO = OPEM_OPER
               AND OPER_LOGIN = P_USUARIO
               AND OPEM_EMPR = P_EMPRESA;
          
            IF V_OPER_IND_HAB_MES_ACT = 'N' THEN
              APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL PERIODO ACTUAL DE FINANZAS ESTA DESHABILITADO PARA ESTE USUARIO, AVISE AL DPTO. DE INFORMATICA!.',
                                   P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
              RAISE_APPLICATION_ERROR(-20002,
                                      'EL PERIODO ACTUAL DE FINANZAS ESTA DESHABILITADO PARA ESTE USUARIO, AVISE AL DPTO. DE INFORMATICA!.' ||
                                      P_FECHA);
            END IF;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              NULL;
          END;
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
          -- RAISE_APPLICATION_ERROR(-20002,
        --                         'EL PERIODO ACTUAL DE FINANZAS ESTA DESHABILITADO PARA ESTE USUARIO, AVISE AL DPTO. DE INFORMATICA!.' ||
        --                        P_FECHA);
      END;
    ELSE
      -----------TRANSAGRO
      SELECT CONF_FEC_LIM_MOD,
             CONF_PER_ACT_INI,
             CONF_PER_SGTE_FIN,
             CONF_PER_SGTE_INI
        INTO V_FECHA_LIM,
             V_CONF_PER_ACT_INI,
             V_CONF_PER_SGTE_FIN,
             V_CONF_PER_SGTE_INI
        FROM FIN_CONFIGURACION
       WHERE CONF_IND_HAB_MES_ACT IN ('N', 'S')
         AND CONF_EMPR = 2;
      IF P_FECHA < V_FECHA_LIM THEN
        RAISE_APPLICATION_ERROR(-20002,
                                'FECHA ESTA FUERA DE LOS LIMITES, AVISE AL DPTO. DE INFORMATICA!.');
      ELSE
        IF P_FECHA BETWEEN V_CONF_PER_SGTE_INI AND V_CONF_PER_SGTE_FIN THEN
          NULL;
        ELSIF P_FECHA BETWEEN V_FECHA_LIM AND V_CONF_PER_SGTE_FIN THEN
          BEGIN
            SELECT NVL(OPEM_IND_HAB_MES_ACT, 'N')
              INTO V_OPER_IND_HAB_MES_ACT
              FROM GEN_OPERADOR, GEN_OPERADOR_EMPRESA
             WHERE OPER_CODIGO = OPEM_OPER
               AND OPER_LOGIN = P_USUARIO
               AND OPEM_EMPR = P_EMPRESA;
          
            IF V_OPER_IND_HAB_MES_ACT = 'N' THEN
              APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL PERIODO ACTUAL DE FINANZAS ESTA DESHABILITADO PARA ESTE USUARIO, AVISE AL DPTO. DE INFORMATICA!.',
                                   P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
              RAISE_APPLICATION_ERROR(-20002,
                                      'EL PERIODO ACTUAL DE FINANZAS ESTA DESHABILITADO PARA ESTE USUARIO, AVISE AL DPTO. DE INFORMATICA!.');
            END IF;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              NULL;
          END;
        ELSIF P_FECHA NOT BETWEEN V_FECHA_LIM AND V_CONF_PER_SGTE_FIN THEN
          RAISE_APPLICATION_ERROR(-20002,
                                  'FECHA DEBE ESTAR COMPRENDIDA EN EL RANGO DE: ' ||
                                  TO_CHAR(V_CONF_PER_ACT_INI, 'DD-MM-YYYY') ||
                                  ' A: ' ||
                                  TO_CHAR(V_CONF_PER_SGTE_FIN, 'DD-MM-YYYY') ||
                                  ' PARA EL SISTEMA DE FINANZAS!');
        END IF;
      END IF;
    
      ----------------FIN TRANSAGRO
    END IF;
    DECLARE
      V_COCI_IND_PRE_CERRADO VARCHAR2(1);
      V_CONF_SUC             NUMBER;
    BEGIN
      SELECT CONF_SUC
        INTO V_CONF_SUC
        FROM GEN_CONFIGURACION
       WHERE CONF_EMPR = P_EMPRESA;
      SELECT COCI_IND_PRE_CERRADO
        INTO V_COCI_IND_PRE_CERRADO
        FROM GEN_CONTROL_CIERRE
       WHERE COCI_EMPR = P_EMPRESA
         AND COCI_SUC = V_CONF_SUC
         AND COCI_SISTEMA = (SELECT SIST_CODIGO
                               FROM GEN_SISTEMA
                              WHERE SIST_DESC_ABREV = 'FIN')
         AND COCI_PERIODO = (SELECT CONF_PERIODO_ACT
                               FROM FIN_CONFIGURACION
                              WHERE CONF_EMPR = P_EMPRESA)
         AND P_FECHA =
             (SELECT P_FECHA
                FROM FIN_CONFIGURACION
               WHERE P_FECHA BETWEEN CONF_FEC_LIM_MOD AND CONF_PER_ACT_FIN
                 AND CONF_EMPR = P_EMPRESA);
      IF V_COCI_IND_PRE_CERRADO = 'S' THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL PERIODO ACTUAL YA FUE PRE-CERRADO PARA EL SISTEMA DE FINANZAS!',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        RAISE_APPLICATION_ERROR(-20002,
                                'EL PERIODO ACTUAL YA FUE PRE-CERRADO PARA EL SISTEMA DE FINANZAS!');
      
      END IF;
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
  END PL_VALIDAR_HAB_MES_FIN;
  /*---HASTA EL 30/08/2019 PROCEDURE PL_VALIDAR_HAB_MES_FIN(P_FECHA   IN DATE,
                                   P_EMPRESA IN NUMBER,
                                   P_USUARIO IN VARCHAR2) IS
    V_VARIABLE             VARCHAR2(1);
    V_OPER_IND_HAB_MES_ACT VARCHAR2(1);
  BEGIN
    --VALIDAR SI EL PERIODO ACTUAL ESTA HABILITADO
    BEGIN
      SELECT 'X'
        INTO V_VARIABLE
        FROM FIN_CONFIGURACION
       WHERE P_FECHA BETWEEN CONF_FEC_LIM_MOD AND CONF_PER_ACT_FIN
            --AND CONF_IND_HAB_MES_ACT = 'N'
         AND CONF_IND_HAB_MES_ACT IN ('N', 'S')
         AND CONF_EMPR = P_EMPRESA;
      --------------------------------------------------------------------------------------------------------------------
      IF V_VARIABLE = 'X' THEN
        BEGIN
          SELECT NVL(OPEM_IND_HAB_MES_ACT, 'N')
            INTO V_OPER_IND_HAB_MES_ACT
            FROM GEN_OPERADOR, GEN_OPERADOR_EMPRESA
           WHERE OPER_CODIGO = OPEM_OPER
             AND OPER_LOGIN = P_USUARIO
             AND OPEM_EMPR = P_EMPRESA;
  
          IF V_OPER_IND_HAB_MES_ACT = 'N' THEN
            APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL PERIODO ACTUAL DE FINANZAS ESTA DESHABILITADO PARA ESTE USUARIO, AVISE AL DPTO. DE INFORMATICA!.',
                                 P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
            RAISE_APPLICATION_ERROR(-20002,
                                    'EL PERIODO ACTUAL DE FINANZAS ESTA DESHABILITADO PARA ESTE USUARIO, AVISE AL DPTO. DE INFORMATICA!.');
          END IF;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            NULL;
        END;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
  
    --SI YA SE HIZO EL PRE-CIERRE DEL SISTEMA FIN ENTONCES
    --NO SE DEBE PERMITIR INGRESAR MOVIMIENTOS
    DECLARE
      V_COCI_IND_PRE_CERRADO VARCHAR2(1);
      V_CONF_SUC             NUMBER;
    BEGIN
      SELECT CONF_SUC
        INTO V_CONF_SUC
        FROM GEN_CONFIGURACION
       WHERE CONF_EMPR = P_EMPRESA;
      SELECT COCI_IND_PRE_CERRADO
        INTO V_COCI_IND_PRE_CERRADO
        FROM GEN_CONTROL_CIERRE
       WHERE COCI_EMPR = P_EMPRESA
         AND COCI_SUC = V_CONF_SUC
         AND COCI_SISTEMA = (SELECT SIST_CODIGO
                               FROM GEN_SISTEMA
                              WHERE SIST_DESC_ABREV = 'FIN')
         AND COCI_PERIODO = (SELECT CONF_PERIODO_ACT
                               FROM FIN_CONFIGURACION
                              WHERE CONF_EMPR = P_EMPRESA)
         AND P_FECHA =
             (SELECT P_FECHA
                FROM FIN_CONFIGURACION
               WHERE P_FECHA BETWEEN CONF_FEC_LIM_MOD AND CONF_PER_ACT_FIN
                 AND CONF_EMPR = P_EMPRESA);
      IF V_COCI_IND_PRE_CERRADO = 'S' THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL PERIODO ACTUAL YA FUE PRE-CERRADO PARA EL SISTEMA DE FINANZAS!',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        RAISE_APPLICATION_ERROR(-20002,
                                'EL PERIODO ACTUAL YA FUE PRE-CERRADO PARA EL SISTEMA DE FINANZAS!');
      END IF;
  
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
  END PL_VALIDAR_HAB_MES_FIN;*/

  PROCEDURE PL_VALIDAR_HAB_MES_STK(P_FECHA   IN DATE,
                                   P_EMPRESA IN NUMBER,
                                   P_USUARIO IN VARCHAR2) IS
    V_VARIABLE             VARCHAR2(1);
    V_OPER_IND_HAB_MES_ACT VARCHAR2(1);
  
  BEGIN
    --VALIDAR SI ESTA DESHABILITADO EL PERIODO ACTUAL DE STOCK
    BEGIN
      SELECT 'X'
        INTO V_VARIABLE
        FROM STK_CONFIGURACION, STK_PERIODO
       WHERE CONF_PERIODO_ACT = PERI_CODIGO
         AND CONF_EMPR = PERI_EMPR
         AND P_FECHA BETWEEN PERI_FEC_INI AND PERI_FEC_FIN
            --AND CONF_IND_HAB_MES_ACT = 'N'
         AND CONF_IND_HAB_MES_ACT IN ('N', 'S')
         AND CONF_EMPR = P_EMPRESA;
      -----------------------------------------------------------------------------------------------------------
      IF V_VARIABLE = 'X' THEN
        BEGIN
          SELECT NVL(OPEM_IND_HAB_MES_STK, 'N')
            INTO V_OPER_IND_HAB_MES_ACT
            FROM GEN_OPERADOR, GEN_OPERADOR_EMPRESA
           WHERE OPER_CODIGO = OPEM_OPER
             AND OPER_LOGIN = P_USUARIO
             AND OPEM_EMPR = P_EMPRESA;
        
          IF V_OPER_IND_HAB_MES_ACT = 'N' THEN
            APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL PERIODO ACTUAL DE STOCK ESTA DESHABILITADO PARA ESTE USUARIO, AVISE AL DPTO. DE INFORMATICA!',
                                 P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
            RAISE_APPLICATION_ERROR(-20002,
                                    'EL PERIODO ACTUAL DE STOCK ESTA DESHABILITADO PARA ESTE USUARIO, AVISE AL DPTO. DE INFORMATICA!.');
          END IF;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            NULL;
        END;
      END IF;
      -----------------------------------------------------------------------------------------------------------------
      --PL_EXHIBIR_ERROR('EL PERIODO ACTUAL ESTA DESHABILITADO PARA EL SISTEMA DE STOCK, AVISE AL DPTO. DE INFORMATICA!.');
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
  
    --SI YA SE HIZO EL PRE-CIERRE DEL SISTEMA STK ENTONCES
    --NO SE DEBE PERMITIR INGRESAR MOVIMIENTOS
    DECLARE
      V_COCI_IND_PRE_CERRADO VARCHAR2(1);
      V_CONF_EMPR            NUMBER;
      V_CONF_SUC             NUMBER;
    BEGIN
      SELECT CONF_EMPR, CONF_SUC
        INTO V_CONF_EMPR, V_CONF_SUC
        FROM GEN_CONFIGURACION
       WHERE CONF_EMPR = P_EMPRESA;
      SELECT COCI_IND_PRE_CERRADO
        INTO V_COCI_IND_PRE_CERRADO
        FROM GEN_CONTROL_CIERRE
       WHERE COCI_EMPR = V_CONF_EMPR
         AND COCI_SUC = V_CONF_SUC
         AND COCI_SISTEMA = (SELECT SIST_CODIGO
                               FROM GEN_SISTEMA
                              WHERE SIST_DESC_ABREV = 'STK')
         AND COCI_PERIODO = (SELECT CONF_PERIODO_ACT
                               FROM STK_CONFIGURACION
                              WHERE CONF_EMPR = P_EMPRESA)
         AND P_FECHA = (SELECT P_FECHA
                          FROM STK_CONFIGURACION, STK_PERIODO
                         WHERE CONF_PERIODO_ACT = PERI_CODIGO
                           AND CONF_EMPR = PERI_EMPR
                           AND P_FECHA BETWEEN PERI_FEC_INI AND PERI_FEC_FIN
                           AND CONF_EMPR = V_CONF_EMPR);
      IF V_COCI_IND_PRE_CERRADO = 'S' THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL PERIODO ACTUAL YA FUE PRE-CERRADO PARA EL SISTEMA DE STOCK!',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        RAISE_APPLICATION_ERROR(-20002,
                                'EL PERIODO ACTUAL YA FUE PRE-CERRADO PARA EL SISTEMA DE STOCK!');
      END IF;
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
  
  END PL_VALIDAR_HAB_MES_STK;

  PROCEDURE PL_VAL_DOC_TIMBRADO(I_NRO_DOC   IN NUMBER,
                                I_CLASE_DOC IN NUMBER,
                                I_FECHA     IN DATE,
                                O_TIMBRADO  OUT NUMBER,
                                I_EMPRESA   IN NUMBER) IS
    /*
    DESCRIPCION: ESTE PROCEDIMIENTO ALMACENADO SIRVE PARA REALIZAR EL CONTROL DE UN NRO DE DOCUMENTO, PARA ELLO RECIBE COMO PARAMETRO
                   I_NRO_DOC: NRO DE DOCUMENTO QUE SE VA A VALIDAR.
                   I_NRO_DOC: CLASE DEL NRO DE DOCUMENTO 1-FACTURA, 4-NOTAS DE CREDITOS ETC.
                   I_FECHA: FECHA DEL DOCUMENTO, SIRVE PARA SABER SI EL TIMBRADO SELECCIONADO ESTA VIGENTE.
                   O_TIMBRADO: PARAMETRO DE SALIDA, ES EL NRO DE TIMBRADO HABILITADO PARA I_NRO_DOC.
    */
    V_NRO_DESDE       NUMBER;
    V_NRO_HASTA       NUMBER;
    V_FECHA_DESDE     DATE;
    V_FECHA_HASTA     DATE;
    V_CTRL_NRO        NUMBER;
    V_CLASE_DOCUMENTO VARCHAR2(15);
  BEGIN
  
    BEGIN
      --RAISE_APPLICATION_ERROR(-20002, 'ERROR ACA ' || I_NRO_DOC || ' aca ' || I_FECHA ); 
    
      --RAISE_APPLICATION_ERROR(-20010,'I_CLASE_DOC: '||I_CLASE_DOC||';'||'I_FECHA: '||I_FECHA);
    
      --BUSCAMOS LA DEFINICION DEL TIMBRADO QUE CONTENGA AL NRO DE DOCUMENTO A CONTROLAR.
      SELECT TIMB_NUMERO,
             TIMB_DOCU_INIC,
             TIMB_DOCU_FIN,
             TIMB_FECH_INIC,
             TIMB_FECH_FIN
        INTO O_TIMBRADO,
             V_NRO_DESDE,
             V_NRO_HASTA,
             V_FECHA_DESDE,
             V_FECHA_HASTA
        FROM FIN_TIMBRADO T, FIN_CLASE_DOC C
       WHERE TIMB_CLASE_MOV = CLDO_CODIGO
         AND TIMB_EMPR = I_EMPRESA
         AND TIMB_EMPR = CLDO_EMPR
         AND I_NRO_DOC BETWEEN T.TIMB_DOCU_INIC AND T.TIMB_DOCU_FIN
         AND T.TIMB_CLASE_MOV = I_CLASE_DOC
         AND T.TIMB_CLASE_MOV = C.CLDO_CODIGO
         AND TRUNC(I_FECHA) BETWEEN T.TIMB_FECH_INIC AND T.TIMB_FECH_FIN;
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        --OBTENEMOS EL NOMBRE DE LA CLASE DE DOCUMENTO.
        SELECT CLDO_DESC
          INTO V_CLASE_DOCUMENTO
          FROM FIN_CLASE_DOC C
         WHERE CLDO_CODIGO = I_CLASE_DOC
           AND C.CLDO_EMPR = I_EMPRESA;
      
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'NO EXISTE TIMBRADO HABILITADO PARA EL NRO: ' ||
                                                   I_NRO_DOC ||
                                                   ' CLASE DOCUMENTO: ' ||
                                                   I_CLASE_DOC || '-' ||
                                                   V_CLASE_DOCUMENTO ||
                                                   CHR(13) ||
                                                   'VERIFIQUE LA HABILITACION DE NROS DE TIMBRADOS, COMPROBANDO RANGOS DE NROS HABLITADOS Y FECHA DE VIGENCIA!.',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      
        RAISE_APPLICATION_ERROR(-20002,
                                'NO EXISTE TIMBRADO HABILITADO PARA EL NRO: ' ||
                                I_NRO_DOC || ' CLASE DOCUMENTO: ' ||
                                I_CLASE_DOC || '-' || V_CLASE_DOCUMENTO ||
                                CHR(13) ||
                                'VERIFIQUE LA HABILITACION DE NROS DE TIMBRADOS, COMPROBANDO RANGOS DE NROS HABLITADOS Y FECHA DE VIGENCIA!.');
      WHEN TOO_MANY_ROWS THEN
        RAISE_APPLICATION_ERROR(-20002, 'REGISTRO DUPLICADO');
    END;
  
    --
    --VALIDAR FECHA DE VIGENCIA DEL TIMBRADO.
    --
    IF I_FECHA IS NULL THEN
      IF TRUNC(SYSDATE) NOT BETWEEN V_FECHA_DESDE AND V_FECHA_HASTA THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'A HOY, EL TIMBRADO ESTA VENCIDO',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        RAISE_APPLICATION_ERROR(-20002, 'A HOY, EL TIMBRADO ESTA VENCIDO');
      END IF;
    ELSE
      --IF TO_DATE(I_FECHA,'DD/MM/YYYY') NOT BETWEEN  TO_DATE(V_FECHA_DESDE,'DD/MM/YYYY') AND TO_DATE(V_FECHA_HASTA,'DD/MM/YYYY') THEN
      IF I_FECHA NOT BETWEEN V_FECHA_DESDE AND V_FECHA_HASTA THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL TIMBRADO ESTA VENCIDO!',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        RAISE_APPLICATION_ERROR(-20002, 'EL TIMBRADO ESTA VENCIDO!');
      END IF;
    END IF;
  
    ------------------------------------------------------------------------------------------------------------------------------------------------------
    --VALIDAR QUE NO SE HAYA UTILIZADO EL NRO DE DOCUMENTO.
    --UN NRO DE DOCUMENTO + NRO DE TIMBRADO SOLO SE PUEDE UTILIZAR 1(UNA) VEZ.
    --SE VERIFICA QUE NO SE UTILICEN NROS ANULADOS.
    ------------------------------------------------------------------------------------------------------------------------------------------------------
  
    IF I_CLASE_DOC = 1 THEN
      --FACTURAS
      SELECT MAX(DOC_NRO_DOC)
        INTO V_CTRL_NRO
        FROM (SELECT NVL(MAX(DOC_NRO_DOC), 0) DOC_NRO_DOC
                FROM FIN_DOCUMENTO F
               WHERE DOC_TIPO_MOV IN (9, 10)
                 AND F.DOC_TIMBRADO = O_TIMBRADO
                 AND DOC_NRO_DOC = I_NRO_DOC
                 AND DOC_EMPR = I_EMPRESA
              UNION
              
              SELECT NVL(MAX(ANUL_NRO_DOC), 0) ANUL_NRO_DOC
                FROM FIN_DOC_ANULADO
               WHERE ANUL_TIPO_MOV IN (9, 10)
                 AND ANUL_NRO_TIMBRADO = O_TIMBRADO
                 AND ANUL_NRO_DOC = I_NRO_DOC
                 AND ANUL_EMPR = I_EMPRESA);
    ELSIF I_CLASE_DOC = 2 THEN
      --RECIBOS
      SELECT MAX(DOC_NRO_DOC)
        INTO V_CTRL_NRO
        FROM (SELECT NVL(MAX(DOC_NRO_DOC), 0) DOC_NRO_DOC
                FROM FIN_DOCUMENTO F
               WHERE DOC_TIPO_MOV IN (6)
                 AND F.DOC_TIMBRADO = O_TIMBRADO
                 AND DOC_NRO_DOC = I_NRO_DOC
                 AND DOC_EMPR = I_EMPRESA
              UNION
              
              SELECT NVL(MAX(ANUL_NRO_DOC), 0) ANUL_NRO_DOC
                FROM FIN_DOC_ANULADO
               WHERE ANUL_TIPO_MOV = 6
                 AND ANUL_NRO_TIMBRADO = O_TIMBRADO
                 AND ANUL_NRO_DOC = I_NRO_DOC
                 AND ANUL_EMPR = I_EMPRESA);
    
    ELSIF I_CLASE_DOC = 4 THEN
      --NOTAS DE CREDITOS.
      SELECT MAX(DOC_NRO_DOC)
        INTO V_CTRL_NRO
        FROM (SELECT NVL(MAX(DOC_NRO_DOC), 0) DOC_NRO_DOC
                FROM FIN_DOCUMENTO F
               WHERE DOC_TIPO_MOV IN (16)
                 AND F.DOC_TIMBRADO = O_TIMBRADO
                 AND DOC_NRO_DOC = I_NRO_DOC
                 AND DOC_EMPR = I_EMPRESA
              UNION
              
              SELECT NVL(MAX(ANUL_NRO_DOC), 0) ANUL_NRO_DOC
                FROM FIN_DOC_ANULADO
               WHERE ANUL_TIPO_MOV IN (16)
                 AND ANUL_NRO_TIMBRADO = O_TIMBRADO
                 AND ANUL_NRO_DOC = I_NRO_DOC
                 AND ANUL_EMPR = I_EMPRESA
              UNION --LAS NC PASAN POR APROBACION YA TAMBIEN Y PARA QUE  NO GENERE CONFLICTOS EN LA HORA DE LA APROBACION, PORQUE HAY CASOS QUE SE COMPARTEN LOS PUNTOS, SE VALIDA TAMBIEN EN LA TEMPRAL. 09/05/2020
              SELECT NVL(MAX(A.DOC_NRO_DOC), 0) NRO_NC_TMP
                FROM FIN_DOCUMENTO_COMI015_TEMP A
               WHERE A.DOC_TIPO_MOV IN (16)
                 AND A.DOC_TIMBRADO = O_TIMBRADO
                 AND A.DOC_NRO_DOC = I_NRO_DOC
                 AND A.DOC_EMPR = I_EMPRESA
                 AND A.COMI005_ESTADO IS NULL);
    
    ELSIF I_CLASE_DOC = 6 THEN
      --RETENCIONES.
      SELECT MAX(DOC_NRO_DOC)
        INTO V_CTRL_NRO
        FROM (SELECT NVL(MAX(DOC_NRO_DOC), 0) DOC_NRO_DOC
                FROM FIN_DOCUMENTO F
               WHERE DOC_TIPO_MOV IN (61, 62, 64)
                 AND F.DOC_TIMBRADO = O_TIMBRADO
                 AND DOC_NRO_DOC = I_NRO_DOC
                 AND DOC_EMPR = I_EMPRESA
              UNION
              
              SELECT NVL(MAX(ANUL_NRO_DOC), 0) ANUL_NRO_DOC
                FROM FIN_DOC_ANULADO
               WHERE ANUL_TIPO_MOV IN (61, 62, 64)
                 AND ANUL_NRO_TIMBRADO = O_TIMBRADO
                 AND ANUL_NRO_DOC = I_NRO_DOC
                 AND ANUL_EMPR = I_EMPRESA);
    
    ELSIF I_CLASE_DOC = 7 THEN
      --REMISIONES
      SELECT MAX(REM_NRO)
        INTO V_CTRL_NRO
        FROM (SELECT NVL(MAX(DOCU_NRO_DOC), 0) REM_NRO
                FROM STK_DOCUMENTO
               WHERE DOCU_CODIGO_OPER = 12 --TRAN_SAL
                 AND DOCU_TIMBRADO = O_TIMBRADO
                 AND DOCU_NRO_DOC = I_NRO_DOC
                 AND DOCU_EMPR = I_EMPRESA
              UNION
              SELECT NVL(MAX(ADOCU_NRO_DOC), 0)
                FROM STK_AUD_DOCUMENTO
               WHERE ADOCU_TIMBRADO = O_TIMBRADO
                 AND ADOCU_NRO_DOC = I_NRO_DOC
                 AND ADOCU_EMPR = I_EMPRESA);
    
    ELSIF I_CLASE_DOC = 8 THEN
      --AUTOFACTURAS
      --IF I_EMPRESA != 3 THEN
      --VALIDACION TEMPORAL
      SELECT MAX(DOC_NRO_DOC)
        INTO V_CTRL_NRO
        FROM (SELECT NVL(MAX(DOC_NRO_DOC), 0) DOC_NRO_DOC
                FROM FIN_DOCUMENTO F
               WHERE DOC_TIPO_MOV IN (7)
                 AND F.DOC_TIMBRADO = O_TIMBRADO
                 AND DOC_NRO_DOC = I_NRO_DOC
                 AND DOC_EMPR = I_EMPRESA
              UNION
              SELECT NVL(MAX(ANUL_NRO_DOC), 0) ANUL_NRO_DOC
                FROM FIN_DOC_ANULADO
               WHERE ANUL_TIPO_MOV = 7
                 AND ANUL_NRO_TIMBRADO = O_TIMBRADO
                 AND ANUL_NRO_DOC = I_NRO_DOC
                 AND ANUL_EMPR = I_EMPRESA
              UNION --LAS AUTOFACTURAS PASAN POR APROBACION YA TAMBIEN Y PARA QUE  NO GENERE CONFLICTOS EN LA HORA DE LA APROBACION, PORQUE HAY CASOS QUE SE COMPARTEN LOS PUNTOS, SE VALIDA TAMBIEN EN LA TEMPRAL. 09/05/2020
              SELECT NVL(MAX(A.DOC_NRO_DOC), 0) NRO_AUT_TMP
                FROM FIN_DOCUMENTO_COMI015_TEMP A
               WHERE A.DOC_TIPO_MOV IN (7)
                 AND A.DOC_TIMBRADO = O_TIMBRADO
                 AND A.DOC_NRO_DOC = I_NRO_DOC
                 AND A.DOC_EMPR = I_EMPRESA
                 AND A.COMI005_ESTADO IS NULL);
      --END IF;
    END IF;
  
    IF V_CTRL_NRO <> 0 THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL NRO DE DOCUMENTO ' ||
                                                 V_CTRL_NRO ||
                                                 ' CON TIMBRADO N? ' ||
                                                 O_TIMBRADO ||
                                                 ' YA EXISTE O HA SIDO ANULADO!',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002,
                              'EL NRO DE DOCUMENTO ' || V_CTRL_NRO ||
                              ' CON TIMBRADO N? ' || O_TIMBRADO ||
                              ' YA EXISTE Y/O HA SIDO ANULADO O ESTA PENDIENTE DE APROBACION!');
    END IF;
  END PL_VAL_DOC_TIMBRADO;

  PROCEDURE PP_VALIDAR_DESCUENTO(I_EMPRESA     IN NUMBER,
                                 I_DOC_CLI     IN NUMBER,
                                 I_ART         IN NUMBER,
                                 I_DOC_FEC_DOC IN DATE,
                                 I_CLAVE_DESC  IN NUMBER) AS
  
    V_COUNT NUMBER;
  BEGIN
    SELECT SUM(CANTIDAD)
      INTO V_COUNT
      FROM (SELECT P.DTOD_CANT - NVL(SUM(FAC.DET_CANT), 0) CANTIDAD
              FROM FAC_PRECIO_DTO_HOLD H,
                   FAC_PRECIO_DTO_DET  P,
                   FIN_CLIENTE         C,
                   FAC_DOCUMENTO_DET   FAC
             WHERE H.DTO_CLAVE = P.DTOD_CLAVE
               AND H.DTO_EMPR = P.DTOD_EMPR
               AND C.CLI_COD_FICHA_HOLDING = H.DTO_HOLDING
               AND C.CLI_EMPR = H.DTO_EMPR
               AND H.DTO_STATUS = 'A'
               AND P.DTOD_CLAVE = FAC.DET_DTO_CONT(+)
               AND P.DTOD_ART = FAC.DET_ART(+)
               AND P.DTOD_EMPR = FAC.DET_EMPR(+)
               AND I_DOC_FEC_DOC BETWEEN H.DTO_FECHA AND H.DTO_FEC_VTO
               AND P.DTOD_ART = I_ART
               AND C.CLI_CODIGO = I_DOC_CLI
               AND H.DTO_EMPR = I_EMPRESA
             GROUP BY P.DTOD_CLAVE, P.DTOD_ART, P.DTOD_CANT);
  
    IF V_COUNT > 0 AND I_CLAVE_DESC IS NULL THEN
    
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL HOLDING DEL CLIENTE TIENE CONTRATO DE DESCUENTO, FAVOR USARLO.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    
      RAISE_APPLICATION_ERROR(-20010,
                              'EL HOLDING DEL CLIENTE TIENE CONTRATO DE DESCUENTO, FAVOR USARLO.');
    
    END IF;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
  END;

  PROCEDURE PP_TRAER_DESC_CLI(P_DOC_CLI                     IN NUMBER,
                              P_EMPRESA                     IN NUMBER,
                              P_DOC_CLI_TEL                 OUT VARCHAR2,
                              P_DOC_CLI_RUC                 OUT VARCHAR2,
                              P_CLI_PERS_REPRESENTANTE      OUT VARCHAR2,
                              P_CLI_DOC_IDENT_REPRESENTANTE OUT VARCHAR2,
                              P_CLI_LOCALIDAD               OUT VARCHAR2,
                              P_W_CLI_MAX_DIAS_ATRASO       OUT NUMBER,
                              P_DIAS_ATRASO                 OUT NUMBER,
                              P_CLI_NOM                     OUT VARCHAR2,
                              P_CLI_DIR                     OUT VARCHAR2,
                              P_DOC_CANAL                   OUT NUMBER,
                              P_CLI_IND_MOD_CANAL           OUT VARCHAR2,
                              P_OCASIONAL                   OUT NUMBER,
                              P_PLAZO_PAGO                  OUT NUMBER,
                              P_PORC_DTO                    OUT NUMBER,
                              P_RECARGO                     OUT NUMBER) IS
    --*
    --V_BLOQUEADO FIN_CLIENTE.CLI_BLOQ_LIM_CR%TYPE;
    --V_ESTADO    FIN_CLIENTE.CLI_EST_CLI%TYPE;
    --V_MAX_DIAS_ATRASO   NUMBER;
    V_CLI_NOM           VARCHAR2(100);
    V_CLI_DIR           VARCHAR2(270);
    V_CLI_CANAL_BETA    NUMBER;
    V_CLI_IND_MOD_CANAL VARCHAR2(1);
    CLIENTE_SIN_CANAL EXCEPTION;
    V_CLI_SUCURSAL NUMBER;
    V_CLI_RAMO     NUMBER;
    --*
  BEGIN
  
    SELECT CLI_NOM,
           CLI_DIR,
           SUBSTR(CLI_TEL, 1, 15),
           CLI_RUC,
           CLI_PERS_REPRESENTANTE,
           CLI_DOC_IDENT_REPRESENTANTE,
           CLI_LOCALIDAD,
           CLI_MAX_DIAS_ATRASO,
           CLI_CANAL_BETA,
           CLI_IND_MOD_CANAL,
           CLI_SUCURSAL,
           CLI_RAMO
      INTO V_CLI_NOM,
           V_CLI_DIR,
           P_DOC_CLI_TEL,
           P_DOC_CLI_RUC,
           P_CLI_PERS_REPRESENTANTE,
           P_CLI_DOC_IDENT_REPRESENTANTE,
           P_CLI_LOCALIDAD,
           P_W_CLI_MAX_DIAS_ATRASO,
           V_CLI_CANAL_BETA,
           V_CLI_IND_MOD_CANAL,
           V_CLI_SUCURSAL,
           V_CLI_RAMO
      FROM FIN_CLIENTE
     WHERE CLI_CODIGO = P_DOC_CLI
       AND CLI_EMPR = P_EMPRESA;
  
    P_DIAS_ATRASO := P_W_CLI_MAX_DIAS_ATRASO;
  
    P_CLI_NOM := V_CLI_NOM;
    P_CLI_DIR := V_CLI_DIR;
  
    --*
    SELECT COUNT(*) --DECODE(COUNT(*), 0, NULL, 1)
      INTO P_OCASIONAL
      FROM FIN_CLIENTE T
     WHERE UPPER(T.CLI_NOM) LIKE '%OCASIONAL%'
       AND T.CLI_CODIGO = P_DOC_CLI
       AND T.CLI_EMPR = P_EMPRESA;
  
    IF V_CLI_CANAL_BETA IS NULL THEN
      IF P_EMPRESA = 1 THEN
        RAISE CLIENTE_SIN_CANAL;
      END IF;
      P_DOC_CANAL         := NULL;
      P_CLI_IND_MOD_CANAL := 'S';
    ELSE
      P_DOC_CANAL         := V_CLI_CANAL_BETA;
      P_CLI_IND_MOD_CANAL := NVL(V_CLI_IND_MOD_CANAL, 'N');
    END IF;
    --*
  
    BEGIN
      --PP_BUSCAR_DESCUENTO_PLAZOPAGO;
      SELECT HOL_PORC_DESCUENTO, NVL(HOL_DIAS_PLAZO_PAGO, 0), HOL_RECARGO
        INTO P_PORC_DTO, P_PLAZO_PAGO, P_RECARGO
        FROM FIN_FICHA_HOLDING F, FIN_CLIENTE C
       WHERE F.HOL_CODIGO = C.CLI_COD_FICHA_HOLDING
         AND C.CLI_CODIGO = P_DOC_CLI
         AND CLI_EMPR = HOL_EMPR
         AND CLI_EMPR = P_EMPRESA;
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        P_PORC_DTO   := 0;
        P_PLAZO_PAGO := 0;
        P_RECARGO    := 0;
    END;
  
    IF V_CLI_RAMO = 3 THEN
      IF V_CLI_SUCURSAL = 1 THEN
        IF TRUNC(SYSDATE) BETWEEN TRUNC(SYSDATE, 'MM') AND
           TRUNC(SYSDATE, 'MM') + 23 THEN
          --- SUMA 24 PARA QUE SEA EL 25 DE CADA MES
        
          P_PLAZO_PAGO := ROUND(ADD_MONTHS(TRUNC(SYSDATE, 'MM'), 1) -
                                TRUNC(SYSDATE));
        ELSE
          P_PLAZO_PAGO := ROUND(ADD_MONTHS(TRUNC(SYSDATE, 'MM'), 2) -
                                TRUNC(SYSDATE));
        END IF;
      END IF;
    
      IF V_CLI_SUCURSAL = 4 THEN
        IF TRUNC(SYSDATE) BETWEEN TRUNC(SYSDATE, 'MM') AND
           TRUNC(SYSDATE, 'MM') + 19 THEN
          --** +24 EL PLAZO SUMADO EL DIA ACTUAL PARA QUE DE VENCIMIENTO 25
          P_PLAZO_PAGO := ROUND((TRUNC(SYSDATE, 'MM') + 24) -
                                TRUNC(SYSDATE));
        ELSE
          P_PLAZO_PAGO := ROUND(ADD_MONTHS((TRUNC(SYSDATE, 'MM') + 24), 1) -
                                TRUNC(SYSDATE));
        END IF;
      END IF;
    
    ELSIF V_CLI_RAMO = 1 THEN
      IF V_CLI_SUCURSAL = 4 THEN
        IF TRUNC(SYSDATE) BETWEEN TRUNC(SYSDATE, 'MM') AND
           TRUNC(SYSDATE, 'MM') + 1 THEN
          --** VENCIMIENTO EL 7
          /* P_PLAZO_PAGO := ROUND(ADD_MONTHS(TRUNC(SYSDATE, 'MM') + 6, 1) -
          TRUNC(SYSDATE));*/
          P_PLAZO_PAGO := ROUND(TO_DATE(TO_CHAR('07/' ||
                                                TO_CHAR(SYSDATE, 'MM/YYYY'))) -
                                TRUNC(SYSDATE, 'MM'));
        ELSE
          /* P_PLAZO_PAGO := ROUND(ADD_MONTHS(TRUNC(SYSDATE, 'MM') + 6, 2) -
          TRUNC(SYSDATE));*/
          P_PLAZO_PAGO := ROUND(TO_DATE(TO_CHAR('07/' ||
                                                TO_CHAR(ADD_MONTHS(SYSDATE,
                                                                   1),
                                                        'MM/YYYY'))) -
                                TRUNC(SYSDATE, 'MM'));
        
        END IF;
      END IF;
    
    ELSIF V_CLI_RAMO = 6 THEN
      /*Todos los clientes que tengan asignado Ramo 6, el Vto de la factura debe ser automatico el 5 de cada mes*/
      IF TRUNC(SYSDATE) BETWEEN TRUNC(SYSDATE, 'MM') AND
         TRUNC(SYSDATE, 'MM') + 25 THEN
        --** VENCIMIENTO EL 5
        P_PLAZO_PAGO := ROUND(ADD_MONTHS(TRUNC(SYSDATE, 'MM') + 4, 1) -
                              TRUNC(SYSDATE));
      
        /*  ROUND(ADD_MONTHS(TRUNC(SYSDATE, 'MM') + 5, 1) -
        TRUNC(SYSDATE));*/
      ELSE
        P_PLAZO_PAGO := ROUND(ADD_MONTHS(TRUNC(SYSDATE, 'MM') + 4, 2) -
                              TRUNC(SYSDATE));
        /*ROUND(ADD_MONTHS(TRUNC(SYSDATE, 'MM') + 5, 2) -
        TRUNC(SYSDATE));*/
      END IF;
    
    END IF;
  
    IF FP_CLI_ES_EMPL(I_CLI => P_DOC_CLI, I_EMPRESA => P_EMPRESA) THEN
      IF FP_RETORNA_CANT_DIA_HABIL(I_FECHA_INI => TRUNC(SYSDATE),
                                   I_FECHA_FIN => TRUNC(LAST_DAY(SYSDATE)),
                                   I_EMPRESA   => P_EMPRESA) > 2 THEN
        P_PLAZO_PAGO := ROUND(LAST_DAY(SYSDATE) - TRUNC(SYSDATE)) - 1;
      ELSE
        P_PLAZO_PAGO := ROUND(ADD_MONTHS(LAST_DAY(SYSDATE), 1) -
                              TRUNC(SYSDATE)) - 1;
      END IF;
    END IF;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'CLIENTE INEXISTENTE.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002, 'CLIENTE INEXISTENTE.');
    
    WHEN CLIENTE_SIN_CANAL THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'CLIENTE SIN CANAL. ASIGNE EL CANAL AL CLIENTE EN EL MANTENIMIENTO DE CLIENTES!.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    
      RAISE_APPLICATION_ERROR(-20002,
                              'CLIENTE SIN CANAL. ASIGNE EL CANAL AL CLIENTE EN EL MANTENIMIENTO DE CLIENTES!.');
    
  END PP_TRAER_DESC_CLI;

  PROCEDURE PP_VALIDAR_LISTA_NEGRA(P_DOC_CLI     IN NUMBER,
                                   P_EMPRESA     IN NUMBER,
                                   P_MARCA       OUT VARCHAR2,
                                   P_LISTA_NEGRA OUT VARCHAR2) IS
    --V_CLI_NOM VARCHAR(100);
    --V_CLI_RUC VARCHAR(20);
    --V_CLI_RUC_DV VARCHAR(20);
    --V_CLI_CI     VARCHAR(20);
    V_MARCA VARCHAR(20);
  BEGIN
  
    V_MARCA := FAC_FUN_HOL_LISTA_NEGRA(P_DOC_CLI, P_EMPRESA);
  
    IF V_MARCA = 'S' THEN
      P_LISTA_NEGRA := 'S';
    ELSE
      P_LISTA_NEGRA := 'N';
    END IF;
  
    P_MARCA := V_MARCA;
  
  END PP_VALIDAR_LISTA_NEGRA;

  PROCEDURE PP_VALIDAR_CLIENTE(P_EMPRESA      IN NUMBER,
                               P_DOC_CLI      IN NUMBER,
                               P_TIPO_FACTURA IN NUMBER,
                               P_DOC_FEC_DOC  IN DATE,
                               P_LIM_FAC_ADIC OUT VARCHAR2) AS
    CANT_FAC_PERM    NUMBER;
    CANT_FAC_PEND    NUMBER;
    COMP             VARCHAR2(2);
    V_CODIGO_HOLDING NUMBER;
  BEGIN
    IF P_DOC_CLI IS NOT NULL THEN
    
      IF P_TIPO_FACTURA = 2 THEN
        FAC_CANT_FACT_PENDIENTE(P_DOC_CLI,
                                CANT_FAC_PERM,
                                CANT_FAC_PEND,
                                P_EMPRESA);
        IF CANT_FAC_PERM <= CANT_FAC_PEND THEN
          BEGIN
            SELECT 'X'
              INTO COMP
              FROM FIN_AUTORIZ_ESPEC
             WHERE AUES_CLI = P_DOC_CLI
               AND AUES_FEC_AUTORIZ =
                   TO_DATE(TO_CHAR(P_DOC_FEC_DOC, 'DD/MM/YYYY'),
                           'DD/MM/YYYY')
               AND AUES_UTILIZADA = 'N'
               AND AUES_FAC_ADI = 'S'
               AND NVL(AUES_FAC_CONT_LN, 'N') = 'N'
               AND AUES_EMPR = P_EMPRESA;
            P_LIM_FAC_ADIC := 'S';
            /*RAISE_APPLICATION_ERROR(-20002,
            'EL HOLDING ALCANZO SU LIMITE DE FACTURAS PENDIENTES PERO
                          PODRA FACTURAR MEDIANTE QUE SE OTORGO UN PERMISO ESPECIAL');*/
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL HOLDING ALCANZO EL LIMITE DE FACTURAS PENDIENTES PERMITIDAS
                                                                      CANTIDAD PERMITIDA: ' ||
                                                         CANT_FAC_PERM || '
                                                                      CANTIDAD PENDIENTES: ' ||
                                                         CANT_FAC_PEND,
                                   P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
              RAISE_APPLICATION_ERROR(-20002,
                                      'EL HOLDING ALCANZO EL LIMITE DE FACTURAS PENDIENTES PERMITIDAS
                                              CANTIDAD PERMITIDA: ' ||
                                      CANT_FAC_PERM || '
                                              CANTIDAD PENDIENTES: ' ||
                                      CANT_FAC_PEND);
            
            WHEN TOO_MANY_ROWS THEN
              APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL CLIENTE TIENE MAS DE UNA AUTORIZACION ESPECIAL PARA LA FECHA: ' ||
                                                         P_DOC_FEC_DOC ||
                                                         'DEBE ANULAR UNO',
                                   P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
              RAISE_APPLICATION_ERROR(-20002,
                                      'EL CLIENTE TIENE MAS DE UNA AUTORIZACION ESPECIAL PARA LA FECHA: ' ||
                                      P_DOC_FEC_DOC || ' DEBE ANULAR UNO');
          END;
        END IF;
      END IF;
      BEGIN
        SELECT CLI_COD_FICHA_HOLDING
          INTO V_CODIGO_HOLDING
          FROM FIN_CLIENTE T
         WHERE T.CLI_EMPR = P_EMPRESA
           AND T.CLI_CODIGO = P_DOC_CLI;
      
      END;
      IF V_CODIGO_HOLDING IN (29225, 12609) THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'NO SE PUEDE FACTURAR A HILAGRO, PARA DAR DE BAJA ARTICULOS SE HACE POR EL PROGRAMA 1-2-50.',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        RAISE_APPLICATION_ERROR(-20002,
                                'NO SE PUEDE FACTURAR A HILAGRO, PARA DAR DE BAJA ARTICULOS SE HACE POR EL PROGRAMA 1-2-50.');
      END IF;
    END IF;
  
    --PP_LISTA_PRECIOS;
  END PP_VALIDAR_CLIENTE;

  PROCEDURE PP_CONTROL_CLIENTE(P_TIPO_FACTURA              IN NUMBER,
                               P_DOC_CLI                   IN NUMBER,
                               P_EMPRESA                   IN NUMBER,
                               P_DOC_FEC_DOC               IN DATE,
                               P_W_IMP_LIM_CR_EMPR         IN OUT NUMBER,
                               P_W_IMP_LIM_CR_DISP_GRUPO   IN OUT NUMBER,
                               P_W_IMP_LIM_CR_DISP_EMPR    IN OUT NUMBER,
                               P_W_IMP_CHEQ_DIFERIDO       IN OUT NUMBER,
                               P_W_IMP_CHEQ_RECHAZADO      IN OUT NUMBER,
                               P_CATEG                     OUT NUMBER,
                               P_CONF_CATEG_CLI_ESPORADICO IN NUMBER) IS
  
    V_EMPLEADO NUMBER;
  BEGIN
  
    IF P_TIPO_FACTURA = 2 --SI ES FACTURA CREDITO
       AND P_DOC_CLI IS NULL THEN
      RAISE_APPLICATION_ERROR(-20002,
                              'CODIGO DE CLIENTE NO PUEDE SER NULO PARA FACTURA CREDITO!');
    END IF;
  
    IF P_DOC_CLI IS NULL THEN
      --SET_ITEM_PROPERTY('BDOC_ENCA.CLI_NOM',ENABLED,PROPERTY_TRUE);
      --SET_ITEM_PROPERTY('BDOC_ENCA.CLI_NOM',NAVIGABLE,PROPERTY_TRUE);
      P_CATEG := P_CONF_CATEG_CLI_ESPORADICO;
    ELSE
    
      --PP_VALIDAR_CLIENTE;
      -- LLAMAR A PROCEDIMIENTO ALMACENADO QUE CALCULA EL LIMITE DE CREDITO.
    
      IF P_EMPRESA = 1 THEN
        SELECT COUNT(EMPL_LEGAJO)
          INTO V_EMPLEADO
          FROM PER_EMPLEADO A
         WHERE A.EMPL_COD_CLIENTE = P_DOC_CLI
           AND A.EMPL_EMPRESA = 1;
      ELSE
        V_EMPLEADO := 0;
      END IF;
    
      IF V_EMPLEADO = 1 THEN
      
        FIN_LIM_CREDITO_EMPLEADO(LAST_DAY(P_DOC_FEC_DOC),
                                 P_DOC_CLI,
                                 P_W_IMP_LIM_CR_EMPR,
                                 P_EMPRESA,
                                 P_W_IMP_LIM_CR_DISP_GRUPO,
                                 P_W_IMP_LIM_CR_DISP_EMPR,
                                 P_W_IMP_CHEQ_DIFERIDO,
                                 P_W_IMP_CHEQ_RECHAZADO);
      
      ELSE
      
        FIN_LIM_CREDITO(P_DOC_FEC_DOC,
                        P_DOC_CLI,
                        P_W_IMP_LIM_CR_EMPR,
                        P_EMPRESA,
                        P_W_IMP_LIM_CR_DISP_GRUPO,
                        P_W_IMP_LIM_CR_DISP_EMPR,
                        P_W_IMP_CHEQ_DIFERIDO,
                        P_W_IMP_CHEQ_RECHAZADO);
      
      END IF;
    
    END IF;
  
  END PP_CONTROL_CLIENTE;

  PROCEDURE PP_CLI_LISTA_PRECIO(I_EMPRESA               IN NUMBER,
                                I_DOC_CLI               IN NUMBER,
                                I_DOC_MON               IN NUMBER,
                                I_SUCURSAL              IN NUMBER,
                                O_LIPR_NRO_LISTA_PRECIO OUT NUMBER) AS
  BEGIN
  
    IF I_DOC_MON = 1 THEN
      SELECT T.COD_PRECIO_BASE
        INTO O_LIPR_NRO_LISTA_PRECIO
        FROM CAST_V_CLIENTE_LISTA_PRECIO T
       WHERE T.COD_CLIENTE = I_DOC_CLI;
    ELSE
      SELECT CASE
               WHEN C.CLI_REPOSITOR IS NOT NULL THEN
                NVL(CON_REPOSITOR, SIN_REPOSITOR)
               ELSE
                SIN_REPOSITOR
             END VEND_LIST_PREC
        INTO O_LIPR_NRO_LISTA_PRECIO
        FROM FIN_CLIENTE C,
             (SELECT T.LIPE_EMPR,
                     T.LIPE_MON,
                     T.LIPE_SUC,
                     T.LIPE_CANAL,
                     MAX(DECODE(T.LIPE_REPOSITOR,
                                'N',
                                T.LIPE_NRO_LISTA_PRECIO)) SIN_REPOSITOR,
                     MAX(DECODE(T.LIPE_REPOSITOR,
                                'S',
                                T.LIPE_NRO_LISTA_PRECIO)) CON_REPOSITOR
                FROM FAC_LISTA_PRECIO T
               WHERE T.LIPE_EMPR = I_EMPRESA
               GROUP BY T.LIPE_EMPR, T.LIPE_MON, T.LIPE_SUC, T.LIPE_CANAL) LP,
             FAC_VENDEDOR_V V
       WHERE C.CLI_CANAL_BETA = LP.LIPE_CANAL
         AND C.CLI_SUCURSAL = LP.LIPE_SUC
         AND C.CLI_EMPR = LP.LIPE_EMPR
         AND C.CLI_VENDEDOR = V.VEND_LEGAJO
         AND C.CLI_EMPR = V.VEND_EMPR
            --AND V.VENDEDOR_NOMBRE NOT LIKE '%H-PLUS%'
         AND LP.LIPE_MON = I_DOC_MON
         AND C.CLI_CODIGO = I_DOC_CLI
         AND C.CLI_EMPR = I_EMPRESA;
    END IF;
  
  END PP_CLI_LISTA_PRECIO;

  PROCEDURE PP_LISTA_PRECIOS(P_EMPRESA                   IN NUMBER,
                             P_DOC_CLI                   IN NUMBER,
                             P_LIPR_NRO_LISTA_PRECIO     OUT NUMBER,
                             P_PED_AUTO                  OUT VARCHAR2,
                             P_TIPO_FACTURA              IN NUMBER,
                             P_DOC_FEC_DOC               IN DATE,
                             P_W_IMP_LIM_CR_EMPR         IN OUT NUMBER,
                             P_W_IMP_LIM_CR_DISP_GRUPO   IN OUT NUMBER,
                             P_W_IMP_LIM_CR_DISP_EMPR    IN OUT NUMBER,
                             P_W_IMP_CHEQ_DIFERIDO       IN OUT NUMBER,
                             P_W_IMP_CHEQ_RECHAZADO      IN OUT NUMBER,
                             P_CATEG                     OUT NUMBER,
                             P_CONF_CATEG_CLI_ESPORADICO IN NUMBER,
                             P_VENDEDOR                  IN NUMBER,
                             I_DOC_MON                   IN NUMBER) IS
    V_NRO_LISTA  NUMBER;
    V_DEFINICION VARCHAR2(5);
    V_ZONA       VARCHAR2(32767);
    -- V_EMPLEADO NUMBER;
  BEGIN
  
    PP_CONTROL_CLIENTE(P_TIPO_FACTURA              => P_TIPO_FACTURA,
                       P_DOC_CLI                   => P_DOC_CLI,
                       P_EMPRESA                   => P_EMPRESA,
                       P_DOC_FEC_DOC               => P_DOC_FEC_DOC,
                       P_W_IMP_LIM_CR_EMPR         => P_W_IMP_LIM_CR_EMPR,
                       P_W_IMP_LIM_CR_DISP_GRUPO   => P_W_IMP_LIM_CR_DISP_GRUPO,
                       P_W_IMP_LIM_CR_DISP_EMPR    => P_W_IMP_LIM_CR_DISP_EMPR,
                       P_W_IMP_CHEQ_DIFERIDO       => P_W_IMP_CHEQ_DIFERIDO,
                       P_W_IMP_CHEQ_RECHAZADO      => P_W_IMP_CHEQ_RECHAZADO,
                       P_CATEG                     => P_CATEG,
                       P_CONF_CATEG_CLI_ESPORADICO => P_CONF_CATEG_CLI_ESPORADICO);
  
    ------------lv.. al crear cuota  el control de cliente lo haremos a la hora de crear la cuota
    ---====================BUSQUEDA DE LISTA DE PRECIO YA SEA DE CLIENTE O ZONA ==============------
  
    /*BEGIN
      SELECT VEND_LIST_PREC
        INTO V_NRO_LISTA
        FROM FAC_VENDEDOR
       WHERE VEND_LEGAJO = P_VENDEDOR
         AND VEND_EMPR = P_EMPRESA;
      P_LIPR_NRO_LISTA_PRECIO := V_NRO_LISTA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_NRO_LISTA := NULL;
    END;*/
  
    /*IF V_NRO_LISTA IS NULL THEN
      DECLARE
      BEGIN
        SELECT FIN_CLIENTE.CLI_REC_LPREC LISTA_CLI_O_ZONA,
               DECODE(FIN_CLIENTE.CLI_REC_LPREC,
                      'C',
                      F.HOL_NRO_LISTA_PRECIO,
                      'Z',
                      FAC_ZONA.ZONA_LPRECIO,
                      NULL) LISTA_PRECIO,
               FAC_ZONA.ZONA_CODIGO || '-  ' || FAC_ZONA.ZONA_DESC DESCRIPCION_ZONA
          INTO V_DEFINICION, V_NRO_LISTA, V_ZONA
          FROM FIN_CLIENTE,
               FAC_ZONA,
               FIN_FICHA_HOLDING F,
               FAC_LISTA_PRECIO  C,
               FAC_LISTA_PRECIO  Z
         WHERE FAC_ZONA.ZONA_CODIGO = FIN_CLIENTE.CLI_ZONA
           AND FIN_CLIENTE.CLI_COD_FICHA_HOLDING = F.HOL_CODIGO
           AND FIN_CLIENTE.CLI_CODIGO = P_DOC_CLI
           AND F.HOL_NRO_LISTA_PRECIO = C.LIPE_NRO_LISTA_PRECIO(+)
           AND FAC_ZONA.ZONA_LPRECIO = Z.LIPE_NRO_LISTA_PRECIO(+)
           AND CLI_EMPR = ZONA_EMPR
           AND ZONA_EMPR = HOL_EMPR
           AND ZONA_EMPR = Z.LIPE_EMPR(+)
           AND HOL_EMPR = C.LIPE_EMPR(+)
           AND CLI_EMPR = P_EMPRESA;
    
        P_LIPR_NRO_LISTA_PRECIO := V_NRO_LISTA;
    
        IF V_NRO_LISTA IS NULL THEN
          IF V_DEFINICION = 'C' THEN
            APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'FALTA SELECCIONAR UNA LISTA DE PRECIOS EN FINM002',
                                 P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
            RAISE_APPLICATION_ERROR(-20002,
                                    'FALTA SELECCIONAR UNA LISTA DE PRECIOS EN FINM002');
          ELSIF V_DEFINICION = 'Z' THEN
            APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'FALTA SELECCIONAR UNA LISTA DE PRECIOS EN FACM003, ZONA= ' ||
                                                       V_ZONA,
                                 P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
            RAISE_APPLICATION_ERROR(-20002,
                                    'FALTA SELECCIONAR UNA LISTA DE PRECIOS EN FACM003, ZONA= ' ||
                                    V_ZONA);
          END IF;
        END IF;
    
    
    
      EXCEPTION
        WHEN OTHERS THEN
          IF V_DEFINICION = 'C' THEN
            APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'FALTA SELECCIONAR UNA LISTA DE PRECIOS EN FINM002',
                                 P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
            RAISE_APPLICATION_ERROR(-20002,
                                    'FALTA SELECCIONAR UNA LISTA DE PRECIOS EN FINM002');
          ELSIF V_DEFINICION = 'Z' THEN
            APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'FALTA SELECCIONAR UNA LISTA DE PRECIOS EN FACM003',
                                 P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
            RAISE_APPLICATION_ERROR(-20002,
                                    'FALTA SELECCIONAR UNA LISTA DE PRECIOS EN FACM003');
          ELSE
            APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'LAS OPCIONES PARA LISTA DE PRECIOS SON POR CLIENTE O POR ZONA, VERIFIQUE FACM015 O FINM002 O FACM003 ',
                                 P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
            RAISE_APPLICATION_ERROR(-20002,
                                    'LAS OPCIONES PARA LISTA DE PRECIOS SON POR CLIENTE O POR ZONA, VERIFIQUE FACM015 O FINM002 O FACM003 ');
          END IF;
      END;
    END IF;*/
  
    SELECT T.COD_PRECIO_SUGERIDO
      INTO P_LIPR_NRO_LISTA_PRECIO
      FROM CAST_V_CLIENTE_LISTA_PRECIO T
     WHERE T.COD_CLIENTE = P_DOC_CLI;
    --I_DOC_MON
    ---==========================================================================================================================================
    DECLARE
      V_CLI_REP NUMBER;
    BEGIN
      SELECT COUNT(*)
        INTO V_CLI_REP
        FROM FIN_CLIENTE
       WHERE (CLI_DIADOM IS NOT NULL OR CLI_DIALUN IS NOT NULL OR
             CLI_DIAMAR IS NOT NULL OR CLI_DIAMIE IS NOT NULL OR
             CLI_DIAJUE IS NOT NULL OR CLI_DIAVIE IS NOT NULL OR
             CLI_DIASAB IS NOT NULL)
         AND CLI_PEDIDO_REPETITIVO IS NULL
         AND CLI_CODIGO = P_DOC_CLI
         AND CLI_EMPR = P_EMPRESA;
      P_PED_AUTO := NULL;
    
    END;
    --==============================================================================================
  
    IF P_PED_AUTO = 'S' THEN
    
      DECLARE
        V_MENSAJE VARCHAR2(32767);
        --V_EXISTEN_DETALLES VARCHAR2(5);
        V_ART_SIN_LISTA VARCHAR2(32767);
        CURSOR C_CURSOR IS
          SELECT ART_COD_ALFANUMERICO,
                 FAC_LISTA_PRECIO_DET.LIPR_NRO_LISTA_PRECIO
            FROM STK_ARTICULO, FAC_CLI_PEDIDOS_REP, FAC_LISTA_PRECIO_DET
           WHERE FAC_CLI_PEDIDOS_REP.PREP_ART =
                 FAC_LISTA_PRECIO_DET.LIPR_ART(+)
             AND STK_ARTICULO.ART_CODIGO = FAC_CLI_PEDIDOS_REP.PREP_ART
             AND (FAC_LISTA_PRECIO_DET.LIPR_NRO_LISTA_PRECIO = V_NRO_LISTA OR
                 FAC_LISTA_PRECIO_DET.LIPR_NRO_LISTA_PRECIO IS NULL)
             AND FAC_CLI_PEDIDOS_REP.PREP_CLI = P_DOC_CLI
             AND ART_EMPR = PREP_EMPR
             AND ART_EMPR = LIPR_EMPR
             AND ART_EMPR = P_EMPRESA
           ORDER BY 1 DESC;
      
      BEGIN
      
        FOR V_CURSOR IN C_CURSOR LOOP
          IF V_CURSOR.LIPR_NRO_LISTA_PRECIO IS NULL THEN
            V_ART_SIN_LISTA := V_CURSOR.ART_COD_ALFANUMERICO || ',' ||
                               V_ART_SIN_LISTA;
          END IF;
        END LOOP;
      
        IF V_ART_SIN_LISTA IS NOT NULL THEN
          V_MENSAJE := 'ALGUNOS ARTICULOS DEL PEDIDO A GENERAR: ' ||
                       V_ART_SIN_LISTA ||
                       'NO TIENE ASIGNADOS LISTAS DE PRECIOS N? ' ||
                       V_NRO_LISTA || ', FAVOR REVISAR FACM015 Y FACM013';
          APEX_ERROR.ADD_ERROR(P_MESSAGE          => V_MENSAJE,
                               P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        
          RAISE_APPLICATION_ERROR(-20002, V_MENSAJE);
        
        END IF;
      END;
    END IF;
  END PP_LISTA_PRECIOS;

  PROCEDURE PP_VALIDAR_CLIENTE2(P_EMPRESA             IN NUMBER,
                                P_DOC_CLI             IN NUMBER,
                                P_TIPO_FACTURA        IN NUMBER,
                                P_W_CATEG             OUT NUMBER,
                                P_W_CLI_PORC_EXEN_IVA OUT NUMBER,
                                P_W_IMP_LIM_CR_GRUPO  OUT NUMBER,
                                P_DOC_LEGAJO          OUT NUMBER,
                                P_MENSAJE_CONFIR      OUT VARCHAR2) IS
  
    V_BLOQUEADO       FIN_CLIENTE.CLI_BLOQ_LIM_CR%TYPE;
    V_ESTADO          FIN_CLIENTE.CLI_EST_CLI%TYPE;
    V_MAX_DIAS_ATRASO NUMBER;
    V_VENDEDOR        NUMBER;
    --*
  BEGIN
    SELECT CLI_CATEG,
           CLI_BLOQ_LIM_CR,
           CLI_EST_CLI,
           CLI_PORC_EXEN_IVA,
           CLI_MAX_DIAS_ATRASO,
           CLI_IMP_LIM_CR,
           CLI_VENDEDOR
      INTO P_W_CATEG,
           V_BLOQUEADO,
           V_ESTADO,
           P_W_CLI_PORC_EXEN_IVA,
           V_MAX_DIAS_ATRASO,
           P_W_IMP_LIM_CR_GRUPO,
           V_VENDEDOR
      FROM FIN_CLIENTE
     WHERE CLI_CODIGO = P_DOC_CLI
       AND CLI_EMPR = P_EMPRESA;
    --*
    IF V_ESTADO NOT IN ('A', 'M') THEN
      IF P_TIPO_FACTURA = 2 THEN
        RAISE_APPLICATION_ERROR(-20002, 'EL CLIENTE NO ES ACTIVO.!');
      ELSE
        P_MENSAJE_CONFIR := 'EL CLIENTE NO ES ACTIVO. DESEA CONTINUAR?';
        --SET_ALERT_BUTTON_PROPERTY('AL_CONFIRMAR',ALERT_BUTTON3,LABEL, NULL);
        --IF  FL_CONFIRMAR_REG('EL CLIENTE NO ES ACTIVO. DESEA CONTINUAR?') <> 'CONFIRMAR' THEN
        --        PL_EXHIBIR_ERROR(NULL);
        --END IF;
      END IF;
    END IF;
  
    IF V_ESTADO = 'M' THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL CLIENTE ES MOROSO.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002, 'EL CLIENTE ES MOROSO.');
    END IF;
  
    IF V_BLOQUEADO = 'S' THEN
      IF P_TIPO_FACTURA = 2 THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL LIMITE DE CREDITO DEL CLIENTE SE ENCUENTRA BLOQUEADO.!',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        RAISE_APPLICATION_ERROR(-20002,
                                'EL LIMITE DE CREDITO DEL CLIENTE SE ENCUENTRA BLOQUEADO.!');
      ELSE
        P_MENSAJE_CONFIR := 'EL LIMITE DE CREDITO DEL CLIENTE SE' ||
                            ' ENCUENTRA BLOQUEADO!. DESEA CONTINUAR?';
        --SET_ALERT_BUTTON_PROPERTY('AL_CONFIRMAR',ALERT_BUTTON3,LABEL, NULL);
        --IF  FL_CONFIRMAR_REG('EL LIMITE DE CREDITO DEL CLIENTE SE'||
        --' ENCUENTRA BLOQUEADO!. DESEA CONTINUAR?') <> 'CONFIRMAR' THEN
        --        PL_EXHIBIR_ERROR(NULL);
        --END IF;
      END IF;
    END IF;
  
    --PP_BUSCAR_VEND_CLIENTE;
    BEGIN
      SELECT EMVC_LEGAJO
        INTO V_VENDEDOR
        FROM FAC_EMPR_VEND_CLIENTE
       WHERE EMVC_CLI = P_DOC_CLI
         AND EMVC_EMPR = P_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
      WHEN TOO_MANY_ROWS THEN
        NULL;
    END;
  
    P_DOC_LEGAJO := V_VENDEDOR;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'CLIENTE INEXISTENTE.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002, 'CLIENTE INEXISTENTE.');
  END PP_VALIDAR_CLIENTE2;

  PROCEDURE PL_VALIDAR_OPCTA(P_CTA       IN NUMBER,
                             P_EMPRESA   IN NUMBER,
                             P_LOGIN     IN VARCHAR2,
                             P_CTA_DESC  IN VARCHAR2,
                             P_OPERACION IN VARCHAR2,
                             P_PROGRAMA  IN VARCHAR2) IS
    V_DEP      VARCHAR2(1);
    V_EXT      VARCHAR2(1);
    V_FEC      DATE;
    V_SUCURSAL NUMBER;
    V_PRO      VARCHAR2(1);
  
  BEGIN
    --BUSCAR LA SUCURSAL
    BEGIN
      SELECT CONF_SUC
        INTO V_SUCURSAL
        FROM GEN_CONFIGURACION
       WHERE CONF_EMPR = P_EMPRESA;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    IF SUBSTR((P_PROGRAMA), 1, 3) = 'FIN' THEN
      IF V_SUCURSAL = 1 THEN
        --BUSCAR CUAL FUE LA MAX FECHA MENOR AL DIA DEL HOY
        BEGIN
          SELECT CTA_ULT_FEC_MOVCR
            INTO V_FEC
            FROM FIN_CUENTA_BANCARIA
           WHERE CTA_CODIGO = P_CTA
             AND CTA_EMPR = P_EMPRESA;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            NULL;
        END;
      END IF;
    END IF;
    /* --LINEA ORIGINAL 22-08-19
    SELECT OP_CTA_PROPIETARIO, OP_CTA_IND_PERM_EXT
      INTO V_DEP, V_EXT
      FROM GEN_OPERADOR_EMPRESA,
           GEN_OPERADOR,
           FIN_OPER_CTA_BCO,
           FIN_CUENTA_BANCARIA
     WHERE OPER_CODIGO = OP_CTA_OPER
       AND OP_CTA_EMPR = CTA_EMPR
       AND OP_CTA_CTA_CODIGO = CTA_CODIGO
       AND OPER_LOGIN = P_LOGIN
       AND OP_CTA_EMPR = P_EMPRESA
       AND OP_CTA_CTA_CODIGO = P_CTA
       AND OPER_CODIGO = OPEM_OPER
       AND OPEM_EMPR = P_EMPRESA;
    
    IF P_OPERACION = 'D' AND V_DEP <> 'S' THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'NO TIENE PERMISO PARA OPERAR CON LA CTA. ' ||
                                                 P_CTA_DESC,
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002,
                              'NO TIENE PERMISO PARA OPERAR CON LA CTA. ' ||
                              P_CTA_DESC);
                              */
    --EXTRAER LOS PERMISOS DE DEPOSITO Y EXTRACCION PARA ESA CAJA...
    SELECT NVL(OP_CTA_IND_PERM_DEP, 'N'),
           NVL(OP_CTA_IND_PERM_EXT, 'N'),
           NVL(OP_CTA_PROPIETARIO, 'N')
      INTO V_DEP, V_EXT, V_PRO
      FROM GEN_OPERADOR_EMPRESA,
           GEN_OPERADOR,
           FIN_OPER_CTA_BCO,
           FIN_CUENTA_BANCARIA
     WHERE OPER_CODIGO = OP_CTA_OPER
       AND OP_CTA_EMPR = CTA_EMPR
       AND OP_CTA_CTA_CODIGO = CTA_CODIGO
       AND OPER_LOGIN = P_LOGIN
       AND OP_CTA_EMPR = P_EMPRESA
       AND OP_CTA_CTA_CODIGO = P_CTA
       AND OPER_CODIGO = OPEM_OPER
       AND OPEM_EMPR = P_EMPRESA;
  
    IF P_PROGRAMA = 'FINI003' THEN
      IF V_PRO = 'N' THEN
        RAISE_APPLICATION_ERROR(-20002,
                                'NO TIENE PERMISO PARA OPERAR CON LA CTA. ' ||
                                P_CTA_DESC);
      END IF;
    
    END IF;
  
    IF P_OPERACION = 'D' AND V_DEP <> 'S' THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'NO TIENE PERMISO PARA OPERAR CON LA CTA. ' ||
                                                 P_CTA_DESC,
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002,
                              'NO TIENE PERMISO PARA OPERAR CON LA CTA. ' ||
                              P_CTA_DESC);
    ELSE
      IF P_OPERACION = 'C' AND V_EXT <> 'S' THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'NO TIENE PERMISO PARA OPERAR CON LA CTA. ' ||
                                                   P_CTA_DESC,
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        RAISE_APPLICATION_ERROR(-20002,
                                'NO TIENE PERMISO PARA OPERAR CON LA CTA. ' ||
                                P_CTA_DESC);
      END IF;
    END IF;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'NO PUEDE REALIZAR OPERACIONES CON LA CTA. ' ||
                                                 P_CTA_DESC,
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002,
                              'NO PUEDE REALIZAR OPERACIONES CON LA CTA. ' ||
                              P_CTA_DESC);
  END PL_VALIDAR_OPCTA;

  PROCEDURE PP_VAL_CTA_BCO_MES_ACTUAL(P_FECHA   IN DATE,
                                      P_CTA_BCO IN NUMBER,
                                      P_EMPRESA IN NUMBER,
                                      P_USUARIO IN VARCHAR2) IS
    V_HABILITADO VARCHAR2(1);
    V_VARIABLE   VARCHAR2(1);
  BEGIN
  
    SELECT 'X'
      INTO V_VARIABLE
      FROM FIN_CONFIGURACION
     WHERE P_FECHA BETWEEN CONF_FEC_LIM_MOD AND CONF_PER_ACT_FIN
       AND CONF_EMPR = P_EMPRESA;
  
    IF V_VARIABLE = 'X' THEN
    
      SELECT OP_CTA_MES_ACTUAL
        INTO V_HABILITADO
        FROM GEN_OPERADOR, FIN_OPER_CTA_BCO
       WHERE OPER_CODIGO = OP_CTA_OPER
         AND OPER_LOGIN = P_USUARIO
         AND OP_CTA_CTA_CODIGO = P_CTA_BCO
         AND OP_CTA_EMPR = P_EMPRESA;
    
      IF V_HABILITADO = 'N' THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'LA CAJA/BANCO NO ESTA HABILITADA EN LA FECHA PARA ESTE USUARIO!.',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        RAISE_APPLICATION_ERROR(-20002,
                                'LA CAJA/BANCO NO ESTA HABILITADA EN LA FECHA PARA ESTE USUARIO!.');
      END IF;
    
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      -- RAISE_APPLICATION_ERROR(-20002,
      --                          'LA CAJA/BANCO NO ESTA HABILITADA EN LA FECHA PARA ESTE USUARIO!.');
    
      NULL;
  END PP_VAL_CTA_BCO_MES_ACTUAL;

  FUNCTION FP_DIAS_DE_ATRASO(P_EMPRESA           IN NUMBER,
                             P_DOC_CLI           IN NUMBER,
                             P_CONF_COD_FAC_CRED IN NUMBER) RETURN NUMBER IS
    V_CANT_DIAS NUMBER;
    V_SYSDATE   DATE;
  BEGIN
    V_SYSDATE := TRUNC(SYSDATE);
    SELECT MAX(V_SYSDATE - CUO_FEC_VTO)
      INTO V_CANT_DIAS
      FROM FIN_DOCUMENTO,
           FIN_CUOTA,
           (SELECT DOC_CLAVE_PADRE CLAVE_ANUL, DOC_EMPR EMPR_ANUL
              FROM FIN_DOCUMENTO
             WHERE DOC_EMPR = 1
               AND DOC_TIPO_MOV = 47) ANUL_FAC --ESTO SE INCLUYE, PORQUE HABIA CASOS DE FACTURAS ANULADAS QUE TENIAN NO TENIAN LA CUOTA EN 0 Y SUMABA TODAVIA EN LA CANTIDAD DE DIAS DE ATRASOS.
     WHERE DOC_CLAVE = CUO_CLAVE_DOC
       AND DOC_EMPR = CUO_EMPR
       AND DOC_EMPR = P_EMPRESA
       AND DOC_CLI = P_DOC_CLI
       AND DOC_TIPO_MOV = P_CONF_COD_FAC_CRED
       AND CUO_SALDO_LOC > 0
       AND CUO_SALDO_MON > 0
       AND DOC_CLAVE = ANUL_FAC.CLAVE_ANUL(+)
       AND DOC_EMPR = ANUL_FAC.EMPR_ANUL(+)
       AND ANUL_FAC.CLAVE_ANUL IS NULL;
  
    RETURN V_CANT_DIAS;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 0;
  END FP_DIAS_DE_ATRASO;

  PROCEDURE PP_VALIDAR_VENCIMIENTOS(P_VALIDAR           IN VARCHAR2,
                                    P_EMPRESA           IN NUMBER,
                                    P_DOC_MON           IN NUMBER,
                                    P_DOC_CLI           IN NUMBER,
                                    P_CONF_COD_FAC_CRED IN NUMBER,
                                    P_DOC_FEC_DOC       IN DATE,
                                    P_CONFIRMAR         OUT VARCHAR2) IS
    V_CANT_DIAS          NUMBER;
    V_MAX_DIAS_ATRASO    NUMBER;
    V_CLI_MAX_ATRASO     NUMBER := 7; --Pedido de Deisi 7 dias de atraso para evitar errors
    V_AUT_ESP_MAX_ATRASO NUMBER;
  BEGIN
  
    --EL LIMITE DE CREDITO DEL CLIENTE
  
    /*SELECT NVL(C.CLI_MAX_DIAS_ATRASO, 0)
     INTO V_CLI_MAX_ATRASO
     FROM FIN_CLIENTE C
    WHERE C.CLI_CODIGO = P_DOC_CLI
      AND C.CLI_EMPR = P_EMPRESA;*/
  
    --VER SI EXISTE UNA AUTORIZACION ESPECIAL EN LA FECHA PARA
    SELECT NVL(MAX(AUES_MAX_DIAS_ATRASO), 0)
      INTO V_AUT_ESP_MAX_ATRASO
      FROM FIN_AUTORIZ_ESPEC
     WHERE AUES_CLI = P_DOC_CLI
       AND AUES_FEC_AUTORIZ = P_DOC_FEC_DOC
       AND AUES_MON = P_DOC_MON
       AND AUES_UTILIZADA = 'N'
       AND AUES_FAC_CONT_LN IS NULL
       AND AUES_EMPR = P_EMPRESA;
  
    IF V_CLI_MAX_ATRASO > V_AUT_ESP_MAX_ATRASO THEN
      V_MAX_DIAS_ATRASO := V_CLI_MAX_ATRASO;
    ELSE
      V_MAX_DIAS_ATRASO := V_AUT_ESP_MAX_ATRASO;
    END IF;
  
    V_CANT_DIAS := FP_DIAS_DE_ATRASO(P_EMPRESA           => P_EMPRESA,
                                     P_DOC_CLI           => P_DOC_CLI,
                                     P_CONF_COD_FAC_CRED => P_CONF_COD_FAC_CRED);
  
    IF V_MAX_DIAS_ATRASO < V_CANT_DIAS THEN
      --NULL;
    
      IF P_VALIDAR = 'VALIDAR' THEN
      
        RAISE_APPLICATION_ERROR(-20002,
                                'EL CLIENTE TIENE UN VENCIMIENTO CON ' ||
                                TO_CHAR(V_CANT_DIAS) || ' DIAS DE ATRASO!');
      
      ELSE
        P_CONFIRMAR := 'EL CLIENTE TIENE UN VENCIMIENTO CON ' ||
                       TO_CHAR(V_CANT_DIAS) ||
                       ' DIAS DE ATRASO!. DESEA CONTINUAR?';
      END IF;
    
    END IF;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      P_CONFIRMAR := NULL;
  END PP_VALIDAR_VENCIMIENTOS;

  PROCEDURE PP_VALIDAR_MONEDA(P_DOC_MON               IN NUMBER,
                              P_MON_LOC               IN NUMBER,
                              P_MON_US                IN NUMBER,
                              P_TIPO_FACTURA          IN NUMBER,
                              P_W_ANT_DOC_MON         IN NUMBER,
                              P_PLAZO_PAGO            IN OUT NUMBER,
                              P_DOC_CLI               IN NUMBER,
                              P_EMPRESA               IN NUMBER,
                              P_DOC_FEC_DOC           IN DATE,
                              P_CONF_COD_FAC_CRED     IN NUMBER,
                              P_USUARIO               IN VARCHAR2,
                              P_W_CLI_MAX_DIAS_ATRASO IN OUT NUMBER,
                              P_W_LIM_CR_ESPECIAL     OUT NUMBER,
                              P_CONFIRMAR_VENC        OUT VARCHAR2,
                              P_PORC_DTO              OUT NUMBER,
                              P_RECARGO               OUT NUMBER,
                              P_IND_BTN_TASA          OUT VARCHAR2,
                              P_W_DOC_TASA_US         OUT NUMBER,
                              P_CONFIRMAR_BLOQ        OUT VARCHAR2) IS
  
    V_BLOQUEADO VARCHAR2(1) := ' ';
  BEGIN
  
    BEGIN
      --PP_TRAER_DESC_MONE;
      IF P_DOC_MON = 1 THEN
        P_W_DOC_TASA_US := 1;
      ELSE
        P_W_DOC_TASA_US := FP_COTIZACION(P_MONEDA      => P_DOC_MON,
                                         P_DOC_FEC_DOC => P_DOC_FEC_DOC,
                                         P_EMPRESA     => P_EMPRESA);
      END IF;
      --*
      IF P_DOC_CLI IS NOT NULL THEN
        BEGIN
          --PP_BUSCAR_CLI_EMPR_MONEDA;
          SELECT HOL_BLOQ_LIMI_CR
            INTO V_BLOQUEADO
            FROM FIN_FICHA_HOLDING FH, FIN_CLIENTE FC
           WHERE FC.CLI_CODIGO = P_DOC_CLI
             AND FC.CLI_COD_FICHA_HOLDING = FH.HOL_CODIGO
             AND CLI_EMPR = HOL_EMPR
             AND CLI_EMPR = P_EMPRESA;
          --*
          IF V_BLOQUEADO = 'S' THEN
            IF P_TIPO_FACTURA = 2 THEN
              RAISE_APPLICATION_ERROR(-20002,
                                      'LA CUENTA DEL CLIENTE EN LA EMPRESA SE ENCUENTRA BLOQUEADA PARA ESTA MONEDA.!');
            ELSE
              P_CONFIRMAR_BLOQ := 'LA CUENTA DEL CLIENTE EN LA EMPRESA SE ENCUENTRA BLOQUEADA PARA ESTA MONEDA ' ||
                                  '. DESEA CONTINUAR?';
            END IF;
          END IF;
          --*
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'CLIENTE NO SE ENCUENTRA HABILITADO PARA OPERAR CON ESTA MONEDA!.',
                                 P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
            RAISE_APPLICATION_ERROR(-20002,
                                    'CLIENTE NO SE ENCUENTRA HABILITADO PARA OPERAR CON ESTA MONEDA!.');
        END;
      END IF;
      --*
    END;
  
    IF P_TIPO_FACTURA = 1 OR P_TIPO_FACTURA = 3 THEN
      --SI ES FACTURA CONTADO O IVA INCLUIDO
      --PP_BUSCAR_CTA_BCO;
      NULL;
    ELSE
      BEGIN
      
        /* VER SI EXISTE UNA AUTORIZACION ESPECIAL EN LA FECHA PARA
        EL LIMITE DE CREDITO DEL CLIENTE */
        P_W_LIM_CR_ESPECIAL := 0;
      
        SELECT NVL(SUM(AUES_IMP), 0)
          INTO P_W_LIM_CR_ESPECIAL
          FROM FIN_AUTORIZ_ESPEC
         WHERE AUES_CLI = P_DOC_CLI
           AND AUES_FEC_AUTORIZ = P_DOC_FEC_DOC -- FELIX 22/05/2014 TO_DATE(TO_CHAR(:BDOC_ENCA.DOC_FEC_DOC,'DD/MM/YYYY'),'DD/MM/YYYY')
           AND AUES_MON = P_DOC_MON
           AND AUES_UTILIZADA = 'N'
           AND AUES_FAC_CONT_LN IS NULL
           AND AUES_EMPR = P_EMPRESA;
      
        IF P_TIPO_FACTURA = 2 THEN
          -- SI ES FACTURA CREDITO
          
         
        PP_CTRL_LIM_CR(P_EMPRESA => P_EMPRESA,
                       P_DOC_CLI => P_DOC_CLI);
                           
                           
                           
      --  RAISE_APPLICATION_ERROR(-20002, 'ERROR: ' );
          PP_VALIDAR_VENCIMIENTOS(P_VALIDAR           => 'VALIDAR',
                                  P_EMPRESA           => P_EMPRESA,
                                  P_DOC_CLI           => P_DOC_CLI,
                                  P_DOC_MON           => P_DOC_MON,
                                  P_DOC_FEC_DOC       => P_DOC_FEC_DOC,
                                  P_CONF_COD_FAC_CRED => P_CONF_COD_FAC_CRED,
                                  P_CONFIRMAR         => P_CONFIRMAR_VENC);
        
        ELSE
          PP_VALIDAR_VENCIMIENTOS(P_VALIDAR           => 'NO VALIDAR',
                                  P_EMPRESA           => P_EMPRESA,
                                  P_DOC_CLI           => P_DOC_CLI,
                                  P_DOC_MON           => P_DOC_MON,
                                  P_DOC_FEC_DOC       => P_DOC_FEC_DOC,
                                  P_CONF_COD_FAC_CRED => P_CONF_COD_FAC_CRED,
                                  P_CONFIRMAR         => P_CONFIRMAR_VENC);
        END IF;
        --*
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
        WHEN TOO_MANY_ROWS THEN
          NULL;
      END;
    END IF;
  
    --SI CAMBIO LA MONEDA O SI PLAZO DE PAGO TODAVIA NO TIENE DATOS
    --ENTONCES SE DEBE BUSCAR PLAZO DE PAGO Y PORCENTAJE DE DESCUENTO
    --  IF (NVL(P_DOC_MON, 0) <> NVL(P_W_ANT_DOC_MON, 0) OR
    --    P_PLAZO_PAGO IS NULL) AND P_DOC_CLI IS NOT NULL THEN
    BEGIN
      --PP_BUSCAR_DESCUENTO_PLAZOPAGO;
      SELECT HOL_PORC_DESCUENTO, /* NVL(HOL_DIAS_PLAZO_PAGO, 0),*/
             HOL_RECARGO
        INTO P_PORC_DTO, /*P_PLAZO_PAGO,*/ P_RECARGO
        FROM FIN_FICHA_HOLDING F, FIN_CLIENTE C
       WHERE F.HOL_CODIGO = C.CLI_COD_FICHA_HOLDING
         AND C.CLI_CODIGO = P_DOC_CLI
         AND CLI_EMPR = HOL_EMPR
         AND CLI_EMPR = P_EMPRESA;
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
    --END IF;
  
    IF P_DOC_MON = P_MON_LOC THEN
      --SET_ITEM_PROPERTY('BDOC_ENCA.W_DOC_TASA_US', ENABLED, PROPERTY_FALSE);
      NULL;
    ELSE
      --SET_ITEM_PROPERTY('BDOC_ENCA.W_DOC_TASA_US', ENABLED, PROPERTY_TRUE);
      --SET_ITEM_PROPERTY('BDOC_ENCA.W_DOC_TASA_US', NAVIGABLE, PROPERTY_TRUE);
    
      BEGIN
        --PP_DESHABILITAR_TASA;
        SELECT NVL(OPEM_MOD_TASA, 'N')
          INTO P_IND_BTN_TASA
          FROM GEN_OPERADOR, GEN_OPERADOR_EMPRESA B
         WHERE OPER_CODIGO = OPEM_OPER
           AND OPER_LOGIN = P_USUARIO
           AND B.OPEM_EMPR = P_EMPRESA;
      
      EXCEPTION
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20002, 'ERROR: ' || SQLERRM);
      END;
    END IF;
  END PP_VALIDAR_MONEDA;

  PROCEDURE PP_CTRL_LIM_CR(P_EMPRESA IN NUMBER, P_DOC_CLI IN NUMBER) IS
    V_FEC_GRAB DATE;
  BEGIN
  
    SELECT F.HOL_FEC_GRAB_LIM_CR
      INTO V_FEC_GRAB
      FROM FIN_FICHA_HOLDING F, FIN_CLIENTE C
     WHERE C.CLI_CODIGO = P_DOC_CLI
       AND C.CLI_COD_FICHA_HOLDING = F.HOL_CODIGO
       AND CLI_EMPR = HOL_EMPR
       AND CLI_EMPR = P_EMPRESA;
       
        /*RAISE_APPLICATION_ERROR(-20002,
                                 V_FEC_GRAB );*/
  
    IF TO_CHAR(SYSDATE, 'DD/MM/YYYY') <> TO_CHAR(V_FEC_GRAB, 'DD/MM/YYYY')   THEN
      FIN_BLOQ_FAC_CR(P_DOC_CLI, P_EMPRESA);
    END IF;
  END PP_CTRL_LIM_CR;

  PROCEDURE PL_CONTROL_OPER_DEP(I_EMPRESA   NUMBER,
                                I_SUCURSAL  NUMBER,
                                I_DEPOSITO  IN NUMBER,
                                I_OPERACION VARCHAR2,
                                P_USUARIO   IN VARCHAR2,
                                I_CLAVE     OUT NUMBER) IS
  
    --** VALIDA USER PARA OPERACIONES HABILITADAS **--
  
    CONT CHAR(1);
    --CLAVE NUMBER;
  BEGIN
  
    IF I_OPERACION = 'DEP' AND I_DEPOSITO IS NOT NULL THEN
      SELECT 'X'
        INTO CONT
        FROM GEN_OPERADOR, GEN_OPERADOR_EMPRESA, STK_OPER_DEPO
       WHERE OPER_LOGIN = P_USUARIO
         AND (OPEM_OPER = STOPE_OPERADOR AND STOPE_EMPR = I_EMPRESA AND
             STOPE_SUC = I_SUCURSAL AND STOPE_CODIGO = I_DEPOSITO AND
             STOPE_DEP = 'S')
         AND OPER_CODIGO = OPEM_OPER
         AND OPEM_EMPR = I_EMPRESA;
    
    ELSE
      SELECT 'X'
        INTO CONT
        FROM GEN_OPERADOR, GEN_OPERADOR_EMPRESA, STK_OPER_DEPO
       WHERE OPER_LOGIN = P_USUARIO
         AND (OPEM_OPER = STOPE_OPERADOR AND STOPE_EMPR = I_EMPRESA AND
             STOPE_SUC = I_SUCURSAL AND STOPE_CODIGO = I_DEPOSITO AND
             STOPE_EXT = 'S')
         AND OPER_CODIGO = OPEM_OPER
         AND OPEM_EMPR = I_EMPRESA;
    
    END IF;
    I_CLAVE := NULL;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
    
      BEGIN
      
        IF I_OPERACION = 'DEP' THEN
        
          SELECT O.STOPES_CLAVE
            INTO I_CLAVE
            FROM STK_DOCUMENTO        T,
                 STK_OPER_DEP_ESP     O,
                 GEN_OPERADOR,
                 GEN_OPERADOR_EMPRESA
           WHERE T.DOCU_CLAVE_STOPES(+) = O.STOPES_CLAVE
             AND OPER_LOGIN = P_USUARIO
             AND T.DOCU_EMPR(+) = O.STOPES_EMPR
             AND TRUNC(SYSDATE) BETWEEN O.STOPES_FECHA_DESDE AND
                 O.STOPES_FECHA_HASTA
             AND T.DOCU_SUC_ORIG(+) = O.STOPES_SUC
             AND T.DOCU_DEP_ORIG(+) = O.STOPES_CODIGO
             AND (OPEM_OPER = STOPES_OPERADOR AND STOPES_EMPR = I_EMPRESA AND
                  STOPES_SUC = I_SUCURSAL AND STOPES_CODIGO = I_DEPOSITO AND
                  STOPES_DEP = 'S')
             AND OPER_CODIGO = OPEM_OPER
             AND OPEM_EMPR = I_EMPRESA
           GROUP BY O.STOPES_CLAVE,
                    O.STOPES_DEP,
                    O.STOPES_EXT,
                    O.STOPES_CANT
          HAVING NVL((O.STOPES_CANT) - COUNT(T.DOCU_CLAVE_STOPES), 0) > 0;
        
        ELSE
        
          SELECT O.STOPES_CLAVE
            INTO I_CLAVE
            FROM STK_DOCUMENTO        T,
                 STK_OPER_DEP_ESP     O,
                 GEN_OPERADOR,
                 GEN_OPERADOR_EMPRESA
           WHERE T.DOCU_CLAVE_STOPES(+) = O.STOPES_CLAVE
             AND OPER_LOGIN = P_USUARIO
             AND T.DOCU_EMPR(+) = O.STOPES_EMPR
             AND TRUNC(SYSDATE) BETWEEN O.STOPES_FECHA_DESDE AND
                 O.STOPES_FECHA_HASTA
             AND T.DOCU_SUC_ORIG(+) = O.STOPES_SUC
             AND T.DOCU_DEP_ORIG(+) = O.STOPES_CODIGO
             AND (OPEM_OPER = STOPES_OPERADOR AND STOPES_EMPR = I_EMPRESA AND
                  STOPES_SUC = I_SUCURSAL AND STOPES_CODIGO = I_DEPOSITO AND
                  STOPES_EXT = 'S')
             AND OPEM_EMPR = I_EMPRESA
             AND OPEM_OPER = OPER_CODIGO
           GROUP BY O.STOPES_CLAVE,
                    O.STOPES_DEP,
                    O.STOPES_EXT,
                    O.STOPES_CANT
          HAVING NVL((O.STOPES_CANT) - COUNT(T.DOCU_CLAVE_STOPES), 0) > 0;
        END IF;
      
      EXCEPTION
      
        WHEN NO_DATA_FOUND THEN
          APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'OPERACION EN DEPOSITO NO HABILITADA PARA OPERADOR!',
                               P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
          RAISE_APPLICATION_ERROR(-20002,
                                  'OPERACION EN DEPOSITO NO HABILITADA PARA OPERADOR!');
        WHEN OTHERS THEN
          APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'ERROR EN PL_CONTROL_OPER_DEP',
                               P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
          RAISE_APPLICATION_ERROR(-20002, 'ERROR EN PL_CONTROL_OPER_DEP');
          ---------------------***********---------------------
      END;
  END PL_CONTROL_OPER_DEP;

  PROCEDURE PP_TRAER_CAMION(P_EMPRESA            IN NUMBER,
                            P_DOC_CAMION         IN NUMBER,
                            P_SUCURSAL           IN NUMBER,
                            P_TRANSPORTISTA      IN NUMBER,
                            P_W_CAPACIDAD_CAMION OUT NUMBER) IS
  BEGIN
  
    SELECT RMAR_MAX_CAP
      INTO P_W_CAPACIDAD_CAMION
      FROM STK_REMI_VEHICULO JHN
     WHERE RMAR_VEH_COD = P_DOC_CAMION
       AND JHN.RMAR_VEH_TRASPORTISTA = P_TRANSPORTISTA
       AND RMAR_EMPR = P_EMPRESA;
  
    DECLARE
      V_COD_SUC NUMBER;
      Y         VARCHAR2(250) := 'ALERT("No unprinted stock items found - cannot execute stock print request")';
    BEGIN
    
      /*SELECT CAM_SUC
       INTO V_COD_SUC
       FROM STK_CAMIONES_V
      WHERE CAM_COD = P_DOC_CAMION;*/
    
      SELECT CAM_SUC
        INTO V_COD_SUC
        FROM (SELECT CAM_SUC, TRANSPORTISTA
                FROM STK_CAMIONES_V
               WHERE TRANSPORTISTA = 3
                 AND CAM_COD = P_DOC_CAMION
              UNION ALL
              SELECT A.RMAR_SUC, A.RMAR_VEH_TRASPORTISTA
                FROM STK_REMI_VEHICULO A
               WHERE A.RMAR_IND_MOV_TAGRO = 'S'
                 AND A.RMAR_VEH_TRASPORTISTA = 7
                 AND A.RMAR_VEH_COD > 342
                 AND A.RMAR_VEH_COD = P_DOC_CAMION)
       WHERE TRANSPORTISTA = P_TRANSPORTISTA;
    
      IF V_COD_SUC <> P_SUCURSAL THEN
        /*
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'ESTE CAMION NO PERTENECE A ESTA SUCURSAL ' ||
                                                   P_SUCURSAL,
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        RAISE_APPLICATION_ERROR(-20002,
                                'ESTE CAMION NO PERTENECE A ESTA SUCURSAL ' ||
                                P_SUCURSAL);
                                */
        HTP.SCRIPT(Y, 'JAVASCRIPT');
      
        --        APEX_APPLICATION.G_PRINT_SUCCESS_MESSAGE := '<SPAN STYLE="color:green">COMPUTER CREATED</SPAN>';
        --APEX_APPLICATION.G_PRINT_SUCCESS_MESSAGE := '<SPAN STYLE="color:green">UPDATES SUCCESSFULLY APPLIED</SPAN>';
      
      END IF;
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'ESTE CAMION NRO ' ||
                                                   P_DOC_CAMION ||
                                                   '  NO TIENE ASIGNADO UN CHOFER!',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        RAISE_APPLICATION_ERROR(-20002,
                                'ESTE CAMION NRO ' || P_DOC_CAMION ||
                                '  NO TIENE ASIGNADO UN CHOFER!');
    END;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'CAMION INEXISTENTE!',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002, 'CAMION INEXISTENTE!');
  END PP_TRAER_CAMION;

  PROCEDURE PP_VALIDAR_CARGA(P_DOC_CAMION        IN NUMBER,
                             P_DOC_OCARGA_LONDON IN OUT NUMBER,
                             P_EMPRESA           IN NUMBER,
                             P_SUCURSAL          IN NUMBER) IS
    V_SUC       NUMBER;
    V_ESTADO    VARCHAR2(1);
    V_CONDUCTOR NUMBER;
    V_FECHA     DATE;
    V_CAMION    NUMBER;
    SALIR EXCEPTION;
  BEGIN
  
    IF P_DOC_CAMION IS NULL THEN
      P_DOC_OCARGA_LONDON := NULL;
    END IF;
  
    IF P_DOC_OCARGA_LONDON IS NULL THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'ORDEN DE CARGA NO PUEDE QUEDAR EN BLANCO' ||
                                                 P_DOC_CAMION,
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002,
                              'ORDEN DE CARGA NO PUEDE QUEDAR EN BLANCO');
    END IF;
  
    SELECT OCA_SUC, OCA_ESTADO, OCA_CONDUCTOR, OCA_FECHA, OCA_CAMION
      INTO V_SUC, V_ESTADO, V_CONDUCTOR, V_FECHA, V_CAMION
      FROM STK_ORDEN_CARGA_LONDON T
     WHERE T.OCA_CLAVE = P_DOC_OCARGA_LONDON
       AND OCA_EMPR = P_EMPRESA;
  
    IF V_SUC <> P_SUCURSAL THEN
      IF V_SUC <> 11 AND P_SUCURSAL <> 1 THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'ORDEN DE CARGA PERTENECE A OTRA SUCURSAL',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        RAISE_APPLICATION_ERROR(-20002,
                                'ORDEN DE CARGA PERTENECE A OTRA SUCURSAL');
      END IF;
    END IF;
  
    IF V_ESTADO = 'C' THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'ORDEN DE CARGA ESTA CERRADA',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002, 'ORDEN DE CARGA ESTA CERRADA');
    END IF;
  
    IF V_FECHA < TRUNC(SYSDATE) - 1 THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'ORDEN DE CARGA PERTENECE A FECHA PASADA',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002,
                              'ORDEN DE CARGA PERTENECE A FECHA PASADA');
    END IF;
  
    IF V_CAMION <> P_DOC_CAMION THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'ORDEN DE CARGA NO CORRESPONDE AL CAMION',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002,
                              'ORDEN DE CARGA NO CORRESPONDE AL CAMION');
    END IF;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'ORDEN DE CARGA INEXISTENTE',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002, 'ORDEN DE CARGA INEXISTENTE');
  END PP_VALIDAR_CARGA;

  PROCEDURE PP_VALIDAR_LISTA_PRECIO(P_EMPRESA          IN NUMBER,
                                    P_NRO_LISTA_PRECIO IN NUMBER,
                                    P_DOC_MON          IN NUMBER) IS
    V_LIS_MON NUMBER;
    SALIR EXCEPTION;
  BEGIN
  
    IF P_NRO_LISTA_PRECIO IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'Error la lista de precio no puede quedar vacio');
    END IF;
    RAISE SALIR;
    IF P_NRO_LISTA_PRECIO IS NOT NULL THEN
      BEGIN
        SELECT LIPE_MON
          INTO V_LIS_MON
          FROM FAC_LISTA_PRECIO      D,
               GEN_OPERADOR_LISTA_PR T,
               GEN_OPERADOR          S,
               GEN_OPERADOR_EMPRESA  OE
         WHERE LIPE_EMPR = P_EMPRESA
           AND LIPE_NRO_LISTA_PRECIO = P_NRO_LISTA_PRECIO
           AND D.LIPE_NRO_LISTA_PRECIO = T.OPLPR_LISTA_PR
           AND D.LIPE_EMPR = T.OPLPR_EMPRE
           AND OE.OPEM_OPER = T.OPLPR_OPERADOR
           AND OE.OPEM_EMPR = T.OPLPR_EMPRE
           AND S.OPER_CODIGO = OE.OPEM_OPER
           AND S.OPER_LOGIN = GEN_DEVUELVE_USER;
      
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
        
          IF GEN_DEVUELVE_USER = 'ADCS' THEN
            V_LIS_MON := 1;
          ELSE
            APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL OPERADOR NO TIENE ASIGNADO LA LISTA DE PRECIOS!',
                                 P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
            RAISE_APPLICATION_ERROR(-20002,
                                    'EL OPERADOR NO TIENE ASIGNADO LA LISTA DE PRECIOS!.');
          END IF;
      END;
    
      IF V_LIS_MON <> P_DOC_MON THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'LA MONEDA DE LA LISTA DE PRECIO ' ||
                                                   P_NRO_LISTA_PRECIO ||
                                                   ' ES DISTINTA A LA MONEDA DE LA FACTURA!.',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        RAISE_APPLICATION_ERROR(-20002,
                                'LA MONEDA DE LA LISTA DE PRECIO ' ||
                                P_NRO_LISTA_PRECIO ||
                                ' ES DISTINTA A LA MONEDA DE LA FACTURA!.');
      END IF;
    ELSIF P_DOC_MON != 2 THEN
      --CAMBIO TEMPORAL, PORQUE ALGUNOS ARTICULOS NO TIENE LISTA DE PRECIO EN DOLAR. 17/12/19. VER CON MR. ROLANDO PARA CORREGIR Y QUITAR ESTA PARTE.
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL CAMPO DE LISTA DE PRECIO NO PUEDE QUEDAR VACIO.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002,
                              'EL CAMPO DE LISTA DE PRECIO NO PUEDE QUEDAR VACIO.');
    
    END IF;
  
  EXCEPTION
    WHEN SALIR THEN
      NULL;
    WHEN NO_DATA_FOUND THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'LISTA DE PRECIOS INEXISTENTE!',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002, 'LISTA DE PRECIOS INEXISTENTE!.');
    
  END PP_VALIDAR_LISTA_PRECIO;

  PROCEDURE PP_VALIDAR_ENCABEZADO(P_DOC_CAMION        IN NUMBER,
                                  P_DOC_TRANSPORTISTA IN NUMBER,
                                  P_DOC_OCARGA_LONDON IN NUMBER,
                                  P_PORC_DTO          IN NUMBER,
                                  P_PLAZO_PAGO        IN NUMBER,
                                  P_DOC_LEGAJO        IN NUMBER,
                                  P_DOC_CLI_TEL       IN VARCHAR2,
                                  P_DEPOSITO          IN NUMBER,
                                  P_RECARGO           IN NUMBER,
                                  P_DOC_MON           IN NUMBER,
                                  P_W_DOC_TASA_US     IN NUMBER,
                                  P_DOC_CLI_RUC       IN VARCHAR2,
                                  P_DOC_CTA_BCO_FCON  IN NUMBER,
                                  P_DOC_CLI_DIR       IN VARCHAR2,
                                  P_DOC_CANAL         IN NUMBER,
                                  P_DOC_CLI           IN NUMBER,
                                  P_PED_AUTO          IN VARCHAR2,
                                  -- P_S_NRO_PEDIDO          IN NUMBER,
                                  P_DOC_TIMBRADO          IN NUMBER,
                                  P_DOC_NRO_DOC           IN NUMBER,
                                  P_DOC_SERIE             IN VARCHAR2,
                                  P_DOC_FEC_DOC           IN DATE,
                                  P_S_TIPO_FACTURA        IN NUMBER,
                                  P_EMPRESA               IN NUMBER,
                                  P_SUCURSAL              IN NUMBER,
                                  P_CONF_PER_ACT_INI      IN DATE,
                                  P_CONF_PER_SGTE_FIN     IN DATE,
                                  P_CONF_STK_PER_ACT_INI  IN DATE,
                                  P_CONF_STK_PER_SGTE_FIN IN DATE,
                                  P_USUARIO               IN VARCHAR2,
                                  P_CONF_CATEG_CLI_ESPOR  IN NUMBER,
                                  P_MON_LOC               IN NUMBER,
                                  P_MON_US                IN NUMBER,
                                  P_CONF_COD_FAC_CRED     IN NUMBER,
                                  P_NRO_LISTA_PRECIO      IN NUMBER,
                                  P_MENSAJE               OUT VARCHAR2,
                                  P_CONTINUAR             OUT VARCHAR2) IS
  
    V_EXISTE NUMBER;
    --V_MENSAJE      VARCHAR2(32000);
    V_TEMPORAL      NUMBER;
    V_TEMPORAL_VAR  VARCHAR2(1000);
    V_FECHA_ULT_FAC DATE;
    V_HAB_FAC   VARCHAR(1);
  BEGIN
  
   SELECT NVL(OPEM_IND_FAC_ATRAS, 'N')
     INTO V_HAB_FAC  
   FROM GEN_OPERADOR_EMPRESA, GEN_OPERADOR
  WHERE OPER_LOGIN = P_USUARIO
    AND OPER_CODIGO = OPEM_OPER
    AND OPEM_EMPR = P_EMPRESA;
  

    -- VALIDAR FACTURA
    SELECT COUNT(*)
      INTO V_EXISTE
      FROM FIN_DOCUMENTO
     WHERE DOC_NRO_DOC = P_DOC_NRO_DOC
       AND DOC_TIMBRADO = P_DOC_TIMBRADO
       AND DOC_TIPO_MOV IN (9, 10)
       AND DOC_EMPR = P_EMPRESA;
  
    SELECT MAX(DOC_FEC_DOC)
      INTO V_FECHA_ULT_FAC
      FROM FIN_DOCUMENTO
     WHERE DOC_NRO_DOC = P_DOC_NRO_DOC - 1
       AND DOC_TIMBRADO = P_DOC_TIMBRADO
       AND DOC_TIPO_MOV IN (9, 10)
       AND DOC_EMPR = P_EMPRESA;
       
      -- RAISE_APPLICATION_ERROR(-20002, P_DOC_FEC_DOC);
  
    IF V_FECHA_ULT_FAC IS NOT NULL THEN
      IF P_DOC_FEC_DOC < V_FECHA_ULT_FAC   AND  V_HAB_FAC   != 'S'  THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'NO PUEDE FACTURAR CON FECHA MENOR A ' ||
                                                   V_FECHA_ULT_FAC,
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      
      END IF;
    
    END IF;
  
    IF V_EXISTE > 1 THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'NUMERO DE FACTURA Y TIMBRADO EXISTENTE',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF;
    -- VALIDAR FECHAS
  
    IF P_DOC_FEC_DOC IS NOT NULL THEN
      IF P_DOC_FEC_DOC NOT BETWEEN P_CONF_PER_ACT_INI AND
         P_CONF_PER_SGTE_FIN THEN
        P_MENSAJE := 'FECHA DEBE ESTAR COMPRENDIDA EN EL RANGO DE: ' ||
                     TO_CHAR(P_CONF_PER_ACT_INI, 'DD-MM-YYYY') || ' A: ' ||
                     TO_CHAR(P_CONF_PER_SGTE_FIN, 'DD-MM-YYYY') ||
                     ' PARA EL SISTEMA DE FINANZAS!';
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => P_MENSAJE,
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      END IF;
      IF P_DOC_FEC_DOC NOT BETWEEN P_CONF_STK_PER_ACT_INI AND
         P_CONF_STK_PER_SGTE_FIN THEN
        P_MENSAJE := 'FECHA DEBE ESTAR COMPRENDIDA EN EL RANGO DE: ' ||
                     TO_CHAR(P_CONF_STK_PER_ACT_INI, 'DD-MM-YYYY') ||
                     ' A: ' ||
                     TO_CHAR(P_CONF_STK_PER_SGTE_FIN, 'DD-MM-YYYY') ||
                     ' PARA EL SISTEMA DE STOCK!';
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => P_MENSAJE,
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      END IF;
    END IF;
    --
    PP_VAL_FACT_PEND_COBRO;
  
    IF P_DOC_LEGAJO IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010, 'VENDEDOR NO PUEDE SER NULO');
    END IF;
  
    PL_VALIDAR_HAB_MES_FIN(P_FECHA   => P_DOC_FEC_DOC,
                           P_EMPRESA => P_EMPRESA,
                           P_USUARIO => P_USUARIO);
    --
    PL_VALIDAR_HAB_MES_STK(P_FECHA   => P_DOC_FEC_DOC,
                           P_EMPRESA => P_EMPRESA,
                           P_USUARIO => P_USUARIO);
    --
    PL_VAL_DOC_TIMBRADO(I_NRO_DOC   => P_DOC_NRO_DOC,
                        I_CLASE_DOC => 1,
                        I_FECHA     => P_DOC_FEC_DOC,
                        O_TIMBRADO  => V_TEMPORAL,
                        I_EMPRESA   => P_EMPRESA);
    --
    /*PP_BUSCAR_PEDIDO(P_EMPRESA     => P_EMPRESA,
    P_NRO_PEDIDO  => P_S_NRO_PEDIDO,
    P_DOC_CLI_NRO_PED => V_TEMPORAL,
    P_DOC_MON     => V_TEMPORAL,
    P_DOC_CLI     => V_TEMPORAL,
    P_CLI_NOM     => V_TEMPORAL_VAR,
    P_CLI_DIR     => V_TEMPORAL_VAR,
    P_DOC_CLI_RUC => V_TEMPORAL_VAR,
    P_DOC_CLI_TEL => V_TEMPORAL_VAR,
    P_MEN_MENSAJE_DESC      => V_TEMPORAL_VAR,
    P_LIPR_NRO_LISTA_PRECIO => V_TEMPORAL);*/
    --
    PP_TRAER_DESC_CLI(P_DOC_CLI                     => P_DOC_CLI,
                      P_EMPRESA                     => P_EMPRESA,
                      P_DOC_CLI_TEL                 => V_TEMPORAL_VAR,
                      P_DOC_CLI_RUC                 => V_TEMPORAL_VAR,
                      P_CLI_PERS_REPRESENTANTE      => V_TEMPORAL_VAR,
                      P_CLI_DOC_IDENT_REPRESENTANTE => V_TEMPORAL_VAR,
                      P_CLI_LOCALIDAD               => V_TEMPORAL_VAR,
                      P_W_CLI_MAX_DIAS_ATRASO       => V_TEMPORAL,
                      P_DIAS_ATRASO                 => V_TEMPORAL,
                      P_CLI_NOM                     => V_TEMPORAL_VAR,
                      P_CLI_DIR                     => V_TEMPORAL_VAR,
                      P_DOC_CANAL                   => V_TEMPORAL,
                      P_CLI_IND_MOD_CANAL           => V_TEMPORAL_VAR,
                      P_OCASIONAL                   => V_TEMPORAL,
                      P_PLAZO_PAGO                  => V_TEMPORAL,
                      P_PORC_DTO                    => V_TEMPORAL,
                      P_RECARGO                     => V_TEMPORAL);
    --
    PP_VALIDAR_CLIENTE(P_EMPRESA      => P_EMPRESA,
                       P_DOC_CLI      => P_DOC_CLI,
                       P_TIPO_FACTURA => P_S_TIPO_FACTURA,
                       P_DOC_FEC_DOC  => P_DOC_FEC_DOC,
                       P_LIM_FAC_ADIC => V_TEMPORAL_VAR);
    --
    PP_LISTA_PRECIOS(P_EMPRESA                   => P_EMPRESA,
                     P_DOC_CLI                   => P_DOC_CLI,
                     P_LIPR_NRO_LISTA_PRECIO     => V_TEMPORAL,
                     P_PED_AUTO                  => V_TEMPORAL_VAR,
                     P_TIPO_FACTURA              => P_S_TIPO_FACTURA,
                     P_DOC_FEC_DOC               => P_DOC_FEC_DOC,
                     P_W_IMP_LIM_CR_EMPR         => V_TEMPORAL,
                     P_W_IMP_LIM_CR_DISP_GRUPO   => V_TEMPORAL,
                     P_W_IMP_LIM_CR_DISP_EMPR    => V_TEMPORAL,
                     P_W_IMP_CHEQ_DIFERIDO       => V_TEMPORAL,
                     P_W_IMP_CHEQ_RECHAZADO      => V_TEMPORAL,
                     P_CATEG                     => V_TEMPORAL,
                     P_CONF_CATEG_CLI_ESPORADICO => P_CONF_CATEG_CLI_ESPOR,
                     P_VENDEDOR                  => P_DOC_LEGAJO,
                     I_DOC_MON                   => P_DOC_MON);
    --
    PP_VALIDAR_CLIENTE2(P_EMPRESA             => P_EMPRESA,
                        P_DOC_CLI             => P_DOC_CLI,
                        P_TIPO_FACTURA        => P_S_TIPO_FACTURA,
                        P_W_CATEG             => V_TEMPORAL,
                        P_W_CLI_PORC_EXEN_IVA => V_TEMPORAL,
                        P_W_IMP_LIM_CR_GRUPO  => V_TEMPORAL,
                        P_DOC_LEGAJO          => V_TEMPORAL,
                        P_MENSAJE_CONFIR      => V_TEMPORAL_VAR);
    --
    IF P_S_TIPO_FACTURA = 1 THEN
      PL_VALIDAR_OPCTA(P_CTA       => P_DOC_CTA_BCO_FCON,
                       P_EMPRESA   => P_EMPRESA,
                       P_LOGIN     => P_USUARIO,
                       P_CTA_DESC  => '',
                       P_OPERACION => 'D',
                       P_PROGRAMA  => 'FACI039');
    END IF;
    --
    PP_VAL_CTA_BCO_MES_ACTUAL(P_FECHA   => P_DOC_FEC_DOC,
                              P_CTA_BCO => P_DOC_CTA_BCO_FCON,
                              P_EMPRESA => P_EMPRESA,
                              P_USUARIO => P_USUARIO);
    --
    PL_CONTROL_OPER_DEP(I_EMPRESA   => P_EMPRESA,
                        I_SUCURSAL  => P_SUCURSAL,
                        I_DEPOSITO  => P_DEPOSITO,
                        I_OPERACION => 'EXT',
                        P_USUARIO   => P_USUARIO,
                        I_CLAVE     => V_TEMPORAL);
    --
    IF P_DOC_CAMION IS NOT NULL THEN
      PP_TRAER_CAMION(P_EMPRESA            => P_EMPRESA,
                      P_DOC_CAMION         => P_DOC_CAMION,
                      P_SUCURSAL           => P_SUCURSAL,
                      P_TRANSPORTISTA      => P_DOC_TRANSPORTISTA,
                      P_W_CAPACIDAD_CAMION => V_TEMPORAL);
      --
      V_TEMPORAL := P_DOC_OCARGA_LONDON;
    
      PP_VALIDAR_CARGA(P_DOC_CAMION        => P_DOC_CAMION,
                       P_DOC_OCARGA_LONDON => V_TEMPORAL,
                       P_EMPRESA           => P_EMPRESA,
                       P_SUCURSAL          => P_SUCURSAL);
    END IF;
  
    PP_VAL_MAX_ATRASO(I_EMPRESA      => P_EMPRESA,
                      I_DOC_CLI      => P_DOC_CLI,
                      I_DOC_FEC_DOC  => P_DOC_FEC_DOC,
                      I_MON          => P_DOC_MON,
                      I_TIPO_FACTURA => P_S_TIPO_FACTURA);
  
    --
    PP_VALIDAR_MONEDA(P_DOC_MON               => P_DOC_MON,
                      P_MON_LOC               => P_MON_LOC,
                      P_MON_US                => P_MON_US,
                      P_TIPO_FACTURA          => P_S_TIPO_FACTURA,
                      P_W_ANT_DOC_MON         => V_TEMPORAL,
                      P_PLAZO_PAGO            => V_TEMPORAL,
                      P_DOC_CLI               => P_DOC_CLI,
                      P_EMPRESA               => P_EMPRESA,
                      P_DOC_FEC_DOC           => P_DOC_FEC_DOC,
                      P_CONF_COD_FAC_CRED     => P_CONF_COD_FAC_CRED,
                      P_USUARIO               => P_USUARIO,
                      P_W_CLI_MAX_DIAS_ATRASO => V_TEMPORAL,
                      P_W_LIM_CR_ESPECIAL     => V_TEMPORAL,
                      P_CONFIRMAR_VENC        => V_TEMPORAL_VAR,
                      P_PORC_DTO              => V_TEMPORAL,
                      P_RECARGO               => V_TEMPORAL,
                      P_IND_BTN_TASA          => V_TEMPORAL_VAR,
                      P_W_DOC_TASA_US         => V_TEMPORAL,
                      P_CONFIRMAR_BLOQ        => V_TEMPORAL_VAR);
  
    --
    PP_VALIDAR_LISTA_PRECIO(P_EMPRESA          => P_EMPRESA,
                            P_NRO_LISTA_PRECIO => P_NRO_LISTA_PRECIO,
                            P_DOC_MON          => P_DOC_MON);
  END PP_VALIDAR_ENCABEZADO;

  PROCEDURE PP_FACTURA(P_EMPRESA             IN NUMBER,
                       P_TIPO_FACTURA        IN NUMBER,
                       P_MAX_ITEMS_FAC_CO    OUT NUMBER,
                       P_MAX_ITEMS_FAC_CR    OUT NUMBER,
                       P_DOC_TIPO_MOV        OUT NUMBER,
                       P_CONF_COD_FAC_CONT   IN NUMBER,
                       P_CONF_COD_FAC_CRED   IN NUMBER,
                       P_DOC_TIMBRADO        OUT NUMBER,
                       P_DOC_TIPO_SALDO      OUT VARCHAR2,
                       P_IND_CUOTA           OUT VARCHAR2,
                       P_CONF_IND_CUOTA_AUTO IN VARCHAR2) IS
  BEGIN
    P_DOC_TIMBRADO := 0;
  
    --INDICADOR PARA HABILITAR O NO EL BOTON CUOTAS
    IF P_TIPO_FACTURA = 1 THEN
      SELECT NVL(CONF_MAX_ITEMS_FAC_CO, 0)
        INTO P_MAX_ITEMS_FAC_CO
        FROM FAC_CONFIGURACION
       WHERE CONF_EMPR = P_EMPRESA;
      P_DOC_TIPO_MOV := P_CONF_COD_FAC_CONT;
      /*SET_ITEM_PROPERTY('BDOC_ENCA.DOC_MON',ENABLED,PROPERTY_FALSE);
      SET_ITEM_PROPERTY('BDOC_ENCA.DOC_CTA_BCO_FCON',ENABLED,PROPERTY_TRUE);
      SET_ITEM_PROPERTY('BDOC_ENCA.DOC_CTA_BCO_FCON',NAVIGABLE,PROPERTY_TRUE);*/
    
    ELSE
      --ELSIF P_TIPO_FACTURA = 2 THEN
      SELECT NVL(CONF_MAX_ITEMS_FAC_CR, 0)
        INTO P_MAX_ITEMS_FAC_CR
        FROM FAC_CONFIGURACION
       WHERE CONF_EMPR = P_EMPRESA;
      P_DOC_TIPO_MOV := P_CONF_COD_FAC_CRED;
      /*   SET_ITEM_PROPERTY('BDOC_ENCA.DOC_MON',ENABLED,PROPERTY_TRUE);
      SET_ITEM_PROPERTY('BDOC_ENCA.DOC_MON',NAVIGABLE,PROPERTY_TRUE);
      SET_ITEM_PROPERTY('BDOC_ENCA.DOC_CTA_BCO_FCON',ENABLED,PROPERTY_FALSE);
      :P2385_DOC_CTA_BCO_FCON := NULL;
      :BDOC_ENCA.BCO_DESC := NULL;
      :BDOC_ENCA.CTA_DESC := NULL;*/
    END IF;
  
    SELECT TMOV_TIPO
      INTO P_DOC_TIPO_SALDO
      FROM GEN_TIPO_MOV
     WHERE TMOV_CODIGO = P_DOC_TIPO_MOV
       AND TMOV_EMPR = P_EMPRESA;
  
    IF P_TIPO_FACTURA = 1 OR P_TIPO_FACTURA = 3 THEN
      -- SI ES FACTURA CONTADO
      P_IND_CUOTA := 'N';
      /*SET_ITEM_PROPERTY('BPIE.CUOTAS', ENABLED,PROPERTY_FALSE);
       SET_ITEM_PROPERTY('BPIE.PAGO',ENABLED,PROPERTY_TRUE);
       SET_ITEM_PROPERTY('BPIE.PAGO',NAVIGABLE,PROPERTY_TRUE);
      
      SET_ITEM_PROPERTY('BDOC_ENCA.DOC_IND_IMPR_CONYUGUE',ENABLED,PROPERTY_FALSE);
      SET_ITEM_PROPERTY('BDOC_ENCA.DOC_IND_IMPR_VENDEDOR',ENABLED,PROPERTY_FALSE);*/
    
    ELSE
    
      /* SET_ITEM_PROPERTY('BDOC_ENCA.DOC_IND_IMPR_CONYUGUE',ENABLED,PROPERTY_TRUE);
      SET_ITEM_PROPERTY('BDOC_ENCA.DOC_IND_IMPR_VENDEDOR',ENABLED,PROPERTY_TRUE);
      SET_ITEM_PROPERTY('BDOC_ENCA.DOC_IND_IMPR_CONYUGUE',NAVIGABLE,PROPERTY_TRUE);
      SET_ITEM_PROPERTY('BDOC_ENCA.DOC_IND_IMPR_VENDEDOR',NAVIGABLE,PROPERTY_TRUE);*/
    
      -- SI ES FACTURA CREDITO
      IF P_CONF_IND_CUOTA_AUTO = 'S' THEN
        -- SI SE DEBE GENERAR AUTOMATICAMENTE LAS CUOTAS
        P_IND_CUOTA := 'N';
        --  SET_ITEM_PROPERTY('BPIE.CUOTAS', ENABLED,PROPERTY_FALSE);
      ELSE
        IF P_IND_CUOTA = 'N' THEN
          P_IND_CUOTA := 'S';
          /*   SET_ITEM_PROPERTY('BPIE.CUOTAS', ENABLED,PROPERTY_TRUE);
          SET_ITEM_PROPERTY('BPIE.CUOTAS', NAVIGABLE,PROPERTY_TRUE);*/
        END IF;
      END IF;
      -- SET_ITEM_PROPERTY('BPIE.PAGO',ENABLED,PROPERTY_FALSE);
    END IF;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20002,
                              'PRIMERO DEBE CONFIGURAR EL SISTEMA DE FACTURACION!');
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20002, 'ERROR!');
  END PP_FACTURA;

  PROCEDURE PP_ADICIONAR_ITEM(P_UM                       IN VARCHAR2,
                              P_ARTICULO                 IN NUMBER,
                              P_CANTIDAD                 IN NUMBER,
                              P_PRECIO                   IN NUMBER,
                              P_IMPORTE                  IN NUMBER,
                              P_ART_KG_CONTENIDO         IN NUMBER,
                              P_ART_FACTOR_CONVERSION    IN NUMBER,
                              P_AREM_PRECIO_VTA          IN NUMBER,
                              P_ART_UM                   IN VARCHAR2,
                              P_IMPU_PORCENTAJE          IN NUMBER,
                              P_IMPU_PORC_BASE_IMPONIBLE IN NUMBER,
                              I_CLAVE_CONTRATO           IN NUMBER,
                              V_TOTAL                    IN NUMBER DEFAULT NULL,
                              V_TASA                     IN NUMBER) IS
  
    V_OPERADOR NUMBER;
    V_ESTADO   NUMBER;
    V_IMPORTE  NUMBER;
  
  BEGIN
    IF NOT
        APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => 'ITEMS_FACI039') THEN
      APEX_COLLECTION.CREATE_COLLECTION(P_COLLECTION_NAME => 'ITEMS_FACI039');
    END IF;
  
    IF V('P_EMPRESA') = 1 THEN
    
      SELECT COUNT(*)
        INTO V_OPERADOR
        FROM GEN_OPERADOR_ROL
       WHERE OPRO_EMPR = 1
         AND OPRO_ROL IN (1, 112)
         AND OPRO_OPERADOR =
             (SELECT OPER_CODIGO
                FROM GEN_OPERADOR
               WHERE OPER_LOGIN = GEN_DEVUELVE_USER);
    
      IF NVL(V_TOTAL, 0) * V_TASA + P_IMPORTE * V_TASA > 2000000 AND
         V_OPERADOR > 0 THEN
      
        PP_AUTORIZACION_LIMITE(V('P_EMPRESA'),
                               V('P1500_DOC_CLI'),
                               V('P1500_DOC_MON'),
                               V_ESTADO,
                               V_IMPORTE);
        IF V_ESTADO <> 2 THEN
          RAISE_APPLICATION_ERROR(-20001,
                                  'La autorizacion aun no fue aprobada');
        
        END IF;
      
        IF NVL(V_TOTAL, 0) * V_TASA + P_IMPORTE * V_TASA >
           V_IMPORTE * V_TASA AND V_ESTADO = 2 THEN
          RAISE_APPLICATION_ERROR(-20001,
                                  'El importe supera la autorizacion');
        END IF;
      
      END IF;
    END IF;
  
    BEGIN
      APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'ITEMS_FACI039',
                                 P_C001            => P_UM, --UM VENTA
                                 P_C002            => P_ART_UM, --UM ARTICULO
                                 P_C003            => TO_CHAR(I_CLAVE_CONTRATO),
                                 P_C004            => 'NUEVA',
                                 P_N001            => P_ARTICULO,
                                 P_N002            => P_CANTIDAD,
                                 P_N003            => P_PRECIO,
                                 P_N004            => P_IMPORTE,
                                 P_N005            => P_ART_KG_CONTENIDO,
                                 P_C005            => P_ART_FACTOR_CONVERSION,
                                 P_C006            => P_AREM_PRECIO_VTA,
                                 P_C007            => P_IMPU_PORCENTAJE,
                                 P_C008            => P_IMPU_PORC_BASE_IMPONIBLE);
    END;
  
  END PP_ADICIONAR_ITEM;

  PROCEDURE PP_ADICIONAR_TARJETA(P_TARJ_TARJETA     IN NUMBER,
                                 P_TARJ_NRO_TARJETA IN VARCHAR2,
                                 P_TARJ_FEC_VTO     IN DATE,
                                 P_TARJ_IMP_LOC     IN NUMBER) IS
  BEGIN
    IF NOT
        APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => 'TARJETA_FACI039') THEN
      APEX_COLLECTION.CREATE_COLLECTION(P_COLLECTION_NAME => 'TARJETA_FACI039');
    END IF;
    APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'TARJETA_FACI039',
                               P_C001            => P_TARJ_NRO_TARJETA,
                               P_N001            => P_TARJ_TARJETA,
                               P_N002            => P_TARJ_IMP_LOC,
                               P_D001            => P_TARJ_FEC_VTO);
  
  END PP_ADICIONAR_TARJETA;

  PROCEDURE PP_ADICIONAR_CHEQUE(P_W_CHEQ_BCO          IN NUMBER,
                                P3_CHEQ_SERIE         IN VARCHAR2,
                                P3_CHEQ_NRO           IN VARCHAR2,
                                P3_CHEQ_MON           IN NUMBER,
                                P3_CHEQ_FEC_DEPOSITAR IN DATE,
                                P3_IMPORTE            IN NUMBER,
                                P3_TASA               IN NUMBER,
                                P3_CHEQ_IMPORTE_LOC   IN NUMBER,
                                P3_CHEQ_NRO_CTA_CHEQ  IN VARCHAR2) IS
  BEGIN
    IF NOT
        APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => 'CHEQUE_FACI039') THEN
      APEX_COLLECTION.CREATE_COLLECTION(P_COLLECTION_NAME => 'CHEQUE_FACI039');
    END IF;
    APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'CHEQUE_FACI039',
                               P_C001            => P3_CHEQ_SERIE,
                               P_C002            => P3_CHEQ_NRO,
                               P_C003            => P3_CHEQ_NRO_CTA_CHEQ,
                               P_N001            => P_W_CHEQ_BCO,
                               P_N002            => P3_CHEQ_MON,
                               P_N003            => P3_IMPORTE,
                               P_N004            => P3_TASA,
                               P_N005            => P3_CHEQ_IMPORTE_LOC,
                               P_D001            => P3_CHEQ_FEC_DEPOSITAR);
  
  END PP_ADICIONAR_CHEQUE;

  FUNCTION FP_BUSCAR_ART_COD_NUMERICO(P_EMPRESA                  IN NUMBER,
                                      P_ARTICULO                 IN NUMBER,
                                      P_ART_CODIGO_FABRICA       OUT STK_ARTICULO.ART_CODIGO_FABRICA%TYPE,
                                      P_ART_MARCA                OUT STK_ARTICULO.ART_MARCA%TYPE,
                                      P_ART_IMPU                 OUT STK_ARTICULO.ART_IMPU%TYPE,
                                      P_ART_EST                  OUT STK_ARTICULO.ART_EST%TYPE,
                                      P_ART_UNID_MED             OUT STK_ARTICULO.ART_UNID_MED%TYPE,
                                      P_ART_TIPO_COMISION        OUT STK_ARTICULO.ART_TIPO_COMISION%TYPE,
                                      P_ART_COD_ALFANUMERICO     OUT STK_ARTICULO.ART_COD_ALFANUMERICO%TYPE,
                                      P_ART_FACTOR_CONVERSION    OUT STK_ARTICULO.ART_FACTOR_CONVERSION%TYPE,
                                      P_IMPU_PORCENTAJE          OUT GEN_IMPUESTO.IMPU_PORCENTAJE%TYPE,
                                      P_IMPU_PORC_BASE_IMPONIBLE OUT GEN_IMPUESTO.IMPU_PORC_BASE_IMPONIBLE%TYPE,
                                      P_ART_KG_CONTENIDO         OUT NUMBER, --STK_ARTICULO.ART_KG_CONTENIDO%TYPE,
                                      P_ART_COD_BARRA            OUT STK_ARTICULO.ART_COD_BARRA%TYPE,
                                      P_ENVA_IND_HA              OUT STK_ENVASES.ENVA_IND_HA%TYPE,
                                      P_ART_IND_FACT_NEGATIVO    OUT STK_ARTICULO.ART_IND_FACT_NEGATIVO%TYPE,
                                      P_ART_DESC                 OUT STK_ARTICULO.ART_IND_FACT_NEGATIVO%TYPE)
    RETURN BOOLEAN IS
  BEGIN
  
    --raise_application_error(-20010,'P_ARTICULO: '||P_ARTICULO||';');
  
    SELECT --ART_CODIGO,
     ART_DESC,
     ART_CODIGO_FABRICA,
     ART_MARCA,
     ART_IMPU,
     ART_EST,
     ART_UNID_MED,
     ART_TIPO_COMISION,
     ART_COD_ALFANUMERICO,
     ART_FACTOR_CONVERSION,
     IMPU_PORCENTAJE,
     IMPU_PORC_BASE_IMPONIBLE,
     NVL(ART_KG_UNID, ART_KG_CONTENIDO), --ART_KG_CONTENIDO
     ART_COD_BARRA,
     DECODE(NVL(ENVA_IND_HA, 'N'), 'S', 1, 0),
     ART_IND_FACT_NEGATIVO
      INTO --:BDOC_DET.DET_ART,
           P_ART_DESC,
           P_ART_CODIGO_FABRICA,
           P_ART_MARCA,
           P_ART_IMPU,
           P_ART_EST,
           P_ART_UNID_MED,
           P_ART_TIPO_COMISION,
           P_ART_COD_ALFANUMERICO,
           P_ART_FACTOR_CONVERSION,
           P_IMPU_PORCENTAJE,
           P_IMPU_PORC_BASE_IMPONIBLE,
           P_ART_KG_CONTENIDO,
           P_ART_COD_BARRA,
           P_ENVA_IND_HA,
           P_ART_IND_FACT_NEGATIVO
      FROM STK_ARTICULO, GEN_IMPUESTO, STK_ENVASES
     WHERE ART_CODIGO = P_ARTICULO
       AND ART_IMPU = IMPU_CODIGO
       AND ENVA_LINEA(+) = ART_LINEA
       AND ENVA_CODIGO(+) = ART_ENVASE
       AND ENVA_EMPR(+) = ART_EMPR
       AND ART_EMPR = IMPU_EMPR
       AND ART_EMPR = P_EMPRESA;
  
    --raise_application_error(-20010,'P_ART_KG_CONTENIDO: '||P_ART_KG_CONTENIDO);
  
    --MESSAGE('ART_FACTOR_CONVERSION='||:BDOC_DET.ART_FACTOR_CONVERSION);PAUSE;
  
    RETURN TRUE;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN FALSE;
    WHEN INVALID_NUMBER THEN
      RETURN FALSE;
  END FP_BUSCAR_ART_COD_NUMERICO;

  PROCEDURE PP_BUSCAR_IMPUESTO(P_DOC_OPERADOR             IN NUMBER,
                               P_EMPRESA                  IN NUMBER,
                               P_DET_COD_IVA              IN STK_ARTICULO.ART_IMPU%TYPE,
                               P_IMPU_PORCENTAJE          OUT GEN_IMPUESTO.IMPU_PORCENTAJE%TYPE,
                               P_IMPU_PORC_BASE_IMPONIBLE OUT GEN_IMPUESTO.IMPU_PORC_BASE_IMPONIBLE%TYPE,
                               P_EXPORTACION              IN VARCHAR2) IS
  
    V_IMPU_CODIGO NUMBER;
  
  BEGIN
  
    IF P_DOC_OPERADOR = 1 OR NVL(P_EXPORTACION, 'N') = 'S' THEN
      --NEGRO
      V_IMPU_CODIGO := 1; --EXENTO
    ELSE
      V_IMPU_CODIGO := P_DET_COD_IVA;
    END IF;
  
    SELECT IMPU_PORCENTAJE, IMPU_PORC_BASE_IMPONIBLE
      INTO P_IMPU_PORCENTAJE, P_IMPU_PORC_BASE_IMPONIBLE
      FROM GEN_IMPUESTO
     WHERE IMPU_CODIGO = V_IMPU_CODIGO
       AND IMPU_EMPR = P_EMPRESA;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL ARTICULO DEBE ESTAR SUJETO A UN IMPUESTO !.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002,
                              'EL ARTICULO DEBE ESTAR SUJETO A UN IMPUESTO !.');
  END PP_BUSCAR_IMPUESTO;

  PROCEDURE PP_BUSCAR_OFERTA(P_EMPRESA     IN NUMBER,
                             P_ARTICULO    IN NUMBER,
                             P_DOC_FEC_DOC IN DATE,
                             P_PRECIO      OUT NUMBER) IS
    V_PREC_OFER NUMBER := 0;
  BEGIN
  
    /* BUSCA PRECIO DE OFERTA DEL ARTICULO PARA EL
    ARTICULO DE DETALLE Y LA FECHA DE DOCUMENTO ENTRE
    LA FECHA DE INICIO Y FIN DE LA OFERTA */
  
    SELECT OFER_PRECIO
      INTO V_PREC_OFER
      FROM FAC_OFERTA
     WHERE OFER_ART = P_ARTICULO
       AND P_DOC_FEC_DOC BETWEEN OFER_FEC_INI AND OFER_FEC_FIN
       AND OFER_EMPR = P_EMPRESA;
    --*
    IF V_PREC_OFER IS NOT NULL THEN
      P_PRECIO := V_PREC_OFER;
    END IF;
    --*
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
  END PP_BUSCAR_OFERTA;

  PROCEDURE PP_BUSCAR_PRECIO(P_EMPRESA                   IN NUMBER,
                             P_LIPR_NRO_LISTA_PRECIO     IN NUMBER,
                             P_ARTICULO                  IN NUMBER,
                             P_DOC_MON                   IN NUMBER,
                             P_RECARGO                   IN NUMBER,
                             P_DOC_FEC_DOC               IN DATE,
                             P_IMPU_PORCENTAJE           IN GEN_IMPUESTO.IMPU_PORCENTAJE%TYPE,
                             P_IMPU_PORC_BASE_IMPONIBLE  IN GEN_IMPUESTO.IMPU_PORC_BASE_IMPONIBLE%TYPE,
                             P_ART_UNID_MED              IN VARCHAR2,
                             P_UNIDAD_VTA                IN VARCHAR2,
                             P_CONF_IND_MODIFICAR_PRECIO IN VARCHAR2,
                             P_ART_FACTOR_CONVERSION     IN NUMBER,
                             P_MON_SIMBOLO               IN VARCHAR2,
                             P_AREM_PRECIO_VTA           OUT NUMBER,
                             P_PRECIO                    OUT NUMBER,
                             P_AREM_MON                  OUT NUMBER,
                             P_EXPORTACION               IN VARCHAR2,
                             P_CLAVE_PROF                NUMBER) IS
    --V_MENSAJE        VARCHAR2(200);
    V_PRECIO_CON_IVA NUMBER := 0;
    V_PREC_UNITARIO  NUMBER := 0;
    V_AUX            NUMBER := 0;
  BEGIN
    -- V_MENSAJE := 'EL ARTICULO NO TIENE PRECIO EN LA LISTA ' ||
    --            TO_CHAR(P_LIPR_NRO_LISTA_PRECIO);
  
    IF P_LIPR_NRO_LISTA_PRECIO IS NOT NULL THEN
    
      SELECT LIPR_PRECIO_UNITARIO, LIPE_MON
        INTO P_PRECIO, P_AREM_MON
        FROM FAC_LISTA_PRECIO, FAC_LISTA_PRECIO_DET
       WHERE LIPE_EMPR = LIPR_EMPR
         AND LIPE_NRO_LISTA_PRECIO = LIPR_NRO_LISTA_PRECIO
         AND LIPE_EMPR = P_EMPRESA
         AND LIPE_MON = P_DOC_MON
         AND LIPR_ART = P_ARTICULO
         AND LIPE_NRO_LISTA_PRECIO = P_LIPR_NRO_LISTA_PRECIO;
    
      --BEGIN----------------------------------------------------------------PARA PRUEBAS CAMB USUARIO **************************
      /* SELECT LIPR_PRECIO_UNITARIO, LIPE_MON
       INTO P_PRECIO, P_AREM_MON
       FROM FAC_LISTA_PRECIO      D,
            FAC_LISTA_PRECIO_DET,
            GEN_OPERADOR_LISTA_PR T,
            GEN_OPERADOR          S
      WHERE LIPE_EMPR = LIPR_EMPR
        AND LIPE_NRO_LISTA_PRECIO = LIPR_NRO_LISTA_PRECIO
        AND LIPE_EMPR = P_EMPRESA
        AND LIPE_MON = P_DOC_MON
        AND D.LIPE_NRO_LISTA_PRECIO = T.OPLPR_LISTA_PR
        AND D.LIPE_EMPR = T.OPLPR_EMPRE
        AND T.OPLPR_OPERADOR = S.OPER_CODIGO
        AND S.OPER_LOGIN = GEN_DEVUELVE_USER
        AND LIPR_ART = P_ARTICULO
        AND LIPE_NRO_LISTA_PRECIO = P_LIPR_NRO_LISTA_PRECIO;*/
      /*  EXCEPTION
          WHEN NO_DATA_FOUND THEN
             RAISE_APPLICATION_ERROR(-20002,
                                      'EL OPERADOR NO TIENE ASIGNADO LA LISTA DE PRECIOS!.');
        END;
      */
      P_PRECIO := P_PRECIO + NVL(P_RECARGO, 0);
    
      P_PRECIO := P_PRECIO * (-1);
    
      V_PRECIO_CON_IVA := P_PRECIO * (-1);
    
      V_AUX           := 1;
      V_PREC_UNITARIO := V_PRECIO_CON_IVA / V_AUX;
    
      P_PRECIO := V_PREC_UNITARIO * (-1);
      --
    ELSIF NVL(P_EXPORTACION, 'N') = 'S' THEN
    
      SELECT D.PROD_QUANT / D.PROD_TOTAL_VALUE A
        INTO P_PRECIO
        FROM FAC_PROFORMA_DET D
       WHERE D.PROD_CLAVE_DET = P_CLAVE_PROF
         AND D.PROD_EMPR = P_EMPRESA
         AND D.PROD_ART = P_ARTICULO;
    
    ELSE
    
      SELECT AREM_PRECIO_VTA, AREM_MON
        INTO P_PRECIO, P_AREM_MON
        FROM STK_ARTICULO_EMPRESA
       WHERE AREM_EMPR = P_EMPRESA
         AND AREM_ART = P_ARTICULO;
    
      P_PRECIO := P_PRECIO + P_RECARGO;
    
      PP_BUSCAR_OFERTA(P_EMPRESA     => P_EMPRESA,
                       P_ARTICULO    => P_ARTICULO,
                       P_DOC_FEC_DOC => P_DOC_FEC_DOC,
                       P_PRECIO      => P_PRECIO);
    END IF;
  
    IF P_ART_UNID_MED = 'KG' AND P_UNIDAD_VTA = 'U' THEN
      IF NVL(P_ART_FACTOR_CONVERSION, 0) = 0 THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'FACTOR DE CONVERSION = 0, NO SE PUEDE REALIZAR LA CONVERSION!.',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        RAISE_APPLICATION_ERROR(-20002,
                                'FACTOR DE CONVERSION = 0, NO SE PUEDE REALIZAR LA CONVERSION!.');
      ELSE
        P_AREM_PRECIO_VTA := P_PRECIO / P_ART_FACTOR_CONVERSION;
      END IF;
    ELSE
      P_AREM_PRECIO_VTA := P_PRECIO;
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      IF P_LIPR_NRO_LISTA_PRECIO IS NOT NULL THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL ARTICULO NO TIENE PRECIO EN LA LISTA ' ||
                                                   TO_CHAR(P_LIPR_NRO_LISTA_PRECIO) ||
                                                   ', EN ESA MONEDA.',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      
        IF P_CONF_IND_MODIFICAR_PRECIO = 'N' THEN
          APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'ARTICULO SIN LISTA DE PRECIO',
                               P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
          RAISE_APPLICATION_ERROR(-20002, 'ARTICULO SIN LISTA DE PRECIO');
        END IF;
      ELSE
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'ARTICULO NO ESTA HABILITADO EN ESTA MONEDA ' ||
                                                   P_MON_SIMBOLO || '!.',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        RAISE_APPLICATION_ERROR(-20002,
                                'ARTICULO NO ESTA HABILITADO EN ESTA MONEDA ' ||
                                P_MON_SIMBOLO || '!.');
      END IF;
    
  END PP_BUSCAR_PRECIO;

  PROCEDURE PP_CALCULAR_PRECIO_UNITARIO(P_MON_US                IN NUMBER,
                                        P_MON_LOC               IN NUMBER,
                                        P_DOC_MON               IN NUMBER,
                                        P_AREM_MON              IN NUMBER,
                                        P_DOC_TASA_US           IN NUMBER,
                                        P_UNIDAD_VTA            IN VARCHAR2,
                                        P_ART_UNID_MED          IN VARCHAR2,
                                        P_ART_FACTOR_CONVERSION IN STK_ARTICULO.ART_FACTOR_CONVERSION%TYPE,
                                        P_PRECIO                IN OUT NUMBER,
                                        P_AREM_PRECIO_VTA       IN OUT NUMBER) IS
  BEGIN
    IF P_DOC_MON = P_MON_US THEN
      -- SI DOCUMENTO ES EN U$
      IF P_AREM_MON = P_MON_LOC THEN
        -- SI EL PRECIO ESTA EN GS. CONVERTIRLO A U$
        P_PRECIO := ROUND((P_PRECIO / P_DOC_TASA_US), 2);
      END IF;
    ELSE
      -- SI DOCUMENTO ES EN GS.
      IF P_AREM_MON = P_MON_US THEN
        -- SI EL PRECIO ESTA EN U$. CONVERTIRLO A GS.
        P_PRECIO := ROUND((P_PRECIO * P_DOC_TASA_US), 2);
      END IF;
    END IF;
    IF P_UNIDAD_VTA = 'U' AND P_ART_UNID_MED = 'KG' THEN
      P_AREM_PRECIO_VTA := P_PRECIO / NVL(P_ART_FACTOR_CONVERSION, 0);
    ELSE
      P_AREM_PRECIO_VTA := P_PRECIO;
    END IF;
  END PP_CALCULAR_PRECIO_UNITARIO;

  FUNCTION PP_BUSCAR_PED_SUC_IMP(P_SUCURSAL IN NUMBER,
                                 P_EMPRESA  IN NUMBER,
                                 P_DEPOSITO IN NUMBER,
                                 P_ARTICULO IN NUMBER) RETURN NUMBER IS
    V_SALDO_PED_SUC_IMP NUMBER;
  BEGIN
    SELECT NVL(SUM(DPED_CANT_PED - DPED_CANT_REC), 0)
      INTO V_SALDO_PED_SUC_IMP
      FROM STK_PED_SUC_IMP, STK_PED_SUC_IMP_DET
     WHERE PED_NRO_PED = DPED_NRO_PED
       AND PED_EMPR = DPED_EMPR
       AND PED_EMPR = P_EMPRESA
       AND PED_SUC_PED = P_SUCURSAL
       AND PED_DEP_PED = P_DEPOSITO
       AND DPED_ART = P_ARTICULO;
  
    RETURN V_SALDO_PED_SUC_IMP;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 0;
  END;

  PROCEDURE PP_VALIDAR_ITEM(I_DET_CANT_PEDIDO       IN NUMBER, --NULL
                            I_DET_CANT              IN OUT NUMBER,
                            I_UNIDAD_VTA            IN VARCHAR2,
                            I_ART_FACTOR_CONVERSION IN NUMBER,
                            O_DET_CANT_GUARDADA     OUT NUMBER,
                            I_ART_IND_FACT_NEGATIVO IN VARCHAR2,
                            P_SUCURSAL              IN NUMBER,
                            P_EMPRESA               IN NUMBER,
                            P_DEPOSITO              IN NUMBER,
                            P_ARTICULO              IN NUMBER,
                            I_CONF_FACT_CERO        IN VARCHAR2,
                            I_W_IND_LISTVAL         IN VARCHAR2,
                            I_DET_NRO_REMIS         IN NUMBER,
                            O_CONFIRMAR             OUT VARCHAR2) IS
    V_MAX_CANT_FACT     NUMBER;
    V_MESSAGE           VARCHAR2(200);
    V_ARDE_CANT_ACT     NUMBER;
    V_ARDE_UBIC         NUMBER;
    V_SALDO_PED_SUC_IMP NUMBER;
    V_CANT_REM          NUMBER;
  BEGIN
    BEGIN
      --PP_BUSCAR_EXISTENCIA
      SELECT NVL(SUM(ARDE_CANT_ACT), 0)
        INTO V_ARDE_CANT_ACT
        FROM STK_ARTICULO_DEPOSITO
       WHERE ARDE_EMPR = P_EMPRESA
         AND ARDE_SUC = P_SUCURSAL
         AND ARDE_DEP = P_DEPOSITO
         AND ARDE_ART = P_ARTICULO;
    
      SELECT NVL(ARDE_UBIC, 0)
        INTO V_ARDE_UBIC
        FROM STK_ARTICULO_DEPOSITO
       WHERE ARDE_EMPR = P_EMPRESA
         AND ARDE_SUC = P_SUCURSAL
         AND ARDE_DEP = P_DEPOSITO
         AND ARDE_ART = P_ARTICULO;
      --
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_ARDE_CANT_ACT := 0;
    END; --PP_BUSCAR_EXISTENCIA
  
    V_SALDO_PED_SUC_IMP := PP_BUSCAR_PED_SUC_IMP(P_SUCURSAL => P_SUCURSAL,
                                                 P_EMPRESA  => P_EMPRESA,
                                                 P_DEPOSITO => P_DEPOSITO,
                                                 P_ARTICULO => P_ARTICULO);
  
    BEGIN
      --PP_BUSCAR_CANT_REMITIDO
      SELECT NVL(SUM(DETR_CANT_REM - DETR_CANT_FACT), 0)
        INTO V_CANT_REM
        FROM STK_REMISION, STK_REMISION_DET
       WHERE REM_EMPR = DETR_EMPR
         AND REM_NRO = DETR_REM
         AND REM_EMPR = P_EMPRESA
         AND REM_SUC = P_SUCURSAL
         AND REM_DEP = P_DEPOSITO
         AND DETR_ART = P_ARTICULO;
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_CANT_REM := 0;
    END; --PP_BUSCAR_CANT_REMITIDO
  
    V_MAX_CANT_FACT := (V_ARDE_CANT_ACT --CANTIDAD MAXIMA A FACTURAR
                       + NVL(V_SALDO_PED_SUC_IMP, 0) - NVL(V_CANT_REM, 0));
    -- V_MESSAGE  := 'CANTIDAD A FACTURAR DEL ARTICULO '||I_ART||' - '||I_ART_DESC||' SUPERA EXISTENCIA EN EL DEPOSITO: '||TO_CHAR(V_MAX_CANT_FACT);
    V_MESSAGE := 'CANTIDAD A FACTURAR DEL ARTICULO SUPERA EXISTENCIA EN EL DEPOSITO: ' ||
                 TO_CHAR(V_MAX_CANT_FACT);
  
    IF NVL(I_DET_CANT_PEDIDO, 0) > 0 AND I_DET_CANT > I_DET_CANT_PEDIDO THEN
      RAISE_APPLICATION_ERROR(-20002,
                              'ESTA CANTIDAD CORRESPONDE A UN PEDIDO PENDIENTE, Y NO PUEDE SER SUPERIOR A: ' ||
                              I_DET_CANT_PEDIDO);
    END IF;
  
    IF I_DET_CANT IS NULL THEN
      I_DET_CANT := 1;
    END IF;
  
    IF I_UNIDAD_VTA = 'U' THEN
      O_DET_CANT_GUARDADA := I_DET_CANT * NVL(I_ART_FACTOR_CONVERSION, 0);
    ELSE
      O_DET_CANT_GUARDADA := I_DET_CANT;
    END IF;
  
    IF I_W_IND_LISTVAL <> 'S' THEN
      IF NVL(I_DET_NRO_REMIS, 0) <> 0 THEN
        --PP_VALIDAR_REMI_DET;
        NULL;
      ELSE
        IF I_DET_CANT > V_MAX_CANT_FACT THEN
          IF I_CONF_FACT_CERO = 'N' AND P_SUCURSAL = 4 THEN
            RAISE_APPLICATION_ERROR(-20002, V_MESSAGE);
          ELSE
            IF NVL(I_ART_IND_FACT_NEGATIVO, 'S') = 'N' AND P_SUCURSAL = 4 THEN
              --SOLO LOMA PLATA
              RAISE_APPLICATION_ERROR(-20002, V_MESSAGE);
            ELSE
              O_CONFIRMAR := V_MESSAGE;
              RAISE_APPLICATION_ERROR(-20002, V_MESSAGE);
            END IF;
          END IF;
        END IF;
      END IF;
    END IF;
    O_CONFIRMAR := NULL;
  END PP_VALIDAR_ITEM;

  PROCEDURE PP_CONTROL_ARTICULO(P_ARTICULO                  IN NUMBER,
                                P_EMPRESA                   IN NUMBER,
                                P_DOC_OPERADOR              IN NUMBER,
                                P_DOC_MON                   IN NUMBER,
                                P_RECARGO                   IN NUMBER,
                                P_DOC_FEC_DOC               IN DATE,
                                P_LIPR_NRO_LISTA_PRECIO     IN NUMBER,
                                P_UNIDAD_VTA                IN VARCHAR2,
                                P_CONF_IND_MODIFICAR_PRECIO IN VARCHAR2,
                                P_MON_SIMBOLO               IN VARCHAR2,
                                P_MON_US                    IN NUMBER,
                                P_MON_LOC                   IN NUMBER,
                                P_DOC_TASA_US               IN NUMBER,
                                P_SUCURSAL                  IN NUMBER,
                                P_DEPOSITO                  IN NUMBER,
                                P_ART_CODIGO_FABRICA        OUT STK_ARTICULO.ART_CODIGO_FABRICA%TYPE,
                                P_ART_MARCA                 OUT STK_ARTICULO.ART_MARCA%TYPE,
                                P_ART_IMPU                  OUT STK_ARTICULO.ART_IMPU%TYPE,
                                P_ART_EST                   OUT STK_ARTICULO.ART_EST%TYPE,
                                P_ART_UNID_MED              OUT STK_ARTICULO.ART_UNID_MED%TYPE,
                                P_ART_TIPO_COMISION         OUT STK_ARTICULO.ART_TIPO_COMISION%TYPE,
                                P_ART_COD_ALFANUMERICO      OUT STK_ARTICULO.ART_COD_ALFANUMERICO%TYPE,
                                P_ART_FACTOR_CONVERSION     OUT STK_ARTICULO.ART_FACTOR_CONVERSION%TYPE,
                                P_IMPU_PORCENTAJE           OUT GEN_IMPUESTO.IMPU_PORCENTAJE%TYPE,
                                P_IMPU_PORC_BASE_IMPONIBLE  OUT GEN_IMPUESTO.IMPU_PORC_BASE_IMPONIBLE%TYPE,
                                P_ART_KG_CONTENIDO          OUT NUMBER, --STK_ARTICULO.ART_KG_CONTENIDO%TYPE,
                                P_ART_COD_BARRA             OUT STK_ARTICULO.ART_COD_BARRA%TYPE,
                                P_ENVA_IND_HA               OUT STK_ENVASES.ENVA_IND_HA%TYPE,
                                P_ART_IND_FACT_NEGATIVO     OUT STK_ARTICULO.ART_IND_FACT_NEGATIVO%TYPE,
                                P_ART_CONCEPTO              OUT FIN_CONCEPTO.FCON_CLAVE%TYPE,
                                P_ART_CTA_CONTABLE          OUT STK_CLASIFICACION.CLAS_CTACO_VENTA%TYPE,
                                P_CANT_REM                  OUT NUMBER,
                                P_IMPU_CODIGO               OUT NUMBER,
                                P_AREM_PRECIO_VTA           OUT NUMBER,
                                P_PRECIO                    OUT NUMBER,
                                P_ADICIONAR                 OUT NUMBER,
                                P_ART_DESC                  OUT STK_ARTICULO.ART_DESC%TYPE,
                                P_EXPORTACION               IN VARCHAR2,
                                P_CLAVE_PROF                NUMBER) IS
  
    V_ART_CODIGO_FABRICA       STK_ARTICULO.ART_CODIGO_FABRICA%TYPE;
    V_ART_MARCA                STK_ARTICULO.ART_MARCA%TYPE;
    V_ART_IMPU                 STK_ARTICULO.ART_IMPU%TYPE;
    V_ART_EST                  STK_ARTICULO.ART_EST%TYPE;
    V_ART_UNID_MED             STK_ARTICULO.ART_UNID_MED%TYPE;
    V_ART_TIPO_COMISION        STK_ARTICULO.ART_TIPO_COMISION%TYPE;
    V_ART_COD_ALFANUMERICO     STK_ARTICULO.ART_COD_ALFANUMERICO%TYPE;
    V_ART_FACTOR_CONVERSION    STK_ARTICULO.ART_FACTOR_CONVERSION%TYPE;
    V_IMPU_PORCENTAJE          GEN_IMPUESTO.IMPU_PORCENTAJE%TYPE;
    V_IMPU_PORC_BASE_IMPONIBLE GEN_IMPUESTO.IMPU_PORC_BASE_IMPONIBLE%TYPE;
    V_ART_KG_CONTENIDO         NUMBER; --STK_ARTICULO.ART_KG_CONTENIDO%TYPE;
    V_ART_COD_BARRA            STK_ARTICULO.ART_COD_BARRA%TYPE;
    V_ENVA_IND_HA              STK_ENVASES.ENVA_IND_HA%TYPE;
    V_ART_IND_FACT_NEGATIVO    STK_ARTICULO.ART_IND_FACT_NEGATIVO%TYPE;
    V_AREM_PRECIO_VTA          NUMBER;
    V_PRECIO                   NUMBER;
    V_AREM_MON                 NUMBER;
    V_SALDO_PED_SUC_IMP        NUMBER;
    V_CLAVE_CONCEPTO           NUMBER;
    V_ART_CONCEPTO             FIN_CONCEPTO.FCON_CLAVE%TYPE;
    V_ART_CTA_CONTABLE         STK_CLASIFICACION.CLAS_CTACO_VENTA%TYPE;
    V_CANT_REM                 NUMBER;
    V_IMPU_CODIGO              NUMBER;
    V_ART_DESC                 STK_ARTICULO.ART_DESC%TYPE;
  BEGIN
    -----
    P_ADICIONAR := 0;
    IF P_ARTICULO IS NULL THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'CODIGO NO PUEDE SER NULO!',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002, 'CODIGO NO PUEDE SER NULO!');
    END IF;
  
    IF FP_BUSCAR_ART_COD_NUMERICO(P_EMPRESA                  => P_EMPRESA,
                                  P_ARTICULO                 => P_ARTICULO,
                                  P_ART_CODIGO_FABRICA       => V_ART_CODIGO_FABRICA,
                                  P_ART_MARCA                => V_ART_MARCA,
                                  P_ART_IMPU                 => V_ART_IMPU,
                                  P_ART_EST                  => V_ART_EST,
                                  P_ART_UNID_MED             => V_ART_UNID_MED,
                                  P_ART_TIPO_COMISION        => V_ART_TIPO_COMISION,
                                  P_ART_COD_ALFANUMERICO     => V_ART_COD_ALFANUMERICO,
                                  P_ART_FACTOR_CONVERSION    => V_ART_FACTOR_CONVERSION,
                                  P_IMPU_PORCENTAJE          => V_IMPU_PORCENTAJE,
                                  P_IMPU_PORC_BASE_IMPONIBLE => V_IMPU_PORC_BASE_IMPONIBLE,
                                  P_ART_KG_CONTENIDO         => V_ART_KG_CONTENIDO,
                                  P_ART_COD_BARRA            => V_ART_COD_BARRA,
                                  P_ENVA_IND_HA              => V_ENVA_IND_HA,
                                  P_ART_IND_FACT_NEGATIVO    => V_ART_IND_FACT_NEGATIVO,
                                  P_ART_DESC                 => V_ART_DESC) THEN
    
      PP_BUSCAR_IMPUESTO(P_DOC_OPERADOR             => P_DOC_OPERADOR,
                         P_EMPRESA                  => P_EMPRESA,
                         P_DET_COD_IVA              => V_ART_IMPU,
                         P_IMPU_PORCENTAJE          => V_IMPU_PORCENTAJE,
                         P_IMPU_PORC_BASE_IMPONIBLE => V_IMPU_PORC_BASE_IMPONIBLE,
                         P_EXPORTACION              => P_EXPORTACION);
    
      PP_BUSCAR_PRECIO(P_EMPRESA                   => P_EMPRESA,
                       P_LIPR_NRO_LISTA_PRECIO     => P_LIPR_NRO_LISTA_PRECIO,
                       P_ARTICULO                  => P_ARTICULO,
                       P_DOC_MON                   => P_DOC_MON,
                       P_RECARGO                   => P_RECARGO,
                       P_DOC_FEC_DOC               => P_DOC_FEC_DOC,
                       P_IMPU_PORCENTAJE           => V_IMPU_PORCENTAJE,
                       P_IMPU_PORC_BASE_IMPONIBLE  => V_IMPU_PORC_BASE_IMPONIBLE,
                       P_ART_UNID_MED              => V_ART_UNID_MED,
                       P_UNIDAD_VTA                => P_UNIDAD_VTA,
                       P_CONF_IND_MODIFICAR_PRECIO => P_CONF_IND_MODIFICAR_PRECIO,
                       P_ART_FACTOR_CONVERSION     => V_ART_FACTOR_CONVERSION,
                       P_MON_SIMBOLO               => P_MON_SIMBOLO,
                       P_AREM_PRECIO_VTA           => V_AREM_PRECIO_VTA,
                       P_PRECIO                    => V_PRECIO,
                       P_AREM_MON                  => V_AREM_MON,
                       P_EXPORTACION               => P_EXPORTACION,
                       P_CLAVE_PROF                => P_CLAVE_PROF);
    
      IF V_AREM_MON NOT IN (P_MON_US, P_MON_LOC) THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL PRECIO DE UN ARTICULO SOLO PUEDE SER EN MONEDA LOCAL O US$',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        RAISE_APPLICATION_ERROR(-20002,
                                'EL PRECIO DE UN ARTICULO SOLO PUEDE SER EN MONEDA LOCAL O US$');
      END IF;
    
      BEGIN
        --PP_BUSCAR_PED_SUC_IMP
        SELECT SUM(DPED_CANT_PED - DPED_CANT_REC)
          INTO V_SALDO_PED_SUC_IMP
          FROM STK_PED_SUC_IMP, STK_PED_SUC_IMP_DET
         WHERE PED_NRO_PED = DPED_NRO_PED
           AND PED_EMPR = DPED_EMPR
           AND PED_EMPR = P_EMPRESA
           AND PED_SUC_PED = P_SUCURSAL
           AND PED_DEP_PED = P_DEPOSITO
           AND DPED_ART = P_ARTICULO;
      
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
        
          V_SALDO_PED_SUC_IMP := 0;
      END; --PP_BUSCAR_PED_SUC_IMP
    
      BEGIN
        --PP_TRAER_CTACO_ARTICULO
        --------POR SI LA CUENTA CONTABLE TIENE ASIGNADO MAS DE UN CONCEPTO DE DEBITO LEER DE LA TABLA CLASIFICACION
        BEGIN
          SELECT CLAS_CONC_ARTICULO
            INTO V_CLAVE_CONCEPTO
            FROM STK_CLASIFICACION A, STK_ARTICULO B
           WHERE CLAS_CODIGO = ART_CLASIFICACION
             AND CLAS_EMPR = ART_EMPR
             AND ART_CODIGO = P_ARTICULO
             AND ART_EMPR = P_EMPRESA;
        
        EXCEPTION
          WHEN OTHERS THEN
            NULL;
        END;
      
        SELECT FC.FCON_CLAVE, CLAS_CTACO_VENTA
          INTO V_ART_CONCEPTO, V_ART_CTA_CONTABLE
          FROM STK_CLASIFICACION C, STK_ARTICULO A, FIN_CONCEPTO FC
         WHERE C.CLAS_CODIGO = A.ART_CLASIFICACION
           AND C.CLAS_CTACO_VENTA = FC.FCON_CLAVE_CTACO(+)
           AND CLAS_EMPR = ART_EMPR
           AND C.CLAS_EMPR = FCON_EMPR(+)
           AND ART_EMPR = P_EMPRESA
           AND FCON_TIPO_SALDO = 'D'
           AND (FCON_CLAVE = V_CLAVE_CONCEPTO OR V_CLAVE_CONCEPTO IS NULL)
           AND A.ART_CODIGO = P_ARTICULO;
        IF V_ART_CTA_CONTABLE IS NULL THEN
          APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'DEBE ASIGNAR UNA CTACONTABLE PARA EL ARTICULO!.',
                               P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
          RAISE_APPLICATION_ERROR(-20002,
                                  'DEBE ASIGNAR UNA CTACONTABLE PARA EL ARTICULO!.');
        END IF;
        --PL_EXHIBIR_MENSAJE(:BDOC_DET.ART_CONCEPTO||'-'||:BDOC_DET.ART_CTA_CONTABLE);
      EXCEPTION
        WHEN TOO_MANY_ROWS THEN
          APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EXISTE MAS DE UN CONCEPTO ASOCIADO A LA CUENTA CONTABLE DEL ARTICULO!.',
                               P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
          RAISE_APPLICATION_ERROR(-20002,
                                  'EXISTE MAS DE UN CONCEPTO ASOCIADO A LA CUENTA CONTABLE DEL ARTICULO!.');
        WHEN NO_DATA_FOUND THEN
          APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'NO EXISTE CTA CONTABLE PARA MERCADERIA O EL ARTICULO NO TIENE ASIGNADO CTAS CONTABLES!.',
                               P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
          RAISE_APPLICATION_ERROR(-20002,
                                  'NO EXISTE CTA CONTABLE PARA MERCADERIA O EL ARTICULO NO TIENE ASIGNADO CTAS CONTABLES!.');
      END; --PP_TRAER_CTACO_ARTICULO
    
      BEGIN
        --PP_BUSCAR_CANT_REMITIDO
        SELECT SUM(DETR_CANT_REM - DETR_CANT_FACT)
          INTO V_CANT_REM
          FROM STK_REMISION, STK_REMISION_DET
         WHERE REM_EMPR = DETR_EMPR
           AND REM_NRO = DETR_REM
           AND REM_EMPR = P_EMPRESA
           AND REM_SUC = P_SUCURSAL
           AND REM_DEP = P_DEPOSITO
           AND DETR_ART = P_ARTICULO;
      
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          V_CANT_REM := 0;
      END; --PP_BUSCAR_CANT_REMITIDO
    
      BEGIN
        --PP_BUSCAR_IMPUESTO
        IF P_DOC_OPERADOR = 1 THEN
          --NEGRO
          V_IMPU_CODIGO := 1; --EXENTO
        ELSE
          V_IMPU_CODIGO := V_ART_IMPU;
        END IF;
      
        SELECT IMPU_PORCENTAJE, IMPU_PORC_BASE_IMPONIBLE
          INTO V_IMPU_PORCENTAJE, V_IMPU_PORC_BASE_IMPONIBLE
          FROM GEN_IMPUESTO
         WHERE IMPU_CODIGO = V_IMPU_CODIGO
           AND IMPU_EMPR = P_EMPRESA;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL ARTICULO DEBE ESTAR SUJETO A UN IMPUESTO !.',
                               P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
          RAISE_APPLICATION_ERROR(-20002,
                                  'EL ARTICULO DEBE ESTAR SUJETO A UN IMPUESTO !.');
      END; --PP_BUSCAR_IMPUESTO
    ELSE
      P_ADICIONAR := 0;
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'NO SE ENCONTRO EL ARTICULO',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002, 'NO SE ENCONTRO EL ARTICULO');
    END IF;
  
    --RAISE_APPLICATION_ERROR(-20010, 'V_ART_KG_CONTENIDO: '||V_ART_KG_CONTENIDO);
  
    P_ART_CODIGO_FABRICA       := P_ART_CODIGO_FABRICA;
    P_ART_MARCA                := V_ART_MARCA;
    P_ART_IMPU                 := V_ART_IMPU;
    P_ART_EST                  := V_ART_EST;
    P_ART_UNID_MED             := V_ART_UNID_MED;
    P_ART_TIPO_COMISION        := V_ART_TIPO_COMISION;
    P_ART_COD_ALFANUMERICO     := V_ART_COD_ALFANUMERICO;
    P_ART_FACTOR_CONVERSION    := V_ART_FACTOR_CONVERSION;
    P_IMPU_PORCENTAJE          := V_IMPU_PORCENTAJE;
    P_IMPU_PORC_BASE_IMPONIBLE := V_IMPU_PORC_BASE_IMPONIBLE;
    P_ART_KG_CONTENIDO         := V_ART_KG_CONTENIDO;
    P_ART_COD_BARRA            := V_ART_COD_BARRA;
    P_ENVA_IND_HA              := V_ENVA_IND_HA;
    P_ART_IND_FACT_NEGATIVO    := V_ART_IND_FACT_NEGATIVO;
    P_ART_CONCEPTO             := V_ART_CONCEPTO;
    P_ART_CTA_CONTABLE         := V_ART_CTA_CONTABLE;
    P_CANT_REM                 := V_CANT_REM;
    P_IMPU_CODIGO              := V_IMPU_CODIGO;
    P_AREM_PRECIO_VTA          := V_AREM_PRECIO_VTA;
    P_PRECIO                   := V_PRECIO;
    P_ADICIONAR                := 1;
    P_ART_DESC                 := V_ART_DESC;
  
  END PP_CONTROL_ARTICULO;

  PROCEDURE PP_RECALCULAR_DETALLES(I_UNIDAD_VTA               IN VARCHAR2,
                                   I_ART_UNID_MED             IN VARCHAR2,
                                   I_ART_FACTOR_CONVERSION    IN NUMBER,
                                   I_PRECIO                   IN NUMBER,
                                   I_DET_CANT                 IN NUMBER,
                                   I_AREM_PRECIO_VTA          IN NUMBER,
                                   I_PORC_DTO                 IN NUMBER,
                                   I_IMPU_PORCENTAJE          IN NUMBER,
                                   I_W_CLI_PORC_EXEN_IVA      IN NUMBER,
                                   I_IMPU_PORC_BASE_IMPONIBLE IN NUMBER,
                                   I_MON_LOC                  IN NUMBER,
                                   I_DOC_TASA_US              IN NUMBER,
                                   I_DOC_MON                  IN NUMBER,
                                   I_TIPO_FACTURA             IN NUMBER,
                                   I_PORC_IVA                 IN NUMBER,
                                   I_MON_DEC_IMP              IN NUMBER,
                                   O_DET_BRUTO_LOC            OUT NUMBER,
                                   O_DET_BRUTO_MON            OUT NUMBER,
                                   O_DET_NETO_LOC             OUT NUMBER,
                                   O_DET_NETO_MON             OUT NUMBER,
                                   O_DET_IVA_LOC              OUT NUMBER,
                                   O_DET_IVA_MON              OUT NUMBER,
                                   O_TOTAL                    OUT NUMBER,
                                   O_BRUTO_EXEN_PRECIO_MON    OUT NUMBER,
                                   O_BRUTO_GRAV_PRECIO_MON    OUT NUMBER,
                                   O_BRUTO_EXEN_MON           OUT NUMBER,
                                   O_BRUTO_GRAV_MON           OUT NUMBER,
                                   O_BRUTO_EXEN_LOC           OUT NUMBER,
                                   O_BRUTO_GRAV_LOC           OUT NUMBER,
                                   O_IVA_MON                  OUT NUMBER,
                                   O_IVA_LOC                  OUT NUMBER,
                                   O_NETO_EXEN_LOC            OUT NUMBER,
                                   O_NETO_EXEN_MON            OUT NUMBER,
                                   O_NETO_GRAV_LOC            OUT NUMBER,
                                   O_NETO_GRAV_MON            OUT NUMBER) IS
    V_BRUTO_LISTA      NUMBER := 0; -- CANTIDAD POR PRECIO UNITARIO DE LISTA
    V_BRUTO            NUMBER := 0; -- CANTIDAD POR PRECIO UNITARIO INGRESADO
    V_BRUTO_GRAV_LISTA NUMBER := 0; -- BRUTO GRAVADO SEGUN PRECIO DE LISTA
    V_BRUTO_EXEN_LISTA NUMBER := 0; -- BRUTO EXENTO  SEGUN PRECIO DE LISTA
    V_BRUTO_GRAV       NUMBER := 0; -- BRUTO GRAVADO SEGUN PRECIO INGRESADO
    V_BRUTO_EXEN       NUMBER := 0; -- BRUTO EXENTO  SEGUN PRECIO INGRESADO
    V_VAL_DTO          NUMBER := 0; -- V_BRUTO POR EL PORCENTAJE DE DESCUENTO
    V_NETO             NUMBER := 0; -- V_BRUTO MENOS V_VAL_DTO
    V_NETO_GRAV        NUMBER := 0; -- V_BRUTO MENOS V_VAL_DTO
    V_NETO_EXEN        NUMBER := 0; -- V_BRUTO MENOS V_VAL_DTO
    V_IVA              NUMBER := 0; -- V_NETO POR PORCENTAJE DE IVA
    --V_IVA_LOC          NUMBER := 0; -- IVA EN MONEDA LOCAL
    V_TOTAL           NUMBER := 0; -- V_NETO MAS V_IVA
    V_CANT_DECI       NUMBER := 0;
    V_PRECIO          NUMBER := 0;
    V_AREM_PRECIO_VTA NUMBER := 0;
    VI_PRECIO         NUMBER := 0;
    V_TOTAL_CON_IVA   NUMBER := 0;
    V_TOTAL_SIN_IVA   NUMBER := 0;
    V_AUX             NUMBER := 0;
  BEGIN
  
    V_CANT_DECI := NVL(I_MON_DEC_IMP, 0); --CANTIDAD DE DECIMALES
    IF I_AREM_PRECIO_VTA < 0 THEN
      VI_PRECIO         := I_AREM_PRECIO_VTA * (-1);
      V_AREM_PRECIO_VTA := I_AREM_PRECIO_VTA * (-1);
    ELSE
      V_AREM_PRECIO_VTA := I_PRECIO;
      VI_PRECIO         := I_PRECIO;
    END IF;
  
    IF I_UNIDAD_VTA = 'U' AND I_ART_UNID_MED = 'KG' THEN
      V_PRECIO      := VI_PRECIO / NVL(I_ART_FACTOR_CONVERSION, 0);
      V_BRUTO_LISTA := ROUND((NVL(I_DET_CANT, 0) *
                             NVL(I_ART_FACTOR_CONVERSION, 0)) *
                             NVL(V_AREM_PRECIO_VTA, 0),
                             V_CANT_DECI);
      V_BRUTO       := ROUND((NVL(I_DET_CANT, 0) *
                             NVL(I_ART_FACTOR_CONVERSION, 0)) *
                             NVL(V_PRECIO, 0),
                             V_CANT_DECI);
    ELSE
      V_PRECIO      := VI_PRECIO;
      V_BRUTO_LISTA := ROUND((NVL(I_DET_CANT, 0)) *
                             NVL(V_AREM_PRECIO_VTA, 0),
                             V_CANT_DECI);
      V_BRUTO       := ROUND((NVL(I_DET_CANT, 0)) * NVL(V_PRECIO, 0),
                             V_CANT_DECI);
    END IF;
  
    V_VAL_DTO := ROUND(((V_BRUTO * NVL(I_PORC_DTO, 0)) / 100), V_CANT_DECI);
    V_NETO    := NVL(V_BRUTO, 0) - NVL(V_VAL_DTO, 0);
  
    IF I_AREM_PRECIO_VTA < 0 THEN
      V_TOTAL_CON_IVA := V_NETO;
      V_AUX           := (1 + ((I_IMPU_PORC_BASE_IMPONIBLE / 100) *
                         (I_IMPU_PORCENTAJE / 100)));
      V_TOTAL_SIN_IVA := (V_TOTAL_CON_IVA / V_AUX);
    
      V_IVA := ROUND((V_TOTAL_CON_IVA - V_TOTAL_SIN_IVA), V_CANT_DECI);
    ELSE
      V_IVA  := ROUND(((V_NETO * I_IMPU_PORCENTAJE /*:PARAMETER.P_PORC_IVA*/
                      ) / 100),
                      V_CANT_DECI);
      V_NETO := V_NETO + V_IVA;
    END IF;
  
    IF NVL(I_IMPU_PORCENTAJE, 0) > 0 THEN
      IF I_W_CLI_PORC_EXEN_IVA IS NOT NULL THEN
        V_BRUTO_EXEN_LISTA := ROUND(((V_BRUTO_LISTA * I_W_CLI_PORC_EXEN_IVA) / 100),
                                    V_CANT_DECI);
        V_BRUTO_GRAV_LISTA := V_BRUTO_LISTA - V_BRUTO_EXEN_LISTA;
        V_BRUTO_EXEN       := ROUND(((V_BRUTO * I_W_CLI_PORC_EXEN_IVA) / 100),
                                    V_CANT_DECI);
        V_BRUTO_GRAV       := V_BRUTO - V_BRUTO_EXEN;
        V_NETO_EXEN        := ROUND(((V_NETO * I_W_CLI_PORC_EXEN_IVA) / 100),
                                    V_CANT_DECI);
        V_NETO_GRAV        := V_NETO - V_NETO_EXEN;
      ELSE
        V_BRUTO_EXEN_LISTA := V_BRUTO_LISTA -
                              ROUND((V_BRUTO_LISTA *
                                    (I_IMPU_PORC_BASE_IMPONIBLE / 100)),
                                    V_CANT_DECI);
        V_BRUTO_GRAV_LISTA := V_BRUTO_LISTA - V_BRUTO_EXEN_LISTA - V_IVA;
        V_BRUTO_EXEN       := V_BRUTO - ROUND((V_BRUTO *
                                              (I_IMPU_PORC_BASE_IMPONIBLE / 100)),
                                              V_CANT_DECI);
        V_BRUTO_GRAV       := V_BRUTO - V_BRUTO_EXEN - V_IVA;
        V_NETO_EXEN        := V_NETO - ROUND((V_NETO *
                                             (I_IMPU_PORC_BASE_IMPONIBLE / 100)),
                                             V_CANT_DECI);
        V_NETO_GRAV        := V_NETO - V_NETO_EXEN - V_IVA;
      END IF;
    ELSE
    
      V_BRUTO_GRAV_LISTA := 0;
      V_BRUTO_EXEN_LISTA := ROUND(V_BRUTO_LISTA, V_CANT_DECI);
      V_BRUTO_GRAV       := 0;
      V_BRUTO_EXEN       := ROUND(V_BRUTO, V_CANT_DECI);
      V_NETO_GRAV        := 0;
      V_NETO_EXEN        := ROUND(V_NETO, V_CANT_DECI);
    END IF;
    --   V_IVA := ROUND(((V_BRUTO_LISTA) / 21), V_CANT_DECI);
  
    V_TOTAL := ROUND(V_NETO_GRAV + V_NETO_EXEN + V_IVA, V_CANT_DECI);
  
    O_TOTAL := ROUND(V_NETO_GRAV + V_NETO_EXEN, V_CANT_DECI);
  
    IF I_DOC_MON = I_MON_LOC THEN
      -- SI LA MONEDA DEL DOCUMENTO ES MONEDA LOCAL
      O_DET_BRUTO_LOC := V_BRUTO_LISTA;
      O_DET_BRUTO_MON := V_BRUTO_LISTA;
      O_DET_NETO_LOC  := V_NETO;
      O_DET_NETO_MON  := V_NETO;
      O_DET_IVA_LOC   := ROUND((V_IVA * I_DOC_TASA_US), V_CANT_DECI);
      O_DET_IVA_MON   := V_IVA;
    ELSE
      -- SI LA MONEDA DEL DOCUMENTO ES U$
      O_DET_BRUTO_MON := V_BRUTO_LISTA;
      O_DET_BRUTO_LOC := ROUND((O_DET_BRUTO_MON * I_DOC_TASA_US),
                               V_CANT_DECI);
      O_DET_NETO_MON  := V_NETO;
      O_DET_NETO_LOC  := ROUND((O_DET_NETO_MON * I_DOC_TASA_US),
                               V_CANT_DECI);
      O_DET_IVA_MON   := V_IVA;
      O_DET_IVA_LOC   := ROUND((O_DET_IVA_MON * I_DOC_TASA_US), V_CANT_DECI);
    END IF;
    -- CALCULA LOS TOTALES EXENTOS, GRAVADOS Y EL IVA
  
    O_BRUTO_EXEN_PRECIO_MON := V_BRUTO_EXEN;
    IF I_TIPO_FACTURA = 3 THEN
      -- FACTURA IVA INCLUIDO
      O_BRUTO_GRAV_PRECIO_MON := V_BRUTO_GRAV +
                                 ROUND(((V_BRUTO_GRAV * I_PORC_IVA) / 100),
                                       V_CANT_DECI);
    ELSE
      O_BRUTO_GRAV_PRECIO_MON := V_BRUTO_GRAV;
    END IF;
    O_BRUTO_EXEN_MON := V_BRUTO_EXEN_LISTA;
    O_BRUTO_GRAV_MON := V_BRUTO_GRAV_LISTA;
    IF I_DOC_MON = I_MON_LOC THEN
      -- SI LA MONEDA DEL DOCUMENTO ES LOCAL
      O_BRUTO_EXEN_LOC := V_BRUTO_EXEN_LISTA;
      O_BRUTO_GRAV_LOC := V_BRUTO_GRAV_LISTA;
    ELSE
      O_BRUTO_EXEN_LOC := ROUND((NVL(O_BRUTO_EXEN_MON, 0) * I_DOC_TASA_US),
                                V_CANT_DECI);
      O_BRUTO_GRAV_LOC := ROUND((NVL(O_BRUTO_GRAV_MON, 0) * I_DOC_TASA_US),
                                V_CANT_DECI);
    END IF;
  
    IF I_DOC_MON = I_MON_LOC THEN
      O_NETO_EXEN_LOC := V_NETO_EXEN;
      O_NETO_EXEN_MON := V_NETO_EXEN;
      O_NETO_GRAV_LOC := V_NETO_GRAV;
      O_NETO_GRAV_MON := V_NETO_GRAV;
    ELSE
      O_NETO_EXEN_MON := V_NETO_EXEN;
      O_NETO_EXEN_LOC := ROUND((NVL(O_NETO_EXEN_MON, 0) * I_DOC_TASA_US),
                               V_CANT_DECI);
      O_NETO_GRAV_MON := V_NETO_GRAV;
      O_NETO_GRAV_LOC := ROUND((NVL(O_NETO_GRAV_MON, 0) * I_DOC_TASA_US),
                               V_CANT_DECI);
    END IF;
  
    O_IVA_MON := V_IVA;
    O_IVA_LOC := ROUND((V_IVA * I_DOC_TASA_US), V_CANT_DECI);
  
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20002, 'ERROR EN PP_RECALCULAR_DETALLES ');
  END PP_RECALCULAR_DETALLES;

  FUNCTION FP_TOT_DTO(I_TOT_BRUTO_EXEN_MON IN NUMBER,
                      I_TOT_BRUTO_GRAV_MON IN NUMBER,
                      I_TOT_NETO_EXEN_MON  IN NUMBER,
                      I_TOT_NETO_GRAV_MON  IN NUMBER) RETURN NUMBER IS
    V_TOT_BRUTO NUMBER;
    V_TOT_NETO  NUMBER;
  BEGIN
    V_TOT_BRUTO := I_TOT_BRUTO_EXEN_MON + I_TOT_BRUTO_GRAV_MON;
    V_TOT_NETO  := I_TOT_NETO_EXEN_MON + I_TOT_NETO_GRAV_MON;
    IF V_TOT_BRUTO > V_TOT_NETO THEN
      RETURN(V_TOT_BRUTO - V_TOT_NETO);
    ELSE
      RETURN 0;
    END IF;
  
  END FP_TOT_DTO;

  PROCEDURE PP_CALCULAR_TOTALES(I_PORC_DTO IN NUMBER,
                                --I_IMPU_PORCENTAJE          IN NUMBER,
                                I_W_CLI_PORC_EXEN_IVA IN NUMBER,
                                --I_IMPU_PORC_BASE_IMPONIBLE IN NUMBER,
                                I_MON_LOC       IN NUMBER,
                                I_DOC_TASA_US   IN NUMBER,
                                I_DOC_MON       IN NUMBER,
                                I_TIPO_FACTURA  IN NUMBER,
                                I_PORC_IVA      IN NUMBER,
                                I_MON_DEC_IMP   IN NUMBER,
                                O_TOTAL_DTO     OUT NUMBER,
                                O_TOTAL_IVA     OUT NUMBER,
                                O_TOTAL         OUT NUMBER,
                                O_NETO_EXEN_MON OUT NUMBER,
                                O_NETO_GRAV_MON OUT NUMBER) IS
    V_DET_BRUTO_LOC         NUMBER;
    V_DET_BRUTO_MON         NUMBER;
    V_DET_NETO_LOC          NUMBER;
    V_DET_NETO_MON          NUMBER;
    V_DET_IVA_LOC           NUMBER;
    V_DET_IVA_MON           NUMBER;
    V_TOTAL                 NUMBER;
    V_BRUTO_EXEN_PRECIO_MON NUMBER;
    V_BRUTO_GRAV_PRECIO_MON NUMBER;
    V_BRUTO_EXEN_MON        NUMBER;
    V_BRUTO_GRAV_MON        NUMBER;
    V_BRUTO_EXEN_LOC        NUMBER;
    V_BRUTO_GRAV_LOC        NUMBER;
    V_IVA_MON               NUMBER;
    V_IVA_LOC               NUMBER;
    V_NETO_EXEN_LOC         NUMBER;
    V_NETO_EXEN_MON         NUMBER;
    V_NETO_GRAV_LOC         NUMBER;
    V_NETO_GRAV_MON         NUMBER;
    V_TOTAL_DTO             NUMBER;
    V_TOTAL_IVA             NUMBER;
    V_TOTAL_T               NUMBER;
    V_TOT_BRUTO_LOC         NUMBER := 0;
    V_TOT_BRUTO_MON         NUMBER := 0;
    V_TOT_NETO_LOC          NUMBER := 0;
    --V_TOT_NETO_MON          NUMBER := 0;
    -- V_DET_IVA_LOC  NUMBER := 0;
    --V_DET_IVA_MON  NUMBER := 0;
    --V_TOT_TOTAL                 NUMBER := 0;
    V_TOT_BRUTO_EXEN_PRECIO_MON NUMBER := 0;
    V_TOT_GRAV_PRECIO_MON       NUMBER := 0;
    V_TOT_EXEN_MON              NUMBER := 0;
    V_TOT_GRAV_MON              NUMBER := 0;
    V_TOT_BRUTO_EXEN_LOC        NUMBER := 0;
    V_TOT_BRUTO_GRAV_LOC        NUMBER := 0;
    V_TOT_IVA_MON               NUMBER := 0;
    V_TOT_IVA_LOC               NUMBER := 0;
    V_TOT_NETO_EXEN_LOC         NUMBER := 0;
    V_TOT_NETO_EXEN_MON         NUMBER := 0;
    V_TOT_GRAV_LOC              NUMBER := 0;
    V_TOT_NETO_GRAV_MON         NUMBER := 0;
  
    CURSOR CU_ARTICULOS IS
      SELECT *
        FROM (SELECT SEQ_ID,
                     SEQ_ID SEQ_ID_V,
                     C001   UM,
                     C002   ART_UM,
                     C003   CLAVE_CONTRATO,
                     N001   ARTICULO,
                     N002   CANTIDAD,
                     N003   PRECIO,
                     N004   IMPORTE,
                     N005   CONTENIDO,
                     C005   FACTOR_CONV,
                     C006   AREM_PRECIO_VTA,
                     C007   IMP_PORC,
                     C008   IMP_P_B_IMPONIBLE
                FROM APEX_COLLECTIONS
               WHERE COLLECTION_NAME = 'ITEMS_FACI039') I;
  
  BEGIN
  
    FOR REG IN CU_ARTICULOS LOOP
      PP_RECALCULAR_DETALLES(I_UNIDAD_VTA               => REG.ART_UM, --I_UNIDAD_VTA,
                             I_ART_UNID_MED             => REG.UM, --I_ART_UNID_MED,
                             I_ART_FACTOR_CONVERSION    => REG.FACTOR_CONV, --I_ART_FACTOR_CONVERSION,
                             I_PRECIO                   => REG.PRECIO, --I_PRECIO,
                             I_DET_CANT                 => REG.CANTIDAD, --I_DET_CANT,
                             I_AREM_PRECIO_VTA          => REG.AREM_PRECIO_VTA, --I_AREM_PRECIO_VTA,
                             I_PORC_DTO                 => I_PORC_DTO,
                             I_IMPU_PORCENTAJE          => REG.IMP_PORC, --I_IMPU_PORCENTAJE,
                             I_W_CLI_PORC_EXEN_IVA      => I_W_CLI_PORC_EXEN_IVA,
                             I_IMPU_PORC_BASE_IMPONIBLE => REG.IMP_P_B_IMPONIBLE, --I_IMPU_PORC_BASE_IMPONIBLE,
                             I_MON_LOC                  => I_MON_LOC,
                             I_DOC_TASA_US              => I_DOC_TASA_US,
                             I_DOC_MON                  => I_DOC_MON,
                             I_TIPO_FACTURA             => I_TIPO_FACTURA,
                             I_PORC_IVA                 => I_PORC_IVA,
                             I_MON_DEC_IMP              => I_MON_DEC_IMP,
                             O_DET_BRUTO_LOC            => V_DET_BRUTO_LOC,
                             O_DET_BRUTO_MON            => V_DET_BRUTO_MON,
                             O_DET_NETO_LOC             => V_DET_NETO_LOC,
                             O_DET_NETO_MON             => V_DET_NETO_MON,
                             O_DET_IVA_LOC              => V_DET_IVA_LOC,
                             O_DET_IVA_MON              => V_DET_IVA_MON,
                             O_TOTAL                    => V_TOTAL,
                             O_BRUTO_EXEN_PRECIO_MON    => V_BRUTO_EXEN_PRECIO_MON,
                             O_BRUTO_GRAV_PRECIO_MON    => V_BRUTO_GRAV_PRECIO_MON,
                             O_BRUTO_EXEN_MON           => V_BRUTO_EXEN_MON,
                             O_BRUTO_GRAV_MON           => V_BRUTO_GRAV_MON,
                             O_BRUTO_EXEN_LOC           => V_BRUTO_EXEN_LOC,
                             O_BRUTO_GRAV_LOC           => V_BRUTO_GRAV_LOC,
                             O_IVA_MON                  => V_IVA_MON,
                             O_IVA_LOC                  => V_IVA_LOC,
                             O_NETO_EXEN_LOC            => V_NETO_EXEN_LOC,
                             O_NETO_EXEN_MON            => V_NETO_EXEN_MON,
                             O_NETO_GRAV_LOC            => V_NETO_GRAV_LOC,
                             O_NETO_GRAV_MON            => V_NETO_GRAV_MON);
    
      V_TOT_BRUTO_LOC             := V_TOT_BRUTO_LOC + V_DET_BRUTO_LOC;
      V_TOT_BRUTO_MON             := V_TOT_BRUTO_MON + V_DET_BRUTO_MON;
      V_TOT_NETO_LOC              := V_TOT_NETO_LOC + V_DET_NETO_LOC;
      V_TOT_BRUTO_EXEN_PRECIO_MON := V_TOT_BRUTO_EXEN_PRECIO_MON +
                                     V_BRUTO_EXEN_PRECIO_MON;
      V_TOT_GRAV_PRECIO_MON       := V_TOT_GRAV_PRECIO_MON +
                                     V_BRUTO_GRAV_PRECIO_MON;
      V_TOT_EXEN_MON              := V_TOT_EXEN_MON + V_BRUTO_EXEN_MON;
      V_TOT_GRAV_MON              := V_TOT_GRAV_MON + V_BRUTO_GRAV_MON;
      V_TOT_BRUTO_EXEN_LOC        := V_TOT_BRUTO_EXEN_LOC +
                                     V_BRUTO_EXEN_LOC;
      V_TOT_BRUTO_GRAV_LOC        := V_TOT_BRUTO_GRAV_LOC +
                                     V_BRUTO_GRAV_LOC;
      V_TOT_IVA_MON               := V_TOT_IVA_MON + V_IVA_MON;
      V_TOT_IVA_LOC               := V_TOT_IVA_LOC + V_IVA_LOC;
      V_TOT_NETO_EXEN_LOC         := V_TOT_NETO_EXEN_LOC + V_NETO_EXEN_LOC;
      V_TOT_NETO_EXEN_MON         := V_TOT_NETO_EXEN_MON + V_NETO_EXEN_MON;
      V_TOT_GRAV_LOC              := V_TOT_GRAV_LOC + V_NETO_GRAV_LOC;
      V_TOT_NETO_GRAV_MON         := V_TOT_NETO_GRAV_MON + V_NETO_GRAV_MON;
    
      APEX_COLLECTION.UPDATE_MEMBER(P_COLLECTION_NAME => 'ITEMS_FACI039',
                                    P_SEQ             => REG.SEQ_ID_V,
                                    P_C001            => REG.UM, --UM VENTA
                                    P_C002            => REG.ART_UM, --UM ARTICULO
                                    P_C003            => REG.CLAVE_CONTRATO,
                                    P_N001            => REG.ARTICULO,
                                    P_N002            => REG.CANTIDAD,
                                    P_N003            => REG.PRECIO,
                                    P_N004            => V_NETO_GRAV_MON +
                                                         V_NETO_EXEN_MON,
                                    P_N005            => REG.CONTENIDO,
                                    
                                    P_C005 => REG.FACTOR_CONV,
                                    P_C006 => REG.AREM_PRECIO_VTA,
                                    P_C007 => REG.IMP_PORC,
                                    P_C008 => REG.IMP_P_B_IMPONIBLE,
                                    P_C009 => V_IVA_MON);
    
    END LOOP;
  
    V_TOTAL_DTO := FP_TOT_DTO(I_TOT_BRUTO_EXEN_MON => V_TOT_EXEN_MON,
                              I_TOT_BRUTO_GRAV_MON => V_TOT_GRAV_MON,
                              I_TOT_NETO_EXEN_MON  => V_TOT_NETO_EXEN_MON,
                              I_TOT_NETO_GRAV_MON  => V_TOT_NETO_GRAV_MON);
  
    V_TOTAL_IVA := V_TOT_IVA_MON; --NVL(:BDOC_DET.W_TOT_IVA_MON,0);--IVA
  
    V_TOTAL_T := NVL(V_TOT_NETO_GRAV_MON, 0) + NVL(V_TOT_NETO_EXEN_MON, 0) +
                 NVL(V_TOT_IVA_MON, 0); --TOTAL
  
    O_TOTAL_DTO     := ROUND(V_TOTAL_DTO, I_MON_DEC_IMP);
    O_TOTAL_IVA     := ROUND(V_TOTAL_IVA, I_MON_DEC_IMP);
    O_TOTAL         := ROUND(V_TOTAL_T, I_MON_DEC_IMP);
    O_NETO_EXEN_MON := V_TOT_NETO_EXEN_MON;
    O_NETO_GRAV_MON := V_TOT_NETO_GRAV_MON;
  
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20002, 'ERROR EN PP_CALCULAR_TOTALES');
  END PP_CALCULAR_TOTALES;

  PROCEDURE PP_INSERTAR_FACTURA_CABECERA(I_DOC_SUC               IN NUMBER,
                                         I_DOC_DEP               IN NUMBER,
                                         I_DOC_TIPO_MOV          IN NUMBER,
                                         I_DOC_CTA_BCO           IN NUMBER,
                                         I_DOC_MON               IN NUMBER,
                                         I_DOC_CLI               IN NUMBER,
                                         I_DOC_CLI_NOM           IN VARCHAR2,
                                         I_DOC_CLI_DIR           IN VARCHAR2,
                                         I_DOC_CLI_RUC           IN VARCHAR2,
                                         I_DOC_CLI_TEL           IN VARCHAR2,
                                         I_COD_VENDEDOR          IN NUMBER,
                                         I_DOC_FEC_DOC           IN DATE,
                                         I_DOC_FEC_OPER          IN DATE,
                                         I_DOC_NETO_GRAV_MON     IN NUMBER,
                                         I_DOC_NETO_EXEN_MON     IN NUMBER,
                                         I_DOC_IVA_MON           IN NUMBER,
                                         I_DOC_NRO_DOC           IN NUMBER,
                                         I_DOC_IND_IMPR_VENDEDOR IN VARCHAR2,
                                         I_DOC_IND_IMPR_CONYUGUE IN VARCHAR2,
                                         I_DOC_CANAL             IN NUMBER,
                                         I_DOC_TIMBRADO          IN NUMBER,
                                         I_OCARGA_LONDON         IN NUMBER,
                                         I_EMPR                  IN NUMBER,
                                         I_TASA                  IN NUMBER,
                                         I_OBS                   IN VARCHAR2,
                                         I_USER                  IN VARCHAR2,
                                         O_DOC_CLAVE_FIN         OUT NUMBER,
                                         O_DOC_CLAVE_STK         OUT NUMBER,
                                         I_EXPORTACION           IN VARCHAR2) IS
  
    V_TASA NUMBER;
    --V_DOC_CTA_BCO_FCON   NUMBER;
    V_IND_ORD_CARGA      VARCHAR2(1);
    BRUTO_GRAV_TOTAL     NUMBER;
    IVA_TOTAL            NUMBER;
    NETO_GRAV_TOTAL      NUMBER;
    BRUTO_GRAV_TOTAL_LOC NUMBER;
    NETO_GRAV_TOTAL_LOC  NUMBER;
    IVA_TOTAL_LOC        NUMBER;
    BRUTO_EXENTO_LOC     NUMBER;
    NETO_EXENTO_LOC      NUMBER;
    V_CTACO              NUMBER;
    V_TIMBRADO           NUMBER;
    V_TIPO_DOC_PROV_CLI  NUMBER;
    ----------------------------
  
    ----------------------------
  
    ----------------------------
    SALIR EXCEPTION;
  
  BEGIN
  
    PL_VAL_DOC_TIMBRADO(I_DOC_NRO_DOC,
                        1,
                        I_DOC_FEC_DOC,
                        V_TIMBRADO,
                        I_EMPR);
  
    IF I_DOC_MON = 1 THEN
      V_TASA := 1;
    ELSE
      V_TASA := I_TASA;
    END IF;
  
    -----------------------------------------------
    O_DOC_CLAVE_FIN := FIN_SEQ_DOC_NEXTVAL;
  
    IF NVL(I_EXPORTACION, 'N') <> 'S' THEN
      O_DOC_CLAVE_STK := STK_SEQ_DOCU_NEXTVAL;
    END IF;
  
    BRUTO_GRAV_TOTAL     := I_DOC_NETO_GRAV_MON;
    BRUTO_GRAV_TOTAL_LOC := ROUND(I_DOC_NETO_GRAV_MON * V_TASA, 2);
    BRUTO_EXENTO_LOC     := ROUND(I_DOC_NETO_EXEN_MON * V_TASA, 2);
  
    NETO_GRAV_TOTAL     := I_DOC_NETO_GRAV_MON;
    NETO_GRAV_TOTAL_LOC := ROUND(I_DOC_NETO_GRAV_MON * V_TASA, 2);
    NETO_EXENTO_LOC     := ROUND(I_DOC_NETO_EXEN_MON * V_TASA, 2);
  
    IVA_TOTAL     := I_DOC_IVA_MON;
    IVA_TOTAL_LOC := ROUND(IVA_TOTAL * V_TASA, 2);
  
    -----------------------------------------------
    IF I_DOC_CLI IS NOT NULL AND I_EMPR IN (1, 2) THEN
      BEGIN
        SELECT CLI_DOC_TIPO
          INTO V_TIPO_DOC_PROV_CLI
          FROM FIN_CLIENTE
         WHERE CLI_EMPR = I_EMPR
           AND CLI_CODIGO = I_DOC_CLI;
        IF V_TIPO_DOC_PROV_CLI IS NULL THEN
          RAISE_APPLICATION_ERROR(-20001,
                                  '!FAVOR, CONFIGURAR EL TIPO DE DOCUMENTO DEL CLIENTE ANTES DE CONTINUAR- PROG 2-1-10!');
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          RAISE_APPLICATION_ERROR(-20001,
                                  '!FAVOR, CONFIGURAR EL TIPO DE DOCUMENTO DEL CLIENTE ANTES DE CONTINUAR- PROG 2-1-10!');
      END;
    END IF;
    --*--
    BEGIN
    
      IF I_DOC_TIPO_MOV = 9 THEN
        V_CTACO := 2707;
      END IF;
    
      INSERT INTO FIN_DOCUMENTO --TABLA EN LA CUAL SE INSERTA
      --------DECLARACION DE CAMPOS DE LA TABLA QUE SE VAN A INSERTAR--------------------
        (DOC_CLAVE,
         DOC_EMPR,
         DOC_CTA_BCO_FCON,
         DOC_COND_VTA,
         DOC_SUC,
         DOC_DPTO,
         DOC_TIPO_MOV,
         DOC_NRO_DOC,
         DOC_TIPO_SALDO,
         DOC_MON,
         DOC_PROV,
         DOC_CLI,
         DOC_CLI_NOM,
         DOC_CLI_DIR,
         DOC_CLI_RUC,
         DOC_CLI_TEL,
         DOC_LEGAJO,
         DOC_BCO_CHEQUE,
         DOC_NRO_CHEQUE,
         DOC_FEC_CHEQUE,
         DOC_FEC_DEP_CHEQUE,
         DOC_EST_CHEQUE,
         DOC_NRO_DOC_DEP,
         DOC_CHEQUE_CERTIF,
         DOC_FEC_OPER,
         DOC_FEC_DOC,
         DOC_BRUTO_EXEN_LOC,
         DOC_BRUTO_EXEN_MON,
         DOC_BRUTO_GRAV_LOC,
         DOC_BRUTO_GRAV_MON,
         DOC_NETO_EXEN_LOC,
         DOC_NETO_EXEN_MON,
         DOC_NETO_GRAV_LOC,
         DOC_NETO_GRAV_MON,
         DOC_IVA_LOC,
         DOC_IVA_MON,
         DOC_SALDO_INI_MON,
         DOC_SALDO_LOC,
         DOC_SALDO_MON,
         DOC_SALDO_PER_ACT_LOC,
         DOC_SALDO_PER_ACT_MON,
         DOC_BASE_IMPON_LOC,
         DOC_BASE_IMPON_MON,
         DOC_DIF_CAMBIO,
         DOC_IND_STK,
         DOC_CLAVE_STK,
         DOC_IND_CUOTA,
         DOC_IND_PEDIDO,
         DOC_IND_FINANRESA,
         DOC_IND_CONSIGNACION,
         DOC_PLAN_FINAN,
         DOC_NRO_LISTADO_LIBRO_IVA,
         DOC_CLAVE_PADRE,
         DOC_LOGIN,
         DOC_FEC_GRAB,
         DOC_SIST,
         DOC_CLI_CODEUDOR,
         DOC_OPERADOR,
         DOC_COBRADOR,
         DOC_CANT_PAGARE,
         DOC_SERIE,
         DOC_CLAVE_SCLI,
         DOC_ORDEN_COMPRA_CLAVE,
         DOC_PRES_CLAVE,
         DOC_TASA,
         DOC_NRO_ORDEN_COMPRA,
         DOC_DIF_CAMBIO_ACUM,
         DOC_DIVISION,
         DOC_FORMA_ENTREGA,
         DOC_OBS,
         DOC_CODIGO_TEFE,
         DOC_GRAV_10_LOC,
         DOC_GRAV_10_MON,
         DOC_GRAV_5_LOC,
         DOC_GRAV_5_MON,
         DOC_IVA_10_LOC,
         DOC_IVA_10_MON,
         DOC_IVA_5_LOC,
         DOC_IVA_5_MON,
         DOC_CAMION,
         DOC_IND_ORD_CARGA,
         DOC_CLAVE_PEDIDO_CAST,
         DOC_IND_IMPR_VENDEDOR,
         DOC_IND_IMPR_CONYUGUE,
         DOC_CANAL_BETA,
         DOC_TIMBRADO,
         DOC_CTACO,
         DOC_IND_EXPORT,
         DOC_TIPO_DOC_CLI_PROV,
         DOC_TIPO_FACTURA,
         DOC_SISTEMA_AUX)
      ---------DECLARACION DE LOS VALORES QUE TOMA EL PROCEDIMIENTO PARA INSERTAR-------------
      VALUES
        (O_DOC_CLAVE_FIN,
         I_EMPR,
         I_DOC_CTA_BCO,
         NULL,
         I_DOC_SUC,
         NULL,
         I_DOC_TIPO_MOV,
         I_DOC_NRO_DOC,
         'D',
         I_DOC_MON,
         NULL,
         I_DOC_CLI,
         I_DOC_CLI_NOM,
         I_DOC_CLI_DIR,
         I_DOC_CLI_RUC,
         I_DOC_CLI_TEL,
         I_COD_VENDEDOR,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         I_DOC_FEC_OPER,
         I_DOC_FEC_DOC,
         BRUTO_EXENTO_LOC,
         BRUTO_EXENTO_LOC,
         BRUTO_GRAV_TOTAL_LOC,
         BRUTO_GRAV_TOTAL,
         NETO_EXENTO_LOC,
         NETO_EXENTO_LOC,
         NETO_GRAV_TOTAL_LOC,
         NETO_GRAV_TOTAL,
         IVA_TOTAL_LOC,
         IVA_TOTAL,
         NETO_GRAV_TOTAL,
         NETO_GRAV_TOTAL,
         NETO_GRAV_TOTAL,
         NETO_GRAV_TOTAL,
         NETO_GRAV_TOTAL,
         NULL,
         NULL,
         NULL,
         'S',
         O_DOC_CLAVE_STK,
         NULL,
         NULL,
         NULL,
         '1',
         NULL,
         NULL,
         NULL,
         I_USER,
         SYSDATE,
         'FAC',
         NULL,
         '2',
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         I_TASA,
         NULL,
         NULL,
         NULL,
         NULL,
         I_OBS,
         NULL,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         NULL,
         V_IND_ORD_CARGA,
         NULL,
         I_DOC_IND_IMPR_VENDEDOR,
         I_DOC_IND_IMPR_CONYUGUE,
         I_DOC_CANAL,
         V_TIMBRADO,
         V_CTACO,
         NVL(I_EXPORTACION, 'N'),
         V_TIPO_DOC_PROV_CLI,
         1,
         'FACI039');
    
      --POST;
    EXCEPTION
      WHEN OTHERS THEN
        --DD ROLLBACK;
        RAISE_APPLICATION_ERROR(-20000,
                                SQLERRM ||
                                ' ERROR AL INSERTAR FIN_DOCUMENTO N? ' ||
                                I_DOC_NRO_DOC || 'I_DOC_NETO_EXEN_MON' ||
                                I_DOC_NETO_EXEN_MON ||
                                'BRUTO_GRAV_TOTAL_LOC' ||
                                BRUTO_GRAV_TOTAL_LOC || 'BRUTO_EXENTO_LOC' ||
                                BRUTO_EXENTO_LOC);
    END;
  
    --------------------------------------------------------------------------------------
    --*--
    --RAISE_APPLICATION_ERROR(-20000,('V_CLAVE_STK= '||V_CLAVE_STK);
    IF NVL(I_EXPORTACION, 'N') <> 'S' THEN
    
      BEGIN
        INSERT INTO STK_DOCUMENTO
          (DOCU_CLAVE,
           DOCU_EMPR,
           DOCU_CODIGO_OPER,
           DOCU_NRO_DOC,
           DOCU_SUC_ORIG,
           DOCU_DEP_ORIG,
           DOCU_SUC_DEST,
           DOCU_DEP_DEST,
           DOCU_MON,
           DOCU_PROV,
           DOCU_CLI,
           DOCU_CLI_NOM,
           DOCU_CLI_RUC,
           DOCU_LEGAJO,
           DOCU_DPTO,
           DOCU_SECCION,
           DOCU_FEC_EMIS,
           DOCU_TIPO_MOV,
           DOCU_GRAV_NETO_LOC,
           DOCU_GRAV_NETO_MON,
           DOCU_EXEN_NETO_LOC,
           DOCU_EXEN_NETO_MON,
           DOCU_GRAV_BRUTO_LOC,
           DOCU_GRAV_BRUTO_MON,
           DOCU_EXEN_BRUTO_LOC,
           DOCU_EXEN_BRUTO_MON,
           DOCU_IVA_LOC,
           DOCU_IVA_MON,
           DOCU_TASA_US,
           DOCU_IND_CUOTA,
           DOCU_IND_CONSIGNACION,
           DOCU_OBS,
           DOCU_CLAVE_PADRE,
           DOCU_NRO_LISTADO_AUD,
           DOCU_LOGIN,
           DOCU_FEC_GRAB,
           DOCU_SIST,
           DOCU_OPERADOR,
           DOCU_NRO_PED,
           DOCU_OCARGA_LONDON,
           DOCU_FEC_OPER,
           DOCU_TIPO_DOC_CLI_PROV,
           DOCU_TIPO_FACTURA)
        VALUES
          (O_DOC_CLAVE_STK,
           I_EMPR,
           3,
           I_DOC_NRO_DOC,
           I_DOC_SUC,
           I_DOC_DEP,
           NULL,
           NULL,
           I_DOC_MON,
           NULL,
           I_DOC_CLI,
           NULL,
           NULL,
           I_COD_VENDEDOR,
           NULL,
           NULL,
           I_DOC_FEC_DOC,
           I_DOC_TIPO_MOV,
           NETO_GRAV_TOTAL_LOC,
           NETO_GRAV_TOTAL,
           NETO_EXENTO_LOC,
           NETO_EXENTO_LOC,
           BRUTO_GRAV_TOTAL_LOC,
           BRUTO_GRAV_TOTAL,
           BRUTO_EXENTO_LOC,
           BRUTO_EXENTO_LOC,
           IVA_TOTAL_LOC,
           IVA_TOTAL,
           I_TASA,
           NULL,
           NULL,
           I_OBS,
           NULL,
           NULL,
           I_USER,
           SYSDATE,
           'FAC',
           '2',
           NULL,
           I_OCARGA_LONDON,
           NVL(I_DOC_FEC_OPER, I_DOC_FEC_DOC),
           V_TIPO_DOC_PROV_CLI,
           1);
      
        --POST;
      EXCEPTION
        WHEN OTHERS THEN
          --DD ROLLBACK;
          RAISE_APPLICATION_ERROR(-20001,
                                  SQLERRM ||
                                  ' ERROR AL INSERTAR STK_DOCUMENTO N? ' ||
                                  I_DOC_NRO_DOC);
      END;
    
    END IF;
  
    BEGIN
      -----=========================================================================================================
    
      -----SE INSERTA EL CONCEPTO DE IVA
    
      INSERT INTO FIN_DOC_CONCEPTO
        (DCON_CLAVE_DOC,
         DCON_ITEM,
         DCON_CLAVE_CONCEPTO,
         DCON_CLAVE_CTACO,
         DCON_TIPO_SALDO,
         DCON_EXEN_LOC,
         DCON_EXEN_MON,
         DCON_GRAV_LOC,
         DCON_GRAV_MON,
         DCON_IVA_LOC,
         DCON_IVA_MON,
         DCON_EMPR)
      VALUES
        (O_DOC_CLAVE_FIN,
         1,
         9,
         118,
         'D',
         0,
         0,
         0,
         0,
         ROUND(I_DOC_IVA_MON * V_TASA, 2),
         I_DOC_IVA_MON,
         I_EMPR);
    
    END;
  
    --CUOTA DE CONTADOS
    IF I_DOC_TIPO_MOV = 9 THEN
      BEGIN
        INSERT INTO FIN_CUOTA
          (CUO_CLAVE_DOC, CUO_FEC_VTO, CUO_IMP_LOC, CUO_IMP_MON, CUO_EMPR)
        VALUES
          (O_DOC_CLAVE_FIN,
           I_DOC_FEC_DOC,
           NETO_GRAV_TOTAL_LOC + NETO_EXENTO_LOC + IVA_TOTAL_LOC,
           I_DOC_NETO_GRAV_MON + I_DOC_NETO_EXEN_MON + I_DOC_IVA_MON,
           I_EMPR);
      
      EXCEPTION
        WHEN OTHERS THEN
          --DD ROLLBACK;
          RAISE_APPLICATION_ERROR(-20001,
                                  SQLERRM ||
                                  ' ERROR AL INSERTAR FIN_CUOTA N? ' ||
                                  I_DOC_NRO_DOC);
        
      END;
    END IF;
  
  END PP_INSERTAR_FACTURA_CABECERA;

  PROCEDURE PP_INSERTAR_FACTURA_CUOTA(I_CLAVE_FIN IN NUMBER,
                                      I_FEC_VTO   IN DATE,
                                      I_IMP_MON   IN NUMBER,
                                      I_EMPR      IN NUMBER,
                                      I_TASA      IN NUMBER) IS
  
  BEGIN
    INSERT INTO FIN_CUOTA
      (CUO_CLAVE_DOC, CUO_FEC_VTO, CUO_IMP_LOC, CUO_IMP_MON, CUO_EMPR)
    VALUES
      (I_CLAVE_FIN,
       I_FEC_VTO,
       ROUND(I_IMP_MON * I_TASA, 2),
       I_IMP_MON,
       I_EMPR);
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20003,
                              SQLERRM || ' ERROR AL INSERTAR FIN_CUOTA');
    
  END PP_INSERTAR_FACTURA_CUOTA;

  PROCEDURE PP_INSERTAR_FACTURA_DETALLE(P_CLAVE_FIN      IN NUMBER,
                                        P_CLAVE_STK      IN NUMBER,
                                        P_NRO_ITEM       IN NUMBER,
                                        P_ART            IN NUMBER,
                                        P_EMPR           IN NUMBER,
                                        P_CANT           IN NUMBER,
                                        P_NETO_MON       IN NUMBER,
                                        P_CODIGO_IMPU    IN NUMBER,
                                        P_IVA_MON        IN NUMBER,
                                        P_EXPORTACION    IN VARCHAR2,
                                        P_TASA           IN NUMBER,
                                        P_CLAVE_CONTRATO IN NUMBER) IS
    V_IMPU_PORCENTAJE NUMBER;
    SALIR EXCEPTION;
    V_DETA_IMPU     VARCHAR2(1);
    V_CTO           NUMBER;
    V_CTA           NUMBER;
    V_DCON_EXEN_MON NUMBER;
    V_DCON_EXEN_LOC NUMBER;
    V_DCON_GRAV_MON NUMBER;
    V_DCON_GRAV_LOC NUMBER;
  BEGIN
  
    --PP_BUSCAR_IMPUESTO;
    SELECT IMPU_PORCENTAJE
      INTO V_IMPU_PORCENTAJE
      FROM GEN_IMPUESTO
     WHERE IMPU_CODIGO = P_CODIGO_IMPU
       AND IMPU_EMPR = P_EMPR;
  
    IF P_CODIGO_IMPU <> 1 THEN
      V_DETA_IMPU     := 'S';
      V_DCON_EXEN_MON := P_NETO_MON;
      V_DCON_EXEN_LOC := ROUND(P_NETO_MON * P_TASA, 2);
      V_DCON_GRAV_MON := 0;
      V_DCON_GRAV_LOC := 0;
    ELSE
      V_DETA_IMPU     := 'N';
      V_DCON_EXEN_MON := 0;
      V_DCON_EXEN_LOC := 0;
      V_DCON_GRAV_MON := P_NETO_MON;
      V_DCON_GRAV_LOC := ROUND(P_NETO_MON * P_TASA, 2);
    END IF;
  
    ------------------------------------------------------------------------------------------
    IF NVL(P_EXPORTACION, 'N') <> 'S' THEN
      BEGIN
        INSERT INTO STK_DOCUMENTO_DET
          (DETA_CLAVE_DOC,
           DETA_NRO_ITEM,
           DETA_ART,
           DETA_EMPR,
           DETA_NRO_REM,
           DETA_CANT,
           DETA_IMP_NETO_LOC,
           DETA_IMP_NETO_MON,
           DETA_IMPU,
           DETA_IVA_LOC,
           DETA_IVA_MON,
           DETA_PORC_DTO,
           DETA_IMP_BRUTO_LOC,
           DETA_IMP_BRUTO_MON,
           DETA_CANT_REM,
           DETA_PERIODO)
        VALUES
          (P_CLAVE_STK,
           P_NRO_ITEM,
           P_ART,
           P_EMPR,
           NULL,
           P_CANT,
           ROUND(P_NETO_MON * P_TASA, 2),
           P_NETO_MON,
           V_DETA_IMPU,
           ROUND(P_IVA_MON * P_TASA, 2),
           P_IVA_MON,
           NULL,
           ROUND(P_NETO_MON * P_TASA, 2),
           P_NETO_MON,
           NULL,
           NULL);
        --POST;
      EXCEPTION
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20000, 'STK_DOCUMENTO_DET ');
      END;
    END IF;
    ----------------------------------------------
    BEGIN
      INSERT INTO FAC_DOCUMENTO_DET
        (DET_CLAVE_DOC,
         DET_NRO_ITEM,
         DET_ART,
         DET_NRO_REMIS,
         DET_CANT,
         DET_BRUTO_LOC,
         DET_BRUTO_MON,
         DET_COD_IVA,
         DET_IVA_LOC,
         DET_IVA_MON,
         DET_PORC_DTO,
         DET_NETO_LOC,
         DET_NETO_MON,
         DET_TIPO_COMISION,
         DET_DESC_LARGA,
         DET_CLAVE_OT,
         DET_EMPR,
         DET_DTO_CONT)
      VALUES
        (P_CLAVE_FIN,
         P_NRO_ITEM,
         P_ART,
         NULL,
         P_CANT,
         ROUND(P_NETO_MON * P_TASA, 2),
         P_NETO_MON,
         P_CODIGO_IMPU,
         ROUND(P_IVA_MON * P_TASA, 2),
         P_IVA_MON,
         0,
         ROUND(P_NETO_MON * P_TASA, 2),
         P_NETO_MON,
         'N',
         NULL,
         NULL,
         P_EMPR,
         P_CLAVE_CONTRATO);
      --POST;
    EXCEPTION
      WHEN OTHERS THEN
        --DD ROLLBACK;
        RAISE_APPLICATION_ERROR(-20000,
                                SQLERRM ||
                                ' ERROR AL INSERTAR FAC_DOCUMENTO_DET ');
    END;
    ----------------------------------------------
    --INSERTAR FIN_DOC_CONCEPTO
    BEGIN
      SELECT DISTINCT DECODE(NVL(P_EXPORTACION, 'N'),
                             'N',
                             DECODE(L.CLAS_CONC_ARTICULO,
                                    NULL,
                                    CV.FCON_CLAVE,
                                    L.CLAS_CONC_ARTICULO),
                             CVE.FCON_CLAVE) CTO,
                      DECODE(NVL(P_EXPORTACION, 'N'),
                             'N',
                             CV.FCON_CLAVE_CTACO,
                             CVE.FCON_CLAVE_CTACO) CTA
        INTO V_CTO, V_CTA
        FROM STK_ARTICULO E,
             STK_CLASIFICACION L,
             (SELECT * FROM FIN_CONCEPTO C WHERE C.FCON_TIPO_SALDO = 'D') CV,
             (SELECT * FROM FIN_CONCEPTO C WHERE C.FCON_TIPO_SALDO = 'D') CVE -- LE PUSE UN FILTRO DE DEBITO POR QUE FLETE EXPORTACION TIENE DOS CONCEPTOS APUNTANDO A LA MISMA CUENTA
      
       WHERE E.ART_CODIGO = P_ART
         AND E.ART_EMPR = P_EMPR
            
         AND E.ART_CLASIFICACION = L.CLAS_CODIGO
         AND E.ART_EMPR = L.CLAS_EMPR
            
         AND L.CLAS_CTACO_VENTA = CV.FCON_CLAVE_CTACO(+)
         AND L.CLAS_EMPR = CV.FCON_EMPR(+)
            
         AND L.CLAS_CTACO_FACT_A_EMBARCAR = CVE.FCON_CLAVE_CTACO(+)
         AND L.CLAS_EMPR = CVE.FCON_EMPR(+);
    
    EXCEPTION
      WHEN OTHERS THEN
        --DD ROLLBACK;
        RAISE_APPLICATION_ERROR(-20000,
                                SQLERRM ||
                                ' ERROR AL INSERTAR FIN_DOC_CONCEPTO, NO EXISTE CLASIFICACION O CUENTA CONTABLE DEL ARTICULO:' ||
                                P_ART);
    END;
  
    BEGIN
      INSERT INTO FIN_DOC_CONCEPTO
        (DCON_CLAVE_DOC,
         DCON_ITEM,
         DCON_CLAVE_CONCEPTO,
         DCON_CLAVE_CTACO,
         DCON_TIPO_SALDO,
         DCON_EXEN_LOC,
         DCON_EXEN_MON,
         DCON_GRAV_LOC,
         DCON_GRAV_MON,
         DCON_IVA_LOC,
         DCON_IVA_MON,
         DCON_PORC_IVA,
         DCON_EMPR)
      VALUES
        (P_CLAVE_FIN,
         P_NRO_ITEM + 1, --UTILIZAMOS EL MISMO NRO DE ITEM + 1, EL 1 SE USO PARA EL IVA
         V_CTO,
         V_CTA,
         'D',
         V_DCON_EXEN_LOC,
         V_DCON_EXEN_MON,
         V_DCON_GRAV_LOC,
         V_DCON_GRAV_MON,
         0,
         0,
         V_IMPU_PORCENTAJE,
         P_EMPR);
    
    EXCEPTION
      WHEN OTHERS THEN
        --DD ROLLBACK;
        RAISE_APPLICATION_ERROR(-20000,
                                SQLERRM ||
                                ' ERROR AL INSERTAR FIN_DOC_CONCEPTO DESDE PP_INSERTAR_FACTURA_DETALLE ');
    END;
  
    ------------------------------------------------------------------------------------------
  
    --POST;
  EXCEPTION
    WHEN OTHERS THEN
      --DD ROLLBACK;
      RAISE_APPLICATION_ERROR(-20000,
                              SQLERRM || ' ERROR AL FACTURAR DETALLE' ||
                              SQLERRM);
  END PP_INSERTAR_FACTURA_DETALLE;

  PROCEDURE PP_INSERTAR_TARJETAS(I_DOC_SUC          IN NUMBER,
                                 I_DOC_CTA_BCO      IN NUMBER,
                                 I_DOC_MON          IN NUMBER,
                                 I_DOC_FEC_DOC      IN DATE,
                                 I_DOC_NRO_DOC      IN NUMBER,
                                 I_EMPR             IN NUMBER,
                                 I_TASA             IN NUMBER,
                                 I_USER             IN VARCHAR2,
                                 I_DOC_CLAVE_PADRE  IN NUMBER, --RECIBE LA CLAVE DE LA FACTURA
                                 I_TARJ_TARJETA     IN NUMBER,
                                 I_TARJ_NRO_TARJETA IN VARCHAR2,
                                 I_TARJ_FEC_VTO     IN DATE,
                                 I_TARJ_IMP_LOC     IN NUMBER) IS
  
    BRUTO_EXENTO_LOC       NUMBER;
    NETO_EXENTO_LOC        NUMBER;
    V_CONF_CONC_TC_INGRESO NUMBER;
    V_CONF_CTA_TC_INGRESO  NUMBER;
    V_DOC_CLAVE_FIN        NUMBER;
  
    SALIR EXCEPTION;
  
  BEGIN
  
    -----------------------------------------------
    V_DOC_CLAVE_FIN := FIN_SEQ_DOC_NEXTVAL;
  
    BRUTO_EXENTO_LOC := NVL(I_TARJ_IMP_LOC, 0) * 1;
    NETO_EXENTO_LOC  := NVL(I_TARJ_IMP_LOC, 0) * 1;
  
    SELECT CONF_CONC_TC_INGRESO
      INTO V_CONF_CONC_TC_INGRESO
      FROM FIN_CONFIGURACION
     WHERE CONF_EMPR = I_EMPR;
  
    SELECT FCON_CLAVE_CTACO
      INTO V_CONF_CTA_TC_INGRESO
      FROM FIN_CONCEPTO
     WHERE FCON_CLAVE = V_CONF_CONC_TC_INGRESO
       AND FCON_EMPR = I_EMPR;
  
    INSERT INTO FIN_DOCUMENTO
      (DOC_CLAVE,
       DOC_EMPR,
       DOC_SUC,
       DOC_CTA_BCO_FCON,
       DOC_TIPO_MOV,
       DOC_NRO_DOC,
       DOC_TIPO_SALDO,
       DOC_MON,
       DOC_FEC_OPER,
       DOC_FEC_DOC,
       DOC_BRUTO_EXEN_LOC,
       DOC_BRUTO_EXEN_MON,
       DOC_BRUTO_GRAV_LOC,
       DOC_BRUTO_GRAV_MON,
       DOC_NETO_EXEN_LOC,
       DOC_NETO_EXEN_MON,
       DOC_NETO_GRAV_LOC,
       DOC_NETO_GRAV_MON,
       DOC_IVA_LOC,
       DOC_IVA_MON,
       DOC_OBS,
       DOC_CLAVE_PADRE,
       DOC_LOGIN,
       DOC_FEC_GRAB,
       DOC_SIST,
       DOC_SERIE,
       DOC_SISTEMA_AUX)
    VALUES
      (V_DOC_CLAVE_FIN,
       I_EMPR,
       I_DOC_SUC,
       I_DOC_CTA_BCO,
       13,
       I_DOC_NRO_DOC,
       'C',
       I_DOC_MON,
       I_DOC_FEC_DOC,
       I_DOC_FEC_DOC,
       I_TARJ_IMP_LOC,
       I_TARJ_IMP_LOC,
       0,
       0,
       I_TARJ_IMP_LOC,
       I_TARJ_IMP_LOC,
       0,
       0,
       0,
       0,
       'TARJETAS/CHEQUES.DOC.' || I_DOC_NRO_DOC || ',TIPO:' || 13,
       I_DOC_CLAVE_PADRE,
       I_USER,
       SYSDATE,
       'FIN',
       NULL,
       'FACI039');
    --===========================================================
    --TARJETAS
  
    --FIN_DOC_CONCEPTO
  
    INSERT INTO FIN_DOC_CONCEPTO
      (DCON_CLAVE_DOC,
       DCON_ITEM,
       DCON_CLAVE_CONCEPTO,
       DCON_CLAVE_CTACO,
       DCON_TIPO_SALDO,
       DCON_EXEN_MON,
       DCON_EXEN_LOC,
       DCON_GRAV_LOC,
       DCON_GRAV_MON,
       DCON_IVA_LOC,
       DCON_IVA_MON,
       DCON_EMPR)
    VALUES
      (V_DOC_CLAVE_FIN,
       1,
       V_CONF_CONC_TC_INGRESO,
       V_CONF_CTA_TC_INGRESO,
       'C',
       ROUND(I_TARJ_IMP_LOC / I_TASA, 2),
       I_TARJ_IMP_LOC,
       0,
       0,
       0,
       0,
       I_EMPR);
  
    INSERT INTO FIN_TC_CUPON
      (CUP_TARJETA,
       CUP_NRO_TARJETA,
       CUP_FEC_VTO,
       CUP_IMP_LOC,
       CUP_ORIGEN,
       CUP_CLAVE_FIN,
       CUP_NRO_CAJA,
       CUP_NRO_Z,
       CUP_EMPR)
    VALUES
      (I_TARJ_TARJETA,
       I_TARJ_NRO_TARJETA,
       I_TARJ_FEC_VTO,
       I_TARJ_IMP_LOC,
       'CJ',
       V_DOC_CLAVE_FIN,
       NULL,
       NULL,
       I_EMPR);
  
    --POST;
  EXCEPTION
    WHEN OTHERS THEN
      --DD ROLLBACK;
      RAISE_APPLICATION_ERROR(-20000,
                              SQLERRM ||
                              ' ERROR AL INSERTAR FIN_DOCUMENTO N? ' ||
                              I_DOC_NRO_DOC);
      --------------------------------------------------------------------------------------
  
  END PP_INSERTAR_TARJETAS;

  PROCEDURE PP_INSERTAR_CHEQUES(I_DOC_SUC            IN NUMBER,
                                I_DOC_CTA_BCO        IN NUMBER,
                                I_DOC_MON            IN NUMBER,
                                I_DOC_FEC_DOC        IN DATE,
                                I_DOC_NRO_DOC        IN NUMBER,
                                I_EMPR               IN NUMBER,
                                I_TASA               IN NUMBER,
                                I_USER               IN VARCHAR2,
                                I_DOC_CLAVE_PADRE    IN NUMBER, --RECIBE LA CLAVE DE LA FACTURA
                                I_CHEQ_NRO           IN VARCHAR2,
                                I_CHEQ_SERIE         IN VARCHAR2,
                                I_CHEQ_BCO           IN NUMBER,
                                I_CHEQ_MON           IN NUMBER,
                                I_CHEQ_IMPORTE       IN NUMBER,
                                I_CLI_CODIGO         IN NUMBER,
                                I_CLI_NOM            IN VARCHAR2,
                                I_CHEQ_FEC_DEPOSITAR IN DATE,
                                I_CHEQ_NRO_CTA_CHEQ  IN VARCHAR2) IS
  
    V_CONCEP          NUMBER;
    V_CUENTA_C        NUMBER;
    VCLAVECHEQ        NUMBER;
    V_CLAVE_NOT_TRANS NUMBER;
    V_CLAVE_CH_RESP   NUMBER;
    VIMPLOC           NUMBER := 0;
    --VIMPCHEQDIFLOC          NUMBER := 0;
    VIMPMON                 NUMBER := 0;
    V_NOT_TRAS              VARCHAR2(1);
    V_CTA_CHEQ_DIF_RESPALDO NUMBER;
    V_CTA_NOT_TRAN_RESPALDO NUMBER;
    V_CONF_CONC_CH_INGRESO  NUMBER;
    V_CONF_CTA_CH_INGRESO   NUMBER;
    V_CLAVE_FIN             NUMBER;
    V_CONF_CONC_CH_RESP     NUMBER;
    V_CONF_CTA_CH_RESP      NUMBER;
  
    --FIN_CHEQUE
  BEGIN
    BEGIN
      SELECT BCO_NOT_TRANSF
        INTO V_NOT_TRAS
        FROM FIN_BANCO
       WHERE BCO_CODIGO = I_DOC_CTA_BCO
         AND BCO_EMPR = I_EMPR;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_NOT_TRAS := 'N';
      WHEN OTHERS THEN
        V_NOT_TRAS := 'N';
    END;
  
    SELECT CTA_CHEQ_DIF_RESPALDO, CTA_NOT_TRAN_RESPALDO
      INTO V_CTA_CHEQ_DIF_RESPALDO, V_CTA_NOT_TRAN_RESPALDO
      FROM FIN_BANCO, GEN_MONEDA, FIN_CUENTA_BANCARIA
     WHERE CTA_BCO = BCO_CODIGO(+)
       AND CTA_MON = MON_CODIGO(+)
       AND CTA_CODIGO = I_DOC_CTA_BCO
       AND CTA_EMPR = BCO_EMPR(+)
       AND CTA_EMPR = MON_EMPR(+)
       AND CTA_EMPR = I_EMPR;
  
    SELECT FCON_CLAVE, FCON_CLAVE_CTACO
      INTO V_CONF_CONC_CH_INGRESO, V_CONF_CTA_CH_INGRESO
      FROM FIN_CONCEPTO
     WHERE FCON_DESC = 'CHEQUES(CR)'
       AND FCON_TIPO_SALDO = 'C'
       AND FCON_EMPR = I_EMPR;
  
    IF V_NOT_TRAS = 'S' AND V_CTA_NOT_TRAN_RESPALDO IS NOT NULL THEN
      V_CONCEP   := 1728;
      V_CUENTA_C := 2648;
    ELSE
      V_CONCEP   := V_CONF_CONC_CH_INGRESO;
      V_CUENTA_C := V_CONF_CTA_CH_INGRESO;
    END IF;
  
    SELECT FCON_CLAVE, FCON_CLAVE_CTACO
      INTO V_CONF_CONC_CH_RESP, V_CONF_CTA_CH_RESP
      FROM FIN_CONCEPTO
     WHERE FCON_DESC = 'CHEQUES DIF RESP(DB)'
       AND FCON_TIPO_SALDO = 'D'
       AND FCON_EMPR = I_EMPR;
  
    V_CLAVE_FIN := FIN_SEQ_DOC_NEXTVAL; --OBTENER LA CLAVE DEL NUEVO DOCUMENTO
    VIMPLOC     := ROUND(NVL(I_CHEQ_IMPORTE, 0), 2);
    VIMPMON     := ROUND(VIMPLOC / I_TASA, 2);
    --FIN_DOCUMENTO
    INSERT INTO FIN_DOCUMENTO
      (DOC_CLAVE,
       DOC_EMPR,
       DOC_SUC,
       DOC_CTA_BCO_FCON,
       DOC_TIPO_MOV,
       DOC_NRO_DOC,
       DOC_TIPO_SALDO,
       DOC_MON,
       DOC_FEC_OPER,
       DOC_FEC_DOC,
       DOC_BRUTO_EXEN_LOC,
       DOC_BRUTO_EXEN_MON,
       DOC_BRUTO_GRAV_LOC,
       DOC_BRUTO_GRAV_MON,
       DOC_NETO_EXEN_LOC,
       DOC_NETO_EXEN_MON,
       DOC_NETO_GRAV_LOC,
       DOC_NETO_GRAV_MON,
       DOC_IVA_LOC,
       DOC_IVA_MON,
       DOC_OBS,
       /*DOC_CLAVE_PADRE,*/
       DOC_LOGIN,
       DOC_FEC_GRAB,
       DOC_SIST,
       DOC_SERIE,
       DOC_REC_COB,
       DOC_SISTEMA_AUX)
    VALUES
      (V_CLAVE_FIN,
       I_EMPR,
       I_DOC_SUC,
       I_DOC_CTA_BCO,
       13,
       I_CHEQ_NRO /*:I_DOC_NRO_DOC*/,
       'C',
       I_DOC_MON,
       I_DOC_FEC_DOC,
       I_DOC_FEC_DOC,
       VIMPLOC,
       VIMPMON,
       0,
       0,
       VIMPLOC,
       VIMPMON,
       0,
       0,
       0,
       0,
       'CHEQ.NRO' || I_CHEQ_NRO || ',FAC.NRO:' || I_DOC_NRO_DOC /*:BAUX.TIPO_MOV*/,
       /*:BAUX.CLAVE_DOC,*/
       I_USER,
       SYSDATE,
       'FIN',
       NULL,
       I_DOC_CLAVE_PADRE, /*CLAVE DE LA FACTURA CONTADO*/
       'FACI039');
  
    --===========================================================
  
    --FIN_DOC_CONCEPTO
    INSERT INTO FIN_DOC_CONCEPTO
      (DCON_CLAVE_DOC,
       DCON_ITEM,
       DCON_CLAVE_CONCEPTO,
       DCON_CLAVE_CTACO,
       DCON_TIPO_SALDO,
       DCON_EXEN_MON,
       DCON_EXEN_LOC,
       DCON_GRAV_LOC,
       DCON_GRAV_MON,
       DCON_IVA_LOC,
       DCON_IVA_MON,
       DCON_EMPR)
    VALUES
      (V_CLAVE_FIN,
       1,
       V_CONCEP,
       V_CUENTA_C,
       'C',
       VIMPMON,
       VIMPLOC,
       0,
       0,
       0,
       0,
       I_EMPR);
  
    VCLAVECHEQ := FIN_SEQ_CHEQUE_NEXTVAL;
  
    INSERT INTO FIN_CHEQUE
      (CHEQ_CLAVE,
       CHEQ_EMPR,
       CHEQ_SERIE,
       CHEQ_NRO,
       CHEQ_SUC,
       CHEQ_BCO,
       CHEQ_MON,
       CHEQ_CLI,
       CHEQ_CLI_NOM,
       CHEQ_TITULAR,
       CHEQ_FEC_EMIS,
       CHEQ_FEC_DEPOSITAR,
       CHEQ_IMPORTE,
       CHEQ_IMPORTE_LOC,
       CHEQ_SITUACION,
       CHEQ_FEC_GRAB,
       CHEQ_LOGIN,
       CHEQ_NRO_CTA_CHEQ)
    VALUES
      (VCLAVECHEQ,
       I_EMPR,
       I_CHEQ_SERIE,
       I_CHEQ_NRO,
       I_DOC_SUC,
       I_CHEQ_BCO,
       I_CHEQ_MON,
       I_CLI_CODIGO,
       I_CLI_NOM,
       I_CLI_NOM,
       I_DOC_FEC_DOC,
       I_CHEQ_FEC_DEPOSITAR,
       VIMPMON,
       VIMPLOC,
       'I',
       SYSDATE,
       I_USER,
       I_CHEQ_NRO_CTA_CHEQ);
  
    --INSERTAR EN LA TABLA QUE RELACIONA FIN_CHEQUE CON FIN_DOCUMENTO
    INSERT INTO FIN_CHEQUE_DOCUMENTO
      (CHDO_CLAVE_DOC, CHDO_CLAVE_CHEQ, CHDO_EMPR)
    VALUES
      (V_CLAVE_FIN, VCLAVECHEQ, I_EMPR);
  
    /*===========================================================================*/
    /*===========================================================================*/
  
    IF V_NOT_TRAS = 'S' AND V_CTA_NOT_TRAN_RESPALDO IS NOT NULL THEN
      V_CLAVE_NOT_TRANS := FIN_SEQ_DOC_NEXTVAL;
      --FIN_DOCUMENTO
      INSERT INTO FIN_DOCUMENTO
        (DOC_CLAVE,
         DOC_EMPR,
         DOC_SUC,
         DOC_CTA_BCO_FCON,
         DOC_TIPO_MOV,
         DOC_NRO_DOC,
         DOC_TIPO_SALDO,
         DOC_MON,
         DOC_FEC_OPER,
         DOC_FEC_DOC,
         DOC_BRUTO_EXEN_LOC,
         DOC_BRUTO_EXEN_MON,
         DOC_BRUTO_GRAV_LOC,
         DOC_BRUTO_GRAV_MON,
         DOC_NETO_EXEN_LOC,
         DOC_NETO_EXEN_MON,
         DOC_NETO_GRAV_LOC,
         DOC_NETO_GRAV_MON,
         DOC_IVA_LOC,
         DOC_IVA_MON,
         DOC_OBS,
         DOC_CLAVE_PADRE,
         DOC_LOGIN,
         DOC_FEC_GRAB,
         DOC_SIST,
         DOC_SERIE,
         DOC_SISTEMA_AUX)
      VALUES
        (V_CLAVE_NOT_TRANS,
         I_EMPR,
         I_DOC_SUC,
         V_CTA_NOT_TRAN_RESPALDO,
         12,
         I_CHEQ_NRO /*:I_DOC_NRO_DOC*/,
         'D',
         I_DOC_MON,
         I_DOC_FEC_DOC,
         I_DOC_FEC_DOC,
         VIMPLOC,
         VIMPMON,
         0,
         0,
         VIMPLOC,
         VIMPMON,
         0,
         0,
         0,
         0,
         'CHEQ.NRO' || I_CHEQ_NRO || ',FAC.NRO:' || I_DOC_NRO_DOC,
         V_CLAVE_FIN /*:BAUX.CLAVE_DOC*/,
         I_USER,
         SYSDATE,
         'FIN',
         NULL,
         'FACI039');
    
      --===========================================================
    
      --FIN_DOC_CONCEPTO
      INSERT INTO FIN_DOC_CONCEPTO
        (DCON_CLAVE_DOC,
         DCON_ITEM,
         DCON_CLAVE_CONCEPTO,
         DCON_CLAVE_CTACO,
         DCON_TIPO_SALDO,
         DCON_EXEN_MON,
         DCON_EXEN_LOC,
         DCON_GRAV_LOC,
         DCON_GRAV_MON,
         DCON_IVA_LOC,
         DCON_IVA_MON,
         DCON_EMPR)
      VALUES
        (V_CLAVE_NOT_TRANS,
         1,
         1728,
         2648,
         'D',
         VIMPMON,
         VIMPLOC,
         0,
         0,
         0,
         0,
         I_EMPR);
      --INSERTAR EN LA TABLA QUE RELACIONA FIN_CHEQUE CON FIN_DOCUMENTO
      INSERT INTO FIN_CHEQUE_DOCU_RESP
        (CHDO_CLAVE_DOC, CHDO_CLAVE_CHEQ, CHDO_EMPR)
      VALUES
        (V_CLAVE_NOT_TRANS, VCLAVECHEQ, I_EMPR);
    
    ELSE
      /*I_CHEQ_FEC_DEPOSITAR <> I_DOC_FEC_DOC AND*/
      ---A PARTIR DE 28-09-2016 TODOS LOS CHEQUES ENTRAN EN LAS AUXILIARES DE DIFERIDOS.--PEDIDO CRISTIAN.
    
      IF V_CTA_CHEQ_DIF_RESPALDO IS NULL THEN
        RAISE_APPLICATION_ERROR(-20001,
                                'LA CAJA NO TIENE UNA AUXILIAR PARA DIFERIDOS');
      END IF;
    
      V_CLAVE_CH_RESP := FIN_SEQ_DOC_NEXTVAL;
      --FIN_DOCUMENTO
      INSERT INTO FIN_DOCUMENTO
        (DOC_CLAVE,
         DOC_EMPR,
         DOC_SUC,
         DOC_CTA_BCO_FCON,
         DOC_TIPO_MOV,
         DOC_NRO_DOC,
         DOC_TIPO_SALDO,
         DOC_MON,
         DOC_FEC_OPER,
         DOC_FEC_DOC,
         DOC_BRUTO_EXEN_LOC,
         DOC_BRUTO_EXEN_MON,
         DOC_BRUTO_GRAV_LOC,
         DOC_BRUTO_GRAV_MON,
         DOC_NETO_EXEN_LOC,
         DOC_NETO_EXEN_MON,
         DOC_NETO_GRAV_LOC,
         DOC_NETO_GRAV_MON,
         DOC_IVA_LOC,
         DOC_IVA_MON,
         DOC_OBS,
         DOC_CLAVE_PADRE,
         DOC_LOGIN,
         DOC_FEC_GRAB,
         DOC_SIST,
         DOC_SERIE,
         DOC_SISTEMA_AUX)
      VALUES
        (V_CLAVE_CH_RESP,
         I_EMPR,
         I_DOC_SUC,
         V_CTA_CHEQ_DIF_RESPALDO,
         12,
         I_CHEQ_NRO /*:I_DOC_NRO_DOC*/,
         'D',
         I_DOC_MON,
         I_DOC_FEC_DOC,
         I_DOC_FEC_DOC,
         VIMPLOC,
         VIMPMON,
         0,
         0,
         VIMPLOC,
         VIMPMON,
         0,
         0,
         0,
         0,
         'CHEQ.NRO' || I_CHEQ_NRO || ',FAC.NRO:' || I_DOC_NRO_DOC,
         V_CLAVE_FIN /*:BAUX.CLAVE_DOC*/,
         I_USER,
         SYSDATE,
         'FIN',
         NULL,
         'FACI039');
    
      --===========================================================
    
      --FIN_DOC_CONCEPTO
      INSERT INTO FIN_DOC_CONCEPTO
        (DCON_CLAVE_DOC,
         DCON_ITEM,
         DCON_CLAVE_CONCEPTO,
         DCON_CLAVE_CTACO,
         DCON_TIPO_SALDO,
         DCON_EXEN_MON,
         DCON_EXEN_LOC,
         DCON_GRAV_LOC,
         DCON_GRAV_MON,
         DCON_IVA_LOC,
         DCON_IVA_MON,
         DCON_EMPR)
      VALUES
        (V_CLAVE_CH_RESP,
         1,
         V_CONF_CONC_CH_RESP,
         V_CONF_CTA_CH_RESP,
         'D',
         VIMPMON,
         VIMPLOC,
         0,
         0,
         0,
         0,
         I_EMPR);
      --INSERTAR EN LA TABLA QUE RELACIONA FIN_CHEQUE CON FIN_DOCUMENTO
      INSERT INTO FIN_CHEQUE_DOCU_RESP
        (CHDO_CLAVE_DOC, CHDO_CLAVE_CHEQ, CHDO_EMPR)
      VALUES
        (V_CLAVE_CH_RESP, VCLAVECHEQ, I_EMPR);
    END IF;
  END;

  PROCEDURE PP_VALIDAR_CHEQ_DIF(P_DOC_FEC_DOC             IN DATE,
                                P_DOC_CLI                 IN NUMBER,
                                P_W_IMP_LIM_CR_EMPR       IN OUT NUMBER,
                                P_EMPRESA                 IN NUMBER,
                                P_W_IMP_LIM_CR_DISP_GRUPO IN OUT NUMBER,
                                P_W_IMP_LIM_CR_DISP_EMPR  IN OUT NUMBER,
                                P_W_IMP_CHEQ_DIFERIDO     IN OUT NUMBER,
                                P_W_IMP_CHEQ_RECHAZADO    IN OUT NUMBER) IS
    V_SUM_CHEQ_DIF NUMBER := 0;
  
    CURSOR CUCHEQUES IS
      SELECT SEQ_ID,
             C001   CHEQ_SERIE,
             C002   CHEQ_NRO,
             C003   NRO_CTA_CHEQ,
             N001   CHEQ_BCO,
             N002   CHEQ_MON,
             N003   IMPORTE,
             N004   TASA,
             N005   IMPORTE_LOC,
             D001   CHEQ_FEC_DEPOSITAR
        FROM APEX_COLLECTIONS
       WHERE COLLECTION_NAME = 'CHEQUE_FACI039';
  
  BEGIN
    FOR REG IN CUCHEQUES LOOP
      IF TO_DATE(REG.CHEQ_FEC_DEPOSITAR, 'DD/MM/YYYY') <>
         TO_DATE(SYSDATE, 'DD/MM/YYYY') THEN
        V_SUM_CHEQ_DIF := V_SUM_CHEQ_DIF + REG.IMPORTE_LOC;
      END IF;
    END LOOP;
  
    IF V_SUM_CHEQ_DIF > 0 THEN
      --CONSULTAMOS EL LIMITE DE CREDITO
      FIN_LIM_CREDITO(P_DOC_FEC_DOC,
                      P_DOC_CLI,
                      P_W_IMP_LIM_CR_EMPR,
                      P_EMPRESA,
                      P_W_IMP_LIM_CR_DISP_GRUPO,
                      P_W_IMP_LIM_CR_DISP_EMPR,
                      P_W_IMP_CHEQ_DIFERIDO,
                      P_W_IMP_CHEQ_RECHAZADO);
    
      --RESTAMOS LOS INTERESES DE LAS CUOTAS VENCIDAS.
      P_W_IMP_LIM_CR_DISP_EMPR := P_W_IMP_LIM_CR_DISP_EMPR -
                                  FIN_INTERES_FACTURA(P_DOC_CLI, P_EMPRESA);
    
      --VALIDAR COBROS CON CHEQUES DIFERIDOS QUE SUPEREN EL LIMITE DE CREDITO DISPONIBLE PARA FACTURAS PARA EL CLIENTE.
      IF V_SUM_CHEQ_DIF > P_W_IMP_LIM_CR_DISP_EMPR THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'TOTAL COBRO CHEQUE DIFERIDO: ' ||
                                                   V_SUM_CHEQ_DIF ||
                                                   ' ,SUPERA EL LIMITE DE CREDITO DISPONIBLE: ' ||
                                                   P_W_IMP_LIM_CR_DISP_EMPR,
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
        RAISE_APPLICATION_ERROR(-20002,
                                'TOTAL COBRO CHEQUE DIFERIDO: ' ||
                                V_SUM_CHEQ_DIF ||
                                ' ,SUPERA EL LIMITE DE CREDITO DISPONIBLE: ' ||
                                P_W_IMP_LIM_CR_DISP_EMPR);
      END IF;
    END IF;
  END PP_VALIDAR_CHEQ_DIF;

  PROCEDURE PP_VALIDAR_CHEQREC(P_EMPRESA    IN NUMBER,
                               P_CHEQ_NRO   IN NUMBER,
                               P_CHEQ_BCO   IN NUMBER,
                               P_CHEQ_SERIE IN VARCHAR2) IS
  BEGIN
    --PRIMERO SE BUSCA EN LA TABLA TEMPORAL DE CHEQUES
    FOR R IN (SELECT 1
                FROM FIN_PLCO_CHEQUES
               WHERE PLCH_BCO = P_CHEQ_BCO
                 AND PLCH_SERIE = P_CHEQ_SERIE
                 AND PLCH_NRO = P_CHEQ_NRO
                 AND PLCH_EMPR = P_EMPRESA) LOOP
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL CHEQUE YA HA SIDO CARGADO PARA ESTA RENDICION.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002,
                              'EL CHEQUE YA HA SIDO CARGADO PARA ESTA RENDICION.');
    END LOOP;
    --LUEGO EN LA TABLA DE CHEQUES
    FOR R IN (SELECT 1
                FROM FIN_CHEQUE
               WHERE CHEQ_BCO = P_CHEQ_BCO
                 AND CHEQ_SERIE = P_CHEQ_SERIE
                 AND CHEQ_NRO = P_CHEQ_NRO
                 AND CHEQ_EMPR = P_EMPRESA) LOOP
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL CHEQUE YA EXISTE.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      RAISE_APPLICATION_ERROR(-20002, 'EL CHEQUE YA EXISTE.');
    END LOOP;
  
  END PP_VALIDAR_CHEQREC;

  PROCEDURE PP_VALIDAR_CHEQUES(P_W_CHEQ_BCO          IN NUMBER,
                               P3_CHEQ_SERIE         IN VARCHAR2,
                               P3_CHEQ_NRO           IN VARCHAR2,
                               P3_CHEQ_MON           IN NUMBER,
                               P3_CHEQ_FEC_DEPOSITAR IN DATE,
                               P3_IMPORTE            IN NUMBER,
                               P3_TASA               IN OUT NUMBER,
                               P3_CHEQ_IMPORTE_LOC   IN OUT NUMBER,
                               P3_CHEQ_NRO_CTA_CHEQ  IN VARCHAR2,
                               P_EMPRESA             IN NUMBER,
                               P_FEC_DOC             IN DATE,
                               P_MON_LOC             IN NUMBER) IS
  BEGIN
    IF P_W_CHEQ_BCO IS NULL THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'POR FAVOR DILIGENCIE EL BANCO.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF;
    IF P3_CHEQ_SERIE IS NULL THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'POR FAVOR DILIGENCIE LA SERIE.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF;
    IF P3_CHEQ_NRO IS NULL THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'POR FAVOR DILIGENCIE EL NUMERO.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF;
    IF P3_CHEQ_MON IS NULL THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'POR FAVOR DILIGENCIE LA MONEDA.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF;
    IF P3_CHEQ_FEC_DEPOSITAR IS NULL THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'POR FAVOR DILIGENCIE LA FECHA DE DEPOSITO.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF;
    IF P3_IMPORTE IS NULL THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'POR FAVOR DILIGENCIE EL IMPORTE.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF;
    IF P3_TASA IS NULL THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'POR FAVOR DILIGENCIE LA TASA.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF;
    IF P3_CHEQ_IMPORTE_LOC IS NULL THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'POR FAVOR DILIGENCIE EL IMPORTE LOC.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF;
    IF P3_CHEQ_NRO_CTA_CHEQ IS NULL THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'POR FAVOR DILIGENCIE EL NUMERO DE CUENTA.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF;
  
    PP_VALIDAR_CHEQREC(P_EMPRESA    => P_EMPRESA,
                       P_CHEQ_NRO   => P3_CHEQ_NRO,
                       P_CHEQ_BCO   => P_W_CHEQ_BCO,
                       P_CHEQ_SERIE => P3_CHEQ_SERIE);
  
    --FECHA A DEP NO DEBE SER MENOR A LA FECHA DEL DOCUMENTO
    IF P3_CHEQ_FEC_DEPOSITAR < P_FEC_DOC THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'FECHA A DEPOSITAR NO PUEDE SER MENOR QUE LA FECHA DEL DOCUMENTO.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      --RAISE_APPLICATION_ERROR(-20002,'FECHA A DEPOSITAR NO PUEDE SER MENOR QUE LA FECHA DEL DOCUMENTO.');
    END IF;
  
    -- NI MAYOR A FECDOC+365
    IF P3_CHEQ_FEC_DEPOSITAR > (P_FEC_DOC + 365) THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'FECHA A DEPOSITAR NO PUEDE MAS DE 365 DIAS DESPUES DE LA FECHA DEL DOCUMENTO.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      --RAISE_APPLICATION_ERROR(-20002,'FECHA A DEPOSITAR NO PUEDE MAS DE 365 DIAS DESPUES DE LA FECHA DEL DOCUMENTO.');
    END IF;
  
    IF P3_IMPORTE <= 0 THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL IMPORTE DEBE SER MAYOR QUE CERO.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      --RAISE_APPLICATION_ERROR(-20002,'EL IMPORTE DEBE SER MAYOR QUE CERO.');
    END IF;
    --
    IF P3_TASA IS NULL THEN
      IF P3_CHEQ_MON <> P_MON_LOC THEN
        P3_TASA := FINC004.FL_BUSCAR_COTIZACION(P3_CHEQ_FEC_DEPOSITAR,
                                                P3_CHEQ_MON,
                                                P_EMPRESA);
      ELSE
        P3_TASA := 1;
      END IF;
    END IF;
    IF P3_CHEQ_MON = P_MON_LOC AND P3_TASA <> 1 THEN
      P3_TASA := 1;
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'LA TASA DEBE SER 1 CUANDO LA MONEDA DEL CHEQUE ES IGUAL A LA MONEDA LOCAL.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      --RAISE_APPLICATION_ERROR(-20002,'LA TASA DEBE SER 1 CUANDO LA MONEDA DEL CHEQUE ES IGUAL A LA MONEDA LOCAL.');
    END IF;
    P3_CHEQ_IMPORTE_LOC := ROUND(P3_IMPORTE * P3_TASA, 2);
  
    --
    IF P3_CHEQ_IMPORTE_LOC <> P3_IMPORTE AND P3_CHEQ_MON = P_MON_LOC THEN
      P3_CHEQ_IMPORTE_LOC := P3_IMPORTE;
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'IMPORTE LOC. E IMPORTE MON. DEBEN SER IGUALES SI EL CHEQUE ES EN MONEDA LOCAL.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      --RAISE_APPLICATION_ERROR(-20002,'IMPORTE LOC. E IMPORTE MON. DEBEN SER IGUALES SI EL CHEQUE ES EN MONEDA LOCAL.');
    END IF;
  
    IF P3_CHEQ_IMPORTE_LOC <= 0 THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL IMPORTE DEBE SER MAYOR QUE CERO.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      --RAISE_APPLICATION_ERROR(-20002,'EL IMPORTE DEBE SER MAYOR QUE CERO.');
    END IF;
  
    IF P3_CHEQ_IMPORTE_LOC <> 0 THEN
      P3_TASA := P3_CHEQ_IMPORTE_LOC / P3_IMPORTE;
    ELSE
      P3_TASA := 1;
    END IF;
  END PP_VALIDAR_CHEQUES;

  PROCEDURE PP_VALIDAR_TARJETAS(P_TARJ_TARJETA      IN NUMBER,
                                P_TARJ_NRO_TARJETA  IN VARCHAR2,
                                P_TARJ_FEC_VTO      IN DATE,
                                P_TARJ_IMP_LOC      IN NUMBER,
                                P_CONF_PER_ACT_INI  IN DATE,
                                P_CONF_PER_SGTE_FIN IN DATE,
                                P_FEC_INIC_SISTEMA  IN DATE,
                                P_FEC_FIN_SISTEMA   IN DATE,
                                P_DOC_FEC_DOC       IN DATE) IS
  BEGIN
    IF P_TARJ_FEC_VTO IS NULL THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'LA FECHA DE VENCIMIENTO NO PUEDE SER NULO!',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF;
    IF P_TARJ_TARJETA IS NULL THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'LA TARJETA NO PUEDE SER NULO!',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF;
    IF P_TARJ_NRO_TARJETA IS NULL THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL NUMERO DE LA TARJETA NO PUEDE SER NULO!',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF;
    IF P_TARJ_IMP_LOC IS NULL THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL IMPORTE NO PUEDE SER NULO!',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF;
    IF NOT P_TARJ_FEC_VTO BETWEEN P_CONF_PER_ACT_INI AND
       P_CONF_PER_SGTE_FIN THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'LA FECHA DEBE ESTAR COMPRENDIDA ENTRE EL ' ||
                                                 TO_CHAR(P_FEC_INIC_SISTEMA,
                                                         'DD/MM/YYYY') ||
                                                 ' Y EL ' ||
                                                 TO_CHAR(P_FEC_FIN_SISTEMA,
                                                         'DD/MM/YYYY'),
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    
      /*  RAISE_APPLICATION_ERROR(-20002,'LA FECHA DEBE ESTAR COMPRENDIDA ENTRE EL '||
               TO_CHAR(P_FEC_INIC_SISTEMA,'DD/MM/YYYY')||
      ' Y EL '||TO_CHAR(P_FEC_FIN_SISTEMA,'DD/MM/YYYY'));*/
    END IF;
  
    --FECHA DE VTO NO DEBE SER MENOR A LA DEL DOCUMENTO
    IF P_TARJ_FEC_VTO < P_DOC_FEC_DOC THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'FECHA DE VENCIMIENTO NO PUEDE SER MENOR QUE LA FECHA DEL DOCUMENTO.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    
      -- RAISE_APPLICATION_ERROR(-20002,'FECHA DE VENCIMIENTO NO PUEDE SER MENOR QUE LA FECHA DEL DOCUMENTO.');
    END IF;
  
    -- NI MAYOR A RENDICION+30
    IF P_TARJ_FEC_VTO > (P_DOC_FEC_DOC + 30) THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'FECHA DE VENCIMIENTO NO PUEDE MAS DE 30 DIAS DESPUES DE LA RENDICION.',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    
      -- RAISE_APPLICATION_ERROR(-20002,'FECHA DE VENCIMIENTO NO PUEDE MAS DE 30 DIAS DESPUES DE LA RENDICION.');
    END IF;
  
  END PP_VALIDAR_TARJETAS;

  PROCEDURE PP_VALIDAR_CUOTAS(P_CANT_CUOTAS       IN NUMBER,
                              P_FEC_PRIM_VTO      IN DATE,
                              P_TIPO_VENCIMIENTO  IN VARCHAR2,
                              P_DIAS_ENTRE_CUOTAS IN NUMBER,
                              P_MON_DEC_IMP       IN NUMBER) IS
  BEGIN
  
    --VALIDAR FECHA
    /*  IF TO_CHAR(P_FEC_PRIM_VTO, 'MM/YYYY') < TO_CHAR(P_FECHA_DOC, 'MM/YYYY') THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'EL PRIMER VENCIMIENTO, NO PUEDE SER MENOR QUE LA FECHA DE LA FACTURA!',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF;*/
  
    --VALIDAR CANTIDAD DE CUOTAS SELECCIONADA
  
    IF NVL(P_CANT_CUOTAS, 0) < 1 THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'DEBE INGRESAR LA CANTIDAD DE CUOTAS QUE DESEA GENERAR!',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF;
  
    --VALIDAR FECHA DEL PRIMER VENCIMIENTO
    IF P_FEC_PRIM_VTO IS NULL THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'DEBE INGRESAR LA FECHA DEL PRIMER VENCIMIENTO!',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF;
  
    --VALIDAR FECHA DEL PRIMER VENCIMIENTO, QUE NO SEA MENOR A LA FECHA ACTUAL. 06/09/2019
    IF P_FEC_PRIM_VTO < TO_DATE(SYSDATE, 'DD/MM/YYYY') THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'LA FECHA DE VENCIMIENTO DE LA CUOTA, NO PUEDE SER MENOR A LA FECHA ACTUAL!',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF; --------------------***** NO OLVIDAR DESCOMENTAR lv
  
    --VALIDAR TIPO DE VENCIMIENTO
    IF P_CANT_CUOTAS > 1 THEN
      IF NVL(P_TIPO_VENCIMIENTO, ' ') NOT IN ('F', 'V') THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'TIPO DE VENCIMIENTO DEBE SER F=FIJO, V=VARIABLE!',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      END IF;
    END IF;
  
    --VALIDAR LOS DIAS ENTRE UNA CUOTA Y LA SIGUIENTE
    --SOLO SI S_TIPO_VENCIMIENTO = 'V' (VARIABLE)
    --Y SI LA CANTIDAD DE CUOTAS ES MAYOR QUE 1
    IF P_CANT_CUOTAS > 1 AND P_TIPO_VENCIMIENTO = 'V' THEN
      IF P_DIAS_ENTRE_CUOTAS IS NULL THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'DEBE INGRESAR LOS DIAS ENTRE UNA CUOTA Y LA OTRA!',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      END IF;
      IF P_DIAS_ENTRE_CUOTAS <= 0 THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'LOS DIAS ENTRE CUOTAS DEBE SER UN NUMERO > QUE CERO!',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      END IF;
    END IF;
  
  END PP_VALIDAR_CUOTAS;

  PROCEDURE PP_ADICIONAR_CUOTA(P_CUO_FEC_VTO IN DATE,
                               P_CUO_IMP_MON IN NUMBER) IS
  BEGIN
    IF NOT
        APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => 'CUOTA_FACI039') THEN
      APEX_COLLECTION.CREATE_COLLECTION(P_COLLECTION_NAME => 'CUOTA_FACI039');
    END IF;
    APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'CUOTA_FACI039',
                               P_N001            => P_CUO_IMP_MON,
                               P_D001            => P_CUO_FEC_VTO);
  
  END PP_ADICIONAR_CUOTA;

  PROCEDURE PP_GENERAR_CUOTAS(P_CANT_CUOTAS       IN NUMBER,
                              P_FEC_PRIM_VTO      IN DATE,
                              P_TIPO_VENCIMIENTO  IN VARCHAR2,
                              P_DIAS_ENTRE_CUOTAS IN NUMBER,
                              P_MON_DEC_IMP       IN NUMBER,
                              P_TOTAL             IN NUMBER,
                              P_FUNCIONARIO       IN NUMBER DEFAULT 0,
                              P_LIMITE_CREDITO    IN NUMBER DEFAULT NULL,
                              P_MONEDA            IN NUMBER DEFAULT 1,
                              P_FEC_DOC           IN DATE DEFAULT NULL) IS
    I             NUMBER;
    V_FECHA       DATE := P_FEC_PRIM_VTO;
    V_CUO_IMP     NUMBER := ROUND(P_TOTAL / P_CANT_CUOTAS, P_MON_DEC_IMP);
    V_TOT_CUO_IMP NUMBER := 0;
    V_CUO_FEC_VTO DATE;
    V_TASA        NUMBER;
  BEGIN
  
    FOR I IN 1 .. P_CANT_CUOTAS LOOP
      V_CUO_FEC_VTO := V_FECHA;
      --:BCUO_DET.S_CUO_FEC_VTO := TO_CHAR(V_FECHA, 'DD/MM/YYYY');
      IF P_TIPO_VENCIMIENTO = 'V' THEN
        V_FECHA := V_FECHA + P_DIAS_ENTRE_CUOTAS;
      ELSE
        V_FECHA := ADD_MONTHS(P_FEC_PRIM_VTO, I);
      END IF;
      --:BCUO_DET.CUO_IMP_MON := V_CUO_IMP;
    
      --AJUSTAR LA DIFERENCIA POR REDONDEO A LA ULTIMA CUOTA;
      IF I = P_CANT_CUOTAS THEN
        V_CUO_IMP := (P_TOTAL - V_TOT_CUO_IMP);
      END IF;
      --*
    
      V_TOT_CUO_IMP := V_TOT_CUO_IMP + V_CUO_IMP;
      -----------------------------------------------LV VALIDAR SI ES FUNCIONARIO
    
      --- RAISE_APPLICATION_ERROR (-20001,P_FUNCIONARIO||' -- '||P_LIMITE_CREDITO);
      IF P_FUNCIONARIO = 1 THEN
        IF P_MONEDA = 1 THEN
          V_TASA := 1;
        ELSE
          V_TASA := FACI039.FP_COTIZACION(P_MONEDA, P_FEC_DOC, 1);
        END IF;
      
        IF V_CUO_IMP * V_TASA > P_LIMITE_CREDITO THEN
          RAISE_APPLICATION_ERROR(-20001,
                                  'EL MONTO DE LA CUOTA A SUPERADO EL LIMITE DE CREDITO MENSUAL DE GS. ' ||
                                  P_LIMITE_CREDITO);
        END IF;
      END IF;
      PP_ADICIONAR_CUOTA(P_CUO_FEC_VTO => V_CUO_FEC_VTO,
                         P_CUO_IMP_MON => V_CUO_IMP);
    END LOOP;
  
    --AJUSTAR LA DIFERENCIA POR REDONDEO A LA ULTIMA CUOTA;
    -- V_CUO_IMP := V_CUO_IMP + (P_TOTAL - V_TOT_CUO_IMP);
    /*PP_ADICIONAR_CUOTA(   P_CUO_FEC_VTO       => V_CUO_FEC_VTO,
    P_CUO_IMP_MON       => V_CUO_IMP);*/
  
  END PP_GENERAR_CUOTAS;

  PROCEDURE PP_VAL_CUOTA_PAGO(P_TIPO_FACTURA IN NUMBER, P_TOTAL IN NUMBER) IS
    V_NUM_ITEMS     NUMBER := 0;
    V_NUM_CUOTAS    NUMBER := 0;
    V_NUM_CHEQUES   NUMBER := 0;
    V_NUM_TARJETAS  NUMBER := 0;
    V_TOTAL_CHEQUE  NUMBER;
    V_TOTAL_TARJETA NUMBER;
    V_TOTAL_CUOTAS  NUMBER;
  BEGIN
  
    /* SELECT COUNT(1)
     INTO V_NUM_ITEMS
     FROM APEX_COLLECTIONS
    WHERE COLLECTION_NAME = 'ITEMS_FACI039';*/
  
    V_NUM_ITEMS := APEX_COLLECTION.COLLECTION_MEMBER_COUNT(P_COLLECTION_NAME => 'ITEMS_FACI039');
  
    IF V_NUM_ITEMS = 0 THEN
      APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'POR FAVOR ADICIONE ARTICULOS A LA VENTA',
                           P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
    END IF;
  
    IF P_TIPO_FACTURA = 1 THEN
      -- CONTADO
      SELECT COUNT(1), SUM(N003 * N004) --SUM(IMPORTE * TASA)
        INTO V_NUM_CHEQUES, V_TOTAL_CHEQUE
        FROM APEX_COLLECTIONS
       WHERE COLLECTION_NAME = 'CHEQUE_FACI039';
    
      SELECT COUNT(1), SUM(N002) --SUM(IMPORTE)
        INTO V_NUM_TARJETAS, V_TOTAL_TARJETA
        FROM APEX_COLLECTIONS
       WHERE COLLECTION_NAME = 'TARJETA_FACI039';
    
      /*IF V_NUM_TARJETAS = 0 AND V_NUM_CHEQUES = 0 THEN
          APEX_ERROR.ADD_ERROR (P_MESSAGE          => 'POR FAVOR DETERMINE UNA FORMA DE PAGO',
                                P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION );
      END IF;*/
    
      IF V_TOTAL_CHEQUE != P_TOTAL THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'LA SUMA DE LOS CHEQUES ES DIFERENTE AL TOTAL DE LA FACTURA',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      END IF;
    
      IF V_TOTAL_TARJETA != P_TOTAL THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'LA SUMA DE LAS TARJETAS ES DIFERENTE AL TOTAL DE LA FACTURA',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      END IF;
    ELSIF P_TIPO_FACTURA = 2 THEN
      -- CREDITO
      SELECT COUNT(1), SUM(N001) -- CUO_IMP_MON
        INTO V_NUM_CUOTAS, V_TOTAL_CUOTAS
        FROM APEX_COLLECTIONS
       WHERE COLLECTION_NAME = 'CUOTA_FACI039';
    
      IF V_NUM_CUOTAS = 0 THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'POR FAVOR DETERMINE LAS CUOTAS',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      END IF;
    
      IF V_TOTAL_CUOTAS != P_TOTAL THEN
        APEX_ERROR.ADD_ERROR(P_MESSAGE          => 'LA SUMA DE LAS CUOTAS ES DIFERENTE AL TOTAL DE LA FACTURA',
                             P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION);
      END IF;
    
    END IF;
  END PP_VAL_CUOTA_PAGO;

  PROCEDURE PP_TERMINAR_PROCESO(I_DOC_OPERADOR IN NUMBER,
                                I_DOC_NRO_DOC  IN NUMBER,
                                P_EMPRESA      IN NUMBER,
                                P_IMPRESORA    IN NUMBER) IS
  BEGIN
    BEGIN
      --PP_UNLOCK_IMPRESORA
      IF I_DOC_OPERADOR = '1' THEN
        UPDATE GEN_IMPRESORA
           SET IMP_ULT_FACT_OPER1 = I_DOC_NRO_DOC
         WHERE IMPR_IP = OWA_UTIL.GET_CGI_ENV('REMOTE_ADDR') --IMP_CODIGO = P_IMPRESORA
           AND IMP_EMPR = P_EMPRESA;
      ELSE
        UPDATE GEN_IMPRESORA
           SET IMP_ULT_FACT = I_DOC_NRO_DOC
         WHERE IMPR_IP = OWA_UTIL.GET_CGI_ENV('REMOTE_ADDR') --IMP_CODIGO = P_IMPRESORA
           AND IMP_EMPR = P_EMPRESA;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20002,
                                'NO SE PUEDE DESBLOQUAR LA IMPRESORA!.');
    END;
  
    BEGIN
      PP_VACIAR_COLLECTION;
    
    EXCEPTION
      WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20002,
                                'SE PRESENTO UN ERROR AL TERMINAR EL PROCESO');
    END;
  
  END PP_TERMINAR_PROCESO;

  PROCEDURE PP_ESTABLECER_IMPORTES_LOC(I_DOC_CLAVE IN NUMBER,
                                       I_EMPRESA   IN NUMBER,
                                       I_TASA      IN NUMBER) IS
  BEGIN
    UPDATE FIN_DOCUMENTO D
       SET D.DOC_BRUTO_EXEN_LOC = ROUND(D.DOC_BRUTO_EXEN_LOC * I_TASA, 2),
           D.DOC_BRUTO_GRAV_LOC = ROUND(D.DOC_BRUTO_GRAV_LOC * I_TASA, 2),
           D.DOC_NETO_EXEN_LOC  = ROUND(D.DOC_NETO_EXEN_LOC * I_TASA, 2),
           D.DOC_NETO_GRAV_LOC  = ROUND(D.DOC_NETO_GRAV_LOC * I_TASA, 2),
           D.DOC_IVA_LOC        = ROUND(D.DOC_IVA_LOC * I_TASA, 2),
           D.DOC_GRAV_10_LOC    = ROUND(D.DOC_GRAV_10_LOC * I_TASA, 2),
           D.DOC_GRAV_5_LOC     = ROUND(D.DOC_GRAV_5_LOC * I_TASA, 2),
           D.DOC_IVA_10_LOC     = ROUND(D.DOC_IVA_10_LOC * I_TASA, 2),
           D.DOC_IVA_5_LOC      = ROUND(D.DOC_IVA_5_LOC * I_TASA, 2)
     WHERE D.DOC_CLAVE = I_DOC_CLAVE
       AND D.DOC_EMPR = I_EMPRESA;
  
    UPDATE FAC_DOCUMENTO_DET G
       SET G.DET_BRUTO_LOC = ROUND(G.DET_BRUTO_MON * I_TASA, 2),
           G.DET_IVA_LOC   = ROUND(G.DET_IVA_MON * I_TASA, 2),
           G.DET_NETO_LOC  = ROUND(G.DET_NETO_MON * I_TASA, 2)
     WHERE G.DET_CLAVE_DOC = I_DOC_CLAVE
       AND G.DET_EMPR = I_EMPRESA;
  
    UPDATE FIN_DOC_CONCEPTO E
       SET E.DCON_EXEN_LOC = ROUND(E.DCON_EXEN_MON * I_TASA, 2),
           E.DCON_GRAV_LOC = ROUND(E.DCON_GRAV_MON * I_TASA, 2),
           E.DCON_IVA_LOC  = ROUND(E.DCON_IVA_MON * I_TASA, 2)
     WHERE E.DCON_CLAVE_DOC = I_DOC_CLAVE
       AND E.DCON_EMPR = I_EMPRESA;
  
    UPDATE FIN_CUOTA F
       SET F.CUO_IMP_LOC = ROUND(F.CUO_IMP_MON * I_TASA, 2)
     WHERE F.CUO_CLAVE_DOC = I_DOC_CLAVE
       AND F.CUO_EMPR = I_EMPRESA;
  
    UPDATE STK_DOCUMENTO H
       SET H.DOCU_GRAV_NETO_LOC  = ROUND(H.DOCU_GRAV_NETO_MON * I_TASA, 2),
           H.DOCU_EXEN_NETO_LOC  = ROUND(H.DOCU_EXEN_NETO_MON * I_TASA, 2),
           H.DOCU_GRAV_BRUTO_LOC = ROUND(H.DOCU_GRAV_BRUTO_MON * I_TASA, 2),
           H.DOCU_EXEN_BRUTO_LOC = ROUND(H.DOCU_EXEN_BRUTO_MON * I_TASA, 2),
           H.DOCU_IVA_LOC        = ROUND(H.DOCU_IVA_MON * I_TASA, 2)
     WHERE H.DOCU_CLAVE = I_DOC_CLAVE
       AND H.DOCU_EMPR = I_EMPRESA;
  
    UPDATE STK_DOCUMENTO_DET I
       SET I.DETA_IMP_NETO_LOC  = ROUND(I.DETA_IMP_NETO_MON * I_TASA, 2),
           I.DETA_IVA_LOC       = ROUND(I.DETA_IVA_MON * I_TASA, 2),
           I.DETA_IMP_BRUTO_LOC = ROUND(I.DETA_IMP_BRUTO_MON * I_TASA, 2)
     WHERE I.DETA_CLAVE_DOC = I_DOC_CLAVE
       AND I.DETA_EMPR = I_EMPRESA;
  
  END PP_ESTABLECER_IMPORTES_LOC;

  PROCEDURE PP_RECALCULAR_PRECIO(IO_PRECIO                  IN OUT NUMBER,
                                 I_IMPU_PORC_BASE_IMPONIBLE IN NUMBER,
                                 I_IMPU_PORCENTAJE          IN NUMBER) IS
    V_PRECIO_CON_IVA NUMBER := 0;
    V_PREC_UNITARIO  NUMBER := 0;
    V_AUX            NUMBER := 0;
  BEGIN
    IF I_IMPU_PORC_BASE_IMPONIBLE IS NULL AND I_IMPU_PORCENTAJE IS NULL THEN
      RAISE_APPLICATION_ERROR(-20002,
                              'EL ARTICULO NO TIENE REGISTRADO EL VALOR DEL IMPUESTO');
    ELSE
      V_PRECIO_CON_IVA := IO_PRECIO * (-1);
      V_AUX            := (1 + ((I_IMPU_PORC_BASE_IMPONIBLE / 100) *
                          (I_IMPU_PORCENTAJE / 100)));
      V_PREC_UNITARIO  := V_PRECIO_CON_IVA / V_AUX;
      IO_PRECIO        := V_PREC_UNITARIO;
    END IF;
  
  END PP_RECALCULAR_PRECIO;

  PROCEDURE PP_VALIDAR_PRECIO(IO_PRECIO                  IN OUT NUMBER,
                              I_ART_UNID_MED             IN VARCHAR2,
                              I_UNIDAD_VTA               IN VARCHAR2,
                              I_IMPU_PORC_BASE_IMPONIBLE IN NUMBER,
                              I_IMPU_PORCENTAJE          IN NUMBER,
                              I_ART_FACTOR_CONVERSION    IN NUMBER,
                              O_AREM_PRECIO_VTA          OUT NUMBER) IS
  
    --V_IVA   NUMBER;
    --V_TOTAL NUMBER;
  BEGIN
    IF IO_PRECIO IS NULL THEN
      IO_PRECIO := 0;
    END IF;
  
    IF I_ART_UNID_MED = 'KG' AND I_UNIDAD_VTA = 'U' THEN
      O_AREM_PRECIO_VTA := IO_PRECIO / NVL(I_ART_FACTOR_CONVERSION, 1);
      IF IO_PRECIO < 0 THEN
        PP_RECALCULAR_PRECIO(IO_PRECIO                  => IO_PRECIO,
                             I_IMPU_PORC_BASE_IMPONIBLE => I_IMPU_PORC_BASE_IMPONIBLE,
                             I_IMPU_PORCENTAJE          => I_IMPU_PORCENTAJE);
      END IF;
    ELSE
      O_AREM_PRECIO_VTA := IO_PRECIO;
      IF IO_PRECIO < 0 THEN
        PP_RECALCULAR_PRECIO(IO_PRECIO                  => IO_PRECIO,
                             I_IMPU_PORC_BASE_IMPONIBLE => I_IMPU_PORC_BASE_IMPONIBLE,
                             I_IMPU_PORCENTAJE          => I_IMPU_PORCENTAJE);
      END IF;
    END IF;
  
    --RAISE_APPLICATION_ERROR(-20010,'PRECIO = '||IO_PRECIO||'  O_AREM_PRECIO_VTA = ' || O_AREM_PRECIO_VTA);
  END PP_VALIDAR_PRECIO;

  PROCEDURE PP_ACTUALIZAR_AUTORIZ_ESPEC(P_EMPRESA     IN NUMBER,
                                        P_CLIENTE     IN NUMBER,
                                        P_DOC_FEC_DOC IN DATE,
                                        P_MONEDA      IN NUMBER,
                                        P_LIST_NEGRA  IN VARCHAR2 DEFAULT NULL) IS
  BEGIN
  
    IF P_LIST_NEGRA IS NULL THEN
      UPDATE FIN_AUTORIZ_ESPEC
         SET AUES_UTILIZADA = 'S'
       WHERE AUES_CLI = P_CLIENTE
         AND AUES_FEC_AUTORIZ =
             TO_DATE(TO_CHAR(P_DOC_FEC_DOC, 'DD/MM/YYYY'), 'DD/MM/YYYY')
         AND AUES_MON = P_MONEDA
         AND AUES_EMPR = P_EMPRESA
         AND AUES_FAC_CONT_LN IS NULL; ---QUE NO SEA POR LISTA NEGRA
    
    ELSIF P_LIST_NEGRA = 'S' THEN
      UPDATE FIN_AUTORIZ_ESPEC
         SET AUES_UTILIZADA = 'S'
       WHERE AUES_CLI = P_CLIENTE
         AND AUES_FEC_AUTORIZ =
             TO_DATE(TO_CHAR(P_DOC_FEC_DOC, 'DD/MM/YYYY'), 'DD/MM/YYYY')
         AND AUES_MON = P_MONEDA
         AND AUES_EMPR = P_EMPRESA
         AND AUES_FAC_CONT_LN IN ('S', 'C');
    
      UPDATE FIN_AUTORIZ_ESPEC_TMP
         SET AUES_UTILIZADA = 'S'
       WHERE AUES_CLI = P_CLIENTE
         AND AUES_FEC_AUTORIZ =
             TO_DATE(TO_CHAR(P_DOC_FEC_DOC, 'DD/MM/YYYY'), 'DD/MM/YYYY')
         AND AUES_MON = P_MONEDA
         AND AUES_EMPR = P_EMPRESA
         AND AUES_FAC_CONT_LN IN ('S', 'C');
    
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;

  PROCEDURE PP_CONTROLAR_LIMITE(IMPORTE                IN NUMBER,
                                I_DOC_FEC_DOC          IN DATE,
                                I_DOC_CLI              IN NUMBER,
                                I_IMP_LIM_CR_DISP_EMPR IN NUMBER,
                                I_EMPRESA              IN NUMBER,
                                I_DOC_MON              IN NUMBER,
                                O_LIM_AUTORIZADO       OUT VARCHAR2) IS
    V_TASA NUMBER;
    --V_LIM_CR_ESP       NUMBER;
    --V_TOTAL_DISPONIBLE NUMBER; --SALDO DISPONIBLE DEL LIMITE DE CREDITO DEL CLIENTE + LOS CHEQUES DIFERIDOS DEL MISMO
    V_DIF_DISP_ESP    NUMBER; --DIFERENCIA ENTRE EL TOTAL DEL DOCUMENTO Y EL TOTAL DISPONIBLE
    W_LIM_CR_ESPECIAL NUMBER;
  BEGIN
  
    IF I_DOC_MON = 1 THEN
      V_TASA := 1;
    ELSE
      V_TASA := FACI039.FP_COTIZACION(I_DOC_MON, I_DOC_FEC_DOC, I_EMPRESA);
    END IF;
  
    --:BDOC_ENCA.W_IMP_LIM_CR_DISP_EMPR := (:BDOC_ENCA.W_IMP_LIM_CR_DISP_EMPR + :BDOC_ENCA.W_IMP_CHEQ_DIFERIDO);
    --V_IMP_LIM_DISP_GRUPO
  
    IF (IMPORTE * V_TASA) > I_IMP_LIM_CR_DISP_EMPR THEN
      -- SI EL IMPORTE DE LA FACTURA ES MAYOR QUE LA DIFERENCIA ENTRE
      -- EL LIMITE DE CREDITO ESPECIAL DEL CLIENTE Y EL SALDO DEL LIMITE DE CREDITO COMUN
      V_DIF_DISP_ESP := (IMPORTE * V_TASA) - I_IMP_LIM_CR_DISP_EMPR;
    
      SELECT NVL(SUM(AUES_IMP), 0)
        INTO W_LIM_CR_ESPECIAL
        FROM FIN_AUTORIZ_ESPEC
       WHERE AUES_CLI = I_DOC_CLI
         AND AUES_FEC_AUTORIZ = I_DOC_FEC_DOC -- FELIX 22/05/2014 TO_DATE(TO_CHAR(:BDOC_ENCA.DOC_FEC_DOC,'DD/MM/YYYY'),'DD/MM/YYYY')
         AND AUES_MON = I_DOC_MON
         AND AUES_UTILIZADA = 'N'
         AND AUES_FAC_CONT_LN IS NULL -------------------ES PARA SABER QUE NO ES POR LISTA NEGRA
         AND AUES_EMPR = I_EMPRESA;
    
      -- IF I_DOC_CLI <> 111321 THEN
    
      IF V_DIF_DISP_ESP > (W_LIM_CR_ESPECIAL * V_TASA) THEN
        O_LIM_AUTORIZADO := 'N';
        /*RAISE_APPLICATION_ERROR(-20001,'LA FACTURA ES MAYOR AL LIM.DE CRED.: '||
        TO_CHAR(I_IMP_LIM_CR_DISP_EMPR)||
        ', Y LA AUTORIZACION ESPECIAL NO EXISTE O '||
        'ES INSUFICIENTE!');*/
      
      ELSE
        O_LIM_AUTORIZADO := 'S';
      END IF;
      --ELSE
      --O_LIM_AUTORIZADO := 'S';
      --END IF;
    
    ELSE
      O_LIM_AUTORIZADO := 'S';
    END IF;
  
  END;

  PROCEDURE PP_VAL_FACT_PEND_COBRO AS
    V_CANT NUMBER := 0;
  BEGIN
    --  RAISE_APPLICATION_ERROR(-20010,GEN_DEVUELVE_USER);
    /*SELECT COUNT(*)
     INTO V_CANT
     FROM FIN_FINI047_FAC_V t--STKI050_FACT_PEND_V T
    WHERE TRUNC(DOC_FEC_GRAB) < TRUNC(SYSDATE) - 4
      AND T.DOCU_OCARGA_LONDON_FAC IS NULL
      AND T.DOC_LOGIN_FAC = GEN_DEVUELVE_USER;*/
    SELECT COUNT(1)
      INTO V_CANT
      FROM STKI050_FACT_PEND_V T
     WHERE TRUNC(DOC_FEC_GRAB) < TRUNC(SYSDATE) - 4
       AND T.DOCU_OCARGA_LONDON_FAC IS NULL
       AND T.DOC_LOGIN_FAC = V('APP_USER');
  
    IF V_CANT > 0 THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'NO PUEDE REALIZAR LA OPERACION TIENE ' ||
                              V_CANT ||
                              ' FACTURA/AS CON MAS DE TRES DIAS PENDIENTE DE COBRO.');
    END IF;
  
  END PP_VAL_FACT_PEND_COBRO;

  PROCEDURE PP_AUTORIZACION_ESPECIAL(P_EMPRESA    IN NUMBER,
                                     P_CLIENTE    IN NUMBER,
                                     P_LISTA_NEG  IN VARCHAR2,
                                     P_SOL_ESTADO OUT NUMBER,
                                     P_TIPO_MV    IN NUMBER,
                                     P_MONEDA     IN NUMBER) IS
    ----------------------------29/07/2020 LV
    AUTORIZACION VARCHAR2(20);
    X_OPCION     VARCHAR2(1);
  BEGIN
  
    IF P_TIPO_MV = 1 THEN
      X_OPCION := 'S';
    ELSE
      X_OPCION := 'C';
    END IF;
    --RAISE_APPLICATION_ERROR (-20001,X_OPCION );
    BEGIN
      SELECT DECODE(AUES_AUTORIZACION,
                    'A',
                    'AUTORIZADA',
                    'R',
                    'RECHAZADO',
                    'POR REVISAR') AUTORIZACION
        INTO AUTORIZACION
        FROM FIN_AUTORIZ_ESPEC_TMP
       WHERE AUES_CLI = P_CLIENTE
            --  AND AUES_FAC_CONT_LN IN('C', 'S')
         AND AUES_UTILIZADA = 'N'
         AND AUES_EMPR = 1
         AND AUES_LOGIN = GEN_DEVUELVE_USER
            -- AND AUES_MON = P_MONEDA
         AND AUES_FAC_CONT_LN = X_OPCION
         AND AUES_FEC_AUTORIZ = TO_CHAR(SYSDATE, 'DD/MM/YYYY');
    
      IF AUTORIZACION = 'AUTORIZADA' THEN
        P_SOL_ESTADO := 2;
      
      ELSE
        RAISE_APPLICATION_ERROR(-20001,
                                'EL CLIENTE ESTA EN LISTA NEGRA, YA SE REALIZO LA SOLICITUD PARA VENTAS Y SU ESTADO ES: ' ||
                                AUTORIZACION);
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        P_SOL_ESTADO := 1;
      WHEN TOO_MANY_ROWS THEN
        BEGIN
          SELECT DECODE(AUES_AUTORIZACION,
                        'A',
                        'AUTORIZADA',
                        'R',
                        'RECHAZADO',
                        'POR REVISAR') AUTO
            INTO AUTORIZACION
            FROM FIN_AUTORIZ_ESPEC_TMP
           WHERE AUES_CLI = P_CLIENTE
             AND AUES_AUTORIZACION = 'A'
             AND AUES_UTILIZADA = 'N'
             AND AUES_LOGIN = GEN_DEVUELVE_USER
                --AND AUES_MON = P_MONEDA
             AND AUES_FAC_CONT_LN = X_OPCION
             AND AUES_FEC_AUTORIZ = TO_CHAR(SYSDATE, 'DD/MM/YYYY');
        
          IF AUTORIZACION = 'AUTORIZADA' THEN
            P_SOL_ESTADO := 2;
          END IF;
        EXCEPTION
          WHEN TOO_MANY_ROWS THEN
            RAISE_APPLICATION_ERROR(-20001,
                                    'ERROR - TIENE MAS DE UNA AUTORIZACI?N PARA EL CLIENTE');
          WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20001,
                                    'LA SOLICITUD AUN NO FUE APROBADA');
        END;
      
    END;
  
  END PP_AUTORIZACION_ESPECIAL;

  PROCEDURE PP_HISTORICO_LISTA_NEGRA(P_DOC_CLI       IN NUMBER, ------------------TODOS LOS QUE TENGAN HISTIORICO EN LN
                                     P_EMPRESA       IN NUMBER, ------------------PERDIR AUTORIZACION ESPECIAL
                                     P_MARCA         OUT VARCHAR2, ---------------YA SE PARA FACTURA CONTADO O CREDITO
                                     P_LISTA_NEGRA_H OUT VARCHAR2) IS
    MARCA   VARCHAR2(1);
    MARCA_H VARCHAR2(1);
  BEGIN
  
    MARCA := FAC_FUN_HOL_LISTA_NEGRA(P_DOC_CLI, P_EMPRESA);
  
    IF MARCA = 'S' THEN
      P_LISTA_NEGRA_H := 'N';
    
    ELSE
      MARCA_H := FAC_FUN_HOL_LISTA_NEGRA_HIST(P_DOC_CLI, P_EMPRESA);
      IF MARCA_H = 'S' THEN
        P_LISTA_NEGRA_H := 'S';
      ELSE
        P_LISTA_NEGRA_H := 'N';
      END IF;
    END IF;
  
    P_MARCA := MARCA_H;
  
  END PP_HISTORICO_LISTA_NEGRA;

  PROCEDURE PP_BLOQ_PROV_CLI(P_EMPRESA IN NUMBER, P_CLIENTE IN NUMBER) IS
  
    V_CANT_REG_PROV NUMBER;
    V_CANT_REG_CLI  NUMBER;
  BEGIN
    --------PEDIDO DEL SR UGO, NO SE PUEDE REALIZAR NINGUNA TRANSACCION SI EL CLIENTE CUENTA CON
    --------ADELANTOS VENCIDOS.. EXECPTO EL PAGO DE LOS MISMO--06/08/2020 LV
    IF P_EMPRESA = 1 THEN
      SELECT COUNT(*)
        INTO V_CANT_REG_PROV
        FROM FIN_DOCUMENTO   A,
             FIN_UNION_CUOTA B,
             FIN_UNION_PAGO  C,
             FIN_PROVEEDOR   D
       WHERE A.DOC_TIPO_MOV IN (31)
         AND A.DOC_EMPR = 1
         AND A.DOC_CLAVE = B.CUO_CLAVE_DOC
         AND A.DOC_EMPR = B.CUO_EMPR
         AND A.DOC_PROV = D.PROV_CODIGO(+)
         AND A.DOC_EMPR = D.PROV_EMPR(+)
         AND (B.CUO_CLAVE_DOC = C.PAG_CLAVE_DOC(+) AND
             B.CUO_FEC_VTO = C.PAG_FEC_VTO(+) AND
             B.CUO_EMPR = C.PAG_EMPR(+))
         AND B.CUO_FEC_VTO < SYSDATE
         AND TRUNC(CUO_SALDO_MON) > 0
         AND TRUNC(SYSDATE) - CUO_FEC_VTO > 0
         AND DOC_PROV IN
             (SELECT PROV_CODIGO
                FROM FIN_PROVEEDOR X
               WHERE PROV_EMPR = 1
                 AND PROV_RUC_DV || '-' || PROV_DV IN
                     (SELECT C.CLI_RUC_DV || '-' || C.CLI_DV
                        FROM FIN_CLIENTE C
                       WHERE CLI_EMPR = 1
                         AND CLI_RAMO <> 33
                         AND CLI_CODIGO = P_CLIENTE));
    
      IF V_CANT_REG_PROV > 0 THEN
        RAISE_APPLICATION_ERROR(-20001,
                                'EL CLIENTE CUENTA CON ' || V_CANT_REG_PROV ||
                                ' ADELANTO/S PROVEEDOR VENCIDO/S');
      END IF;
    
      SELECT COUNT(1)
        INTO V_CANT_REG_CLI
        FROM FIN_DOCUMENTO   A,
             FIN_UNION_CUOTA B,
             FIN_UNION_PAGO  C,
             FIN_CLIENTE     D
       WHERE A.DOC_TIPO_MOV IN (18)
         AND A.DOC_EMPR = 1
         AND A.DOC_CLAVE = B.CUO_CLAVE_DOC
         AND A.DOC_EMPR = B.CUO_EMPR
         AND A.DOC_CLI = D.CLI_CODIGO(+)
         AND A.DOC_EMPR = D.CLI_EMPR(+)
         AND (B.CUO_CLAVE_DOC = C.PAG_CLAVE_DOC(+) AND
             B.CUO_FEC_VTO = C.PAG_FEC_VTO(+) AND
             B.CUO_EMPR = C.PAG_EMPR(+))
         AND B.CUO_FEC_VTO < SYSDATE
         AND TRUNC(CUO_SALDO_MON) > 0
         AND TRUNC(SYSDATE) - CUO_FEC_VTO > 0
         AND CLI_RAMO <> 33
         AND CLI_CODIGO = P_CLIENTE;
    
      IF V_CANT_REG_CLI > 0 THEN
        RAISE_APPLICATION_ERROR(-20001,
                                'EL CLIENTE CUENTA CON ' || V_CANT_REG_CLI ||
                                ' ADELANTO/S CLIENTE VENCIDO/S');
      END IF;
    
    END IF;
  
  END PP_BLOQ_PROV_CLI;

  FUNCTION FP_CLI_ES_EMPL(I_CLI IN NUMBER, I_EMPRESA IN NUMBER)
    RETURN BOOLEAN IS
    V_COUNT NUMBER;
  BEGIN
    SELECT COUNT(*)
      INTO V_COUNT
      FROM PER_EMPLEADO E
     WHERE E.EMPL_COD_CLIENTE = I_CLI
       AND E.EMPL_EMPRESA = I_EMPRESA;
  
    IF V_COUNT = 0 THEN
      RETURN FALSE;
    ELSE
      RETURN TRUE;
    END IF;
  END FP_CLI_ES_EMPL;

  PROCEDURE PP_VAL_MAX_ATRASO(I_EMPRESA      IN NUMBER,
                              I_DOC_CLI      IN NUMBER,
                              I_DOC_FEC_DOC  IN DATE,
                              I_MON          IN NUMBER,
                              I_TIPO_FACTURA IN NUMBER) IS
    --V_CLI_MAX_ATRASO     NUMBER;
    --V_TOTAL_ATRASO       NUMBER;
    --V_AUT_ESP_MAX_ATRASO NUMBER;
    V_CONF_COD_FAC_CRED NUMBER := 10;
    V_MENSAJE           VARCHAR2(200);
  BEGIN
  
    IF I_TIPO_FACTURA = 2 THEN
      -- SI ES FACTURA CREDITO
      PP_VALIDAR_VENCIMIENTOS(P_VALIDAR           => 'VALIDAR',
                              P_EMPRESA           => I_EMPRESA,
                              P_DOC_CLI           => I_DOC_CLI,
                              P_DOC_MON           => I_MON,
                              P_DOC_FEC_DOC       => I_DOC_FEC_DOC,
                              P_CONF_COD_FAC_CRED => V_CONF_COD_FAC_CRED,
                              P_CONFIRMAR         => V_MENSAJE);
    END IF;
  
  END PP_VAL_MAX_ATRASO;

  PROCEDURE PP_BORRAR_DETALLE(I_SEQ IN NUMBER) AS
  BEGIN
    APEX_COLLECTION.DELETE_MEMBER(P_COLLECTION_NAME => 'ITEMS_FACI039',
                                  P_SEQ             => I_SEQ);
  
    APEX_COLLECTION.RESEQUENCE_COLLECTION(P_COLLECTION_NAME => 'ITEMS_FACI039');
  
  END PP_BORRAR_DETALLE;

  PROCEDURE PP_VACIAR_COLLECTION AS
  BEGIN
    IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => 'ITEMS_FACI039') THEN
      APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => 'ITEMS_FACI039');
    END IF;
    IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => 'TARJETA_FACI039') THEN
      APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => 'TARJETA_FACI039');
    END IF;
    IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => 'CHEQUE_FACI039') THEN
      APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => 'CHEQUE_FACI039');
    END IF;
    IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => 'CUOTA_FACI039') THEN
      APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => 'CUOTA_FACI039');
    END IF;
  END PP_VACIAR_COLLECTION;

  PROCEDURE PP_AUTORIZACION_LIMITE(P_EMPRESA    IN NUMBER,
                                   P_CLIENTE    IN NUMBER,
                                   P_MONEDA     IN NUMBER,
                                   P_SOL_ESTADO OUT NUMBER,
                                   P_IMP_SOL    OUT NUMBER) IS
    ----------------------------29/07/2020 LV
    AUTORIZACION VARCHAR2(20);
    X_OPCION     VARCHAR2(1);
    V_AUT_IMP    NUMBER;
  BEGIN
  
    BEGIN
      SELECT DECODE(AUES_AUTORIZACION,
                    'A',
                    'AUTORIZADA',
                    'R',
                    'RECHAZADO',
                    'POR REVISAR') AUTORIZACION,
             AUES_IMP
        INTO AUTORIZACION, V_AUT_IMP
        FROM FIN_AUTORIZ_ESPEC_TMP A
       WHERE AUES_CLI = P_CLIENTE
         AND AUES_UTILIZADA = 'N'
         AND AUES_EMPR = 1
         AND AUES_LOGIN = GEN_DEVUELVE_USER
         AND AUES_MON = P_MONEDA
         AND A.AUES_TIPO_AUT = 'D'
         AND AUES_FEC_AUTORIZ = TO_CHAR(SYSDATE, 'DD/MM/YYYY');
    
      IF AUTORIZACION = 'AUTORIZADA' THEN
        P_SOL_ESTADO := 2;
        P_IMP_SOL    := V_AUT_IMP;
      
      ELSE
        RAISE_APPLICATION_ERROR(-20001,
                                'YA SE REALIZO LA SOLICITUD, SU ESTADO ES: ' ||
                                AUTORIZACION);
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        --NULL;
      
        RAISE_APPLICATION_ERROR(-20001,
                                'El importe supera los dos millones, favor solicitar autorizacion en caso de seguir facturando');
      
      WHEN TOO_MANY_ROWS THEN
        BEGIN
          SELECT DECODE(AUES_AUTORIZACION,
                        'A',
                        'AUTORIZADA',
                        'R',
                        'RECHAZADO',
                        'POR REVISAR') AUTO,
                 AUES_IMP
            INTO AUTORIZACION, V_AUT_IMP
            FROM FIN_AUTORIZ_ESPEC_TMP
           WHERE AUES_CLI = P_CLIENTE
             AND AUES_AUTORIZACION = 'A'
             AND AUES_UTILIZADA = 'N'
             AND AUES_LOGIN = GEN_DEVUELVE_USER
             AND AUES_MON = P_MONEDA
             AND AUES_TIPO_AUT = 'D'
             AND AUES_FEC_AUTORIZ = TO_CHAR(SYSDATE, 'DD/MM/YYYY');
        
          IF AUTORIZACION = 'AUTORIZADA' THEN
            P_SOL_ESTADO := 2;
            P_IMP_SOL    := V_AUT_IMP;
          END IF;
        EXCEPTION
          WHEN TOO_MANY_ROWS THEN
            RAISE_APPLICATION_ERROR(-20001,
                                    'ERROR - TIENE MAS DE UNA AUTORIZACION PARA EL CLIENTE');
          WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20001,
                                    'LA SOLICITUD AUN NO FUE APROBADA');
        END;
      
    END;
  
  END PP_AUTORIZACION_LIMITE;

END FACI039;
/
