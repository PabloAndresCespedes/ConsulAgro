create or replace package PERM077 is

  -- Author  : PRO4
  -- Created : 11/01/2021 9:48:27
  -- Purpose : PERM077

  PROCEDURE PP_BUSCAR_EMPLEADO (I_EMPRESA   IN NUMBER,
                                I_LEGAJO    IN NUMBER,
                                O_PROVEEDOR OUT NUMBER);

  PROCEDURE PP_INSERTAR_AUTORIZACION (I_EMPRESA    IN NUMBER,
                                      I_AUTO       IN PER_AUT_ADELANTO%ROWTYPE);


  PROCEDURE PP_RECHAZAR_SOLICITUD (I_EMPRESA      IN NUMBER,
                                   I_COD_AUT      IN NUMBER,
                                   I_MOTIVO       IN VARCHAR2);


  PROCEDURE PP_APROBAR_SOLICITUD (I_EMPRESA  IN NUMBER);

   PROCEDURE PP_BUSCAR_AUTORIZACION (P_EMPRESA    IN NUMBER,
                                     P_LEGAJO     IN NUMBER,
                                     P_MONTO_HAB  OUT NUMBER,
                                     P_COD_AUTO   OUT NUMBER,
                                     P_FECHA      IN DATE,--,
                                     P_MONEDA    IN NUMBER DEFAULT NULL
                                       );


    PROCEDURE PP_MOD_ESTADO_AUTORIZACION (P_EMPRESA           IN NUMBER,
                                          P_NRO_AUTORIZACION  IN NUMBER);
end PERM077;
/
create or replace package body PERM077 is

  PROCEDURE PP_BUSCAR_EMPLEADO (I_EMPRESA    IN NUMBER,
                                I_LEGAJO     IN NUMBER,
                                O_PROVEEDOR  OUT NUMBER)IS

BEGIN
      SELECT E.EMPL_CODIGO_PROV
         INTO O_PROVEEDOR
        FROM PER_EMPLEADO E
        WHERE EMPL_LEGAJO = I_LEGAJO
        AND EMPL_EMPRESA = I_EMPRESA;
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
     RAISE_APPLICATION_ERROR (-20001, 'El empleado no existe');


 END PP_BUSCAR_EMPLEADO ;



  PROCEDURE PP_INSERTAR_AUTORIZACION (I_EMPRESA    IN NUMBER,
                                      I_AUTO       IN PER_AUT_ADELANTO%ROWTYPE) IS



  V_COD_AUTORIZACION  NUMBER;
  V_NRO_AUTO NUMBER;
  BEGIN

  SELECT SEQ_PER_AUT_ADEL_PROV.NEXTVAL
  INTO V_COD_AUTORIZACION
  FROM DUAL;
          SELECT COUNT(*) +1
           INTO V_NRO_AUTO
          FROM PER_AUT_ADELANTO
          WHERE AUT_EMPLEADO = I_AUTO.AUT_EMPLEADO
            AND AUT_EMPR = I_EMPRESA;


            INSERT INTO PER_AUT_ADELANTO
              (AUT_CODIGO,
               AUT_PROV,
               AUT_EMPLEADO,
               AUT_EMPR,
               AUT_FECHA,
               AUT_MONEDA,
               AUT_MONTO,
               AUT_OBSERVACION,
               AUT_ESTADO,
               AUT_UTILIZADA,
               AUT_LOGIN,
               AUT_FEC_GRA,
               AUT_NRO_AUTO)
            VALUES
              (V_COD_AUTORIZACION,
               I_AUTO.AUT_PROV,
               I_AUTO.AUT_EMPLEADO,
               I_EMPRESA,
               I_AUTO.AUT_FECHA,
               I_AUTO.AUT_MONEDA,
               I_AUTO.AUT_MONTO,
               I_AUTO.AUT_OBSERVACION,
               'P',
               'N',
               gen_devuelve_user,
               SYSDATE,
               V_NRO_AUTO);


  END PP_INSERTAR_AUTORIZACION;



  PROCEDURE PP_RECHAZAR_SOLICITUD  (I_EMPRESA      IN NUMBER,
                                    I_COD_AUT      IN NUMBER,
                                    I_MOTIVO       IN VARCHAR2) IS

   AUTORIZADO NUMBER;
  BEGIN
     SELECT COUNT(* )
      INTO  AUTORIZADO
      FROM GEN_OPERADOR_EMPRESA  T
      WHERE OPEM_IND_APROB_ADEL_PROV = 'S'
         AND OPEM_EMPR = I_EMPRESA
         AND OPEM_OPER = (SELECT OPER_CODIGO
                            FROM GEN_OPERADOR T
                            WHERE OPER_LOGIN  = GEN_DEVUELVE_USER);

  IF AUTORIZADO = 1 THEN
        UPDATE PER_AUT_ADELANTO
           SET
               AUT_ESTADO = 'R',
               AUT_LOGIN_APRO = GEN_DEVUELVE_USER,
               AUT_FEC_APROB = SYSDATE,
               AUT_OBS_RECHAZO = I_MOTIVO
         WHERE AUT_CODIGO = I_COD_AUT
           AND AUT_EMPR = 1;

   ELSE
    RAISE_APPLICATION_ERROR(-20001, 'El usuario no esta autorizado para rechazar la solicitud');
 END IF;

  END PP_RECHAZAR_SOLICITUD;

 PROCEDURE PP_APROBAR_SOLICITUD  (I_EMPRESA  IN NUMBER) IS


   AUTORIZADO NUMBER;
 BEGIN

     SELECT COUNT(* )
      INTO  AUTORIZADO
      FROM GEN_OPERADOR_EMPRESA  T
      WHERE OPEM_IND_APROB_ADEL_PROV = 'S'
         AND OPEM_EMPR = I_EMPRESA
         AND OPEM_OPER = (SELECT OPER_CODIGO
                            FROM GEN_OPERADOR T
                            WHERE OPER_LOGIN  = GEN_DEVUELVE_USER);


 IF AUTORIZADO = 1 THEN


    FOR I IN 1 .. APEX_APPLICATION.G_F01.COUNT LOOP


      UPDATE PER_AUT_ADELANTO
           SET
               AUT_ESTADO = 'A',
               AUT_LOGIN_APRO = GEN_DEVUELVE_USER,
               AUT_FEC_APROB = SYSDATE
         WHERE AUT_CODIGO = APEX_APPLICATION.G_F01(I)
           AND AUT_EMPR = I_EMPRESA;

    END LOOP;
 ELSE
    RAISE_APPLICATION_ERROR(-20001, 'El usuario no esta autorizado para aprobar la solicitud');
 END IF;

 END PP_APROBAR_SOLICITUD;


 PROCEDURE PP_BUSCAR_AUTORIZACION (P_EMPRESA    IN NUMBER,
                                   P_LEGAJO     IN NUMBER,
                                   P_MONTO_HAB  OUT NUMBER,
                                   P_COD_AUTO   OUT NUMBER,
                                   P_FECHA      IN DATE, --,
                                   P_MONEDA    IN NUMBER DEFAULT NULL
                                    )IS

   V_ESTADO_AUTORIZACION VARCHAR2(1);
   V_MONTO               NUMBER;
   V_COD_AUTO            NUMBER;
   V_MONEDA              NUMBER;
   V_TASA          number;
 BEGIN

   BEGIN

        SELECT AUT_ESTADO
            INTO V_ESTADO_AUTORIZACION
          FROM PER_AUT_ADELANTO
          WHERE AUT_EMPR      = P_EMPRESA
            AND AUT_FECHA     = P_FECHA---TRUNC(SYSDATE)
            AND AUT_UTILIZADA = 'N'
            AND AUT_ESTADO    IN ('P','A')
            AND AUT_LOGIN     = gen_devuelve_user
            AND AUT_EMPLEADO  =  P_LEGAJO
            AND AUT_MONEDA    = NVL(P_MONEDA,AUT_MONEDA);

                --     RAISE_APPLICATION_ERROR(-20001,V_ESTADO_AUTORIZACION);

           --  RAISE_APPLICATION_ERROR(-20001,V_ESTADO_AUTORIZACION);
   EXCEPTION WHEN NO_DATA_FOUND THEN
              NULL;
            WHEN TOO_MANY_ROWS THEN
               BEGIN
                 SELECT AUT_ESTADO
                    INTO V_ESTADO_AUTORIZACION
                  FROM PER_AUT_ADELANTO
                  WHERE AUT_EMPR      = P_EMPRESA
                    AND AUT_FECHA     = P_FECHA---TRUNC(SYSDATE)
                    AND AUT_UTILIZADA = 'N'
                    AND AUT_ESTADO    IN ('A')
                    AND AUT_LOGIN     = gen_devuelve_user
                    AND AUT_EMPLEADO  =  P_LEGAJO
                     AND AUT_MONEDA    = NVL(P_MONEDA,AUT_MONEDA);
                     
                 --  AND AUT_MONEDA    = P_MONEDA;
             EXCEPTION WHEN TOO_MANY_ROWS THEN
               RAISE_APPLICATION_ERROR(-20001, 'El empleado posee dos autorizaciones de l?neas de credito, favor verificar');

      WHEN no_data_found THEN
        RAISE_APPLICATION_ERROR(-20001,'El empleado no posee autorizacion para adelanto');

             END;
   END;

   IF V_ESTADO_AUTORIZACION  = 'P' THEN
     RAISE_APPLICATION_ERROR(-20001, 'El empleado posee una solicitud de limite de credito en estado :"En Espera"');
   END IF;

   IF V_ESTADO_AUTORIZACION  = 'A' THEN



     SELECT AUT_MONTO, AUT_CODIGO, A.AUT_MONEDA
       INTO V_MONTO, V_COD_AUTO, V_MONEDA
      FROM PER_AUT_ADELANTO A
      WHERE AUT_EMPR      = P_EMPRESA
        AND AUT_FECHA     = P_FECHA--TRUNC(SYSDATE)
        AND AUT_UTILIZADA = 'N'
        AND AUT_ESTADO    IN ('A')
        AND AUT_LOGIN     = gen_devuelve_user
        AND AUT_EMPLEADO  =  P_LEGAJO
       AND AUT_MONEDA    = NVL(P_MONEDA,AUT_MONEDA);
     --   AND AUT_MONEDA    = P_MONEDA;
       -- RAISE_APPLICATION_ERROR(-20001,V_MONTO);

          IF V_MONEDA = 1 THEN
              V_TASA := 1;
            ELSE
              V_TASA := FACI039.FP_COTIZACION(V_MONEDA, P_FECHA, 1);
            END IF;
      V_MONTO := V_MONTO *V_TASA;
   ELSE
     V_MONTO := NULL;
     V_COD_AUTO := NULL;
     V_MONEDA := NULL;
   END IF;



    P_MONTO_HAB := V_MONTO;
    P_COD_AUTO  := V_COD_AUTO;
    --P_MONEDA    := V_MONEDA;


     /*   IF gen_devuelve_user = 'DAVIDM' THEN
      RAISE_APPLICATION_ERROR(-20001, P_MONTO_HAB||''||P_COD_AUTO||'----'||P_LEGAJO);
     END IF;*/

 --RAISE_APPLICATION_ERROR(-20001, P_MONTO_HAB||'-'||P_COD_AUTO||'----'||P_LEGAJO);

 END PP_BUSCAR_AUTORIZACION;

 PROCEDURE PP_MOD_ESTADO_AUTORIZACION (P_EMPRESA            IN NUMBER,
                                       P_NRO_AUTORIZACION   IN NUMBER)IS


 BEGIN
      UPDATE PER_AUT_ADELANTO A
         SET AUT_UTILIZADA = 'S'
       WHERE A.AUT_CODIGO  = P_NRO_AUTORIZACION
        AND AUT_LOGIN      = gen_devuelve_user
        AND AUT_EMPR       = P_EMPRESA;

 END PP_MOD_ESTADO_AUTORIZACION;


end PERM077;
/
