create or replace package PERL004 is

  -- Author  : PRO4
  -- Created : 25/01/2021 15:21:53
  -- Purpose : PERL004

  PROCEDURE PP_MOSTRAR_PERIODO(I_EMPRESA   IN NUMBER,
                               I_PERIODO   IN NUMBER,
                               O_FEC_DESDE OUT DATE,
                               O_FEC_HASTA OUT DATE);

  PROCEDURE PP_MOSTRAR_EMPLEADO(I_EMPRESA     IN NUMBER,
                                I_EMPL_LEGAJO IN NUMBER,
                                O_EMPL_NOMBRE OUT VARCHAR2,
                                O_EMPL_CI     OUT NUMBER);

  PROCEDURE PP_MOSTRAR_CONCEPTO(I_EMPRESA  IN NUMBER,
                                I_CONCEPTO IN NUMBER,
                                O_CON_DESC OUT VARCHAR2);

  PROCEDURE PP_TRAER_FORMA_PAGO(I_EMPRESA    IN NUMBER,
                                I_FORMA_PAGO IN NUMBER,
                                O_PAGO_DESC  OUT VARCHAR2);

  PROCEDURE PP_TRAER_SUCURSAL(I_EMPRESA  IN NUMBER,
                              I_SUCURSAL IN NUMBER,
                              O_SUC_DESC OUT VARCHAR2);

  PROCEDURE PP_TRAER_MONEDA(I_EMPRESA      IN NUMBER,
                            I_MONEDA       IN NUMBER,
                            O_MON_DESC     OUT VARCHAR2,
                            O_CANT_DECIMAL OUT NUMBER);

  PROCEDURE PP_TRAER_DEPARTAMENTO(I_EMPRESA   IN NUMBER,
                                  I_DPTO      IN NUMBER,
                                  O_DPTO_DESC OUT VARCHAR2,
                                  I_SUCURSAL  IN NUMBER);

  PROCEDURE PP_RECIBO_SALARIO(P_EMPRESA      IN NUMBER,
                              P_PERIODO      IN NUMBER,
                              P_DESDE        IN DATE,
                              P_HASTA        IN DATE,
                              P_LEGAJO       IN NUMBER,
                              P_SUCURSAL     IN NUMBER,
                              P_FORMA_PAGO   IN NUMBER,
                              P_DEPARTAMENTO IN NUMBER,
                              P_MONEDA       IN NUMBER,
                              P_SESSION      IN NUMBER,
                              P_SITUACION    IN VARCHAR2,
                              P_TIPO         IN NUMBER,
                              P_INCLUIR      IN VARCHAR2,
                              P_CARGO        IN NUMBER,
                              P_GRUPO        IN VARCHAR2 DEFAULT 'N',
                              P_EXCLUIR_LEG  IN VARCHAR2);

  PROCEDURE PP_RECIBO_SALARIO_TAGRO(P_EMPRESA      IN NUMBER,
                                    P_PERIODO      IN NUMBER,
                                    P_DESDE        IN DATE,
                                    P_HASTA        IN DATE,
                                    P_CONCEPTO     IN NUMBER,
                                    P_LEGAJO       IN NUMBER,
                                    P_SUCURSAL     IN NUMBER,
                                    P_FORMA_PAGO   IN NUMBER,
                                    P_DEPARTAMENTO IN NUMBER,
                                    P_MONEDA       IN NUMBER,
                                    P_SESSION      IN NUMBER,
                                    P_SITUACION    IN VARCHAR2,
                                    P_TIPO         IN NUMBER,
                                    P_USUARIO      IN VARCHAR2,
                                    P_EXCLUIR_LEG  IN VARCHAR2);

  PROCEDURE PP_LLAMAR_REPORTE(P_EMPRESA      IN NUMBER,
                              P_SESSION      IN NUMBER,
                              P_USUARIO      IN VARCHAR2,
                              P_TIPO_REPORTE IN VARCHAR2);

  PROCEDURE PP_LLAMAR_REPORTE_TAGRO(P_EMPRESA      IN NUMBER,
                                    P_SESSION      IN NUMBER,
                                    P_USUARIO      IN VARCHAR2,
                                    P_TIPO_REPORTE IN VARCHAR2,
                                    P_FECHA_DESDE  IN DATE,
                                    P_FECHA_HASTA  IN DATE);

END PERL004;
/
create or replace package body PERL004 is

  PROCEDURE PP_MOSTRAR_PERIODO(I_EMPRESA   IN NUMBER,
                               I_PERIODO   IN NUMBER,
                               O_FEC_DESDE OUT DATE,
                               O_FEC_HASTA OUT DATE) IS

  BEGIN

    SELECT TO_CHAR(PERI_FEC_INI, 'DD/MM/YYYY'),
           TO_CHAR(PERI_FEC_FIN, 'DD/MM/YYYY')
      INTO O_FEC_DESDE, O_FEC_HASTA
      FROM PER_PERIODO
     WHERE PERI_CODIGO = I_PERIODO
       AND PERI_EMPR = I_EMPRESA;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      O_FEC_DESDE := NULL;
      O_FEC_HASTA := NULL;
      RAISE_APPLICATION_ERROR(-20001, 'Periodo inexistente!');
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001,
                              'Excepci?n WHEN OTHERS! en PP_MOSTRAR_PERIODO');

  END PP_MOSTRAR_PERIODO;

  PROCEDURE PP_MOSTRAR_EMPLEADO(I_EMPRESA     IN NUMBER,
                                I_EMPL_LEGAJO IN NUMBER,
                                O_EMPL_NOMBRE OUT VARCHAR2,
                                O_EMPL_CI     OUT NUMBER) IS
    v varchar2(100);
  BEGIN

    SELECT EMPL_NOMBRE || ' ' || EMPL_APE, empl_doc_ident
      INTO O_EMPL_NOMBRE, O_EMPL_CI
      FROM PER_EMPLEADO
     WHERE EMPL_LEGAJO = I_EMPL_LEGAJO
       AND EMPL_EMPRESA = I_EMPRESA;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20001, 'Empleado inexistente!.');
  END PP_MOSTRAR_EMPLEADO;

  PROCEDURE PP_MOSTRAR_CONCEPTO(I_EMPRESA  IN NUMBER,
                                I_CONCEPTO IN NUMBER,
                                O_CON_DESC OUT VARCHAR2) IS
  BEGIN
    SELECT PCON_DESC
      INTO O_CON_DESC
      FROM PER_CONCEPTO
     WHERE PCON_CLAVE = I_CONCEPTO
       AND PCON_EMPR = I_EMPRESA;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20001, 'Concepto no existe!');
  END PP_MOSTRAR_CONCEPTO;

  PROCEDURE PP_TRAER_FORMA_PAGO(I_EMPRESA    IN NUMBER,
                                I_FORMA_PAGO IN NUMBER,
                                O_PAGO_DESC  OUT VARCHAR2) IS
  BEGIN
    SELECT FORMA_DESC
      INTO O_PAGO_DESC
      FROM PER_FORMA_PAGO
     WHERE FORMA_CODIGO = I_FORMA_PAGO
       AND FORMA_EMPR = I_EMPRESA;
    --
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20001, 'Forma de Pago inexistente!');

  END PP_TRAER_FORMA_PAGO;

  PROCEDURE PP_TRAER_SUCURSAL(I_EMPRESA  IN NUMBER,
                              I_SUCURSAL IN NUMBER,
                              O_SUC_DESC OUT VARCHAR2) IS
  BEGIN
    SELECT SUC_DESC
      INTO O_SUC_DESC
      FROM GEN_SUCURSAL
     WHERE SUC_CODIGO = I_SUCURSAL
       AND SUC_EMPR = I_EMPRESA;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20001, 'Sucursal Inexistente');
  END PP_TRAER_SUCURSAL;

  PROCEDURE PP_TRAER_MONEDA(I_EMPRESA      IN NUMBER,
                            I_MONEDA       IN NUMBER,
                            O_MON_DESC     OUT VARCHAR2,
                            O_CANT_DECIMAL OUT NUMBER) IS
  BEGIN
    SELECT MON_DESC, MON_DEC_IMP
      INTO O_MON_DESC, O_CANT_DECIMAL
      FROM GEN_MONEDA
     WHERE MON_CODIGO = I_MONEDA
       AND MON_EMPR = I_EMPRESA;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20001, 'Moneda inexistente!');

  END PP_TRAER_MONEDA;

  PROCEDURE PP_TRAER_DEPARTAMENTO(I_EMPRESA   IN NUMBER,
                                  I_DPTO      IN NUMBER,
                                  O_DPTO_DESC OUT VARCHAR2,
                                  I_SUCURSAL  IN NUMBER) IS
  BEGIN
    SELECT G.DPTO_DESC
      INTO O_DPTO_DESC
      FROM GEN_DEPARTAMENTO G
     WHERE G.DPTO_CODIGO = I_DPTO
       AND G.DPTO_EMPR = I_EMPRESA
       AND G.DPTO_SUC = I_SUCURSAL;
    --
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20001,
                              'Departamento inexistente y/o debe seleccionar la sucursal!');

  END PP_TRAER_DEPARTAMENTO;

  PROCEDURE PP_RECIBO_SALARIO(P_EMPRESA      IN NUMBER,
                              P_PERIODO      IN NUMBER,
                              P_DESDE        IN DATE,
                              P_HASTA        IN DATE,
                              P_LEGAJO       IN NUMBER,
                              P_SUCURSAL     IN NUMBER,
                              P_FORMA_PAGO   IN NUMBER,
                              P_DEPARTAMENTO IN NUMBER,
                              P_MONEDA       IN NUMBER,
                              P_SESSION      IN NUMBER,
                              P_SITUACION    IN VARCHAR2,
                              P_TIPO         IN NUMBER,
                              P_INCLUIR      IN VARCHAR2,
                              P_CARGO        IN NUMBER,
                              P_GRUPO        IN VARCHAR2 DEFAULT 'N',
                              P_EXCLUIR_LEG  IN VARCHAR2) IS

    TYPE CURTYP IS REF CURSOR;
    C_PERL004 CURTYP;
    V_SQL     VARCHAR2(30000);
    V_SQL_2   VARCHAR2(30000);
    V_WHERE   VARCHAR2(30000);
    V_WHERE1  VARCHAR2(30000);
    V_WHERE2  VARCHAR2(30000);
    V_WHERE3  VARCHAR2(30000);
    V_SAL     VARCHAR2(30000);
    V_SAL_2     VARCHAR2(30000);

    TYPE CUR_TYPE IS RECORD(
      LEGAJO               VARCHAR2(200),
      NOMBRE               VARCHAR2(200),
      EMPL_DOC_IDENTIDAD   VARCHAR2(200),
      EMPL_DIR             VARCHAR2(200),
      FORMA_PAGO           VARCHAR2(200),
      EMPL_NRO_CTA         VARCHAR2(200),
      EMPL_BCO_PAGO        VARCHAR2(200),
      EMPL_TEL             VARCHAR2(200),
      FORMA_SALARIO_DES    VARCHAR2(200),
      EMPL_IND_HS_EXTRA    VARCHAR2(2),
      BANCO_DES            VARCHAR2(200),
      DIAS_TRABAJADO       VARCHAR2(200),
      SALARIO              VARCHAR2(200),
      NRO_RECIBO           VARCHAR2(200),
      TOTAL                VARCHAR2(200),
      MOSTAR_MARC          VARCHAR2(200),
      CARGO                NUMBER,
      salario_base         number,
      car_desc             VARCHAR2(200),
      CENTRAL              VARCHAR2(200),
      SUCU_CODI            VARCHAR2(200),
      SUCURSAL             VARCHAR2(200),
      SUC_IND_CASA_CENTRAL VARCHAR2(200)); /*,
                            PERIODO            NUMBER,
                            PER_INICIO         DATE,
                            PER_FIN            DATE,
                            MONEDA             NUMBER,
                            FECHA_LETRA        VARCHAR2(200),
                            MON_ISO            VARCHAR2(20),
                            ORDEN              NUMBER,
                            CONCEPTO           VARCHAR2(200),
                            CONC_DETALLE       VARCHAR2(200),
                            IMPORTE            NUMBER,
                            TIPO_CONCEPTO      VARCHAR2(1),
                            TASA               NUMBER,
                            P_LOGIN            VARCHAR2(20),
                            P_SESSION          VARCHAR2(20),
                            P_DEPARTAMENTO     NUMBER,
                            P_SUCURSAL         NUMBER,
                            P_EMPRESA          NUMBER,
                            HS_TRABAJADAS      VARCHAR2(30));*/
    C CUR_TYPE;

    V_LEGAJO  NUMBER;
    P_USUARIO VARCHAR2(20);
    V_REPORTE VARCHAR2(20);

    X_PERI_INICIO DATE;
    X_PERI_FIN    DATE;

    X_INICIO_JOR DATE;
    X_FIN_JOR    DATE;

    V_CANTIDAD_DIAS NUMBER;
  BEGIN

    SELECT A.PERI_FEC_INI, A.PERI_FEC_FIN, A.PERI_JORN_INI, A.PERI_JORN_FIN
      INTO X_PERI_INICIO, X_PERI_FIN, X_INICIO_JOR, X_FIN_JOR
      FROM PER_PERIODO A
     WHERE A.PERI_EMPR = P_EMPRESA
       AND A.PERI_CODIGO = P_PERIODO;

    ------VWHERE3--------------
    P_USUARIO := gen_devuelve_user;
    IF P_LEGAJO IS NOT NULL THEN
      V_WHERE3 := V_WHERE3 || ' AND PDOC_EMPLEADO =' || P_LEGAJO || ' ';
    END IF;

    V_WHERE3 := V_WHERE3 || ' AND TO_DATE(PDOC_FEC)   BETWEEN ' ||
                'TO_DATE(''' || X_PERI_INICIO ||
                ''',''DD/MM//YYYY'')  AND
                   TO_DATE(''' || X_PERI_FIN ||
                ''',''DD/MM//YYYY'') ';

    ---FINWHERE3-------------

    P_USUARIO := gen_devuelve_user;
    IF P_LEGAJO IS NOT NULL THEN
      V_WHERE := V_WHERE || ' AND EMPL_LEGAJO =' || P_LEGAJO || ' ';
    END IF;

    IF P_SUCURSAL IS NOT NULL THEN
      V_WHERE := V_WHERE || ' AND EMPL_SUCURSAL =' || P_SUCURSAL || ' ';

    end if;

    IF P_SUCURSAL = 1 and P_GRUPO in ('C', 'L', 'H') THEN
      V_WHERE := V_WHERE ||
                 '       AND EMPL_CARGO  IN (SELECT A.CAR_CODIGO
                            FROM PER_CARGO A
                            WHERE A.CAR_TIPO_COMISION = ''' ||
                 P_GRUPO || '''----''L''
                            AND A.CAR_EMPR = 1)' || ' ';

    END IF;

    IF P_DEPARTAMENTO IS NOT NULL THEN
      V_WHERE := V_WHERE || ' AND EMPL_DEPARTAMENTO =' || P_DEPARTAMENTO || ' ';
    END IF;

    IF P_FORMA_PAGO IS NOT NULL THEN
      V_WHERE := V_WHERE || ' AND EMPL_FORMA_PAGO =' || P_FORMA_PAGO || ' ';
    END IF;

    IF P_MONEDA IS NOT NULL THEN
      V_WHERE := V_WHERE || ' AND PDOC_MON =' || P_MONEDA || ' ';
    END IF;

    IF P_SITUACION IS NOT NULL THEN
      V_WHERE := V_WHERE || ' AND EMPL_SITUACION  =''' || P_SITUACION ||
                 ''' ';
    END IF;
    IF P_CARGO IS NOT NULL THEN
      V_WHERE := V_WHERE || ' AND EMPL_CARGO  =''' || P_CARGO || ''' ';
    END IF;

    --- V_WHERE := V_WHERE || ' AND EMPL_DEPARTAMENTO <>22';---  =''' || P_CARGO || ''' ';
    --raise_application_error (-20001,P_GRUPO);

    IF P_GRUPO IN ('L', 'C', 'H') THEN
      V_WHERE := V_WHERE ||
                 '       AND EMPL_CARGO  IN (SELECT A.CAR_CODIGO
                                      FROM PER_CARGO A
                                      WHERE A.CAR_TIPO_COMISION =''' ||
                 P_GRUPO || '''
                                      AND A.CAR_EMPR = 1)' || ' ';
    END IF;

    IF P_EXCLUIR_LEG IS NOT NULL THEN

      V_WHERE := V_WHERE || ' AND EMPL_LEGAJO NOT IN (
       (SELECT REGEXP_SUBSTR(REGEXP_REPLACE(''' ||
                 P_EXCLUIR_LEG ||
                 ''', '':'', '',''),
                             ''[^,]+'',
                             1,
                             LEVEL) CTA_BCO
          FROM DUAL
        CONNECT BY REGEXP_SUBSTR(REGEXP_REPLACE(''' ||
                 P_EXCLUIR_LEG ||
                 ''', '':'', '',''),
                                 ''[^,]+'',
                                 1,
                                 LEVEL) IS NOT NULL))';

    END IF;

    IF P_TIPO = 1 then --> 1: JORNAL
      V_REPORTE := 'PERL004A';
      V_SQL     := ' SELECT PER_EMPLEADO.EMPL_LEGAJO ,
                          PER_EMPLEADO.EMPL_APE|| '', ''||PER_EMPLEADO.EMPL_NOMBRE   NOMBRE,
                           EMPL_DOC_IDENT,
                           CASE WHEN ROW_NUMBER() OVER (PARTITION BY EMPL_LEGAJO ORDER BY PDOC_FEC) = 2 AND  PCON_DESC LIKE ''%AGUINA%'' THEN
                             ''Complemento Aguinaldo correspondiente al a?o ''|| TO_CHAR(TO_DATE(''' ||
                   P_HASTA ||
                   '''), ''yyyy'')
                           ELSE
                             ''Complemento Aguinaldo correspondiente al a?o '' || TO_CHAR(TO_DATE(''' ||
                   P_HASTA ||
                   '''), ''yyyy'')
                         END DESCRIPCION_AGUI, ---DIRECCION
                         NULL, --FPAGO
                         A.MON_ISO,--- EMPL_NRO_cTA,
                         PDOC_CLAVE, ---EMPL_BCO_PAGO
                         TO_CHAR(PDOC_FEC,''DD/MM/YYYY''), ---TELEFONO,
                          ''Son ''||MON_ISO||'': ''||GENERAL.FP_CONV_NRO_TXT(PDDET_IMP_LOC, PDOC_MON) IMPORT_LETRA,--SAL_dESC
                         NULL,--HS_EXTRAS
                         E.EMPR_RAZON_SOCIAL,--BCO_DESC
                         NULL,---DIAS_tRA
                         PER_DOCUMENTO_DET.PDDET_IMP IMPORTE,---SALARIO
                         PER_DOCUMENTO_DET.PDDET_IMP_LOC IMPORTE_LOC,--NRO_REC
                         PDOC_MON,--TOT
                         NULL,
                         NULL,
                         null,
                         null,
                         DECODE(''S'',''S'',SUC_DIR || '' TEL/FAX:'' || NVL(SUC_TEL,S.SUC_FAX)||'' ''||S.SUC_EMAIL,NULL) CENTRAL,
                         DECODE(SUC_IND_CASA_CENTRAL,''S'',NULL,''SUCURSAL '' || SUC_CODIGO) SUCU_CODI,
                         DECODE(SUC_IND_CASA_CENTRAL,''S'',NULL,SUC_DIR || '' TEL/FAX:'' || NVL(SUC_TEL,S.SUC_FAX)||'' ''||S.SUC_EMAIL) SUCURSAL,
                         SUC_IND_CASA_CENTRAL
                      FROM PER_DOCUMENTO,
                           PER_DOCUMENTO_DET,
                           PER_CONCEPTO,
                           PER_DPTO_CONC,
                           PER_EMPLEADO,
                           FIN_CONCEPTO,
                           GEN_MONEDA A,
                           GEN_EMPRESA E,
                           GEN_SUCURSAL S
                     WHERE PER_DOCUMENTO.PDOC_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_DOC
                       AND PER_CONCEPTO.PCON_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_CONCEPTO
                       AND PER_CONCEPTO.PCON_CLAVE = PER_DPTO_CONC.DPTOC_PER_CONC

                       AND SUC_EMPR = PDOC_EMPR
                       AND SUC_CODIGO = 1

                       AND PER_EMPLEADO.EMPL_LEGAJO = PER_DOCUMENTO.PDOC_EMPLEADO
                       AND PER_EMPLEADO.EMPL_DEPARTAMENTO = PER_DPTO_CONC.DPTOC_DPTO
                       AND FIN_CONCEPTO.FCON_CLAVE = PER_DPTO_CONC.DPTOC_FIN_CONC
                       ----AND NVL(PDDET_CLAVE_FIN, PDOC_CLAVE_FIN) is not null
                       AND PDOC_EMPR = EMPR_CODIGO
                       AND PDOC_MON = MON_CODIGO
                       AND PDOC_EMPR = MON_EMPR
                       AND PDOC_EMPR              = PDDET_EMPR
                       AND PCON_EMPR              = PDDET_EMPR
                       AND PCON_EMPR              = DPTOC_EMPR
                       AND EMPL_EMPRESA           = PDOC_EMPR
                       AND  EMPL_EMPRESA          = DPTOC_EMPR
                       AND FCON_EMPR             = DPTOC_EMPR
                       AND PCON_CLAVE = 3
                       AND PDOC_FEC BETWEEN TO_DATE(''' ||
                   P_DESDE || ''') AND TO_DATE(''' || P_HASTA ||
                   ''' )
                       AND PDOC_EMPR = ' || P_EMPRESA || '
                        ' || V_WHERE || '
                     ORDER BY PER_EMPLEADO.EMPL_APE|| '', ''||PER_EMPLEADO.EMPL_NOMBRE,
                              PER_DOCUMENTO.PDOC_CLAVE,
                              FIN_CONCEPTO.FCON_TIPO_SALDO';

    END IF;--> FIN TIPO (1) JORNAL
    
    IF P_TIPO IN (2, 4) then --> 2:MENSUAL, 4: TEMPORAL
      
      IF P_INCLUIR = 'S' THEN
        V_REPORTE := 'PERL004B1';
      ELSIF P_TIPO = 2 THEN
        V_REPORTE := 'PERL004_SIN';
      ELSE
        V_REPORTE := 'PERL004_FACTURAS';
      END IF;
      
      IF P_EMPRESA = 3 THEN

 ---------------------------INICIO REPORT MENSUAL----------------------------------------------------
    V_SAL := 'CREATE OR REPLACE VIEW PERL004_CED_TEMP AS
SELECT EMPL_LEGAJO,
       NOMBRE,
       ''SALARIO ADM:'' CONCEPTOS,
       NVL(EMPL_IMP_PLUS_SALARIAL, 0) INGRESOS,
       NULL EGRESOS,
       NULL IPS_AMH,
       NULL ANTICIPOS,
       NULL PRESTAMOS,
       NULL AUCENCIAS,
       NULL CH_EI,
       NULL PH_EI,
       1 ORDEN
  FROM (SELECT PER_EMPLEADO.EMPL_LEGAJO,
               PER_EMPLEADO.EMPL_NOMBRE || '' '' || PER_EMPLEADO.EMPL_APE NOMBRE,
               EMPL_IMP_PLUS_SALARIAL
          FROM PER_DOCUMENTO, PER_DOCUMENTO_DET, PER_CONCEPTO, PER_EMPLEADO
         WHERE PER_DOCUMENTO.PDOC_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_DOC
           AND PER_CONCEPTO.PCON_CLAVE =
               PER_DOCUMENTO_DET.PDDET_CLAVE_CONCEPTO
           AND PER_EMPLEADO.EMPL_LEGAJO = PER_DOCUMENTO.PDOC_EMPLEADO
           AND PCON_CANCELADO_POR_CONC IS NULL
           AND PDOC_FORM NOT IN (''PERI053'')

           AND PER_DOCUMENTO.PDOC_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR
           AND PER_CONCEPTO.PCON_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR
           AND PER_EMPLEADO.EMPL_EMPRESA = PER_DOCUMENTO.PDOC_EMPR

           AND PDOC_EMPR = 3
           AND PDDET_EMPR = 3
           AND PDOC_EMPR = 3
           ' || V_WHERE3 || '
              -----------------P_WHERE----------------
           /*AND PDOC_EMPLEADO = $P{P_LEGAJO}
           AND PDOC_FEC BETWEEN ''01/05/2021'' AND ''30/05/2022''*/
           )

----------------------------------------
 GROUP BY EMPL_LEGAJO, NOMBRE, EMPL_IMP_PLUS_SALARIAL

UNION ALL
SELECT EMPL_LEGAJO,
       NOMBRE,
       PCON_DESC CONCEPTOS,
       NVL(SUM(BONIFICACION), 0) + NVL(SALARIO, 0) INGRESOS,
       NULL EGRESOS,
       NULL IPS_AMH,
       NULL ANTICIPOS,
       NULL PRESTAMOS,
       NULL AUCENCIAS,
       NULL CH_EI,
       NULL PH_EI,
       2 ORDEN
  FROM (SELECT PER_EMPLEADO.EMPL_LEGAJO,
               PER_EMPLEADO.EMPL_NOMBRE || '' '' || PER_EMPLEADO.EMPL_APE NOMBRE,
               PER_CONCEPTO.PCON_DESC,
               DECODE(PDDET_CLAVE_CONCEPTO, 8, PDDET_IMP_LOC, 0) BONIFICACION,
               DECODE(PDDET_CLAVE_CONCEPTO, 2, EMPL_SALARIO_BASE, 0) SALARIO
          FROM PER_DOCUMENTO, PER_DOCUMENTO_DET, PER_CONCEPTO, PER_EMPLEADO
         WHERE PER_DOCUMENTO.PDOC_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_DOC
           AND PER_CONCEPTO.PCON_CLAVE =
               PER_DOCUMENTO_DET.PDDET_CLAVE_CONCEPTO
           AND PER_EMPLEADO.EMPL_LEGAJO = PER_DOCUMENTO.PDOC_EMPLEADO
           AND PCON_CANCELADO_POR_CONC IS NULL
           AND PDOC_FORM NOT IN (''PERI053'')

           AND PER_DOCUMENTO.PDOC_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR
           AND PER_CONCEPTO.PCON_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR
           AND PER_EMPLEADO.EMPL_EMPRESA = PER_DOCUMENTO.PDOC_EMPR

           AND PDOC_EMPR = 3
           AND PDDET_EMPR = 3
           AND PDOC_EMPR = 3
           AND PDDET_CLAVE_CONCEPTO IN (2, 8)
           ' || V_WHERE3 || '
              -----------------P_WHERE----------------
          /* AND PDOC_EMPLEADO = $P{P_LEGAJO}
           AND PDOC_FEC BETWEEN ''01/05/2022'' AND ''30/05/2022''*/
           )
----------------------------------------
 GROUP BY EMPL_LEGAJO, NOMBRE, PCON_DESC, BONIFICACION, SALARIO
UNION ALL
-------------TRAYENDO DATOS ---------------------------
SELECT PEM.EMPL_LEGAJO,
       PEM.EMPL_NOMBRE || '' '' || PEM.EMPL_APE NOMBRE,
       ''HS E.I.:'' CONCEPTOS,
       SUM(PDD.PDDET_IMP_EI) INGRESOS,
       NULL EGRESOS,
       NULL IPS_AMH,
       NULL ANTICIPOS,
       NULL PRESTAMOS,
       NULL AUCENCIAS,
       SUM(PDD.PDDET_CANT_HS_EI) || '' Horas'' CH_EI,
       SUM(PDD.PDDET_PH_EI) PH_EI,
       3 ORDEN
  FROM PER_DOCUMENTO PD, PER_DOCUMENTO_DET PDD, PER_EMPLEADO PEM
 WHERE PD.PDOC_CLAVE = PDD.PDDET_CLAVE_DOC
   AND PEM.EMPL_LEGAJO = PD.PDOC_EMPLEADO
   AND PEM.EMPL_EMPRESA = 3
   AND PD.PDOC_PERIODO = ' || P_PERIODO || '
   AND PD.PDOC_EMPLEADO = ' || P_LEGAJO || '
   AND PDD.PDDET_CLAVE_CONCEPTO = ''2''
   AND PDD.PDDET_EMPR = 3
   AND PD.PDOC_EMPR = 3
 GROUP BY EMPL_LEGAJO, PEM.EMPL_NOMBRE || '' '' || PEM.EMPL_APE
UNION ALL
SELECT PEM.EMPL_LEGAJO,
       PEM.EMPL_NOMBRE || '' '' || PEM.EMPL_APE NOMBRE,
       ''HS E.E.B.:'' CONCEPTOS,
       SUM(PDD.PDDET_IMP_EEB) INGRESOS,
       NULL EGRESOS,
       NULL IPS_AMH,
       NULL ANTICIPOS,
       NULL PRESTAMOS,
       NULL AUCENCIAS,
       SUM(PDD.PDDET_CANT_HS_EEB) || '' Horas'' CH_EEB,
       SUM(PDD.PDDET_PH_EEB) PH_EEB,
       4 ORDEN
  FROM PER_DOCUMENTO PD, PER_DOCUMENTO_DET PDD, PER_EMPLEADO PEM
 WHERE PD.PDOC_CLAVE = PDD.PDDET_CLAVE_DOC
   AND PEM.EMPL_LEGAJO = PD.PDOC_EMPLEADO
   AND PEM.EMPL_EMPRESA = 3
   AND PD.PDOC_PERIODO = ' || P_PERIODO || '
   AND PD.PDOC_EMPLEADO = ' || P_LEGAJO || '
   AND PDD.PDDET_CLAVE_CONCEPTO = ''2''
   AND PDD.PDDET_EMPR = 3
   AND PD.PDOC_EMPR = 3
 GROUP BY EMPL_LEGAJO, PEM.EMPL_NOMBRE || '' '' || PEM.EMPL_APE
UNION ALL
SELECT PEM.EMPL_LEGAJO,
       PEM.EMPL_NOMBRE || '' '' || PEM.EMPL_APE NOMBRE,
       ''HS E.M.:'' CONCEPTOS,
       SUM(PDD.PDDET_IMP_EM) INGRESOS,
       NULL EGRESOS,
       NULL IPS_AMH,
       NULL ANTICIPOS,
       NULL PRESTAMOS,
       NULL AUCENCIAS,
       SUM(PDD.PDDET_CANT_HS_EM) || '' Horas'' CH_EM,
       SUM(PDD.PDDET_PH_EM) PH_EM,
       5 ORDEN
  FROM PER_DOCUMENTO PD, PER_DOCUMENTO_DET PDD, PER_EMPLEADO PEM
 WHERE PD.PDOC_CLAVE = PDD.PDDET_CLAVE_DOC
   AND PEM.EMPL_LEGAJO = PD.PDOC_EMPLEADO
   AND PEM.EMPL_EMPRESA = 3
   AND PD.PDOC_PERIODO = ' || P_PERIODO || '
   AND PD.PDOC_EMPLEADO = ' || P_LEGAJO || '
   AND PDD.PDDET_CLAVE_CONCEPTO = ''2''
   AND PDD.PDDET_EMPR = 3
   AND PD.PDOC_EMPR = 3
 GROUP BY EMPL_LEGAJO, PEM.EMPL_NOMBRE || '' '' || PEM.EMPL_APE
UNION ALL
SELECT PEM.EMPL_LEGAJO,
       PEM.EMPL_NOMBRE || '' '' || PEM.EMPL_APE NOMBRE,
       ''HS ADM:'' CONCEPTOS,
       SUM(PDD.PDDET_IMP_ADM) INGRESOS,
       NULL EGRESOS,
       NULL IPS_AMH,
       NULL ANTICIPOS,
       NULL PRESTAMOS,
       NULL AUCENCIAS,
       SUM(PDD.PDDET_CANT_HS_ADM) || '' Horas'' CH_ADM,
       SUM(PDD.PDDET_PH_ADM) PH_ADM,
       6 ORDEN
  FROM PER_DOCUMENTO PD, PER_DOCUMENTO_DET PDD, PER_EMPLEADO PEM
 WHERE PD.PDOC_CLAVE = PDD.PDDET_CLAVE_DOC
   AND PEM.EMPL_LEGAJO = PD.PDOC_EMPLEADO
   AND PEM.EMPL_EMPRESA = 3
   AND PD.PDOC_PERIODO = ' || P_PERIODO || '
   AND PD.PDOC_EMPLEADO = ' || P_LEGAJO || '
   AND PDD.PDDET_CLAVE_CONCEPTO = ''2''
   AND PDD.PDDET_EMPR = 3
   AND PD.PDOC_EMPR = 3
 GROUP BY EMPL_LEGAJO, PEM.EMPL_NOMBRE || '' '' || PEM.EMPL_APE
UNION ALL
SELECT PEM.EMPL_LEGAJO,
       PEM.EMPL_NOMBRE || '' '' || PEM.EMPL_APE NOMBRE,
       ''HS MIES:'' CONCEPTOS,
       SUM(PDD.PDDET_IMP_MIES) INGRESOS,
       NULL EGRESOS,
       NULL IPS_AMH,
       NULL ANTICIPOS,
       NULL PRESTAMOS,
       NULL AUCENCIAS,
       SUM(PDD.PDDET_CANT_HS_MIES) || '' Horas'' CH_MIES,
       SUM(PDD.PDDET_PH_MIES) PH_MIES,
       7 ORDEN
  FROM PER_DOCUMENTO PD, PER_DOCUMENTO_DET PDD, PER_EMPLEADO PEM
 WHERE PD.PDOC_CLAVE = PDD.PDDET_CLAVE_DOC
   AND PEM.EMPL_LEGAJO = PD.PDOC_EMPLEADO
   AND PEM.EMPL_EMPRESA = 3
   AND PD.PDOC_PERIODO = ' || P_PERIODO || '
   AND PD.PDOC_EMPLEADO = ' || P_LEGAJO || '
   AND PDD.PDDET_CLAVE_CONCEPTO = ''2''
   AND PDD.PDDET_EMPR = 3
   AND PD.PDOC_EMPR = 3
 GROUP BY EMPL_LEGAJO, PEM.EMPL_NOMBRE || '' '' || PEM.EMPL_APE
UNION ALL
SELECT PEM.EMPL_LEGAJO,
       PEM.EMPL_NOMBRE || '' '' || PEM.EMPL_APE NOMBRE,
       ''HS OFICINA:'' CONCEPTOS,
       SUM(PDD.PDDET_IMP_OF) INGRESOS,
       NULL EGRESOS,
       NULL IPS_AMH,
       NULL ANTICIPOS,
       NULL PRESTAMOS,
       NULL AUCENCIAS,
       SUM(PDD.PDDET_CANT_HS_OF) || '' Horas'' CH_OF,
       SUM(PDD.PDDET_PH_OF) PH_OF,
       8 ORDEN
  FROM PER_DOCUMENTO PD, PER_DOCUMENTO_DET PDD, PER_EMPLEADO PEM
 WHERE PD.PDOC_CLAVE = PDD.PDDET_CLAVE_DOC
   AND PEM.EMPL_LEGAJO = PD.PDOC_EMPLEADO
   AND PEM.EMPL_EMPRESA = 3
   AND PD.PDOC_PERIODO = ' || P_PERIODO || '
   AND PD.PDOC_EMPLEADO = ' || P_LEGAJO || '
   AND PDD.PDDET_CLAVE_CONCEPTO = ''2''
   AND PDD.PDDET_EMPR = 3
   AND PD.PDOC_EMPR = 3
 GROUP BY EMPL_LEGAJO, PEM.EMPL_NOMBRE || '' '' || PEM.EMPL_APE
-------------FIN DATOS ---------------------------
UNION ALL
SELECT EMPL_LEGAJO,
       NOMBRE,
       PCON_DESC CONCEPTOS,
       0 INGRESOS,
       0 EGRESOS,
       SUM(IPS_AMH) IPS_AMH,
       SUM(ANTICIPOS) ANTICIPOS,
       SUM(PRESTAMOS) PRESTAMOS,
       SUM(AUCENCIAS) AUCENCIAS,
       NULL HORAS_COBRABLES,
       NULL PRECIO_X_HORA,
       11 ORDEN
  FROM (SELECT PER_EMPLEADO.EMPL_LEGAJO,
               PER_EMPLEADO.EMPL_NOMBRE || '' '' || PER_EMPLEADO.EMPL_APE NOMBRE,
               PER_CONCEPTO.PCON_DESC,
               DECODE(PDDET_CLAVE_CONCEPTO, 4, PDDET_IMP, 0) IPS_AMH,
               DECODE(PDDET_CLAVE_CONCEPTO, 16, PDDET_IMP_LOC, 0) ANTICIPOS,
               DECODE(PDDET_CLAVE_CONCEPTO, 18, PDDET_IMP_LOC, 0) PRESTAMOS,
               DECODE(PDDET_CLAVE_CONCEPTO, 7, PDDET_IMP_LOC, 0) AUCENCIAS
          FROM PER_DOCUMENTO, PER_DOCUMENTO_DET, PER_CONCEPTO, PER_EMPLEADO
         WHERE PER_DOCUMENTO.PDOC_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_DOC
           AND PER_CONCEPTO.PCON_CLAVE =
               PER_DOCUMENTO_DET.PDDET_CLAVE_CONCEPTO
           AND PER_EMPLEADO.EMPL_LEGAJO = PER_DOCUMENTO.PDOC_EMPLEADO
           AND PCON_CANCELADO_POR_CONC IS NULL
           AND PDOC_FORM NOT IN (''PERI053'')

           AND PER_DOCUMENTO.PDOC_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR
           AND PER_CONCEPTO.PCON_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR
           AND PER_EMPLEADO.EMPL_EMPRESA = PER_DOCUMENTO.PDOC_EMPR

           AND PDOC_EMPR = 3
           AND PDDET_EMPR = 3
           AND PDOC_EMPR = 3
           AND PDDET_CLAVE_CONCEPTO IN (4, 7, 16, 18)
           ' || V_WHERE3 || '
              -----------------P_WHERE----------------
           /*AND PDOC_EMPLEADO = $P{P_LEGAJO}
           AND PDOC_FEC BETWEEN ''01/05/2022'' AND ''30/05/2022''*/
           )
----------------------------------------
 GROUP BY EMPL_LEGAJO, NOMBRE, PCON_DESC
UNION ALL
SELECT PER_EMPLEADO.EMPL_LEGAJO,
       PER_EMPLEADO.EMPL_NOMBRE || '' '' || PER_EMPLEADO.EMPL_APE NOMBRE,
       INITCAP(PCON_DESC) CONCEPTOS,
       CASE
         WHEN PCON_IE_RECIBO = ''I'' THEN
          SUM(PDDET_IMP_LOC)
         ELSE
          0
       END INGRESOS,
       CASE
         WHEN PCON_IE_RECIBO = ''E'' THEN
          SUM(PDDET_IMP_LOC)
         ELSE
          0
       END EGRESOS,
       0 IPS_AMH,
       0 ANTICIPOS,
       0 PRESTAMOS,
       0 AUCENCIAS,
       NULL HORAS_COBRABLES,
       NULL PRECIO_X_HORA,
       10 ORDEN
  FROM PER_DOCUMENTO,
       PER_DOCUMENTO_DET,
       PER_CONCEPTO,
       PER_EMPLEADO,
       GEN_SUCURSAL,
       GEN_MONEDA,
       GEN_EMPRESA
 WHERE PER_DOCUMENTO.PDOC_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_DOC
   AND PER_CONCEPTO.PCON_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_CONCEPTO
   AND PER_EMPLEADO.EMPL_LEGAJO = PER_DOCUMENTO.PDOC_EMPLEADO
   AND PER_EMPLEADO.EMPL_SUCURSAL = GEN_SUCURSAL.SUC_CODIGO
   AND PDOC_MON = MON_CODIGO
   AND PCON_CANCELADO_POR_CONC IS NULL
   AND PCON_IE_RECIBO IN (''I'', ''E'')
   AND PDOC_EMPR = EMPR_CODIGO
   AND PDDET_EMPR = EMPR_CODIGO
   AND PCON_EMPR = EMPR_CODIGO
   AND EMPL_EMPRESA = EMPR_CODIGO
   AND SUC_EMPR = EMPR_CODIGO
   AND MON_EMPR = EMPR_CODIGO
   AND PDOC_EMPR = 3
   ' || V_WHERE3 || '

      -----------------P_WHERE----------------
   /*AND PDOC_EMPLEADO = $P{P_LEGAJO}
   AND PDOC_FEC BETWEEN ''01/05/2022'' AND ''30/05/2022''*/
----------------------------------------
 GROUP BY PER_EMPLEADO.EMPL_LEGAJO,
          PER_EMPLEADO.EMPL_NOMBRE || '' '' || PER_EMPLEADO.EMPL_APE,
          PCON_DESC,
          PCON_IE_RECIBO,
          EMPL_SALARIO_BASE
 ORDER BY 12 ASC';
 ---------------------------FIN REPORT MENSUAL----------------------------------------------------
    GEN_EXEC(V_SAL);
    iNSERT INTO X (CAMPO1, Otro) VALUES (V_SAL, 'PERL004TT');
    COMMIT;
-----------------------------INICIO REPORT JORNALERO----------------------------------------------------

V_SAL_2 := 'CREATE OR REPLACE VIEW PERL004_CED_JORNAL_TEMP AS
SELECT EMPL_LEGAJO,
       NOMBRE,
       PCON_DESC CONCEPTOS,
       NVL(SUM(BONIFICACION), 0) + NVL(SALARIO, 0) INGRESOS,
       NULL EGRESOS,
       NULL IPS_AMH,
       NULL ANTICIPOS,
       NULL PRESTAMOS,
       NULL AUCENCIAS,
       NULL HORASCOBRABLES,
       1 ORDEN
  FROM (SELECT PER_EMPLEADO.EMPL_LEGAJO,
               PER_EMPLEADO.EMPL_NOMBRE || '' '' || PER_EMPLEADO.EMPL_APE NOMBRE,
               PER_CONCEPTO.PCON_DESC,
               DECODE(PDDET_CLAVE_CONCEPTO, 8, PDDET_IMP_LOC, 0) BONIFICACION,
               DECODE(PDDET_CLAVE_CONCEPTO, 2, EMPL_SALARIO_BASE, 0) SALARIO
          FROM PER_DOCUMENTO, PER_DOCUMENTO_DET, PER_CONCEPTO, PER_EMPLEADO
         WHERE PER_DOCUMENTO.PDOC_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_DOC
           AND PER_CONCEPTO.PCON_CLAVE =
               PER_DOCUMENTO_DET.PDDET_CLAVE_CONCEPTO
           AND PER_EMPLEADO.EMPL_LEGAJO = PER_DOCUMENTO.PDOC_EMPLEADO
           AND PCON_CANCELADO_POR_CONC IS NULL
           AND PDOC_FORM NOT IN (''PERI053'')

           AND PER_DOCUMENTO.PDOC_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR
           AND PER_CONCEPTO.PCON_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR
           AND PER_EMPLEADO.EMPL_EMPRESA = PER_DOCUMENTO.PDOC_EMPR

           AND PDOC_EMPR = 3
           AND PDDET_EMPR = 3
           AND PDOC_EMPR = 3
           AND PDDET_CLAVE_CONCEPTO IN (2, 8)
           ' || V_WHERE3 || '
              -----------------P_WHERE----------------
          /* AND PDOC_EMPLEADO = $P{P_LEGAJO}
           AND PDOC_FEC BETWEEN ''01/05/2022'' AND ''30/05/2022''*/
           )
----------------------------------------
 GROUP BY EMPL_LEGAJO, NOMBRE, PCON_DESC, BONIFICACION, SALARIO
UNION ALL
-------------TRAYENDO DATOS ---------------------------
SELECT EMPL_LEGAJO,
       EMPL_NOMBRE NOMBRE,
       ''HS Diurnas:'' CONCEPTOS,
       MAX(TOTIMPHD) INGRESOS,
       NULL EGRESOS,
       NULL IPS_AMH,
       NULL ANTICIPOS,
       NULL PRESTAMOS,
       NULL AUCENCIAS,
       MAX(HD) || '' Horas'' HORASCOBRABLES,
       2 ORDEN
  FROM PER_PERL029_TEMP
 WHERE EMPL_LEGAJO = ' || P_LEGAJO || '
   AND PERIODO = ' || P_PERIODO || '
   AND EMPR = 3
 GROUP BY EMPL_LEGAJO, EMPL_NOMBRE
UNION ALL
SELECT EMPL_LEGAJO,
       EMPL_NOMBRE NOMBRE,
       ''HS Nocturnas 30%:'' CONCEPTOS,
       MAX(TOTIMPHN) INGRESOS,
       NULL EGRESOS,
       NULL IPS_AMH,
       NULL ANTICIPOS,
       NULL PRESTAMOS,
       NULL AUCENCIAS,
       MAX(HN) || '' Horas'' HORASCOBRABLES,
       3 ORDEN
  FROM PER_PERL029_TEMP
 WHERE EMPL_LEGAJO = ' || P_LEGAJO || '
   AND PERIODO = ' || P_PERIODO || '
   AND EMPR = 3
 GROUP BY EMPL_LEGAJO, EMPL_NOMBRE
UNION ALL
SELECT EMPL_LEGAJO,
       EMPL_NOMBRE NOMBRE,
       ''HS D.Extras 50%:'' CONCEPTOS,
       MAX(TOTIMPHDE) INGRESOS,
       NULL EGRESOS,
       NULL IPS_AMH,
       NULL ANTICIPOS,
       NULL PRESTAMOS,
       NULL AUCENCIAS,
       MAX(HDE) || '' Horas'' HORASCOBRABLES,
       4 ORDEN
  FROM PER_PERL029_TEMP
 WHERE EMPL_LEGAJO = ' || P_LEGAJO || '
   AND PERIODO = ' || P_PERIODO || '
   AND EMPR = 3
 GROUP BY EMPL_LEGAJO, EMPL_NOMBRE
UNION ALL
SELECT EMPL_LEGAJO,
       EMPL_NOMBRE NOMBRE,
       ''HS N.Extras 100%:'' CONCEPTOS,
       MAX(TOTIMPHNE) INGRESOS,
       NULL EGRESOS,
       NULL IPS_AMH,
       NULL ANTICIPOS,
       NULL PRESTAMOS,
       NULL AUCENCIAS,
       MAX(HNE) || '' Horas'' HORASCOBRABLES,
       5 ORDEN
  FROM PER_PERL029_TEMP
 WHERE EMPL_LEGAJO = ' || P_LEGAJO || '
   AND PERIODO = ' || P_PERIODO || '
   AND EMPR = 3
 GROUP BY EMPL_LEGAJO, EMPL_NOMBRE
UNION ALL
SELECT EMPL_LEGAJO,
       EMPL_NOMBRE NOMBRE,
       ''HS D.Dom\Fer 100%:'' CONCEPTOS,
       MAX(TOTIMPHFD) INGRESOS,
       NULL EGRESOS,
       NULL IPS_AMH,
       NULL ANTICIPOS,
       NULL PRESTAMOS,
       NULL AUCENCIAS,
       MAX(HFD) || '' Horas'' HORASCOBRABLES,
       6 ORDEN
  FROM PER_PERL029_TEMP
 WHERE EMPL_LEGAJO = ' || P_LEGAJO || '
   AND PERIODO = ' || P_PERIODO || '
   AND EMPR = 3
 GROUP BY EMPL_LEGAJO, EMPL_NOMBRE
UNION ALL
SELECT EMPL_LEGAJO,
       EMPL_NOMBRE NOMBRE,
       ''HS N.Dom\Fer 100%:'' CONCEPTOS,
       MAX(TOTIMPHFN) INGRESOS,
       NULL EGRESOS,
       NULL IPS_AMH,
       NULL ANTICIPOS,
       NULL PRESTAMOS,
       NULL AUCENCIAS,
       MAX(HFN) || '' Horas'' HORASCOBRABLES,
       7 ORDEN
  FROM PER_PERL029_TEMP
 WHERE EMPL_LEGAJO = ' || P_LEGAJO || '
   AND PERIODO = ' || P_PERIODO || '
   AND EMPR = 3
 GROUP BY EMPL_LEGAJO, EMPL_NOMBRE
UNION ALL
SELECT EMPL_LEGAJO,
       EMPL_NOMBRE NOMBRE,
       ''HS Mixta Extra:'' CONCEPTOS,
       MAX(TOTIMPHME) INGRESOS,
       NULL EGRESOS,
       NULL IPS_AMH,
       NULL ANTICIPOS,
       NULL PRESTAMOS,
       NULL AUCENCIAS,
       MAX(HME) || '' Horas'' HORASCOBRABLES,
       8 ORDEN
  FROM PER_PERL029_TEMP
 WHERE EMPL_LEGAJO = ' || P_LEGAJO || '
   AND PERIODO = ' || P_PERIODO || '
   AND EMPR = 3
 GROUP BY EMPL_LEGAJO, EMPL_NOMBRE
-------------FIN DATOS ---------------------------
UNION ALL
SELECT EMPL_LEGAJO,
       NOMBRE,
       PCON_DESC CONCEPTOS,
       0 INGRESOS,
       0 EGRESOS,
       SUM(IPS_AMH) IPS_AMH,
       SUM(ANTICIPOS) ANTICIPOS,
       SUM(PRESTAMOS) PRESTAMOS,
       SUM(AUCENCIAS) AUCENCIAS,
       NULL HORASCOBRABLES,
       9 ORDEN
  FROM (SELECT PER_EMPLEADO.EMPL_LEGAJO,
               PER_EMPLEADO.EMPL_NOMBRE || '' '' || PER_EMPLEADO.EMPL_APE NOMBRE,
               PER_CONCEPTO.PCON_DESC,
               DECODE(PDDET_CLAVE_CONCEPTO, 4, PDDET_IMP, 0) IPS_AMH,
               DECODE(PDDET_CLAVE_CONCEPTO, 16, PDDET_IMP_LOC, 0) ANTICIPOS,
               DECODE(PDDET_CLAVE_CONCEPTO, 18, PDDET_IMP_LOC, 0) PRESTAMOS,
               DECODE(PDDET_CLAVE_CONCEPTO, 7, PDDET_IMP_LOC, 0) AUCENCIAS
          FROM PER_DOCUMENTO, PER_DOCUMENTO_DET, PER_CONCEPTO, PER_EMPLEADO
         WHERE PER_DOCUMENTO.PDOC_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_DOC
           AND PER_CONCEPTO.PCON_CLAVE =
               PER_DOCUMENTO_DET.PDDET_CLAVE_CONCEPTO
           AND PER_EMPLEADO.EMPL_LEGAJO = PER_DOCUMENTO.PDOC_EMPLEADO
           AND PCON_CANCELADO_POR_CONC IS NULL
           AND PDOC_FORM NOT IN (''PERI053'')

           AND PER_DOCUMENTO.PDOC_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR
           AND PER_CONCEPTO.PCON_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR
           AND PER_EMPLEADO.EMPL_EMPRESA = PER_DOCUMENTO.PDOC_EMPR

           AND PDOC_EMPR = 3
           AND PDDET_EMPR = 3
           AND PDOC_EMPR = 3
           AND PDDET_CLAVE_CONCEPTO IN (4, 7, 16, 18)
           ' || V_WHERE3 || '
              -----------------P_WHERE----------------
           /*AND PDOC_EMPLEADO = $P{P_LEGAJO}
           AND PDOC_FEC BETWEEN ''01/05/2022'' AND ''30/05/2022''*/
           )
----------------------------------------
 GROUP BY EMPL_LEGAJO, NOMBRE, PCON_DESC
UNION ALL
SELECT PER_EMPLEADO.EMPL_LEGAJO,
       PER_EMPLEADO.EMPL_NOMBRE || '' '' || PER_EMPLEADO.EMPL_APE NOMBRE,
       INITCAP(PCON_DESC) CONCEPTOS,
       CASE
         WHEN PCON_IE_RECIBO = ''I'' THEN
          SUM(PDDET_IMP_LOC)
         ELSE
          0
       END INGRESOS,
       CASE
         WHEN PCON_IE_RECIBO = ''E'' THEN
          SUM(PDDET_IMP_LOC)
         ELSE
          0
       END EGRESOS,
       0 IPS_AMH,
       0 ANTICIPOS,
       0 PRESTAMOS,
       0 AUCENCIAS,
       NULL HORASCOBRABLES,
       10 ORDEN
  FROM PER_DOCUMENTO,
       PER_DOCUMENTO_DET,
       PER_CONCEPTO,
       PER_EMPLEADO,
       GEN_SUCURSAL,
       GEN_MONEDA,
       GEN_EMPRESA
 WHERE PER_DOCUMENTO.PDOC_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_DOC
   AND PER_CONCEPTO.PCON_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_CONCEPTO
   AND PER_EMPLEADO.EMPL_LEGAJO = PER_DOCUMENTO.PDOC_EMPLEADO
   AND PER_EMPLEADO.EMPL_SUCURSAL = GEN_SUCURSAL.SUC_CODIGO
   AND PDOC_MON = MON_CODIGO
   AND PCON_CANCELADO_POR_CONC IS NULL
   AND PCON_IE_RECIBO IN (''I'', ''E'')
   AND PDOC_EMPR = EMPR_CODIGO
   AND PDDET_EMPR = EMPR_CODIGO
   AND PCON_EMPR = EMPR_CODIGO
   AND EMPL_EMPRESA = EMPR_CODIGO
   AND SUC_EMPR = EMPR_CODIGO
   AND MON_EMPR = EMPR_CODIGO
   AND PDOC_EMPR = 3
   ' || V_WHERE3 || '

      -----------------P_WHERE----------------
   /*AND PDOC_EMPLEADO = $P{P_LEGAJO}
   AND PDOC_FEC BETWEEN ''01/05/2022'' AND ''30/05/2022''*/
----------------------------------------
 GROUP BY PER_EMPLEADO.EMPL_LEGAJO,
          PER_EMPLEADO.EMPL_NOMBRE || '' '' || PER_EMPLEADO.EMPL_APE,
          PCON_DESC,
          PCON_IE_RECIBO,
          EMPL_SALARIO_BASE
 ORDER BY 11 ASC';
-----------------------------INICIO REPORT JORNALERO----------------------------------------------------
    GEN_EXEC(V_SAL_2);
    iNSERT INTO X (CAMPO1, Otro) VALUES (V_SAL_2, 'PERL004TT2');
    COMMIT;
        IF P_INCLUIR = 'S' THEN
          V_REPORTE := 'PERL004B4';
        ELSIF P_TIPO = 2 AND P_FORMA_PAGO = 1 THEN
          V_REPORTE := 'PERL004_SIN_2';
        ELSIF P_TIPO = 2 AND P_FORMA_PAGO = 2 THEN
          V_REPORTE := 'PERL004_SIN_3';
        ELSE
          V_REPORTE := 'PERL004_FACTURAS';
        END IF;
END IF; -- > fin P_EMPRESA 3 >> CEDEC

      V_SQL := 'SELECT ---------------------------------*CABECERA
           EMPL_LEGAJO,
           EMPL_APE|| '', '' ||EMPL_NOMBRE NOMBRE,
           EMPL_DOC_IDENT,
           EMPL_DIR,
           EMPL_FORMA_PAGO,
           EMPL_NRO_CTA,
           EMPL_BCO_PAGO,
           EMPL_TEL,

          CASE WHEN   EMPL_FORMA_PAGO = 1 THEN
             CASE WHEN ' || P_PERIODO ||
               ' <= 126 THEN
                   ''Jornal Hora: ''||MON_ISO||'' ''||to_char(EMPL_IMP_HORA_N_D-464,''999G999G999G999'')
              ELSE
                ''Jornal Hora: ''||MON_ISO||'' ''||to_char(EMPL_IMP_HORA_N_D,''999G999G999G999'')
              END
            else
               CASE WHEN EMPL_SALARIO_BASE between 1  and 5096485 AND ' ||
               P_PERIODO || ' <= 126 THEN
               ''Salario Mensual: ''||MON_ISO||'' ''||to_char((EMPL_SALARIO_BASE-96485),''999G999G999G999'')

               ELSE
                 ''Salario Mensual: ''||MON_ISO||'' ''||to_char(EMPL_SALARIO_BASE,''999G999G999G999'')
                END
          end for_salario,

           EMPL_IND_HORA_EXTRA,
           (SELECT BCO_DESC
              FROM FIN_BANCO
              WHERE BCO_EMPR = ' || P_EMPRESA || '
              AND BCO_CODIGO = EMPL_BCO_PAGO) BANCO_DES,
              FP_CANT_DIAS_TRABAJADOS(P_FECHA => TO_DATE(''' ||
               P_HASTA || '''),
                                      P_LEGAJO => EMPL_LEGAJO,
                                      P_EMPRESA => 1) DIAS_TRABAJADOS,
          DECODE(EMPL_FORMA_PAGO, 1,NVL(EMPL_IMP_HORA_N_D, 0),NVL(EMPL_SALARIO_BASE, 0)) SALARIO,
          ---------------------------------*DETALLE
           MAX(A.PDOC_CLAVE) NRO_RECIBO,
           SUM(DECODE(PCON_IND_DBCR,''D'', -PDDET_IMP, PDDET_IMP) ) TOTAL,
            CASE WHEN EMPL_FORMA_PAGO = 1 THEN
                   1
             WHEN EMPL_FORMA_PAGO = 2 AND EMPL_IND_HORA_EXTRA  = ''S'' THEN
                  1
             ELSE
                 0
            END MOSTAR_MARC,
            EMPL_CARGO,
            round(EMPL_SALARIO_BASE),
            car_Desc,
            DECODE(''S'',''S'',SUC_DIR || '' TEL/FAX:'' || NVL(SUC_TEL,S.SUC_FAX)||'' ''||S.SUC_EMAIL,NULL) CENTRAL,
            DECODE(SUC_IND_CASA_CENTRAL,''S'',NULL,''SUCURSAL '' || SUC_CODIGO) SUCU_CODI,
            DECODE(SUC_IND_CASA_CENTRAL,''S'',NULL,SUC_DIR || '' TEL/FAX:'' || NVL(SUC_TEL,S.SUC_FAX)||'' ''||S.SUC_EMAIL) SUCURSAL,
            SUC_IND_CASA_CENTRAL
      FROM PER_DOCUMENTO A, PER_DOCUMENTO_DET B, PER_CONCEPTO C, PER_EMPLEADO E, GEN_MONEDA S, per_cargo, GEN_SUCURSAL S
     WHERE A.PDOC_CLAVE = B.PDDET_CLAVE_DOC
       AND A.PDOC_EMPR = B.PDDET_EMPR
       AND B.PDDET_CLAVE_CONCEPTO = C.PCON_CLAVE

       AND SUC_EMPR= A.PDOC_EMPR
       AND SUC_CODIGO = 1

       AND B.PDDET_EMPR = C.PCON_EMPR
       AND A.PDOC_EMPLEADO = EMPL_LEGAJO
       AND A.PDOC_EMPR = EMPL_EMPRESA
       AND PDOC_MON = S.MON_CODIGO
       AND PDOC_EMPR = S.MON_EMPR
       and empl_cargo = car_codigo
       and empl_empresa = car_empr
       AND EMPL_FORMA_PAGO IN(1,2,5)
      --- AND NVL(PDDET_CLAVE_FIN, PDOC_CLAVE_FIN) is not null
      -------------------------------------***
       AND A.PDOC_CLAVE_PADRE IS NULL ---PARA NO INCLUIR IPS DE VACACION
       AND A.PDOC_FORM != ''PERI092''------PARA NO INCLUIR VACACION
       AND C.PCON_CLAVE  <>6
      --- and empl_legajo not in (104560,104563,104562,104555,104545,104559,104561,104554,104556,104549,104552,104557,104558,104550,104550)
       AND NVL(C.PCON_FIN_TMOV,0) <> 31

       AND A.PDOC_EMPR = ' || P_EMPRESA || '
       AND PDOC_PERIODO =  ' || P_PERIODO || '
      ' || V_WHERE || '
    GROUP BY  EMPL_LEGAJO,
           EMPL_APE|| '', '' ||EMPL_NOMBRE,
           EMPL_FORMA_PAGO,
           EMPL_NRO_CTA,
           EMPL_BCO_PAGO,
           EMPL_TEL,
           EMPL_DOC_IDENT,
           EMPL_DIR,
           EMPL_IND_HORA_EXTRA,
           EMPL_IMP_HORA_N_D,
           EMPL_SALARIO_BASE,
           MON_ISO,
           EMPL_CARGO,
           car_desc,
           DECODE(''S'',''S'',SUC_DIR || '' TEL/FAX:'' || NVL(SUC_TEL,S.SUC_FAX)||'' ''||S.SUC_EMAIL,NULL) ,
            DECODE(SUC_IND_CASA_CENTRAL,''S'',NULL,''SUCURSAL '' || SUC_CODIGO) ,
            DECODE(SUC_IND_CASA_CENTRAL,''S'',NULL,SUC_DIR || '' TEL/FAX:'' || NVL(SUC_TEL,S.SUC_FAX)||'' ''||S.SUC_EMAIL) ,
            SUC_IND_CASA_CENTRAL';
    END IF; --> fin P_TIPO IN (2, 4)

    IF P_TIPO = 3 then --> 3: SERVICIOS PERSONALES
      V_REPORTE := 'PERL004L';
      V_SQL     := 'select per_empleado.empl_legajo,
                         per_empleado.empl_ape|| '', '' || per_empleado.empl_nombre nombre,
                         null,
                         ''Dr.J.E.Estigarribia, '' || to_char(TO_DATE(''' ||
                   P_HASTA ||
                   '''), ''dd'') || '' de '' ||
                         ltrim(rtrim(to_char(TO_DATE(''' ||
                   P_HASTA || '''), ''month''))) || '' de '' ||
                         to_char(TO_DATE(''' || P_HASTA ||
                   '''), ''yyyy'') fecha,
                         null,
                         per_concepto.pcon_desc,
                         null,
                         null,
                         null,
                         null,
                         ''Recibi(mos) de: ''||E.EMPR_RAZON_SOCIAL,
                         pdoc_mon,
                         decode(fin_concepto.fcon_tipo_saldo,
                                ''C'',
                                per_documento_det.pddet_imp,
                                -per_documento_det.pddet_imp) importe,
                         decode(fin_concepto.fcon_tipo_saldo,
                                ''C'',
                                per_documento_det.pddet_imp_loc,
                                -per_documento_det.pddet_imp_loc) importe_loc,

                        null,
                        null,
                        NULL,
                        null,
                        null,
                        DECODE(''S'',''S'',SUC_DIR || '' TEL/FAX:'' || NVL(SUC_TEL,S.SUC_FAX)||'' ''||S.SUC_EMAIL,NULL) CENTRAL,
                         DECODE(SUC_IND_CASA_CENTRAL,''S'',NULL,''SUCURSAL '' || SUC_CODIGO) SUCU_CODI,
                         DECODE(SUC_IND_CASA_CENTRAL,''S'',NULL,SUC_DIR || '' TEL/FAX:'' || NVL(SUC_TEL,S.SUC_FAX)||'' ''||S.SUC_EMAIL) SUCURSAL,
                         SUC_IND_CASA_CENTRAL
                    from per_documento,
                         per_documento_det,
                         per_concepto,
                         per_dpto_conc,
                         per_empleado,
                         fin_concepto,
                         GEN_EMPRESA E,
                         GEN_SUCURSAL S
                   where per_documento.pdoc_clave = per_documento_det.pddet_clave_doc
                     and per_concepto.pcon_clave = per_documento_det.pddet_clave_concepto
                     and per_concepto.pcon_clave = per_dpto_conc.dptoc_per_conc

                     AND SUC_EMPR = PDOC_EMPR
                     AND SUC_CODIGO = 1

                     and per_empleado.empl_legajo = per_documento.pdoc_empleado
                     and per_empleado.empl_departamento = per_dpto_conc.dptoc_dpto
                     and fin_concepto.fcon_clave = per_dpto_conc.dptoc_fin_conc
                ---      AND NVL(PDDET_CLAVE_FIN, PDOC_CLAVE_FIN) is not null
                    AND pdoc_empr              = pddet_empr
                     AND pcon_empr              = pddet_empr
                     AND pcon_empr              = dptoc_empr
                     AND empl_empresa           = pdoc_empr
                     AND  empl_empresa          = dptoc_empr
                     AND fcon_empr             = dptoc_empr
                     AND pdoc_empr = EMPR_CODIGO
                     AND pdoc_empr  = ' || P_EMPRESA || '
                     ' || V_WHERE || '
                     AND PDOC_FEC BETWEEN TO_DATE(''' ||
                   P_DESDE || ''') AND TO_DATE(''' || P_HASTA ||
                   ''' )
                   order by per_empleado.empl_ape|| '', '' || per_empleado.empl_nombre,
                            per_documento.pdoc_clave,
                            fin_concepto.fcon_tipo_saldo';

    END IF; --> FIN P_TIPO 3: SERVICIOS PERSONALES

    delete x where Otro = 'PERL004333333';

    --  RAISE_APPLICATION_ERROR (-20001,'ASHDFKJASDF');

    iNSERT INTO X (CAMPO1, Otro) VALUES (V_SQL, 'PERL004333333');
    
    COMMIT;
    
    OPEN C_PERL004 FOR V_SQL;

    DELETE PERL004_TEMP
     WHERE P_SESSION = P_SESSION
       AND P_LOGIN = P_USUARIO;

    DELETE PERL004_REC_TEMP
     WHERE P_SESSION = P_SESSION
       AND P_LOGIN = P_USUARIO;

    DELETE PERL004_MARC_TEMP
     WHERE P_SESSION = P_SESSION
       AND P_LOGIN = P_USUARIO;

    DELETE PERL004_REC_TRANS_TEMP
     WHERE P_SESSION = P_SESSION
       AND P_LOGIN = P_USUARIO;

    DELETE PERL004_FAC_TEMP
     WHERE P_SESSION = P_SESSION
       AND P_USUARIO = P_USUARIO;
       
    COMMIT;

    loop --> INICIO THE BIG LOOP
      FETCH C_PERL004
        INTO C;
      EXIT WHEN C_PERL004%NOTFOUND;
      ---   V_LEGAJO := C.LEGAJO;

      --- dbms_output.put_line  (C.LEGAJO);
      ---------------------------------------------------------------------***** IMPORTES
      IF P_TIPO = 2 THEN

        --- PARA LA CANTIDAD DE DIAS
        --- VAMOS A CONTAR LOS DIAS DE MARCACION DE LOS JORNALEROS

        IF C.FORMA_PAGO = 1 THEN
          /*  SELECT count(distinct(MARC_FECHA))--, MARC_EMPLEADO, MARC_EVENTO, MARC_HORA, MARC_ESTADO--, P_USUARIO, P_SESSION
           into V_CANTIDAD_DIAS
           FROM PER_PERL004B B, PER_PERIODO PR
          WHERE PR.PERI_CODIGO = P_PERIODO
            AND B.EMPR_CODIGO =1--- P_EMPRESA
            AND B.EMPR_CODIGO = PERI_EMPR
            AND MARC_EMPLEADO = C.LEGAJO ;*/

          /*
          SELECT count(distinct(a.MARC_FECHA))
          INTO V_CANTIDAD_DIAS
            FROM PER_MARC_FINAL_V A
           WHERE A.MARC_EMPLEADO = C.LEGAJO
             AND A.PERFIN_EMPR = 1
             AND A.MARC_FECHA BETWEEN X_INICIO_JOR and X_FIN_JOR;*/

          V_CANTIDAD_DIAS := 0;
        ELSE

          BEGIN
            /* select T.PERCON_CANT_DIAS
              INTO V_CANTIDAD_DIAS
            from PER_EMPL_CONC t
            WHERE T.PERCON_EMPLEADO = C.LEGAJO
            AND T.PERCON_CONCEPTO = 2
            AND T.PERCON_EMPR = 1;*/

            SELECT PDOC_CANT_DIAS
              INTO V_CANTIDAD_DIAS
              FROM PER_DOCUMENTO A
             WHERE PDOC_EMPR = 1
               AND PDOC_EMPLEADO = C.LEGAJO -----V.EMPLEADO ;
               AND A.PDOC_CANT_DIAS IS NOT NULL
               and a.pdoc_form = 'PERI005'
               AND TO_CHAR(A.PDOC_FEC, 'MM/YYYY') =
                   TO_CHAR(TO_DATE(P_HASTA), 'mm/yyyy') -- TO_DATE(P_DESDE,'mm/yyyy')
             group by PDOC_CANT_DIAS;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              V_CANTIDAD_DIAS := 0; --C.DIAS_TRABAJADO;
          END;

        END IF;
        --  X_INICIO_JOR, X_FIN_JOR

        INSERT INTO PERL004_TEMP
          (LEGAJO,
           NOMBRE,
           EMPL_DOC_IDENTIDAD,
           EMPL_DIR,
           PERIODO,
           PER_INICIO,
           PER_FIN,
           DIAS_TRABAJADO,
           FORMA_SALARIO_DES,
           MONEDA,
           NRO_RECIBO,
           SAL_TOTAL,
           FORMA_PAGO,
           EMPL_NRO_CTA,
           EMPL_BCO_PAGO,
           BANCO_DES,
           FECHA_LETRA,
           SALARIO,
           EMPL_TEL,
           MON_ISO,
           MOSTAR_MARC,
           ORDEN,
           CONCEPTO,
           CONC_DETALLE,
           IMPORTE,
           TIPO_CONCEPTO,
           TASA,
           P_LOGIN,
           P_SESSION,
           P_DEPARTAMENTO,
           P_SUCURSAL,
           P_EMPRESA,
           EMPL_IND_HS_EXTRA,
           HS_TRABAJADAS,
           P_TIPO_RECIBO,
           car_desc,
           CENTRAL,
           SUCU_CODI,
           SUCURSAL,
           SUC_IND_CASA_CENTRAL)

          SELECT C.LEGAJO,
                 C.NOMBRE,
                 C.EMPL_DOC_IDENTIDAD,
                 C.EMPL_DIR,
                 P_PERIODO,
                 X_PERI_INICIO, ---P_DESDE,
                 X_PERI_FIN, ---
                 case
                   when V_CANTIDAD_DIAS < 0 then
                    0
                   when C.salario_base = 0 then
                    0
                   else
                    V_CANTIDAD_DIAS

                 end cant_dias,
                 C.FORMA_SALARIO_DES,
                 P_MONEDA,
                 C.NRO_RECIBO,
                 C.TOTAL,
                 C.FORMA_PAGO,
                 C.EMPL_NRO_CTA,
                 C.EMPL_BCO_PAGO,
                 C.BANCO_DES,
                 NULL,
                 C.SALARIO,
                 C.EMPL_TEL,
                 NULL,
                 C.MOSTAR_MARC, -----MARCACION
                 ORDEN,
                 DETALLE,
                 DETALLE2,
                 IMPORTE_HORAS,
                 TIPO,
                 NULL,
                 P_USUARIO,
                 P_SESSION,
                 P_DEPARTAMENTO,
                 P_SUCURSAL,
                 P_EMPRESA,
                 C.EMPL_IND_HS_EXTRA,
                 HORAS_TRABAJADAS,
                 'SALARIO',
                 c.car_desc,
                 CENTRAL,
                 SUCU_CODI,
                 SUCURSAL,
                 SUC_IND_CASA_CENTRAL

            FROM (SELECT DECODE('S',
                                'S',
                                SUC_DIR || ' TEL/FAX:' ||
                                NVL(SUC_TEL, S.SUC_FAX) || ' ' || S.SUC_EMAIL,
                                NULL) CENTRAL,
                         DECODE(SUC_IND_CASA_CENTRAL,
                                'S',
                                NULL,
                                'SUCURSAL ' || SUC_CODIGO) SUCU_CODI,
                         DECODE(SUC_IND_CASA_CENTRAL,
                                'S',
                                NULL,
                                SUC_DIR || ' TEL/FAX:' ||
                                NVL(SUC_TEL, S.SUC_FAX) || ' ' || S.SUC_EMAIL) SUCURSAL,
                         SUC_IND_CASA_CENTRAL
                    FROM GEN_SUCURSAL S
                   WHERE S.SUC_EMPR = P_SUCURSAL
                     AND S.SUC_CODIGO = 1),
                 (SELECT C.LEGAJO LEGAJO,
                         B.ORDEN,
                         B.DETALLE,
                         null DETALLE2,
                         CASE
                           when P_GRUPO = 'L' and P_PERIODO = 124 THEN
                            '0 Horas'
                           WHEN ORDEN = 2 AND C.CARGO <> 15 THEN
                            SUM(NVL(A.HD, 0)) || ' Horas' --DIURNO
                           WHEN ORDEN = 3 AND C.CARGO <> 15 THEN
                            SUM(NVL(A.HN, 0)) || ' Horas' --NOCTURNO
                           WHEN ORDEN = 4 AND C.CARGO <> 15 THEN
                            SUM(NVL(A.HDE, 0)) || ' Horas' --DIURNO EXTRA
                           WHEN ORDEN = 5 AND C.CARGO <> 15 THEN
                            SUM(NVL(A.HNE, 0)) || ' Horas' --NOCTURNO EXTA
                           WHEN ORDEN = 6 AND C.CARGO <> 15 THEN
                            SUM(NVL(A.HFD, 0)) || ' Horas' --FERIADO DIURNO
                           WHEN ORDEN = 7 AND C.CARGO <> 15 THEN
                            SUM(NVL(A.HFN, 0)) || ' Horas' ---FERIADO NOCTURNO
                           WHEN ORDEN = 8 AND C.CARGO <> 15 THEN
                           --case when 103833 = C.LEGAJO then
                           --- '91,4 Horas'
                           --else
                            SUM(NVL(A.HME, 0)) || ' Horas' --MIXTA
                         --end

                           WHEN ORDEN = 9 AND C.CARGO = 15 and
                                C.FORMA_PAGO <> 2 THEN
                            SUM(NVL(A.HD, 0)) + SUM(NVL(A.HN, 0)) +
                            SUM(NVL(A.HDE, 0)) + SUM(NVL(A.HNE, 0)) +
                            SUM(NVL(A.HME, 0)) || ' Horas'
                         --- SUM(NVL(A.HFD,0))+  SUM(NVL(A.HME,0))+/*jjjj*/ SUM(NVL(A.HD,0))+  SUM(NVL(A.HN,0))+ SUM(NVL(A.HDE,0))+SUM(NVL(A.HNE,0)) +SUM(NVL(A.HFN,0))||' Horas'
                           WHEN ORDEN = 10 AND C.CARGO = 15 and
                                C.FORMA_PAGO <> 2 THEN
                           -- 0||' Horas'
                            SUM(NVL(A.HFD, 0)) + SUM(NVL(A.HFN, 0)) ||
                            ' Horas'
                           WHEN ORDEN IN (2, 3, 4, 5, 6, 7, 8) AND
                                C.CARGO = 15 and C.FORMA_PAGO <> 2 THEN
                            '0 Horas'
                           WHEN ORDEN IN (9, 10) AND C.CARGO <> 15 and
                                C.FORMA_PAGO <> 2 THEN
                            '0 Horas'
                         END HORAS_TRABAJADAS,

                         CASE
                           WHEN P_GRUPO = 'L' and P_PERIODO = 124 THEN
                            0
                           WHEN ORDEN = 2 AND C.CARGO <> 15 THEN
                            SUM(NVL(A.totimphd, 0)) --DIURNO
                           WHEN ORDEN = 3 AND C.CARGO <> 15 THEN
                            SUM(NVL(A.totimphn, 0)) --NOCTURNO
                           WHEN ORDEN = 4 AND C.CARGO <> 15 THEN
                            SUM(NVL(A.TOTIMPHDE, 0)) --DIURNO EXTRA
                           WHEN ORDEN = 5 AND C.CARGO <> 15 THEN
                            SUM(NVL(A.TOTIMPHNE, 0)) --NOCTURNO EXTA
                           WHEN ORDEN = 6 AND C.CARGO <> 15 THEN
                            SUM(NVL(A.TOTIMPHFD, 0)) --FERIADO DIURNO
                           WHEN ORDEN = 7 AND C.CARGO <> 15 THEN
                            SUM(NVL(A.TOTIMPHFN, 0)) ---FERIADO NOCTURNO
                           WHEN ORDEN = 8 AND C.CARGO <> 15 THEN
                           ---- case when 103833 = C.LEGAJO then
                           ---  to_number(2171298)
                           --else
                            SUM(NVL(A.TOTIMPHME, 0)) --MIXTA

                         --   end
                           WHEN ORDEN = 9 AND C.CARGO = 15 and
                                C.FORMA_PAGO <> 2 THEN

                           --a.HD+a.HDE+a.HN+a.HNE+a.HME+a.HFD+a.HFN
                           -- (SUM(NVL(A.HD,0))+  SUM(NVL(A.HDE,0))+SUM(NVL(A.HN,0))+SUM(NVL(A.HNE,0))+SUM(NVL(A.HME,0))+SUM(NVL(A.HFD,0))+SUM(NVL(A.HFN,0)))*10542

                            SUM(NVL(A.totimphd, 0)) + SUM(NVL(A.totimphn, 0)) +
                            SUM(NVL(A.TOTIMPHDE, 0)) +
                            SUM(NVL(A.TOTIMPHNE, 0)) +
                            SUM(NVL(A.TOTIMPHME, 0))
                           WHEN ORDEN = 10 AND C.CARGO = 15 and
                                C.FORMA_PAGO <> 2 THEN
                            SUM(NVL(A.TOTIMPHFD, 0)) +
                            SUM(NVL(A.TOTIMPHFN, 0))

                           WHEN ORDEN IN (2, 3, 4, 5, 6, 7, 8) AND
                                C.CARGO = 15 THEN
                            0
                           WHEN ORDEN IN (9, 10) AND C.CARGO <> 15 THEN
                            0

                         END IMPORTE_HORAS,
                         NULL CLAVE,
                         'I' TIPO

                    FROM (SELECT a.*
                            FROM PER_PERL034_V_ACT A
                           WHERE A.EMPL_LEGAJO = C.LEGAJO
                             AND A.PERIODO = P_PERIODO
                             AND A.EMPL_EMPRESA = P_EMPRESA
                          /*GROUP BY a.EMPL_LEGAJO,
                          a.EMPL_NOMBRE,
                          a.EMPL_PAGA_IPS,
                          a.EMPL_SUCURSAL,
                          a.DPTO_CODIGO,
                          a.DPTO_DESC,
                          a.PERIODO,
                          a.EMPL_EMPRESA,
                          a.HD,
                          a.HDE,
                          a.HN,
                          a.HNE,
                          a.HME,
                          a.HFD,
                          a.HFN,
                          a.IMP_HD,
                          a.IMP_HDE,
                          a.IMP_HN,
                          a.IMP_HNE,
                          a.IMP_HME,
                          a.IMP_HFD,
                          a.IMP_HFN,
                          a.TOTIMPHD,
                          a.TOTIMPHDE,
                          a.TOTIMPHN,
                          a.TOTIMPHNE,
                          a.TOTIMPHME,
                          a.TOTIMPHFD,
                          a.TOTIMPHFN,
                          a.FORMA_PAGO*/
                          ) A,
                         (SELECT REGEXP_SUBSTR('Hs Diurnas:,Hs Nocturnas 30%:,Hs D. Extras 50%:,Hs N. Extras 100%:,Hs D. Dom\Fer 100%:,Hs N. Dom\Fer 100%:,Hs Mixta Extra:,Hs Total:,Hs. Feriado:',
                                               '[^,]+',
                                               1,
                                               LEVEL) DETALLE,
                                 LEVEL + 1 ORDEN,
                                 C.LEGAJO LEGAJo
                            FROM DUAL
                          CONNECT BY REGEXP_SUBSTR('Hs Diurnas:,Hs Nocturnas 30%:,Hs D. Extras 50%:,Hs N. Extras 100%:,Hs D. Dom\Fer 100%:,Hs N. Dom\Fer 100%:,Hs Mixta Extra:,Hs Total:,Hs. Feriado:',
                                                   '[^,]+',
                                                   1,
                                                   LEVEL) IS NOT NULL) B
                   WHERE EMPL_LEGAJO(+) = LEGAJO
                   GROUP BY B.ORDEN, B.DETALLE
                  UNION ALL
                  SELECT C.LEGAJO,
                         1 ORDEN,
                         initcap(PCON_DESC),
                         NULL,
                         case
                           when V_CANTIDAD_DIAS < 0 then
                            0
                           else
                            V_CANTIDAD_DIAS
                         end || ' Dias' cant_dias, --NULL,
                         NVL(IMPORTE_LOC, 0),
                         PCON_CLAVE,
                         'I'
                    FROM PER_CONCEPTO,
                         (SELECT C.LEGAJO LEGAJO,
                                 initcap(PCON_DESC) DETALLE,
                                 CASE
                                   WHEN DO.PDOC_FORM <> 'PERI034' THEN
                                    NVL(SUM(PDDET_IMP_LOC), 0)
                                   ELSE
                                    0
                                 END IMPORTE_LOC,
                                 PDDET_CLAVE_CONCEPTO CONCEPTO,
                                 PDOC_EMPR EMPRESA
                            FROM PER_DOCUMENTO     DO,
                                 PER_DOCUMENTO_DET AA,
                                 PER_CONCEPTO      M
                           WHERE DO.PDOC_CLAVE = AA.PDDET_CLAVE_DOC
                             AND DO.PDOC_EMPR = AA.PDDET_EMPR
                             AND AA.PDDET_CLAVE_CONCEPTO = M.PCON_CLAVE
                             AND AA.PDDET_EMPR = M.PCON_EMPR
                             AND PCON_CANCELADO_POR_CONC IS NULL
                             AND PCON_CLAVE = 2
                             AND PCON_EMPR = P_EMPRESA
                                ----     AND NVL(PDDET_CLAVE_FIN, PDOC_CLAVE_FIN) is not null
                                ---AND PDOC_PERIODO(+) = P_PERIODO
                             AND PDOC_FEC(+) BETWEEN TO_DATE(P_DESDE) AND
                                 TO_DATE(P_HASTA)
                             AND PDOC_EMPLEADO(+) = C.LEGAJO
                           GROUP BY PDOC_EMPLEADO,
                                    initcap(PCON_DESC),
                                    PDDET_CLAVE_CONCEPTO,
                                    PDOC_EMPR,
                                    DO.PDOC_FORM) A
                   WHERE PCON_CLAVE = CONCEPTO(+)
                     AND PCON_EMPR = EMPRESA(+)
                     AND PCON_CLAVE = 2
                     AND PCON_EMPR = P_EMPRESA
                  UNION ALL
                  SELECT C.LEGAJO,
                         11 ORDEN,
                         initcap(PCON_DESC),
                         NULL,
                         null,
                         NVL(IMPORTE_LOC, 0),
                         PCON_CLAVE,
                         'I'
                    FROM PER_CONCEPTO,
                         (SELECT C.LEGAJO LEGAJO,
                                 initcap(PCON_DESC) DETALLE,

                                 SUM(PDDET_IMP_LOC) IMPORTE_LOC,
                                 PDDET_CLAVE_CONCEPTO CONCEPTO,
                                 PDOC_EMPR EMPRESA
                            FROM PER_DOCUMENTO     DO,
                                 PER_DOCUMENTO_DET AA,
                                 PER_CONCEPTO      M
                           WHERE DO.PDOC_CLAVE = AA.PDDET_CLAVE_DOC
                             AND DO.PDOC_EMPR = AA.PDDET_EMPR
                             AND AA.PDDET_CLAVE_CONCEPTO = M.PCON_CLAVE
                             AND AA.PDDET_EMPR = M.PCON_EMPR
                             AND PCON_CANCELADO_POR_CONC IS NULL
                             AND PCON_CLAVE = 12
                             AND PCON_EMPR = P_EMPRESA
                                ---   AND NVL(PDDET_CLAVE_FIN, PDOC_CLAVE_FIN) is not null
                                -- AND PDOC_PERIODO(+) = P_PERIODO----AND  PDOC_FEC(+) BETWEEN TO_DATE($P{P_FEC_INI}) AND TO_DATE($P{P_FEC_FIN})
                             AND PDOC_FEC(+) BETWEEN TO_DATE(P_DESDE) AND
                                 TO_DATE(P_HASTA)
                             AND PDOC_EMPLEADO(+) = C.LEGAJO
                           GROUP BY PDOC_EMPLEADO,
                                    initcap(PCON_DESC),
                                    PDDET_CLAVE_CONCEPTO,
                                    PDOC_EMPR) A
                   WHERE PCON_CLAVE = CONCEPTO(+)
                     AND PCON_EMPR = EMPRESA(+)
                     AND PCON_CLAVE = 12
                     AND PCON_EMPR = P_EMPRESA
                  UNION ALL
                  SELECT C.LEGAJO,
                         12 ORDEN,
                         initcap(PCON_DESC),
                         NULL,
                         null,
                         NVL(IMPORTE_LOC, 0),
                         PCON_CLAVE,
                         'I'
                    FROM PER_CONCEPTO,
                         (SELECT C.LEGAJO LEGAJO,
                                 initcap(PCON_DESC) DETALLE,

                                 SUM(PDDET_IMP_LOC) IMPORTE_LOC,
                                 PDDET_CLAVE_CONCEPTO CONCEPTO,
                                 PDOC_EMPR EMPRESA
                            FROM PER_DOCUMENTO     DO,
                                 PER_DOCUMENTO_DET AA,
                                 PER_CONCEPTO      M
                           WHERE DO.PDOC_CLAVE = AA.PDDET_CLAVE_DOC
                             AND DO.PDOC_EMPR = AA.PDDET_EMPR
                             AND AA.PDDET_CLAVE_CONCEPTO = M.PCON_CLAVE
                             AND AA.PDDET_EMPR = M.PCON_EMPR
                             AND PCON_CANCELADO_POR_CONC IS NULL
                             AND PCON_CLAVE = 8
                             AND PCON_EMPR = P_EMPRESA
                                ----    AND NVL(PDDET_CLAVE_FIN, PDOC_CLAVE_FIN) is not null
                                --AND PDOC_PERIODO(+) = P_PERIODO----AND  PDOC_FEC(+) BETWEEN TO_DATE($P{P_FEC_INI}) AND TO_DATE($P{P_FEC_FIN})
                             AND PDOC_FEC(+) BETWEEN TO_DATE(P_DESDE) AND
                                 TO_DATE(P_HASTA)
                             AND PDOC_EMPLEADO(+) = C.LEGAJO
                           GROUP BY PDOC_EMPLEADO,
                                    initcap(PCON_DESC),
                                    PDDET_CLAVE_CONCEPTO,
                                    PDOC_EMPR) A
                   WHERE PCON_CLAVE = CONCEPTO(+)
                     AND PCON_EMPR = EMPRESA(+)
                     AND PCON_CLAVE = 8
                     AND PCON_EMPR = P_EMPRESA
                  UNION ALL
                  SELECT C.LEGAJO,
                         13 ORDEN,
                         /* case when rownum = 1 then
                          'Otros Ingresos:'
                          else
                            null
                         end ,*/
                         initcap(PER_CONCEPTO.PCON_DESC) DETALLE,
                         NULL,
                         null,
                         PER_DOCUMENTO_DET.PDDET_IMP_LOC IMPORTE_LOC,
                         PDDET_CLAVE_CONCEPTO,
                         'I'
                    FROM PER_DOCUMENTO, PER_DOCUMENTO_DET, PER_CONCEPTO
                   WHERE PER_DOCUMENTO.PDOC_CLAVE =
                         PER_DOCUMENTO_DET.PDDET_CLAVE_DOC
                     AND PER_CONCEPTO.PCON_CLAVE =
                         PER_DOCUMENTO_DET.PDDET_CLAVE_CONCEPTO
                     AND PCON_CANCELADO_POR_CONC IS NULL
                     AND PDDET_EMPR = PDOC_EMPR
                     AND PCON_EMPR = PDOC_EMPR
                        ----      AND NVL(PDDET_CLAVE_FIN, PDOC_CLAVE_FIN) is not null
                     AND PDOC_FORM <> 'PERI034'
                     AND PDDET_CLAVE_CONCEPTO NOT IN (2, 12, 3, 6, 8)
                     AND PDOC_EMPR = P_EMPRESA
                     AND PER_CONCEPTO.PCON_IND_DBCR = 'C'
                        --AND PDOC_PERIODO = P_PERIODO
                     AND PDOC_FEC BETWEEN TO_DATE(P_DESDE) AND
                         TO_DATE(P_HASTA)
                        --AND PDOC_FEC BETWEEN TO_DATE($P{P_FEC_INI}) AND TO_DATE($P{P_FEC_FIN})
                     AND PDOC_EMPLEADO = C.LEGAJO


                   UNION ALL

                   SELECT C.LEGAJO LEGAJO,
                           14 ORDEN,
                           INITCAP(PCON_DESC) DETALLE,
                           NULL,
                           NULL,
                           SUM(PDDET_IMP_LOC) IMPORTE_LOC,
                           PDDET_CLAVE_CONCEPTO CONCEPTO,
                           'I'
                      FROM PER_DOCUMENTO DO, PER_DOCUMENTO_DET AA, PER_CONCEPTO M
                     WHERE DO.PDOC_CLAVE = AA.PDDET_CLAVE_DOC
                       AND DO.PDOC_EMPR = AA.PDDET_EMPR
                       AND AA.PDDET_CLAVE_CONCEPTO = M.PCON_CLAVE
                       AND AA.PDDET_EMPR = M.PCON_EMPR
                       AND PCON_CANCELADO_POR_CONC IS NOT NULL
                       AND PCON_CLAVE = 39
                       AND PCON_EMPR = P_EMPRESA
                          ---   AND NVL(PDDET_CLAVE_FIN, PDOC_CLAVE_FIN) is not null
                          -- AND PDOC_PERIODO(+) = P_PERIODO----AND  PDOC_FEC(+) BETWEEN TO_DATE($P{P_FEC_INI}) AND TO_DATE($P{P_FEC_FIN})
                       AND PDOC_FEC(+) BETWEEN TO_DATE(P_DESDE) AND TO_DATE(P_HASTA)
                       AND PDOC_EMPLEADO(+) = C.LEGAJO
                           AND pdoc_fec <> '13/07/2022'
                     GROUP BY PDOC_EMPLEADO,
                              INITCAP(PCON_DESC),
                              PDDET_CLAVE_CONCEPTO,
                              PDOC_EMPR


                  union all

                  SELECT C.LEGAJO LEGAJO,
                         CASE
                           WHEN PCON_CLAVE IN (31, 4) THEN
                            1
                           WHEN PCON_CLAVE = 16 THEN
                            2
                           WHEN PCON_CLAVE = 18 THEN
                            3
                           WHEN PCON_CLAVE = 7 THEN
                            4
                           WHEN PCON_CLAVE = 30 THEN
                            5
                         END ORDEN,
                         CASE
                           WHEN PCON_CLAVE = 4 THEN
                            'I.P.S'
                           WHEN PCON_CLAVE = 31 THEN
                            'A.M.H'
                           WHEN PCON_CLAVE = 16 THEN
                            'Anticipos'
                           WHEN PCON_CLAVE = 18 THEN
                            'Prestamos'
                           WHEN PCON_CLAVE = 7 THEN
                            'Ausencia'
                           WHEN PCON_CLAVE = 30 THEN
                            'Viatico'
                         END PCON_DESC,
                         NULL otros_Egresos,
                         null,
                         NVL(IMPORTE_LOC, 0) IMPORTE,
                         PCON_CLAVE,
                         'E'
                    FROM PER_CONCEPTO,
                         (SELECT C.LEGAJO LEGAJO,
                                 NVL(SUM(PDDET_IMP_LOC), 0) IMPORTE_LOC,
                                 PDDET_CLAVE_CONCEPTO CONCEPTO,
                                 PDOC_EMPR EMPRESA
                            FROM PER_DOCUMENTO     DO,
                                 PER_DOCUMENTO_DET AA,
                                 PER_CONCEPTO      M
                           WHERE DO.PDOC_CLAVE = AA.PDDET_CLAVE_DOC
                             AND DO.PDOC_EMPR = AA.PDDET_EMPR
                             AND AA.PDDET_CLAVE_CONCEPTO = M.PCON_CLAVE
                             AND AA.PDDET_EMPR = M.PCON_EMPR
                             AND PCON_CANCELADO_POR_CONC IS NULL
                             AND PCON_CLAVE IN (4, 16, 18, 7, 31, 30)
                                ----    AND NVL(PDDET_CLAVE_FIN, PDOC_CLAVE_FIN) is not null
                                --AND PDOC_CLAVE_PADRE IS NULL
                             AND case
                                   when PDOC_CLAVE_PADRE IS not NULL and
                                        PDOC_FORM = 'PERI091' then
                                    1
                                   when PDOC_CLAVE_PADRE IS NULL then
                                    1
                                   else
                                    0
                                 end = 1
                             AND PDOC_FORM <> 'PERI092'
                             AND PCON_EMPR = P_EMPRESA
                                --AND PDOC_PERIODO(+) = P_PERIODO-- BETWEEN TO_DATE(&P_FEC_INI) AND TO_DATE(&P_FEC_FIN)
                             AND PDOC_FEC(+) BETWEEN TO_DATE(P_DESDE) AND
                                 TO_DATE(P_HASTA)
                             AND PDOC_EMPLEADO(+) = C.LEGAJO
                             
                           AND pdoc_fec <> '13/07/2022'
                           GROUP BY PDOC_EMPLEADO,
                                    INITCAP(PCON_DESC),
                                    PDDET_CLAVE_CONCEPTO,
                                    PDOC_EMPR) A
                   WHERE PCON_CLAVE = CONCEPTO ---(+)
                     AND PCON_EMPR = EMPRESA --(+)
                     AND PCON_CLAVE IN (CASE WHEN C.FORMA_PAGO IN (1, 2) THEN 4 ELSE 31 END,
                                        16,
                                        18,
                                        7)
                     AND PCON_EMPR = P_EMPRESA
                  union all
                  SELECT C.LEGAJO LEGAJO,
                         5        orden,
                         /*case when rownum = 1 then
                         'Otros Egresos:'
                         else
                           null
                           end otros_egresos,*/

                         initcap(PCON_DESC),
                         NULL,
                         null,
                         PDDET_IMP_LOC IMPORTE_LOC,
                         PDDET_CLAVE_CONCEPTO CONCEPTO,
                         'E'
                    FROM PER_DOCUMENTO     DO,
                         PER_DOCUMENTO_DET AA,
                         PER_CONCEPTO      M
                   WHERE DO.PDOC_CLAVE = AA.PDDET_CLAVE_DOC
                     AND DO.PDOC_EMPR = AA.PDDET_EMPR
                     AND AA.PDDET_CLAVE_CONCEPTO = M.PCON_CLAVE
                     AND AA.PDDET_EMPR = M.PCON_EMPR
                        --- AND NVL(PDDET_CLAVE_FIN, PDOC_CLAVE_FIN) is not null
                     AND PCON_CANCELADO_POR_CONC IS NULL
                     AND PCON_CLAVE NOT IN (4, 16, 18, 7, 31)
                     AND PCON_CANCELADO_POR_CONC IS NULL
                     AND PCON_IE_RECIBO = 'E'
                     AND PCON_IND_DBCR = 'D'
                     AND PCON_EMPR = P_EMPRESA
                           AND pdoc_fec <> '13/07/2022'
                     AND PDOC_FEC(+) BETWEEN TO_DATE(P_DESDE) AND
                         TO_DATE(P_HASTA)
                        --AND PDOC_PERIODO = P_PERIODO
                     AND PDOC_EMPLEADO(+) = C.LEGAJO)
           ORDER BY 1, 8, 2;

        IF C.MOSTAR_MARC = '1' THEN

          INSERT INTO PERL004_MARC_TEMP
            (MARC_FECHA,
             MARC_EMPLEADO,
             MARC_EVENTO,
             MARC_HORA,
             MARC_ESTADO,
             P_LOGIN,
             P_SESSION)
            SELECT MARC_FECHA,
                   MARC_EMPLEADO,
                   MARC_EVENTO,
                   MARC_HORA,
                   MARC_ESTADO,
                   P_USUARIO,
                   P_SESSION
              FROM PER_PERL004B B, PER_PERIODO PR
             WHERE PR.PERI_CODIGO = P_PERIODO
               AND B.EMPR_CODIGO = P_EMPRESA
               AND B.EMPR_CODIGO = PERI_EMPR
               AND MARC_EMPLEADO = C.LEGAJO;

        END IF;

        --   IF P_INCLUIR  = 'S' THEN
        -------------------------------recibo de adelantos

        INSERT INTO PERL004_TEMP
          (LEGAJO,
           NOMBRE,
           EMPL_DOC_IDENTIDAD,
           EMPL_DIR,
           FORMA_SALARIO_DES,
           MONEDA,
           NRO_RECIBO,
           EMPL_NRO_CTA,
           BANCO_DES,
           CONCEPTO,
           FECHA_LETRA,
           P_LOGIN,
           P_SESSION,
           P_EMPRESA,
           P_TIPO_RECIBO,
           CENTRAL,
           SUCU_CODI,
           SUCURSAL,
           SUC_IND_CASA_CENTRAL)

          SELECT PER_EMPLEADO.EMPL_LEGAJO,
                 'Nombre: ' || PER_EMPLEADO.EMPL_APE || ', ' ||
                 PER_EMPLEADO.EMPL_NOMBRE NOMBRE,
                 EMPL_DOC_IDENT,
                 'Dr.J.E.Estigarribia, ' || TO_CHAR(TO_DATE(P_HASTA), 'dd') ||
                 ' de ' || LTRIM(RTRIM(TO_CHAR(TO_DATE(P_HASTA), 'month'))) ||
                 ' de ' || TO_CHAR(TO_DATE(P_HASTA), 'yyyy') FECHA,
                 M.MON_SIMBOLO,
                 PDOC_MON,
                 PDOC_CLAVE,
                 '                                     Recibi de: ' ||
                 E.EMPR_RAZON_SOCIAL || ' la suma de ' ||
                 GENERAL.FP_CONV_NRO_TXT(ROUND(PDDET_IMP), PDOC_MON) || ' ' ||
                 MON_DESC || ' en concepto de ' ||
                 REPLACE(PCON_DESC, '(-)', '') ||
                 ' a ser descontado de mi sueldo correspondiente al mes de ' ||
                 REPLACE(TO_CHAR(TO_DATE(P_HASTA), 'month'), ' ', '') || '.' EN_CONCEPTO,

                 ROUND(PER_DOCUMENTO_DET.PDDET_IMP) IMPORTE,
                 ROUND(PER_DOCUMENTO_DET.PDDET_IMP_LOC) IMPORTE_LOC,
                 PER_CONCEPTO.PCON_DESC,
                 P_USUARIO,
                 P_SESSION,
                 P_EMPRESA,
                 'RECIBO_ADELANTO',
                 DECODE('S',
                        'S',
                        SUC_DIR || ' TEL/FAX:' || NVL(SUC_TEL, S.SUC_FAX) || ' ' ||
                        S.SUC_EMAIL,
                        NULL) CENTRAL,
                 DECODE(SUC_IND_CASA_CENTRAL,
                        'S',
                        NULL,
                        'SUCURSAL ' || SUC_CODIGO) SUCU_CODI,
                 DECODE(SUC_IND_CASA_CENTRAL,
                        'S',
                        NULL,
                        SUC_DIR || ' TEL/FAX:' || NVL(SUC_TEL, S.SUC_FAX) || ' ' ||
                        S.SUC_EMAIL) SUCURSAL,
                 SUC_IND_CASA_CENTRAL
            FROM PER_DOCUMENTO,
                 PER_DOCUMENTO_DET,
                 PER_CONCEPTO,
                 --  PER_DPTO_CONC ,
                 PER_EMPLEADO,
                 -- FIN_CONCEPTO,
                 GEN_EMPRESA  E,
                 GEN_MONEDA   M,
                 GEN_SUCURSAL S
           WHERE PER_DOCUMENTO.PDOC_CLAVE =
                 PER_DOCUMENTO_DET.PDDET_CLAVE_DOC
             AND PER_CONCEPTO.PCON_CLAVE =
                 PER_DOCUMENTO_DET.PDDET_CLAVE_CONCEPTO
                ---   AND NVL(PDDET_CLAVE_FIN, PDOC_CLAVE_FIN) is not null
             AND PER_EMPLEADO.EMPL_LEGAJO = PER_DOCUMENTO.PDOC_EMPLEADO
             AND PER_DOCUMENTO.PDOC_MON = MON_CODIGO

             AND SUC_CODIGO = 1
             AND SUC_EMPR = PDOC_EMPR

             AND PDOC_EMPR = MON_EMPR
             AND PDOC_EMPR = PDDET_EMPR
             AND PCON_EMPR = PDDET_EMPR
             AND PCON_EMPR = EMPL_EMPRESA
             AND EMPL_EMPRESA = PDOC_EMPR

             AND PDOC_EMPR = EMPR_CODIGO
             AND PDOC_EMPR = P_EMPRESA
             AND EMPL_LEGAJO = C.LEGAJO
             AND PCON_FIN_TMOV IS NULL
             AND PCON_CLAVE NOT IN (28, 37)--> 28: COMBUSTIBLES(-), 37: EGRESOS VARIOS TRANS(+)

             AND NVL(PDOC_CLAVE_FIN, PDDET_CLAVE_FIN) NOT IN
                 (SELECT X.PAG_CLAVE_PAGO
                    FROM FIN_PAGO X
                   WHERE X.PAG_CLAVE_DOC IN
                         (SELECT NVL(S.DOC_CLAVE, 1) HILA_RECIBO
                            FROM FIN_DOCUMENTO S
                           WHERE DOC_CLAVE IN
                                 (SELECT NVL(A.PDOC_CLAVE_FIN,
                                             B.PDDET_CLAVE_FIN) CLAVE_dOC
                                    FROM PER_DOCUMENTO     A,
                                         PER_DOCUMENTO_DET B,
                                         PER_CONCEPTO      C
                                   WHERE A.PDOC_CLAVE = B.PDDET_CLAVE_DOC
                                     AND A.PDOC_EMPR = B.PDDET_EMPR
                                     AND B.PDDET_CLAVE_CONCEPTO = C.PCON_CLAVE
                                     AND B.PDDET_EMPR = C.PCON_EMPR
                                     AND A.PDOC_EMPR = P_EMPRESA
                                     AND PDOC_EMPLEADO = C.LEGAJO
                                     AND PDOC_PERIODO = P_PERIODO
                                     AND C.PCON_IND_DBCR = 'C'
                                     AND NVL(C.PCON_IND_IPS, 'N') = 'N'))
                        --  AND DOC_CLAVE_PADRE_ORIG IS NOT NULL)
                     and (select count(*)
                            from fin_cuota a
                           where a.cuo_clave_doc = x.pag_clave_doc
                             and a.cuo_empr = P_EMPRESA -- 1 era el parametro anterior hasta el 06/08/2022
                          ) >= 3)

             AND PDOC_PERIODO = P_PERIODO
          ---PDOC_FEC BETWEEN TO_DATE(P_DESDE) AND TO_DATE(P_HASTA)
           ORDER BY PER_EMPLEADO.EMPL_NOMBRE || ' ' ||
                    PER_EMPLEADO.EMPL_APE,
                    PER_DOCUMENTO.PDOC_CLAVE;

        FOR RHIL IN (

                     SELECT NVL(S.DOC_CLAVE_PADRE_ORIG, DOC_CLAVE) HILA_RECIBO
                       FROM FIN_DOCUMENTO S
                      WHERE DOC_CLAVE IN
                            (SELECT PAG_CLAVE_PAGO ---A.PAG_CLAVE_DOC
                               FROM FIN_PAGO A
                              WHERE A.PAG_CLAVE_PAGO IN
                                    (

                                     SELECT S.DOC_CLAVE_PADRE_ORIG HILA_RECIBO
                                       FROM FIN_DOCUMENTO S, FIN_CUOTA P
                                      WHERE S.DOC_CLAVE = P.CUO_CLAVE_DOC
                                        AND S.DOC_EMPR = P.CUO_EMPR
                                        AND DOC_CLAVE IN
                                            (SELECT NVL(A.PDOC_CLAVE_FIN,
                                                        B.PDDET_CLAVE_FIN) CLAVE_dOC
                                               FROM PER_DOCUMENTO     A,
                                                    PER_DOCUMENTO_DET B,
                                                    PER_CONCEPTO      C
                                              WHERE A.PDOC_CLAVE =
                                                    B.PDDET_CLAVE_DOC
                                                AND A.PDOC_EMPR = B.PDDET_EMPR
                                                AND B.PDDET_CLAVE_CONCEPTO =
                                                    C.PCON_CLAVE
                                                AND B.PDDET_EMPR = C.PCON_EMPR
                                                AND A.PDOC_EMPR = P_EMPRESA
                                                AND PDOC_EMPLEADO = C.LEGAJO
                                                AND PDOC_PERIODO = P_PERIODO
                                                AND C.PCON_IND_DBCR = 'C'
                                                AND NVL(C.PCON_IND_IPS, 'N') = 'N')
                                      GROUP BY DOC_CLAVE_PADRE_ORIG
                                     having COUNT(P.CUO_CLAVE_DOC) >= 15))) LOOP

          ---------------------------------------------RECIDO DE PAGOS
          INSERT INTO PERL004_REC_TEMP
            (ORIGEN,
             CLI_CODIGO,
             CLI_NOM,
             CLI_RUC,
             NRO_FACTURA,
             NRO_RECIBO,
             COD_MONEDA,
             DOC_CTA_BCO,
             IMPORTE_PAGO,
             FECHA_PAGO,
             DAYPAGO,
             MONTHPAGO,
             YEARPAGO,
             RUC,
             SIMBOLO,
             IMPORTE_RECIBO,
             IMPORTE_PAGO_TXT1,
             IMPORTE_PAGO_TXT2,
             PAG_CLAVE_PAGO,
             PAG_FEC_VTO,
             LUGAR_Y_FECHA,
             P_LOGIN,
             P_SESSION,
             P_LEGAJO,
             P_DETALLE)

            SELECT FD.DOC_CLAVE ORIGEN,
                   FC.CLI_CODIGO,
                   FC.CLI_NOM,
                   FC.CLI_RUC,
                   SUBSTR(LPAD(FD.DOC_NRO_DOC, 13, 0), 0, 3) || '-' ||
                   SUBSTR(LPAD(FD.DOC_NRO_DOC, 13, 0), 4, 3) || '-' ||
                   SUBSTR(LPAD(FD.DOC_NRO_DOC, 13, 0), 7, 14) NRO_FACTURA,
                   SUBSTR(LPAD(FR.DOC_NRO_DOC, 13, 0), 0, 3) || '-' ||
                   SUBSTR(LPAD(FR.DOC_NRO_DOC, 13, 0), 4, 3) || '-' ||
                   SUBSTR(LPAD(FR.DOC_NRO_DOC, 13, 0), 7, 14) NRO_RECIBO,
                   FD.DOC_MON COD_MONEDA,
                   FD.DOC_CTA_BCO,
                   FP.PAG_IMP_MON IMPORTE_PAGO,
                   FR.DOC_FEC_DOC FECHA_PAGO,
                   TO_CHAR(FR.DOC_FEC_DOC, 'dd') AS DAYPAGO,
                   TO_CHAR(FR.DOC_FEC_DOC, 'Month') AS MONTHPAGO,
                   TO_CHAR(FR.DOC_FEC_DOC, 'YYYY') AS YEARPAGO,
                   E.EMPR_RUC RUC,
                   D.MON_SIMBOLO SIMBOLO,
                   LPAD(TRIM(DECODE(MON_CODIGO,
                                    1,
                                    TO_CHAR(FR.DOC_NETO_EXEN_MON,
                                            '999G999G999G999'),
                                    TO_CHAR(FR.DOC_NETO_EXEN_MON,
                                            '999G999G999G999D00'))),
                        30,
                        '-') IMPORTE_RECIBO,
                   RPAD(NVL(GENERAL.FP_CONV_NRO_TXT(FR.DOC_NETO_EXEN_MON,
                                                    FR.DOC_MON),
                            '-'),
                        90,
                        '-') IMPORTE_PAGO_TXT1,
                   RPAD(NVL(SUBSTR(GENERAL.FP_CONV_NRO_TXT(FR.DOC_NETO_EXEN_MON,
                                                           FR.DOC_MON),
                                   91,
                                   1000),
                            '--'),
                        90,
                        '-') IMPORTE_PAGO_TXT2,
                   FP.PAG_CLAVE_PAGO,
                   PAG_FEC_VTO,
                   UPPER(S.SUC_LOCALIDAD || ',  ' ||
                         TO_CHAR(FR.DOC_FEC_DOC, 'dd') || ' de ' ||
                         TO_CHAR(FR.DOC_FEC_DOC,
                                 'Month',
                                 'nls_date_language=spanish') || ' del ' ||
                         TO_CHAR(FR.DOC_FEC_DOC, 'yyyy') || '.') LUGAR_Y_FECHA,
                   P_USUARIO,
                   P_SESSION,
                   C.LEGAJO,
                   FR.DOC_OBS
              FROM FIN_DOCUMENTO  FD,
                   FIN_UNION_PAGO FP,
                   FIN_CLIENTE    FC,
                   GEN_EMPRESA    E,
                   GEN_MONEDA     D,
                   FIN_DOCUMENTO  FR,
                   GEN_SUCURSAL   S
             WHERE FD.DOC_CLAVE = FP.PAG_CLAVE_DOC
               AND FD.DOC_EMPR = FP.PAG_EMPR
               AND FD.DOC_CLI = FC.CLI_CODIGO
               AND FD.DOC_EMPR = FC.CLI_EMPR
               AND FD.DOC_EMPR = EMPR_CODIGO
               AND FD.DOC_MON = MON_CODIGO
               AND FD.DOC_EMPR = MON_EMPR
               AND PAG_CLAVE_PAGO = FR.DOC_CLAVE
               AND PAG_EMPR = FR.DOC_EMPR
               AND FP.PAG_CLAVE_PAGO = RHIL.HILA_RECIBO
                  /*IN (SELECT NVL(A.PDOC_CLAVE_FIN,B.PDDET_CLAVE_FIN) CLAVE_dOC
                   FROM PER_DOCUMENTO A, PER_DOCUMENTO_DET B, PER_CONCEPTO C
                  WHERE A.PDOC_CLAVE = B.PDDET_CLAVE_DOC
                    AND A.PDOC_EMPR = B.PDDET_EMPR
                    AND B.PDDET_CLAVE_CONCEPTO = C.PCON_CLAVE
                    AND B.PDDET_EMPR = C.PCON_EMPR
                    AND A.PDOC_EMPR = P_EMPRESA
                    AND PDOC_EMPLEADO = C.LEGAJO
                    AND PDOC_PERIODO = P_PERIODO
                    AND PCON_CLAVE NOT IN (37,28)
                    AND C.PCON_IND_DBCR =  'D')*/
               AND FP.PAG_EMPR = P_EMPRESA
               AND FR.DOC_SUC = SUC_CODIGO
               AND FR.DOC_EMPR = SUC_EMPR
            UNION ALL
            SELECT FD.DOC_CLAVE ORIGEN,
                   FC.PROV_CODIGO,
                   FC.PROV_RAZON_SOCIAL,
                   FC.PROV_RUC,
                   SUBSTR(LPAD(FD.DOC_NRO_DOC, 13, 0), 0, 3) || '-' ||
                   SUBSTR(LPAD(FD.DOC_NRO_DOC, 13, 0), 4, 3) || '-' ||
                   SUBSTR(LPAD(FD.DOC_NRO_DOC, 13, 0), 7, 14) NRO_FACTURA,
                   SUBSTR(LPAD(FR.DOC_NRO_DOC, 13, 0), 0, 3) || '-' ||
                   SUBSTR(LPAD(FR.DOC_NRO_DOC, 13, 0), 4, 3) || '-' ||
                   SUBSTR(LPAD(FR.DOC_NRO_DOC, 13, 0), 7, 14) NRO_RECIBO,
                   FD.DOC_MON COD_MONEDA,
                   FD.DOC_CTA_BCO,
                   FP.PAG_IMP_MON IMPORTE_PAGO,
                   FR.DOC_FEC_DOC FECHA_PAGO,
                   TO_CHAR(FR.DOC_FEC_DOC, 'dd') AS DAYPAGO,
                   TO_CHAR(FR.DOC_FEC_DOC, 'Month') AS MONTHPAGO,
                   TO_CHAR(FR.DOC_FEC_DOC, 'YYYY') AS YEARPAGO,
                   E.EMPR_RUC RUC,
                   D.MON_SIMBOLO SIMBOLO,
                   LPAD(TRIM(DECODE(MON_CODIGO,
                                    1,
                                    TO_CHAR(FR.DOC_NETO_EXEN_MON,
                                            '999G999G999G999'),
                                    TO_CHAR(FR.DOC_NETO_EXEN_MON,
                                            '999G999G999G999D00'))),
                        30,
                        '-') IMPORTE_RECIBO,
                   RPAD(NVL(GENERAL.FP_CONV_NRO_TXT(FR.DOC_NETO_EXEN_MON,
                                                    FR.DOC_MON),
                            '-'),
                        90,
                        '-') IMPORTE_PAGO_TXT1,
                   RPAD(NVL(SUBSTR(GENERAL.FP_CONV_NRO_TXT(FR.DOC_NETO_EXEN_MON,
                                                           FR.DOC_MON),
                                   91,
                                   1000),
                            '--'),
                        90,
                        '-') IMPORTE_PAGO_TXT2,
                   FP.PAG_CLAVE_PAGO,
                   PAG_FEC_VTO,
                   UPPER(S.SUC_LOCALIDAD || ',  ' ||
                         TO_CHAR(FR.DOC_FEC_DOC, 'dd') || ' de ' ||
                         TO_CHAR(FR.DOC_FEC_DOC,
                                 'Month',
                                 'nls_date_language=spanish') || ' del ' ||
                         TO_CHAR(FR.DOC_FEC_DOC, 'yyyy') || '.') LUGAR_Y_FECHA,
                   P_USUARIO,
                   P_SESSION,
                   C.LEGAJO,
                   FD.DOC_OBS
              FROM FIN_DOCUMENTO  FD,
                   FIN_UNION_PAGO FP,
                   FIN_PROVEEDOR  FC,
                   GEN_EMPRESA    E,
                   GEN_MONEDA     D,
                   FIN_DOCUMENTO  FR,
                   GEN_SUCURSAL   S
             WHERE FD.DOC_CLAVE = FP.PAG_CLAVE_DOC
               AND FD.DOC_EMPR = FP.PAG_EMPR
               AND FD.DOC_PROV = FC.PROV_CODIGO
               AND FD.DOC_EMPR = FC.PROV_EMPR
               AND FD.DOC_EMPR = EMPR_CODIGO
               AND FD.DOC_MON = MON_CODIGO
               AND FD.DOC_EMPR = MON_EMPR
               AND PAG_CLAVE_PAGO = FR.DOC_CLAVE
               AND PAG_EMPR = FR.DOC_EMPR
                  --    AND ---FP.PAG_CLAVE_DOC
               AND PAG_CLAVE_PAGO = RHIL.HILA_RECIBO
                  /*  IN (SELECT NVL(A.PDOC_CLAVE_FIN,B.PDDET_CLAVE_FIN) CLAVE_dOC
                   FROM PER_DOCUMENTO A, PER_DOCUMENTO_DET B, PER_CONCEPTO C
                  WHERE A.PDOC_CLAVE = B.PDDET_CLAVE_DOC
                    AND A.PDOC_EMPR = B.PDDET_EMPR
                    AND B.PDDET_CLAVE_CONCEPTO = C.PCON_CLAVE
                    AND B.PDDET_EMPR = C.PCON_EMPR
                    AND A.PDOC_EMPR = P_EMPRESA
                    AND PDOC_EMPLEADO = C.LEGAJO
                    AND PDOC_PERIODO = P_PERIODO
                    AND PCON_CLAVE NOT IN (37,28)
                    AND C.PCON_IND_DBCR =  'D')*/
               AND FP.PAG_EMPR = P_EMPRESA
               AND FR.DOC_SUC = SUC_CODIGO
               AND FR.DOC_EMPR = SUC_EMPR
             ORDER BY ORIGEN;

        END LOOP;

        FOR TRANSA IN (SELECT A.DOC_CLAVE_PADRE_ORIG
                         FROM FIN_DOCUMENTO A
                        WHERE DOC_EMPR = 1
                          AND DOC_CLAVE IN
                              (SELECT P.PAG_CLAVE_DOC
                                 FROM FIN_PAGO P
                                WHERE P.PAG_EMPR = 1
                                  AND P.PAG_CLAVE_PAGO IN
                                      (SELECT NVL(PDOC_CLAVE_FIN,
                                                  PDDET_CLAVE_FIN) CLAVE
                                         FROM PER_DOCUMENTO     A,
                                              PER_DOCUMENTO_DET B ---, PER_CONCEPTO C
                                        WHERE A.PDOC_CLAVE = B.PDDET_CLAVE_DOC
                                          AND A.PDOC_EMPR = B.PDDET_EMPR
                                          AND A.PDOC_EMPR = 1
                                          AND B.PDDET_CLAVE_CONCEPTO IN
                                              (37, 28, 29, 38)
                                          AND PDOC_EMPLEADO = C.LEGAJO
                                          AND PDOC_FEC BETWEEN
                                              TO_DATE(P_DESDE) AND
                                              TO_DATE(P_HASTA)))) LOOP

          INSERT INTO PERL004_REC_TRANS_TEMP
            (IMPORTE_RECIBO,
             MON_CODIGO,
             MON_SIMBOLO,
             MON_DESC,
             NRO_RECIBO,
             NRO_FACTURA_1,
             CLI_NOM,
             CLI_RUC,
             NRO_FACTURA,
             IMPORTE_COBRO_CUOTA,
             MON_DEC_IMP,
             FECHA_RECIBO,
             DOC_CLAVE_LIQUID,
             DOC_CLAVE,
             P_LEGAJO,
             P_SESSION,
             P_LOGIN,
             P_DETALLE,
             SUCU_CODI,
             SUCURSAL,
             SUC_IND_CASA_CENTRAL)

            SELECT R.DOC_NETO_EXEN_MON IMPORTE_RECIBO,
                   M.MON_CODIGO,
                   M.MON_SIMBOLO,
                   M.MON_DESC,
                   R.DOC_NRO_DOC NRO_RECIBO,
                   fp_formato_nro_fac(v_nro => F.DOC_NRO_DOC) NRO_FACTURA_1,
                   C.CLI_NOM,
                   C.CLI_RUC,
                   F.DOC_NRO_DOC NRO_FACTURA,
                   UP.pag_imp_mon IMPORTE_COBRO_CUOTA,
                   mon_dec_imp,
                   suc_dir || ' , ' || TO_CHAR(R.DOC_FEC_DOC, 'DD') ||
                   ' de ' ||
                   RTRIM(TO_CHAR(R.DOC_FEC_DOC,
                                 'MONTH',
                                 'nls_date_language=spanish')) || ' de ' ||
                   TO_CHAR(R.DOC_FEC_DOC, 'YYYY') || '.' FECHA_RECIBO,
                   NVL(r.doc_clave_liquid, r.doc_clave) doc_clave_liquid,
                   r.DOC_CLAVE,
                   c.legajo,
                   P_SESSION,
                   P_USUARIO,
                   R.DOC_OBS,
                   'SUCURSAL ' || SUC_CODIGO || ' : ' SUCU_CODI,
                   SUC_DIR || ' TEL:' || SUC_TEL SUCURSAL,
                   SUC_IND_CASA_CENTRAL
              FROM FIN_DOCUMENTO   R,
                   FIN_UNION_PAGO  UP,
                   FIN_UNION_CUOTA UC,
                   FIN_DOCUMENTO   F,
                   FIN_CLIENTE     C,
                   GEN_MONEDA      M,
                   GEN_SUCURSAL
             WHERE R.DOC_EMPR = 2

               AND R.DOC_CLAVE = UP.PAG_CLAVE_PAGO
               AND R.DOC_EMPR = UP.PAG_EMPR

               AND UP.PAG_CLAVE_DOC = UC.CUO_CLAVE_DOC
               AND UP.PAG_EMPR = UC.CUO_EMPR

               AND UP.PAG_FEC_VTO = UC.CUO_FEC_VTO
               AND UP.PAG_EMPR = UC.CUO_EMPR

               AND UC.CUO_CLAVE_DOC = F.DOC_CLAVE
               AND UC.CUO_EMPR = F.DOC_EMPR

               AND R.DOC_CLI = C.CLI_CODIGO
               AND R.DOC_EMPR = C.CLI_EMPR

               AND R.DOC_TIPO_MOV = 6
               AND R.DOC_MON = M.MON_CODIGO
               AND R.DOC_EMPR = M.MON_EMPR

               AND R.DOC_SUC = SUC_CODIGO
               AND R.DOC_EMPR = SUC_EMPR

               AND R.DOC_CLAVE = TRANSA.DOC_CLAVE_PADRE_ORIG

             ORDER BY R.DOC_CLAVE, UP.PAG_FEC_VTO;

        END LOOP;
        --  END IF;

      END IF;
      IF P_TIPO in (1, 3) THEN

        INSERT INTO PERL004_TEMP
          (LEGAJO,
           NOMBRE,
           EMPL_DOC_IDENTIDAD,
           EMPL_DIR,
           PERIODO,
           PER_INICIO,
           PER_FIN,
           DIAS_TRABAJADO,
           FORMA_SALARIO_DES,
           MONEDA,
           NRO_RECIBO,
           SAL_TOTAL,
           FORMA_PAGO,
           EMPL_NRO_CTA,
           EMPL_BCO_PAGO,
           BANCO_DES,
           FECHA_LETRA,
           SALARIO,
           EMPL_TEL,
           MON_ISO,
           MOSTAR_MARC,
           ORDEN,
           CONCEPTO,
           CONC_DETALLE,
           IMPORTE,
           TIPO_CONCEPTO,
           TASA,
           P_LOGIN,
           P_SESSION,
           P_DEPARTAMENTO,
           P_SUCURSAL,
           P_EMPRESA,
           EMPL_IND_HS_EXTRA,
           HS_TRABAJADAS,
           CENTRAL,
           SUCU_CODI,
           SUCURSAL,
           SUC_IND_CASA_CENTRAL)

        VALUES
          (C.LEGAJO,
           C.NOMBRE,
           C.EMPL_DOC_IDENTIDAD,
           C.EMPL_DIR,
           P_PERIODO,
           X_PERI_INICIO,
           X_PERI_FIN,
           C.DIAS_TRABAJADO,
           C.FORMA_SALARIO_DES,
           P_MONEDA,
           C.NRO_RECIBO,
           C.TOTAL,
           C.FORMA_PAGO,
           C.EMPL_NRO_CTA,
           C.EMPL_BCO_PAGO,
           C.BANCO_DES,
           NULL,
           C.SALARIO,
           C.EMPL_TEL,
           NULL,
           C.MOSTAR_MARC, -----MARCACION
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           P_USUARIO,
           P_SESSION,
           P_DEPARTAMENTO,
           P_SUCURSAL,
           P_EMPRESA,
           NULL,
           NULL,
           C.CENTRAL,
           C.SUCU_CODI,
           C.SUCURSAL,
           C.SUC_IND_CASA_CENTRAL);

      END IF;

      IF P_TIPO = 4 THEN
        FOR FAC IN (SELECT A.DOC_CLAVE FAC_CLAVE, A.DOC_CLAVE_STK STK_CLAVE
                      FROM FIN_DOCUMENTO A
                     WHERE DOC_EMPR = 1
                       AND DOC_TIPO_MOV = 10
                       AND DOC_CLAVE IN
                           (SELECT T.PAG_CLAVE_DOC
                              FROM FIN_PAGO T
                             WHERE T.PAG_EMPR = 1
                               AND T.PAG_CLAVE_PAGO IN
                                   (SELECT NVL(S.DOC_CLAVE_PADRE_ORIG,
                                               DOC_CLAVE) HILA_RECIBO
                                      FROM FIN_DOCUMENTO S
                                     WHERE DOC_CLAVE IN
                                          /* (SELECT A.PAG_CLAVE_DOC
                                           FROM FIN_PAGO A
                                          WHERE A.PAG_CLAVE_PAGO IN*/
                                           (SELECT NVL(A.PDOC_CLAVE_FIN,
                                                       B.PDDET_CLAVE_FIN) CLAVE_DOC
                                              FROM PER_DOCUMENTO     A,
                                                   PER_DOCUMENTO_DET B,
                                                   PER_CONCEPTO      C
                                             WHERE A.PDOC_CLAVE =
                                                   B.PDDET_CLAVE_DOC
                                               AND A.PDOC_EMPR = B.PDDET_EMPR
                                               AND B.PDDET_CLAVE_CONCEPTO =
                                                   C.PCON_CLAVE
                                               AND B.PDDET_EMPR = C.PCON_EMPR
                                               AND A.PDOC_EMPR = P_EMPRESA
                                               AND PDOC_EMPLEADO = C.LEGAJO
                                               AND PDOC_PERIODO = P_PERIODO
                                               AND PCON_CLAVE = 1 -- NOT IN (37, 28)
                                            -- AND C.PCON_IND_DBCR = 'D'
                                            ))) --)
                     group by A.DOC_CLAVE, A.DOC_CLAVE_STK) LOOP

          IF FAC.STK_CLAVE IS NOT NULL THEN

            INSERT INTO PERL004_FAC_TEMP
              (DOC_MON,
               DOC_CLAVE,
               NRO_COMPROBANTE,
               FECHA,
               MARCA_X_CONT,
               MARCA_X_CRED,
               PLAZO,
               RUC,
               NOMBRE,
               DIRECCION,
               TELEFONO,
               NUMERO_TIMBRADO,
               DATE_INI_TIMBRADO,
               DATE_FIN_TIMBRADO,
               DET_NRO_ITEM,
               COD_ARTICULO,
               DESC_ARTICULO,
               ART_COD_BARRA,
               CANTIDAD,
               PRECIO_UNIT,
               EXENTO,
               NETO_5,
               NETO_10,
               SUMA_IVA_5,
               SUMA_IVA_10,
               TOTAL_IVA,
               TOTAL_EXENTO,
               TOTAL_NETO_5,
               TOTAL_NETO_10,
               TOTAL,
               CLI_OBS_FACT,
               MON_SIMBOLO,
               TIMB_AUTORIZACION,
               HOL_CODIGO,
               CLI_CODIGO,
               MON_DESC,
               MON_CODIGO,
               P_SESSION,
               P_LEGAJO,
               P_USUARIO,
               P_TIPO,
               CENTRAL,
               SUCU_CODI,
               SUCURSAL,
               SUC_IND_CASA_CENTRAL)
              SELECT DOC_MON,
                     F.DOC_CLAVE,
                     SUBSTR('00' || DOC_NRO_DOC, 0, 3) || '-' ||
                     SUBSTR(DOC_NRO_DOC, 2, 3) || '-' ||
                     SUBSTR(DOC_NRO_DOC, 5, 7) NRO_COMPROBANTE,
                     REPLACE(TO_CHAR(DOC_FEC_DOC, 'DD/MM/YYYY'), ' ') FECHA,
                     DECODE(DOC_TIPO_MOV, 9, 'X') MARCA_X_CONT,
                     DECODE(DOC_TIPO_MOV, 10, 'X') MARCA_X_CRED,
                     DECODE(DOC_TIPO_MOV,
                            10,
                            FIN_VENCIMIENTOS_FACTURAS(F.DOC_CLAVE, DOC_EMPR),
                            NULL) PLAZO,
                     NVL(F.DOC_CLI_RUC, C.CLI_RUC) RUC,
                     '(' || F.DOC_CLI || ') ' ||
                     NVL(F.DOC_CLI_NOM, C.CLI_NOM) NOMBRE,
                     Z.ZONA_DESC || ' - ' || NVL(F.DOC_CLI_DIR, C.CLI_DIR) DIRECCION,
                     NVL(F.DOC_CLI_TEL, C.CLI_TEL) TELEFONO,
                     T.TIMB_NUMERO AS NUMERO_TIMBRADO,
                     TO_CHAR(T.TIMB_FECH_INIC, 'DD/MM/YYYY') AS DATE_INI_TIMBRADO,
                     TO_CHAR(T.TIMB_FECH_FIN, 'DD/MM/YYYY') AS DATE_FIN_TIMBRADO,
                     D.DET_NRO_ITEM,
                     S.ART_CODIGO COD_ARTICULO,
                     S.ART_DESC DESC_ARTICULO,
                     S.ART_COD_BARRA,
                     DECODE(NVL(D.DET_CANT, 0), 0, 1, D.DET_CANT) CANTIDAD,
                     (NVL(D.DET_NETO_MON, 0) + NVL(D.DET_IVA_MON, 0)) /
                     DECODE(NVL(D.DET_CANT, 0), 0, 1, D.DET_CANT) PRECIO_UNIT,
                     DECODE(S.ART_IMPU, 1, D.DET_NETO_MON, 0) EXENTO,
                     DECODE(S.ART_IMPU,
                            5,
                            (D.DET_NETO_MON + D.DET_IVA_MON),
                            0) NETO_5,
                     DECODE(S.ART_IMPU,
                            2,
                            (D.DET_NETO_MON + D.DET_IVA_MON),
                            0) NETO_10,
                     F.DOC_IVA_5_MON SUMA_IVA_5,
                     F.DOC_IVA_10_MON SUMA_IVA_10,
                     (F.DOC_IVA_5_MON + F.DOC_IVA_10_MON) TOTAL_IVA,
                     F.DOC_NETO_EXEN_MON TOTAL_EXENTO,
                     (NVL(F.DOC_GRAV_5_MON, 0) + NVL(F.DOC_IVA_5_MON, 0)) TOTAL_NETO_5,
                     (NVL(F.DOC_GRAV_10_MON, 0) + NVL(F.DOC_IVA_10_MON, 0)) TOTAL_NETO_10,
                     (NVL(F.DOC_NETO_EXEN_MON, 0) +
                     NVL(F.DOC_NETO_GRAV_MON, 0) + NVL(F.DOC_IVA_MON, 0)) TOTAL,
                     HOL_OBS_FACT CLI_OBS_FACT,
                     M.MON_SIMBOLO,
                     T.TIMB_AUTORIZACION,
                     H.HOL_CODIGO,
                     CLI_CODIGO,
                     M.MON_DESC,
                     M.MON_CODIGO,
                     P_SESSION,
                     C.LEGAJO,
                     P_USUARIO,
                     'A',
                     DECODE('S',
                            'S',
                            SUC_DIR || ' TEL/FAX:' ||
                            NVL(SUC_TEL, S.SUC_FAX) || ' ' || S.SUC_EMAIL,
                            NULL) CENTRAL,
                     DECODE(SUC_IND_CASA_CENTRAL,
                            'S',
                            NULL,
                            'SUCURSAL ' || SUC_CODIGO) SUCU_CODI,
                     DECODE(SUC_IND_CASA_CENTRAL,
                            'S',
                            NULL,
                            SUC_DIR || ' TEL/FAX:' ||
                            NVL(SUC_TEL, S.SUC_FAX) || ' ' || S.SUC_EMAIL) SUCURSAL,
                     SUC_IND_CASA_CENTRAL
                FROM FIN_DOCUMENTO     F,
                     FAC_DOCUMENTO_DET D,
                     FIN_TIMBRADO      T,
                     STK_ARTICULO      S,
                     FIN_CLIENTE       C,
                     FIN_FICHA_HOLDING H,
                     GEN_MONEDA        M,
                     FAC_ZONA          Z,
                     GEN_SUCURSAL      S
               WHERE F.DOC_CLAVE = D.DET_CLAVE_DOC
                 AND F.DOC_EMPR = D.DET_EMPR

                 AND DOC_SUC = SUC_CODIGO
                 AND DOC_EMPR = SUC_EMPR

                 AND D.DET_ART = S.ART_CODIGO
                 AND D.DET_EMPR = S.ART_EMPR

                 AND C.CLI_ZONA = Z.ZONA_CODIGO
                 AND C.CLI_EMPR = Z.ZONA_EMPR

                 AND F.DOC_CLI = C.CLI_CODIGO
                 AND DOC_EMPR = CLI_EMPR
                 AND F.DOC_CLAVE_FAC_NCR IS NULL
                 AND F.DOC_TIMBRADO = T.TIMB_NUMERO
                 AND DOC_EMPR = TIMB_EMPR

                 AND C.CLI_COD_FICHA_HOLDING = H.HOL_CODIGO
                 AND CLI_EMPR = HOL_EMPR
                 AND DOC_TIPO_MOV IN (9, 10)

                 AND F.DOC_NRO_DOC BETWEEN T.TIMB_DOCU_INIC AND
                     T.TIMB_DOCU_FIN
                 AND F.DOC_FEC_DOC BETWEEN T.TIMB_FECH_INIC AND
                     T.TIMB_FECH_FIN

                 AND T.TIMB_CLASE_MOV = 1

                 AND F.DOC_MON = M.MON_CODIGO
                 AND F.DOC_MON = MON_EMPR

                 AND DOC_EMPR = 1
                 AND F.DOC_CLAVE = FAC.FAC_CLAVE

               ORDER BY F.DOC_CLAVE, DOC_NRO_DOC, D.DET_NRO_ITEM;

          ELSE
            INSERT INTO PERL004_FAC_TEMP
              (DOC_MON,
               DOC_CLAVE,
               NRO_COMPROBANTE,
               FECHA,
               MARCA_X_CONT,
               MARCA_X_CRED,
               PLAZO,
               RUC,
               NOMBRE,
               DIRECCION,
               TELEFONO,
               NUMERO_TIMBRADO,
               DATE_INI_TIMBRADO,
               DATE_FIN_TIMBRADO,
               DET_NRO_ITEM,
               COD_ARTICULO,
               DESC_ARTICULO,
               ART_COD_BARRA,
               CANTIDAD,
               PRECIO_UNIT,
               EXENTO,
               NETO_5,
               NETO_10,
               SUMA_IVA_5,
               SUMA_IVA_10,
               TOTAL_IVA,
               TOTAL_EXENTO,
               TOTAL_NETO_5,
               TOTAL_NETO_10,
               TOTAL,
               CLI_OBS_FACT,
               MON_SIMBOLO,
               TIMB_AUTORIZACION,
               HOL_CODIGO,
               CLI_CODIGO,
               MON_DESC,
               MON_CODIGO,
               P_SESSION,
               P_LEGAJO,
               P_USUARIO,
               P_TIPO,
               CENTRAL,
               SUCU_CODI,
               SUCURSAL,
               SUC_IND_CASA_CENTRAL)

              SELECT DISTINCT A.DOC_MON,
                              A.DOC_CLAVE,
                              SUBSTR('00' || A.DOC_NRO_DOC, 0, 3) || '-' ||
                              SUBSTR(A.DOC_NRO_DOC, 2, 3) || '-' ||
                              SUBSTR(A.DOC_NRO_DOC, 5, 7) NRO_COMPROBANTE,
                              REPLACE(TO_CHAR(A.DOC_FEC_DOC, 'DD/MM/YYYY'),
                                      ' ') FECHA,
                              DECODE(a.DOC_TIPO_MOV, 9, 'X') MARCA_X_CONT,
                              DECODE(a.DOC_TIPO_MOV, 10, 'X') MARCA_X_CRED,
                              A.DOC_CLI COD_CLI, -- PLAZO,
                              A.DOC_CLI_RUC RUC,
                              '(' || A.DOC_CLI || ') ' || D.CLI_NOM NOMBRE,
                              D.CLI_DIR DIRECCION,
                              D.CLI_TEL TELEFONO,
                              FT.TIMB_NUMERO NUMERO_TIMBRADO,
                              TO_CHAR(FT.TIMB_FECH_INIC, 'DD/MM/YYYY') AS DATE_INI_TIMBRADO,
                              TO_CHAR(FT.TIMB_FECH_FIN, 'DD/MM/YYYY') AS DATE_FIN_TIMBRADO,
                              1 DET_NRO_ITEM,
                              1,
                              a.doc_obs DESC_ARTICULO,
                              null ART_COD_BARRA,
                              1 CANTIDAD,
                              A.DOC_NETO_EXEN_MON + A.DOC_NETO_GRAV_MON +
                              A.DOC_IVA_MON PRECIO_UNIT,
                              NVL(A.DOC_NETO_EXEN_MON, 0) EXENTO,
                              NVL(A.DOC_GRAV_5_MON, 0) +
                              NVL(A.DOC_IVA_5_MON, 0) NETO_5,
                              NVL(A.DOC_GRAV_10_MON, 0) +
                              NVL(A.DOC_IVA_10_MON, 0) NETO_10,
                              NVL(A.DOC_IVA_5_MON, 0) SUMA_IVA_5,
                              NVL(A.DOC_IVA_10_MON, 0) SUMA_IVA_10,
                              (NVL(A.DOC_IVA_5_MON, 0) +
                              NVL(A.DOC_IVA_10_MON, 0)) TOTAL_IVA,
                              A.DOC_NETO_EXEN_MON TOTAL_EXENTO,
                              (NVL(A.DOC_GRAV_5_MON, 0) +
                              NVL(A.DOC_IVA_5_MON, 0)) TOTAL_NETO_5,
                              (NVL(A.DOC_GRAV_10_MON, 0) +
                              NVL(A.DOC_IVA_10_MON, 0)) TOTAL_NETO_10,
                              (NVL(A.DOC_NETO_EXEN_MON, 0) +
                              NVL(A.DOC_NETO_GRAV_MON, 0) +
                              NVL(A.DOC_IVA_MON, 0)) TOTAL,
                              DECODE(F.DOC_NRO_DOC,
                                     NULL,
                                     NULL,
                                     (SUBSTR('00' || F.DOC_NRO_DOC, 0, 3) || '-' ||
                                     SUBSTR(F.DOC_NRO_DOC, 2, 3) || '-' ||
                                     SUBSTR(F.DOC_NRO_DOC, 5, 7))) NRO_FACTURA, ---CLI_OBS_fAC
                              F.DOC_FEC_DOC FECHA_FACT,
                              FT.TIMB_AUTORIZACION,
                              NULL,
                              NULL,
                              M.MON_DESC,
                              M.MON_CODIGO,
                              P_SESSION,
                              P_LEGAJO,
                              P_USUARIO,
                              'B',
                              DECODE('S',
                                     'S',
                                     SUC_DIR || ' TEL/FAX:' ||
                                     NVL(SUC_TEL, S.SUC_FAX) || ' ' ||
                                     S.SUC_EMAIL,
                                     NULL) CENTRAL,
                              DECODE(SUC_IND_CASA_CENTRAL,
                                     'S',
                                     NULL,
                                     'SUCURSAL ' || SUC_CODIGO) SUCU_CODI,
                              DECODE(SUC_IND_CASA_CENTRAL,
                                     'S',
                                     NULL,
                                     SUC_DIR || ' TEL/FAX:' ||
                                     NVL(SUC_TEL, S.SUC_FAX) || ' ' ||
                                     S.SUC_EMAIL) SUCURSAL,
                              SUC_IND_CASA_CENTRAL
                FROM FIN_DOCUMENTO    A,
                     FIN_DOC_CONCEPTO B,
                     FIN_CONCEPTO     C,
                     FIN_CLIENTE      D,
                     FIN_TIMBRADO     FT,
                     GEN_MONEDA       M,
                     FIN_DOCUMENTO    F,
                     GEN_SUCURSAL     S
               WHERE A.DOC_CLAVE = B.DCON_CLAVE_DOC
                 AND A.DOC_EMPR = B.DCON_EMPR

                 AND A.DOC_SUC = SUC_CODIGO
                 AND A.DOC_EMPR = SUC_EMPR

                 AND B.DCON_CLAVE_CONCEPTO = c.fcon_clave(+)
                 AND B.DCON_EMPR = C.FCON_EMPR(+)
                 AND A.DOC_CLI = D.CLI_CODIGO
                 AND F.DOC_CLAVE(+) = A.DOC_CLAVE_FAC_NCR
                 AND F.DOC_EMPR(+) = A.DOC_EMPR
                 AND A.DOC_EMPR = CLI_EMPR
                 AND A.DOC_EMPR = P_EMPRESA
                 AND A.DOC_TIMBRADO = FT.TIMB_NUMERO(+)
                 AND A.DOC_EMPR = TIMB_EMPR(+)
                 AND A.DOC_NRO_DOC BETWEEN FT.TIMB_DOCU_INIC AND
                     FT.TIMB_DOCU_FIN
                 AND A.DOC_MON = M.MON_CODIGO
                 AND A.DOC_EMPR = MON_EMPR
                 and a.doc_obs is not null
                 AND A.DOC_CLAVE = FAC.FAC_CLAVE;

          END IF;

        END LOOP;
      END IF;

    END LOOP;


    BEGIN
      -- CALL THE PROCEDURE
      PERL004.PP_LLAMAR_REPORTE(P_EMPRESA      => P_EMPRESA,
                                P_SESSION      => P_SESSION,
                                P_USUARIO      => P_USUARIO,
                                P_TIPO_REPORTE => V_REPORTE);
    END;


  END PP_RECIBO_SALARIO;
  PROCEDURE PP_LLAMAR_REPORTE(P_EMPRESA      IN NUMBER,
                              P_SESSION      IN NUMBER,
                              P_USUARIO      IN VARCHAR2,
                              P_TIPO_REPORTE IN VARCHAR2) IS

    V_AMPER      VARCHAR2(2) := '&';
    V_PARAMETROS VARCHAR2(1000);
    V_USER       VARCHAR2(20);
    l_tipo_reporte varchar2(400);
  BEGIN

    V_PARAMETROS := 'P_FORMATO=' || APEX_UTIL.URL_ENCODE('pdf');
    V_PARAMETROS := V_PARAMETROS || V_AMPER || 'P_EMPRESA=' ||
                    URL_ENCODE(P_EMPRESA); ---
    V_PARAMETROS := V_PARAMETROS || V_AMPER || 'P_LOGIN=' ||
                    URL_ENCODE(P_USUARIO); ---
    V_PARAMETROS := V_PARAMETROS || V_AMPER || 'P_SESSION=' ||
                    URL_ENCODE(P_SESSION); ---
    V_PARAMETROS := V_PARAMETROS || V_AMPER || 'P_MONEDA=' || URL_ENCODE(1); ---
    V_PARAMETROS := V_PARAMETROS || V_AMPER || '=' || URL_ENCODE(1); ---
    /* IF P_EMPRESA  =  2 THEN

     V_PARAMETROS := V_PARAMETROS || V_AMPER || 'P_FEC_FIN=' ||
                     URL_ENCODE(1);

    END IF  ;    */

    ---RAISE_APPLICATION_ERROR (-20001,P_EMPRESA||'- '||P_USUARIO ||'- '||P_SESSION);

    if p_empresa in ( 1,2,3 ) then
      l_tipo_reporte := P_TIPO_REPORTE;
    else
      l_tipo_reporte := P_TIPO_REPORTE||'_HOLDING';
    end if;
    
    DELETE FROM GEN_PARAMETROS_REPORT WHERE USUARIO = P_USUARIO;
    INSERT INTO GEN_PARAMETROS_REPORT
      (PARAMETROS, USUARIO, NOMBRE_REPORTE, FORMATO_SALIDA)
    VALUES
    -- (V_PARAMETROS, V_USER, 'PERI088', V_TIPO_SALIDA);
      (V_PARAMETROS, P_USUARIO, P_TIPO_REPORTE, 'pdf');
    COMMIT;

  END PP_LLAMAR_REPORTE;

  PROCEDURE PP_LLAMAR_REPORTE_TAGRO(P_EMPRESA      IN NUMBER,
                                    P_SESSION      IN NUMBER,
                                    P_USUARIO      IN VARCHAR2,
                                    P_TIPO_REPORTE IN VARCHAR2,
                                    P_FECHA_DESDE  IN DATE,
                                    P_FECHA_HASTA  IN DATE) AS

    V_AMPER      VARCHAR2(2) := '&';
    V_PARAMETROS VARCHAR2(1000);
    V_USER       VARCHAR2(20);

  BEGIN

    V_PARAMETROS := 'P_FORMATO=' || APEX_UTIL.URL_ENCODE('pdf');
    V_PARAMETROS := V_PARAMETROS || V_AMPER || 'P_EMPRESA=' ||
                    URL_ENCODE(P_EMPRESA); ---
    V_PARAMETROS := V_PARAMETROS || V_AMPER || 'P_LOGIN=' ||
                    URL_ENCODE(P_USUARIO); ---
    V_PARAMETROS := V_PARAMETROS || V_AMPER || 'P_SESSION=' ||
                    URL_ENCODE(P_SESSION); ---
    V_PARAMETROS := V_PARAMETROS || V_AMPER || '=' || URL_ENCODE(1); ---
    V_PARAMETROS := V_PARAMETROS || V_AMPER || 'P_FEC_INI=' ||
                    URL_ENCODE(P_FECHA_DESDE);
    V_PARAMETROS := V_PARAMETROS || V_AMPER || 'P_FEC_FIN=' ||
                    URL_ENCODE(P_FECHA_HASTA);

    DELETE FROM GEN_PARAMETROS_REPORT WHERE USUARIO = P_USUARIO;
    INSERT INTO GEN_PARAMETROS_REPORT
      (PARAMETROS, USUARIO, NOMBRE_REPORTE, FORMATO_SALIDA)
    VALUES

      (V_PARAMETROS, P_USUARIO, P_TIPO_REPORTE, 'pdf');
    COMMIT;

  END;

  PROCEDURE PP_RECIBO_SALARIO_TAGRO(P_EMPRESA      IN NUMBER,
                                    P_PERIODO      IN NUMBER,
                                    P_DESDE        IN DATE,
                                    P_HASTA        IN DATE,
                                    P_CONCEPTO     IN NUMBER,
                                    P_LEGAJO       IN NUMBER,
                                    P_SUCURSAL     IN NUMBER,
                                    P_FORMA_PAGO   IN NUMBER,
                                    P_DEPARTAMENTO IN NUMBER,
                                    P_MONEDA       IN NUMBER,
                                    P_SESSION      IN NUMBER,
                                    P_SITUACION    IN VARCHAR2,
                                    P_TIPO         IN NUMBER,
                                    P_USUARIO      IN VARCHAR2,
                                    P_EXCLUIR_LEG  IN VARCHAR2) AS

    TYPE CURTYP IS REF CURSOR;
    C_PERL204    CURTYP;
    C_PERL204A   CURTYP;
    C_PERL204CAB CURTYP;
    V_SQL        VARCHAR2(30000);
    V_SQLCAB     VARCHAR2(30000);
    V_WHERE      VARCHAR2(30000);
    V_REPORTE    VARCHAR2(50);
    V_WHERE_2    VARCHAR2(30000);
    V_CARGO      NUMBER;
    V_DOC_OC     NUMBER;
    TYPE CUR_TYPE IS RECORD(
      EMPL_LEGAJO          NUMBER,
      NOMBRE               VARCHAR2(100),
      FECHA                VARCHAR2(250),
      PCON_DESC            VARCHAR2(100),
      IMPORTE              NUMBER,
      IMPORTE_LOC          NUMBER,
      PDOC_MON             NUMBER,
      PDDET_CLAVE_FIN      NUMBER,
      EMPL_DOC_IDENT       VARCHAR2(25),
      FECHA_CAB            DATE,
      PDOC_CLAVE           NUMBER,
      SUCU_CODI            VARCHAR2(250),
      SUCURSAL             VARCHAR2(250),
      SUC_IND_CASA_CENTRAL VARCHAR2(250));
    C CUR_TYPE;
    -----------------------------------------------
    TYPE CUR_TYPE2 IS RECORD(
      LEGAJO     NUMBER,
      P_CONCEPTO VARCHAR2(100),
      MONTO      NUMBER,
      MONTO2     NUMBER,
      TIPO       VARCHAR2(9)

      );
    A CUR_TYPE2;
    -------------------------------------------------
    TYPE CUR_TYPE3 IS RECORD(
      NOMBRE               VARCHAR2(50),
      EMPL_TEL             VARCHAR2(50),
      EMPL_DOC_IDENT       VARCHAR2(50),
      EMPL_DIR             VARCHAR2(50),
      SUCURSAL             VARCHAR2(50),
      SALARIO_DESC         VARCHAR2(200),
      EMPL_LEGAJO          NUMBER,
      BANCO_DES            VARCHAR2(50),
      EMPL_NRO_CTA         NUMBER,
      EMPL_COB_EFECTIVO    VARCHAR2(2),
      SUCU_CODI            VARCHAR2(200),
      SUCURSAL_DES         VARCHAR2(200),
      SUC_IND_CASA_CENTRAL VARCHAR2(200),
      PDOC_CLAVE           NUMBER);
    CA CUR_TYPE3;

    --------------------------------------------------

  BEGIN

    /*****  Para Detalle De Choferes   ****/
    IF P_TIPO = 2 THEN
      SELECT P.EMPL_CARGO
        INTO V_CARGO
        FROM PER_EMPLEADO P
       WHERE P.EMPL_LEGAJO = P_LEGAJO
         AND P.EMPL_EMPRESA = P_EMPRESA;
    END IF;
    IF V_CARGO = 1 AND P_TIPO = 2 THEN
      SELECT DISTINCT PDOC_CLAVE
        INTO V_DOC_OC
        FROM PER_DOCUMENTO,
             PER_DOCUMENTO_DET,
             PER_CONCEPTO,
             PER_EMPLEADO,
             GEN_SUCURSAL,
             GEN_MONEDA
       WHERE PER_DOCUMENTO.PDOC_EMPR = P_EMPRESA

         AND PER_DOCUMENTO.PDOC_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_DOC
         AND PER_DOCUMENTO.PDOC_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR

         AND PER_CONCEPTO.PCON_CLAVE =
             PER_DOCUMENTO_DET.PDDET_CLAVE_CONCEPTO
         AND PER_CONCEPTO.PCON_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR

         AND PER_EMPLEADO.EMPL_LEGAJO = PER_DOCUMENTO.PDOC_EMPLEADO
         AND PER_EMPLEADO.EMPL_EMPRESA = PER_DOCUMENTO.PDOC_EMPR

         AND PER_EMPLEADO.EMPL_SUCURSAL = GEN_SUCURSAL.SUC_CODIGO
         AND PER_EMPLEADO.EMPL_EMPRESA = GEN_SUCURSAL.SUC_EMPR

         AND PDOC_MON = MON_CODIGO
         AND PDOC_EMPR = MON_EMPR

         AND PCON_CANCELADO_POR_CONC IS NULL
         AND PCON_CLAVE in (2, 38) ---SUELDO
         AND EMPL_FORMA_PAGO <> 1

         AND EMPL_SUCURSAL = P_SUCURSAL
         AND PDOC_FEC BETWEEN P_DESDE AND P_HASTA
         AND PDOC_EMPLEADO = P_LEGAJO;

    END IF;

    IF P_TIPO = 1 THEN
      V_REPORTE := 'PERL204A';
    ELSIF P_TIPO = 2 THEN
      V_REPORTE := 'PERL004B_TAGRO';
    ELSIF P_TIPO = 3 THEN
      V_REPORTE := 'PERL204CC';
    END IF;

    IF P_SUCURSAL IS NOT NULL THEN
      V_WHERE   := V_WHERE || ' AND EMPL_SUCURSAL =' || P_SUCURSAL || ' ';
      V_WHERE_2 := V_WHERE_2 || ' AND EMPL_SUCURSAL =' || P_SUCURSAL || ' ';
    END IF;
    IF P_DESDE IS NOT NULL THEN
      V_WHERE := V_WHERE || 'AND  PDOC_FEC BETWEEN ''' ||
                 TO_CHAR(P_DESDE, 'DD/MM/YYYY') || ''' AND ''' ||
                 TO_CHAR(P_HASTA, 'DD/MM/YYYY') || '''';
    END IF;
    IF P_LEGAJO IS NOT NULL THEN
      V_WHERE   := V_WHERE || 'AND PDOC_EMPLEADO =''' || P_LEGAJO || ''' ';
      V_WHERE_2 := V_WHERE_2 || 'AND EMPL_LEGAJO =''' || P_LEGAJO || '''';
    END IF;
    IF P_TIPO = 1 THEN
      --AGUINALDO.
      V_WHERE := V_WHERE || 'AND PCON_CLAVE =3';
    ELSE
      IF P_CONCEPTO IS NOT NULL THEN
        V_WHERE := V_WHERE || 'AND  PCON_CLAVE =''' || P_CONCEPTO || ''' ';
      END IF;
    END IF;

    IF P_FORMA_PAGO IS NOT NULL THEN
      V_WHERE := V_WHERE || ' AND EMPL_FORMA_PAGO =' || P_FORMA_PAGO || ' ';
    END IF;

    IF P_MONEDA IS NOT NULL THEN
      V_WHERE := V_WHERE || ' AND PDOC_MON =' || P_MONEDA || ' ';
    END IF;

    IF P_PERIODO IS NOT NULL THEN

      V_WHERE_2 := V_WHERE_2 || ' AND PERIODO =' || P_PERIODO || ' ';
    END IF;

    IF P_EXCLUIR_LEG IS NOT NULL THEN

      V_WHERE := V_WHERE || ' AND PDOC_EMPLEADO NOT IN (
       (SELECT REGEXP_SUBSTR(REGEXP_REPLACE(''' ||
                 P_EXCLUIR_LEG ||
                 ''', '':'', '',''),
                             ''[^,]+'',
                             1,
                             LEVEL) CTA_BCO
          FROM DUAL
        CONNECT BY REGEXP_SUBSTR(REGEXP_REPLACE(''' ||
                 P_EXCLUIR_LEG ||
                 ''', '':'', '',''),
                                 ''[^,]+'',
                                 1,
                                 LEVEL) IS NOT NULL))';

      V_WHERE_2 := V_WHERE_2 ||
                   'AND EMPL_LEGAJO NOT IN (
       (SELECT REGEXP_SUBSTR(REGEXP_REPLACE(''' ||
                   P_EXCLUIR_LEG ||
                   ''', '':'', '',''),
                             ''[^,]+'',
                             1,
                             LEVEL) CTA_BCO
          FROM DUAL
        CONNECT BY REGEXP_SUBSTR(REGEXP_REPLACE(''' ||
                   P_EXCLUIR_LEG ||
                   ''', '':'', '',''),
                                 ''[^,]+'',
                                 1,
                                 LEVEL) IS NOT NULL))';

    END IF;

    IF P_TIPO <> 2 THEN

      V_SQL := 'SELECT PER_EMPLEADO.EMPL_LEGAJO,
       PER_EMPLEADO.EMPL_NOMBRE || '' '' || PER_EMPLEADO.EMPL_APE NOMBRE,
       ''Dr.J.E.Estigarribia, '' || to_char(to_date(''' ||
               P_HASTA || '''), ''dd'') ||
       '' de '' || ltrim(rtrim(to_char(to_date(''' || P_HASTA ||
               '''), ''month''))) ||
       '' de '' || to_char(to_date(''' || P_HASTA ||
               '''), ''yyyy'') fecha,
       PER_CONCEPTO.PCON_DESC,
       DECODE(FIN_CONCEPTO.FCON_TIPO_SALDO,
              ''C'',
              PER_DOCUMENTO_DET.PDDET_IMP,
              -PER_DOCUMENTO_DET.PDDET_IMP) IMPORTE,
       DECODE(FIN_CONCEPTO.FCON_TIPO_SALDO,
              ''C'',
              PER_DOCUMENTO_DET.PDDET_IMP_LOC,
              -PER_DOCUMENTO_DET.PDDET_IMP_LOC) IMPORTE_LOC,
       PDOC_MON,
       PDDET_CLAVE_FIN,
       EMPL_DOC_IDENT CI,
       PDOC_FEC FECHA_CAB,
       PDOC_CLAVE EMPL_BCO_PAGO,
       ''SUCURSAL ''||SUC_CODIGO||'' : '' SUCU_CODI,
       SUC_DIR ||'' TEL:''|| SUC_TEL SUCURSAL, SUC_IND_CASA_CENTRAL,
        MAX(PDOC_CLAVE) OVER(PARTITION BY EMPL_LEGAJO) PDOC_CLAVE
  FROM PER_DOCUMENTO,
       PER_DOCUMENTO_DET,
       PER_CONCEPTO,
       PER_DPTO_CONC,
       PER_EMPLEADO,
       FIN_CONCEPTO,
       GEN_SUCURSAL
 WHERE PER_DOCUMENTO.PDOC_EMPR = ' || P_EMPRESA || '

   AND FIN_CONCEPTO.FCON_SUC =  SUC_CODIGO
   AND FIN_CONCEPTO.FCON_EMPR = SUC_EMPR

   AND PER_DOCUMENTO.PDOC_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_DOC
   AND PER_DOCUMENTO.PDOC_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR

   AND PER_CONCEPTO.PCON_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_CONCEPTO
   AND PER_CONCEPTO.PCON_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR

   AND PER_CONCEPTO.PCON_CLAVE = PER_DPTO_CONC.DPTOC_PER_CONC
   AND PER_CONCEPTO.PCON_EMPR = PER_DPTO_CONC.DPTOC_EMPR

   AND PER_EMPLEADO.EMPL_LEGAJO = PER_DOCUMENTO.PDOC_EMPLEADO
   AND PER_EMPLEADO.EMPL_EMPRESA = PER_DOCUMENTO.PDOC_EMPR

   AND PER_EMPLEADO.EMPL_DEPARTAMENTO = PER_DPTO_CONC.DPTOC_DPTO
   AND PER_EMPLEADO.EMPL_EMPRESA = PER_DPTO_CONC.DPTOC_EMPR

   AND FIN_CONCEPTO.FCON_CLAVE = PER_DPTO_CONC.DPTOC_FIN_CONC
   AND FIN_CONCEPTO.FCON_EMPR = PER_DPTO_CONC.DPTOC_EMPR
   ' || V_WHERE || '  ';

      OPEN C_PERL204 FOR V_SQL;
      DELETE PERL204_TEMP
       WHERE P_SESSION = P_SESSION
         AND P_USER = P_USUARIO;
      LOOP
        FETCH C_PERL204
          INTO C;
        EXIT WHEN C_PERL204%NOTFOUND;
        INSERT INTO PERL204_TEMP
          (EMPL_LEGAJO,
           NOMBRE,
           FECHA,
           PCON_DESC,
           IMPORTE,
           IMPORTE_LOC,
           PDOC_MON,
           PDDET_CLAVE_FIN,
           EMPL_DOC_IDENT,
           FECHA_CAB,
           PDOC_CLAVE,
           P_SESSION,
           P_USER,
           EMPRESA,
           SUCU_CODI,
           SUCURSAL,
           SUC_IND_CASA_CENTRAL)
        VALUES
          (C.EMPL_LEGAJO,
           C.NOMBRE,
           C.FECHA,
           C.PCON_DESC,
           C.IMPORTE,
           C.IMPORTE_LOC,
           C.PDOC_MON,
           C.PDDET_CLAVE_FIN,
           C.EMPL_DOC_IDENT,
           C.FECHA_CAB,
           C.PDOC_CLAVE,
           P_SESSION,
           P_USUARIO,
           P_EMPRESA,
           C.SUCU_CODI,
           C.SUCURSAL,
           C.SUC_IND_CASA_CENTRAL);
      END LOOP;

    END IF;

    IF P_TIPO = 2 THEN

      V_SQLCAB := 'SELECT
       DISTINCT  NOMBRE,
       EMPL_TEL ,
       EMPL_DOC_IDENT ,
       EMPL_DIR   ,
       SUCURSAL   ,
    CASE   WHEN  EMPL_FORMA_PAGO = 1 THEN
      ''Jornal Hora:  ''||MON_ISO||'' ''||(TO_CHAR((SALARIO),''999G999G999G999''))
    WHEN EMPL_FORMA_PAGO = 2 AND   SALARIO <= 0 THEN
      ''Comisionista: ''||MON_ISO||'' ''||(TO_CHAR((SALARIO),''999G999G999G999''))
    WHEN   EMPL_FORMA_PAGO = 2 AND   SALARIO > 0 AND  SUCURSAL= ''TRANSPORTE'' THEN
       ''Comisionista: ''||MON_ISO||'' ''||(TO_CHAR((SALARIO),''999G999G999G999''))
    WHEN  EMPL_FORMA_PAGO = 4 THEN
     ''Salario: ''||MON_ISO||'' ''||(TO_CHAR((SALARIO),''999G999G999G999''))
    ELSE
      ''Salario Mensual: ''||MON_ISO||'' ''||(TO_CHAR((SALARIO),''999G999G999G999''))
    END SALARIO_DESC  ,
        EMPL_LEGAJO  , BCO_DESC  , EMPL_NRO_CTA    ,
        EMPL_COB_EFECTIVO

        ,SUCU_CODI,SUCURSAL_DES,SUC_IND_CASA_CENTRAL,
        MAX(PDOC_CLAVE) OVER(PARTITION BY EMPL_LEGAJO) PDOC_CLAVE
  FROM (SELECT PER_EMPLEADO.EMPL_LEGAJO,
               PER_EMPLEADO.EMPL_CARGO,
               EMPL_CODIGO_PROV,
               PER_EMPLEADO.EMPL_NOMBRE || '' '' || PER_EMPLEADO.EMPL_APE NOMBRE,
               PER_CONCEPTO.PCON_DESC,
               DECODE(PER_CONCEPTO.PCON_IND_DBCR,
                      ''C'',
                      PER_DOCUMENTO_DET.PDDET_IMP,
                      -PER_DOCUMENTO_DET.PDDET_IMP) IMPORTE,
               DECODE(PER_CONCEPTO.PCON_IND_DBCR,
                      ''C'',
                      PER_DOCUMENTO_DET.PDDET_IMP_LOC,
                      -PER_DOCUMENTO_DET.PDDET_IMP_LOC) IMPORTE_LOC,
               PER_CONCEPTO.PCON_ORDEN,
               PDOC_MON,
               PDDET_CLAVE_FIN,
               PDOC_CLAVE,
               DECODE(EMPL_FORMA_PAGO,
                      1,
                      NVL(EMPL_IMP_HORA_N_D, 0),
                      NVL(EMPL_SALARIO_BASE, 0)) SALARIO,
               DECODE(PDDET_CLAVE_CONCEPTO, 12, NVL(PDDET_IMP_LOC, 0), 0) COMISION,
               DECODE(PDDET_CLAVE_CONCEPTO, 8, NVL(PDDET_IMP_LOC, 0), 0) BONIFICACION,
               MON_ISO,
               DECODE(PDDET_CLAVE_CONCEPTO,
                      31,
                      PDDET_IMP,
                      DECODE(PDDET_CLAVE_CONCEPTO, 4, PDDET_IMP, 0)) IPS_AMH,
               DECODE(PDDET_CLAVE_CONCEPTO, 16, PDDET_IMP_LOC, 0) ANTICIPOS,
               DECODE(PDDET_CLAVE_CONCEPTO, 18, PDDET_IMP_LOC, 0) PRESTAMOS,
               DECODE(PDDET_CLAVE_CONCEPTO, 39, PDDET_IMP_LOC, 0) AUCENCIAS,
               DECODE(PDDET_CLAVE_CONCEPTO,17,NVL(PDDET_IMP_LOC, 0), 0) ANTICIPOS_POR_APORTE,
               EMPL_FORMA_PAGO,
               PDDET_CLAVE_CONCEPTO,
               EMPL_NRO_CTA,
               EMPL_BCO_PAGO,
               EMPL_TEL,
               EMPL_DOC_IDENT,
               EMPL_DIR,
               SUC_DESC SUCURSAL,
               EMPL_COB_EFECTIVO COBRO_EFECTIVO,
               TO_CHAR(COF_PORC_SUELDO,''990D99'')||''%'' PORC_SUELDO,
               COF_CODIGO CODIGO_CHOFER1   ,
               PER_DOCUMENTO.PDOC_FEC FECHA  ,
               B.BCO_DESC   ,EMPL_COB_EFECTIVO,
               ''SUCURSAL ''||SUC_CODIGO||'' : '' SUCU_CODI,
               SUC_DIR ||'' TEL:''|| SUC_TEL SUCURSAL_DES, SUC_IND_CASA_CENTRAL
          FROM PER_DOCUMENTO,
               PER_DOCUMENTO_DET,
               PER_CONCEPTO,
               PER_EMPLEADO,
               GEN_SUCURSAL,
               GEN_MONEDA,
               TRA_CHOFER  ,
               FIN_BANCO   B
         WHERE PER_DOCUMENTO.PDOC_EMPR = 2
           AND PER_DOCUMENTO.PDOC_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_DOC
           AND PER_DOCUMENTO.PDOC_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR
           AND PER_CONCEPTO.PCON_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_CONCEPTO
           AND PER_CONCEPTO.PCON_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR
           AND PER_EMPLEADO.EMPL_LEGAJO = PER_DOCUMENTO.PDOC_EMPLEADO
           AND PER_EMPLEADO.EMPL_EMPRESA = PER_DOCUMENTO.PDOC_EMPR
           AND PER_EMPLEADO.EMPL_SUCURSAL = GEN_SUCURSAL.SUC_CODIGO
           AND PER_EMPLEADO.EMPL_EMPRESA = GEN_SUCURSAL.SUC_EMPR
           AND PDOC_MON = MON_CODIGO
           AND PDOC_EMPR = MON_EMPR
           AND EMPL_CODIGO_CHOFER = COF_CODIGO(+)
           AND EMPL_EMPRESA = COF_EMPR (+)
           AND EMPL_BCO_PAGO  =  B.BCO_CODIGO
           AND EMPL_EMPRESA  =  B.BCO_EMPR
          ' || V_WHERE || '
           AND PCON_CANCELADO_POR_CONC IS NULL  )';

      INSERT INTO X (CAMPO1, Otro) VALUES (V_SQLCAB, 'PERL004CAB');
      COMMIT;

      V_SQL := '
  SELECT PER_EMPLEADO.EMPL_LEGAJO,    ''Salario''  CONCEPTO   ,   NULL MONTO  ,  SUM(PDDET_IMP_LOC) monto   ,   ''S'' TIPO
  FROM PER_DOCUMENTO,
       PER_DOCUMENTO_DET,
       PER_CONCEPTO,
       PER_EMPLEADO,
       GEN_SUCURSAL,
       GEN_MONEDA
 WHERE PER_DOCUMENTO.PDOC_EMPR = 2

   AND PER_DOCUMENTO.PDOC_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_DOC
   AND PER_DOCUMENTO.PDOC_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR

   AND PER_CONCEPTO.PCON_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_CONCEPTO
   AND PER_CONCEPTO.PCON_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR

   AND PER_EMPLEADO.EMPL_LEGAJO = PER_DOCUMENTO.PDOC_EMPLEADO
   AND PER_EMPLEADO.EMPL_EMPRESA = PER_DOCUMENTO.PDOC_EMPR

   AND PER_EMPLEADO.EMPL_SUCURSAL = GEN_SUCURSAL.SUC_CODIGO
   AND PER_EMPLEADO.EMPL_EMPRESA = GEN_SUCURSAL.SUC_EMPR
   AND PDOC_MON = MON_CODIGO
   AND PDOC_EMPR = MON_EMPR
   AND PCON_CANCELADO_POR_CONC IS NULL
   AND PCON_CLAVE in( 2, 38) ---SUELDO
   AND EMPL_FORMA_PAGO <> 1
 ' || V_WHERE || '
 GROUP BY PER_EMPLEADO.EMPL_LEGAJO
  UNION ALL
SELECT PDOC_EMPLEADO , ''Vacaciones'', NULL, NVL(V.IMPORTE_LOC, 0)  MONTO1  ,  ''I'' TIPO
  FROM PERL204_CONCEPTOS_V V

 WHERE 1=1  ' || V_WHERE || '
  AND V.PDDET_CLAVE_CONCEPTO = 6
  UNION ALL
  SELECT  EMPL_LEGAJO , ''Hs Diurnas'' CONCEPTO, T.HD MONTO, TOTIMPHD  MONTO1  ,  ''I'' TIPO
  FROM PER_PERL034_V_ACT T
 WHERE 1=1  ' || V_WHERE_2 || '
UNION ALL
SELECT EMPL_LEGAJO , ''Hs Nocturnas 30%'' CONCEPTO, T.HN MONTO, TOTIMPHN MONTO1  ,  ''I'' TIPO
  FROM PER_PERL034_V_ACT T
 WHERE 1=1  ' || V_WHERE_2 || '
UNION ALL
SELECT  EMPL_LEGAJO , ''Hs D Extras 50%'' CONCEPTO, T.HDE MONTO, TOTIMPHDE MONTO1  ,  ''I'' TIPO
  FROM PER_PERL034_V_ACT T
 WHERE 1=1  ' || V_WHERE_2 || '
UNION ALL
SELECT EMPL_LEGAJO , ''Hs N Extras 100%'' CONCEPTO, T.HNE MONTO, TOTIMPHNE MONTO1  ,  ''I'' TIPO
  FROM PER_PERL034_V_ACT T
 WHERE 1=1  ' || V_WHERE_2 || '
UNION ALL
SELECT EMPL_LEGAJO , ''Hs D Dom\Fer 100%'' CONCEPTO, T.HFD MONTO, TOTIMPHFD MONTO1  ,  ''I'' TIPO
  FROM PER_PERL034_V_ACT T
 WHERE 1=1  ' || V_WHERE_2 || '
UNION ALL
SELECT  EMPL_LEGAJO , ''Hs N Dom\Fer 100%'' CONCEPTO, T.HFN MONTO, TOTIMPHFN MONTO1  ,  ''I'' TIPO
  FROM PER_PERL034_V_ACT T
 WHERE 1=1  ' || V_WHERE_2 || '
UNION ALL
SELECT EMPL_LEGAJO , ''Hs Mixta Extra'' CONCEPTO, T.HME MONTO, TOTIMPHME MONTO1  ,  ''I'' TIPO
  FROM PER_PERL034_V_ACT T
 WHERE 1=1  ' || V_WHERE_2 || '
UNION ALL
SELECT PDOC_EMPLEADO , ''Comision'', NULL, NVL(V.COMISION, 0)  MONTO1  ,  ''I'' TIPO
  FROM PERL204_CONCEPTOS_V V

 WHERE 1=1  ' || V_WHERE || '
  AND V.PDDET_CLAVE_CONCEPTO = 12
 
  
UNION ALL
SELECT PDOC_EMPLEADO , ''Bonificacion Familiar'', NULL, NVL(V.BONIFICACION, 0)  MONTO1  ,  ''I'' TIPO
  FROM PERL204_CONCEPTOS_V V
 WHERE 1=1  ' || V_WHERE || '
   AND V.PDDET_CLAVE_CONCEPTO = 8
   ---- otros ingresos
  union all
      SELECT
      PDOC_EMPLEADO , INITCAP(PCON_DESC) CONCEPTO ,
       null  MONTO1 ,
       PDDET_IMP_LOC MONTO1  ,  ''I'' TIPO
  FROM PER_DOCUMENTO,
       PER_DOCUMENTO_DET,
       PER_CONCEPTO,
       PER_EMPLEADO,
       GEN_SUCURSAL,
       GEN_MONEDA
 WHERE PER_DOCUMENTO.PDOC_EMPR = 2
   AND PER_DOCUMENTO.PDOC_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_DOC
   AND PER_DOCUMENTO.PDOC_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR
   AND PER_CONCEPTO.PCON_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_CONCEPTO
   AND PER_CONCEPTO.PCON_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR
   AND PER_EMPLEADO.EMPL_LEGAJO = PER_DOCUMENTO.PDOC_EMPLEADO
   AND PER_EMPLEADO.EMPL_EMPRESA = PER_DOCUMENTO.PDOC_EMPR
   AND PER_EMPLEADO.EMPL_SUCURSAL = GEN_SUCURSAL.SUC_CODIGO
   AND PER_EMPLEADO.EMPL_EMPRESA = GEN_SUCURSAL.SUC_EMPR
   AND PDOC_MON = MON_CODIGO
   AND PDOC_EMPR = MON_EMPR
   AND PCON_CANCELADO_POR_CONC IS NULL
   AND PCON_IE_RECIBO = ''I''
 ' || V_WHERE || '
  UNION ALL
SELECT PDOC_EMPLEADO ,''IPS'', NULL, NVL(V.IPS_AMH, 0)  MONTO1  ,  ''E'' TIPO
  FROM PERL204_CONCEPTOS_V V
 WHERE 1=1  ' || V_WHERE || '
  AND PDDET_CLAVE_CONCEPTO  =  4
   --AND V.PDDET_CLAVE_CONCEPTO = 8
  UNION ALL
SELECT PDOC_EMPLEADO , ''Anticipos'', NULL, NVL(V.Anticipos, 0) MONTO1  ,  ''E'' TIPO
  FROM PERL204_CONCEPTOS_V V
 WHERE 1=1  ' || V_WHERE || '
  AND PDDET_CLAVE_CONCEPTO  =  16
    UNION ALL
SELECT PDOC_EMPLEADO , ''Anticipos Por Aportes'', NULL, NVL(v.Anticipos_por_aporte, 0)  MONTO1  ,  ''E'' TIPO
  FROM PERL204_CONCEPTOS_V V
 WHERE 1=1  ' || V_WHERE || '
  AND PDDET_CLAVE_CONCEPTO  =  17
     UNION ALL
SELECT PDOC_EMPLEADO , ''Prestamos'', NULL, NVL(v.Prestamos, 0)  MONTO1  ,  ''E'' TIPO
  FROM PERL204_CONCEPTOS_V V
 WHERE 1=1  ' || V_WHERE || '
  AND PDDET_CLAVE_CONCEPTO  =  18
       UNION ALL
SELECT PDOC_EMPLEADO ,''Ausencias'', NULL, NVL(V.PRESTAMOS, 0)  MONTO1  ,  ''E'' TIPO
  FROM PERL204_CONCEPTOS_V V
 WHERE 1=1  ' || V_WHERE || '
  AND PDDET_CLAVE_CONCEPTO  =  39
  /**** otro egresos ***/
union all
 SELECT EMPL_LEGAJO ,
       V.PCON_DESC CONCEPTO,
       null ,
       SUM(V.IMPORTE_LOC) MONTO1  ,  ''E'' TIPO
  FROM (
        SELECT PER_EMPLEADO.EMPL_LEGAJO EMPL_LEGAJO,
                PER_EMPLEADO.EMPL_NOMBRE || '' '' || PER_EMPLEADO.EMPL_APE NOMBRE,
                INITCAP(''OTROS DESCUENTOS'') PCON_DESC,
                PDDET_IMP IMPORTE,
                PDDET_IMP_LOC IMPORTE_LOC,
                PDOC_MON,
                EMPL_FORMA_PAGO,
                32 PDDET_CLAVE_CONCEPTO,
                PDOC_CLAVE
          FROM PER_DOCUMENTO,
                PER_DOCUMENTO_DET,
                PER_CONCEPTO,
                PER_EMPLEADO,
                GEN_SUCURSAL,
                GEN_MONEDA
         WHERE PER_DOCUMENTO.PDOC_EMPR = 2

           AND PER_DOCUMENTO.PDOC_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_DOC
           AND PER_DOCUMENTO.PDOC_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR

           AND PER_CONCEPTO.PCON_CLAVE =
               PER_DOCUMENTO_DET.PDDET_CLAVE_CONCEPTO
           AND PER_CONCEPTO.PCON_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR

           AND PER_EMPLEADO.EMPL_LEGAJO = PER_DOCUMENTO.PDOC_EMPLEADO
           AND PER_EMPLEADO.EMPL_EMPRESA = PER_DOCUMENTO.PDOC_EMPR

           AND PER_EMPLEADO.EMPL_SUCURSAL = GEN_SUCURSAL.SUC_CODIGO
           AND PER_EMPLEADO.EMPL_EMPRESA = GEN_SUCURSAL.SUC_EMPR

           AND PDOC_MON = MON_CODIGO
           AND PDOC_EMPR = MON_EMPR

           AND PCON_CANCELADO_POR_CONC IS NULL
   ' || V_WHERE || '
           AND PDDET_CLAVE_CONCEPTO IN (34, 32)
        UNION
        SELECT PER_EMPLEADO.EMPL_LEGAJO EMPL_LEGAJO,
                PER_EMPLEADO.EMPL_NOMBRE || '' '' || PER_EMPLEADO.EMPL_APE NOMBRE,
                INITCAP(PCON_DESC) PCON_DESC,
                PDDET_IMP IMPORTE,
                PDDET_IMP_LOC IMPORTE_LOC,
                PDOC_MON,
                EMPL_FORMA_PAGO,
                PDDET_CLAVE_CONCEPTO,
                PDOC_CLAVE
          FROM PER_DOCUMENTO,
                PER_DOCUMENTO_DET,
                PER_CONCEPTO,
                PER_EMPLEADO,
                GEN_SUCURSAL,
                GEN_MONEDA
         WHERE PER_DOCUMENTO.PDOC_EMPR = 2

           AND PER_DOCUMENTO.PDOC_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_DOC
           AND PER_DOCUMENTO.PDOC_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR

           AND PER_CONCEPTO.PCON_CLAVE =
               PER_DOCUMENTO_DET.PDDET_CLAVE_CONCEPTO
           AND PER_CONCEPTO.PCON_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR

           AND PER_EMPLEADO.EMPL_LEGAJO = PER_DOCUMENTO.PDOC_EMPLEADO
           AND PER_EMPLEADO.EMPL_EMPRESA = PER_DOCUMENTO.PDOC_EMPR

           AND PER_EMPLEADO.EMPL_SUCURSAL = GEN_SUCURSAL.SUC_CODIGO
           AND PER_EMPLEADO.EMPL_EMPRESA = GEN_SUCURSAL.SUC_EMPR

           AND PDOC_MON = MON_CODIGO
           AND PDOC_EMPR = MON_EMPR

           AND PCON_CANCELADO_POR_CONC IS NULL
           AND PCON_IE_RECIBO = ''E''
           AND PDDET_CLAVE_CONCEPTO NOT IN (34, 32)
     ' || V_WHERE || '

        ) V
 GROUP BY V.EMPL_LEGAJO,
          V.NOMBRE,
          V.PCON_DESC,
          V.PDOC_MON,
          V.EMPL_FORMA_PAGO,
          V.PDDET_CLAVE_CONCEPTO';

      INSERT INTO X (CAMPO1, Otro) VALUES (V_SQL, 'PERL004A');
      COMMIT;

      OPEN C_PERL204A FOR V_SQL;
      DELETE PERL004A_TEMP
       WHERE p_session = P_SESSION
         AND P_USUARIO = P_USUARIO;
      LOOP
        FETCH C_PERL204A
          INTO A;
        EXIT WHEN C_PERL204A%NOTFOUND;
        INSERT INTO PERL004A_TEMP
          (P_CONCEPTO,
           MONTO,
           MONTO2,
           TIPO,
           P_SESSION,
           P_USUARIO,
           P_EMPRESA,
           LEGAJO)
        VALUES
          (A.P_CONCEPTO,
           A.MONTO,
           A.MONTO2,
           A.TIPO,
           P_SESSION,
           P_USUARIO,
           P_EMPRESA,
           A.LEGAJO);
      END LOOP;
      OPEN C_PERL204CAB FOR V_SQLCAB;
      DELETE PERL004_CAB_TEMP
       WHERE p_session = P_SESSION
         AND P_USUARIO = P_USUARIO;
      LOOP
        FETCH C_PERL204CAB
          INTO CA;
        EXIT WHEN C_PERL204CAB%NOTFOUND;
        INSERT INTO PERL004_CAB_TEMP
          (NOMBRE,
           EMPL_TEL,
           EMPL_DOC_IDENT,
           EMPL_DIR,
           SUCURSAL,
           SALARIO_DESC,
           EMPL_LEGAJO,
           P_SESSION,
           P_USUARIO,
           P_EMPRESA,
           BANCO_DES,
           EMPL_NRO_CTA,
           EMPL_COB_EFECTIVO,
           SUCU_CODI,
           SUCURSAL_DES,
           SUC_IND_CASA_CENTRAL,
           PDOC_CLAVE)
        VALUES
          (CA.NOMBRE,
           CA.EMPL_TEL,
           CA.EMPL_DOC_IDENT,
           CA.EMPL_DIR,
           CA.SUCURSAL,
           CA.SALARIO_DESC,
           CA.EMPL_LEGAJO,
           P_SESSION,
           P_USUARIO,
           P_EMPRESA,
           CA.BANCO_DES,
           CA.EMPL_NRO_CTA,
           CA.EMPL_COB_EFECTIVO,
           CA.SUCU_CODI,
           CA.SUCURSAL_DES,
           CA.SUC_IND_CASA_CENTRAL,
           CA.PDOC_CLAVE);
      END LOOP;

    END IF;
    BEGIN
      -- CALL THE PROCEDURE
      PERL004.PP_LLAMAR_REPORTE_TAGRO(P_EMPRESA      => P_EMPRESA,
                                      P_SESSION      => P_SESSION,
                                      P_USUARIO      => P_USUARIO,
                                      P_TIPO_REPORTE => V_REPORTE,
                                      P_FECHA_DESDE  => P_DESDE,
                                      P_FECHA_HASTA  => P_HASTA);
    END;

  END;
end PERL004;
/
