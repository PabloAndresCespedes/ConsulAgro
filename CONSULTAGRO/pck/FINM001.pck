CREATE OR REPLACE PACKAGE FINM001 AS

  /* TODO ENTER PACKAGE DECLARATIONS (TYPES, EXCEPTIONS, METHODS ETC) HERE */
  PROCEDURE PP_VERIFICA_RUCC(P_CLI_RUC_DV IN VARCHAR2, --LIZV
                             P_EMPRESA    IN NUMBER,
                             P_CLI_CODIGO IN NUMBER,
                             O_MENSAJE    OUT VARCHAR2);

  PROCEDURE PP_VERIFICA_LISTA(P_EMPRESA    IN NUMBER,
                              P_CLI_RUC_DV IN VARCHAR2); --LV

  PROCEDURE PP_CARGAR_HOLDING_EXIST(P_CLI_COD_FICHA_HOLDING OUT NUMBER,
                                    P_RUC                   IN VARCHAR2,
                                    P_EMPRESA               IN NUMBER);

  FUNCTION FP_GENERA_HOLDING(P_EMPRESA NUMBER, P_CLI_NOM VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PP_GUARDAR_CLIENTE(I_CLI     IN FIN_CLIENTE%ROWTYPE,
                               I_OPER    IN VARCHAR2,
                               I_EMPRESA IN NUMBER,
                               I_USUARIO IN VARCHAR2);

  FUNCTION FP_BUSCAR_CIUDAD(I_CIUDAD IN VARCHAR2) RETURN NUMBER;

  FUNCTION FP_BUSCAR_BARRIO(I_BARRIO IN VARCHAR2,
                            I_DPTO   IN NUMBER,
                            I_CIUDAD IN VARCHAR2) RETURN NUMBER;

  FUNCTION FP_GET_CLI_COD RETURN NUMBER;

  FUNCTION FP_GET_CLI_COORDENADA(I_CLI_CODIGO IN NUMBER) RETURN VARCHAR2;

  PROCEDURE PP_ACT_CLI_BARR(I_DPTO    IN VARCHAR2,
                            I_BARRIO  IN VARCHAR2,
                            I_CIUDAD  IN VARCHAR2,
                            I_CLIENTE IN NUMBER,
                            I_CLI_DIR IN VARCHAR2,
                            I_LAT     IN VARCHAR2,
                            I_LNG     IN VARCHAR2);

  FUNCTION FP_GRADO(I_NUM IN NUMBER) RETURN NUMBER;

  FUNCTION FP_MINUTOS(I_NUM IN NUMBER) RETURN NUMBER;

  FUNCTION FP_SEGUNDO(I_NUM IN NUMBER) RETURN NUMBER;

  FUNCTION FP_VISITA_SEMANA(I_DIA       IN VARCHAR2,
                            I_QUINCENAL IN VARCHAR2,
                            I_PAR       IN VARCHAR2) RETURN VARCHAR2;

  FUNCTION FP_CLI_DIA_VISITA(I_LUNES     IN VARCHAR2,
                             I_MARTES    IN VARCHAR2,
                             I_MIERCOLES IN VARCHAR2,
                             I_JUEVES    IN VARCHAR2,
                             I_VIERNES   IN VARCHAR2,
                             I_SABADO    IN VARCHAR2) RETURN VARCHAR2;

                               FUNCTION FP_CLI_FRECUENCIA_VISITA(I_SEMANAL     IN VARCHAR2,
                                    I_QUINCENAL   IN VARCHAR2,
                                    I_TIPO_SEMANA IN VARCHAR2)
    RETURN VARCHAR2;
END FINM001;
/
CREATE OR REPLACE PACKAGE BODY FINM001 AS

  PROCEDURE PP_VERIFICA_RUCC(P_CLI_RUC_DV IN VARCHAR2, ---LIZV
                             P_EMPRESA    IN NUMBER,
                             P_CLI_CODIGO IN NUMBER,
                             O_MENSAJE    OUT VARCHAR2) AS
    V_COD VARCHAR2(32000);

    CURSOR C IS
      SELECT CLI_CODIGO
        FROM FIN_CLIENTE
       WHERE LTRIM(RTRIM(CLI_RUC_DV)) = LTRIM(RTRIM(P_CLI_RUC_DV))
         AND LTRIM(RTRIM(CLI_RUC)) NOT IN
             ('.', '.-.', 'CI', 'CI:', '0', '0-0', '1', '1-.', '44444401-7')
            --AND CLI_CODIGO <> P_CLI_CODIGO
         AND CLI_EMPR = P_EMPRESA;

  BEGIN

    FOR V IN C LOOP
      V_COD := TO_CHAR(V_COD) || ';' || TO_CHAR(V.CLI_CODIGO);
    END LOOP;

    IF V_COD IS NOT NULL THEN
      O_MENSAJE := SUBSTR(V_COD, 2); --V_COD;
      /* RAISE_APPLICATION_ERROR(-20000,
      'El RUC ya existe para el/los cliente/s: ' ||
      SUBSTR(V_COD, 2));*/
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      --ANTES NO FUNCIONABA LA VERIFICACION, AL PONER EN EL WHEN OTHERS, SE DISPARA BIEN LA NOTIFICACION... 14/08/2019
      RAISE_APPLICATION_ERROR(-20001,
                              'El RUC ya existe para el/los cliente/s: ' ||
                              SUBSTR(V_COD, 2));
  END PP_VERIFICA_RUCC;

  PROCEDURE PP_VERIFICA_LISTA(P_EMPRESA    IN NUMBER,
                              P_CLI_RUC_DV IN VARCHAR2) AS
    ---LV
    COMP CHAR(1);
  BEGIN
    SELECT 'X'
      INTO COMP
      FROM FIN_CLI_LIST_NEGRA
     WHERE LTRIM(RTRIM(CLISNE_CI_RUC)) = LTRIM(RTRIM(P_CLI_RUC_DV))
        OR LTRIM(RTRIM(CLISNE_CI)) = LTRIM(RTRIM(P_CLI_RUC_DV))
       AND CLISNE_EMPR = P_EMPRESA;

    IF COMP = 'X' THEN
      RAISE_APPLICATION_ERROR(-20001,
                              'El Ruc ingresado coincide con uno de los que se encuentra en la lista negra');
    ELSE
      NULL;
    END IF;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
    WHEN TOO_MANY_ROWS THEN
      RAISE_APPLICATION_ERROR(-20001,
                              'El Ruc ingresado coincide con varios de los que se encuentran en la lista negra');
  END PP_VERIFICA_LISTA;

  PROCEDURE PP_CARGAR_HOLDING_EXIST(P_CLI_COD_FICHA_HOLDING OUT NUMBER,
                                    P_RUC                   IN VARCHAR2,
                                    P_EMPRESA               IN NUMBER) IS
  BEGIN
    SELECT H.HOL_CODIGO
      INTO P_CLI_COD_FICHA_HOLDING
      FROM FIN_FICHA_HOLDING H, FIN_CLIENTE C
     WHERE C.CLI_EMPR = P_EMPRESA
       AND C.CLI_EMPR = H.HOL_EMPR
       AND C.CLI_COD_FICHA_HOLDING = H.HOL_CODIGO
       AND C.CLI_RUC NOT IN ('44444401', '77777701', '88888801')
       AND C.CLI_RUC = P_RUC
     GROUP BY H.HOL_CODIGO, H.HOL_DESC;
  EXCEPTION
    WHEN TOO_MANY_ROWS THEN
      RAISE_APPLICATION_ERROR(-20001,
                              'Varios holding utilizan el mismo RUC');
    WHEN NO_DATA_FOUND THEN
      NULL;
  END;

  FUNCTION FP_GENERA_HOLDING(P_EMPRESA NUMBER, P_CLI_NOM VARCHAR2)
    RETURN NUMBER IS
    V_NRO NUMBER;
  BEGIN
    /* Debe hacer select de la secuencia, para que no se pierda la correlatividad. 12/05/2021
    SELECT NVL(MAX(HOL_CODIGO), 0) + 1
      INTO V_NRO
      FROM FIN_FICHA_HOLDING
     WHERE HOL_EMPR = P_EMPRESA;
     */
    V_NRO := SEQ_FIN_FHOLDING.NEXTVAL;

    INSERT INTO FIN_FICHA_HOLDING
      (HOL_CODIGO,
       HOL_EMPR,
       HOL_DESC,
       HOL_LIMITE_CRED,
       HOL_LIMITE_CRED_UTILIZADO,
       HOL_OBS_FACT,
       HOL_BLOQ_LIMI_CR,
       HOL_PREST_IMP_LIM_CR,
       HOL_MON,
       HOL_PREST_SALDO_ACT_TENT_MON,
       HOL_PREST_SALDO_ACT_TENT_LOC,
       HOL_LOGIN,
       HOL_FEC_GRAB_LIM_CR,
       HOL_FEC_ACT_LIM)
    VALUES
      (V_NRO, P_EMPRESA, P_CLI_NOM, 0, 0, 'EFECTIVO', 'N', 0.0000, 1, 0, 0,gen_devuelve_user,SYSDATE,SYSDATE);

    RETURN V_NRO;
  END FP_GENERA_HOLDING;

  PROCEDURE PP_GUARDAR_CLIENTE(I_CLI     IN FIN_CLIENTE%ROWTYPE,
                               I_OPER    IN VARCHAR2,
                               I_EMPRESA IN NUMBER,
                               I_USUARIO IN VARCHAR2) AS
  BEGIN
    IF I_OPER = 'INSERT' THEN
      INSERT INTO FIN_CLIENTE
        (CLI_CODIGO,
         CLI_NOM,
         CLI_PROPIETARIO,
         CLI_NOM_FANTASIA,
         CLI_DIR,
         CLI_DOC_IDENT_PROPIETARIO,
         CLI_LOCALIDAD,
         CLI_FEC_NAC_PROPIETARIO,
         CLI_TEL,
         CLI_FAX,
         CLI_RUC,
         CLI_RAMO,
         CLI_PERSONERIA,
         CLI_CATEG,
         CLI_ZONA,
         CLI_PAIS,
         CLI_FEC_ANIV,
         CLI_PERS_CONTACTO,
         CLI_FORMA_ENVIO,
         CLI_OBS,
         CLI_PORC_EXEN_IVA,
         CLI_DOC_IDENT_CONTACTO,
         CLI_EST_CLI,
         CLI_FEC_NAC_CONTACTO,
         CLI_MON,
         CLI_IMP_LIM_CR,
         CLI_BLOQ_LIM_CR,
         CLI_MAX_DIAS_ATRASO,
         CLI_PERS_REPRESENTANTE,
         CLI_FEC_INGRESO,
         CLI_FEC_ACTUALIZACION,
         CLI_IND_POTENCIAL,
         CLI_DOC_IDENT_REPRESENTANTE,
         CLI_FEC_NAC_REPRESENTANTE,
         CLI_LUGAR_ORIGEN_REPLICA,
         CLI_NRO_FINCA,
         CLI_NOM_CONYUGUE,
         CLI_DOC_IDENT_CONYUGUE,
         CLI_DIR_PARTICULAR,
         CLI_LUGAR_TRABAJO,
         CLI_TEL_PARTICULAR,
         CLI_BARRIO,
         CLI_PROFESION,
         CLI_EMAIL,
         CLI_NRO_FICHA,
         CLI_PROYECTO,
         CLI_SEXO,
         CLI_TEL_CONTACTO,
         CLI_ESTADO_CIVIL,
         CLI_SALARIO,
         CLI_FEC_NAC_CONYUGUE,
         CLI_DIADOM,
         CLI_DIALUN,
         CLI_DIAMAR,
         CLI_DIAMIE,
         CLI_DIAJUE,
         CLI_DIAVIE,
         CLI_DIASAB,
         CLI_PEDIDO_REPETITIVO,
         CLI_REC_LPREC,
         CLI_DV,
         CLI_RUC_DV,
         CLI_CIUDAD,
         CLI_DIA_LUNES,
         CLI_DIA_MARTES,
         CLI_DIA_MIERCOLES,
         CLI_DIA_JUEVES,
         CLI_DIA_VIERNES,
         CLI_DIA_SABADO,
         CLI_VENDEDOR,
         CLI_ORDEN_LUN,
         CLI_ORDEN_MAR,
         CLI_ORDEN_MIER,
         CLI_ORDEN_JUE,
         CLI_ORDEN_VIER,
         CLI_ORDEN_SAB,
         CLI_COD_BARRIO,
         CLI_REPOSITOR,
         CLI_IND_PAGO_CON_CHEQUE,
         CLI_MAX_CANT_CUOTA_CONT,
         CLI_COD_PROVEEDOR,
         CLI_COMI_ASO,
         CLI_APELLIDO,
         CLI_NOMBRE,
         CLI_DIR_ESPECIFICA,
         CLI_ASOCIACION,
         CLI_DIR_FOTO,
         CLI_INFORMCONF,
         CLI_FEC_INGRESO_CLI,
         CLI_SECTOR_PROD,
         CLI_PERSONA,
         CLI_CANAL,
         CLI_IND_EXHIBIDOR,
         CLI_IND_MOD_CANAL,
         CLI_OBS_FACT,
         CLI_SUCURSAL,
         CLI_COBRADOR,
         CLI_COD_PROSPECTO,
         CLI_IND_SILO,
         CLI_COD_FICHA_HOLDING,
         CLI_CHORTI_DEB_AUTO,
         CLI_CHORTI_NRO_CTA,
         CLI_EXP_DIR,
         CLI_EXP_RUC,
         CLI_IND_TACHO,
         CLI_TEL_SMS,
         CLI_FREC_VISITA_SEMANAL,
         CLI_FREC_VISITA_QUINCENAL,
         CLI_LATITUD,
         CLI_LONGITUD,
         CLI_SUP_COBRANZA,
         CLI_RUC_HOLDING,
         CLI_EMPR,
         CLI_RESUMEN_FACT,
         CLI_ACA_COL_ORIG,
         CLI_HOLDING,
         CLI_ACA_DIR_LAB,
         CLI_ACA_TEL_LAB,
         CLI_ACA_NOM_FAC,
         CLI_ACA_DIR_FAC,
         CLI_ACA_CIP_FAC,
         CLI_ACA_RUC_FAC,
         CLI_ACA_NIVEL,
         CLI_ACA_GRADO,
         CLI_ACA_TURNO,
         CLI_ACA_SECCION,
         CLI_ACA_APLAZADO,
         CLI_ACA_CLI_TIPO,
         CLI_ACA_INACTIVO,
         CLI_ACA_IMG_BLOB,
         CLI_ACA_IMG_LR,
         CLI_COND_PAGO,
         CLI_FEC_ENTR_EXHIBIDOR,
         CLI_COD_EMPL_EMPR_ORIG,
         CLI_CODIGO_ANTERIOR,
         CLI_CTRL_PRECIO_AQ,
         CLI_FACT_COMB_GS,
         CLI_IND_EXIGIR_RESP_FCRED,
         CLI_PORC_FLETE,
         CLI_SEGMENTO,
         CLI_VALOR_AGREGADO,
         CLI_SEGMENTO_MACRO,
         CLI_SEGMENTO_MICRO,
         CLI_AUTO_LNEGRA,
         CLI_FREC_VISITA_SEM_PAR_IMPAR,
         CLI_IND_BLOQUEO_VTA_PERDIDA,
         CLI_SEGMENTO_SUC_CANAL,
         CLI_CANAL_BETA,
         CLI_HR_AM_RECEP_DESDE,
         CLI_HR_AM_RECEP_HASTA,
         CLI_HR_PM_RECEP_DESDE,
         CLI_HR_PM_RECEP_HASTA,
         CLI_DOC_TIPO,
         CLI_EMPRESA_FUNCIONARIO
         )
      VALUES
        (I_CLI.CLI_CODIGO,
         I_CLI.CLI_NOM,
         I_CLI.CLI_PROPIETARIO,
         I_CLI.CLI_NOM_FANTASIA,
         I_CLI.CLI_DIR,
         I_CLI.CLI_DOC_IDENT_PROPIETARIO,
         I_CLI.CLI_LOCALIDAD,
         I_CLI.CLI_FEC_NAC_PROPIETARIO,
         I_CLI.CLI_TEL,
         I_CLI.CLI_FAX,
         I_CLI.CLI_RUC,
         I_CLI.CLI_RAMO,
         I_CLI.CLI_PERSONERIA,
         I_CLI.CLI_CATEG,
         I_CLI.CLI_ZONA,
         I_CLI.CLI_PAIS,
         I_CLI.CLI_FEC_ANIV,
         I_CLI.CLI_PERS_CONTACTO,
         I_CLI.CLI_FORMA_ENVIO,
         I_CLI.CLI_OBS,
         I_CLI.CLI_PORC_EXEN_IVA,
         I_CLI.CLI_DOC_IDENT_CONTACTO,
         I_CLI.CLI_EST_CLI,
         I_CLI.CLI_FEC_NAC_CONTACTO,
         I_CLI.CLI_MON,
         I_CLI.CLI_IMP_LIM_CR,
         I_CLI.CLI_BLOQ_LIM_CR,
         I_CLI.CLI_MAX_DIAS_ATRASO,
         I_CLI.CLI_PERS_REPRESENTANTE,
         I_CLI.CLI_FEC_INGRESO,
         I_CLI.CLI_FEC_ACTUALIZACION,
         I_CLI.CLI_IND_POTENCIAL,
         I_CLI.CLI_DOC_IDENT_REPRESENTANTE,
         I_CLI.CLI_FEC_NAC_REPRESENTANTE,
         I_CLI.CLI_LUGAR_ORIGEN_REPLICA,
         I_CLI.CLI_NRO_FINCA,
         I_CLI.CLI_NOM_CONYUGUE,
         I_CLI.CLI_DOC_IDENT_CONYUGUE,
         I_CLI.CLI_DIR_PARTICULAR,
         I_CLI.CLI_LUGAR_TRABAJO,
         I_CLI.CLI_TEL_PARTICULAR,
         I_CLI.CLI_BARRIO,
         I_CLI.CLI_PROFESION,
         I_CLI.CLI_EMAIL,
         I_CLI.CLI_NRO_FICHA,
         I_CLI.CLI_PROYECTO,
         I_CLI.CLI_SEXO,
         I_CLI.CLI_TEL_CONTACTO,
         I_CLI.CLI_ESTADO_CIVIL,
         I_CLI.CLI_SALARIO,
         I_CLI.CLI_FEC_NAC_CONYUGUE,
         I_CLI.CLI_DIADOM,
         I_CLI.CLI_DIALUN,
         I_CLI.CLI_DIAMAR,
         I_CLI.CLI_DIAMIE,
         I_CLI.CLI_DIAJUE,
         I_CLI.CLI_DIAVIE,
         I_CLI.CLI_DIASAB,
         I_CLI.CLI_PEDIDO_REPETITIVO,
         I_CLI.CLI_REC_LPREC,
         I_CLI.CLI_DV,
         I_CLI.CLI_RUC_DV,
         I_CLI.CLI_CIUDAD,
         I_CLI.CLI_DIA_LUNES,
         I_CLI.CLI_DIA_MARTES,
         I_CLI.CLI_DIA_MIERCOLES,
         I_CLI.CLI_DIA_JUEVES,
         I_CLI.CLI_DIA_VIERNES,
         I_CLI.CLI_DIA_SABADO,
         I_CLI.CLI_VENDEDOR,
         I_CLI.CLI_ORDEN_LUN,
         I_CLI.CLI_ORDEN_MAR,
         I_CLI.CLI_ORDEN_MIER,
         I_CLI.CLI_ORDEN_JUE,
         I_CLI.CLI_ORDEN_VIER,
         I_CLI.CLI_ORDEN_SAB,
         I_CLI.CLI_COD_BARRIO,
         I_CLI.CLI_REPOSITOR,
         I_CLI.CLI_IND_PAGO_CON_CHEQUE,
         I_CLI.CLI_MAX_CANT_CUOTA_CONT,
         I_CLI.CLI_COD_PROVEEDOR,
         I_CLI.CLI_COMI_ASO,
         I_CLI.CLI_APELLIDO,
         I_CLI.CLI_NOMBRE,
         I_CLI.CLI_DIR_ESPECIFICA,
         I_CLI.CLI_ASOCIACION,
         I_CLI.CLI_DIR_FOTO,
         I_CLI.CLI_INFORMCONF,
         I_CLI.CLI_FEC_INGRESO_CLI,
         I_CLI.CLI_SECTOR_PROD,
         I_CLI.CLI_PERSONA,
         I_CLI.CLI_CANAL,
         I_CLI.CLI_IND_EXHIBIDOR,
         I_CLI.CLI_IND_MOD_CANAL,
         I_CLI.CLI_OBS_FACT,
         I_CLI.CLI_SUCURSAL,
         I_CLI.CLI_COBRADOR,
         I_CLI.CLI_COD_PROSPECTO,
         I_CLI.CLI_IND_SILO,
         I_CLI.CLI_COD_FICHA_HOLDING,
         I_CLI.CLI_CHORTI_DEB_AUTO,
         I_CLI.CLI_CHORTI_NRO_CTA,
         I_CLI.CLI_EXP_DIR,
         I_CLI.CLI_EXP_RUC,
         I_CLI.CLI_IND_TACHO,
         I_CLI.CLI_TEL_SMS,
         I_CLI.CLI_FREC_VISITA_SEMANAL,
         I_CLI.CLI_FREC_VISITA_QUINCENAL,
         I_CLI.CLI_LATITUD,
         I_CLI.CLI_LONGITUD,
         I_CLI.CLI_SUP_COBRANZA,
         I_CLI.CLI_RUC_HOLDING,
         I_CLI.CLI_EMPR,
         I_CLI.CLI_RESUMEN_FACT,
         I_CLI.CLI_ACA_COL_ORIG,
         I_CLI.CLI_HOLDING,
         I_CLI.CLI_ACA_DIR_LAB,
         I_CLI.CLI_ACA_TEL_LAB,
         I_CLI.CLI_ACA_NOM_FAC,
         I_CLI.CLI_ACA_DIR_FAC,
         I_CLI.CLI_ACA_CIP_FAC,
         I_CLI.CLI_ACA_RUC_FAC,
         I_CLI.CLI_ACA_NIVEL,
         I_CLI.CLI_ACA_GRADO,
         I_CLI.CLI_ACA_TURNO,
         I_CLI.CLI_ACA_SECCION,
         I_CLI.CLI_ACA_APLAZADO,
         I_CLI.CLI_ACA_CLI_TIPO,
         I_CLI.CLI_ACA_INACTIVO,
         I_CLI.CLI_ACA_IMG_BLOB,
         I_CLI.CLI_ACA_IMG_LR,
         I_CLI.CLI_COND_PAGO,
         I_CLI.CLI_FEC_ENTR_EXHIBIDOR,
         I_CLI.CLI_COD_EMPL_EMPR_ORIG,
         I_CLI.CLI_CODIGO_ANTERIOR,
         I_CLI.CLI_CTRL_PRECIO_AQ,
         I_CLI.CLI_FACT_COMB_GS,
         I_CLI.CLI_IND_EXIGIR_RESP_FCRED,
         I_CLI.CLI_PORC_FLETE,
         I_CLI.CLI_SEGMENTO,
         I_CLI.CLI_VALOR_AGREGADO,
         I_CLI.CLI_SEGMENTO_MACRO,
         I_CLI.CLI_SEGMENTO_MICRO,
         I_CLI.CLI_AUTO_LNEGRA,
         I_CLI.CLI_FREC_VISITA_SEM_PAR_IMPAR,
         I_CLI.CLI_IND_BLOQUEO_VTA_PERDIDA,
         I_CLI.CLI_SEGMENTO_SUC_CANAL,
         I_CLI.CLI_CANAL_BETA,
         I_CLI.CLI_HR_AM_RECEP_DESDE,
         I_CLI.CLI_HR_AM_RECEP_HASTA,
         I_CLI.CLI_HR_PM_RECEP_DESDE,
         I_CLI.CLI_HR_PM_RECEP_HASTA,
         I_CLI.CLI_DOC_TIPO,
         I_CLI.cli_empresa_funcionario --> 12/08/2022 9:04:43 @PabloACespedes \(^-^)/, para limite de credito
         );

    ELSIF I_OPER = 'UPDATE' THEN
      UPDATE FIN_CLIENTE
         SET CLI_NOM                     = I_CLI.CLI_NOM,
             CLI_PROPIETARIO             = I_CLI.CLI_PROPIETARIO,
             CLI_NOM_FANTASIA            = I_CLI.CLI_NOM_FANTASIA,
             CLI_DIR                     = I_CLI.CLI_DIR,
             CLI_DOC_IDENT_PROPIETARIO   = I_CLI.CLI_DOC_IDENT_PROPIETARIO,
             CLI_LOCALIDAD               = I_CLI.CLI_LOCALIDAD,
             CLI_FEC_NAC_PROPIETARIO     = I_CLI.CLI_FEC_NAC_PROPIETARIO,
             CLI_TEL                     = I_CLI.CLI_TEL,
             CLI_FAX                     = I_CLI.CLI_FAX,
             CLI_RUC                     = I_CLI.CLI_RUC,
             CLI_RAMO                    = I_CLI.CLI_RAMO,
             CLI_PERSONERIA              = I_CLI.CLI_PERSONERIA,
             CLI_CATEG                   = I_CLI.CLI_CATEG,
             CLI_ZONA                    = I_CLI.CLI_ZONA,
             CLI_PAIS                    = I_CLI.CLI_PAIS,
             CLI_FEC_ANIV                = I_CLI.CLI_FEC_ANIV,
             CLI_PERS_CONTACTO           = I_CLI.CLI_PERS_CONTACTO,
             CLI_FORMA_ENVIO             = I_CLI.CLI_FORMA_ENVIO,
             CLI_OBS                     = I_CLI.CLI_OBS,
             CLI_PORC_EXEN_IVA           = I_CLI.CLI_PORC_EXEN_IVA,
             CLI_DOC_IDENT_CONTACTO      = I_CLI.CLI_DOC_IDENT_CONTACTO,
             CLI_EST_CLI                 = I_CLI.CLI_EST_CLI,
             CLI_FEC_NAC_CONTACTO        = I_CLI.CLI_FEC_NAC_CONTACTO,
             CLI_MON                     = I_CLI.CLI_MON,
             CLI_IMP_LIM_CR              = I_CLI.CLI_IMP_LIM_CR,
             CLI_BLOQ_LIM_CR             = I_CLI.CLI_BLOQ_LIM_CR,
             CLI_MAX_DIAS_ATRASO         = I_CLI.CLI_MAX_DIAS_ATRASO,
             CLI_PERS_REPRESENTANTE      = I_CLI.CLI_PERS_REPRESENTANTE,
             CLI_FEC_INGRESO             = I_CLI.CLI_FEC_INGRESO,
             CLI_FEC_ACTUALIZACION       = I_CLI.CLI_FEC_ACTUALIZACION,
             CLI_IND_POTENCIAL           = I_CLI.CLI_IND_POTENCIAL,
             CLI_DOC_IDENT_REPRESENTANTE = I_CLI.CLI_DOC_IDENT_REPRESENTANTE,
             CLI_FEC_NAC_REPRESENTANTE   = I_CLI.CLI_FEC_NAC_REPRESENTANTE,
             CLI_LUGAR_ORIGEN_REPLICA    = I_CLI.CLI_LUGAR_ORIGEN_REPLICA,
             CLI_NRO_FINCA               = I_CLI.CLI_NRO_FINCA,
             CLI_NOM_CONYUGUE            = I_CLI.CLI_NOM_CONYUGUE,
             CLI_DOC_IDENT_CONYUGUE      = I_CLI.CLI_DOC_IDENT_CONYUGUE,
             CLI_DIR_PARTICULAR          = I_CLI.CLI_DIR_PARTICULAR,
             CLI_LUGAR_TRABAJO           = I_CLI.CLI_LUGAR_TRABAJO,
             CLI_TEL_PARTICULAR          = I_CLI.CLI_TEL_PARTICULAR,
             CLI_BARRIO                  = I_CLI.CLI_BARRIO,
             CLI_PROFESION               = I_CLI.CLI_PROFESION,
             CLI_EMAIL                   = I_CLI.CLI_EMAIL,
             CLI_NRO_FICHA               = I_CLI.CLI_NRO_FICHA,
             CLI_PROYECTO                = I_CLI.CLI_PROYECTO,
             CLI_SEXO                    = I_CLI.CLI_SEXO,
             CLI_TEL_CONTACTO            = I_CLI.CLI_TEL_CONTACTO,
             CLI_ESTADO_CIVIL            = I_CLI.CLI_ESTADO_CIVIL,
             CLI_SALARIO                 = I_CLI.CLI_SALARIO,
             CLI_FEC_NAC_CONYUGUE        = I_CLI.CLI_FEC_NAC_CONYUGUE,
             CLI_DIADOM                  = I_CLI.CLI_DIADOM,
             CLI_DIALUN                  = I_CLI.CLI_DIALUN,
             CLI_DIAMAR                  = I_CLI.CLI_DIAMAR,
             CLI_DIAMIE                  = I_CLI.CLI_DIAMIE,
             CLI_DIAJUE                  = I_CLI.CLI_DIAJUE,
             CLI_DIAVIE                  = I_CLI.CLI_DIAVIE,
             CLI_DIASAB                  = I_CLI.CLI_DIASAB,
             CLI_PEDIDO_REPETITIVO       = I_CLI.CLI_PEDIDO_REPETITIVO,
             CLI_REC_LPREC               = I_CLI.CLI_REC_LPREC,
             CLI_DV                      = I_CLI.CLI_DV,
             CLI_RUC_DV                  = I_CLI.CLI_RUC_DV,
             CLI_CIUDAD                  = I_CLI.CLI_CIUDAD,
             CLI_DIA_LUNES               = I_CLI.CLI_DIA_LUNES,
             CLI_DIA_MARTES              = I_CLI.CLI_DIA_MARTES,
             CLI_DIA_MIERCOLES           = I_CLI.CLI_DIA_MIERCOLES,
             CLI_DIA_JUEVES              = I_CLI.CLI_DIA_JUEVES,
             CLI_DIA_VIERNES             = I_CLI.CLI_DIA_VIERNES,
             CLI_DIA_SABADO              = I_CLI.CLI_DIA_SABADO,
             CLI_VENDEDOR                = I_CLI.CLI_VENDEDOR,
             CLI_ORDEN_LUN               = I_CLI.CLI_ORDEN_LUN,
             CLI_ORDEN_MAR               = I_CLI.CLI_ORDEN_MAR,
             CLI_ORDEN_MIER              = I_CLI.CLI_ORDEN_MIER,
             CLI_ORDEN_JUE               = I_CLI.CLI_ORDEN_JUE,
             CLI_ORDEN_VIER              = I_CLI.CLI_ORDEN_VIER,
             CLI_ORDEN_SAB               = I_CLI.CLI_ORDEN_SAB,
             CLI_COD_BARRIO              = I_CLI.CLI_COD_BARRIO,
             CLI_REPOSITOR               = I_CLI.CLI_REPOSITOR,
             CLI_IND_PAGO_CON_CHEQUE     = I_CLI.CLI_IND_PAGO_CON_CHEQUE,
             CLI_MAX_CANT_CUOTA_CONT     = I_CLI.CLI_MAX_CANT_CUOTA_CONT,
             CLI_COD_PROVEEDOR           = I_CLI.CLI_COD_PROVEEDOR,
             CLI_COMI_ASO                = I_CLI.CLI_COMI_ASO,
             CLI_APELLIDO                = I_CLI.CLI_APELLIDO,
             CLI_NOMBRE                  = I_CLI.CLI_NOMBRE,
             CLI_DIR_ESPECIFICA          = I_CLI.CLI_DIR_ESPECIFICA,
             CLI_ASOCIACION              = I_CLI.CLI_ASOCIACION,
             CLI_DIR_FOTO                = I_CLI.CLI_DIR_FOTO,
             CLI_INFORMCONF              = I_CLI.CLI_INFORMCONF,
             CLI_FEC_INGRESO_CLI         = I_CLI.CLI_FEC_INGRESO_CLI,
             CLI_SECTOR_PROD             = I_CLI.CLI_SECTOR_PROD,
             CLI_PERSONA                 = I_CLI.CLI_PERSONA,
             CLI_CANAL                   = I_CLI.CLI_CANAL,
             CLI_IND_EXHIBIDOR           = I_CLI.CLI_IND_EXHIBIDOR,
             CLI_IND_MOD_CANAL           = I_CLI.CLI_IND_MOD_CANAL,
             CLI_OBS_FACT                = I_CLI.CLI_OBS_FACT,
             CLI_SUCURSAL                = I_CLI.CLI_SUCURSAL,
             CLI_COBRADOR                = I_CLI.CLI_COBRADOR,
             CLI_COD_PROSPECTO           = I_CLI.CLI_COD_PROSPECTO,
             CLI_IND_SILO                = I_CLI.CLI_IND_SILO,
             CLI_COD_FICHA_HOLDING       = I_CLI.CLI_COD_FICHA_HOLDING,
             CLI_CHORTI_DEB_AUTO         = I_CLI.CLI_CHORTI_DEB_AUTO,
             CLI_CHORTI_NRO_CTA          = I_CLI.CLI_CHORTI_NRO_CTA,
             CLI_EXP_DIR                 = I_CLI.CLI_EXP_DIR,
             CLI_EXP_RUC                 = I_CLI.CLI_EXP_RUC,
             CLI_IND_TACHO               = I_CLI.CLI_IND_TACHO,
             CLI_TEL_SMS                 = I_CLI.CLI_TEL_SMS,
             CLI_FREC_VISITA_SEMANAL     = I_CLI.CLI_FREC_VISITA_SEMANAL,
             CLI_FREC_VISITA_QUINCENAL   = I_CLI.CLI_FREC_VISITA_QUINCENAL,
             CLI_LATITUD                 = I_CLI.CLI_LATITUD,
             CLI_LONGITUD                = I_CLI.CLI_LONGITUD,
             CLI_SUP_COBRANZA            = I_CLI.CLI_SUP_COBRANZA,
             CLI_RUC_HOLDING             = I_CLI.CLI_RUC_HOLDING,
             CLI_RESUMEN_FACT            = I_CLI.CLI_RESUMEN_FACT,
             CLI_ACA_COL_ORIG            = I_CLI.CLI_ACA_COL_ORIG,
             CLI_HOLDING                 = I_CLI.CLI_HOLDING,
             CLI_ACA_DIR_LAB             = I_CLI.CLI_ACA_DIR_LAB,
             CLI_ACA_TEL_LAB             = I_CLI.CLI_ACA_TEL_LAB,
             CLI_ACA_NOM_FAC             = I_CLI.CLI_ACA_NOM_FAC,
             CLI_ACA_DIR_FAC             = I_CLI.CLI_ACA_DIR_FAC,
             CLI_ACA_CIP_FAC             = I_CLI.CLI_ACA_CIP_FAC,
             CLI_ACA_RUC_FAC             = I_CLI.CLI_ACA_RUC_FAC,
             CLI_ACA_NIVEL               = I_CLI.CLI_ACA_NIVEL,
             CLI_ACA_GRADO               = I_CLI.CLI_ACA_GRADO,
             CLI_ACA_TURNO               = I_CLI.CLI_ACA_TURNO,
             CLI_ACA_SECCION             = I_CLI.CLI_ACA_SECCION,
             CLI_ACA_APLAZADO            = I_CLI.CLI_ACA_APLAZADO,
             CLI_ACA_CLI_TIPO            = I_CLI.CLI_ACA_CLI_TIPO,
             CLI_ACA_INACTIVO            = I_CLI.CLI_ACA_INACTIVO,
             CLI_ACA_IMG_BLOB            = I_CLI.CLI_ACA_IMG_BLOB,
             CLI_ACA_IMG_LR              = I_CLI.CLI_ACA_IMG_LR,
             CLI_COND_PAGO                 = NVL(I_CLI.CLI_COND_PAGO,CLI_COND_PAGO),
             CLI_FEC_ENTR_EXHIBIDOR        = I_CLI.CLI_FEC_ENTR_EXHIBIDOR,
             CLI_COD_EMPL_EMPR_ORIG        = I_CLI.CLI_COD_EMPL_EMPR_ORIG,
             CLI_CODIGO_ANTERIOR           = I_CLI.CLI_CODIGO_ANTERIOR,
             CLI_CTRL_PRECIO_AQ            = I_CLI.CLI_CTRL_PRECIO_AQ,
             CLI_FACT_COMB_GS              = I_CLI.CLI_FACT_COMB_GS,
             CLI_IND_EXIGIR_RESP_FCRED     = I_CLI.CLI_IND_EXIGIR_RESP_FCRED,
             CLI_PORC_FLETE                = I_CLI.CLI_PORC_FLETE,
             CLI_SEGMENTO                  = I_CLI.CLI_SEGMENTO,
             CLI_VALOR_AGREGADO            = I_CLI.CLI_VALOR_AGREGADO,
             CLI_SEGMENTO_MACRO            = I_CLI.CLI_SEGMENTO_MACRO,
             CLI_SEGMENTO_MICRO            = I_CLI.CLI_SEGMENTO_MICRO,
             CLI_AUTO_LNEGRA               = I_CLI.CLI_AUTO_LNEGRA,
             CLI_FREC_VISITA_SEM_PAR_IMPAR = I_CLI.CLI_FREC_VISITA_SEM_PAR_IMPAR,
             CLI_IND_BLOQUEO_VTA_PERDIDA   = I_CLI.CLI_IND_BLOQUEO_VTA_PERDIDA,
             CLI_SEGMENTO_SUC_CANAL        = I_CLI.CLI_SEGMENTO_SUC_CANAL,
             CLI_CANAL_BETA                = I_CLI.CLI_CANAL_BETA,
             CLI_HR_AM_RECEP_DESDE        = I_CLI.CLI_HR_AM_RECEP_DESDE,
             CLI_HR_AM_RECEP_HASTA        = I_CLI.CLI_HR_AM_RECEP_HASTA,
             CLI_HR_PM_RECEP_DESDE        =I_CLI.CLI_HR_PM_RECEP_DESDE,
             CLI_HR_PM_RECEP_HASTA        =I_CLI.CLI_HR_PM_RECEP_HASTA,
             CLI_DOC_TIPO                 = I_CLI.CLI_DOC_TIPO,
             CLI_EMPRESA_FUNCIONARIO      = I_CLI.cli_empresa_funcionario
       WHERE CLI_CODIGO = I_CLI.CLI_CODIGO
         AND CLI_EMPR = I_CLI.CLI_EMPR;

    /*  IF FINI110.PP_AUX_CREDITO(I_USUARIO => I_USUARIO,
                                I_EMPRESA => I_EMPRESA) >= 1 THEN

        UPDATE FIN_CLIENTE
           SET CLI_COND_PAGO = I_CLI.CLI_COND_PAGO
         WHERE CLI_CODIGO = I_CLI.CLI_CODIGO
           AND CLI_EMPR = I_CLI.CLI_EMPR;
      END IF;*/

    ELSIF I_OPER = 'DELETE' THEN
      DELETE FIN_CLIENTE
       WHERE CLI_CODIGO = I_CLI.CLI_CODIGO
         AND CLI_EMPR = I_CLI.CLI_EMPR;
    END IF;
  END PP_GUARDAR_CLIENTE;

  FUNCTION FP_BUSCAR_CIUDAD(I_CIUDAD IN VARCHAR2) RETURN NUMBER AS
    V_CODIGO NUMBER;
    V_DESC   VARCHAR2(200) := TRANSLATE(UPPER(I_CIUDAD), 'AEIOU', 'AEIOU');
  BEGIN
    IF UPPER(V_DESC) LIKE 'ASUNCION' THEN
      V_DESC := 'CENTRAL';
    END IF;

    SELECT T.CIU_CODIGO
      INTO V_CODIGO
      FROM FAC_CIUDAD T
     WHERE T.CIU_EMPR = 1
       AND ROWNUM = 1
       AND UPPER(V_DESC) LIKE
           TRANSLATE(UPPER(SUBSTR(T.CIU_DESC, 0, LENGTH(T.CIU_DESC) - 2) || '%'),
                     'AEIOU',
                     'AEIOU');
    RETURN V_CODIGO;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 1;
  END FP_BUSCAR_CIUDAD;

  FUNCTION FP_BUSCAR_BARRIO(I_BARRIO IN VARCHAR2,
                            I_DPTO   IN NUMBER,
                            I_CIUDAD IN VARCHAR2) RETURN NUMBER AS
    V_CODIGO NUMBER;
  BEGIN

    SELECT T.BARR_CODIGO
      INTO V_CODIGO
      FROM GEN_BARRIO T
     WHERE T.BARR_EMPR = 1
          -- AND T.BARR_CODIGO_DEP = I_DPTO
       AND ROWNUM = 1
       AND T.BARR_CIU_DESC = I_CIUDAD
       AND UPPER(T.BARR_DESC) = UPPER(I_BARRIO);
    RETURN V_CODIGO;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      SELECT NVL(MAX(T.BARR_CODIGO), 0) + 1
        INTO V_CODIGO
        FROM GEN_BARRIO T
       WHERE T.BARR_EMPR = 1;
      INSERT INTO GEN_BARRIO
        (BARR_CODIGO,
         BARR_DESC,
         BARR_CODIGO_LOC,
         BARR_CODIGO_DEP,
         BARR_EMPR,
         BARR_CIU_DESC)
      VALUES
        (V_CODIGO, UPPER(I_BARRIO), 1, I_DPTO, 1, I_CIUDAD);

      RETURN V_CODIGO;

  END FP_BUSCAR_BARRIO;

  FUNCTION FP_GET_CLI_COD RETURN NUMBER AS
    V_RETURN    NUMBER;
    V_DISTANCIA NUMBER;
  BEGIN
    SELECT CLI_CODIGO, DISTANCIA_DE_SUCURSAL
      INTO V_RETURN, V_DISTANCIA
      FROM (SELECT T.CLI_CODIGO,
                   P2P_DISTANCE(P_LATITUDE1  => S.SUC_LATITUD,
                                P_LONGITUDE1 => S.SUC_LONGITUD,
                                P_LATITUDE2  => T.CLI_LATITUD,
                                P_LONGITUDE2 => T.CLI_LONGITUD) DISTANCIA_DE_SUCURSAL

              FROM FIN_CLIENTE T, GEN_SUCURSAL S
             WHERE T.CLI_LONGITUD IS NOT NULL
               AND T.CLI_SUCURSAL = S.SUC_CODIGO
               AND T.CLI_EMPR = S.SUC_EMPR
               AND T.CLI_COD_BARRIO IS NULL
                  --AND T.CLI_SUCURSAL = 1
               AND T.CLI_EMPR = 1
            -- AND T.CLI_PAIS = 1
            --   AND T.CLI_EST_CLI = 'A'
             ORDER BY T.CLI_LATITUD
            --   ORDER BY T.CLI_LONGITUD||T.CLI_LATITUD -- DESC
            --  ORDER BY DISTANCIA_DE_SUCURSAL
            )
     WHERE ROWNUM = 1;

    /*
    SELECT T.CLI_CODIGO
      INTO V_RETURN
      FROM FIN_CLIENTE T
     WHERE T.CLI_EMPR = 1
       AND T.CLI_LATITUD IS NOT NULL
       AND T.CLI_COD_BARRIO IS NULL
       AND T.CLI_EST_CLI = 'A'
       AND T.CLI_SUCURSAL = 2
       AND ROWNUM = 1;*/
    RETURN V_RETURN;
  END FP_GET_CLI_COD;

  FUNCTION FP_GET_CLI_COORDENADA(I_CLI_CODIGO IN NUMBER) RETURN VARCHAR2 AS
    V_VARCHAR2 VARCHAR2(200);
  BEGIN
    SELECT REPLACE(T.CLI_LATITUD, ',', '.') || ',' ||
           REPLACE(T.CLI_LONGITUD, ',', '.') A
      INTO V_VARCHAR2
      FROM FIN_CLIENTE T
     WHERE T.CLI_EMPR = 1
       AND T.CLI_CODIGO = I_CLI_CODIGO;
    RETURN V_VARCHAR2;
  END FP_GET_CLI_COORDENADA;

  PROCEDURE PP_ACT_CLI_BARR(I_DPTO    IN VARCHAR2,
                            I_BARRIO  IN VARCHAR2,
                            I_CIUDAD  IN VARCHAR2,
                            I_CLIENTE IN NUMBER,
                            I_CLI_DIR IN VARCHAR2,
                            I_LAT     IN VARCHAR2,
                            I_LNG     IN VARCHAR2) AS

    V_BARRIO      NUMBER;
    V_DPTO        NUMBER;
    V_BARRIO_DESC VARCHAR2(500);
  BEGIN
    -- RAISE_APPLICATION_ERROR(-20010,
    --'I_DPTO = ' || I_DPTO);

    /* IF I_CIUDAD IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010,
                              'V_BARRIO = ' || V_BARRIO || ' I_CIUDAD= ' ||
                              I_CIUDAD);
    END IF;*/

    V_DPTO := FINM001.FP_BUSCAR_CIUDAD(I_CIUDAD => I_DPTO);

    IF I_BARRIO IS NULL THEN
      V_BARRIO_DESC := 'Desconocido';
    ELSE
      V_BARRIO_DESC := I_BARRIO;
    END IF;

    V_BARRIO := FINM001.FP_BUSCAR_BARRIO(I_BARRIO => V_BARRIO_DESC,
                                         I_DPTO   => V_DPTO,
                                         I_CIUDAD => I_CIUDAD);

    IF V_BARRIO IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'V_BARRIO = ' || V_BARRIO || ' I_CIUDAD= ' ||
                              I_CIUDAD);
    END IF;

    UPDATE GEN_BARRIO T
       SET T.BARR_LAT = I_LAT, T.BARR_LNG = I_LNG
     WHERE T.BARR_CODIGO = V_BARRIO
       AND T.BARR_EMPR = 1
       AND BARR_LAT IS NULL;

    UPDATE FIN_CLIENTE T
       SET T.CLI_COD_BARRIO = NVL(V_BARRIO, T.CLI_COD_BARRIO),
           T.CLI_CIUDAD     = NVL(V_DPTO, T.CLI_CIUDAD),
           T.CLI_BARRIO     = I_CIUDAD
     WHERE T.CLI_EMPR = 1
       AND T.CLI_CODIGO = I_CLIENTE;

    IF LENGTH(I_CLI_DIR) > 5 AND I_CLI_DIR NOT LIKE '%Unnamed Road%' THEN
      BEGIN
        UPDATE FIN_CLIENTE T
           SET T.CLI_DIR = I_CLI_DIR || ' - ' || T.CLI_DIR
         WHERE T.CLI_EMPR = 1
           AND T.CLI_CODIGO = I_CLIENTE
           AND INSTR(T.CLI_DIR, ' ') <> 0
           AND LENGTH(T.CLI_DIR) < 15;

        IF SQL%NOTFOUND THEN
          UPDATE FIN_CLIENTE T
             SET T.CLI_DIR = I_CLI_DIR
           WHERE T.CLI_EMPR = 1
             AND T.CLI_CODIGO = I_CLIENTE
             AND INSTR(T.CLI_DIR, ' ') = 0;

        END IF;
      END;
    END IF;

    COMMIT;

  END PP_ACT_CLI_BARR;

  FUNCTION FP_GRADO(I_NUM IN NUMBER) RETURN NUMBER AS
  BEGIN
    RETURN TRUNC(ABS(I_NUM));
  END FP_GRADO;

  FUNCTION FP_MINUTOS(I_NUM IN NUMBER) RETURN NUMBER AS
    V_MINUTOS NUMBER;
  BEGIN
    V_MINUTOS := (FP_GRADO(I_NUM) - ABS(I_NUM)) * 60;

    RETURN TRUNC(ABS(V_MINUTOS));
  END FP_MINUTOS;

  FUNCTION FP_SEGUNDO(I_NUM IN NUMBER) RETURN NUMBER AS
    V_MINUTOS NUMBER;
    V_SEGUNDO NUMBER;
  BEGIN
    V_MINUTOS := (FP_GRADO(I_NUM) - ABS(I_NUM)) * 60;
    V_SEGUNDO := (FP_MINUTOS(I_NUM) - ABS(V_MINUTOS)) * 60;
    RETURN ABS(V_SEGUNDO);
  END FP_SEGUNDO;

  FUNCTION FP_VISITA_SEMANA(I_DIA       IN VARCHAR2,
                            I_QUINCENAL IN VARCHAR2,
                            I_PAR       IN VARCHAR2) RETURN VARCHAR2 IS
    V_DIA VARCHAR2(2);
    V_PAR VARCHAR2(100);
  BEGIN

    V_DIA := NVL(I_DIA, 'N');
    V_PAR := NVL(I_PAR, 'PAR');

    IF NVL(I_QUINCENAL, 'N') = 'S' THEN
      IF V_PAR = 'PAR' AND MOD(TO_CHAR(SYSDATE, 'IW'), 2) = 0 THEN
        RETURN V_DIA;
      ELSIF V_PAR <> 'PAR' AND MOD(TO_CHAR(SYSDATE, 'IW'), 2) <> 0 THEN
        RETURN V_DIA;
      ELSE
        RETURN 'N';
      END IF;
    END IF;
    RETURN I_DIA;
  END FP_VISITA_SEMANA;

  FUNCTION FP_CLI_DIA_VISITA(I_LUNES     IN VARCHAR2,
                             I_MARTES    IN VARCHAR2,
                             I_MIERCOLES IN VARCHAR2,
                             I_JUEVES    IN VARCHAR2,
                             I_VIERNES   IN VARCHAR2,
                             I_SABADO    IN VARCHAR2) RETURN VARCHAR2 AS
    V_DIA_VISITA VARCHAR2(500);

    PROCEDURE PP_VALOR(I_DIA IN VARCHAR2) AS
    BEGIN
      IF V_DIA_VISITA IS NULL THEN
        V_DIA_VISITA := V_DIA_VISITA || I_DIA;
      ELSE
        V_DIA_VISITA := V_DIA_VISITA || ',' || I_DIA;
      END IF;
    END PP_VALOR;
  BEGIN

    IF I_LUNES = 'S' THEN
      PP_VALOR(I_DIA => 'LUNES');
    END IF;
    IF I_MARTES = 'S' THEN
      PP_VALOR(I_DIA => 'MARTES');
    END IF;
    IF I_MIERCOLES = 'S' THEN
      PP_VALOR(I_DIA => 'MIERCOLES');
    END IF;
    IF I_JUEVES = 'S' THEN
      PP_VALOR(I_DIA => 'JUEVES');
    END IF;
    IF I_VIERNES = 'S' THEN
      PP_VALOR(I_DIA => 'VIERNES');
    END IF;
    IF I_SABADO = 'S' THEN
      PP_VALOR(I_DIA => 'SABADO');
    END IF;

    IF V_DIA_VISITA IS NULL THEN
      V_DIA_VISITA := 'NO ASIGNADO';
    END IF;

    RETURN V_DIA_VISITA;

  END FP_CLI_DIA_VISITA;

  FUNCTION FP_CLI_FRECUENCIA_VISITA(I_SEMANAL     IN VARCHAR2,
                                    I_QUINCENAL   IN VARCHAR2,
                                    I_TIPO_SEMANA IN VARCHAR2)
    RETURN VARCHAR2 AS
    V_FRECUENCIA VARCHAR2(500);
  BEGIN
    IF I_SEMANAL = 'S' THEN
      V_FRECUENCIA := 'SEMANAL';
    ELSIF I_QUINCENAL = 'S' THEN
      V_FRECUENCIA := 'QUINCENAL ' || I_TIPO_SEMANA;
    ELSE
      V_FRECUENCIA := 'NO ASIGNADO';
    END IF;
   RETURN V_FRECUENCIA;
  END FP_CLI_FRECUENCIA_VISITA;

END FINM001;
/
