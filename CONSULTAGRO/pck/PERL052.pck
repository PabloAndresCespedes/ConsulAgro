create or replace package PERL052 is

 PROCEDURE PP_CREAR_CONSULTA_REGISTRO (P_EMPRESA            IN NUMBER,
                                       P_FEC_INI            IN DATE,
                                       P_FEC_FIN            IN DATE,
                                       P_EMPLEADO           IN NUMBER,
                                       P_DEPARTAMENTO       IN NUMBER,
                                       P_SUCURSAL           IN NUMBER,
                                       P_AREA_ORG           IN NUMBER,
                                       P_OPCION             IN VARCHAR2,
                                       P_JSA                IN VARCHAR2,
                                       P_SABADO             IN VARCHAR2);

 PROCEDURE PP_CREAR_PUNTUALIDAD       (P_EMPRESA            IN NUMBER,
                                          P_FEC_INI            IN DATE,
                                          P_FEC_FIN            IN DATE,
                                          P_EMPLEADO           IN NUMBER,
                                          P_DEPARTAMENTO       IN NUMBER,
                                          P_SUCURSAL           IN NUMBER,
                                          P_AREA_ORG           IN NUMBER,
                                          P_JSA                IN VARCHAR2,
                                          P_SABADO             IN VARCHAR2,
                                          P_FORMA_PAGO         IN NUMBER,
                                          P_CARGO              IN NUMBER,
                                          P_EXCLUIR_PORT       IN VARCHAR2);
 PROCEDURE PP_LLAMAR_REPORTE  (P_EMPRESA         IN NUMBER,
                            P_LOGIN           IN VARCHAR2,
                            P_SESSION         IN NUMBER,
                            P_REP             IN VARCHAR2,
                            P_FEC_INI         IN DATE,
                            P_FEC_FIN         IN DATE,
                            P_EMPLEADO        IN NUMBER,
                            P_DEPARTAMENTO    IN NUMBER,
                            P_SUCURSAL        IN NUMBER,
                            P_AREA_ORG        IN NUMBER,
                            P_OPCIONES        IN VARCHAR2,
                            P_JSA             IN VARCHAR2,
                            P_SABADO          IN VARCHAR2,
                            P_FORMATO         IN NUMBER,
                            P_CARGO           IN NUMBER,
                           P_FORMA_PAGO      IN NUMBER);


end PERL052;
/
create or replace package body PERL052 is

   PROCEDURE PP_CREAR_CONSULTA_REGISTRO (P_EMPRESA            IN NUMBER,
                                          P_FEC_INI            IN DATE,
                                          P_FEC_FIN            IN DATE,
                                          P_EMPLEADO           IN NUMBER,
                                          P_DEPARTAMENTO       IN NUMBER,
                                          P_SUCURSAL           IN NUMBER,
                                          P_AREA_ORG           IN NUMBER,
                                          P_OPCION             IN VARCHAR2,
                                          P_JSA                IN VARCHAR2,
                                          P_SABADO             IN VARCHAR2) IS





 P_QUERY VARCHAR2(32767);
 P_WHERE VARCHAR2(32767);


 BEGIN

 IF P_FEC_INI IS NULL THEN
   RAISE_APPLICATION_ERROR(-20001, 'La fecha de inicio no puede quedar vacia');
 END IF;

 IF P_FEC_FIN IS NULL THEN
   RAISE_APPLICATION_ERROR(-20001, 'La fecha de fin no puede quedar vacia');
 END IF;

 IF P_EMPLEADO IS NOT NULL THEN
   P_WHERE := P_WHERE||'AND EMPL_LEGAJO = '||P_EMPLEADO||'';
 END IF;

 IF P_DEPARTAMENTO IS NOT NULL  AND P_EMPRESA = 2THEN
   P_WHERE := P_WHERE||'AND DPTO_CODIGO= '||P_DEPARTAMENTO||'';

   ELSIF P_DEPARTAMENTO IS NOT NULL THEN
      P_WHERE := P_WHERE||'AND COD_DPTO= '||P_DEPARTAMENTO||'';
 END IF;

 IF P_SUCURSAL IS NOT NULL THEN
   P_WHERE := P_WHERE||'AND SUCURSAL= '||P_SUCURSAL||'';
 END IF;

 IF P_OPCION IS NOT NULL THEN

   IF P_OPCION = 'K' THEN
    P_WHERE := P_WHERE||'AND MARC_FECHA IS NULL AND OPCION = ''S'' '||'';---;ENTRADA IS NULL AND SALIDA IS NULL AND OPCION = ''S'''||'';
   ELSE
    P_WHERE := P_WHERE||'AND OPCION= '''||P_OPCION||'''';
   END IF;

 END IF;


 IF P_AREA_ORG IS NOT NULL AND P_EMPRESA = 2 THEN
   P_WHERE := P_WHERE||'AND AREA1= '||P_AREA_ORG||'';

 ELSIF P_AREA_ORG IS NOT NULL  THEN
   P_WHERE := P_WHERE||'AND JSA_COD_AREA= '||P_AREA_ORG||'';
 END IF;
 IF P_SABADO = 'D' AND P_EMPRESA = 2 THEN
  P_WHERE := P_WHERE||'AND OPCION NOT IN (''D'')'||'';

 elsif P_SABADO = 'D' AND P_EMPRESA = 1 THEN
     P_WHERE := P_WHERE||'AND TO_CHAR(FECHA_HOR,''D'', ''NLS_DATE_LANGUAGE=ENGLISH'')  NOT IN (7)'||'';
 END IF;


IF P_EMPRESA = 1 THEN

              P_QUERY := 'SELECT FECHA_HOR,
                                 FECHA_HOR FECHA_FILTRO,
                                 EMPL_LEGAJO,
                                 EMPLEADO,
                                 EMPL_FEC_INGRESO,
                                 EMPL_FEC_SALIDA,
                                 AREA_CLAVE,
                                 AREA,
                                 CARGO,
                                 NIVEL,
                                 SUCURSAL,
                                 DESC_SUCURSAL,
                                 DEPARTAMENTO,
                                 COD_DPTO,
                                 EMPL_FORMA_PAGO,
                                 DEPARTAMENTO2,
                                 EMPL_SITUACION,
                                 EMPL_TIPO_CONT,
                                 FORMA_PAGO,
                                 SITUACION,
                                 MARC_FECHA,
                                 ENTRADA_HOR,
                                 SALIDA_HOR,
                                 SALIDA_ALM,
                                 ENTR_ALM,
                                 MIN(ENTRADA) ENTRADA,
                                 MAX(SALIDA)  SALIDA,
                                 MIN(HORARIO_SAL_ALMUERZO) SALIDA_ALMUERZO,
                                 MAX(HORARIO_ENT_ALMUERZO) ENTRADA_ALMUERZO,
                                 TO_CHAR(MIN(ENTRADA),''HH24:MI:SS'') ENTRADA_HORA,
                                 TO_CHAR(MAX(SALIDA),''HH24:MI:SS'')  SALIDA_HORA,
                                 TO_CHAR(MIN(HORARIO_SAL_ALMUERZO),''HH24:MI:SS'') SALIDA_ALMUERZO_HORA,
                                 TO_CHAR(MAX(HORARIO_ENT_ALMUERZO),''HH24:MI:SS'') ENTRADA_ALMUERZO_HORA,
                                 JSA_COD_AREA,
                                 JSA_DESC_AREA,
                                 OPCION,
                                 CASE
                                   WHEN OPCION = ''D'' THEN
                                     ''DOMINGO''
                                   WHEN OPCION = ''H'' THEN
                                     ''SABADO''
                                   WHEN OPCION = ''X'' THEN
                                     ''FICHA INACTIVA''
                                   WHEN OPCION = ''F'' THEN
                                     ''FERIADO''
                                   WHEN OPCION = ''V'' THEN
                                     ''VAC. REG. LEGAL''
                                   WHEN OPCION = ''T'' THEN
                                     ''VAC. REG. INTERNO''
                                   WHEN OPCION = ''L'' THEN
                                     ''LICENCIA''
                                   WHEN OPCION = ''R'' THEN
                                     ''REPOSO''
                                   WHEN OPCION = ''P'' THEN
                                     ''PERMISO''
                                   WHEN OPCION = ''A'' THEN
                                     ''AUSENCIA JUSTIFICADA''
                                   WHEN OPCION = ''G'' THEN
                                     ''VIAJE LABORAL''
                                   WHEN OPCION = ''U'' THEN
                                     ''SUSPENCION''
                                   WHEN OPCION = ''S'' AND MIN(ENTRADA) IS NULL AND MAX(SALIDA) IS NULL THEN
                                     ''AUSENTE''
                                   ELSE
                                       ''PRESENTE''
                                   END DETALLE,
                                   CASE
                                     WHEN TO_CHAR(MIN(ENTRADA),''HH24:MI:SS'')  > ENTRADA_HOR AND OPCION = ''S'' THEN
                                       ''IMPUNTUAL''
                                     WHEN TO_CHAR(MIN(ENTRADA),''HH24:MI:SS'') < ENTRADA_HOR AND OPCION = ''S'' THEN
                                       ''PUNTUAL''
                                   END PUNTUALIDAD

                          FROM(


                           SELECT FECHA_HOR,
                                  EMPL_LEGAJO,
                                  EMPLEADO,
                                  ENTRADA_HOR,
                                  SALIDA_HOR,
                                  SALIDA_ALM,
                                  ENTR_ALM,
                                  TURNO,
                                  EMPL_FEC_INGRESO,
                                  EMPL_FEC_SALIDA,
                                  AREA_CLAVE,
                                  AREA,
                                  CARGO,
                                  NIVEL,
                                  SUCURSAL,
                                  DESC_SUCURSAL,
                                  DEPARTAMENTO,
                                  COD_DPTO,
                                  EMPL_FORMA_PAGO,
                                  DEPARTAMENTO2,
                                  EMPL_SITUACION,
                                  EMPL_TIPO_CONT,
                                  FORMA_PAGO,
                                  SITUACION,
                                  ALMUERZO,
                                  MAR.MARC_FECHA,
                                  MAR.MARC_EVENTO,
                                  MAR.MARC_HORA,
                                  MAR.MARC_EMPLEADO,
                                  MAR.MARC_EMPR,
                                  MAR.HORA_RELOJ,
                                  MAR.FECHA_HORARIO,
                                 CASE
                                    WHEN  MARC_EVENTO = ''E'' AND IND = ''N'' THEN
                                        MAR.HORA_RELOJ
                                   END ENTRADA,
                                 CASE
                                  WHEN  MARC_EVENTO = ''S'' AND IND = ''N'' THEN
                                        MAR.HORA_RELOJ
                                   END SALIDA,

                                 HORARIO_SAL_ALMUERZO,
                                 HORARIO_ENT_ALMUERZO,
                                 JSA_COD_AREA,
                                 JSA_DESC_AREA,
                                 OPCION
                          FROM
                            (SELECT DIAS.FECHA FECHA_HOR ,
                                         P.EMPL_LEGAJO,
                                         P.EMPL_LEGAJO || '' - '' || P.EMPL_NOMBRE || '' '' || P.EMPL_APE EMPLEADO,
                                         CASE
                                           WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA THEN
                                                TO_CHAR(TUR_DESDE, ''HH24:MI:SS'')
                                           END ENTRADA_HOR,
                                          CASE
                                          WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA THEN
                                                TO_CHAR(TUR_HASTA, ''HH24:MI:SS'')
                                           END SALIDA_HOR,
                                         CASE
                                           WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA AND P.EMPL_MARC_ALMUERZO = ''S'' THEN
                                                TO_CHAR(TUREMP_SAL_ALMUERZO, ''HH24:MI:SS'')
                                           END SALIDA_ALM,
                                         CASE
                                          WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA AND P.EMPL_MARC_ALMUERZO = ''S'' THEN
                                                TO_CHAR(TUREMP_ENT_ALMUERZO, ''HH24:MI:SS'')
                                           END ENTR_ALM,
                                          TUREMP_TURNO TURNO,
                                         P.EMPL_FEC_INGRESO,
                                         P.EMPL_FEC_SALIDA,
                                         AREA.AREDPP_CLAVE || '' - '' || AREA.AREDPP_DESC AREA,
                                         AREDPP_CLAVE AREA_CLAVE,
                                         CAR.CAR_CODIGO || '' - '' || CAR.CAR_DESC CARGO,
                                         NIV.NIVORG_DESC NIVEL,
                                         P.EMPL_SUCURSAL SUCURSAL,
                                         SUC_CODIGO|| '' - '' ||SUC_DESC DESC_SUCURSAL,
                                         DEP.DPTO_DESC DEPARTAMENTO,
                                         DEP.DPTO_CODIGO COD_DPTO,
                                         EMPL_FORMA_PAGO,
                                         CASE
                                          WHEN DPTO_SUC = 2 THEN
                                               ''CDA''
                                          WHEN DPTO_CODIGO = 1 THEN
                                               ''ADMINISTRATIVO''
                                          WHEN (DPTO_CODIGO IN(14,2) OR DPTO_SUC NOT IN (1,2)) THEN
                                              ''COMERCIAL''
                                          WHEN DPTO_CODIGO = 22 THEN
                                              ''LOGISTICA''
                                          ELSE
                                             ''INDUSTRIAL''
                                          END DEPARTAMENTO2,
                                          EMPL_SITUACION,
                                          EMPL_TIPO_CONT,
                                          DECODE(EMPL_FORMA_PAGO,1,''JORNAL'',2,''MENSUAL'',3, ''SERVICIOS PERSONALES'', 4,''TEMPORAL'',5,''AMH'') FORMA_PAGO,
                                          DECODE (EMPL_SITUACION,''I'',''INACTIVO'',''A'',''ACTIVO'',NULL) SITUACION,
                                          nvl(EMPL_MARC_ALMUERZO,''N'') ALMUERZO,
                                           CASE
                                         WHEN AREDPP_CLAVE = 1 THEN
                                          2
                                         WHEN AREDPP_CLAVE IN (2,6) THEN
                                         1
                                         WHEN AREDPP_CLAVE = 5 THEN
                                          5
                                         WHEN AREDPP_CLAVE = 4 THEN
                                         3
                                         WHEN AREDPP_CLAVE = 3 THEN
                                          4
                                        END JSA_COD_AREA,
                                        CASE
                                         WHEN AREDPP_CLAVE = 1 THEN
                                          ''ADM''
                                         WHEN AREDPP_CLAVE IN (2,6)   THEN
                                          ''FIN E INFO''
                                         WHEN AREDPP_CLAVE = 5 THEN
                                          ''INDUSTRIAL''
                                         WHEN AREDPP_CLAVE = 4  THEN
                                          ''CDA''
                                         WHEN AREDPP_CLAVE = 3  THEN
                                          ''COMERCIAL''
                                       END JSA_DESC_AREA,
                                       PER_VER_DIA_LABORAL(P.EMPL_LEGAJO,
                                                                   1,
                                                                   DIAS.FECHA,
                                                                   P.EMPL_FEC_INGRESO,
                                                                   P.EMPL_FEC_SALIDA,
                                                                   DECODE(EMPL_FORMA_PAGO,1,''J'',2,''M'')) OPCION
                                    from PER_CONFIGURACION PCON,
                                         PER_PERIODO PPER,
                                         (SELECT I_DIA FECHA
                                            FROM table(return_table_dia_prof(TO_NUMBER(to_char(SYSDATE,
                                                                                               ''YYYY''))))) DIAS,
                                         PER_EMPLEADO P,
                                         PER_AREA_DPP AREA,
                                         PER_CARGO_DPP CARDPP,
                                         PER_CARGO CAR,
                                         PER_NIVEL_ORGANI NIV,
                                         GEN_DEPARTAMENTO DEP,
                                         GEN_SUCURSAL SUC,
                                         PER_TURNOS_EMPL TUR,
                                         PER_TURNOS M
                                   WHERE P.EMPL_EMPRESA = 1
                                     AND P.EMPL_EMPRESA = DEP.DPTO_EMPR(+)
                                     AND P.EMPL_DEPARTAMENTO = DEP.DPTO_CODIGO(+)
                                     AND P.EMPL_SUCURSAL = DEP.DPTO_SUC(+)
                                     AND P.EMPL_SUCURSAL = SUC_CODIGO(+)
                                     AND P.EMPL_EMPRESA = SUC_EMPR(+)
                                     AND EMPL_FEC_INGRESO IS NOT NULL
                                     AND EMPL_CODIGO_PROV<>0
                                     AND EMPL_FORMA_PAGO <> 0
                                     AND EMPL_SITUACION IS NOT NULL
                                     AND P.EMPL_CONS_MARC = ''S''

                                     AND TUR.TUREMP_TURNO = M.TUR_CODIGO(+)
                                     AND TUR.TUREMP_EMPR = M.TUR_EMPR(+)
                                     AND TUR.TUREMP_EMPR(+) =EMPL_EMPRESA
                                     AND TUR.TUREMP_EMPLEADO(+) = EMPL_LEGAJO
                                     ---AND DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA
                                     AND PCON.CONF_EMPR = P.EMPL_EMPRESA
                                     AND PCON.CONF_EMPR = PPER.PERI_EMPR
                                     AND (PCON.CONF_PERI_SGTE - 3) = PPER.PERI_CODIGO
                                     AND DIAS.FECHA >=''01/06/2020''
                                     AND DIAS.FECHA BETWEEN PPER.PERI_FEC_INI AND SYSDATE
                                     AND P.EMPL_EMPRESA = AREA.AREDPP_EMPR(+)
                                     AND P.EMPL_AREA_ORGANI = AREA.AREDPP_CLAVE(+)
                                     AND P.EMPL_EMPRESA = CARDPP.CARDPP_EMPR(+)
                                     AND P.EMPL_CARGO = CARDPP.CARDPP_CLAVE(+)
                                     AND CARDPP.CARDPP_EMPR = CAR.CAR_EMPR(+)
                                     AND CARDPP.CARDPP_CLAVE = CAR.CAR_CODIGO(+)
                                     AND CARDPP.CARDPP_EMPR = NIV.NIVORG_EMPR(+)
                                     AND CARDPP.CARDPP_NIV_ORGANI = NIV.NIVORG_CLAVE(+)) EMPL,
                                     ( SELECT T.MARC_FECHA,
                                                 T.MARC_EVENTO,
                                                 T.MARC_HORA,
                                                 T.MARC_EMPLEADO,
                                                 T.MARC_EMPR,
                                                 T.HORA_RELOJ,
                                                CASE
                                                  WHEN  MARC_EVENTO = ''S'' AND  to_date(to_char(HORA_RELOJ, ''hh24:mi:ss''), ''hh24:mi:ss'') between to_date(''00:00:00'', ''hh24:mi:ss'') and to_date(''01:00:00'', ''hh24:mi:ss'') THEN
                                                        MARC_FECHA-1
                                                  WHEN  MARC_EVENTO = ''E'' AND  to_date(to_char(HORA_RELOJ, ''hh24:mi:ss''), ''hh24:mi:ss'') between to_date(''00:00:00'', ''hh24:mi:ss'') and to_date(''01:00:00'', ''hh24:mi:ss'') THEN
                                                      MARC_FECHA+1
                                                  ELSE
                                                     MARC_FECHA
                                                 END FECHA_HORARIO,
                                              CASE
                                                WHEN  MARC_EVENTO = ''S'' AND  EMPL_MARC_ALMUERZO = ''S'' AND to_date(to_char(HORA_RELOJ, ''hh24:mi:ss''), ''hh24:mi:ss'') between to_date(''11:00:00'', ''hh24:mi:ss'') and to_date(''13:30:00'', ''hh24:mi:ss'') THEN
                                                      HORA_RELOJ
                                                 END HORARIO_SAL_ALMUERZO,
                                              CASE
                                                 WHEN  MARC_EVENTO = ''E'' AND  EMPL_MARC_ALMUERZO = ''S'' AND to_date(to_char(HORA_RELOJ, ''hh24:mi:ss''), ''hh24:mi:ss'') between to_date(''11:00:00'', ''hh24:mi:ss'') and to_date(''13:30:00'', ''hh24:mi:ss'') THEN
                                                       HORA_RELOJ
                                                 END HORARIO_ENT_ALMUERZO,
                                             CASE
                                                WHEN  EMPL_MARC_ALMUERZO = ''S'' AND to_date(to_char(HORA_RELOJ, ''hh24:mi:ss''), ''hh24:mi:ss'') between to_date(''11:00:00'', ''hh24:mi:ss'') and to_date(''13:30:00'', ''hh24:mi:ss'') THEN
                                                      ''S''
                                                 ELSE
                                                     ''N''
                                                 END IND
                                            FROM PER_MARCACION_DIARIA_V T,
                                                 PER_CONFIGURACION      PC,
                                                 PER_PERIODO            PE,
                                                 PER_EMPLEADO           EM
                                           WHERE PC.CONF_EMPR = 1
                                             AND PC.CONF_EMPR = PE.PERI_EMPR
                                             AND EM.EMPL_EMPRESA = PC.CONF_EMPR
                                             AND EM.EMPL_LEGAJO = T.MARC_EMPLEADO
                                             AND EM.EMPL_FEC_INGRESO IS NOT NULL
                                             AND EM.EMPL_CODIGO_PROV <> 0
                                             AND EMPL_FORMA_PAGO <> 0
                                             AND EM.EMPL_CONS_MARC = ''S''
                                             AND EMPL_SITUACION IS NOT NULL
                                             AND MARC_FECHA >= ''01/06/2020''
                                             AND (PC.CONF_PERI_SGTE - 3) = PE.PERI_CODIGO
                                             AND PC.CONF_EMPR = T.MARC_EMPR
                                             AND T.MARC_FECHA BETWEEN PE.PERI_FEC_INI AND SYSDATE) MAR

                                   WHERE EMPL_LEGAJO = MARC_EMPLEADO (+)
                                     AND FECHA_HOR  = FECHA_HORARIO(+)
                                     AND EMPL_SITUACION  = ''A'')
                             WHERE FECHA_HOR BETWEEN '''||P_FEC_INI||'''  AND '''||P_FEC_FIN||'''
                              '||P_WHERE||'
                                     GROUP BY FECHA_HOR,
                                       EMPL_LEGAJO,
                                       EMPLEADO,
                                       EMPL_FEC_INGRESO,
                                       EMPL_FEC_SALIDA,
                                       AREA_CLAVE,
                                       AREA,
                                       CARGO,
                                       NIVEL,
                                       SUCURSAL,
                                       DESC_SUCURSAL,
                                       DEPARTAMENTO,
                                       COD_DPTO,
                                       EMPL_FORMA_PAGO,
                                       DEPARTAMENTO2,
                                       EMPL_SITUACION,
                                       EMPL_TIPO_CONT,
                                       FORMA_PAGO,
                                       SITUACION,
                                       MARC_FECHA,
                                       ENTRADA_HOR,
                                       SALIDA_HOR,
                                       SALIDA_ALM,
                                       ENTR_ALM,
                                       JSA_COD_AREA,
                                       JSA_DESC_AREA,
                                       OPCION';
        ELSE
           P_QUERY :='SELECT FECHA,
                                FECHA FECHA_FILTRO,
                                EMPL_LEGAJO,
                                EMPLEADO,
                                FECHA_SALIDA,
                                FECHA_INGRESO,
                                CLAV_AREA,
                                AREA,
                                CARGO,
                                NIVEL,
                                SUCURSAL,
                                SUC_DESC,
                                DEPARTAMENTO,
                                DPTO_CODIGO,
                                EMPL_FORMA_PAGO,
                                DEPARTAMENTO DEPARTAMENTO2,
                                EMPL_SITUACION,
                                EMPL_TIPO_cONT,
                                EMPL_FORMA_PAGO,
                                EMPL_SITUACION,
                                MARC_FECHA,
                                HORA_ENT,
                                HORA_SAL,
                                HORA_SALIDA_ALMUERZO,
                                HORA_ENTRADA_ALMUERZO,
                                ENTRADA,
                                SALIDA,
                                MAX(ALMUERZO_ENTRADA) ENTRADA_ALMUERZO,
                                MAX(ALMUERZO_SALIDA) SALIDA_ALMUERZO,
                                TO_CHAR(ENTRADA,''HH24:MI:SS''),
                                TO_CHAR(SALIDA,''HH24:MI:SS''),
                                TO_CHAR(to_DATE(MAX(ALMUERZO_ENTRADA), ''DD/MM/YYYY HH24:MI:SS''),''HH24:MI:SS'') ENTRADA_ALMUERZO,
                                TO_CHAR(to_DATE(MAX(ALMUERZO_SALIDA), ''DD/MM/YYYY HH24:MI:SS''),''HH24:MI:SS'') SALIDA_ALMUERZO,
                                AREA1,
                               AREA_Des1,
                               ESTADO_DIAS,
                               CASE
                               WHEN ENTRADA IS NULL AND SALIDA IS NULL AND ESTADO_DIAS = ''S'' THEN ''AUSENTE''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''P'' THEN ''PERMISO''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''R'' THEN ''REPOSO''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''V'' THEN ''VAC. REG. LEGAL''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''L'' THEN ''LICENCIA''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''G'' THEN ''VIAJE LABORAL''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''A'' THEN ''AUSENCIA JUSTIFICADA''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''U'' THEN ''SUSPENCION''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''T'' THEN ''VAC. REG. INTERNO''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''F'' THEN ''FERIADO''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''H'' THEN ''SABADO''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''D'' THEN ''DOMINGO''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''X'' THEN ''DOMINGO''
                               WHEN (ENTRADA IS NOT NULL OR SALIDA IS NOT NULL) AND ESTADO_DIAS = ''S'' THEN ''PRESENTE''
                         END AUSENTE,
                        CASE--PUNTUALIDAD
                           WHEN TO_CHAR(ENTRADA, ''HH24:MI'') <= HORA_ENT   AND ESTADO_DIAS = ''S'' THEN ''PUNTUAL''
                           WHEN TO_CHAR(ENTRADA, ''HH24:MI'')  > HORA_ENT   AND ESTADO_DIAS = ''S'' THEN ''IMPUNTUAL''
                          END PUNTUALIDAD
                      FROM(
                      SELECT DIAS_HABILES."FECHA",
                             DIAS_HABILES."EMPL_LEGAJO",
                             DIAS_HABILES."EMPLEADO",
                             DIAS_HABILES."AREA",
                             DIAS_HABILES."CARGO",
                             DIAS_HABILES."NIVEL",
                             DIAS_HABILES."SUCURSAL",
                             DIAS_HABILES."DEPARTAMENTO",
                             MARCADOS."MARC_FECHA",
                             MARCADOS."MARC_EMPLEADO",
                             MARCADOS."ENTRADA",
                             MARCADOS."SALIDA",
                             MARCADOS."ENTRADA_MENS",
                             MARCADOS."SALIDA_MENS",
                             MARCADOS."MARCADOR_ENTRADA",
                             MARCADOS."MARCADOR_SALIDA",
                             CASE
                               WHEN MARCADOS.MARCADOR_ENTRADA = ''S'' AND
                                    MARCADOS.MARCADOR_SALIDA = ''S'' THEN
                                ''S''
                               ELSE
                                ''N''
                             END CUMPLE,
                          CASE
                               WHEN MARCADOS.ENTRADA IS NOT NULL AND TO_CHAR(MARCADOS.ENTRADA, ''HH24:MI'') >= ''00:00''  AND TO_CHAR(MARCADOS.ENTRADA, ''HH24:MI'') <=''12:00'' THEN
                                 ''Ma?ana''
                               WHEN MARCADOS.ENTRADA IS NOT NULL AND TO_CHAR(MARCADOS.ENTRADA, ''HH24:MI'') >= ''12:01'' THEN
                                  ''Tarde''
                               ELSE
                                ''''
                          END TURNO,
                              CASE WHEN MARCADOS.MARC_EVENTO = ''E''  AND TO_CHAR(MARCADOS.MARC_HORA, ''HH24:MI'') >= ''11:30''   AND TO_CHAR(MARCADOS.MARC_HORA, ''HH24:MI'') <= ''13:30'' and '||P_EMPRESA||' <> 1  THEN
                                    TO_CHAR(MARCADOS.MARC_HORA, ''DD/MM/YYYY HH24:MI:SS'')
                          END ALMUERZO_E,
                          CASE
                            WHEN MARCADOS.MARC_EVENTO = ''S''  AND TO_CHAR(MARCADOS.MARC_HORA, ''HH24:MI'') >= ''11:30''   AND TO_CHAR(MARCADOS.MARC_HORA, ''HH24:MI'') <= ''13:30'' and '||P_EMPRESA||'  <> 1  THEN
                                    TO_CHAR(MARCADOS.MARC_HORA, ''DD/MM/YYYY HH24:MI:SS'')
                          END ALMUERZO_S,
                           CASE WHEN MARCADOS.MARC_EVENTO = ''E''  AND TO_CHAR(MARCADOS.MARC_HORA, ''HH24:MI'') >= ''11:30''   AND TO_CHAR(MARCADOS.MARC_HORA, ''HH24:MI'') <= ''13:30'' and '||P_EMPRESA||' <> 1  THEN
                                    TO_CHAR(MARCADOS.MARC_HORA, ''DD/MM/YYYY HH24:MI:SS'')
                          END ALMUERZO_ENTRADA,
                           CASE WHEN MARCADOS.MARC_EVENTO = ''S''  AND TO_CHAR(MARCADOS.MARC_HORA, ''HH24:MI'') >= ''11:30''   AND TO_CHAR(MARCADOS.MARC_HORA, ''HH24:MI'') <= ''13:30'' and '||P_EMPRESA||' <> 1 THEN
                                    TO_CHAR(MARCADOS.HORA_RELOJ, ''DD/MM/YYYY HH24:MI:SS'')
                          END ALMUERZO_SALIDA,
                        --  DECODE(ESTADO_DIAS,''V'',''S'',ESTADO_DIAS) ESTADO_DIAS,
                          CASE
                               WHEN ESTADO_DIAS= ''S'' THEN
                                     PER_VERIFICAR_VAC_RINTERNO(EMPL_LEGAJO,
                                                                '||P_EMPRESA||',
                                                                FECHA,
                                                                FECHA_INGRESO,
                                                                FECHA_SALIDA,
                                                                ''M'')
                              ELSE
                                ESTADO_DIAS
                              END ESTADO_DIAS,
                          FECHA_SALIDA,
                          FECHA_INGRESO,
                          CLAV_AREA,
                          DPTO_CODIGO,
                          EMPL_CONS_MARC_TAB,
                          EMPL_MARC_ENTRADA,
                          EMPL_MARC_SALIDA,
                          SUC_DESC,
                          AREA1,
                          AREA_Des1,
                           Case
                             WHEN DEPARTAMENTO2 IN (''ADMINISTRATIVO'', ''COMERCIAL'') and '||P_EMPRESA||' = 1 THEN ''11:45''
                             WHEN DEPARTAMENTO2 IN (''INDUSTRIAL'') and '||P_EMPRESA||' = 1 THEN ''12:00''
                            WHEN DEPARTAMENTO2  IN (''LOGISTICA'') and '||P_EMPRESA||' = 1 THEN ''12:15''
                            ELSE  ''12:00''
                            END HORA_SALIDA_ALMUERZO,
                           Case
                             WHEN DEPARTAMENTO2 IN (''ADMINISTRATIVO'', ''COMERCIAL'') and '||P_EMPRESA||' = 1 THEN  ''12:45''
                             WHEN DEPARTAMENTO2 IN (''INDUSTRIAL'') and '||P_EMPRESA||' = 1 THEN ''13:00''
                            WHEN DEPARTAMENTO2  IN (''LOGISTICA'') and '||P_EMPRESA||' = 1 THEN ''13:15''
                            ELSE  ''13:00''
                            END HORA_ENTRADA_ALMUERZO,
                            ---HORARIOS DE ENTRADA
                        /* CASE
                           WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''06:15''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''07:30''  THEN
                               ''07:00''
                           WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''05:30''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''06:15''   THEN
                               ''06:00''
                            WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''13:30''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''14:30'' THEN
                               ''14:00''
                            WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''21:00''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''22:50'' THEN
                               ''22:00''
                           WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''05:30''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''06:35''    THEN
                             ''06:00''
                          WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''07:30''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''08:35''   THEN
                             ''08:00''
                           WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''08:30''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''09:35''    THEN
                             ''09:00''
                            WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''09:30''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''10:35''    THEN
                             ''10:00''
                            WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''11:30''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''12:35''    THEN
                             ''12:00''
                        ELSE*/
                           to_char(EMPL_MARC_ENTRADA, ''HH24:MI'') HORA_ENT,
                          -- END  HORA_ENT,
                           ---HORARIOS DE SALIDA
                          CASE WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''06:30''
                               AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''07:30''
                               AND TO_CHAR(MARCADOS."SALIDA", ''HH24:MI'')>= ''16:00''
                               THEN-----horario salida normal
                                    ''17:00''
                           WHEN  TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''13:30''
                           AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''05:30''
                           AND TO_CHAR(MARCADOS."SALIDA", ''HH24:MI'')>= ''06:15'' THEN
                             ''14:00''
                            WHEN  TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''13:30''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''14:30''
                           AND TO_CHAR(MARCADOS."SALIDA", ''HH24:MI'')>= ''20:00'' THEN
                             ''22:00''
                            WHEN  TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''21:30''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''22:30''  THEN
                             ''06:00''
                         WHEN  TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''05:30''
                           AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''06:15''
                            AND TO_CHAR(MARCADOS."SALIDA", ''HH24:MI'')>= ''15:30'' THEN
                             ''16:00''
                          WHEN  TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''07:30''
                           AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''08:30''
                            AND TO_CHAR(MARCADOS."SALIDA", ''HH24:MI'')>= ''17:30'' THEN
                             ''18:00''
                          WHEN  TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''08:30''
                           AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''09:30''
                            AND TO_CHAR(MARCADOS."SALIDA", ''HH24:MI'')>= ''18:00'' THEN
                             ''19:00''
                         WHEN  TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''08:30''
                           AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''10:30''
                            AND TO_CHAR(MARCADOS."SALIDA", ''HH24:MI'')>= ''18:00'' THEN
                             ''20:00''
                          WHEN  TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''11:30''
                           AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''12:30''
                            AND TO_CHAR(MARCADOS."SALIDA", ''HH24:MI'')>= ''20:00'' THEN
                             ''22:00''
                           ELSE
                            to_char(EMPL_MARC_SALIDA, ''HH24:MI'')
                           END  HORA_SAL,
                           EMPL_FORMA_PAGO,
                           EMPL_SITUACION,
                           EMPL_TIPO_cONT
                        FROM (SELECT DIAS.FECHA,
                                     P.EMPL_LEGAJO,
                                     P.EMPL_LEGAJO || '' - '' || P.EMPL_NOMBRE || '' '' || P.EMPL_APE EMPLEADO,
                                     AREA.AREDPP_CLAVE || '' - '' || AREA.AREDPP_DESC AREA,
                                     CAR.CAR_CODIGO || '' - '' || CAR.CAR_DESC CARGO,
                                     NIV.NIVORG_DESC NIVEL,
                                     P.EMPL_SUCURSAL SUCURSAL,
                                     DEP.DPTO_DESC DEPARTAMENTO,
                                      CAR.CAR_CODIGO cargo_codigo,
                                     CASE
                                      WHEN DPTO_SUC = 2 and '||P_EMPRESA||' = 1 THEN
                                           ''CDA''
                                      WHEN DPTO_CODIGO = 1 and '||P_EMPRESA||' = 1 THEN
                                           ''ADMINISTRATIVO''
                                      WHEN (DPTO_CODIGO IN(14,2) OR DPTO_SUC NOT IN (1,2))  and '||P_EMPRESA||' = 1 THEN
                                          ''COMERCIAL''
                                      WHEN DPTO_CODIGO = 22 and '||P_EMPRESA||' = 1 THEN
                                          ''LOGISTICA''
                                      when  '||P_EMPRESA||' = 1 then
                                         ''INDUSTRIAL''
                                      when  '||P_EMPRESA||' <> 1 then
                                        DEP.DPTO_DESC
                                      END DEPARTAMENTO2,

                                      PER_VERIFICAR_DIA_LABORAL(P.EMPL_LEGAJO,
                                                                '||P_EMPRESA||',
                                                                DIAS.FECHA,
                                                                P.EMPL_FEC_INGRESO,
                                                                P.EMPL_FEC_SALIDA,
                                                                DECODE(EMPL_FORMA_PAGO,1,''J'',2,''M'')) ESTADO_DIAS,
                                     P.EMPL_FEC_SALIDA FECHA_SALIDA,
                                     P.EMPL_FEC_INGRESO FECHA_INGRESO,
                                     AREDPP_CLAVE CLAV_AREA,
                                     DEP.DPTO_CODIGO DPTO_CODIGO,
                                     NVL(EMPL_CONS_MARC_TAB,''S'') EMPL_CONS_MARC_TAB,
                                     EMPL_MARC_ENTRADA,
                                     EMPL_MARC_SALIDA,
                                     SUC_DESC,
                                   CASE
                                     when '||P_EMPRESA||' <> 1 then  AREDPP_CLAVE
                                    END AREA1,
                                   CASE
                                     when '||P_EMPRESA||' <> 1 then AREDPP_DESC
                                  END AREA_Des1,
                                  DECODE(EMPL_FORMA_PAGO,1,''JORNAL'',2,''MENSUAL'',3, ''SERVICIOS PERSONALES'', 4,''TEMPORAL'',5,''AMH'') EMPL_FORMA_PAGO,
                                  DECODE (EMPL_SITUACION,''I'',''INACTIVO'',''A'',''ACTIVO'',NULL) EMPL_SITUACION,
                                  EMPL_TIPO_cONT
                                FROM PER_CONFIGURACION PCON,
                                     PER_PERIODO PPER,
                                     (SELECT I_DIA FECHA
                                        FROM TABLE(RETURN_TABLE_DIA_PROF(TO_NUMBER(TO_CHAR(SYSDATE,
                                                                                           ''YYYY''))))) DIAS,
                                     PER_EMPLEADO P,
                                     PER_AREA_DPP AREA,
                                     PER_CARGO_DPP CARDPP,
                                     PER_CARGO CAR,
                                     PER_NIVEL_ORGANI NIV,
                                     GEN_DEPARTAMENTO DEP,
                                     GEN_SUCURSAL SUC
                               WHERE P.EMPL_EMPRESA = '||P_EMPRESA||'
                                 AND P.EMPL_EMPRESA = DEP.DPTO_EMPR(+)
                                 AND P.EMPL_DEPARTAMENTO = DEP.DPTO_CODIGO(+)
                                 AND P.EMPL_SUCURSAL = DEP.DPTO_SUC(+)
                                 AND P.EMPL_SUCURSAL = SUC.SUC_CODIGO(+)
                                 AND P.EMPL_EMPRESA = SUC.SUC_EMPR (+)
                                -- AND P.EMPL_FEC_INGRESO >= '''||P_FEC_INI||'''
                               --  AND P.EMPL_FORMA_PAGO <> 1
                                 AND P.EMPL_SITUACION = ''A''
                                 AND P.EMPL_CONS_MARC = ''S''
                                 AND PCON.CONF_EMPR = P.EMPL_EMPRESA
                                 AND PCON.CONF_EMPR = PPER.PERI_EMPR
                                 AND (PCON.CONF_PERI_ACT - 3) = PPER.PERI_CODIGO
                                 AND DIAS.FECHA BETWEEN PPER.PERI_FEC_INI AND SYSDATE
                                 AND P.EMPL_EMPRESA = AREA.AREDPP_EMPR(+)
                                 AND P.EMPL_AREA_ORGANI = AREA.AREDPP_CLAVE(+)
                                 AND P.EMPL_EMPRESA = CARDPP.CARDPP_EMPR(+)
                                 AND P.EMPL_CARGO = CARDPP.CARDPP_CLAVE(+)
                                 AND CARDPP.CARDPP_EMPR = CAR.CAR_EMPR(+)
                                 AND CARDPP.CARDPP_CLAVE = CAR.CAR_CODIGO(+)
                                 AND CARDPP.CARDPP_EMPR = NIV.NIVORG_EMPR(+)
                                 AND CARDPP.CARDPP_NIV_ORGANI = NIV.NIVORG_CLAVE(+)) DIAS_HABILES,

                             (SELECT CALC.*,
                                     CASE
                                       WHEN CALC.ENTRADA IS NOT NULL THEN
                                        CASE
                                          WHEN TO_DATE(''01/01/2018 '' ||
                                                       TO_CHAR(CALC.ENTRADA, ''HH24:MI:SS''),
                                                       ''DD/MM/YYYY HH24:MI:SS'') >=
                                               TO_DATE(''01/01/2018 '' ||
                                                       TO_CHAR(CALC.ENTRADA_MENS, ''HH24:MI:SS''),
                                                       ''DD/MM/YYYY HH24:MI:SS'') THEN
                                           ''N''
                                          ELSE
                                           ''S''
                                        END
                                       ELSE
                                        ''N''
                                     END MARCADOR_ENTRADA,
                                     CASE
                                       WHEN CALC.SALIDA IS NOT NULL THEN
                                        CASE
                                          WHEN TO_DATE(''01/01/2018 '' ||
                                                       TO_CHAR(CALC.SALIDA, ''HH24:MI:SS''),
                                                       ''DD/MM/YYYY HH24:MI:SS'') <=
                                               TO_DATE(''01/01/2018 '' ||
                                                       TO_CHAR(CALC.SALIDA_MENS, ''HH24:MI:SS''),
                                                       ''DD/MM/YYYY HH24:MI:SS'') THEN
                                           ''N''
                                          ELSE
                                           ''S''
                                        END
                                       ELSE
                                        ''N''
                                     END MARCADOR_SALIDA
                                FROM (SELECT CONS.MARC_FECHA,
                                             CONS.MARC_EVENTO,
                                             CONS.MARC_HORA,
                                             CONS.MARC_EMPLEADO,
                                             (SELECT MIN(EN.HORA_RELOJ)
                                                FROM PER_MARCACION_DIARIA_V EN
                                               WHERE EN.MARC_FECHA = CONS.MARC_FECHA
                                                 AND EN.MARC_EMPLEADO = CONS.MARC_EMPLEADO
                                                 AND EN.MARC_EVENTO = ''E''
                                                 AND EN.MARC_EMPR =  '||P_EMPRESA||') ENTRADA,
                                             (SELECT MAX(EN.HORA_RELOJ)
                                                FROM PER_MARCACION_DIARIA_V EN
                                               WHERE EN.MARC_FECHA = CONS.MARC_FECHA
                                                 AND EN.MARC_EMPLEADO = CONS.MARC_EMPLEADO
                                                 AND EN.MARC_EVENTO = ''S''
                                                 AND EN.MARC_EMPR = '||P_EMPRESA||') SALIDA,
                                             TO_DATE(TO_CHAR(CONS.MARC_FECHA, ''DD/MM/YYYY'') || '' '' ||
                                                     TO_CHAR(CONS.CONF_ENTRADA, ''HH24:MI:SS''),
                                                     ''DD/MM/YYYY HH24:MI:SS'') ENTRADA_MENS,
                                             TO_DATE(TO_CHAR(CONS.MARC_FECHA, ''DD/MM/YYYY'') || '' '' ||
                                                     TO_CHAR(CONS.CONF_SALIDA, ''HH24:MI:SS''),
                                                     ''DD/MM/YYYY HH24:MI:SS'') SALIDA_MENS,
                                                    HORA_RELOJ---
                                        FROM (SELECT T.MARC_FECHA,
                                                     T.MARC_EVENTO,
                                                     T.MARC_HORA,
                                                     T.MARC_EMPLEADO,
                                                     T.MARC_EMPR,
                                                     T.HORA_RELOJ,
                                                     EM.EMPL_MARC_ENTRADA CONF_ENTRADA,
                                                     EM.EMPL_MARC_SALIDA  CONF_SALIDA
                                                FROM PER_MARCACION_DIARIA_V T,
                                                     PER_CONFIGURACION      PC,
                                                     PER_PERIODO            PE,
                                                     PER_EMPLEADO           EM
                                               WHERE PC.CONF_EMPR = '||P_EMPRESA||'
                                                 AND PC.CONF_EMPR = PE.PERI_EMPR
                                                 AND EM.EMPL_EMPRESA = PC.CONF_EMPR
                                                 AND EM.EMPL_LEGAJO = T.MARC_EMPLEADO
                                                -- AND EM.EMPL_FORMA_PAGO <> 1
                                                 AND EM.EMPL_SITUACION = ''A''
                                                 AND EM.EMPL_CONS_MARC = ''S''
                                                 AND (PC.CONF_PERI_ACT - 3) = PE.PERI_CODIGO
                                                 AND PC.CONF_EMPR = T.MARC_EMPR

                                                 AND T.MARC_FECHA BETWEEN PE.PERI_FEC_INI AND
                                                     SYSDATE) CONS,
                                             PER_EMPLEADO EMP
                                       WHERE CONS.MARC_EMPR = '||P_EMPRESA||'
                                         AND CONS.MARC_EMPR = EMP.EMPL_EMPRESA
                                         AND CONS.MARC_EMPLEADO = EMP.EMPL_LEGAJO
                                       GROUP BY MARC_FECHA,
                                                CONS.MARC_EVENTO,
                                                CONS.MARC_HORA,
                                                CONS.MARC_EMPLEADO,
                                                CONS.MARC_EMPLEADO,
                                                CONS.CONF_ENTRADA,
                                                CONS.CONF_SALIDA,
                                                HORA_RELOJ
                                       ORDER BY CONS.MARC_FECHA) CALC) MARCADOS
                       WHERE DIAS_HABILES.FECHA = MARCADOS.MARC_FECHA(+)
                         AND DIAS_HABILES.EMPL_LEGAJO = MARCADOS.MARC_EMPLEADO(+))
                         WHERE FECHA BETWEEN'''||P_FEC_INI||''' AND '''||P_FEC_FIN||'''
                         '||P_WHERE||'
                          and ESTADO_DIAS = ''S''
                         GROUP BY  FECHA, EMPL_LEGAJO, EMPLEADO, AREA, CARGO,NIVEL,SUCURSAL,DEPARTAMENTO,
                         MARC_FECHA,MARC_EMPLEADO,ENTRADA,SALIDA,ENTRADA_MENS,SALIDA_MENS,MARCADOR_ENTRADA,
                         MARCADOR_SALIDA,CUMPLE,TURNO,ESTADO_DIAS, FECHA_INGRESO, FECHA_SALIDA, TO_CHAR(FECHA,''D''),
                         CLAV_AREA,DPTO_CODIGO,EMPL_CONS_MARC_TAB, EMPL_MARC_ENTRADA,
                         EMPL_MARC_SALIDA,SUC_DESC,SALIDA,AREA1, AREA_Des1,
                         HORA_ENTRADA_ALMUERZO, --35
                          HORA_SALIDA_ALMUERZO,HORA_ENT,HORA_SAL,EMPL_FORMA_PAGO, EMPL_SITUACION,
                           EMPL_TIPO_cONT';

        END IF;

insert into x
  (campo1,  otro)
values
  (P_QUERY,  'PERL052_MARCACIONES');

                  IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => 'PERL052_MARCACIONES') THEN
                         APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => 'PERL052_MARCACIONES');
                   END IF;
                      APEX_COLLECTION.CREATE_COLLECTION_FROM_QUERY_B(P_COLLECTION_NAME => 'PERL052_MARCACIONES',
                                                                     P_QUERY           => P_QUERY);

 END PP_CREAR_CONSULTA_REGISTRO;

    PROCEDURE PP_CREAR_PUNTUALIDAD       (P_EMPRESA            IN NUMBER,
                                          P_FEC_INI            IN DATE,
                                          P_FEC_FIN            IN DATE,
                                          P_EMPLEADO           IN NUMBER,
                                          P_DEPARTAMENTO       IN NUMBER,
                                          P_SUCURSAL           IN NUMBER,
                                          P_AREA_ORG           IN NUMBER,
                                          P_JSA                IN VARCHAR2,
                                          P_SABADO             IN VARCHAR2,
                                          P_FORMA_PAGO         IN NUMBER,
                                          P_CARGO              IN NUMBER,
                                          P_EXCLUIR_PORT       IN VARCHAR2) IS





 P_QUERY VARCHAR2(32767);
 P_WHERE VARCHAR2(4000);
 P_QUERY2 VARCHAR2(32767);

 BEGIN

 IF P_FEC_INI IS NULL THEN
   RAISE_APPLICATION_ERROR(-20001, 'La fecha de inicio no puede quedar vacia');
 END IF;

 IF P_FEC_FIN IS NULL THEN
   RAISE_APPLICATION_ERROR(-20001, 'La fecha de fin no puede quedar vacia');
 END IF;

 IF P_EMPLEADO IS NOT NULL THEN
   P_WHERE := P_WHERE||'AND EMPL_LEGAJO = '||P_EMPLEADO||'';
 END IF;

 IF P_DEPARTAMENTO IS NOT NULL  AND P_EMPRESA = 2THEN
   P_WHERE := P_WHERE||'AND DPTO_CODIGO= '||P_DEPARTAMENTO||'';

   ELSIF  P_DEPARTAMENTO IS NOT NULL THEN
      P_WHERE := P_WHERE||'AND COD_DPTO= '||P_DEPARTAMENTO||'';
 END IF;

 IF P_SUCURSAL IS NOT NULL THEN
   P_WHERE := P_WHERE||'AND SUCURSAL= '||P_SUCURSAL||'';
 END IF;


 IF P_FORMA_PAGO IS NOT NULL THEN
   P_WHERE := P_WHERE||'AND EMPL_FORMA_PAGO= '||P_FORMA_PAGO||'';
 END IF;

IF P_CARGO IS NOT NULL THEN
   P_WHERE := P_WHERE||'AND CAR_CODIGO= '||P_CARGO||'';
 END IF;


 IF P_EXCLUIR_PORT = 1 THEN
   P_WHERE := P_WHERE||'AND CAR_CODIGO <> 15';
 END IF;

/*
 IF P_JSA = 'S' THEN
   P_WHERE := P_WHERE||'AND EMPL_CONS_MARC_TAB= '''||P_JSA||'''';
 END IF;
*/
 IF P_AREA_ORG IS NOT NULL AND P_EMPRESA = 2 THEN
   P_WHERE := P_WHERE||'AND AREA1= '||P_AREA_ORG||'';

 ELSIF P_AREA_ORG IS NOT NULL THEN
   P_WHERE := P_WHERE||'AND AREA_CLAVE= '||P_AREA_ORG||'';
 END IF;



 IF P_SABADO = 'S' THEN
  P_WHERE := P_WHERE||'AND TO_CHAR(FECHA_HOR,''D'') NOT IN (7)'||'';
 END IF;

if P_JSA is not null then
  P_WHERE := P_WHERE||'AND JSA_COD_AREA= '||P_JSA||'';
end if;

  IF P_EMPRESA = 1 THEN
     P_QUERY :=   'SELECT FECHA_HOR,
                          FECHA_HOR FILTRO_HORARIO,
                            EMPL_LEGAJO,
                            EMPLEADO,
                            NULL,
                            NULL,
                            AREA_CLAVE,
                            AREA,
                            CARGO,
                            NULL,
                            SUCURSAL,
                            DESC_SUCURSAL,--
                            NULL,
                            COD_DPTO,
                            DEPARTAMENTO,
                            EMPL_FORMA_PAGO,
                            DEPARTAMENTO2,
                            EMPL_SITUACION,
                            CAR_CODIGO CAR_CODIGO,
                            NULL,
                            SITUACION,
                            NULL,
                            CASE WHEN  NRO_DIA = 6 AND EMPL_FORMA_PAGO = 2 AND ENTRADA_HOR >= ''07:00'' THEN
                               ''07:00''
                               
                                   ELSE
                           ENTRADA_HOR
                           END ENTRADA_HOR,
                            CASE WHEN  NRO_DIA = 6 AND EMPL_FORMA_PAGO = 2 THEN
                               ''12:00''
                               ELSE
                                 SALIDA_HOR
                            END SALIDA_HOR ,
                           SALIDA_ALM ,
                           CASE WHEN ENTRADA_HOR >= ''11:00'' THEN 
                                ENTRADA_HOR
                             ELSE
                                ENTR_ALM 
                               END ENTR_ALM ,
                           NULL,
                           NULL,
                           NULL,
                           --NULL,*/
                           CASE WHEN OPCION = ''S'' AND NRO_DIA = 6 AND min(ENTRADA_SAB) IS NOT NULL AND EMPL_FORMA_PAGO = 2   THEN
                                 TO_CHAR( min(ENTRADA_SAB),''HH24:MI'')
                                 WHEN OPCION = ''S'' AND NRO_DIA = 6 AND min(ENTRADA_SAB) IS NULL AND EMPL_FORMA_PAGO = 2 THEN
                                   ''SIN MARC.''
                                WHEN OPCION <> ''S'' AND NRO_DIA = 6 AND min(ENTRADA_SAB) IS NOT NULL AND EMPL_FORMA_PAGO = 2 THEN
                                   TO_CHAR( min(ENTRADA_SAB),''HH24:MI'')
                                WHEN OPCION = ''S''  AND min(ENTRADA) IS NULL AND TO_CHAR(SYSDATE,''HH24:MI'') > ENTRADA_HOR  THEN
                                     ''SIN MARC.''
                                WHEN OPCION <> ''S'' AND min(ENTRADA) IS NULL THEN
                                      DETALLE
                               WHEN OPCION = ''S'' AND NRO_DIA <> 6 AND min(ENTRADA) IS NOT NULL AND EMPL_FORMA_PAGO = 2 and ENTRADA_HOR >= ''11:00''  THEN
                                 null 
                                ELSE
                                TO_CHAR(min(ENTRADA),''HH24:MI'')
                           END  ENTRADA,

                            CASE WHEN OPCION = ''S'' AND NRO_DIA = 6 AND MAX(SALIDA_SAB) IS NOT NULL AND EMPL_FORMA_PAGO = 2 /*AND TO_CHAR(SYSDATE,''HH24:MI'') > SALIDA_HOR */ THEN
                                      TO_CHAR( MAX(SALIDA_SAB),''HH24:MI'')
                                WHEN OPCION = ''S'' AND NRO_DIA = 6 AND MAX(SALIDA_SAB) IS NULL AND EMPL_FORMA_PAGO = 2 /*AND TO_CHAR(SYSDATE,''HH24:MI'') > SALIDA_HOR */ THEN
                                     ''SIN MARC.''
                                WHEN OPCION <> ''S'' AND NRO_DIA = 6 AND MAX(SALIDA_SAB) IS NOT NULL AND EMPL_FORMA_PAGO = 2  AND TO_CHAR(SYSDATE,''HH24:MI'') > SALIDA_HOR  THEN
                                   TO_CHAR( MAX(SALIDA_SAB),''HH24:MI'')
                                 WHEN OPCION = ''S'' AND MAX(SALIDA) IS NULL AND TO_CHAR(SYSDATE,''HH24:MI'') > SALIDA_HOR  THEN
                                      ''SIN MARC.''
                                 WHEN OPCION <> ''S'' AND MAX(SALIDA) IS NULL THEN
                                      DETALLE
                                 ELSE
                                      TO_CHAR(MAX(SALIDA),''HH24:MI'')
                           END  SALIDA,


                            CASE WHEN NRO_DIA = 6 AND EMPL_FORMA_PAGO = 2 and OPCION <> ''S'' THEN
                                      DETALLE
                                 WHEN NRO_DIA = 6 AND EMPL_FORMA_PAGO = 2 THEN
                                     null
                                 WHEN OPCION <> ''S''  AND MAX(SALIDA) IS NULL THEN
                                      DETALLE
                                 WHEN OPCION = ''S'' AND MAX(SALIDA_ALMUERZO) IS NULL AND SALIDA_ALM IS NOT NULL AND TO_CHAR(SYSDATE,''HH24:MI'') > SALIDA_ALM  THEN
                                 ''SIN MARC.''
                             ELSE
                                 TO_CHAR(MAX(SALIDA_ALMUERZO),''HH24:MI'')
                             END  SALIDA_ALMUERZO,

                            CASE WHEN NRO_DIA = 6 AND EMPL_FORMA_PAGO = 2 and OPCION <> ''S'' THEN
                                      DETALLE
                                 WHEN NRO_DIA = 6 AND EMPL_FORMA_PAGO = 2 THEN
                                      NULL
                              WHEN OPCION <> ''S'' AND MAX(ENTRADA_ALMUERZO)  IS NULL THEN
                                      DETALLE
                               WHEN OPCION = ''S'' AND MAX(ENTRADA_ALMUERZO) IS NULL AND ENTR_ALM IS NOT NULL  AND TO_CHAR(SYSDATE,''HH24:MI'') > ENTR_ALM THEN
                                 ''SIN MARC.''
                              WHEN NRO_DIA <> 6 and  ENTRADA_HOR >= ''11:00'' THEN
                                   TO_CHAR(min(ENTRADA),''HH24:MI'')
                                    
                             ELSE
                                 TO_CHAR(MAX(ENTRADA_ALMUERZO),''HH24:MI'')
                           END  ENTRADA_ALMUERZO,
                        JSA_COD_AREA,
                        JSA_DESC_AREA,
                        OPCION,
                        DETALLE,
                        NULL,
                       to_char(FECHA_HOR,''iw'') semana,
                       to_char(FECHA_HOR,''DAY'') DIA,
                        ''Semana: ''||to_char(FECHA_HOR,''iw'')||'' del ''||TRUNC(FECHA_HOR, ''iw'')||'' al ''||TRUNC(TRUNC(FECHA_HOR, ''iw'') + 7 - 1/86400) semana_Des--41
                    FROM (
                     SELECT FECHA_HOR,
                            EMPL_LEGAJO,
                            EMPLEADO,
                            AREA,
                            AREA_CLAVE,
                            CARGO,
                            SUCURSAL,
                            DESC_SUCURSAL,
                            DEPARTAMENTO,
                            COD_DPTO,
                            EMPL_FORMA_PAGO,
                            DEPARTAMENTO2,
                            EMPL_SITUACION,
                            SITUACION,
                            ALMUERZO,
                            JSA_COD_AREA,
                            JSA_DESC_AREA,
                            EMPL_EMPRESA,
                            OPCION,
                            CASE
                               WHEN OPCION = ''D'' THEN ''DOMINGO''
                               WHEN OPCION = ''H'' THEN ''SABADO''
                               WHEN OPCION = ''X'' THEN ''FICHA INACTIVA''
                               WHEN OPCION = ''F'' THEN ''FERIADO''
                               WHEN OPCION = ''V'' THEN ''VAC. R .LEG''
                               WHEN OPCION = ''T'' THEN ''VAC. R. INT''
                               WHEN OPCION = ''L'' THEN ''LICENCIA''
                               WHEN OPCION = ''R'' THEN ''REPOSO''
                               WHEN OPCION = ''P'' THEN ''PERMISO''
                               WHEN OPCION = ''A'' THEN ''AUSENCIA JUST.''
                               WHEN OPCION = ''G'' THEN ''VIAJE LABORAL''
                               WHEN OPCION = ''U'' THEN ''SUSPENCION''
                               WHEN OPCION = ''S'' THEN ''PRESENTE''
                               END DETALLE,
                           CASE  WHEN    EVENTO = ''E'' AND HORA BETWEEN COMP_ENTRADA_MENOS AND COMP_ENTRADA_MAS THEN
                              HORA_RELOJ
                            END ENTRADA,
                            CASE WHEN   EVENTO = ''S'' AND HORA BETWEEN COMP_SALIDA_MENOS AND COMP_SALIDA_MAS THEN
                              HORA_RELOJ
                            END SALIDA,
                            CASE WHEN   EVENTO = ''S'' AND HORA BETWEEN COMP_SAL_ALM_MENOS AND COMP_SAL_ALM_MAS THEN
                              HORA_RELOJ
                            END SALIDA_ALMUERZO,
                            CASE WHEN   EVENTO = ''E'' AND HORA BETWEEN COMP_ENT_ALM_MENOS AND COMP_ENT_ALM_MAS THEN
                              HORA_RELOJ
                            END ENTRADA_ALMUERZO,


                            ------***

                            CASE  WHEN   EVENTO = ''E'' AND NRO_DIA = ''6'' AND EMPL_FORMA_PAGO = 2 AND HORA BETWEEN ''05:00'' AND ''23:00'' THEN
                              HORA_RELOJ
                            END ENTRADA_SAB,
                            CASE WHEN   EVENTO = ''S'' AND NRO_DIA = ''6'' AND EMPL_FORMA_PAGO = 2  AND HORA BETWEEN ''09:00''  AND ''14:00'' THEN
                              HORA_RELOJ
                            END SALIDA_SAB,
                            ---------***
                           HORA_RELOJ,
                           COMP_SALIDA_MAS,
                           COMP_SALIDA_MENOS,
                           EVENTO,
                           ENTRADA_HOR,
                           SALIDA_HOR,
                           SALIDA_ALM,
                           ENTR_ALM,
                           NRO_DIA,
                           CAR_CODIGO
                      FROM
                            (SELECT DIAS.FECHA FECHA_HOR,
                                   P.EMPL_LEGAJO,
                                   P.EMPL_LEGAJO || '' - '' || P.EMPL_NOMBRE || '' '' || P.EMPL_APE EMPLEADO,
                                  -------------------------------horario comparativo
                                    CASE
                                     WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA THEN
                                      TO_CHAR((TUR.TUREMP_ENT- 6/24), ''HH24:MI:SS'')
                                   END COMP_ENTRADA_MENOS,
                                   CASE
                                     WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA THEN
                                       case when to_char(TUR.TUREMP_ENT,''HH24:MI:SS'') >= ''20:00'' then
                                         ''23:59''
                                        else
                                         TO_CHAR((TUR.TUREMP_ENT+ 5/24), ''HH24:MI:SS'')
                                         end 
                                   END COMP_ENTRADA_MAS,
                                   CASE
                                     WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA THEN
                                     TO_CHAR(TUR.TUREMP_SAL-3/24,''HH24:MI:SS'')
                                   END COMP_SALIDA_MENOS,
                                    CASE
                                     WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA THEN
                                      CASE WHEN TO_CHAR(TUREMP_SAL, ''HH24:MI'') BETWEEN ''17:00'' AND ''23:00'' THEN
                                           ''23:59''
                                         ELSE
                                        TO_CHAR(TUR.TUREMP_SAL +6/24, ''HH24:MI:SS'')
                                        END
                                   END COMP_SALIDA_MAS,
                                   CASE
                                     WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA AND
                                          P.EMPL_MARC_ALMUERZO = ''S'' THEN
                                      TO_CHAR(TUREMP_SAL_ALMUERZO- 3/24, ''HH24:MI:SS'')
                                   END COMP_SAL_ALM_MENOS,
                                    CASE
                                     WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA AND
                                          P.EMPL_MARC_ALMUERZO = ''S'' THEN
                                      TO_CHAR(TUREMP_ent_ALMUERZO+ 3/24, ''HH24:MI:SS'')
                                   END COMP_SAL_ALM_MAS,
                                   CASE
                                     WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA AND
                                          P.EMPL_MARC_ALMUERZO = ''S'' THEN
                                      TO_CHAR(TUREMP_ENT_ALMUERZO-2/24, ''HH24:MI:SS'')
                                   END COMP_ENT_ALM_MENOS,
                                    CASE
                                     WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA AND
                                          P.EMPL_MARC_ALMUERZO = ''S'' THEN
                                      TO_CHAR(TUREMP_ENT_ALMUERZO+2/24, ''HH24:MI:SS'')
                                   END COMP_ENT_ALM_MAS,
                                -------------------------------------------------------------HORARIO NORMAL

                                  CASE
                                     WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA THEN
                                      TO_CHAR((TUR.TUREMP_ENT), ''HH24:MI'')
                                   END ENTRADA_HOR,
                                   CASE
                                     WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA THEN
                                      TO_CHAR(TUR.TUREMP_SAL, ''HH24:MI'')
                                   END SALIDA_HOR,
                                   CASE
                                     WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA AND
                                          P.EMPL_MARC_ALMUERZO = ''S'' THEN
                                      TO_CHAR(TUREMP_SAL_ALMUERZO, ''HH24:MI'')
                                   END SALIDA_ALM,
                                   CASE
                                     WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA AND
                                          P.EMPL_MARC_ALMUERZO = ''S'' THEN
                                      TO_CHAR(TUREMP_ENT_ALMUERZO, ''HH24:MI'')
                                   END ENTR_ALM,
                                   P.EMPL_FEC_INGRESO,
                                   P.EMPL_FEC_SALIDA,
                                   AREA.AREDPP_CLAVE || '' - '' || AREA.AREDPP_DESC AREA,
                                   AREDPP_CLAVE AREA_CLAVE,
                                   CAR.CAR_CODIGO || '' - '' || CAR.CAR_DESC CARGO,
                                   NIV.NIVORG_DESC NIVEL,
                                   P.EMPL_SUCURSAL SUCURSAL,
                                   SUC_CODIGO || '' - '' || SUC_DESC DESC_SUCURSAL,
                                   DEP.DPTO_DESC DEPARTAMENTO,
                                   DEP.DPTO_CODIGO COD_DPTO,
                                   EMPL_FORMA_PAGO,
                                   CASE
                                     WHEN DPTO_SUC = 2 THEN
                                      ''CDA''
                                     WHEN DPTO_CODIGO = 1 THEN
                                      ''ADMINISTRATIVO''
                                     WHEN (DPTO_CODIGO IN (14, 2) OR DPTO_SUC NOT IN (1, 2)) THEN
                                      ''COMERCIAL''
                                     WHEN DPTO_CODIGO = 22 THEN
                                      ''LOGISTICA''
                                     ELSE
                                      ''INDUSTRIAL''
                                   END DEPARTAMENTO2,
                                   EMPL_SITUACION,
                                   DECODE(EMPL_SITUACION, ''I'', ''INACTIVO'', ''A'', ''ACTIVO'', NULL) SITUACION,
                                   NVL(EMPL_MARC_ALMUERZO, ''N'') ALMUERZO,
                                  -- AREDPP_CLAVE JSA_COD_AREA,
                                  -- AREDPP_DESC JSA_DESC_AREA,

                                   CASE
                                         WHEN AREDPP_CLAVE = 1 THEN
                                          2
                                         WHEN AREDPP_CLAVE IN (2,6) THEN
                                         1
                                         WHEN AREDPP_CLAVE = 5 THEN
                                          5
                                         WHEN AREDPP_CLAVE = 4 THEN
                                         3
                                         WHEN AREDPP_CLAVE = 3 THEN
                                          4
                                        END JSA_COD_AREA,
                                        CASE
                                         WHEN AREDPP_CLAVE = 1 THEN
                                          ''ADM''
                                         WHEN AREDPP_CLAVE IN (2,6)   THEN
                                          ''FIN E INFO''
                                         WHEN AREDPP_CLAVE = 5 THEN
                                          ''INDUSTRIAL''
                                         WHEN AREDPP_CLAVE = 4  THEN
                                          ''CDA''
                                         WHEN AREDPP_CLAVE = 3  THEN
                                          ''COMERCIAL''
                                       END JSA_DESC_AREA,
                                   EMPL_EMPRESA,
                                   TO_CHAR(DIAS.FECHA,''D'') NRO_DIA,
                                   PER_VER_DIA_LABORAL(P.EMPL_LEGAJO,
                                                       1,
                                                       DIAS.FECHA,
                                                       P.EMPL_FEC_INGRESO,
                                                       P.EMPL_FEC_SALIDA,
                                                       DECODE(EMPL_FORMA_PAGO, 1, ''J'', 2, ''M'')) OPCION,
                                   CAR_CODIGO

                              FROM PER_EMPLEADO P,
                                   (SELECT I_DIA FECHA
                                      FROM TABLE(RETURN_TABLE_DIA_PROF(TO_NUMBER(TO_CHAR(SYSDATE, ''YYYY''))))) DIAS,
                                   PER_AREA_DPP AREA,
                                   PER_CARGO_DPP CARDPP,
                                   PER_CARGO CAR,
                                   PER_NIVEL_ORGANI NIV,
                                   GEN_DEPARTAMENTO DEP,
                                   GEN_SUCURSAL SUC,
                                   PER_TURNOS_EMPL TUR
                             WHERE P.EMPL_EMPRESA = 1
                               AND P.EMPL_EMPRESA = DEP.DPTO_EMPR(+)
                               AND P.EMPL_DEPARTAMENTO = DEP.DPTO_CODIGO(+)
                               AND P.EMPL_SUCURSAL = DEP.DPTO_SUC(+)
                               AND P.EMPL_SUCURSAL = SUC_CODIGO(+)
                               AND P.EMPL_EMPRESA = SUC_EMPR(+)
                               AND EMPL_FEC_INGRESO IS NOT NULL
                               AND EMPL_CODIGO_PROV <> 0
                               AND EMPL_FORMA_PAGO <> 0
                               AND EMPL_SITUACION IS NOT NULL
                               AND P.EMPL_CONS_MARC = ''S''
                               AND TUR.TUREMP_EMPR = EMPL_EMPRESA
                               AND TUR.TUREMP_EMPLEADO = EMPL_LEGAJO
                               AND P.EMPL_SITUACION = ''A''
                               AND DIAS.FECHA BETWEEN SYSDATE - 100 AND SYSDATE
                               AND DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA
                               AND P.EMPL_EMPRESA = AREA.AREDPP_EMPR(+)
                               AND P.EMPL_AREA_ORGANI = AREA.AREDPP_CLAVE(+)
                               AND P.EMPL_EMPRESA = CARDPP.CARDPP_EMPR(+)
                               AND P.EMPL_CARGO = CARDPP.CARDPP_CLAVE(+)
                               AND CARDPP.CARDPP_EMPR = CAR.CAR_EMPR(+)
                               AND CARDPP.CARDPP_CLAVE = CAR.CAR_CODIGO(+)
                               AND CARDPP.CARDPP_EMPR = NIV.NIVORG_EMPR(+)
                               AND CARDPP.CARDPP_NIV_ORGANI = NIV.NIVORG_CLAVE(+)
                              /* AND EMPL_LEGAJO  IN(104021, 20654,481,104218,104216,104217)
                               AND DIAS.FECHA IN( ''24/04/2021'',''26/04/2021'') */
                               ) HOR,
                            (SELECT T.MARC_FECHA,
                                    T.MARC_EVENTO EVENTO,
                                    T.MARC_EMPLEADO,
                                    T.MARC_EMPR,
                                    T.HORA_RELOJ,
                                    TO_CHAR(T.HORA_RELOJ, ''HH24:MI:SS'') HORA,
                                    CASE
                                      WHEN  MARC_EVENTO = ''S'' AND  to_date(to_char(HORA_RELOJ, ''hh24:mi:ss''), ''hh24:mi:ss'') between to_date(''00:00:00'', ''hh24:mi:ss'') and to_date(''01:00:00'', ''hh24:mi:ss'') THEN
                                            MARC_FECHA-1
                                      WHEN  MARC_EVENTO = ''E'' AND  to_date(to_char(HORA_RELOJ, ''hh24:mi:ss''), ''hh24:mi:ss'') between to_date(''00:00:00'', ''hh24:mi:ss'') and to_date(''01:00:00'', ''hh24:mi:ss'') THEN
                                          MARC_FECHA+1
                                      ELSE
                                         MARC_FECHA
                                      END FECHA_HORARIO
                          FROM PER_MARCACION_DIARIA_V T
                         WHERE T.MARC_FECHA BETWEEN SYSDATE-100 AND SYSDATE
                           /*AND MARC_FECHA    IN( ''24/04/2021'',''26/04/2021'')
                           AND MARC_EMPLEADO   IN(104021, 20654,481,104218,104216,104217)*/
                         ) MAR

                       WHERE HOR.EMPL_LEGAJO   = MAR.MARC_EMPLEADO (+)
                         AND HOR.EMPL_EMPRESA  = MAR.MARC_EMPR (+)
                         AND HOR.FECHA_HOR     = MARC_FECHA(+)
                         AND EMPL_EMPRESA = 1
                         AND HOR.FECHA_HOR BETWEEN '''||P_FEC_INI||''' AND '''||P_FEC_FIN||'''
                          '||P_WHERE||'

                        /* AND HOR.FECHA_HOR    IN( ''24/04/2021'',''26/04/2021'')
                         AND HOR.EMPL_LEGAJO   IN(104021, 20654,481,104218,104216,104217)*/
      )
                         GROUP BY FECHA_HOR,
                            EMPL_LEGAJO,
                            EMPLEADO,
                            AREA,
                            AREA_CLAVE,
                            CARGO,
                            SUCURSAL,
                            DESC_SUCURSAL,
                            DEPARTAMENTO,
                            COD_DPTO,
                            EMPL_FORMA_PAGO,
                            DEPARTAMENTO2,
                            EMPL_SITUACION,
                            SITUACION,
                            ALMUERZO,
                            JSA_COD_AREA,
                            JSA_DESC_AREA,
                            EMPL_EMPRESA,
                            OPCION,
                            DETALLE,
                            ENTRADA_HOR,
                            SALIDA_HOR,
                            SALIDA_ALM,
                            ENTR_ALM,
                            NRO_DIA,
                            CAR_CODIGO
                            ORDER BY FECHA_HOR,EMPL_LEGAJO ';
  P_QUERY2:= 'SELECT FECHA_HORARIO, FECHA_HOR FECHA_FILTRO, EMPL_LEGAJO, EMPLEADO,
          EMPL_FEC_INGRESO, EMPL_FEC_SALIDA,AREA_CLAVE,  AREA,
         CARGO, NIVEL, SUCURSAL,  DESC_SUCURSAL,   DEPARTAMENTO, COD_DPTO, EMPL_FORMA_PAGO,
         DEPARTAMENTO2, EMPL_SITUACION,  A_CAR_CODIGO, FORMA_PAGO,SITUACION,
         MARC_FECHA,
        /*  CASE WHEN A_CAR_CODIGO = 7 THEN
          CASE
             WHEN TO_CHAR(MIN(ENTRADA),''HH24:MI:SS'')  > ''05:00:00'' AND TO_CHAR(MIN(ENTRADA),''HH24:MI:SS'') < ''11:00''  AND NVL(OPCION,''S'') = ''S''   THEN
                  ''06:00:00''
             WHEN TO_CHAR(MIN(ENTRADA),''HH24:MI:SS'')  > ''12:00'' AND NVL(OPCION,''S'') = ''S'' THEN
               ''13:00:00''
              ELSE
                 ''06:00''
             END
         ELSE

         ENTRADA_HOR
        END */ null ENTRADA_HOR,

         null SALIDA_HOR, null SALIDA_ALM, null ENTR_ALM,  MIN(ENTRADA) ENTRADA,    MAX(SALIDA)  SALIDA,
         MIN(HORARIO_SAL_ALMUERZO) SALIDA_ALMUERZO,
         MAX(HORARIO_ENT_ALMUERZO) ENTRADA_ALMUERZO,
           CASE WHEN NVL(OPCION,''S'') = ''S'' THEN
         TO_CHAR(MIN(ENTRADA),''HH24:MI:SS'')
         ELSE
           OPCION2
         END  ENTRADA_HORA, --C030
         CASE WHEN NVL(OPCION,''S'') = ''S'' THEN
         TO_CHAR(MAX(SALIDA),''HH24:MI:SS'')
         ELSE
           OPCION2
         END SALIDA_HORA,--c031
         CASE WHEN NVL(OPCION,''S'') = ''S'' THEN
         TO_CHAR(MIN(HORARIO_SAL_ALMUERZO),''HH24:MI:SS'')
         ELSE
           OPCION2
          END SALIDA_ALMUERZO_HORA,--c032
          CASE WHEN NVL(OPCION,''S'') = ''S'' THEN
         TO_CHAR(MAX(HORARIO_ENT_ALMUERZO),''HH24:MI:SS'')
         ELSE
           OPCION2
           END ENTRADA_ALMUERZO_HORA,--c033
         JSA_COD_AREA,
         JSA_DESC_AREA,
         OPCION,
         CASE
           WHEN OPCION = ''D'' THEN  ''DOMINGO''
           WHEN OPCION = ''H'' THEN  ''SABADO''
           WHEN OPCION = ''X'' THEN   ''FICHA INACTIVA''
           WHEN OPCION = ''F'' THEN  ''FERIADO''
           WHEN OPCION = ''V'' THEN       ''VAC. REG. LEGAL''
           WHEN OPCION = ''T'' THEN    ''VAC. REG. INTERNO''
           WHEN OPCION = ''L'' THEN  ''LICENCIA''
           WHEN OPCION = ''R'' THEN  ''REPOSO''
           WHEN OPCION = ''P'' THEN      ''PRERMISO''
           WHEN OPCION = ''A'' THEN   ''AUSENCIA JUSTIFICADA''
           WHEN OPCION = ''G'' THEN     ''VIAJE LABORAL''
           WHEN OPCION = ''U'' THEN ''SUSPENCION''
           WHEN OPCION = ''S'' AND MIN(ENTRADA) IS NULL AND MAX(SALIDA) IS NULL THEN
             ''AUSENTE''
           ELSE
               ''PRESENTE''
           END DETALLE,
           case when A_CAR_CODIGO = 7 then
          CASE
             WHEN TO_CHAR(MIN(ENTRADA),''HH24:MI:SS'')  > ''06:00'' AND TO_CHAR(MIN(ENTRADA),''HH24:MI:SS'') < ''11:00''   AND NVL(OPCION,''S'') = ''S''
               or TO_CHAR(MIN(ENTRADA),''HH24:MI:SS'')  > ''13:00'' AND NVL(OPCION,''S'') = ''S'' THEN
               ''IMPUNTUAL''
             WHEN TO_CHAR(MIN(ENTRADA),''HH24:MI:SS'') <  ''06:00''  AND TO_CHAR(MIN(ENTRADA),''HH24:MI:SS'') < ''11:00'' AND NVL(OPCION,''S'') = ''S''
               or  TO_CHAR(MIN(ENTRADA),''HH24:MI:SS'') <  ''13:00''  AND NVL(OPCION,''S'') = ''S'' THEN
               ''PUNTUAL''
           END
        else
           /*CASE
             WHEN TO_CHAR(MIN(ENTRADA),''HH24:MI:SS'')  > NVL(ENTRADA_HOR,''07:00'') AND NVL(OPCION,''S'') = ''S'' THEN
               ''IMPUNTUAL''
             WHEN TO_CHAR(MIN(ENTRADA),''HH24:MI:SS'') < NVL(ENTRADA_HOR,''07:00'')  AND NVL(OPCION,''S'') = ''S'' THEN
               ''PUNTUAL''
           END*/ null

        end  PUNTUALIDAD

  FROM(
 SELECT FECHA_HOR, EMPL_LEGAJO,  EMPL.EMPLEADO,  ENTRADA_HOR, SALIDA_HOR, SALIDA_ALM,
        ENTR_ALM,  TURNO,   EMPL_FEC_INGRESO,   EMPL_FEC_SALIDA,      nvl(MAR.A_COD_ORGANIGRAMA,JSA_COD_AREA) AREA_CLAVE,
        nvl(a_area,jsa_Desc_Area) AREA,   EMPL.CARGO CARGO,  NIVEL,    SUCURSAL SUCURSAL,  DESC_SUCURSAL,
        A_COD_DPTO DEPARTAMENTO,
        COD_DPTO, A_FORMA_PAGO EMPL_FORMA_PAGO,   DEPARTAMENTO2, A_SITUCION EMPL_SITUACION,
        A_CAR_CODIGO,   FORMA_PAGO, SITUACION,ALMUERZO,
        MAR.MARC_FECHA,
        MAR.MARC_EVENTO,
        MAR.MARC_HORA,
        MAR.MARC_EMPLEADO,
        MAR.MARC_EMPR,
        MAR.HORA_RELOJ,
       FECHA_HOR FECHA_HORARIO, --MAR.FECHA_HORARIO,
       CASE
          WHEN  MARC_EVENTO = ''E'' AND IND = ''N'' THEN
              MAR.HORA_RELOJ
         END ENTRADA,
       CASE
        WHEN  MARC_EVENTO = ''S'' AND IND = ''N'' THEN
              MAR.HORA_RELOJ
         END SALIDA,

       HORARIO_SAL_ALMUERZO,
       HORARIO_ENT_ALMUERZO,
       JSA_COD_AREA,
       JSA_DESC_AREA,
       OPCION,
       CASE
           WHEN OPCION = ''D'' THEN  ''DOMINGO''
           WHEN OPCION = ''H'' THEN  ''SABADO''
           WHEN OPCION = ''X'' THEN   ''FICHA INACTIVA''
           WHEN OPCION = ''F'' THEN  ''FERIADO''
           WHEN OPCION = ''V'' THEN       ''VAC. REG. LEGAL''
           WHEN OPCION = ''T'' THEN    ''VAC. REG. INTERNO''
           WHEN OPCION = ''L'' THEN  ''LICENCIA''
           WHEN OPCION = ''R'' THEN  ''REPOSO''
           WHEN OPCION = ''P'' THEN      ''PRERMISO''
           WHEN OPCION = ''A'' THEN   ''AUSENCIA JUSTIFICADA''
           WHEN OPCION = ''G'' THEN     ''VIAJE LABORAL''
           WHEN OPCION = ''U'' THEN ''SUSPENCION''
           WHEN OPCION = ''S''  THEN
             ''PRESENTE''
           ELSE
               ''AUSENTE''
           END opcion2
FROM
  (SELECT DIAS.FECHA FECHA_HOR ,
               P.EMPL_LEGAJO,
               P.EMPL_LEGAJO || '' - '' || P.EMPL_NOMBRE || '' '' || P.EMPL_APE EMPLEADO,
               CASE
                 WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA THEN
                      TO_CHAR(TUR_DESDE, ''HH24:MI:SS'')
                 END ENTRADA_HOR,
                CASE
                WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA THEN
                      TO_CHAR(TUR_HASTA, ''HH24:MI:SS'')
                 END SALIDA_HOR,
               CASE
                 WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA AND P.EMPL_MARC_ALMUERZO = ''S'' THEN
                      TO_CHAR(TUREMP_SAL_ALMUERZO, ''HH24:MI:SS'')
                 END SALIDA_ALM,
               CASE
                WHEN DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA AND P.EMPL_MARC_ALMUERZO = ''S'' THEN
                      TO_CHAR(TUREMP_ENT_ALMUERZO, ''HH24:MI:SS'')
                 END ENTR_ALM,
                TUREMP_TURNO TURNO,
               P.EMPL_FEC_INGRESO,
               P.EMPL_FEC_SALIDA,
               AREA.AREDPP_CLAVE || '' - '' || AREA.AREDPP_DESC AREA,
               AREDPP_CLAVE AREA_CLAVE,
               CAR.CAR_CODIGO || '' - '' || CAR.CAR_DESC CARGO,
               NIV.NIVORG_DESC NIVEL,
               P.EMPL_SUCURSAL SUCURSAL,
               SUC_CODIGO|| '' - '' ||SUC_DESC DESC_SUCURSAL,
               DEP.DPTO_DESC DEPARTAMENTO,
               DEP.DPTO_CODIGO COD_DPTO,
               EMPL_FORMA_PAGO,
               CASE
                WHEN DPTO_SUC = 2 THEN
                     ''CDA''
                WHEN DPTO_CODIGO = 1 THEN
                     ''ADMINISTRATIVO''
                WHEN (DPTO_CODIGO IN(14,2) OR DPTO_SUC NOT IN (1,2)) THEN
                    ''COMERCIAL''
                WHEN DPTO_CODIGO = 22 THEN
                    ''LOGISTICA''
                ELSE
                   ''INDUSTRIAL''
                END DEPARTAMENTO2,
                EMPL_SITUACION,
                EMPL_TIPO_CONT,
                DECODE(EMPL_FORMA_PAGO,1,''JORNAL'',2,''MENSUAL'',3, ''SERVICIOS PERSONALES'', 4,''TEMPORAL'',5,''AMH'') FORMA_PAGO,
                DECODE (EMPL_SITUACION,''I'',''INACTIVO'',''A'',''ACTIVO'',NULL) SITUACION,
                nvl(EMPL_MARC_ALMUERZO,''N'') ALMUERZO,

               AREDPP_CLAVE JSA_COD_AREA,
               AREDPP_desc JSA_DESC_AREA,
             PER_VER_DIA_LABORAL(P.EMPL_LEGAJO,
                                 1,
                                 DIAS.FECHA,
                                 P.EMPL_FEC_INGRESO,
                                 P.EMPL_FEC_SALIDA,
                                 DECODE(EMPL_FORMA_PAGO,1,''J'',2,''M'')) OPCION
          from PER_CONFIGURACION PCON,
               PER_PERIODO PPER,
               (SELECT I_DIA FECHA
                  FROM table(return_table_dia_prof(TO_NUMBER(to_char(SYSDATE,
                                                                     ''YYYY''))))) DIAS,
               PER_EMPLEADO P,
               PER_AREA_DPP AREA,
               PER_CARGO_DPP CARDPP,
               PER_CARGO CAR,
               PER_NIVEL_ORGANI NIV,
               GEN_DEPARTAMENTO DEP,
               GEN_SUCURSAL SUC,
               PER_TURNOS_EMPL TUR,
               PER_TURNOS M
         WHERE P.EMPL_EMPRESA = 1
           AND P.EMPL_EMPRESA = DEP.DPTO_EMPR(+)
           AND P.EMPL_DEPARTAMENTO = DEP.DPTO_CODIGO(+)
           AND P.EMPL_SUCURSAL = DEP.DPTO_SUC(+)
           AND P.EMPL_SUCURSAL = SUC_CODIGO(+)
           AND P.EMPL_EMPRESA = SUC_EMPR(+)
           AND EMPL_FEC_INGRESO IS NOT NULL
           AND EMPL_CODIGO_PROV<>0
           AND EMPL_FORMA_PAGO <> 0
           AND EMPL_SITUACION IS NOT NULL
           AND P.EMPL_CONS_MARC = ''S''
           AND TUR.TUREMP_TURNO = M.TUR_CODIGO(+)
           AND TUR.TUREMP_EMPR = M.TUR_EMPR(+)
           AND TUR.TUREMP_EMPR(+) =EMPL_EMPRESA
           AND TUR.TUREMP_EMPLEADO(+) = EMPL_LEGAJO
        --   AND DIAS.FECHA BETWEEN TUR.TUREMP_DESDE AND TUR.TUREMP_HASTA
           AND PCON.CONF_EMPR = P.EMPL_EMPRESA
           AND P.EMPL_SITUACION = ''A''
           AND PCON.CONF_EMPR = PPER.PERI_EMPR
           AND (PCON.CONF_PERI_ACT - 3) = PPER.PERI_CODIGO
           AND DIAS.FECHA >=''01/06/2020''
           AND DIAS.FECHA BETWEEN PPER.PERI_FEC_INI AND SYSDATE
           AND P.EMPL_EMPRESA = AREA.AREDPP_EMPR(+)
           AND P.EMPL_AREA_ORGANI = AREA.AREDPP_CLAVE(+)
           AND P.EMPL_EMPRESA = CARDPP.CARDPP_EMPR(+)
           AND P.EMPL_CARGO = CARDPP.CARDPP_CLAVE(+)
           AND CARDPP.CARDPP_EMPR = CAR.CAR_EMPR(+)
           AND CARDPP.CARDPP_CLAVE = CAR.CAR_CODIGO(+)
           AND CARDPP.CARDPP_EMPR = NIV.NIVORG_EMPR(+)
           AND CARDPP.CARDPP_NIV_ORGANI = NIV.NIVORG_CLAVE(+)) EMPL,
           ( SELECT T.MARC_FECHA,
                       T.MARC_EVENTO,
                       T.MARC_HORA,
                       T.MARC_EMPLEADO,
                       T.MARC_EMPR,
                       T.HORA_RELOJ,
                      CASE
                        WHEN  MARC_EVENTO = ''S'' AND  to_date(to_char(HORA_RELOJ, ''hh24:mi:ss''), ''hh24:mi:ss'') between to_date(''00:00:00'', ''hh24:mi:ss'') and to_date(''01:00:00'', ''hh24:mi:ss'') THEN
                              MARC_FECHA-1
                        WHEN  MARC_EVENTO = ''E'' AND  to_date(to_char(HORA_RELOJ, ''hh24:mi:ss''), ''hh24:mi:ss'') between to_date(''00:00:00'', ''hh24:mi:ss'') and to_date(''01:00:00'', ''hh24:mi:ss'') THEN
                            MARC_FECHA+1
                        ELSE
                           MARC_FECHA
                       END FECHA_HORARIO,
                   CASE
                      WHEN  MARC_EVENTO = ''S'' AND  EMPL_MARC_ALMUERZO = ''S'' AND to_date(to_char(HORA_RELOJ, ''hh24:mi:ss''), ''hh24:mi:ss'') between to_date(''11:00:00'', ''hh24:mi:ss'') and to_date(''13:30:00'', ''hh24:mi:ss'') AND CAR_CODIGO <>7 THEN
                            HORA_RELOJ
                       END HORARIO_SAL_ALMUERZO,
                    CASE
                       WHEN  MARC_EVENTO = ''E'' AND  EMPL_MARC_ALMUERZO = ''S'' AND to_date(to_char(HORA_RELOJ, ''hh24:mi:ss''), ''hh24:mi:ss'') between to_date(''11:00:00'', ''hh24:mi:ss'') and to_date(''13:30:00'', ''hh24:mi:ss'') AND CAR_CODIGO <>7 THEN
                             HORA_RELOJ
                       END HORARIO_ENT_ALMUERZO,
                   CASE
                      WHEN  EMPL_MARC_ALMUERZO = ''S'' AND to_date(to_char(HORA_RELOJ, ''hh24:mi:ss''), ''hh24:mi:ss'') between to_date(''11:00:00'', ''hh24:mi:ss'') and to_date(''13:30:00'', ''hh24:mi:ss'') AND CAR_CODIGO <>7 THEN
                            ''S''
                       ELSE
                           ''N''
                       END IND,
                       EM.EMPL_AREA_ORGANI A_COD_ORGANIGRAMA,
                       AREA.AREDPP_DESC A_AREA,
                       CAR_CODIGO       A_CAR_CODIGO,
                       CAR.CAR_CODIGO || '' - '' || CAR.CAR_DESC A_CARGO,
                       EMPL_SUCURSAL A_COD_SUC,
                       EMPL_DEPARTAMENTO A_COD_DPTO,
                       EMPL_FORMA_PAGO  A_FORMA_PAGO,
                       EMPL_SITUACION A_SITUCION,
                       CAR.CAR_CODIGO || '' - '' || CAR.CAR_DESC CARGO,
                        EMPL_LEGAJO || '' - '' || EMPL_NOMBRE || '' '' || EMPL_APE EMPLEADO
                  FROM PER_MARCACION_DIARIA_V T,
                       PER_CONFIGURACION      PC,
                       PER_PERIODO            PE,
                       PER_EMPLEADO           EM,
                       PER_AREA_DPP           AREA,
                       PER_CARGO_DPP          CARDPP,
                       PER_CARGO              CAR
                 WHERE PC.CONF_EMPR = 1
                   AND PC.CONF_EMPR = PE.PERI_EMPR
                   AND EM.EMPL_EMPRESA = PC.CONF_EMPR
                   AND EM.EMPL_LEGAJO = T.MARC_EMPLEADO
                   AND EM.EMPL_FEC_INGRESO IS NOT NULL
                   AND EM.EMPL_EMPRESA = AREA.AREDPP_EMPR(+)
                   AND EM.EMPL_AREA_ORGANI = AREA.AREDPP_CLAVE(+)
                   AND EM.EMPL_EMPRESA = CARDPP.CARDPP_EMPR(+)
                   AND EM.EMPL_CARGO = CARDPP.CARDPP_CLAVE(+)
                   AND CARDPP.CARDPP_EMPR = CAR.CAR_EMPR(+)
                   AND CARDPP.CARDPP_CLAVE = CAR.CAR_CODIGO(+)
                   AND EM.EMPL_CODIGO_PROV <> 0
                   AND EMPL_FORMA_PAGO <> 0
                   AND EM.EMPL_SITUACION = ''A''
                   AND EM.EMPL_CONS_MARC = ''S''
                   AND EMPL_SITUACION IS NOT NULL
                   AND MARC_FECHA >= ''01/06/2020''
                   AND (PC.CONF_PERI_ACT - 3) = PE.PERI_CODIGO
                   AND PC.CONF_EMPR = T.MARC_EMPR
                   AND T.MARC_FECHA BETWEEN PE.PERI_FEC_INI AND SYSDATE) MAR
         WHERE EMPL_LEGAJO  = MARC_EMPLEADO (+)
           AND FECHA_HOR   = FECHA_HORARIO(+)
           )
   WHERE FECHA_HOR BETWEEN '''||P_FEC_INI||'''  AND '''||P_FEC_FIN||'''
 ---- -- AND NVL(OPCION,''S'') = ''S''
  -- and CARGO  in (''15 - PORTERIA'')
   '||P_WHERE||'
 GROUP BY FECHA_HOR, EMPL_LEGAJO, EMPLEADO,EMPL_FEC_INGRESO,    EMPL_FEC_SALIDA,
   AREA_CLAVE,AREA, CARGO, NIVEL,SUCURSAL, DESC_SUCURSAL, DEPARTAMENTO, COD_DPTO,
   EMPL_FORMA_PAGO,DEPARTAMENTO2,EMPL_SITUACION, A_CAR_CODIGO, fORMA_PAGO,   SITUACION,
   MARC_FECHA, --ENTRADA_HOR,  SALIDA_HOR, SALIDA_ALM,  ENTR_ALM,
   JSA_COD_AREA, JSA_DESC_AREA, OPCION,FECHA_HORARIO, OPCION2';


   else -->fin @hilagro
           P_QUERY :='SELECT FECHA,--Flag
                                FECHA FECHA_FILTRO,
                                EMPL_LEGAJO,
                                EMPLEADO,
                                FECHA_SALIDA,
                                FECHA_INGRESO,
                                CLAV_AREA,
                                AREA,
                                CARGO,
                                NIVEL,
                                SUCURSAL,
                                SUC_DESC,
                                DEPARTAMENTO,
                                DPTO_CODIGO,
                                EMPL_FORMA_PAGO,
                                DEPARTAMENTO DEPARTAMENTO2,
                                EMPL_SITUACION,
                                EMPL_TIPO_cONT,
                                EMPL_FORMA_PAGO,
                                EMPL_SITUACION,
                                MARC_FECHA,
                                HORA_ENT,
                                HORA_SAL,
                                HORA_SALIDA_ALMUERZO,
                                HORA_ENTRADA_ALMUERZO,
                                ENTRADA,
                                SALIDA,
                                MAX(ALMUERZO_ENTRADA) ENTRADA_ALMUERZO,
                                MAX(ALMUERZO_SALIDA) SALIDA_ALMUERZO,
                                TO_CHAR(ENTRADA,''HH24:MI:SS''),
                                TO_CHAR(SALIDA,''HH24:MI:SS''),
                                TO_CHAR(to_DATE(MAX(ALMUERZO_ENTRADA), ''DD/MM/YYYY HH24:MI:SS''),''HH24:MI:SS'') ENTRADA_ALMUERZO,
                                TO_CHAR(to_DATE(MAX(ALMUERZO_SALIDA), ''DD/MM/YYYY HH24:MI:SS''),''HH24:MI:SS'') SALIDA_ALMUERZO,
                                AREA1,
                               AREA_Des1,
                               ESTADO_DIAS,
                               CASE
                               WHEN ENTRADA IS NULL AND SALIDA IS NULL AND ESTADO_DIAS = ''S'' THEN ''AUSENTE''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''P'' THEN ''PERMISO''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''R'' THEN ''REPOSO''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''V'' THEN ''VAC. REG. LEGAL''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''L'' THEN ''LICENCIA''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''G'' THEN ''VIAJE LABORAL''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''A'' THEN ''AUSENCIA JUSTIFICADA''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''U'' THEN ''SUSPENCION''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''T'' THEN ''VAC. REG. INTERNO''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''F'' THEN ''FERIADO''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''H'' THEN ''SABADO''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''D'' THEN ''DOMINGO''
                               WHEN ENTRADA IS NULL AND ESTADO_DIAS = ''X'' THEN ''DOMINGO''
                               WHEN (ENTRADA IS NOT NULL OR SALIDA IS NOT NULL) AND ESTADO_DIAS = ''S'' THEN ''PRESENTE''
                         END AUSENTE,
                        CASE--PUNTUALIDAD
                           WHEN TO_CHAR(ENTRADA, ''HH24:MI'') <= HORA_ENT   AND ESTADO_DIAS = ''S'' THEN ''PUNTUAL''
                           WHEN TO_CHAR(ENTRADA, ''HH24:MI'')  > HORA_ENT   AND ESTADO_DIAS = ''S'' THEN ''IMPUNTUAL''
                          END PUNTUALIDAD
                      FROM(
                      SELECT DIAS_HABILES."FECHA",
                             DIAS_HABILES."EMPL_LEGAJO",
                             DIAS_HABILES."EMPLEADO",
                             DIAS_HABILES."AREA",
                             DIAS_HABILES."CARGO",
                             DIAS_HABILES."NIVEL",
                             DIAS_HABILES."SUCURSAL",
                             DIAS_HABILES."DEPARTAMENTO",
                             MARCADOS."MARC_FECHA",
                             MARCADOS."MARC_EMPLEADO",
                             MARCADOS."ENTRADA",
                             MARCADOS."SALIDA",
                             MARCADOS."ENTRADA_MENS",
                             MARCADOS."SALIDA_MENS",
                             MARCADOS."MARCADOR_ENTRADA",
                             MARCADOS."MARCADOR_SALIDA",
                             CASE
                               WHEN MARCADOS.MARCADOR_ENTRADA = ''S'' AND
                                    MARCADOS.MARCADOR_SALIDA = ''S'' THEN
                                ''S''
                               ELSE
                                ''N''
                             END CUMPLE,
                          CASE
                               WHEN MARCADOS.ENTRADA IS NOT NULL AND TO_CHAR(MARCADOS.ENTRADA, ''HH24:MI'') >= ''00:00''  AND TO_CHAR(MARCADOS.ENTRADA, ''HH24:MI'') <=''12:00'' THEN
                                 ''Ma?ana''
                               WHEN MARCADOS.ENTRADA IS NOT NULL AND TO_CHAR(MARCADOS.ENTRADA, ''HH24:MI'') >= ''12:01'' THEN
                                  ''Tarde''
                               ELSE
                                ''''
                          END TURNO,
                              CASE WHEN MARCADOS.MARC_EVENTO = ''E''  AND TO_CHAR(MARCADOS.MARC_HORA, ''HH24:MI'') >= ''11:30''   AND TO_CHAR(MARCADOS.MARC_HORA, ''HH24:MI'') <= ''13:30'' and '||P_EMPRESA||' <> 1  THEN
                                    TO_CHAR(MARCADOS.MARC_HORA, ''DD/MM/YYYY HH24:MI:SS'')
                          END ALMUERZO_E,
                          CASE
                            WHEN MARCADOS.MARC_EVENTO = ''S''  AND TO_CHAR(MARCADOS.MARC_HORA, ''HH24:MI'') >= ''11:30''   AND TO_CHAR(MARCADOS.MARC_HORA, ''HH24:MI'') <= ''13:30'' and '||P_EMPRESA||'  <> 1  THEN
                                    TO_CHAR(MARCADOS.MARC_HORA, ''DD/MM/YYYY HH24:MI:SS'')
                          END ALMUERZO_S,
                           CASE WHEN MARCADOS.MARC_EVENTO = ''E''  AND TO_CHAR(MARCADOS.MARC_HORA, ''HH24:MI'') >= ''11:30''   AND TO_CHAR(MARCADOS.MARC_HORA, ''HH24:MI'') <= ''13:30'' and '||P_EMPRESA||' <> 1  THEN
                                    TO_CHAR(MARCADOS.MARC_HORA, ''DD/MM/YYYY HH24:MI:SS'')
                          END ALMUERZO_ENTRADA,
                           CASE WHEN MARCADOS.MARC_EVENTO = ''S''  AND TO_CHAR(MARCADOS.MARC_HORA, ''HH24:MI'') >= ''11:30''   AND TO_CHAR(MARCADOS.MARC_HORA, ''HH24:MI'') <= ''13:30'' and '||P_EMPRESA||' <> 1 THEN
                                    TO_CHAR(MARCADOS.HORA_RELOJ, ''DD/MM/YYYY HH24:MI:SS'')
                          END ALMUERZO_SALIDA,
                        --  DECODE(ESTADO_DIAS,''V'',''S'',ESTADO_DIAS) ESTADO_DIAS,
                          CASE
                               WHEN ESTADO_DIAS= ''S'' THEN
                                     PER_VERIFICAR_VAC_RINTERNO(EMPL_LEGAJO,
                                                                '||P_EMPRESA||',
                                                                FECHA,
                                                                FECHA_INGRESO,
                                                                FECHA_SALIDA,
                                                                ''M'')
                              ELSE
                                ESTADO_DIAS
                              END ESTADO_DIAS,
                          FECHA_SALIDA,
                          FECHA_INGRESO,
                          CLAV_AREA,
                          DPTO_CODIGO,
                          EMPL_CONS_MARC_TAB,
                          EMPL_MARC_ENTRADA,
                          EMPL_MARC_SALIDA,
                          SUC_DESC,
                          AREA1,
                          AREA_Des1,
                           Case
                             WHEN DEPARTAMENTO2 IN (''ADMINISTRATIVO'', ''COMERCIAL'') and '||P_EMPRESA||' = 1 THEN ''11:45''
                             WHEN DEPARTAMENTO2 IN (''INDUSTRIAL'') and '||P_EMPRESA||' = 1 THEN ''12:00''
                            WHEN DEPARTAMENTO2  IN (''LOGISTICA'') and '||P_EMPRESA||' = 1 THEN ''12:15''
                            ELSE  ''12:00''
                            END HORA_SALIDA_ALMUERZO,
                           Case
                             WHEN DEPARTAMENTO2 IN (''ADMINISTRATIVO'', ''COMERCIAL'') and '||P_EMPRESA||' = 1 THEN  ''12:45''
                             WHEN DEPARTAMENTO2 IN (''INDUSTRIAL'') and '||P_EMPRESA||' = 1 THEN ''13:00''
                            WHEN DEPARTAMENTO2  IN (''LOGISTICA'') and '||P_EMPRESA||' = 1 THEN ''13:15''
                            ELSE  ''13:00''
                            END HORA_ENTRADA_ALMUERZO,
                            ---HORARIOS DE ENTRADA
                         CASE
                           WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''06:15''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''07:30''  THEN
                               ''07:00''
                           WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''05:30''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''06:15''   THEN
                               ''06:00''
                            WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''13:30''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''14:30'' THEN
                               ''14:00''
                            WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''21:00''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''22:50'' THEN
                               ''22:00''
                           WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''05:30''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''06:35''    THEN
                             ''06:00''
                          WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''07:30''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''08:35''   THEN
                             ''08:00''
                           WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''08:30''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''09:35''    THEN
                             ''09:00''
                            WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''09:30''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''10:35''    THEN
                             ''10:00''
                            WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''11:30''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''12:35''    THEN
                             ''12:00''
                        ELSE
                           to_char(EMPL_MARC_ENTRADA, ''HH24:MI'')
                           END  HORA_ENT,
                           ---HORARIOS DE SALIDA
                          CASE WHEN TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''06:30''
                               AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''07:30''
                               AND TO_CHAR(MARCADOS."SALIDA", ''HH24:MI'')>= ''16:00''
                               THEN-----horario salida normal
                                    ''17:00''
                           WHEN  TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''13:30''
                           AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''05:30''
                           AND TO_CHAR(MARCADOS."SALIDA", ''HH24:MI'')>= ''06:15'' THEN
                             ''14:00''
                            WHEN  TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''13:30''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''14:30''
                           AND TO_CHAR(MARCADOS."SALIDA", ''HH24:MI'')>= ''20:00'' THEN
                             ''22:00''
                            WHEN  TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''21:30''
                            AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''22:30''  THEN
                             ''06:00''
                         WHEN  TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''05:30''
                           AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''06:15''
                            AND TO_CHAR(MARCADOS."SALIDA", ''HH24:MI'')>= ''15:30'' THEN
                             ''16:00''
                          WHEN  TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''07:30''
                           AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''08:30''
                            AND TO_CHAR(MARCADOS."SALIDA", ''HH24:MI'')>= ''17:30'' THEN
                             ''18:00''
                          WHEN  TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''08:30''
                           AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''09:30''
                            AND TO_CHAR(MARCADOS."SALIDA", ''HH24:MI'')>= ''18:00'' THEN
                             ''19:00''
                         WHEN  TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''08:30''
                           AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''10:30''
                            AND TO_CHAR(MARCADOS."SALIDA", ''HH24:MI'')>= ''18:00'' THEN
                             ''20:00''
                          WHEN  TO_CHAR(  MARCADOS."ENTRADA", ''HH24:MI'') >= ''11:30''
                           AND TO_CHAR(MARCADOS."ENTRADA", ''HH24:MI'') <= ''12:30''
                            AND TO_CHAR(MARCADOS."SALIDA", ''HH24:MI'')>= ''20:00'' THEN
                             ''22:00''
                           ELSE
                            to_char(EMPL_MARC_SALIDA, ''HH24:MI'')
                           END  HORA_SAL,
                           EMPL_FORMA_PAGO,
                           EMPL_SITUACION,
                           EMPL_TIPO_cONT
                        FROM (SELECT DIAS.FECHA,
                                     P.EMPL_LEGAJO,
                                     P.EMPL_LEGAJO || '' - '' || P.EMPL_NOMBRE || '' '' || P.EMPL_APE EMPLEADO,
                                     AREA.AREDPP_CLAVE || '' - '' || AREA.AREDPP_DESC AREA,
                                     CAR.CAR_CODIGO || '' - '' || CAR.CAR_DESC CARGO,
                                     NIV.NIVORG_DESC NIVEL,
                                     P.EMPL_SUCURSAL SUCURSAL,
                                     DEP.DPTO_DESC DEPARTAMENTO,
                                      CAR.CAR_CODIGO cargo_codigo,
                                     CASE
                                      WHEN DPTO_SUC = 2 and '||P_EMPRESA||' = 1 THEN
                                           ''CDA''
                                      WHEN DPTO_CODIGO = 1 and '||P_EMPRESA||' = 1 THEN
                                           ''ADMINISTRATIVO''
                                      WHEN (DPTO_CODIGO IN(14,2) OR DPTO_SUC NOT IN (1,2))  and '||P_EMPRESA||' = 1 THEN
                                          ''COMERCIAL''
                                      WHEN DPTO_CODIGO = 22 and '||P_EMPRESA||' = 1 THEN
                                          ''LOGISTICA''
                                      when  '||P_EMPRESA||' = 1 then
                                         ''INDUSTRIAL''
                                      when  '||P_EMPRESA||' <> 1 then
                                        DEP.DPTO_DESC
                                      END DEPARTAMENTO2,

                                      PER_VERIFICAR_DIA_LABORAL(P.EMPL_LEGAJO,
                                                                '||P_EMPRESA||',
                                                                DIAS.FECHA,
                                                                P.EMPL_FEC_INGRESO,
                                                                P.EMPL_FEC_SALIDA,
                                                                DECODE(EMPL_FORMA_PAGO,1,''J'',2,''M'')) ESTADO_DIAS,
                                     P.EMPL_FEC_SALIDA FECHA_SALIDA,
                                     P.EMPL_FEC_INGRESO FECHA_INGRESO,
                                     AREDPP_CLAVE CLAV_AREA,
                                     DEP.DPTO_CODIGO DPTO_CODIGO,
                                     NVL(EMPL_CONS_MARC_TAB,''S'') EMPL_CONS_MARC_TAB,
                                     EMPL_MARC_ENTRADA,
                                     EMPL_MARC_SALIDA,
                                     SUC_DESC,
                                   CASE
                                     when '||P_EMPRESA||' <> 1 then  AREDPP_CLAVE
                                    END AREA1,
                                   CASE
                                     when '||P_EMPRESA||' <> 1 then AREDPP_DESC
                                  END AREA_Des1,
                                  DECODE(EMPL_FORMA_PAGO,1,''JORNAL'',2,''MENSUAL'',3, ''SERVICIOS PERSONALES'', 4,''TEMPORAL'',5,''AMH'') EMPL_FORMA_PAGO,
                                  DECODE (EMPL_SITUACION,''I'',''INACTIVO'',''A'',''ACTIVO'',NULL) EMPL_SITUACION,
                                  EMPL_TIPO_cONT
                                FROM PER_CONFIGURACION PCON,
                                     PER_PERIODO PPER,
                                     (SELECT I_DIA FECHA
                                        FROM TABLE(RETURN_TABLE_DIA_PROF(TO_NUMBER(TO_CHAR(SYSDATE,
                                                                                           ''YYYY''))))) DIAS,
                                     PER_EMPLEADO P,
                                     PER_AREA_DPP AREA,
                                     PER_CARGO_DPP CARDPP,
                                     PER_CARGO CAR,
                                     PER_NIVEL_ORGANI NIV,
                                     GEN_DEPARTAMENTO DEP,
                                     GEN_SUCURSAL SUC
                               WHERE P.EMPL_EMPRESA = '||P_EMPRESA||'
                                 AND P.EMPL_EMPRESA = DEP.DPTO_EMPR(+)
                                 AND P.EMPL_DEPARTAMENTO = DEP.DPTO_CODIGO(+)
                                 AND P.EMPL_SUCURSAL = DEP.DPTO_SUC(+)
                                 AND P.EMPL_SUCURSAL = SUC.SUC_CODIGO(+)
                                 AND P.EMPL_EMPRESA = SUC.SUC_EMPR (+)
                                -- AND P.EMPL_FEC_INGRESO >= '''||P_FEC_INI||'''
                                --- AND P.EMPL_FORMA_PAGO <> 1
                                 AND P.EMPL_SITUACION = ''A''
                                 AND P.EMPL_CONS_MARC = ''S''
                                 AND PCON.CONF_EMPR = P.EMPL_EMPRESA
                                 AND PCON.CONF_EMPR = PPER.PERI_EMPR
                                 AND (PCON.CONF_PERI_ACT - 3) = PPER.PERI_CODIGO
                                 AND DIAS.FECHA BETWEEN PPER.PERI_FEC_INI AND SYSDATE
                                 AND P.EMPL_EMPRESA = AREA.AREDPP_EMPR(+)
                                 AND P.EMPL_AREA_ORGANI = AREA.AREDPP_CLAVE(+)
                                 AND P.EMPL_EMPRESA = CARDPP.CARDPP_EMPR(+)
                                 AND P.EMPL_CARGO = CARDPP.CARDPP_CLAVE(+)
                                 AND CARDPP.CARDPP_EMPR = CAR.CAR_EMPR(+)
                                 AND CARDPP.CARDPP_CLAVE = CAR.CAR_CODIGO(+)
                                 AND CARDPP.CARDPP_EMPR = NIV.NIVORG_EMPR(+)
                                 AND CARDPP.CARDPP_NIV_ORGANI = NIV.NIVORG_CLAVE(+)) DIAS_HABILES,

                             (SELECT CALC.*,
                                     CASE
                                       WHEN CALC.ENTRADA IS NOT NULL THEN
                                        CASE
                                          WHEN TO_DATE(''01/01/2018 '' ||
                                                       TO_CHAR(CALC.ENTRADA, ''HH24:MI:SS''),
                                                       ''DD/MM/YYYY HH24:MI:SS'') >=
                                               TO_DATE(''01/01/2018 '' ||
                                                       TO_CHAR(CALC.ENTRADA_MENS, ''HH24:MI:SS''),
                                                       ''DD/MM/YYYY HH24:MI:SS'') THEN
                                           ''N''
                                          ELSE
                                           ''S''
                                        END
                                       ELSE
                                        ''N''
                                     END MARCADOR_ENTRADA,
                                     CASE
                                       WHEN CALC.SALIDA IS NOT NULL THEN
                                        CASE
                                          WHEN TO_DATE(''01/01/2018 '' ||
                                                       TO_CHAR(CALC.SALIDA, ''HH24:MI:SS''),
                                                       ''DD/MM/YYYY HH24:MI:SS'') <=
                                               TO_DATE(''01/01/2018 '' ||
                                                       TO_CHAR(CALC.SALIDA_MENS, ''HH24:MI:SS''),
                                                       ''DD/MM/YYYY HH24:MI:SS'') THEN
                                           ''N''
                                          ELSE
                                           ''S''
                                        END
                                       ELSE
                                        ''N''
                                     END MARCADOR_SALIDA
                                FROM (SELECT CONS.MARC_FECHA,
                                             CONS.MARC_EVENTO,
                                             CONS.MARC_HORA,
                                             CONS.MARC_EMPLEADO,
                                             (SELECT MIN(EN.HORA_RELOJ)
                                                FROM PER_MARCACION_DIARIA_V EN
                                               WHERE EN.MARC_FECHA = CONS.MARC_FECHA
                                                 AND EN.MARC_EMPLEADO = CONS.MARC_EMPLEADO
                                                 AND EN.MARC_EVENTO = ''E''
                                                 AND EN.MARC_EMPR =  '||P_EMPRESA||') ENTRADA,
                                             (SELECT MAX(EN.HORA_RELOJ)
                                                FROM PER_MARCACION_DIARIA_V EN
                                               WHERE EN.MARC_FECHA = CONS.MARC_FECHA
                                                 AND EN.MARC_EMPLEADO = CONS.MARC_EMPLEADO
                                                 AND EN.MARC_EVENTO = ''S''
                                                 AND EN.MARC_EMPR = '||P_EMPRESA||') SALIDA,
                                             TO_DATE(TO_CHAR(CONS.MARC_FECHA, ''DD/MM/YYYY'') || '' '' ||
                                                     TO_CHAR(CONS.CONF_ENTRADA, ''HH24:MI:SS''),
                                                     ''DD/MM/YYYY HH24:MI:SS'') ENTRADA_MENS,
                                             TO_DATE(TO_CHAR(CONS.MARC_FECHA, ''DD/MM/YYYY'') || '' '' ||
                                                     TO_CHAR(CONS.CONF_SALIDA, ''HH24:MI:SS''),
                                                     ''DD/MM/YYYY HH24:MI:SS'') SALIDA_MENS,
                                                    HORA_RELOJ---
                                        FROM (SELECT T.MARC_FECHA,
                                                     T.MARC_EVENTO,
                                                     T.MARC_HORA,
                                                     T.MARC_EMPLEADO,
                                                     T.MARC_EMPR,
                                                     T.HORA_RELOJ,
                                                     EM.EMPL_MARC_ENTRADA CONF_ENTRADA,
                                                     EM.EMPL_MARC_SALIDA  CONF_SALIDA
                                                FROM PER_MARCACION_DIARIA_V T,
                                                     PER_CONFIGURACION      PC,
                                                     PER_PERIODO            PE,
                                                     PER_EMPLEADO           EM
                                               WHERE PC.CONF_EMPR = '||P_EMPRESA||'
                                                 AND PC.CONF_EMPR = PE.PERI_EMPR
                                                 AND EM.EMPL_EMPRESA = PC.CONF_EMPR
                                                 AND EM.EMPL_LEGAJO = T.MARC_EMPLEADO
                                                 AND EM.EMPL_FORMA_PAGO <> 1
                                                 AND EM.EMPL_SITUACION = ''A''
                                                 AND EM.EMPL_CONS_MARC = ''S''
                                                 AND (PC.CONF_PERI_ACT - 3) = PE.PERI_CODIGO
                                                 AND PC.CONF_EMPR = T.MARC_EMPR

                                                 AND T.MARC_FECHA BETWEEN PE.PERI_FEC_INI AND
                                                     SYSDATE) CONS,
                                             PER_EMPLEADO EMP
                                       WHERE CONS.MARC_EMPR = '||P_EMPRESA||'
                                         AND CONS.MARC_EMPR = EMP.EMPL_EMPRESA
                                         AND CONS.MARC_EMPLEADO = EMP.EMPL_LEGAJO
                                       GROUP BY MARC_FECHA,
                                                CONS.MARC_EVENTO,
                                                CONS.MARC_HORA,
                                                CONS.MARC_EMPLEADO,
                                                CONS.MARC_EMPLEADO,
                                                CONS.CONF_ENTRADA,
                                                CONS.CONF_SALIDA,
                                                HORA_RELOJ
                                       ORDER BY CONS.MARC_FECHA) CALC) MARCADOS
                       WHERE DIAS_HABILES.FECHA = MARCADOS.MARC_FECHA(+)
                         AND DIAS_HABILES.EMPL_LEGAJO = MARCADOS.MARC_EMPLEADO(+))
                         WHERE FECHA BETWEEN '''||P_FEC_INI||''' AND '''||P_FEC_FIN||'''
                         '||P_WHERE||'
                         and ESTADO_DIAS <> ''W''
                         GROUP BY  FECHA, EMPL_LEGAJO, EMPLEADO, AREA, CARGO,NIVEL,SUCURSAL,DEPARTAMENTO,
                         MARC_FECHA,MARC_EMPLEADO,ENTRADA,SALIDA,ENTRADA_MENS,SALIDA_MENS,MARCADOR_ENTRADA,
                         MARCADOR_SALIDA,CUMPLE,TURNO,ESTADO_DIAS, FECHA_INGRESO, FECHA_SALIDA, TO_CHAR(FECHA,''D''),
                         CLAV_AREA,DPTO_CODIGO,EMPL_CONS_MARC_TAB, EMPL_MARC_ENTRADA,
                         EMPL_MARC_SALIDA,SUC_DESC,SALIDA,AREA1, AREA_Des1,
                         HORA_ENTRADA_ALMUERZO, --35
                          HORA_SALIDA_ALMUERZO,HORA_ENT,HORA_SAL,EMPL_FORMA_PAGO, EMPL_SITUACION,
                           EMPL_TIPO_cONT';

             END IF;


delete x
where otro = 'perl052_pacr';
/*
insert into x
  (campo1, otro)
values
  (p_query, 'perl052_pacr');
*/

                  IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => 'PERL052_MARC_PUNT') THEN
                         APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => 'PERL052_MARC_PUNT');
                  END IF;
                  APEX_COLLECTION.CREATE_COLLECTION_FROM_QUERY_B(P_COLLECTION_NAME => 'PERL052_MARC_PUNT',
                                                                 P_QUERY           => P_QUERY);


 END PP_CREAR_PUNTUALIDAD;


 PROCEDURE PP_LLAMAR_REPORTE (P_EMPRESA         IN NUMBER,
                              P_LOGIN           IN VARCHAR2,
                              P_SESSION         IN NUMBER,
                              P_REP             IN VARCHAR2,
                              P_FEC_INI         IN DATE,
                              P_FEC_FIN         IN DATE,
                              P_EMPLEADO        IN NUMBER,
                              P_DEPARTAMENTO    IN NUMBER,
                              P_SUCURSAL        IN NUMBER,
                              P_AREA_ORG        IN NUMBER,
                              P_OPCIONES        IN VARCHAR2,
                              P_JSA             IN VARCHAR2,
                              P_SABADO          IN VARCHAR2,
                              P_FORMATO         IN NUMBER,
                              P_CARGO           IN NUMBER,
                              P_FORMA_PAGO      IN NUMBER) IS

  V_SUC  VARCHAR2(60);
  V_DEP  VARCHAR2(60);
  V_ARE  VARCHAR2(60);
  V_EMPL  VARCHAR2(60);
  V_JSA  VARCHAR2(60);
  V_SAB  VARCHAR2(60);
  V_OPCION VARCHAR2(60);
  V_REPORTE VARCHAR2(60);
  V_PARAMETROS    VARCHAR2(4000);
   V_IDENTIFICADOR VARCHAR2(2) := '&';

   v_cant_empleados number;
   v_cant_registro number;
  BEGIN
 
 BEGIN
  SELECT A.SUC_DESC
  INTO V_SUC
  FROM GEN_SUCURSAL A
  WHERE A.SUC_EMPR = P_EMPRESA
  AND A.SUC_CODIGO =P_SUCURSAL;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
      V_SUC:='TODAS LAS SUCURSALES';
END;

BEGIN
 SELECT EMPL_LEGAJO||' - '||EMPL_NOMBRE||' '||EMPL_APE
  INTO V_EMPL
  FROM PER_EMPLEADO E
  WHERE EMPL_EMPRESA = P_EMPRESA
  AND EMPL_LEGAJO =P_EMPLEADO;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
      V_EMPL:='TODOS LOS EMPLEADOS';
END;

BEGIN
 SELECT D.DPTO_DESC
  INTO V_DEP
  FROM GEN_DEPARTAMENTO D
  WHERE D.DPTO_EMPR = P_EMPRESA
  AND D.DPTO_CODIGO =P_DEPARTAMENTO;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
      V_DEP :='TODOS LOS DEPARTAMENTOS';
END;
  IF P_AREA_ORG = 2 THEN
    V_ARE := '2 - ADM';
  ELSIF P_AREA_ORG = 1 THEN
    V_ARE := '1 - FIN E INFO';
  ELSIF P_AREA_ORG = 3 THEN
    V_ARE := '3 - CDA';
  ELSIF P_AREA_ORG = 4 THEN
    V_ARE := '4 - INDUSTRIAL';
   ELSIF P_AREA_ORG = 5 THEN
    V_ARE := '5 - COMERCIAL';
  ELSE
    V_ARE:= 'TODAS LAS AREAS';
  END IF;

  IF P_SABADO = 'D' THEN
    V_SAB := 'INCLUYE DIA DOMINGO';
  else
    V_SAB := 'NO INCLUYE DIA DOMINGO';
  end if;

  IF P_JSA = 'S' THEN
    V_JSA := 'LISTA DE PERSONAS QUE SE CONSIDERAN PARA JSA';
  else
    V_JSA := 'TODOS LOS QUE CONSIDERA ASISTENCIA';
  end if;

           IF P_OPCIONES = 'D' THEN
             V_OPCION:= 'DOMINGO';
           ELSIF P_OPCIONES = 'H' THEN
             V_OPCION:='SABADO';
           ELSIF P_OPCIONES = 'X' THEN
             V_OPCION:='FICHA INACTIVA';
           ELSIF P_OPCIONES = 'F' THEN
             V_OPCION:='FERIADO';
           ELSIF P_OPCIONES = 'V' THEN
             V_OPCION:='VAC. REG. LEGAL';
           ELSIF P_OPCIONES = 'T' THEN
             V_OPCION:='VAC. REG. INTERNO';
           ELSIF P_OPCIONES = 'L' THEN
             V_OPCION:='LICENCIA';
           ELSIF P_OPCIONES = 'R' THEN
             V_OPCION:='REPOSO';
           ELSIF P_OPCIONES = 'P' THEN
             V_OPCION:='PRERMISO';
           ELSIF P_OPCIONES = 'A' THEN
             V_OPCION:='AUSENCIA JUSTIFICADA';
           ELSIF P_OPCIONES = 'G' THEN
             V_OPCION:='VIAJE LABORAL';
           ELSIF P_OPCIONES = 'U' THEN
            V_OPCION:='SUSPENCION';
           ELSIF P_OPCIONES = 'K' THEN
             V_OPCION:='AUSENTE';
           ELSE
             V_OPCION:='PRESENTE';
           END IF;


   DELETE FINL052_TEMP
    WHERE V_SESSION = P_SESSION
     AND V_LOGIN = P_LOGIN
      AND V_REP =P_REP;
      commit;


SELECT COUNT(EMPL_LEGAJO)
INTO V_CANT_EMPLEADOS
  FROM PER_EMPLEADO E
  WHERE EMPL_EMPRESA = P_EMPRESA
  AND (EMPL_LEGAJO = P_EMPLEADO OR   P_EMPLEADO IS NULL)
  AND (E.EMPL_DEPARTAMENTO = P_DEPARTAMENTO OR  P_DEPARTAMENTO IS NULL)
  AND (E.EMPL_CARGO = P_CARGO OR   P_CARGO IS NULL)
  AND E.EMPL_CONS_MARC = 'S'
  AND (EMPL_SUCURSAL = P_SUCURSAL OR P_SUCURSAL IS NULL)
  AND (EMPL_AREA_ORGANI =P_AREA_ORG OR   P_AREA_ORG IS NULL)
  AND (EMPL_FORMA_PAGO = P_FORMA_PAGO OR P_FORMA_PAGO IS NULL)
  AND EMPL_SITUACION  = 'A';



SELECT  COUNT(DISTINCT(C003)) 
  INTO V_CANT_REGISTRO
   FROM APEX_COLLECTIONS
  WHERE COLLECTION_NAME = 'PERL052_MARC_PUNT'
    and C030 <> 'SIN MARC.';
    


  IF P_REP = 'A'  THEN
          insert into finl052_temp
              (fecha,               legajo,           empl_nombre,       cod_area,         area,
               cod_dep,             departamento,     cod_suc,           sucural,          cargo,
               estado,              puntualidad,      horario_entrada,   horario_salida,   marcacion_entrada,
               marcacion_salida,
               v_fecha_desde,       v_fecha_hasta,    v_sucural,         v_departamento,   v_area,
               v_legajo,            v_jsa,            v_sabado,          v_login,          v_session,
               v_opcion,            v_rep,            SEMANA,            DIA,              SEMANA_DESC)
         SELECT C001,               C003,             C004,              C034,             C035,
                C014,               C013,             C011,              C012,             C009,
                C037,               C038,             C022,              C023,             C030,
                C031,
                P_FEC_INI,          P_FEC_FIN,        V_SUC,             V_DEP,            V_ARE,
                V_EMPL,             V_JSA,            V_SAB,             P_LOGIN,          P_SESSION,
                V_OPCION,               'A',          C039,              C040,             C041
         from APEX_collections
        where COLLECTION_NAME = 'PERL052_MARCACIONES';
        V_REPORTE :='PERL052_r';
elsif P_REP in ('Y') and P_EMPRESA = 2 THEN
       insert into finl052_temp
              (fecha,               legajo,           empl_nombre,       cod_area,         area,
               cod_dep,             departamento,     cod_suc,           sucural,          cargo,
               estado,              puntualidad,      horario_entrada,   horario_salida,   marcacion_entrada,
               marcacion_salida,
               v_fecha_desde,       v_fecha_hasta,    v_sucural,         v_departamento,   v_area,
               v_legajo,            v_jsa,            v_sabado,          v_login,          v_session,
               v_opcion,            v_rep,            SEMANA,            DIA,              SEMANA_DESC)
         SELECT C001,               C003,             C004,              C034,             C035,
                C014,               C013,             C011,              C012,             C009,
                C037,               C038,             '13:00',             '12:00',             C030,
                C031,
                P_FEC_INI,          P_FEC_FIN,        V_SUC,             V_DEP,            V_ARE,
                V_EMPL,             V_JSA,            V_SAB,             P_LOGIN,          P_SESSION,
                V_OPCION,               'A',          C039,              C040,             C041
         from APEX_collections
        where COLLECTION_NAME = 'PERL052_MARCACIONES';
        V_REPORTE :='PERL052_rt2';
    
 ELSIF P_REP = 'B' THEN
     insert into finl052_temp
              (fecha,               legajo,           empl_nombre,       cod_area,         area,
               cod_dep,             departamento,     cod_suc,           sucural,          cargo,
               estado,              puntualidad,      horario_entrada,   horario_salida,   marcacion_entrada,
               marcacion_salida,    hor_almuerzo_ent, hor_almuerzo_sal,  mar_almuerzo_ent, mar_almuerzo_sal,
               v_fecha_desde,       v_fecha_hasta,    v_sucural,         v_departamento,   v_area,
               v_legajo,            v_jsa,            v_sabado,          v_login,          v_session,
               v_opcion,            v_rep,            SEMANA,            DIA,              SEMANA_DESC
               )
         SELECT C001,               C003,             C004,              C034,             C008,
                C014,               nvl(C013, c015),  C011,              C012,             C009,
                C037,               C038,             C023,              C024,             C030,
                
                C031,               decode(P_EMPRESA,
                                           1,C026,
                                           '13:00'),  decode(P_EMPRESA,
                                                             1,C025,
                                                             '12:00'),   C033,             C032,
                                                             
                P_FEC_INI,          P_FEC_FIN,        V_SUC,             V_DEP,            V_ARE,
                V_EMPL,             V_JSA,            V_SAB,             P_LOGIN,          P_SESSION,
                null,               'B',              C039,              C040,             C041
         from APEX_collections
        where COLLECTION_NAME = 'PERL052_MARC_PUNT';

         IF P_FORMATO = 1 THEN
             V_REPORTE :=  'PERL052_a';
         ELSIF P_FORMATO = 2 THEN
             V_REPORTE :=  'PERL052_a1';
         ELSIF P_FORMATO = 3 THEN
             V_REPORTE :=  'PERL052_a2';
         END IF;
         
   END if;
   
    V_PARAMETROS := 'P_FORMATO=' || URL_ENCODE('pdf');
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_SESSION=' ||
                    URL_ENCODE(P_SESSION);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_LOGIN=' ||
                    URL_ENCODE(P_LOGIN);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_CANT_EMPL=' ||
                    URL_ENCODE(V_CANT_EMPLEADOS);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_CANT_REGISTRO=' ||
                    URL_ENCODE(V_CANT_REGISTRO);




    DELETE FROM GEN_PARAMETROS_REPORT WHERE USUARIO = P_LOGIN;
    INSERT INTO GEN_PARAMETROS_REPORT
      (PARAMETROS, USUARIO, NOMBRE_REPORTE, FORMATO_SALIDA)
    VALUES
      (V_PARAMETROS, P_LOGIN, V_REPORTE, 'pdf');
    COMMIT;

 END PP_LLAMAR_REPORTE;

end PERL052;
/
