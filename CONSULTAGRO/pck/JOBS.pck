create or replace package JOBS is

  -- Author  : PROGRAMACION4
  -- Created : 27/07/2020 11:14:01
  -- Purpose : PAQUETE PARA PONER LOS JOBS

  PROCEDURE PP_JOB_CADA_10MIN;

  PROCEDURE PP_JOB_15_SEG_LOG_CPU;

end JOBS;
/
create or replace package body JOBS is

  PROCEDURE PP_JOB_CADA_10MIN AS
  BEGIN
    BEGIN
   --   TRAM299.PP_JOB_OCARGA;
      -- agregado para actaulizar cotizacion de Hilagro
      gen_cotizacion_pck.sincronizar_cotizacion;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
  
    BEGIN
   --   STKI049.PP_TIEMPO_EXCEDIDO_CARGA;
    null;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
  
  END PP_JOB_CADA_10MIN;

  PROCEDURE PP_JOB_15_SEG_LOG_CPU AS
  BEGIN
    insert into gen_log_sql_cpu_usage
      (glog_username,
       glog_osuser,
       glog_sql_exec_start,
       glog_ssid,
       glog_machine,
       glog_terminal,
       glog_program,
       glog_modulo,
       glog_action,
       glog_spid,
       glog_user_apex,
       glog_cpu_usage_seconds,
       glog_sql_id,
       glog_sql_text)
      SELECT SS.USERNAME,
             ss.OSUSER,
             ss.SQL_EXEC_START,
             SE.SID,
             SS.MACHINE,
             SS.TERMINAL,
             SS.PROGRAM,
             ss.MODULE,
             ss.ACTION,
             SPID,
             REGEXP_SUBSTR(CLIENT_IDENTIFIER, '[^:]+') USER_APEX,
             VALUE / 100 CPU_USAGE_SECONDS,
             SQ.SQL_ID,
             SQ.SQL_TEXT
        FROM V$SESSION  SS,
             V$SESSTAT  SE,
             V$STATNAME SN,
             V$SQL      SQ,
             V$PROCESS  PP
       WHERE SE.STATISTIC# = SN.STATISTIC#
         AND NAME LIKE '%CPU used by this session%'
         AND SE.SID = SS.SID
         AND SS.PADDR = PP.ADDR
         AND SS.SQL_ID = SQ.SQL_ID
         AND SS.STATUS = 'ACTIVE'
         AND SS.TYPE = 'USER'
         AND SS.USERNAME NOT IN ('SYSMAN')
         AND SS.USERNAME IS NOT NULL
         AND ROUND((NVL(SYSDATE - SS.SQL_EXEC_START, 0) * 86400)) > 15
       group by SS.USERNAME,
                SE.SID,
                SS.MACHINE,
                SS.TERMINAL,
                ss.SQL_EXEC_START,
                ss.OSUSER,
                SS.PROGRAM,
                ss.MODULE,
                ss.ACTION,
                SPID,
                REGEXP_SUBSTR(CLIENT_IDENTIFIER, '[^:]+'),
                VALUE / 100,
                SQ.SQL_ID,
                SQ.SQL_TEXT
       ORDER BY CPU_USAGE_SECONDS DESC;
    COMMIT;
  EXCEPTION 
    WHEN OTHERS THEN
      NULL;
  END PP_JOB_15_SEG_LOG_CPU;

end JOBS;
/
