CREATE OR REPLACE PACKAGE GENM017 IS

  -- AUTHOR  : PROGRAMACION10
  -- CREATED : 19/02/2020 13:57:48
  -- PURPOSE :

  PROCEDURE PP_GUARDAR_OPERADORES(I_GOPER IN GEN_OPERADOR_EMPRESA%ROWTYPE,
                                  I_OPER  IN VARCHAR2);
  PROCEDURE PP_GUARDAR_PERMISO_MOD(I_CAMPO   NUMBER,
                                   I_OPER    IN VARCHAR2,
                                   I_EMPRESA IN NUMBER,
                                   I_TIPO    IN VARCHAR2);
  PROCEDURE PP_CARGAR_PRODUCTO(I_DESCRIPCION OUT VARCHAR2,
                               I_CODIGO      IN NUMBER,
                               I_EMPRESA     IN NUMBER,
                               I_CLAVE       OUT NUMBER);
  PROCEDURE PP_CARGAR_ANALISIS(I_OPEM_OPER IN NUMBER, I_PRODUCTO IN NUMBER);
  PROCEDURE PP_GUARDAR_PERMISO_ANALISIS(I_TABLA   NUMBER,
                                        I_OPER    IN VARCHAR2,
                                        I_EMPRESA IN NUMBER,
                                        I_TIPO    IN VARCHAR2);

 PROCEDURE PP_ACTULIZAR_OPERADOR(P_EMPRESA           IN NUMBER,
                                 P_OPER_NOMBRE       IN VARCHAR2,
                                 P_OPER_DESC_ABREV   IN VARCHAR2,
                                 P_OPER_MAX_SESSIONS IN VARCHAR2,
                                 P_IMAGEN            IN BLOB,
                                 P_OPER_MENU         IN VARCHAR2,
                                 P_OPER_CODIGO       IN NUMBER,
                                 P_OPER_EMPR         IN NUMBER);
END GENM017;
/
CREATE OR REPLACE PACKAGE BODY GENM017 IS

  PROCEDURE PP_GUARDAR_PERMISO_MOD(I_CAMPO   NUMBER,
                                   I_OPER    IN VARCHAR2,
                                   I_EMPRESA IN NUMBER,
                                   I_TIPO    VARCHAR2) AS
  BEGIN
    IF I_CAMPO IS NOT NULL THEN
      IF I_TIPO = 'S' THEN
        INSERT INTO ACO_TABLA_HAB_ENTSAL
          (ATEN_ROL, ATEN_EMPR, ATEN_CAMPO, ATEN_FECHA_REG, ATEN_LOGIN)
        VALUES
          (I_OPER, I_EMPRESA, I_CAMPO, SYSDATE, GEN_DEVUELVE_USER);
      
      ELSE
        DELETE ACO_TABLA_HAB_ENTSAL
         WHERE ATEN_EMPR = I_EMPRESA
           AND ATEN_CAMPO = I_CAMPO
           AND ATEN_ROL = I_OPER;
      END IF;
    
      COMMIT;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001, 'ERROR :' || SQLERRM);
  END;

  PROCEDURE PP_CARGAR_PRODUCTO(I_DESCRIPCION OUT VARCHAR2,
                               I_CODIGO      IN NUMBER,
                               I_EMPRESA     IN NUMBER,
                               I_CLAVE       OUT NUMBER) IS
    V_PROD_TRIG NUMBER;
  BEGIN
    SELECT T.PROD_DESC, T.PROD_CLAVE, PROD_GRUPO
      INTO I_DESCRIPCION, I_CLAVE, V_PROD_TRIG
      FROM ACO_PRODUCTO T
     WHERE T.PROD_EMPR = I_EMPRESA
       AND T.PROD_CODIGO = I_CODIGO
       AND PROD_GRUPO = 4;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      /*
      
      /*APEX_ERROR.ADD_ERROR (
       P_MESSAGE          => 'This custom account is not active!',
       P_DISPLAY_LOCATION => APEX_ERROR.C_INLINE_IN_NOTIFICATION ); */
    
      NULL;
    
    /*
    RAISE_APPLICATION_ERROR(-20010,
                             'La restriccion solo se aplica para producto Trigo!');    */
  
  END;

  PROCEDURE PP_GUARDAR_PERMISO_ANALISIS(I_TABLA   NUMBER,
                                        I_OPER    IN VARCHAR2,
                                        I_EMPRESA IN NUMBER,
                                        I_TIPO    VARCHAR2) AS
  BEGIN
    IF I_TABLA IS NOT NULL THEN
      IF I_TIPO = 'S' THEN
        INSERT INTO ACO_PROD_TABLA_OPER
          (APTO_OPERADOR, APTO_TABLA, APTO_EMPR, APTO_FEC_REG, APTO_LOGIN)
        VALUES
          (I_OPER, I_TABLA, I_EMPRESA, SYSDATE, GEN_DEVUELVE_USER);
      ELSE
        DELETE ACO_PROD_TABLA_OPER
         WHERE APTO_EMPR = I_EMPRESA
           AND APTO_TABLA = I_TABLA
           AND APTO_OPERADOR = I_OPER;
      END IF;
    
      COMMIT;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20002, 'ERROR :' || SQLERRM);
  END;

  PROCEDURE PP_CARGAR_ANALISIS(I_OPEM_OPER IN NUMBER, I_PRODUCTO IN NUMBER) IS
  BEGIN
    APEX_COLLECTION.CREATE_OR_TRUNCATE_COLLECTION(P_COLLECTION_NAME => 'GENM017_CALIF');
    FOR V IN (SELECT DISTINCT NVL(T.AVAO_TABLA, A.CAL_CODIGO) CAL_CODIGO,
                              A.CAL_DESC,
                              AVAO_LOGIN,
                              AVAO_REPORTE,
                              AVAO_TICKET
                FROM ACO_VISUALIZAR_ANALISIS_OPER T, ACO_CALIFICACION A
               WHERE AVAO_TABLA(+) = A.CAL_CODIGO
                 AND T.AVAO_EMPR(+) = A.CAL_EMPR
                 AND AVAO_OPERADOR(+) = I_OPEM_OPER
                 AND AVAO_PRODUCTO(+) = I_PRODUCTO
                 AND A.CAL_EMPR = 2) LOOP
    
      APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'GENM017_CALIF',
                                 P_N001            => V.CAL_CODIGO,
                                 P_C001            => V.CAL_DESC,
                                 P_C002            => V.AVAO_LOGIN,
                                 P_C003            => V.AVAO_REPORTE,
                                 P_C004            => V.AVAO_TICKET);
    END LOOP;
  END;

  PROCEDURE PP_GUARDAR_OPERADORES(I_GOPER IN GEN_OPERADOR_EMPRESA%ROWTYPE,
                                  I_OPER  IN VARCHAR2) AS
  BEGIN
  
    IF I_OPER = 'INSERT' THEN
      INSERT INTO GEN_OPERADOR_EMPRESA
        (OPEM_OPER,
         OPEM_EMPR,
         OPEM_SUC,
         OPEM_DEP,
         OPEM_DPTO,
         OPEM_SEC,
         OPEM_IND_ADMIN,
         OPEM_IND_HAB_MES_ACT,
         OPEM_AUT_PAGOS,
         OPEM_IND_HAB_MES_STK,
         OPEM_IND_MODIF_CONTRATO,
         OPEM_IND_DELETE_CONTRATO,
         OPEM_AUT_CONTRATO,
         OPEM_TIPO_MENU,
         OPEM_ORDEN_MENU,
         OPEM_AGRUP_MENU,
         OPEM_MOSTRAR_MENU,
         OPEM_AUT_PRODUCCION,
         OPEM_AUT_PERD_AJUSTE,
         OPEM_MOD_TASA,
         OPEM_IND_VER_CONC_RESTRINGIDO,
         OPEM_IND_APRUEBA_PRESTAMO,
         OPEM_CLAVE_SUPERVISOR,
         OPEM_IND_PESO_MANUAL,
         OPEM_ADM_SOL_COMPRAS,
         OPEM_IND_APRUEBA_OC,
         OPEM_PROTEC_HOL,
         OPEM_SUP_SALIDA,
         OPEM_CLAVE_SALIDA,
         OPEM_ENC_SUC_COM,
         OPEM_PROTEC_DOC_COM,
         OPEM_CAMBIA_FCON_FCRE,
         OPEM_VER_OBS_HIS,
         OPEM_MODIF_OBS_HIS,
         OPEM_REIMPRESO,
         OPEM_ADEL_ENC_DEP,
         OPEM_ADEL_ENC_SUP,
         OPEM_PER_CIERRE,
         OPEM_PER_VER_OBS,
         OPEM_PER_AG_OBS,
         OPEM_CTRL_PERSONA,
         OPEM_IND_SUCURSAL,
         OPEM_IND_VISTA_PED_INSUMO,
         OPEM_ACC_BIENES_EN_FICHA,
         OPEM_ACC_TOT_SOL_CRED,
         OPEM_BASCULA,
         OPEM_CAMB_CLAVE,
         OPEM_CODIGO,
         OPEM_DATE_HAB_BASCULA,
         OPEM_DESC_ABREV,
         OPEM_EMITE_DESEMBOLSO,
         OPEM_HAB_AP_LIQ,
         OPEM_HAB_DTO_INT_LIQ,
         OPEM_IDIOMA,
         OPEM_IMPRESORA,
         OPEM_IND_ADM_ACOPIO,
         OPEM_IND_ANUL_ENTRA_GRANO,
         OPEM_IND_ANUL_REM_ACO,
         OPEM_IND_AUD_PRECIO_VTA_INS,
         OPEM_IND_BORRADO_FIN,
         OPEM_IND_CHECK_CARGA,
         OPEM_IND_CRED_COSECHA,
         OPEM_IND_CRED_FUNCIO,
         OPEM_IND_CRED_TALLER,
         OPEM_IND_CRED_VEN_CLI,
         OPEM_IND_DOC_ESTADO,
         OPEM_IND_FACT_COMB_GS,
         OPEM_IND_FACT_SIN_REM,
         OPEM_IND_FEC_CONS_INT,
         OPEM_IND_HAB_BASCULA,
         OPEM_IND_MOD_GARANTIA,
         OPEM_IND_MOD_PED_INSUM,
         OPEM_IND_MOD_PRECIO_VTA_INS,
         OPEM_IND_MOD_RET_PER,
         OPEM_IND_PAGARE_EMIT,
         OPEM_IND_PET_SERV,
         OPEM_IND_PLANIFICA_PAGOS,
         OPEM_IND_SERIES_NULL,
         OPEM_IND_SUC_DOC_EMIT,
         OPEM_IND_TRAN_SINCONTRATO,
         OPEM_IND_USU_DOC_EMIT,
         OPEM_IND_VERIF_CONTAB,
         OPEM_IND_VERIF_PRECIO,
         OPEM_IND_VER_PROYECCIONES,
         OPEM_IND_VER_VENTA_ACO,
         OPEM_LIM_APROB_US,
         OPEM_MOD_CONTACTOS,
         OPEM_SUC_CUBO_CUCOB_LC,
         OPEM_TIPC_CUBO_CUCOB_LC,
         OPEM_IMG_BLOB,
         OPEM_AUT_SBP_KG_CA,
         OPEM_ACC_CATEGORIA_CLIENTE,
         OPEM_IND_COSTO_INSUMO,
         OPEM_IND_DESACT_FAC_MERMA,
         OPEM_INF_HAB_FEC_MERMA,
         OPEM_IND_MOD_HS_GPS,
         OPEM_IND_HAB_FEC_GPS,
         OPEM_IND_PERM_CAMBIO_VEND,
         OPEM_FORMATO_MAQ,
         OPEM_PATH_MAQ_ANALIZADOR,
         OPEM_IND_PERM_MANEJO_CONTR,
         OPEM_IND_PERM_EDIT_EVAL,
         OPEM_IND_PEST_CAST_NCR,
         OPEM_IND_MOD_AUSENCIA,
         OPEM_IND_TICKET_SOPORTE_JEFE,
         OPEM_SUC_FIJACION,
         OPEM_IND_ARTICULO_COSTO,
         OPEM_IND_PERM_EVAL_CODUCT,
         OPEM_IND_HAB_STOCK_VIRTUAL,
         OPEM_IND_LIMITE_CRE_CUOTA,
         OPEM_IND_APROB_ADEL_PROV,
         OPEM_IND_ANUL_TOMA_INV,
         OPEM_IND_ELIM_OBS,
         OPEM_IND_PERM_SUC_PEDIDO,
         OPEM_IND_ELIM_DTO_HOLD,
         OPEM_MOD_ESTADO_CLI,
         OPEM_IND_SOL_CR_REF,
         OPEM_IND_VER_LOTE,
         OPEM_IND_VER_CUENTA,
         OPEM_IND_VER_GARANTIA,
         OPEM_IND_VER_SILO,
         OPEM_IND_HAB_ESP_FIN,
         OPEM_IND_CONSUMO_TRANSP,
         OPEM_IND_PERM_BUSCULA,
         OPEM_IND_ABRIR_OCARGA,
         OPEM_IND_ALERTA,
         OPEM_IND_MOSTRAR_ALERTA,
         OPEM_IND_ABRIR_OT,
         OPEM_SILO_PROV_REM,
         OPEM_IND_ABRIR_OC_F,
         OPEM_IND_DEP_DIRECTO)
      VALUES
        (I_GOPER.OPEM_OPER,
         I_GOPER.OPEM_EMPR,
         I_GOPER.OPEM_SUC,
         I_GOPER.OPEM_DEP,
         I_GOPER.OPEM_DPTO,
         I_GOPER.OPEM_SEC,
         I_GOPER.OPEM_IND_ADMIN,
         I_GOPER.OPEM_IND_HAB_MES_ACT,
         I_GOPER.OPEM_AUT_PAGOS,
         I_GOPER.OPEM_IND_HAB_MES_STK,
         I_GOPER.OPEM_IND_MODIF_CONTRATO,
         I_GOPER.OPEM_IND_DELETE_CONTRATO,
         I_GOPER.OPEM_AUT_CONTRATO,
         I_GOPER.OPEM_TIPO_MENU,
         I_GOPER.OPEM_ORDEN_MENU,
         I_GOPER.OPEM_AGRUP_MENU,
         I_GOPER.OPEM_MOSTRAR_MENU,
         I_GOPER.OPEM_AUT_PRODUCCION,
         I_GOPER.OPEM_AUT_PERD_AJUSTE,
         I_GOPER.OPEM_MOD_TASA,
         I_GOPER.OPEM_IND_VER_CONC_RESTRINGIDO,
         I_GOPER.OPEM_IND_APRUEBA_PRESTAMO,
         I_GOPER.OPEM_CLAVE_SUPERVISOR,
         I_GOPER.OPEM_IND_PESO_MANUAL,
         I_GOPER.OPEM_ADM_SOL_COMPRAS,
         I_GOPER.OPEM_IND_APRUEBA_OC,
         I_GOPER.OPEM_PROTEC_HOL,
         I_GOPER.OPEM_SUP_SALIDA,
         I_GOPER.OPEM_CLAVE_SALIDA,
         I_GOPER.OPEM_ENC_SUC_COM,
         I_GOPER.OPEM_PROTEC_DOC_COM,
         I_GOPER.OPEM_CAMBIA_FCON_FCRE,
         I_GOPER.OPEM_VER_OBS_HIS,
         I_GOPER.OPEM_MODIF_OBS_HIS,
         I_GOPER.OPEM_REIMPRESO,
         I_GOPER.OPEM_ADEL_ENC_DEP,
         I_GOPER.OPEM_ADEL_ENC_SUP,
         I_GOPER.OPEM_PER_CIERRE,
         I_GOPER.OPEM_PER_VER_OBS,
         I_GOPER.OPEM_PER_AG_OBS,
         I_GOPER.OPEM_CTRL_PERSONA,
         I_GOPER.OPEM_IND_SUCURSAL,
         I_GOPER.OPEM_IND_VISTA_PED_INSUMO,
         I_GOPER.OPEM_ACC_BIENES_EN_FICHA,
         I_GOPER.OPEM_ACC_TOT_SOL_CRED,
         I_GOPER.OPEM_BASCULA,
         I_GOPER.OPEM_CAMB_CLAVE,
         I_GOPER.OPEM_CODIGO,
         I_GOPER.OPEM_DATE_HAB_BASCULA,
         I_GOPER.OPEM_DESC_ABREV,
         I_GOPER.OPEM_EMITE_DESEMBOLSO,
         I_GOPER.OPEM_HAB_AP_LIQ,
         I_GOPER.OPEM_HAB_DTO_INT_LIQ,
         I_GOPER.OPEM_IDIOMA,
         I_GOPER.OPEM_IMPRESORA,
         I_GOPER.OPEM_IND_ADM_ACOPIO,
         I_GOPER.OPEM_IND_ANUL_ENTRA_GRANO,
         I_GOPER.OPEM_IND_ANUL_REM_ACO,
         I_GOPER.OPEM_IND_AUD_PRECIO_VTA_INS,
         I_GOPER.OPEM_IND_BORRADO_FIN,
         I_GOPER.OPEM_IND_CHECK_CARGA,
         I_GOPER.OPEM_IND_CRED_COSECHA,
         I_GOPER.OPEM_IND_CRED_FUNCIO,
         I_GOPER.OPEM_IND_CRED_TALLER,
         I_GOPER.OPEM_IND_CRED_VEN_CLI,
         I_GOPER.OPEM_IND_DOC_ESTADO,
         I_GOPER.OPEM_IND_FACT_COMB_GS,
         I_GOPER.OPEM_IND_FACT_SIN_REM,
         I_GOPER.OPEM_IND_FEC_CONS_INT,
         I_GOPER.OPEM_IND_HAB_BASCULA,
         I_GOPER.OPEM_IND_MOD_GARANTIA,
         I_GOPER.OPEM_IND_MOD_PED_INSUM,
         I_GOPER.OPEM_IND_MOD_PRECIO_VTA_INS,
         I_GOPER.OPEM_IND_MOD_RET_PER,
         I_GOPER.OPEM_IND_PAGARE_EMIT,
         I_GOPER.OPEM_IND_PET_SERV,
         I_GOPER.OPEM_IND_PLANIFICA_PAGOS,
         I_GOPER.OPEM_IND_SERIES_NULL,
         I_GOPER.OPEM_IND_SUC_DOC_EMIT,
         I_GOPER.OPEM_IND_TRAN_SINCONTRATO,
         I_GOPER.OPEM_IND_USU_DOC_EMIT,
         I_GOPER.OPEM_IND_VERIF_CONTAB,
         I_GOPER.OPEM_IND_VERIF_PRECIO,
         I_GOPER.OPEM_IND_VER_PROYECCIONES,
         I_GOPER.OPEM_IND_VER_VENTA_ACO,
         I_GOPER.OPEM_LIM_APROB_US,
         I_GOPER.OPEM_MOD_CONTACTOS,
         I_GOPER.OPEM_SUC_CUBO_CUCOB_LC,
         I_GOPER.OPEM_TIPC_CUBO_CUCOB_LC,
         I_GOPER.OPEM_IMG_BLOB,
         I_GOPER.OPEM_AUT_SBP_KG_CA,
         I_GOPER.OPEM_ACC_CATEGORIA_CLIENTE,
         I_GOPER.OPEM_IND_COSTO_INSUMO,
         I_GOPER.OPEM_IND_DESACT_FAC_MERMA,
         I_GOPER.OPEM_INF_HAB_FEC_MERMA,
         I_GOPER.OPEM_IND_MOD_HS_GPS,
         I_GOPER.OPEM_IND_HAB_FEC_GPS,
         I_GOPER.OPEM_IND_PERM_CAMBIO_VEND,
         I_GOPER.OPEM_FORMATO_MAQ,
         I_GOPER.OPEM_PATH_MAQ_ANALIZADOR,
         I_GOPER.OPEM_IND_PERM_MANEJO_CONTR,
         I_GOPER.OPEM_IND_PERM_EDIT_EVAL,
         I_GOPER.OPEM_IND_PEST_CAST_NCR,
         I_GOPER.OPEM_IND_MOD_AUSENCIA,
         I_GOPER.OPEM_IND_TICKET_SOPORTE_JEFE,
         I_GOPER.OPEM_SUC_FIJACION,
         I_GOPER.OPEM_IND_ARTICULO_COSTO,
         I_GOPER.OPEM_IND_PERM_EVAL_CODUCT,
         I_GOPER.OPEM_IND_HAB_STOCK_VIRTUAL,
         I_GOPER.OPEM_IND_LIMITE_CRE_CUOTA,
         I_GOPER.OPEM_IND_APROB_ADEL_PROV,
         I_GOPER.OPEM_IND_ANUL_TOMA_INV,
         I_GOPER.OPEM_IND_ELIM_OBS,
         I_GOPER.OPEM_IND_PERM_SUC_PEDIDO,
         I_GOPER.OPEM_IND_ELIM_DTO_HOLD,
         I_GOPER.OPEM_MOD_ESTADO_CLI,
         I_GOPER.OPEM_IND_SOL_CR_REF,
         I_GOPER.OPEM_IND_VER_LOTE,
         I_GOPER.OPEM_IND_VER_CUENTA,
         I_GOPER.OPEM_IND_VER_GARANTIA,
         I_GOPER.OPEM_IND_VER_SILO,
         I_GOPER.OPEM_IND_HAB_ESP_FIN,
         I_GOPER.OPEM_IND_CONSUMO_TRANSP,
         I_GOPER.OPEM_IND_PERM_BUSCULA,
         I_GOPER.OPEM_IND_ABRIR_OCARGA,
         I_GOPER.OPEM_IND_ALERTA,
         I_GOPER.OPEM_IND_MOSTRAR_ALERTA,
         I_GOPER.OPEM_IND_ABRIR_OT,
         I_GOPER.OPEM_SILO_PROV_REM,
         I_GOPER.OPEM_IND_ABRIR_OC_F,
         I_GOPER.OPEM_IND_DEP_DIRECTO);
    
    ELSIF I_OPER = 'UPDATE' THEN
    
      UPDATE GEN_OPERADOR_EMPRESA
         SET ---OPEM_OPER                     = NVL(I_GOPER.OPEM_OPER,
             -- OPEM_EMPR                     = NVL(I_GOPER.OPEM_EMPR,
                                   OPEM_SUC = NVL(I_GOPER.OPEM_SUC, OPEM_SUC),
             OPEM_DEP                      = NVL(I_GOPER.OPEM_DEP, OPEM_DEP),
             OPEM_DPTO                     = NVL(I_GOPER.OPEM_DPTO,
                                                 OPEM_DPTO),
             OPEM_SEC                      = NVL(I_GOPER.OPEM_SEC, OPEM_SEC),
             OPEM_IND_ADMIN                = NVL(I_GOPER.OPEM_IND_ADMIN,
                                                 OPEM_IND_ADMIN),
             OPEM_IND_HAB_MES_ACT          = NVL(I_GOPER.OPEM_IND_HAB_MES_ACT,
                                                 OPEM_IND_HAB_MES_ACT),
             OPEM_AUT_PAGOS                = NVL(I_GOPER.OPEM_AUT_PAGOS,
                                                 OPEM_AUT_PAGOS),
             OPEM_IND_HAB_MES_STK          = NVL(I_GOPER.OPEM_IND_HAB_MES_STK,
                                                 OPEM_IND_HAB_MES_STK),
             OPEM_IND_MODIF_CONTRATO       = NVL(I_GOPER.OPEM_IND_MODIF_CONTRATO,
                                                 OPEM_IND_MODIF_CONTRATO),
             OPEM_IND_DELETE_CONTRATO      = NVL(I_GOPER.OPEM_IND_DELETE_CONTRATO,
                                                 OPEM_IND_DELETE_CONTRATO),
             OPEM_AUT_CONTRATO             = NVL(I_GOPER.OPEM_AUT_CONTRATO,
                                                 OPEM_AUT_CONTRATO),
             OPEM_TIPO_MENU                = NVL(I_GOPER.OPEM_TIPO_MENU,
                                                 OPEM_TIPO_MENU),
             OPEM_ORDEN_MENU               = NVL(I_GOPER.OPEM_ORDEN_MENU,
                                                 OPEM_ORDEN_MENU),
             OPEM_AGRUP_MENU               = NVL(I_GOPER.OPEM_AGRUP_MENU,
                                                 OPEM_AGRUP_MENU),
             OPEM_MOSTRAR_MENU             = NVL(I_GOPER.OPEM_MOSTRAR_MENU,
                                                 OPEM_MOSTRAR_MENU),
             OPEM_AUT_PRODUCCION           = NVL(I_GOPER.OPEM_AUT_PRODUCCION,
                                                 OPEM_AUT_PRODUCCION),
             OPEM_AUT_PERD_AJUSTE          = NVL(I_GOPER.OPEM_AUT_PERD_AJUSTE,
                                                 OPEM_AUT_PERD_AJUSTE),
             OPEM_MOD_TASA                 = NVL(I_GOPER.OPEM_MOD_TASA,
                                                 OPEM_MOD_TASA),
             OPEM_IND_VER_CONC_RESTRINGIDO = NVL(I_GOPER.OPEM_IND_VER_CONC_RESTRINGIDO,
                                                 OPEM_IND_VER_CONC_RESTRINGIDO),
             OPEM_IND_APRUEBA_PRESTAMO     = NVL(I_GOPER.OPEM_IND_APRUEBA_PRESTAMO,
                                                 OPEM_IND_APRUEBA_PRESTAMO),
             OPEM_CLAVE_SUPERVISOR         = NVL(I_GOPER.OPEM_CLAVE_SUPERVISOR,
                                                 OPEM_CLAVE_SUPERVISOR),
             OPEM_IND_PESO_MANUAL          = NVL(I_GOPER.OPEM_IND_PESO_MANUAL,
                                                 OPEM_IND_PESO_MANUAL),
             OPEM_ADM_SOL_COMPRAS          = NVL(I_GOPER.OPEM_ADM_SOL_COMPRAS,
                                                 OPEM_ADM_SOL_COMPRAS),
             OPEM_IND_APRUEBA_OC           = NVL(I_GOPER.OPEM_IND_APRUEBA_OC,
                                                 OPEM_IND_APRUEBA_OC),
             OPEM_PROTEC_HOL               = NVL(I_GOPER.OPEM_PROTEC_HOL,
                                                 OPEM_PROTEC_HOL),
             OPEM_SUP_SALIDA               = NVL(I_GOPER.OPEM_SUP_SALIDA,
                                                 OPEM_SUP_SALIDA),
             OPEM_CLAVE_SALIDA             = NVL(I_GOPER.OPEM_CLAVE_SALIDA,
                                                 OPEM_CLAVE_SALIDA),
             OPEM_ENC_SUC_COM              = NVL(I_GOPER.OPEM_ENC_SUC_COM,
                                                 OPEM_ENC_SUC_COM),
             OPEM_PROTEC_DOC_COM           = NVL(I_GOPER.OPEM_PROTEC_DOC_COM,
                                                 OPEM_PROTEC_DOC_COM),
             OPEM_CAMBIA_FCON_FCRE         = NVL(I_GOPER.OPEM_CAMBIA_FCON_FCRE,
                                                 OPEM_CAMBIA_FCON_FCRE),
             OPEM_VER_OBS_HIS              = NVL(I_GOPER.OPEM_VER_OBS_HIS,
                                                 OPEM_VER_OBS_HIS),
             OPEM_MODIF_OBS_HIS            = NVL(I_GOPER.OPEM_MODIF_OBS_HIS,
                                                 OPEM_MODIF_OBS_HIS),
             OPEM_REIMPRESO                = NVL(I_GOPER.OPEM_REIMPRESO,
                                                 OPEM_REIMPRESO),
             OPEM_ADEL_ENC_DEP             = NVL(I_GOPER.OPEM_ADEL_ENC_DEP,
                                                 OPEM_ADEL_ENC_DEP),
             OPEM_ADEL_ENC_SUP             = NVL(I_GOPER.OPEM_ADEL_ENC_SUP,
                                                 OPEM_ADEL_ENC_SUP),
             OPEM_PER_CIERRE               = NVL(I_GOPER.OPEM_PER_CIERRE,
                                                 OPEM_PER_CIERRE),
             OPEM_PER_VER_OBS              = NVL(I_GOPER.OPEM_PER_VER_OBS,
                                                 OPEM_PER_VER_OBS),
             OPEM_PER_AG_OBS               = NVL(I_GOPER.OPEM_PER_AG_OBS,
                                                 OPEM_PER_AG_OBS),
             OPEM_CTRL_PERSONA             = NVL(I_GOPER.OPEM_CTRL_PERSONA,
                                                 OPEM_CTRL_PERSONA),
             OPEM_IND_SUCURSAL             = NVL(I_GOPER.OPEM_IND_SUCURSAL,
                                                 OPEM_IND_SUCURSAL),
             OPEM_IND_VISTA_PED_INSUMO     = NVL(I_GOPER.OPEM_IND_VISTA_PED_INSUMO,
                                                 OPEM_IND_VISTA_PED_INSUMO),
             OPEM_ACC_BIENES_EN_FICHA      = NVL(I_GOPER.OPEM_ACC_BIENES_EN_FICHA,
                                                 OPEM_ACC_BIENES_EN_FICHA),
             OPEM_ACC_TOT_SOL_CRED         = NVL(I_GOPER.OPEM_ACC_TOT_SOL_CRED,
                                                 OPEM_ACC_TOT_SOL_CRED),
             OPEM_BASCULA                  = NVL(I_GOPER.OPEM_BASCULA,
                                                 OPEM_BASCULA),
             OPEM_CAMB_CLAVE               = NVL(I_GOPER.OPEM_CAMB_CLAVE,
                                                 OPEM_CAMB_CLAVE),
             OPEM_CODIGO                   = NVL(I_GOPER.OPEM_CODIGO,
                                                 OPEM_CODIGO),
             OPEM_DATE_HAB_BASCULA         = NVL(I_GOPER.OPEM_DATE_HAB_BASCULA,
                                                 OPEM_DATE_HAB_BASCULA),
             OPEM_DESC_ABREV               = NVL(I_GOPER.OPEM_DESC_ABREV,
                                                 OPEM_DESC_ABREV),
             OPEM_EMITE_DESEMBOLSO         = NVL(I_GOPER.OPEM_EMITE_DESEMBOLSO,
                                                 OPEM_EMITE_DESEMBOLSO),
             OPEM_HAB_AP_LIQ               = NVL(I_GOPER.OPEM_HAB_AP_LIQ,
                                                 OPEM_HAB_AP_LIQ),
             OPEM_HAB_DTO_INT_LIQ          = NVL(I_GOPER.OPEM_HAB_DTO_INT_LIQ,
                                                 OPEM_HAB_DTO_INT_LIQ),
             OPEM_IDIOMA                   = NVL(I_GOPER.OPEM_IDIOMA,
                                                 OPEM_IDIOMA),
             OPEM_IMPRESORA                = NVL(I_GOPER.OPEM_IMPRESORA,
                                                 OPEM_IMPRESORA),
             OPEM_IND_ADM_ACOPIO           = NVL(I_GOPER.OPEM_IND_ADM_ACOPIO,
                                                 OPEM_IND_ADM_ACOPIO),
             OPEM_IND_ANUL_ENTRA_GRANO     = NVL(I_GOPER.OPEM_IND_ANUL_ENTRA_GRANO,
                                                 OPEM_IND_ANUL_ENTRA_GRANO),
             OPEM_IND_ANUL_REM_ACO         = NVL(I_GOPER.OPEM_IND_ANUL_REM_ACO,
                                                 OPEM_IND_ANUL_REM_ACO),
             OPEM_IND_AUD_PRECIO_VTA_INS   = NVL(I_GOPER.OPEM_IND_AUD_PRECIO_VTA_INS,
                                                 OPEM_IND_AUD_PRECIO_VTA_INS),
             OPEM_IND_BORRADO_FIN          = NVL(I_GOPER.OPEM_IND_BORRADO_FIN,
                                                 OPEM_IND_BORRADO_FIN),
             OPEM_IND_CHECK_CARGA          = NVL(I_GOPER.OPEM_IND_CHECK_CARGA,
                                                 OPEM_IND_CHECK_CARGA),
             OPEM_IND_CRED_COSECHA         = NVL(I_GOPER.OPEM_IND_CRED_COSECHA,
                                                 OPEM_IND_CRED_COSECHA),
             OPEM_IND_CRED_FUNCIO          = NVL(I_GOPER.OPEM_IND_CRED_FUNCIO,
                                                 OPEM_IND_CRED_FUNCIO),
             OPEM_IND_CRED_TALLER          = NVL(I_GOPER.OPEM_IND_CRED_TALLER,
                                                 OPEM_IND_CRED_TALLER),
             OPEM_IND_CRED_VEN_CLI         = NVL(I_GOPER.OPEM_IND_CRED_VEN_CLI,
                                                 OPEM_IND_CRED_VEN_CLI),
             OPEM_IND_DOC_ESTADO           = NVL(I_GOPER.OPEM_IND_DOC_ESTADO,
                                                 OPEM_IND_DOC_ESTADO),
             OPEM_IND_FACT_COMB_GS         = NVL(I_GOPER.OPEM_IND_FACT_COMB_GS,
                                                 OPEM_IND_FACT_COMB_GS),
             OPEM_IND_FACT_SIN_REM         = NVL(I_GOPER.OPEM_IND_FACT_SIN_REM,
                                                 OPEM_IND_FACT_SIN_REM),
             OPEM_IND_FEC_CONS_INT         = NVL(I_GOPER.OPEM_IND_FEC_CONS_INT,
                                                 OPEM_IND_FEC_CONS_INT),
             OPEM_IND_HAB_BASCULA          = NVL(I_GOPER.OPEM_IND_HAB_BASCULA,
                                                 OPEM_IND_HAB_BASCULA),
             OPEM_IND_MOD_GARANTIA         = NVL(I_GOPER.OPEM_IND_MOD_GARANTIA,
                                                 OPEM_IND_MOD_GARANTIA),
             OPEM_IND_MOD_PED_INSUM        = NVL(I_GOPER.OPEM_IND_MOD_PED_INSUM,
                                                 OPEM_IND_ANUL_ENTRA_GRANO),
             OPEM_IND_MOD_PRECIO_VTA_INS   = NVL(I_GOPER.OPEM_IND_MOD_PRECIO_VTA_INS,
                                                 OPEM_IND_MOD_PRECIO_VTA_INS),
             OPEM_IND_MOD_RET_PER          = NVL(I_GOPER.OPEM_IND_MOD_RET_PER,
                                                 OPEM_IND_MOD_RET_PER),
             OPEM_IND_PAGARE_EMIT          = NVL(I_GOPER.OPEM_IND_PAGARE_EMIT,
                                                 OPEM_IND_PAGARE_EMIT),
             OPEM_IND_PET_SERV             = NVL(I_GOPER.OPEM_IND_PET_SERV,
                                                 OPEM_IND_PET_SERV),
             OPEM_IND_PLANIFICA_PAGOS      = NVL(I_GOPER.OPEM_IND_PLANIFICA_PAGOS,
                                                 OPEM_IND_PLANIFICA_PAGOS),
             OPEM_IND_SERIES_NULL          = NVL(I_GOPER.OPEM_IND_SERIES_NULL,
                                                 OPEM_IND_SERIES_NULL),
             OPEM_IND_SUC_DOC_EMIT         = NVL(I_GOPER.OPEM_IND_SUC_DOC_EMIT,
                                                 OPEM_IND_SUC_DOC_EMIT),
             OPEM_IND_TRAN_SINCONTRATO     = NVL(I_GOPER.OPEM_IND_TRAN_SINCONTRATO,
                                                 OPEM_IND_TRAN_SINCONTRATO),
             OPEM_IND_USU_DOC_EMIT         = NVL(I_GOPER.OPEM_IND_USU_DOC_EMIT,
                                                 OPEM_IND_USU_DOC_EMIT),
             OPEM_IND_VERIF_CONTAB         = NVL(I_GOPER.OPEM_IND_VERIF_CONTAB,
                                                 OPEM_IND_VERIF_CONTAB),
             OPEM_IND_VERIF_PRECIO         = NVL(I_GOPER.OPEM_IND_VERIF_PRECIO,
                                                 OPEM_IND_VERIF_PRECIO),
             OPEM_IND_VER_PROYECCIONES     = NVL(I_GOPER.OPEM_IND_VER_PROYECCIONES,
                                                 OPEM_IND_VER_PROYECCIONES),
             OPEM_IND_VER_VENTA_ACO        = NVL(I_GOPER.OPEM_IND_VER_VENTA_ACO,
                                                 OPEM_IND_VER_VENTA_ACO),
             OPEM_LIM_APROB_US             = NVL(I_GOPER.OPEM_LIM_APROB_US,
                                                 OPEM_LIM_APROB_US),
             OPEM_MOD_CONTACTOS            = NVL(I_GOPER.OPEM_MOD_CONTACTOS,
                                                 OPEM_MOD_CONTACTOS),
             OPEM_SUC_CUBO_CUCOB_LC        = NVL(I_GOPER.OPEM_SUC_CUBO_CUCOB_LC,
                                                 OPEM_SUC_CUBO_CUCOB_LC),
             OPEM_TIPC_CUBO_CUCOB_LC       = NVL(I_GOPER.OPEM_TIPC_CUBO_CUCOB_LC,
                                                 OPEM_TIPC_CUBO_CUCOB_LC),
             OPEM_IMG_BLOB                 = NVL(I_GOPER.OPEM_IMG_BLOB,
                                                 OPEM_IMG_BLOB),
             OPEM_AUT_SBP_KG_CA            = NVL(I_GOPER.OPEM_AUT_SBP_KG_CA,
                                                 OPEM_AUT_SBP_KG_CA),
             OPEM_ACC_CATEGORIA_CLIENTE    = NVL(I_GOPER.OPEM_ACC_CATEGORIA_CLIENTE,
                                                 OPEM_ACC_CATEGORIA_CLIENTE),
             OPEM_IND_COSTO_INSUMO         = NVL(I_GOPER.OPEM_IND_COSTO_INSUMO,
                                                 OPEM_IND_COSTO_INSUMO),
             OPEM_IND_DESACT_FAC_MERMA     = NVL(I_GOPER.OPEM_IND_DESACT_FAC_MERMA,
                                                 OPEM_IND_DESACT_FAC_MERMA),
             OPEM_INF_HAB_FEC_MERMA        = NVL(I_GOPER.OPEM_INF_HAB_FEC_MERMA,
                                                 OPEM_INF_HAB_FEC_MERMA),
             OPEM_IND_MOD_HS_GPS           = NVL(I_GOPER.OPEM_IND_MOD_HS_GPS,
                                                 OPEM_IND_MOD_HS_GPS),
             OPEM_IND_HAB_FEC_GPS          = NVL(I_GOPER.OPEM_IND_HAB_FEC_GPS,
                                                 OPEM_IND_HAB_FEC_GPS),
             OPEM_IND_PERMISO_BONOS        = NVL(I_GOPER.OPEM_IND_PERMISO_BONOS,
                                                 OPEM_IND_PERMISO_BONOS),
             OPEM_IND_PERM_CAMBIO_VEND     = NVL(I_GOPER.OPEM_IND_PERM_CAMBIO_VEND,
                                                 OPEM_IND_PERM_CAMBIO_VEND),
             OPEM_FORMATO_MAQ              = NVL(I_GOPER.OPEM_FORMATO_MAQ,
                                                 OPEM_FORMATO_MAQ),
             OPEM_PATH_MAQ_ANALIZADOR      = NVL(I_GOPER.OPEM_PATH_MAQ_ANALIZADOR,
                                                 OPEM_PATH_MAQ_ANALIZADOR),
             OPEM_IND_PERM_MANEJO_CONTR    = NVL(I_GOPER.OPEM_IND_PERM_MANEJO_CONTR,
                                                 OPEM_IND_PERM_MANEJO_CONTR),
             OPEM_IND_PEST_CAST_NCR        = NVL(I_GOPER.OPEM_IND_PEST_CAST_NCR,
                                                 OPEM_IND_PEST_CAST_NCR),
             OPEM_IND_PERM_EDIT_EVAL       = NVL(I_GOPER.OPEM_IND_PERM_EDIT_EVAL,
                                                 OPEM_IND_PERM_EDIT_EVAL),
             OPEM_IND_MOD_AUSENCIA         = NVL(I_GOPER.OPEM_IND_MOD_AUSENCIA,
                                                 OPEM_IND_MOD_AUSENCIA),
             OPEM_IND_TICKET_SOPORTE_JEFE  = NVL(I_GOPER.OPEM_IND_TICKET_SOPORTE_JEFE,
                                                 OPEM_IND_TICKET_SOPORTE_JEFE),
             OPEM_SUC_FIJACION             = NVL(I_GOPER.OPEM_SUC_FIJACION,
                                                 OPEM_SUC_FIJACION),
             OPEM_IND_ARTICULO_COSTO       = NVL(I_GOPER.OPEM_IND_ARTICULO_COSTO,
                                                 OPEM_IND_ARTICULO_COSTO),
             OPEM_IND_PERM_EVAL_CODUCT     = NVL(I_GOPER.OPEM_IND_PERM_EVAL_CODUCT,
                                                 OPEM_IND_PERM_EVAL_CODUCT),
             OPEM_IND_HAB_STOCK_VIRTUAL    = NVL(I_GOPER.OPEM_IND_HAB_STOCK_VIRTUAL,
                                                 OPEM_IND_HAB_STOCK_VIRTUAL),
             OPEM_IND_LIMITE_CRE_CUOTA     = NVL(I_GOPER.OPEM_IND_LIMITE_CRE_CUOTA,
                                                 OPEM_IND_LIMITE_CRE_CUOTA),
             OPEM_IND_APROB_ADEL_PROV      = NVL(I_GOPER.OPEM_IND_APROB_ADEL_PROV,
                                                 OPEM_IND_APROB_ADEL_PROV),
             OPEM_IND_ANUL_TOMA_INV        = NVL(I_GOPER.OPEM_IND_ANUL_TOMA_INV,
                                                 OPEM_IND_ANUL_TOMA_INV),
             OPEM_IND_ELIM_OBS             = NVL(I_GOPER.OPEM_IND_ELIM_OBS,
                                                 OPEM_IND_ELIM_OBS),
             OPEM_IND_PERM_SUC_PEDIDO      = NVL(I_GOPER.OPEM_IND_PERM_SUC_PEDIDO,
                                                 OPEM_IND_PERM_SUC_PEDIDO),
             OPEM_IND_ELIM_DTO_HOLD        = NVL(I_GOPER.OPEM_IND_ELIM_DTO_HOLD,
                                                 OPEM_IND_ELIM_DTO_HOLD),
             OPEM_MOD_ESTADO_CLI           = NVL(I_GOPER.OPEM_MOD_ESTADO_CLI,
                                                 OPEM_MOD_ESTADO_CLI),
             OPEM_IND_SOL_CR_REF           = NVL(I_GOPER.OPEM_IND_SOL_CR_REF,
                                                 OPEM_IND_SOL_CR_REF),
             OPEM_IND_VER_LOTE             = NVL(I_GOPER.OPEM_IND_VER_LOTE,
                                                 OPEM_IND_VER_LOTE),
             OPEM_IND_VER_CUENTA           = NVL(I_GOPER.OPEM_IND_VER_CUENTA,
                                                 OPEM_IND_VER_CUENTA),
             OPEM_IND_VER_GARANTIA         = NVL(I_GOPER.OPEM_IND_VER_GARANTIA,
                                                 OPEM_IND_VER_GARANTIA),
             OPEM_IND_VER_SILO             = NVL(I_GOPER.OPEM_IND_VER_SILO,
                                                 OPEM_IND_VER_SILO),
             OPEM_IND_HAB_ESP_FIN          = NVL(I_GOPER.OPEM_IND_HAB_ESP_FIN,
                                                 OPEM_IND_HAB_ESP_FIN),
             OPEM_IND_CONSUMO_TRANSP       = NVL(I_GOPER.OPEM_IND_CONSUMO_TRANSP,
                                                 OPEM_IND_CONSUMO_TRANSP),
             OPEM_IND_PERM_BUSCULA         = NVL(I_GOPER.OPEM_IND_PERM_BUSCULA,
                                                 OPEM_IND_PERM_BUSCULA),
             OPEM_IND_ABRIR_OCARGA         = NVL(I_GOPER.OPEM_IND_ABRIR_OCARGA,
                                                 OPEM_IND_ABRIR_OCARGA),
             OPEM_IND_ALERTA               = NVL(I_GOPER.OPEM_IND_ALERTA,
                                                 OPEM_IND_ALERTA),
             OPEM_IND_MOSTRAR_ALERTA       = NVL(I_GOPER.OPEM_IND_MOSTRAR_ALERTA,
                                                 OPEM_IND_MOSTRAR_ALERTA),
             OPEM_IND_ABRIR_OT             = I_GOPER.OPEM_IND_ABRIR_OT,
             OPEM_SILO_PROV_REM            = NVL(I_GOPER.OPEM_SILO_PROV_REM,
                                                 OPEM_SILO_PROV_REM),
             OPEM_IND_ABRIR_OC_F           = NVL(I_GOPER.OPEM_IND_ABRIR_OC_F,
                                                 OPEM_IND_ABRIR_OC_F),
             OPEM_IND_DEP_DIRECTO          =  NVL(I_GOPER.OPEM_IND_DEP_DIRECTO,
                                                 OPEM_IND_DEP_DIRECTO)                                  
       WHERE OPEM_OPER = I_GOPER.OPEM_OPER
         AND OPEM_EMPR = I_GOPER.OPEM_EMPR;
    
    ELSIF I_OPER = 'DELETE' THEN
      DELETE GEN_OPERADOR_EMPRESA
       WHERE OPEM_OPER = I_GOPER.OPEM_OPER
         AND OPEM_EMPR = I_GOPER.OPEM_EMPR;
    
    END IF;
  END;

  PROCEDURE PP_ACTULIZAR_OPERADOR(P_EMPRESA           IN NUMBER,
                                  P_OPER_NOMBRE       IN VARCHAR2,
                                  P_OPER_DESC_ABREV   IN VARCHAR2,
                                  P_OPER_MAX_SESSIONS IN VARCHAR2,
                                  P_IMAGEN            IN BLOB,
                                  P_OPER_MENU         IN VARCHAR2,
                                  P_OPER_CODIGO       IN NUMBER,
                                  P_OPER_EMPR         IN NUMBER) AS
  BEGIN
  
    UPDATE GEN_OPERADOR
       SET OPER_NOMBRE       = P_OPER_NOMBRE,
           OPER_DESC_ABREV   = P_OPER_DESC_ABREV,
           OPER_MAX_SESSIONS = P_OPER_MAX_SESSIONS,
           OPER_EMPR         = P_OPER_EMPR,
           OPER_MENU         = P_OPER_MENU,
           OPER_IMG_BLOB     = NVL(P_IMAGEN, OPER_IMG_BLOB)
     WHERE OPER_CODIGO = P_OPER_CODIGO;
  
    IF P_OPER_MAX_SESSIONS = 0 OR P_OPER_MAX_SESSIONS IS NULL THEN
      ---- ELIMINAR LOS PRIVILEGIOS QUE TENIA ASIGNADO EL OPERADOR EN CASO DE QUE EL EMPLEADO ESTE INACTIVO ----
      ---------------------------------------
      DELETE COM_OPER_CONCEPTO_SOL A
       WHERE A.OPERCONC_EMPR = P_EMPRESA
         AND A.OPERCONC_OPER_COD = P_OPER_CODIGO;
      ---------------------------------------
      DELETE FIN_OPER_CTA_BCO
       WHERE OP_CTA_OPER = P_OPER_CODIGO
         AND OP_CTA_EMPR = P_EMPRESA;
      ---------------------------------------
      /*
      DELETE GEN_OPERADOR_EMPRESA
       WHERE OPEM_OPER = P_OPER_CODIGO
         AND OPEM_EMPR = P_EMPRESA;
         */
      ---------------------------------------
      DELETE GEN_OPERADOR_ROL
       WHERE OPRO_OPERADOR = P_OPER_CODIGO
         AND OPRO_EMPR = P_EMPRESA;
      ----------------------------------------
      DELETE GEN_OPER_DEPARTAMENTO
       WHERE OD_OPERADOR = P_OPER_CODIGO
         AND OD_EMPR = P_EMPRESA;
      ----------------------------------------
      DELETE GEN_OPER_PEST_RRHH
       WHERE OPERRHH_OPER = P_OPER_CODIGO
         AND OPERRHH_EMPR = P_EMPRESA;
      -----------------------------------------
      DELETE GEN_OPER_TMOV
       WHERE OPMO_OPERADOR = P_OPER_CODIGO
         AND OPMO_EMPR = P_EMPRESA;
      -----------------------------------------
      DELETE STK_OPER_DEPO
       WHERE STOPE_OPERADOR = P_OPER_CODIGO
         AND STOPE_EMPR = P_EMPRESA;
      -----------------------------------------
      DELETE ACO_TABLA_HAB_ENTSAL
       WHERE ATEN_ROL = P_OPER_CODIGO
         AND ATEN_EMPR = P_EMPRESA;
      ------------------------------------------
    
    END IF;
  
  END;

END GENM017;
/
