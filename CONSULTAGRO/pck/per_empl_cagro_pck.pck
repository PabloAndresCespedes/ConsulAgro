create or replace package per_empl_cagro_pck is

  -- Author  : @PabloACespedes
  -- Created : 09/08/2022 14:27:38
  -- Purpose : cambios de estado en tabla remota de empleados de consultagro
  -- a la fecha solo de consultagro, asumiendo que la tabla solo aloja datos de
  -- esta empresa
  
  procedure eliminar_empleado(
    in_legajo number
  );
  
  procedure add_upd_empleado(
    in_legajo    number,
    in_nombre    varchar2,
    in_apellido  varchar2,
    in_direccion varchar2,
    in_doc_iden  number,
    in_fec_nac   date,
    in_tel       varchar2,
    in_estado    varchar2,
    in_lim_credito number,
    in_cod_proveedor number,
    in_action    varchar2 --> I || U insert or update
  );
  
  procedure cambiar_estado_empleado(
    in_legajo number,
    in_estado varchar2
  );
  
end per_empl_cagro_pck;
/
create or replace package body per_empl_cagro_pck is
  
  procedure eliminar_empleado(
    in_legajo number
  )as
  begin
    delete from per_empleado_cagro c --> esta tabla esta en una DB Remota definida por dBLINK
    where c.legajo = in_legajo;
  end eliminar_empleado;
  
  procedure add_upd_empleado(
    in_legajo      number,
    in_nombre      varchar2,
    in_apellido    varchar2,
    in_direccion   varchar2,
    in_doc_iden    number,
    in_fec_nac     date,
    in_tel         varchar2,
    in_estado      varchar2,
    in_lim_credito number,
    in_cod_proveedor number,
    in_action      varchar2 --> I || U insert or update
  )as
  l_existe number;
  begin
    if in_action = 'I' then
      insert into per_empleado_cagro --> esta tabla esta en una DB Remota definida por dBLINK
      select null,
             in_legajo,
             in_nombre,
             in_apellido,
             in_direccion,
             in_doc_iden,
             in_fec_nac,
             in_tel,
             in_estado,
             in_lim_credito,
             in_cod_proveedor
      from dual;
    else -- U
      <<existe_empleado>>
      begin
        select distinct 1 into l_existe
        from per_empleado_cagro
        where legajo = in_legajo;
        
        update per_empleado_cagro e
        set   e.legajo   = in_legajo,
              e.nombre   = in_nombre,
              e.apellido = in_apellido,
              e.direccion = in_direccion,
              e.documento_identidad = in_doc_iden,
              e.fecha_nac = in_fec_nac,
              e.telefono  = in_tel,
              e.estado    = in_estado,
              e.limite_credito = in_lim_credito,
              e.proveedor_id = in_cod_proveedor
        where e.legajo = in_legajo;
      
      exception
        when no_data_found then
          insert into per_empleado_cagro --> esta tabla esta en una DB Remota definida por dBLINK
          select null,
                 in_legajo,
                 in_nombre,
                 in_apellido,
                 in_direccion,
                 in_doc_iden,
                 in_fec_nac,
                 in_tel,
                 in_estado,
                 in_lim_credito,
                 in_cod_proveedor
          from dual;
      end existe_empleado;
    end if;
  exception
    when dup_val_on_index then --> duplicados no pasa nada
      null;
  end add_upd_empleado;
  
  procedure cambiar_estado_empleado(
    in_legajo number,
    in_estado varchar2
  )as
  begin
    update per_empleado_cagro --> esta tabla esta en una DB Remota definida por dBLINK
    set    estado = in_estado
    where  legajo = in_legajo;
  end cambiar_estado_empleado;
  
end per_empl_cagro_pck;
/
