create or replace package paq_empresas is

  -- Author  : @PabloACespedes
  -- Created : 27/07/2022 9:09:45
  -- Purpose : Libreria de empresas para configuraciones u objetos de DDBB
  
  co_consultagro     constant number := 1;
  co_consultagro_rem constant number := 22; --> remoto en DB Hilagro
  
  function consultagro return number;

  function consultagro_remoto return number;
  
end paq_empresas;
/
create or replace package body paq_empresas is

  function consultagro return number is
  begin
    return co_consultagro;
  end consultagro;
  
  function consultagro_remoto return number is
  begin
    return co_consultagro_rem;
  end consultagro_remoto;
  
end paq_empresas;
/
