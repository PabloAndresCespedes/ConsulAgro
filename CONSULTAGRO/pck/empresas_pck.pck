create or replace package empresas_pck is

  -- Author  : @PabloACespedes
  -- Created : 10/08/2022 15:45:31
  -- Purpose : libreria de empresas
  
  co_consultagro constant gen_empresa.empr_codigo%type := 1;
  
  function consultagro return gen_empresa.empr_codigo%type;
  
end empresas_pck;
/
create or replace package body empresas_pck is

  function consultagro return gen_empresa.empr_codigo%type is
  begin
    return co_consultagro;
  end consultagro;
  
end empresas_pck;
/
