create or replace package fini238 is

  -- Author  : @PabloACespedes
  -- Created : 24/08/2022 10:12:12
  -- Purpose : Importacion de documentos externos
  
  procedure importar_datos(
    in_empresa     in number
  );
  
  procedure revertir_importacion(
    in_importacion in number
  );
  
end fini238;
/
create or replace package body fini238 is
  
  co_empr_cagro   constant number := paq_empresas.co_consultagro;
  co_cagro_remoto constant number := paq_empresas.consultagro_remoto;
  
  -- generar importacion
  function generar_importacion(in_empresa number)
    return fin_import_doc_ext_cab.id%type
  is
  l_new_id fin_import_doc_ext_cab.id%type;
  l_user   varchar2(50);
  begin
    
    l_user := coalesce(sys_context('APEX$SESSION','APP_USER'),user);
    
    insert into fin_import_doc_ext_cab(app_user, empresa_id)
    values(l_user, in_empresa)
    returning id into l_new_id;
    
    return l_new_id;
  
  end generar_importacion;
  
  procedure agg_detalle_imporacion(
    in_importacion_id in number,
    in_clave_externa  in number,
    in_clave_local    in number,
    in_empresa        in number
  )as
  begin
    insert into fin_import_doc_ext_det
    (
     import_id,
     doc_clave_externa,
     doc_clave_local,
     empresa_id
    )
    values(
     in_importacion_id,
     in_clave_externa,
     in_clave_local,
     in_empresa
    );
    
  end agg_detalle_imporacion;
  
  procedure importar_datos(
    in_empresa     in number
  )as
  l_doc_clave     fin_documento.doc_clave%type;
  l_empresa       number;
  l_importacion   number;
  l_clave_externa number;
  l_cta_adelanto_prov number;
  begin
    -- inicializacion variables
    l_empresa     := in_empresa;
    l_importacion := generar_importacion( in_empresa => l_empresa );
        
    -- ADD FIN_DOCUMENTO
    <<agg_fin_documento>>
    for fd in (select
           DOC_CLAVE doc_clave_externa,
           l_empresa doc_empr,
           null cta_bco,
           1 DOC_SUC,
           DOC_TIPO_MOV,
           DOC_NRO_DOC,
           DOC_TIPO_SALDO,
           DOC_MON,
           DOC_PROV,
           DOC_CLI_NOM,
           NVL(DOC_FEC_OPER, sysdate) f_operacion,
           NVL(DOC_FEC_DOC, sysdate) f_documento,
           DOC_BRUTO_EXEN_LOC,
           DOC_BRUTO_EXEN_MON,
           DOC_BRUTO_GRAV_LOC,
           DOC_BRUTO_GRAV_MON,
           DOC_NETO_EXEN_LOC,
           DOC_NETO_EXEN_MON,
           DOC_NETO_GRAV_LOC,
           DOC_NETO_GRAV_MON,
           DOC_IVA_LOC,
           DOC_IVA_MON,
           DOC_OBS,
           DOC_BASE_IMPON_LOC,
           DOC_BASE_IMPON_MON,
           DOC_DIF_CAMBIO,
           DOC_LOGIN,
           sysdate f_grabacion,
           DOC_SIST,
           DOC_OPERADOR,
           DOC_TASA,
           DOC_DIF_CAMBIO_ACUM,
           DOC_EMPLEADO,
           DOC_IND_CANC_PMO,
           fc.fcon_clave_ctaco, --> se_envia en DOC_CTACO, pero en realidad es el concepto
           DOC_NRO_TRANS_COMBUS,
           DOC_CLAVE_PADRE_ORIG
    from fin_documento_cagro f
    inner join fin_proveedor p on (p.prov_codigo = f.doc_prov)
    inner join per_empleado e on ( e.empl_legajo = f.doc_empleado and e.empl_empresa = p.prov_empr )
    inner join gen_tipo_mov tm on ( tm.tmov_codigo = f.doc_tipo_mov and tm.tmov_empr = e.empl_empresa )
    inner join fin_concepto fc on (fc.fcon_clave = f.DOC_CTACO )
    left  join fin_import_doc_ext_det ed on (ed.doc_clave_externa = f.doc_clave )
    where p.prov_empr   = l_empresa
    and   fc.fcon_empr  = l_empresa
    and   ed.id is null
    and   ((ed.id is not null and ed.empresa_id = l_empresa or ed.id is null))
    -- en Hilagro consultagro es 22 pero aqui en CAGRO ES 1, por eso la conversion de id's
    and   case when f.doc_empr = co_cagro_remoto then co_empr_cagro else f.doc_empr end = l_empresa
    )
    loop
      -- PK
      l_doc_clave     := FIN_SEQ_DOC_NEXTVAL;
      l_clave_externa := fd.doc_clave_externa;
      
      INSERT INTO FIN_DOCUMENTO
              (doc_clave,
               doc_empr,
               doc_cta_bco,
               doc_suc,
               doc_tipo_mov,
               doc_nro_doc,
               doc_tipo_saldo,
               doc_mon,
               doc_prov,
               doc_cli_nom,
               doc_fec_oper,
               doc_fec_doc,
               doc_bruto_exen_loc,
               doc_bruto_exen_mon,
               doc_bruto_grav_loc,
               doc_bruto_grav_mon,
               doc_neto_exen_loc,
               doc_neto_exen_mon,
               doc_neto_grav_loc,
               doc_neto_grav_mon,
               doc_iva_loc,
               doc_iva_mon,
               doc_obs,
               doc_base_impon_loc,
               doc_base_impon_mon,
               doc_dif_cambio,
               doc_login,
               doc_fec_grab,
               doc_sist,
               doc_operador,
               doc_tasa,
               doc_dif_cambio_acum,
               doc_empleado,
               doc_ind_canc_pmo,
               doc_ctaco,
               doc_nro_trans_combus,
               doc_clave_padre_orig
      )values(l_doc_clave,
              fd.doc_empr,
              fd.cta_bco,
              fd.doc_suc,
              fd.doc_tipo_mov,
              fd.doc_nro_doc,
              fd.doc_tipo_saldo,
              fd.doc_mon,
              fd.doc_prov,
              fd.doc_cli_nom,
              fd.f_operacion,
              fd.f_documento,
              fd.doc_bruto_exen_loc,
              fd.doc_bruto_exen_mon,
              fd.doc_bruto_grav_loc,
              fd.doc_bruto_grav_mon,
              fd.doc_neto_exen_loc,
              fd.doc_neto_exen_mon,
              fd.doc_neto_grav_loc,
              fd.doc_neto_grav_mon,
              fd.doc_iva_loc,
              fd.doc_iva_mon,
              fd.doc_obs,
              fd.doc_base_impon_loc,
              fd.doc_base_impon_mon,
              fd.doc_dif_cambio,
              fd.doc_login,
              fd.f_grabacion,
              fd.doc_sist,
              fd.doc_operador,
              fd.doc_tasa,
              fd.doc_dif_cambio_acum,
              fd.doc_empleado,
              fd.doc_ind_canc_pmo,
              fd.fcon_clave_ctaco, --> se_envia en doc_ctaco, pero en realidad es el concepto
              fd.doc_nro_trans_combus,
              fd.doc_clave_padre_orig
      );
      
      -- registrar detalle de la importacion
      agg_detalle_imporacion(
        in_importacion_id => l_importacion,
        in_clave_externa  => l_clave_externa,
        in_clave_local    => l_doc_clave,
        in_empresa        => l_empresa
      );
      
      <<obt_cta_adel_prov>>
      begin
        select f.conf_cta_adel_prov
        into l_cta_adelanto_prov
        from fin_configuracion f
        where f.conf_empr = l_empresa;

        if l_cta_adelanto_prov is null then
          raise no_data_found;
        end if;

      exception
        when no_data_found then
          Raise_application_error(-20000, 'Es necesario configurar en la empresa la cuenta de adelanto proveedor');
      end obt_cta_adel_prov;
          
      <<agg_fin_doc_concepto>>
      begin
        insert into fin_doc_concepto
                (dcon_empr,
                 dcon_clave_doc,
                 dcon_item,
                 dcon_clave_concepto,
                 dcon_clave_ctaco,
                 dcon_tipo_saldo,
                 dcon_exen_loc,
                 dcon_exen_mon,
                 dcon_grav_loc,
                 dcon_grav_mon,
                 dcon_iva_loc,
                 dcon_iva_mon,
                 dcon_ind_tipo_iva_compra,
                 dcon_obs
                 )
        select l_empresa,
               l_doc_clave,
               c.dcon_item,
               cc.fcon_clave       concepto_clave,
               l_cta_adelanto_prov cuenta_clave,
               c.dcon_tipo_saldo,
               c.dcon_exen_loc,
               c.dcon_exen_mon,
               c.dcon_grav_loc,
               c.dcon_grav_mon,
               c.dcon_iva_loc,
               c.dcon_iva_mon,
               c.dcon_ind_tipo_iva_compra,
               c.dcon_obs
        from fin_doc_concepto_cagro c
        inner join fin_concepto cc on ( cc.fcon_clave = c.dcon_clave_concepto )
        -- en Hilagro consultagro es 22 pero aqui en CAGRO ES 1, por eso la conversion de id's
        where case when c.dcon_empr = co_cagro_remoto then co_empr_cagro else c.dcon_empr end = l_empresa
        and   cc.fcon_empr     = l_empresa
        and   c.DCON_CLAVE_DOC = l_clave_externa;
      end agg_fin_doc_concepto;
      
      <<agg_fin_cuota>>
      begin
        -- ADD FIN_CUOTA
        insert into fin_cuota
                (cuo_clave_doc,
                 cuo_fec_vto,
                 cuo_imp_loc,
                 cuo_imp_mon,
                 cuo_dif_cambio,
                 cuo_empr
                 )
        select l_doc_clave,
               c.cuo_fec_vto,
               c.cuo_imp_loc,
               c.cuo_imp_mon,
               c.cuo_dif_cambio,
               l_empresa
        from fin_cuota_cagro c
        -- en Hilagro consultagro es 22 pero aqui en CAGRO ES 1, por eso la conversion de id's
        where case when c.cuo_empr = co_cagro_remoto then co_empr_cagro else c.cuo_empr end = l_empresa
        and   c.CUO_CLAVE_DOC = l_clave_externa;
      end agg_fin_cuota;
    end loop agg_fin_documento;     
  end importar_datos;
  
  procedure revertir_importacion(
    in_importacion in number
  )as
  begin
    -- remover documentos
    for i in (select d.doc_clave_local DOC_CLAVE,
                     d.empresa_id      empresa
              from fin_import_doc_ext_det d
              where d.import_id = in_importacion
    ) loop
      delete from fin_cuota p
      where p.cuo_clave_doc = i.doc_clave
      and p.cuo_empr = i.empresa;

      delete from fin_doc_concepto q
      where q.dcon_clave_doc = i.doc_clave
      and q.dcon_empr = i.empresa;

      delete from fin_documento r
      where r.doc_clave = i.doc_clave
      and r.doc_empr = i.empresa;
    end loop;
    
    -- eliminar cabecera
    -- El detalle se_va en cascada \(^.^)\
    delete from fin_import_doc_ext_cab c
    where c.id = in_importacion;
    
  end revertir_importacion;
end fini238;
/
