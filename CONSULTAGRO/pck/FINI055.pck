CREATE OR REPLACE PACKAGE FINI055 IS

  PROCEDURE PP_CALIFICAR_DISP(I_EMPRESA      IN NUMBER,
                              I_EMPLEADO     IN NUMBER,
                              I_COD_PROV     IN NUMBER,
                              O_POR_UTILIDAD OUT NUMBER,
                              O_DISP_PROME   OUT NUMBER,
                              O_SUELDO_PROM  OUT NUMBER,
                              O_EMPL_NOMBRE  OUT VARCHAR2,
                              O_FEC_INGRESO  OUT DATE,
                              O_EMPL_DEP     OUT VARCHAR2,
                              O_EMPL_CARGO   OUT VARCHAR2);

  PROCEDURE PP_CARGAR_DISP_DET(I_EMPRESA IN NUMBER,
                               I_PERIODO IN DATE,
                               I_PROV    IN NUMBER,
                               I_EMPLE   IN NUMBER);

  FUNCTION FP_CARGAR_OPER_HAB(I_TIPO_ADEL IN VARCHAR2,
                              I_CLAVE     IN NUMBER,
                              I_SUC       IN NUMBER,
                              I_EMPRESA   IN NUMBER) RETURN VARCHAR2;

  FUNCTION FP_VERIFICAR_PERMISO(I_EMPRESA    IN NUMBER,
                                I_TIPO_ADEL  IN VARCHAR2,
                                I_CLAVE_PROV NUMBER,
                                I_SUC        NUMBER) RETURN VARCHAR2;

  FUNCTION FP_PORCETAJE_UTILIDAD(P_EMPRESA  IN NUMBER,
                                 P_EMPLEADO IN NUMBER,
                                 P_COD_PROV IN NUMBER) RETURN NUMBER;

  PROCEDURE PP_GUARDAR_CAMBIOS(I_EMPRESA      IN NUMBER,
                               I_DOC_CLAVE    IN NUMBER,
                               I_TIPO_ADEL    IN VARCHAR2,
                               I_EMPL_LEG     IN NUMBER,
                               I_OLD_EST_GRAL IN VARCHAR2,
                               I_NEW_EST_GRAL IN VARCHAR2,
                               I_OLD_EST_RRHH IN VARCHAR2,
                               I_NEW_EST_RRHH IN VARCHAR2,
                               I_OLD_EST_DEP  IN VARCHAR2,
                               I_NEW_EST_DEP  IN VARCHAR2,
                               I_OBS          IN VARCHAR2,
                               I_COD_PROV     IN NUMBER);

  PROCEDURE PP_GUARDAR_CAMBIOS_NEW(I_EMPRESA IN NUMBER);

END FINI055;
/
CREATE OR REPLACE PACKAGE BODY FINI055 IS

  PROCEDURE PP_CALIFICAR_DISP(I_EMPRESA      IN NUMBER,
                              I_EMPLEADO     IN NUMBER,
                              I_COD_PROV     IN NUMBER,
                              O_POR_UTILIDAD OUT NUMBER,
                              O_DISP_PROME   OUT NUMBER,
                              O_SUELDO_PROM  OUT NUMBER,
                              O_EMPL_NOMBRE  OUT VARCHAR2,
                              O_FEC_INGRESO  OUT DATE,
                              O_EMPL_DEP     OUT VARCHAR2,
                              O_EMPL_CARGO   OUT VARCHAR2) AS
    V_DEBITO_PROM NUMBER;
    V_EMPL_DPTO   NUMBER;
    V_EMPL_CARGO  NUMBER;
  BEGIN
  
    SELECT D.EMPL_LEGAJO || ' *- ' || D.EMPL_NOMBRE || ' ' || D.EMPL_APE A,
           D.EMPL_FEC_INGRESO,
           D.EMPL_DEPARTAMENTO,
           D.EMPL_CARGO
      INTO O_EMPL_NOMBRE, O_FEC_INGRESO, V_EMPL_DPTO, V_EMPL_CARGO
      FROM PER_EMPLEADO D
     WHERE D.EMPL_LEGAJO = I_EMPLEADO
       AND D.EMPL_EMPRESA = I_EMPRESA;
  
    SELECT T.DPTO_CODIGO || '-* ' || T.DPTO_DESC SECTOR
      INTO O_EMPL_DEP
      FROM GEN_DEPARTAMENTO T
     WHERE T.DPTO_CODIGO = V_EMPL_DPTO
       AND T.DPTO_EMPR = I_EMPRESA;
  
    SELECT T.CAR_CODIGO || ' *-' || T.CAR_DESC CARGO
      INTO O_EMPL_CARGO
      FROM PER_CARGO T
     WHERE T.CAR_CODIGO = V_EMPL_CARGO
       AND T.CAR_EMPR = I_EMPRESA;
  
    SELECT AVG(CREDITO) CREDITO,
           AVG(DEBITO) DEBITO,
           AVG(CREDITO - DEBITO) DISPONIBILIDAD
      INTO O_SUELDO_PROM, V_DEBITO_PROM, O_DISP_PROME
      FROM (SELECT T.PERI_FEC_INI, SUM(CREDITO) CREDITO, SUM(DEBITO) DEBITO
              FROM CUBO_EMPLEADOS_PERL011 T, PER_CONCEPTO C
             WHERE C.PCON_EMPR = I_EMPRESA
               AND C.PCON_EMPR = T.EMPL_EMPRESA
               AND T.PCON_CLAVE = C.PCON_CLAVE
               AND C.PCON_CONS_PROM_ADEL = 'S'
               AND T.PERI_FEC_INI > ADD_MONTHS(TRUNC(SYSDATE, 'MM'), -6)
               AND T.EMPL_LEGAJO = I_EMPLEADO
             GROUP BY T.PERI_FEC_INI);
  
    O_POR_UTILIDAD := 1;
  
    --*** DESCUENTO FUTURO
  
    /*    SELECT SUM(CUO_SALDO_LOC) S
     
         FROM GEN_TIPO_MOV, FIN_DOCUMENTO DO, FIN_DOCUMENTO DP,
              FIN_CUOTA, FIN_PAGO
        WHERE TMOV_EMPR = :P_EMPRESA
        AND   TMOV_EMPR = DO.DOC_EMPR
        AND  (DO.DOC_EMPR = CUO_EMPR)
        AND   DP.DOC_EMPR(+) = PAG_EMPR
        AND   TMOV_CODIGO = DO.DOC_TIPO_MOV
        AND  (CUO_FEC_VTO = PAG_FEC_VTO(+) AND
              CUO_CLAVE_DOC = PAG_CLAVE_DOC(+) AND CUO_EMPR = PAG_EMPR(+))
        AND  (DO.DOC_CLAVE = CUO_CLAVE_DOC)
        AND   DP.DOC_CLAVE(+) = PAG_CLAVE_PAGO
        AND   DO.DOC_PROV  = :P39_PROVEEDOR
        AND  (TMOV_IND_AFECTA_SALDO = 'P' -- C=CLIENTE, P=PROVEEDOR, N=NO
           OR DO.DOC_TIPO_MOV IN (31,81))
        AND   CUO_FEC_VTO > :P39_DIA_PERIODO
        AND  (DO.DOC_SALDO_MON <> 0 );
    */
  END PP_CALIFICAR_DISP;

  PROCEDURE PP_CARGAR_DISP_DET(I_EMPRESA IN NUMBER,
                               I_PERIODO IN DATE,
                               I_PROV    IN NUMBER,
                               I_EMPLE   IN NUMBER) AS
  
    CURSOR C_PERI IS
      SELECT 'PER' SIS,
             P.FILTRO_PERIODO,
             TO_DATE(P.FILTRO_FECHA, 'DD-MM-YYYY') FECHA,
             P.FILTRO_CONCEPTO,
             P.CREDITO,
             P.DEBITO,
             P.FECHA_GRAB
        FROM CUBO_EMPLEADOS_PERL011 P, PER_CONCEPTO C
       WHERE P.EMPL_EMPRESA = I_EMPRESA
         AND P.EMPL_EMPRESA = C.PCON_EMPR
         AND P.PCON_CLAVE = C.PCON_CLAVE
            --AND C.PCON_CONS_PROM_ADEL = 'S'
         AND TRUNC(P.PERI_FEC_INI, 'MM') = I_PERIODO
         AND P.EMPL_LEGAJO = I_EMPLE
      UNION ALL
      SELECT 'FIN' SIS,
             FP.PERI_CODIGO,
             F.DOC_FEC_DOC FECHA,
             '1 SERV. PERSONAL' CONCEPTO,
             NVL(F.DOC_NETO_GRAV_LOC + F.DOC_IVA_LOC + F.DOC_NETO_EXEN_LOC,
                 0) CREDITO,
             0 DEBITO,
             F.DOC_FEC_GRAB
        FROM FIN_DOCUMENTO F, FIN_PERIODO FP
       WHERE F.DOC_TIPO_MOV = I_EMPRESA
         AND F.DOC_PROV = I_PROV
         AND TRUNC(F.DOC_FEC_DOC, 'MM') = I_PERIODO
         AND F.DOC_FEC_DOC BETWEEN FP.PERI_FEC_INI AND FP.PERI_FEC_FIN
         AND NVL(F.DOC_NETO_GRAV_LOC + F.DOC_IVA_LOC + F.DOC_NETO_EXEN_LOC,
                 0) > 0;
  
    CURSOR C_PERI_A IS
      SELECT 'FIN' SIS,
             FP.PERI_CODIGO,
             TO_DATE(CUO_FEC_VTO, 'DD/MM/YYYY'),
             TMOV_DESC,
             0,
             CUO_SALDO_LOC,
             DO.DOC_FEC_GRAB
        FROM GEN_TIPO_MOV,
             FIN_DOCUMENTO DO,
             FIN_DOCUMENTO DP,
             FIN_CUOTA,
             FIN_PAGO,
             PER_PERIODO   PERH,
             FIN_PERIODO   FP
       WHERE TMOV_EMPR = I_EMPRESA
         AND DO.DOC_EMPR = TMOV_EMPR
         AND TMOV_EMPR = PERH.PERI_EMPR
         AND (DO.DOC_EMPR = CUO_EMPR)
         AND DP.DOC_EMPR(+) = PAG_EMPR
         AND FP.PERI_EMPR = TMOV_EMPR
         AND TMOV_CODIGO = DO.DOC_TIPO_MOV
         AND PERH.PERI_FEC_INI = I_PERIODO
         AND PERH.PERI_FEC_FIN BETWEEN FP.PERI_FEC_INI AND FP.PERI_FEC_FIN
         AND (CUO_FEC_VTO = PAG_FEC_VTO(+) AND
             CUO_CLAVE_DOC = PAG_CLAVE_DOC(+) AND CUO_EMPR = PAG_EMPR(+))
         AND (DO.DOC_CLAVE = CUO_CLAVE_DOC)
         AND DP.DOC_CLAVE(+) = PAG_CLAVE_PAGO
         AND DO.DOC_PROV = I_PROV
         AND (TMOV_IND_AFECTA_SALDO = 'P' OR DO.DOC_TIPO_MOV IN (31, 81))
         AND CUO_FEC_VTO <= PERH.PERI_FEC_FIN
         AND (DO.DOC_SALDO_MON <> 0)
         AND CUO_SALDO_LOC > 0;
  
    CURSOR C_PERI_2 IS
      SELECT 'FIN' SIS,
             FP.PERI_CODIGO,
             TO_DATE(CUO_FEC_VTO, 'DD/MM/YYYY'),
             TMOV_DESC,
             0,
             CUO_SALDO_LOC,
             CUO_CLAVE_DOC,
             DO.DOC_FEC_GRAB
        FROM GEN_TIPO_MOV,
             FIN_DOCUMENTO DO,
             FIN_DOCUMENTO DP,
             FIN_CUOTA,
             FIN_PAGO,
             PER_PERIODO   PERH,
             FIN_PERIODO   FP
       WHERE TMOV_EMPR = I_EMPRESA
         AND DO.DOC_EMPR = TMOV_EMPR
         AND TMOV_EMPR = PERH.PERI_EMPR
         AND (DO.DOC_EMPR = CUO_EMPR)
         AND DP.DOC_EMPR(+) = PAG_EMPR
         AND FP.PERI_EMPR = TMOV_EMPR
         AND TMOV_CODIGO = DO.DOC_TIPO_MOV
         AND PERH.PERI_FEC_INI = I_PERIODO
         AND PERH.PERI_FEC_FIN BETWEEN FP.PERI_FEC_INI AND FP.PERI_FEC_FIN
         AND (CUO_FEC_VTO = PAG_FEC_VTO(+) AND
             CUO_CLAVE_DOC = PAG_CLAVE_DOC(+))
         AND (DO.DOC_CLAVE = CUO_CLAVE_DOC)
         AND DP.DOC_CLAVE(+) = PAG_CLAVE_PAGO
         AND DO.DOC_PROV = I_PROV
         AND DO.DOC_EMPR = I_EMPRESA
         AND (TMOV_IND_AFECTA_SALDO = 'P' OR DO.DOC_TIPO_MOV IN (31, 81))
         AND CUO_FEC_VTO > PERH.PERI_FEC_FIN
         AND (DO.DOC_SALDO_MON <> 0)
         AND CUO_SALDO_LOC > 0;
  
  BEGIN
  
    APEX_COLLECTION.CREATE_OR_TRUNCATE_COLLECTION(P_COLLECTION_NAME => 'FINI055_DET_DISP');
  
    FOR C IN C_PERI LOOP
      APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'FINI055_DET_DISP',
                                 P_C001            => C.SIS,
                                 P_N001            => C.FILTRO_PERIODO,
                                 P_D001            => C.FECHA,
                                 P_C002            => C.FILTRO_CONCEPTO,
                                 P_D002            => C.FECHA_GRAB,
                                 P_N002            => C.CREDITO,
                                 P_N003            => C.DEBITO);
    
    END LOOP;
  
  END PP_CARGAR_DISP_DET;

  FUNCTION FP_CARGAR_OPER_HAB(I_TIPO_ADEL IN VARCHAR2,
                              I_CLAVE     IN NUMBER,
                              I_SUC       IN NUMBER,
                              I_EMPRESA   IN NUMBER) RETURN VARCHAR2 IS
    MARC            VARCHAR2(5);
    V_FAC_LOG_APROB VARCHAR2(200);
    V_P             VARCHAR2(300);
  BEGIN
  
    IF I_TIPO_ADEL = 'C' THEN
      BEGIN
      
        SELECT WM_CONCAT(G.OPER_LOGIN)
          INTO V_FAC_LOG_APROB
          FROM GEN_OPER_CANAL_VENTA C,
               GEN_OPERADOR         G,
               GEN_OPERADOR_EMPRESA GE
         WHERE C.OPERCAN_EMPR = I_EMPRESA
           AND C.OPERCAN_EMPR = GE.OPEM_EMPR
           AND GE.OPEM_OPER = G.OPER_CODIGO
           AND C.OPERCAN_OPER = G.OPER_CODIGO
           AND C.OPERCAN_CANAL =
               (SELECT CLI.CLI_CANAL
                  FROM FIN_CLIENTE CLI
                 WHERE CLI.CLI_CODIGO = I_CLAVE
                   AND CLI.CLI_EMPR = I_EMPRESA)
           AND 'S' =
               (SELECT 'S'
                  FROM FIN_ADEL_SUCASIG T
                 WHERE T.ADELSUC_CLAVE_SUC_COD = I_SUC
                   AND T.ADELSUC_CLAVE_SUC_EMP = I_EMPRESA
                   AND T.ADELSUC_CLAVE_OPER =
                       (SELECT G2.OPER_CODIGO
                          FROM GEN_OPERADOR G2, GEN_OPERADOR_EMPRESA GEM
                         WHERE G2.OPER_LOGIN = G.OPER_LOGIN
                           AND G2.OPER_CODIGO = GEM.OPEM_OPER
                           AND GEM.OPEM_EMPR = I_EMPRESA));
      
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          V_FAC_LOG_APROB := NULL;
      END;
    
      RETURN V_FAC_LOG_APROB;
    
    ELSIF I_TIPO_ADEL = 'P' THEN
    
      BEGIN
        SELECT NVL('S', 'N')
          INTO MARC
          FROM FIN_PROVEEDOR P
         WHERE P.PROV_CODIGO = I_CLAVE
           AND P.PROV_TIPO IN (10)
           AND P.PROV_EMPR = I_EMPRESA;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          MARC := 'N';
      END;
    
      IF MARC = 'S' THEN
      
        SELECT WM_CONCAT(G.OPER_LOGIN)
          INTO V_FAC_LOG_APROB
          FROM GEN_OPER_TIP_PROV P, GEN_OPERADOR G, GEN_OPERADOR_EMPRESA GE
         WHERE P.OPETIPRO_EMPR = I_EMPRESA
           AND P.OPETIPRO_EMPR = GE.OPEM_EMPR
           AND P.OPETIPRO_OPER = G.OPER_CODIGO
           AND G.OPER_CODIGO = GE.OPEM_OPER
           AND P.OPETIPRO_TIP_PROV =
               (SELECT PR.PROV_TIPO
                  FROM FIN_PROVEEDOR PR
                 WHERE PR.PROV_CODIGO = I_CLAVE
                   AND PR.PROV_EMPR = I_EMPRESA)
           AND 'S' IN
               (SELECT 'S'
                  FROM FIN_ADEL_SUCASIG T
                 WHERE T.ADELSUC_CLAVE_SUC_COD = I_SUC
                   AND T.ADELSUC_CLAVE_SUC_EMP = I_EMPRESA
                   AND T.ADELSUC_CLAVE_OPER = G.OPER_CODIGO
                   AND 'S' =
                       (SELECT NVL('S', 'N')
                          FROM FIN_ADEL_DEPASIG F
                         WHERE F.ADELDEP_CLAVE_EMPR = I_EMPRESA
                           AND F.ADELDEP_CLAVE_DEP_COD =
                               (SELECT P.EMPL_DEPARTAMENTO
                                  FROM PER_EMPLEADO P
                                 WHERE P.EMPL_EMPRESA = I_EMPRESA
                                   AND P.EMPL_CODIGO_PROV = I_CLAVE
                                   AND P.EMPL_SITUACION = 'A')
                           AND F.ADELDEP_CLAVE_OPER = G.OPER_CODIGO));
      
      ELSE
      
        BEGIN
          SELECT WM_CONCAT(G.OPER_LOGIN)
            INTO V_FAC_LOG_APROB
            FROM GEN_OPER_TIP_PROV    P,
                 GEN_OPERADOR         G,
                 GEN_OPERADOR_EMPRESA GE
           WHERE P.OPETIPRO_EMPR = I_EMPRESA
             AND P.OPETIPRO_EMPR = GE.OPEM_EMPR
             AND P.OPETIPRO_OPER = G.OPER_CODIGO
             AND GE.OPEM_OPER = G.OPER_CODIGO
             AND P.OPETIPRO_TIP_PROV =
                 (SELECT PR.PROV_TIPO
                    FROM FIN_PROVEEDOR PR
                   WHERE PR.PROV_CODIGO = I_CLAVE
                     AND PR.PROV_EMPR = I_EMPRESA)
             AND 'S' = (SELECT 'S'
                          FROM FIN_ADEL_SUCASIG T
                         WHERE T.ADELSUC_CLAVE_SUC_COD = I_SUC
                           AND T.ADELSUC_CLAVE_SUC_EMP = I_EMPRESA
                           AND T.ADELSUC_CLAVE_OPER = G.OPER_CODIGO);
        
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            V_FAC_LOG_APROB := NULL;
        END;
      END IF;
    
      RETURN V_FAC_LOG_APROB;
    
    END IF;
  
  END FP_CARGAR_OPER_HAB;

  FUNCTION FP_VERIFICAR_PERMISO(I_EMPRESA    IN NUMBER,
                                I_TIPO_ADEL  IN VARCHAR2,
                                I_CLAVE_PROV NUMBER,
                                I_SUC        NUMBER) RETURN VARCHAR2 IS
    V_CONT NUMBER;
    MARC   VARCHAR2(1);
  BEGIN
    IF I_TIPO_ADEL = 'C' then
      SELECT COUNT(*)
        INTO V_CONT
        FROM GEN_OPER_CANAL_VENTA C,
             GEN_OPERADOR         G,
             GEN_OPERADOR_EMPRESA GE
       WHERE C.OPERCAN_EMPR = I_EMPRESA
         AND C.OPERCAN_OPER = G.OPER_CODIGO
         AND G.OPER_LOGIN = GEN_DEVUELVE_USER
         AND G.OPER_CODIGO = GE.OPEM_OPER
         AND GE.OPEM_EMPR = C.OPERCAN_EMPR
         AND C.OPERCAN_CANAL =
             (SELECT CLI.CLI_CANAL
                FROM FIN_CLIENTE CLI
               WHERE CLI.CLI_CODIGO = I_CLAVE_PROV
                 AND CLI.CLI_EMPR = I_EMPRESA)
         AND 'S' = (SELECT 'S'
                      FROM FIN_ADEL_SUCASIG T
                     WHERE T.ADELSUC_CLAVE_SUC_COD = I_SUC
                       AND T.ADELSUC_CLAVE_SUC_EMP = 1
                       AND T.ADELSUC_CLAVE_OPER = G.OPER_CODIGO
                       AND T.ADELSUC_CLAVE_EMPR = I_EMPRESA);
    
    ELSIF I_TIPO_ADEL = 'P' then
      
      BEGIN
        SELECT NVL('S', 'N')
          INTO MARC
          FROM FIN_PROVEEDOR P
         WHERE P.PROV_CODIGO = I_CLAVE_PROV
           AND P.PROV_TIPO IN (10)
           AND P.PROV_EMPR = I_EMPRESA;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          MARC := 'N';
      END;
    
      IF MARC = 'S' THEN
      
        SELECT distinct COUNT(*)
          INTO V_CONT
          FROM GEN_OPER_TIP_PROV P, GEN_OPERADOR G, GEN_OPERADOR_EMPRESA GE
         WHERE P.OPETIPRO_EMPR = I_EMPRESA
           AND P.OPETIPRO_OPER = G.OPER_CODIGO
           AND G.OPER_LOGIN = GEN_DEVUELVE_USER
           AND G.OPER_CODIGO = GE.OPEM_OPER
           AND P.OPETIPRO_EMPR = GE.OPEM_EMPR
           AND P.OPETIPRO_TIP_PROV =
               (SELECT distinct PR.PROV_TIPO
                  FROM FIN_PROVEEDOR PR
                 WHERE PR.PROV_CODIGO = I_CLAVE_PROV
                   AND PR.PROV_EMPR = I_EMPRESA)
           AND 'S' =
               (SELECT distinct 'S'
                  FROM FIN_ADEL_SUCASIG T
                 WHERE T.ADELSUC_CLAVE_SUC_COD = I_SUC
                   AND T.ADELSUC_CLAVE_SUC_EMP = I_EMPRESA
                   AND T.ADELSUC_CLAVE_OPER = G.OPER_CODIGO
                   AND 'S' = (SELECT distinct NVL('S', 'N')
                                FROM FIN_ADEL_DEPASIG F
                               WHERE F.ADELDEP_CLAVE_DEP_COD =
                                     (SELECT distinct P.EMPL_DEPARTAMENTO
                                        FROM PER_EMPLEADO P
                                       WHERE P.EMPL_CODIGO_PROV = I_CLAVE_PROV
                                         AND P.EMPL_SITUACION IN ('A', 'I')
                                         AND P.EMPL_EMPRESA = I_EMPRESA)
                                 AND F.ADELDEP_CLAVE_OPER = G.OPER_CODIGO
                                 AND F.ADELDEP_CLAVE_EMPR = I_EMPRESA));
      
      else
        SELECT distinct COUNT(*)
          INTO V_CONT
          FROM GEN_OPER_TIP_PROV P, 
               GEN_OPERADOR G, 
               GEN_OPERADOR_EMPRESA GE
         WHERE P.OPETIPRO_EMPR = I_EMPRESA
           AND P.OPETIPRO_OPER = G.OPER_CODIGO
           AND G.OPER_LOGIN = GEN_DEVUELVE_USER
           AND G.OPER_CODIGO = GE.OPEM_OPER
           AND P.OPETIPRO_EMPR = GE.OPEM_EMPR
           AND P.OPETIPRO_TIP_PROV =
               (SELECT distinct PR.PROV_TIPO
                  FROM FIN_PROVEEDOR PR
                 WHERE PR.PROV_CODIGO = I_CLAVE_PROV
                   AND PR.PROV_EMPR = I_EMPRESA)
           AND 'S' = (SELECT distinct 'S'
                        FROM FIN_ADEL_SUCASIG T
                       WHERE T.ADELSUC_CLAVE_SUC_COD = I_SUC
                         AND T.ADELSUC_CLAVE_SUC_EMP = I_EMPRESA
                         AND T.ADELSUC_CLAVE_OPER = G.OPER_CODIGO);
      END IF;
    END IF;
  
    IF V_CONT > 0  THEN
      RETURN('S');
    ELSE
      RETURN('N');
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN('N');
    WHEN OTHERS THEN 
       raise_application_error(-20001,'Error en  FP_VERIFICAR_PERMISO'||SQLCODE||' -ERROR- '||SQLERRM);
      
  END FP_VERIFICAR_PERMISO;

  FUNCTION FP_PORCETAJE_UTILIDAD(P_EMPRESA  IN NUMBER,
                                 P_EMPLEADO IN NUMBER,
                                 P_COD_PROV IN NUMBER) RETURN NUMBER IS
  
    CONTADOR         NUMBER := 1;
    PER_ACTUAL       NUMBER;
    V_SUM_CRED_1     NUMBER;
    V_SUM_BED_1      NUMBER;
    V_SUM_TOT_1      NUMBER;
    V_PORC_1         NUMBER;
    V_SERV_PER_1     NUMBER;
    V_SERV_PER_1_DEB NUMBER;
    V_SUM_CRED_2     NUMBER;
    V_SUM_BED_2      NUMBER;
    V_SUM_TOT_2      NUMBER;
    V_PORC_2         NUMBER;
    V_SERV_PER_2     NUMBER;
    V_SERV_PER_2_DEB NUMBER;
    V_SUM_CRED_3     NUMBER;
    V_SUM_BED_3      NUMBER;
    V_SUM_TOT_3      NUMBER;
    V_PORC_3         NUMBER;
    V_SERV_PER_3     NUMBER;
    V_SERV_PER_3_DEB NUMBER;
    V_SUM_CRED_4     NUMBER;
    V_SUM_BED_4      NUMBER;
    V_SUM_TOT_4      NUMBER;
    V_PORC_4         NUMBER;
    V_SERV_PER_4     NUMBER;
    V_SERV_PER_4_DEB NUMBER;
    V_SUM_CRED_5     NUMBER;
    V_SUM_BED_5      NUMBER;
    V_SUM_TOT_5      NUMBER;
    V_PORC_5         NUMBER;
    V_SERV_PER_5     NUMBER;
    V_SERV_PER_5_DEB NUMBER;
    V_SUM_CRED_6     NUMBER;
    V_SUM_BED_6      NUMBER;
    V_SUM_TOT_6      NUMBER;
    V_PORC_6         NUMBER;
    V_SERV_PER_6     NUMBER;
    V_SERV_PER_6_DEB NUMBER;
    V_CONT_MESES     NUMBER;
    V_TOT_MESES      NUMBER;
    V_UL_DIA_PER     DATE;
    V_TOT_UTIL       NUMBER;
    V_PORC_UTIL      NUMBER;
    V_DISP_EMP_LEG   NUMBER;
    V_DISP_PER_ACT   NUMBER;
    V_DISP_1_MES     CHAR(15);
    V_DISP_INGRESO_1 NUMBER;
    V_DISP_EGRESO_1  NUMBER;
    V_TOTAL_1_MES    NUMBER;
    V_DISP_2_MES     CHAR(15);
    V_DISP_INGRESO_2 NUMBER;
    V_DISP_EGRESO_2  NUMBER;
    V_TOTAL_2_MES    NUMBER;
    V_DISP_3_MES     CHAR(15);
    V_DISP_INGRESO_3 NUMBER;
    V_DISP_EGRESO_3  NUMBER;
    V_TOTAL_3_MES    NUMBER;
    V_DISP_4_MES     CHAR(15);
    V_DISP_INGRESO_4 NUMBER;
    V_DISP_EGRESO_4  NUMBER;
    V_TOTAL_4_MES    NUMBER;
    V_DISP_5_MES     CHAR(15);
    V_DISP_INGRESO_5 NUMBER;
    V_DISP_EGRESO_5  NUMBER;
    V_TOTAL_5_MES    NUMBER;
    V_DISP_6_MES     CHAR(15);
    V_DISP_INGRESO_6 NUMBER;
    V_DISP_EGRESO_6  NUMBER;
    V_TOTAL_6_MES    NUMBER;
    V_DISP_PROM      NUMBER;
    V_DISP_UTIL      NUMBER;
    V_PROM_CRED      NUMBER;
    V_PORC_DIS       NUMBER;
    V_TOT_SUELDO     NUMBER;
    V_SUEL_PROM      NUMBER;
    V_PORC           CHAR := 'X';
  
  BEGIN
  
    SELECT CONF_PERI_SGTE
      INTO PER_ACTUAL
      FROM PER_CONFIGURACION
     WHERE CONF_EMPR = P_EMPRESA;
  
    SELECT TO_CHAR(PERI_FEC_FIN, 'DD/MM/YYYY')
      INTO V_UL_DIA_PER
      FROM PER_PERIODO
     WHERE PERI_CODIGO = PER_ACTUAL
       AND PERI_EMPR = P_EMPRESA;
  
    SELECT (TO_CHAR(PERI_FEC_INI, 'MONTH'))
      INTO V_DISP_1_MES
      FROM PER_PERIODO
     WHERE PERI_CODIGO = (PER_ACTUAL - 1)
       AND PERI_EMPR = P_EMPRESA;
  
    ------------------------------------------------------------
  
    BEGIN
    
      SELECT NVL(SUM(DECODE(T.FILTRO_PERIODO, (PER_ACTUAL - 1), CREDITO, 0)),
                 1) CRED1,
             NVL(SUM(DECODE(T.FILTRO_PERIODO, (PER_ACTUAL - 1), DEBITO, 0)),
                 1) DEB1,
             NVL(SUM(DECODE(T.FILTRO_PERIODO, (PER_ACTUAL - 2), CREDITO, 0)),
                 1) CRED2,
             NVL(SUM(DECODE(T.FILTRO_PERIODO, (PER_ACTUAL - 2), DEBITO, 0)),
                 1) DEB2,
             NVL(SUM(DECODE(T.FILTRO_PERIODO, (PER_ACTUAL - 3), CREDITO, 0)),
                 1) CRED3,
             NVL(SUM(DECODE(T.FILTRO_PERIODO, (PER_ACTUAL - 3), DEBITO, 0)),
                 1) DEB3,
             NVL(SUM(DECODE(T.FILTRO_PERIODO, (PER_ACTUAL - 4), CREDITO, 0)),
                 1) CRED4,
             NVL(SUM(DECODE(T.FILTRO_PERIODO, (PER_ACTUAL - 4), DEBITO, 0)),
                 1) DEB4,
             NVL(SUM(DECODE(T.FILTRO_PERIODO, (PER_ACTUAL - 5), CREDITO, 0)),
                 1) CRED5,
             NVL(SUM(DECODE(T.FILTRO_PERIODO, (PER_ACTUAL - 5), DEBITO, 0)),
                 1) DEB5,
             NVL(SUM(DECODE(T.FILTRO_PERIODO, (PER_ACTUAL - 6), CREDITO, 0)),
                 1) CRED6,
             NVL(SUM(DECODE(T.FILTRO_PERIODO, (PER_ACTUAL - 6), DEBITO, 0)),
                 1) DEB6
        INTO V_SUM_CRED_1,
             V_SUM_BED_1,
             V_SUM_CRED_2,
             V_SUM_BED_2,
             V_SUM_CRED_3,
             V_SUM_BED_3,
             V_SUM_CRED_4,
             V_SUM_BED_4,
             V_SUM_CRED_5,
             V_SUM_BED_5,
             V_SUM_CRED_6,
             V_SUM_BED_6
        FROM CUBO_EMPLEADOS_PERL011 T, PER_CONCEPTO C
       WHERE C.PCON_EMPR = P_EMPRESA
         AND C.PCON_EMPR = T.EMPL_EMPRESA
         AND T.PCON_CLAVE = C.PCON_CLAVE
         AND C.PCON_CONS_PROM_ADEL = 'S'
         AND T.FILTRO_PERIODO BETWEEN (PER_ACTUAL - 6) AND PER_ACTUAL
         AND T.EMPL_LEGAJO = P_EMPLEADO;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_SUM_CRED_1 := 0;
        V_SUM_BED_1  := 0;
        V_SUM_TOT_1  := 0;
        V_SUM_CRED_2 := 0;
        V_SUM_BED_2  := 0;
        V_SUM_TOT_2  := 0;
        V_SUM_CRED_3 := 0;
        V_SUM_BED_3  := 0;
        V_SUM_TOT_3  := 0;
        V_SUM_CRED_4 := 0;
        V_SUM_BED_4  := 0;
        V_SUM_TOT_4  := 0;
        V_SUM_CRED_5 := 0;
        V_SUM_BED_5  := 0;
        V_SUM_TOT_5  := 0;
        V_SUM_CRED_6 := 0;
        V_SUM_BED_6  := 0;
        V_SUM_TOT_6  := 0;
    END;
  
    BEGIN
    
      BEGIN
      
        SELECT NVL(SUM(DECODE(PERI.PERI_CODIGO,
                              (PER_ACTUAL - 1),
                              FE.DOC_NETO_GRAV_LOC,
                              0)),
                   1) V_SERV_PER_1,
               NVL(SUM(DECODE(PERI.PERI_CODIGO,
                              (PER_ACTUAL - 1),
                              FE.DOC_IVA_LOC,
                              0)),
                   1) V_SERV_PER_1_DEB,
               NVL(SUM(DECODE(PERI.PERI_CODIGO,
                              (PER_ACTUAL - 2),
                              FE.DOC_NETO_GRAV_LOC,
                              0)),
                   1) V_SERV_PER_2,
               NVL(SUM(DECODE(PERI.PERI_CODIGO,
                              (PER_ACTUAL - 2),
                              FE.DOC_IVA_LOC,
                              0)),
                   1) V_SERV_PER_2_DEB,
               NVL(SUM(DECODE(PERI.PERI_CODIGO,
                              (PER_ACTUAL - 3),
                              FE.DOC_NETO_GRAV_LOC,
                              0)),
                   1) V_SERV_PER_3,
               NVL(SUM(DECODE(PERI.PERI_CODIGO,
                              (PER_ACTUAL - 3),
                              FE.DOC_IVA_LOC,
                              0)),
                   1) V_SERV_PER_3_DEB,
               NVL(SUM(DECODE(PERI.PERI_CODIGO,
                              (PER_ACTUAL - 4),
                              FE.DOC_NETO_GRAV_LOC,
                              0)),
                   1) V_SERV_PER_4,
               NVL(SUM(DECODE(PERI.PERI_CODIGO,
                              (PER_ACTUAL - 4),
                              FE.DOC_IVA_LOC,
                              0)),
                   1) V_SERV_PER_4_DEB,
               NVL(SUM(DECODE(PERI.PERI_CODIGO,
                              (PER_ACTUAL - 5),
                              FE.DOC_NETO_GRAV_LOC,
                              0)),
                   1) V_SERV_PER_5,
               NVL(SUM(DECODE(PERI.PERI_CODIGO,
                              (PER_ACTUAL - 5),
                              FE.DOC_IVA_LOC,
                              0)),
                   1) V_SERV_PER_5_DEB,
               NVL(SUM(DECODE(PERI.PERI_CODIGO,
                              (PER_ACTUAL - 6),
                              FE.DOC_NETO_GRAV_LOC,
                              0)),
                   1) V_SERV_PER_6,
               NVL(SUM(DECODE(PERI.PERI_CODIGO,
                              (PER_ACTUAL - 6),
                              FE.DOC_IVA_LOC,
                              0)),
                   1) V_SERV_PER_6_DEB
          INTO V_SERV_PER_1,
               V_SERV_PER_1_DEB,
               V_SERV_PER_2,
               V_SERV_PER_2_DEB,
               V_SERV_PER_3,
               V_SERV_PER_3_DEB,
               V_SERV_PER_4,
               V_SERV_PER_4_DEB,
               V_SERV_PER_5,
               V_SERV_PER_5_DEB,
               V_SERV_PER_6,
               V_SERV_PER_6_DEB
          FROM (SELECT F.DOC_NETO_GRAV_LOC,
                       F.DOC_IVA_LOC,
                       F.DOC_FEC_DOC,
                       F.DOC_EMPR
                  FROM FIN_DOCUMENTO F,
                       (SELECT P.PERI_FEC_FIN FEC_FIN, P.PERI_EMPR
                          FROM PER_PERIODO P
                         WHERE P.PERI_CODIGO = (PER_ACTUAL - 1)
                           AND P.PERI_EMPR = P_EMPRESA) PERI_1,
                       (SELECT P.PERI_FEC_INI INI, P.PERI_EMPR
                          FROM PER_PERIODO P
                         WHERE P.PERI_CODIGO = (PER_ACTUAL - 6)
                           AND P.PERI_EMPR = P_EMPRESA) PERI_6
                 WHERE F.DOC_TIPO_MOV = 1
                   AND F.DOC_EMPR = P_EMPRESA
                   AND F.DOC_PROV = P_COD_PROV
                   AND F.DOC_FEC_DOC BETWEEN PERI_6.INI AND PERI_1.FEC_FIN) FE,
               PER_PERIODO PERI
         WHERE FE.DOC_FEC_DOC BETWEEN PERI.PERI_FEC_INI AND
               PERI.PERI_FEC_FIN
           AND FE.DOC_EMPR = PERI.PERI_EMPR
           AND PERI.PERI_EMPR = P_EMPRESA;
      
        V_SUM_CRED_1 := V_SUM_CRED_1 + V_SERV_PER_1 + V_SERV_PER_1_DEB;
        V_SUM_CRED_2 := V_SUM_CRED_2 + V_SERV_PER_2 + V_SERV_PER_2_DEB;
        V_SUM_CRED_3 := V_SUM_CRED_3 + V_SERV_PER_3 + V_SERV_PER_3_DEB;
        V_SUM_CRED_4 := V_SUM_CRED_4 + V_SERV_PER_4 + V_SERV_PER_4_DEB;
        V_SUM_CRED_5 := V_SUM_CRED_5 + V_SERV_PER_5 + V_SERV_PER_5_DEB;
        V_SUM_CRED_6 := V_SUM_CRED_6 + V_SERV_PER_6 + V_SERV_PER_6_DEB;
      
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          V_SUM_CRED_1 := V_SUM_CRED_1 + 0;
          V_SUM_BED_1  := V_SUM_BED_1 + 0;
          V_SUM_CRED_2 := V_SUM_CRED_2 + 0;
          V_SUM_BED_2  := V_SUM_BED_2 + 0;
          V_SUM_CRED_3 := V_SUM_CRED_3 + 0;
          V_SUM_BED_3  := V_SUM_BED_3 + 0;
          V_SUM_CRED_4 := V_SUM_CRED_4 + 0;
          V_SUM_BED_4  := V_SUM_BED_4 + 0;
          V_SUM_CRED_5 := V_SUM_CRED_5 + 0;
          V_SUM_BED_5  := V_SUM_BED_5 + 0;
          V_SUM_CRED_6 := V_SUM_CRED_6 + 0;
          V_SUM_BED_6  := V_SUM_BED_6 + 0;
        
      END;
    
      V_SUM_TOT_1 := V_SUM_CRED_1 + V_SUM_BED_1;
      V_SUM_TOT_2 := V_SUM_CRED_2 + V_SUM_BED_2;
      V_SUM_TOT_3 := V_SUM_CRED_3 + V_SUM_BED_3;
      V_SUM_TOT_4 := V_SUM_CRED_4 + V_SUM_BED_4;
      V_SUM_TOT_5 := V_SUM_CRED_5 + V_SUM_BED_5;
      V_SUM_TOT_6 := V_SUM_CRED_6 + V_SUM_BED_6;
    
      IF V_SUM_TOT_1 > 0 THEN
        V_DISP_INGRESO_1 := V_SUM_CRED_1;
        V_DISP_EGRESO_1  := V_SUM_BED_1;
        V_TOTAL_1_MES    := V_SUM_CRED_1 - V_SUM_BED_1;
        V_PORC_1         := ROUND((V_SUM_CRED_1 * 100) / V_SUM_TOT_1);
      
      ELSE
      
        V_TOTAL_1_MES    := 0;
        V_DISP_INGRESO_1 := 0;
        V_DISP_EGRESO_1  := 0;
        V_PORC_1         := 0;
      
      END IF;
    
      IF V_SUM_TOT_2 > 0 THEN
      
        V_DISP_INGRESO_2 := V_SUM_CRED_2;
        V_DISP_EGRESO_2  := V_SUM_BED_2;
        V_TOTAL_2_MES    := V_SUM_CRED_2 - V_SUM_BED_2;
        V_PORC_2         := ROUND((V_SUM_CRED_2 * 100) / V_SUM_TOT_2);
      
      ELSE
      
        V_TOTAL_2_MES    := 0;
        V_DISP_INGRESO_2 := 0;
        V_DISP_EGRESO_2  := 0;
        V_PORC_2         := 0;
      
      END IF;
    
      IF V_SUM_TOT_3 > 0 THEN
      
        V_DISP_INGRESO_3 := V_SUM_CRED_3;
        V_DISP_EGRESO_3  := V_SUM_BED_3;
        V_TOTAL_3_MES    := V_SUM_CRED_3 - V_SUM_BED_3;
        V_PORC_3         := ROUND((V_SUM_CRED_3 * 100) / V_SUM_TOT_3);
      
      ELSE
      
        V_TOTAL_3_MES    := 0;
        V_DISP_INGRESO_3 := 0;
        V_DISP_EGRESO_3  := 0;
        V_PORC_3         := 0;
      
      END IF;
    
      IF V_SUM_TOT_4 > 0 THEN
      
        V_DISP_INGRESO_4 := V_SUM_CRED_4;
        V_DISP_EGRESO_4  := V_SUM_BED_4;
        V_TOTAL_4_MES    := V_SUM_CRED_4 - V_SUM_BED_4;
        V_PORC_4         := ROUND((V_SUM_CRED_4 * 100) / V_SUM_TOT_4);
      
      ELSE
      
        V_TOTAL_4_MES    := 0;
        V_DISP_INGRESO_4 := 0;
        V_DISP_EGRESO_4  := 0;
        V_PORC_4         := 0;
      
      END IF;
    
      IF V_SUM_TOT_5 > 0 THEN
        V_DISP_INGRESO_5 := V_SUM_CRED_5;
        V_DISP_EGRESO_5  := V_SUM_BED_5;
        V_TOTAL_5_MES    := V_SUM_CRED_5 - V_SUM_BED_5;
        V_PORC_5         := ROUND((V_SUM_CRED_5 * 100) / V_SUM_TOT_5);
      
      ELSE
      
        V_TOTAL_5_MES    := 0;
        V_DISP_INGRESO_5 := 0;
        V_DISP_EGRESO_5  := 0;
        V_PORC_5         := 0;
      
      END IF;
    
      IF V_SUM_TOT_6 > 0 THEN
        V_DISP_INGRESO_6 := V_SUM_CRED_6;
        V_DISP_EGRESO_6  := V_SUM_BED_6;
        V_TOTAL_6_MES    := V_SUM_CRED_6 - V_SUM_BED_6;
        V_PORC_6         := ROUND((V_SUM_CRED_6 * 100) / V_SUM_TOT_6);
      
      ELSE
      
        V_TOTAL_6_MES    := 0;
        V_DISP_INGRESO_6 := 0;
        V_DISP_EGRESO_6  := 0;
        V_PORC_6         := 0;
      
      END IF;
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_TOTAL_1_MES    := 0;
        V_DISP_INGRESO_1 := 0;
        V_DISP_EGRESO_1  := 0;
        V_PORC_1         := 0;
        V_TOTAL_2_MES    := 0;
        V_DISP_INGRESO_2 := 0;
        V_DISP_EGRESO_2  := 0;
        V_PORC_2         := 0;
        V_TOTAL_3_MES    := 0;
        V_DISP_INGRESO_3 := 0;
        V_DISP_EGRESO_3  := 0;
        V_PORC_3         := 0;
        V_TOTAL_4_MES    := 0;
        V_DISP_INGRESO_4 := 0;
        V_DISP_EGRESO_4  := 0;
        V_PORC_4         := 0;
        V_TOTAL_5_MES    := 0;
        V_DISP_INGRESO_5 := 0;
        V_DISP_EGRESO_5  := 0;
        V_PORC_5         := 0;
        V_TOTAL_6_MES    := 0;
        V_DISP_INGRESO_6 := 0;
        V_DISP_EGRESO_6  := 0;
        V_PORC_6         := 0;
      
    END;
  
    ---------------------------------------------------------------------------------------------
  
    ----------------------------CARGA EL PROMEDIO DE LOS ULTIMOS 6 MESES-------------------------
  
    ---------------------------------------------------------------------------------------------
  
    V_CONT_MESES := 0;
    V_TOT_MESES  := 0;
    V_TOT_SUELDO := 0;
  
    IF V_TOTAL_1_MES > 0 THEN
      V_CONT_MESES := V_CONT_MESES + 1;
      V_TOT_MESES  := V_TOT_MESES + V_TOTAL_1_MES;
      V_TOT_SUELDO := V_TOT_SUELDO + V_DISP_INGRESO_1;
    
    END IF;
  
    IF V_TOTAL_2_MES > 0 THEN
      V_CONT_MESES := V_CONT_MESES + 1;
      V_TOT_MESES  := V_TOT_MESES + V_TOTAL_2_MES;
      V_TOT_SUELDO := V_TOT_SUELDO + V_DISP_INGRESO_2;
    
    END IF;
  
    IF V_TOTAL_3_MES > 0 THEN
      V_CONT_MESES := V_CONT_MESES + 1;
      V_TOT_MESES  := V_TOT_MESES + V_TOTAL_3_MES;
      V_TOT_SUELDO := V_TOT_SUELDO + V_DISP_INGRESO_3;
    
    END IF;
  
    IF V_TOTAL_4_MES > 0 THEN
      V_CONT_MESES := V_CONT_MESES + 1;
      V_TOT_MESES  := V_TOT_MESES + V_TOTAL_4_MES;
      V_TOT_SUELDO := V_TOT_SUELDO + V_DISP_INGRESO_4;
    
    END IF;
  
    IF V_TOTAL_5_MES > 0 THEN
      V_CONT_MESES := V_CONT_MESES + 1;
      V_TOT_MESES  := V_TOT_MESES + V_TOTAL_5_MES;
      V_TOT_SUELDO := V_TOT_SUELDO + V_DISP_INGRESO_5;
    
    END IF;
  
    IF V_TOTAL_6_MES > 0 THEN
      V_CONT_MESES := V_CONT_MESES + 1;
      V_TOT_MESES  := V_TOT_MESES + V_TOTAL_6_MES;
      V_TOT_SUELDO := V_TOT_SUELDO + V_DISP_INGRESO_6;
    END IF;
  
    IF V_CONT_MESES > 0 THEN
      V_DISP_PROM := V_TOT_MESES / V_CONT_MESES;
      V_SUEL_PROM := V_TOT_SUELDO / V_CONT_MESES;
    
    ELSE
      V_DISP_PROM := 0;
      V_SUEL_PROM := 0;
    END IF;
  
    ----------------------------------------------------------------------------------
  
    --------------------TRAE PENDIENTES DEL EMPLEADO----------------------------------
  
    BEGIN
    
      SELECT SUM(CUO_SALDO_LOC)
        INTO V_TOT_UTIL
        FROM GEN_TIPO_MOV,
             FIN_DOCUMENTO DO,
             FIN_DOCUMENTO DP,
             FIN_CUOTA,
             FIN_PAGO
       WHERE DO.DOC_EMPR = P_EMPRESA
         AND TMOV_EMPR = DO.DOC_EMPR
         AND (DO.DOC_EMPR = CUO_EMPR)
         AND DP.DOC_EMPR(+) = PAG_EMPR
         AND TMOV_CODIGO = DO.DOC_TIPO_MOV
         AND (CUO_FEC_VTO = PAG_FEC_VTO(+) AND
             CUO_CLAVE_DOC = PAG_CLAVE_DOC(+) AND CUO_EMPR = PAG_EMPR(+))
         AND (DO.DOC_CLAVE = CUO_CLAVE_DOC)
         AND DP.DOC_CLAVE(+) = PAG_CLAVE_PAGO
         AND DO.DOC_PROV = P_COD_PROV
         AND (TMOV_IND_AFECTA_SALDO = 'P' -- C=CLIENTE, P=PROVEEDOR, N=NO             
             OR DO.DOC_TIPO_MOV IN (31, 81))
         AND CUO_FEC_VTO <= V_UL_DIA_PER
         AND (DO.DOC_SALDO_MON <> 0);
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_TOT_UTIL  := 0;
        V_DISP_UTIL := 0;
    END;
  
    IF V_TOT_UTIL > 0 AND V_DISP_PROM > 0 THEN
      V_DISP_UTIL := V_TOT_UTIL;
      V_PORC_UTIL := ROUND((V_TOT_UTIL * 100) / V_SUEL_PROM);
    ELSE
      V_PORC_UTIL := 0;
      V_DISP_UTIL := 0;
    
    END IF;
    IF V_PORC_UTIL > 0 AND V_PORC_UTIL < 35 THEN
      RETURN 1;
    ELSIF V_PORC_UTIL > 35 AND V_PORC_UTIL <= 70 THEN
      RETURN 2;
    ELSIF V_PORC_UTIL > 70 AND V_PORC_UTIL <= 500 THEN
      RETURN 3;
    ELSIF V_PORC_UTIL > 100 THEN
      RETURN 4;
    ELSIF P_EMPLEADO IS NULL THEN
      RETURN 5;
    ELSE
      RETURN 0;
    END IF;
  END FP_PORCETAJE_UTILIDAD;

  PROCEDURE PP_GUARDAR_CAMBIOS(I_EMPRESA      IN NUMBER,
                               I_DOC_CLAVE    IN NUMBER,
                               I_TIPO_ADEL    IN VARCHAR2,
                               I_EMPL_LEG     IN NUMBER,
                               I_OLD_EST_GRAL IN VARCHAR2,
                               I_NEW_EST_GRAL IN VARCHAR2,
                               I_OLD_EST_RRHH IN VARCHAR2,
                               I_NEW_EST_RRHH IN VARCHAR2,
                               I_OLD_EST_DEP  IN VARCHAR2,
                               I_NEW_EST_DEP  IN VARCHAR2,
                               I_OBS          IN VARCHAR2,
                               I_COD_PROV     IN NUMBER) AS
    V_MARC            VARCHAR2(5);
    V_ESTADO_GRAL_EST VARCHAR2(2);
    V_ESTADO_RRHH     VARCHAR2(2);
    V_ESTADO_DEP      VARCHAR2(2);
    X_QUINCENA        VARCHAR2(60);
    X_VALI_DEPAT      VARCHAR2(60);
  
    PROCEDURE PP_APROBAR_DOC AS
    BEGIN
    
      UPDATE FIN_DOCUMENTO_COMI015_TEMP
         SET COMI005_ESTADO      = 'T',
             COMI005_FEC_ACT_EST = SYSDATE,
             COMI005_LOGIN_EST   = GEN_DEVUELVE_USER
       WHERE DOC_CLAVE = I_DOC_CLAVE
         AND DOC_EMPR = I_EMPRESA;
    
    END PP_APROBAR_DOC;
  
    PROCEDURE PP_ANULAR_DOC AS
    BEGIN
    
      UPDATE FIN_DOCUMENTO_COMI015_TEMP
         SET COMI005_ESTADO      = 'A',
             COMI005_FEC_ACT_EST = SYSDATE,
             COMI005_LOGIN_EST   = GEN_DEVUELVE_USER
       WHERE DOC_CLAVE = I_DOC_CLAVE
         AND DOC_EMPR = I_EMPRESA;
    
    END PP_ANULAR_DOC;
  BEGIN
  
    ---*** VALIDACION GENERAL
  
    IF I_OLD_EST_GRAL = 'T' THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'No se puede modificar un documento ya aprobado');
    ELSIF I_OLD_EST_GRAL = 'A' THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'No se puede modificar un documento ya anulado');
    END IF;
  
    ---**** ADELANTOS A PROVEEDOR EMPLEADO
  
    IF I_TIPO_ADEL = 'P' AND I_EMPL_LEG IS NOT NULL THEN
    
      BEGIN
        SELECT 'S'
          INTO V_MARC
          FROM FIN_ADEL_APROB_ENC F
         WHERE F.ADELAPRO_FIN_COD = I_DOC_CLAVE
           AND F.ADELAPRO_EMPR = I_EMPRESA;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          INSERT INTO FIN_ADEL_APROB_ENC
            (ADELAPRO_FIN_COD, ADELAPRO_EMPR)
          VALUES
            (I_DOC_CLAVE, I_EMPRESA);
      END;
    
      IF I_OLD_EST_RRHH <> I_NEW_EST_RRHH THEN
      
        SELECT COUNT(*)
          INTO X_QUINCENA
          FROM DUAL
         WHERE I_OBS LIKE '%QUINCENA%';
      
        SELECT COUNT(*)
          INTO X_VALI_DEPAT
          FROM PER_EMPLEADO A
         WHERE EMPL_DEPARTAMENTO IN (5, 21, 6, 13, 8, 4, 22, 27, 28, 7)
           AND EMPL_CARGO NOT IN (191, 170, 167)
           AND EMPL_EMPRESA = I_EMPRESA
           AND A.EMPL_CODIGO_PROV = I_COD_PROV;
      
        IF I_OLD_EST_RRHH = 'T' THEN
          RAISE_APPLICATION_ERROR(-20010,
                                  'No se puede modificar un documento ya aprobado');
        ELSIF I_OLD_EST_RRHH = 'A' THEN
          RAISE_APPLICATION_ERROR(-20010,
                                  'No se puede modificar un documento ya anulado');
        ELSIF I_OLD_EST_DEP = 'P' AND X_QUINCENA > 0 AND X_VALI_DEPAT > 0 AND
              I_EMPRESA = 1 THEN
          RAISE_APPLICATION_ERROR(-20010,
                                  'El documento primero debe ser aprobado o anulado por el encargado de departamento');
        
        END IF;
      
        UPDATE FIN_ADEL_APROB_ENC
           SET ADELAPRO_ESTADO_RRHH = I_NEW_EST_RRHH,
               ADELAPRO_LOGIN_RRHH  = GEN_DEVUELVE_USER,
               ADELAPRO_FECHA_RRHH  = SYSDATE
         WHERE ADELAPRO_FIN_COD = I_DOC_CLAVE
           AND ADELAPRO_EMPR = I_EMPRESA;
      
      END IF;
    
      IF I_NEW_EST_DEP <> I_OLD_EST_DEP THEN
        ---*-***** VALIDACION ENCARGADO DE DEPARTAMENTO
        IF I_OLD_EST_DEP = 'T' THEN
          RAISE_APPLICATION_ERROR(-20010,
                                  'No se puede modificar un documento ya aprobado');
        ELSIF I_OLD_EST_DEP = 'A' THEN
          RAISE_APPLICATION_ERROR(-20010,
                                  'No se puede modificar un documento ya anulado');
        
        END IF;
      
        UPDATE FIN_ADEL_APROB_ENC
           SET ADELAPRO_ESTADO_DEP = I_NEW_EST_DEP,
               ADELAPRO_LOGIN_DEP  = GEN_DEVUELVE_USER,
               ADELAPRO_FECHA_DEP  = SYSDATE
         WHERE ADELAPRO_FIN_COD = I_DOC_CLAVE
           AND ADELAPRO_EMPR = I_EMPRESA;
      
      END IF;
    
      IF I_OLD_EST_GRAL = 'P' THEN
      
        SELECT NVL(ADELAPRO_ESTADO_RRHH, 'P'),
               NVL(ADELAPRO_ESTADO_DEP, 'P')
          INTO V_ESTADO_RRHH, V_ESTADO_DEP
          FROM FIN_ADEL_APROB_ENC
         WHERE ADELAPRO_FIN_COD = I_DOC_CLAVE
           AND ADELAPRO_EMPR = I_EMPRESA;
      
        IF V_ESTADO_RRHH = 'T' AND V_ESTADO_DEP = 'T' THEN
          PP_APROBAR_DOC;
        ELSIF V_ESTADO_RRHH = 'A' OR V_ESTADO_DEP = 'A' THEN
          PP_ANULAR_DOC;
        END IF;
      
      END IF;
    
    ELSE
      --****
      IF I_OLD_EST_GRAL <> I_NEW_EST_GRAL THEN
        IF I_NEW_EST_GRAL = 'T' THEN
          PP_APROBAR_DOC;
        ELSIF I_NEW_EST_GRAL = 'A' THEN
          PP_ANULAR_DOC;
        ELSE
          NULL;
        END IF;
      ELSE
        RAISE_APPLICATION_ERROR(-20010,
                                'No se encontro cambios en el estado');
      END IF;
    
    END IF;
  
  END PP_GUARDAR_CAMBIOS;

  PROCEDURE PP_GUARDAR_CAMBIOS_NEW(I_EMPRESA IN NUMBER) AS
  
    PROCEDURE PP_ACTUALIZAR_EST_DOC(I_CLAVE  IN NUMBER,
                                    I_ESTADO IN VARCHAR2) AS
    BEGIN
    
      UPDATE FIN_DOCUMENTO_COMI015_TEMP
         SET COMI005_ESTADO      = I_ESTADO,
             COMI005_FEC_ACT_EST = SYSDATE,
             COMI005_LOGIN_EST   = GEN_DEVUELVE_USER
       WHERE DOC_CLAVE = I_CLAVE
         AND DOC_EMPR = I_EMPRESA;
    
    END PP_ACTUALIZAR_EST_DOC;
  
    PROCEDURE PP_VALIDAR_CAMBIO(I_CLAVE IN NUMBER) AS
      V_ESTADO_RRHH VARCHAR2(2);
      V_ESTADO_DEP  VARCHAR2(2);
    BEGIN
    
      SELECT NVL(ADELAPRO_ESTADO_RRHH, 'P'), NVL(ADELAPRO_ESTADO_DEP, 'P')
        INTO V_ESTADO_RRHH, V_ESTADO_DEP
        FROM FIN_ADEL_APROB_ENC
       WHERE ADELAPRO_FIN_COD = I_CLAVE
         AND ADELAPRO_EMPR = I_EMPRESA;
    
      IF V_ESTADO_RRHH = 'T' AND V_ESTADO_DEP = 'T' THEN
        PP_ACTUALIZAR_EST_DOC(I_CLAVE => I_CLAVE, I_ESTADO => 'T');
      ELSIF V_ESTADO_RRHH = 'A' OR V_ESTADO_DEP = 'A' THEN
        PP_ACTUALIZAR_EST_DOC(I_CLAVE => I_CLAVE, I_ESTADO => 'A');
      END IF;
    
    END PP_VALIDAR_CAMBIO;
  
    PROCEDURE PP_INSERTAR_DOC_ADELANTO(I_CLAVE IN NUMBER) AS
      V_MARC VARCHAR2(2);
    BEGIN
      SELECT 'S'
        INTO V_MARC
        FROM FIN_ADEL_APROB_ENC F
       WHERE F.ADELAPRO_FIN_COD = I_CLAVE
         AND F.ADELAPRO_EMPR = I_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        INSERT INTO FIN_ADEL_APROB_ENC
          (ADELAPRO_FIN_COD, ADELAPRO_EMPR)
        VALUES
          (I_CLAVE, I_EMPRESA);
    END PP_INSERTAR_DOC_ADELANTO;
  
    PROCEDURE PP_ACTALIZAR_EST_RRHH(I_CLAVE  IN NUMBER,
                                    I_ESTADO IN VARCHAR2) AS
    BEGIN
      UPDATE FIN_ADEL_APROB_ENC
         SET ADELAPRO_ESTADO_RRHH = I_ESTADO,
             ADELAPRO_LOGIN_RRHH  = GEN_DEVUELVE_USER,
             ADELAPRO_FECHA_RRHH  = SYSDATE
       WHERE ADELAPRO_FIN_COD = I_CLAVE
         AND ADELAPRO_EMPR = I_EMPRESA;
    
      PP_VALIDAR_CAMBIO(I_CLAVE => I_CLAVE);
    
    END PP_ACTALIZAR_EST_RRHH;
  
    PROCEDURE PP_ACTUALIZAR_EST_DEPART(I_CLAVE  IN NUMBER,
                                       I_ESTADO IN VARCHAR2) AS
    BEGIN
      UPDATE FIN_ADEL_APROB_ENC
         SET ADELAPRO_ESTADO_DEP = I_ESTADO,
             ADELAPRO_LOGIN_DEP  = GEN_DEVUELVE_USER,
             ADELAPRO_FECHA_DEP  = SYSDATE
       WHERE ADELAPRO_FIN_COD = I_CLAVE
         AND ADELAPRO_EMPR = I_EMPRESA;
    
      PP_VALIDAR_CAMBIO(I_CLAVE => I_CLAVE);
    
    END PP_ACTUALIZAR_EST_DEPART;
  
  BEGIN
  
    --****** RRHH *****-----
    -- APROBAR   APEX_APPLICATION.G_F01.COUNT   
    FOR I IN 1 .. APEX_APPLICATION.G_F01.COUNT LOOP
      PP_INSERTAR_DOC_ADELANTO(I_CLAVE => APEX_APPLICATION.G_F01(I));
      PP_ACTALIZAR_EST_RRHH(I_CLAVE  => APEX_APPLICATION.G_F01(I),
                            I_ESTADO => 'T');
    END LOOP;
  
    -- ANULAR   APEX_APPLICATION.G_F02.COUNT 
    FOR I IN 1 .. APEX_APPLICATION.G_F02.COUNT LOOP
      PP_INSERTAR_DOC_ADELANTO(I_CLAVE => APEX_APPLICATION.G_F02(I));
      PP_ACTALIZAR_EST_RRHH(I_CLAVE  => APEX_APPLICATION.G_F02(I),
                            I_ESTADO => 'A');
    
    END LOOP;
  
    --**** DEPARTAMENTO ****-----  
    -- APROBAR   APEX_APPLICATION.G_F03.COUNT 
  
    FOR I IN 1 .. APEX_APPLICATION.G_F03.COUNT LOOP
      PP_INSERTAR_DOC_ADELANTO(I_CLAVE => APEX_APPLICATION.G_F03(I));
      PP_ACTUALIZAR_EST_DEPART(I_CLAVE  => APEX_APPLICATION.G_F03(I),
                               I_ESTADO => 'T');
    END LOOP;
  
    -- ANULAR   APEX_APPLICATION.G_F04.COUNT 
    FOR I IN 1 .. APEX_APPLICATION.G_F04.COUNT LOOP
      PP_INSERTAR_DOC_ADELANTO(I_CLAVE => APEX_APPLICATION.G_F04(I));
      PP_ACTUALIZAR_EST_DEPART(I_CLAVE  => APEX_APPLICATION.G_F04(I),
                               I_ESTADO => 'A');
    END LOOP;
  
    ---**** ESTADO GENERAL *****----  
    -- APROBAR   APEX_APPLICATION.G_F05.COUNT 
  
    FOR I IN 1 .. APEX_APPLICATION.G_F05.COUNT LOOP
      PP_ACTUALIZAR_EST_DOC(I_CLAVE  => APEX_APPLICATION.G_F05(I),
                            I_ESTADO => 'T');
    END LOOP;
  
    -- ANULAR   APEX_APPLICATION.G_F06.COUNT 
    FOR I IN 1 .. APEX_APPLICATION.G_F06.COUNT LOOP
      PP_ACTUALIZAR_EST_DOC(I_CLAVE  => APEX_APPLICATION.G_F06(I),
                            I_ESTADO => 'A');
    END LOOP;
  
  END PP_GUARDAR_CAMBIOS_NEW;

END FINI055;
/
