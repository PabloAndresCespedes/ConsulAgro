create or replace package gen_cotizacion_pck is

  -- Author  : PROGRAMACION7
  -- Created : 09/08/2022 15:21:43
  -- Purpose : paquete de sicronizacion de cotizacion de HILAGRO
  
  procedure sincronizar_cotizacion;

end gen_cotizacion_pck;
/
create or replace package body gen_cotizacion_pck is

  procedure sincronizar_cotizacion as
    co_hilagro constant number := 1;
    co_cagro   constant number := 1;
  begin
    insert into stk_cotizacion(cot_fec,
                               cot_mon,
                               cot_tasa,
                               cot_empr,
                               cot_fec_ingreso,
                               cot_login_ingreso,
                               cot_compra,
                               cot_venta)
    select ch.cot_fec,
           ch.cot_mon,
           ch.cot_tasa,
           ch.cot_empr,
           current_date,
           'ADCS',
           ch.cot_compra,
           ch.cot_venta
    from stk_cotizacion_hilagro ch
    where ch.cot_empr = co_hilagro
    and   ch.cot_fec >= add_months(current_date, -1) --> un mes atras basta y sobra
    and   ch.cot_fec ||''|| ch.cot_mon not in (select x.cot_fec||''||x.cot_mon from stk_cotizacion x where x.cot_empr=co_cagro);
  exception
    when dup_val_on_index then
      null;
  end sincronizar_cotizacion;
end gen_cotizacion_pck;
/
