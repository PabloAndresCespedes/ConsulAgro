CREATE OR REPLACE PACKAGE FINP006 IS

  -- AUTHOR  : PROGRAMACION13
  -- CREATED : 03/05/2021 22:41:12
  -- PURPOSE :  IMPORTAR RETENCIONES RECIBIDAS

  PROCEDURE PP_IMPORT_EXCEL(P_EMPRESA IN NUMBER, P_ARCHIVO IN VARCHAR2);

  FUNCTION FIN_IMP_RETEN_REC(P_SUCURSAL IN NUMBER,
                             P_CAJA     IN NUMBER,
                             P_EMPRESA  IN NUMBER) RETURN NUMBER;

  FUNCTION FIN_IMP_RETEN_REC_TAGRO RETURN NUMBER;

END FINP006;
/
CREATE OR REPLACE PACKAGE BODY FINP006 IS

  PROCEDURE PP_IMPORT_EXCEL(P_EMPRESA IN NUMBER, P_ARCHIVO IN VARCHAR2) IS

    CURSOR C_FILE IS
      WITH FILES AS
       (SELECT D.BLOB_CONTENT, D.FILENAME
          FROM APEX_APPLICATION_TEMP_FILES D
         WHERE D.NAME = P_ARCHIVO
           AND ROWNUM = 1)
      SELECT LINE_NUMBER,
             COL001,
             COL002,
             COL003,
             COL004,
             COL005,
             COL006,
             COL007,
             COL008,
             COL009,
             COL010,
             COL011,
             COL012,
             COL013,
             COL014,
             COL015,
             COL016,
             COL017,
             COL018,
             COL019,
             COL020,
             COL021,
             /*to_number((COL022),
'9999999999999D99',
'NLS_NUMERIC_CHARACTERS=''. ''')*/
-- 02/09/2022 07:51 @PabloACespedes, ajustado en @Cagro ya que tiene otro lenguaje la ddbb
-- #105 Issue
             COL022, --SE FORMATEO DE ESTA MANERA PARA PODER TRATAR EL PUNTO FLOTANTE DEL XLSX
             COL023,
             COL024,
             ROW_INFO
        FROM FILES F
       CROSS JOIN TABLE (APEX_DATA_PARSER.PARSE(P_CONTENT   => F.BLOB_CONTENT,
                               P_FILE_NAME => F.FILENAME)) EXCEL
       WHERE LINE_NUMBER > 1;
  BEGIN
    IF P_ARCHIVO IS NULL THEN
      RAISE_APPLICATION_ERROR(-20001,
                              'Archivo destino no se ha especificado correctamente.');
    END IF;

    DELETE FROM FIN_RETENCION_REC
    WHERE LOGIN = GEN_DEVUELVE_USER
    AND   EMPR = P_EMPRESA;
    
    COMMIT;
    
    FOR REC IN C_FILE LOOP


  --  RAISE_APPLICATION_ERROR(-20001,  REC.COL018);
      INSERT INTO FIN_RETENCION_REC
      VALUES
        (REC.COL001,
         REC.COL002,
        to_date( TO_DATE(REC.COL003,'yyyy-MM-dd'),'dd/mm/yyyy' ),
         REC.COL004,
         REC.COL005,
         REC.COL006,
         REC.COL007,
         REC.COL008,
         REC.COL009,
         REC.COL010,
         REC.COL011,
        to_date( TO_DATE(REC.COL012,'yyyy-MM-dd'),'dd/mm/yyyy'),
         REC.COL013,
         REC.COL014,
         REC.COL015,
         REC.COL016,
         REC.COL017,
        to_date( TO_DATE(REC.COL018,'yyyy-MM-dd'),'dd/mm/yyyy'),
         REC.COL019,
         REC.COL020,
         REC.COL021,
         REC.COL022,
         REC.COL023,
         REC.COL024,
         GEN_DEVUELVE_USER,
         P_EMPRESA,
         0);

    END LOOP;

    --SE ELIMINA PARA LIBERAR EL CURSOR, PARA QUE PUEDA IMPORTAR BIEN EN LA SIGTE VEZ
    DELETE FROM APEX_APPLICATION_TEMP_FILES M
    WHERE M.NAME = P_ARCHIVO
    AND ROWNUM = 1;
    
  END PP_IMPORT_EXCEL;

  FUNCTION FIN_IMP_RETEN_REC(P_SUCURSAL IN NUMBER,
                             P_CAJA     IN NUMBER,
                             P_EMPRESA  IN NUMBER) RETURN NUMBER IS
    V_CANT                NUMBER := 0;
    V_TMOV                NUMBER := 22; --RETENCIONES RECIBIDAS.
    V_TIMBRADO_GRAL       NUMBER := 99999999; --TIMBRADO GRAL.
    V_OBS                 VARCHAR2(50);
    V_DCON_CLAVE_CONCEPTO NUMBER := 883; --RETENCION RECIBIDA IVA.
    V_DCON_CLAVE_CTACO    NUMBER := 838; --RETENCION RECIBIDA IVA.
    V_CLAVE_RET           NUMBER;
    V_EXISTE_RETENCION    NUMBER;
    V_CLAVE_RETENCION     NUMBER;

    CURSOR CUR_RETENCIONES IS
      SELECT *
        FROM FIN_RETENCION_REC R, FIN_DOCUMENTO, FIN_CLIENTE
       WHERE DOC_CLI = CLI_CODIGO
         AND DOC_EMPR = CLI_EMPR
         AND REPLACE(SUBSTR(COMPROBANTE_VENTA, 1, 8) ||
                     REPLACE(LPAD(SUBSTR(COMPROBANTE_VENTA, 9, 8), 7, 0),
                             '-',
                             ''),
                     '-',
                     '') = DOC_NRO_DOC
         AND CLI_RUC_DV = R.RUC_INFORMANTE
         AND TO_NUMBER(TIMBRADO) = DOC_TIMBRADO
         AND DOC_TIPO_MOV IN (9, 10)
         AND DOC_SUC = P_SUCURSAL
         AND R.EMPR = DOC_EMPR
         AND DOC_EMPR = P_EMPRESA
            ----
            --  AND COMPROBANTE_VENTA='002-001-0037238'
            ----
         AND LOGIN = gen_devuelve_user
         AND REGEXP_LIKE(COMPROBANTE_VENTA, '-[0-9]') --ESTAS LINEAS, SON PARA QUE EVITE IMPORTAR COMPROBANTES QUE TIENEN LETRAS EN LA PARTE DEL NUMERO DE FACTURA.
      ;

    CURSOR MODIFICA_NRO_DOC IS
      SELECT ROWID,
             SUBSTR(COMPROBANTE_VENTA, 1, 8) ||
             LPAD(SUBSTR(COMPROBANTE_VENTA, 9), 7, 0) NRO_COMPROBANTE
        FROM FIN_RETENCION_REC R
       WHERE R.LOGIN = gen_devuelve_user
         AND LENGTH(COMPROBANTE_VENTA) < 15
         AND R.EMPR = P_EMPRESA;

    CURSOR CUR_RETENCIONES_TAGRO IS
      SELECT *
        FROM FIN_RETENCION_REC R, FIN_DOCUMENTO, FIN_PERSONA
       WHERE DOC_PROV = PNA_CODIGO
         AND DOC_TIPO_MOV IN (1, 2)
         AND REPLACE(NUMERO_FACTURA_COMIS, '-', '') = DOC_NRO_DOC
         AND TIMBRADO_FACTURA_COMIS = DOC_NRO_TIMBRADO
         AND R.EMPR = DOC_EMPR
         AND DOC_EMPR = P_EMPRESA
         AND LOGIN = gen_devuelve_user;

  BEGIN
    
   
    IF P_EMPRESA = 2 THEN

      UPDATE FIN_RETENCION_REC
         SET TIPO                   = REPLACE(TIPO, '"', ''),
             ESTADO                 = REPLACE(ESTADO, '"', ''),
             COMPROBANTE            = REPLACE(COMPROBANTE, '"', ''),
             RUC_INFORMANTE         = REPLACE(RUC_INFORMANTE, '"', ''),
             INFORMANTE             = REPLACE(INFORMANTE, '"', ''),
             INFORMADO_TIPO         = REPLACE(INFORMADO_TIPO, '"', ''),
             RUC_INFORMADO          = REPLACE(RUC_INFORMADO, '"', ''),
             IDENTIFICACION         = REPLACE(IDENTIFICACION, '"', ''),
             INFORMADO              = REPLACE(INFORMADO, '"', ''),
             CONTROL                = REPLACE(CONTROL, '"', ''),
             FECHA_EMISION          = REPLACE(FECHA_EMISION, '"', ''),
             COMPROBANTE_VENTA      = REPLACE(COMPROBANTE_VENTA, '"', ''),
             TIMBRADO               = REPLACE(TIMBRADO, '"', ''),
             TOTAL_RENTA            = REPLACE(TOTAL_RENTA, '"', ''),
             TOTAL_IVA              = REPLACE(TOTAL_IVA, '"', ''),
             TOTAL_CABEZAS          = REPLACE(TOTAL_CABEZAS, '"', ''),
             TOTAL_TONELADAS        = REPLACE(TOTAL_TONELADAS, '"', ''),
             NUMERO_FACTURA_COMIS   = REPLACE(NUMERO_FACTURA_COMIS, '"', ''),
             TIMBRADO_FACTURA_COMIS = REPLACE(TIMBRADO_FACTURA_COMIS,
                                              '"',
                                              '')
       WHERE LOGIN = GEN_DEVUELVE_USER
         AND EMPR = P_EMPRESA;

      FOR R IN CUR_RETENCIONES_TAGRO LOOP
        --OBTENEMOS LA CLAVE DE LA RETENCION
        BEGIN
          SELECT DOC_CLAVE
            INTO V_CLAVE_RETENCION
            FROM FIN_DOCUMENTO
           WHERE DOC_TIPO_MOV IN (62)
             AND DOC_CLAVE_PADRE IN (R.DOC_CLAVE_PADRE)
             AND DOC_EMPR = P_EMPRESA
             AND DOC_CTRL_RET_REC IS NULL;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            V_CLAVE_RETENCION := 0;
        END;
        --ACTUALIZAMOS LOS NUMEROS, TIMBRADO Y CONTROL DE LA RETENCION.
        BEGIN
          UPDATE FIN_DOCUMENTO
             SET DOC_NRO_DOC      = REPLACE(R.COMPROBANTE, '-', ''),
                 DOC_NRO_TIMBRADO = R.TIMBRADO_FACTURA_COMIS,
                 DOC_CTRL_RET_REC = R.CONTROL
           WHERE DOC_CLAVE = V_CLAVE_RETENCION
             AND DOC_EMPR = P_EMPRESA;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-21000, SQLERRM);
        END;
        COMMIT;
        V_CANT := V_CANT + 1;
      END LOOP;

      RETURN NVL(V_CANT, 0);

    ELSE
      UPDATE FIN_RETENCION_REC
         SET TIPO              = REPLACE(TIPO, '"', ''),
             ESTADO            = REPLACE(ESTADO, '"', ''),
             COMPROBANTE       = REPLACE(COMPROBANTE, '"', ''),
             RUC_INFORMANTE    = REPLACE(RUC_INFORMANTE, '"', ''),
             INFORMANTE        = REPLACE(INFORMANTE, '"', ''),
             INFORMADO_TIPO    = REPLACE(INFORMADO_TIPO, '"', ''),
             RUC_INFORMADO     = REPLACE(RUC_INFORMADO, '"', ''),
             IDENTIFICACION    = REPLACE(IDENTIFICACION, '"', ''),
             INFORMADO         = REPLACE(INFORMADO, '"', ''),
             CONTROL           = REPLACE(CONTROL, '"', ''),
             FECHA_EMISION     = REPLACE(FECHA_EMISION, '"', ''),
             COMPROBANTE_VENTA = REPLACE(COMPROBANTE_VENTA, '"', ''),
             TIMBRADO          = REPLACE(TIMBRADO, '"', ''),
             TOTAL_RENTA       = REPLACE(REPLACE(REPLACE(TOTAL_RENTA,
                                                         '"',
                                                         ''),
                                                 '.',
                                                 NULL),
                                         ',',
                                         '.'),
             TOTAL_IVA         = REPLACE(REPLACE(REPLACE(TOTAL_IVA, '"', ''),
                                                 '.',
                                                 NULL),
                                         ',',
                                         '.'),
             TOTAL_CABEZAS     = REPLACE(TOTAL_CABEZAS, '"', ''),
             TOTAL_TONELADAS   = REPLACE(TOTAL_TONELADAS, '"', '')
       WHERE LOGIN = GEN_DEVUELVE_USER
         AND EMPR = P_EMPRESA;

      -- ESTO ARREGLA LOS COMPROBANTES QUE TIENEN PROBLEMAS EN LA NUMERACION
      FOR C IN MODIFICA_NRO_DOC LOOP
        UPDATE FIN_RETENCION_REC
           SET COMPROBANTE_VENTA = C.NRO_COMPROBANTE
         WHERE ROWID = C.ROWID;
      END LOOP;

      FOR R IN CUR_RETENCIONES LOOP

        BEGIN
          SELECT DOC_CLAVE
            INTO V_EXISTE_RETENCION
            FROM FIN_DOCUMENTO
           WHERE DOC_TIPO_MOV = V_TMOV
             AND DOC_EMPR = P_EMPRESA
             AND DOC_NRO_DOC = TO_NUMBER(REPLACE(R.COMPROBANTE, '-', NULL))
             AND DOC_CTRL_RET_REC = R.CONTROL;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            V_EXISTE_RETENCION := NULL;
        END;

        IF V_EXISTE_RETENCION IS NULL THEN
          V_OBS       := SUBSTR('RET IVA ' || R.CLI_NOM, 1, 40);
          V_CLAVE_RET := FIN_SEQ_DOC_NEXTVAL;

          --FIN_DOCUMENTO
          INSERT INTO FIN_DOCUMENTO
            (DOC_CLAVE,
             DOC_EMPR,
             DOC_CTA_BCO,
             DOC_SUC,
             DOC_TIPO_MOV,
             DOC_NRO_DOC,
             DOC_TIPO_SALDO,
             DOC_MON,
             DOC_CLI,
             DOC_CLI_NOM,
             DOC_CLI_DIR,
             DOC_CLI_RUC,
             DOC_CLI_TEL,
             DOC_FEC_OPER,
             DOC_FEC_DOC,
             DOC_BRUTO_EXEN_LOC,
             DOC_BRUTO_EXEN_MON,
             DOC_BRUTO_GRAV_LOC,
             DOC_BRUTO_GRAV_MON,
             DOC_NETO_EXEN_LOC,
             DOC_NETO_EXEN_MON,
             DOC_NETO_GRAV_LOC,
             DOC_NETO_GRAV_MON,
             DOC_IVA_LOC,
             DOC_IVA_MON,
             DOC_SALDO_LOC,
             DOC_SALDO_MON,
             DOC_OBS,
             DOC_BASE_IMPON_LOC,
             DOC_BASE_IMPON_MON,
             DOC_LOGIN,
             DOC_FEC_GRAB,
             DOC_SIST,
             DOC_OPERADOR,
             DOC_TASA,
             DOC_IVA_10_LOC,
             DOC_IVA_10_MON,
             DOC_IVA_5_LOC,
             DOC_IVA_5_MON,
             DOC_GRAV_10_LOC,
             DOC_GRAV_10_MON,
             DOC_GRAV_5_MON,
             DOC_GRAV_5_LOC,
             DOC_IND_CANC_PMO,
             DOC_CANAL,
             DOC_TIMBRADO,
             DOC_CTRL_RET_REC)
          VALUES
            (V_CLAVE_RET,
             R.DOC_EMPR,
             P_CAJA,
             P_SUCURSAL,
             V_TMOV,
             REPLACE(R.COMPROBANTE, '-', ''),
             'C',
             1,
             R.DOC_CLI,
             R.CLI_NOM,
             R.CLI_DIR,
             R.CLI_RUC_DV,
             R.CLI_TEL,
             TO_DATE(R.FECHA_EMISION, 'DD/MM/YYYY'),
             TO_DATE(R.FECHA_EMISION, 'dd/mm/yyyy'),
             R.TOTAL_IVA,
             R.TOTAL_IVA,
             0,
             0,
             R.TOTAL_IVA,
             R.TOTAL_IVA,
             0,
             0,
             0,
             0,
             0,
             0,
             V_OBS,
             0,
             0,
             GEN_DEVUELVE_USER,
             SYSDATE,
             'FIN',
             2,
             1,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             'N',
             R.DOC_CANAL,
             V_TIMBRADO_GRAL,
             R.CONTROL);

          --FIN_DOC_CONCEPTO
          INSERT INTO FIN_DOC_CONCEPTO
            (DCON_CLAVE_DOC,
             DCON_ITEM,
             DCON_CLAVE_CONCEPTO,
             DCON_CLAVE_CTACO,
             DCON_TIPO_SALDO,
             DCON_EXEN_LOC,
             DCON_EXEN_MON,
             DCON_GRAV_LOC,
             DCON_GRAV_MON,
             DCON_IVA_LOC,
             DCON_IVA_MON,
             DCON_OBS,
             DCON_PORC_IVA,
             DCON_CANAL,
             DCON_EMPR)
          VALUES
            (V_CLAVE_RET,
             1,
             V_DCON_CLAVE_CONCEPTO,
             V_DCON_CLAVE_CTACO,
             'C',
             R.TOTAL_IVA,
             R.TOTAL_IVA,
             0,
             0,
             0,
             0,
             V_OBS,
             0,
             R.DOC_CANAL,
             P_EMPRESA);

          V_CANT := V_CANT + 1;

        END IF;

      END LOOP;

      RETURN NVL(V_CANT, 0);
    END IF;
  END FIN_IMP_RETEN_REC;

  FUNCTION FIN_IMP_RETEN_REC_TAGRO RETURN NUMBER IS

    CURSOR CUR_RETENCIONES IS
      SELECT *
        FROM FIN_RETENCION_REC R, FIN_DOCUMENTO, FIN_PERSONA
       WHERE EMPR = DOC_EMPR
         AND DOC_EMPR = PNA_EMPR
         AND DOC_PROV = PNA_CODIGO
         AND DOC_TIPO_MOV IN (1, 2)
         AND REPLACE(NUMERO_FACTURA_COMIS, '-', '') = DOC_NRO_DOC
         AND TIMBRADO_FACTURA_COMIS = DOC_NRO_TIMBRADO
         AND R.EMPR = DOC_EMPR
         AND LOGIN = gen_devuelve_user
         AND DOC_EMPR = 2; --TRANSAGRO

    V_CANT            NUMBER;
    V_CLAVE_RETENCION NUMBER;

  BEGIN

    UPDATE FIN_RETENCION_REC
       SET TIPO                   = REPLACE(TIPO, '"', ''),
           ESTADO                 = REPLACE(ESTADO, '"', ''),
           COMPROBANTE            = REPLACE(COMPROBANTE, '"', ''),
           RUC_INFORMANTE         = REPLACE(RUC_INFORMANTE, '"', ''),
           INFORMANTE             = REPLACE(INFORMANTE, '"', ''),
           INFORMADO_TIPO         = REPLACE(INFORMADO_TIPO, '"', ''),
           RUC_INFORMADO          = REPLACE(RUC_INFORMADO, '"', ''),
           IDENTIFICACION         = REPLACE(IDENTIFICACION, '"', ''),
           INFORMADO              = REPLACE(INFORMADO, '"', ''),
           CONTROL                = REPLACE(CONTROL, '"', ''),
           FECHA_EMISION          = REPLACE(FECHA_EMISION, '"', ''),
           COMPROBANTE_VENTA      = REPLACE(COMPROBANTE_VENTA, '"', ''),
           TIMBRADO               = REPLACE(TIMBRADO, '"', ''),
           TOTAL_RENTA            = REPLACE(TOTAL_RENTA, '"', ''),
           TOTAL_IVA              = REPLACE(TOTAL_IVA, '"', ''),
           TOTAL_CABEZAS          = REPLACE(TOTAL_CABEZAS, '"', ''),
           TOTAL_TONELADAS        = REPLACE(TOTAL_TONELADAS, '"', ''),
           NUMERO_FACTURA_COMIS   = REPLACE(NUMERO_FACTURA_COMIS, '"', ''),
           TIMBRADO_FACTURA_COMIS = REPLACE(TIMBRADO_FACTURA_COMIS, '"', ''),
           EMPR                   = 2
     WHERE LOGIN = GEN_DEVUELVE_USER
       AND EMPR = 2;

    V_CANT := 0;
    FOR R IN CUR_RETENCIONES LOOP
      --OBTENEMOS LA CLAVE DE LA RETENCION
      BEGIN
        SELECT DOC_CLAVE
          INTO V_CLAVE_RETENCION
          FROM FIN_DOCUMENTO
         WHERE DOC_TIPO_MOV IN (62)
           AND DOC_CLAVE_PADRE IN (R.DOC_CLAVE_PADRE)
           AND DOC_CTRL_RET_REC IS NULL
           AND DOC_EMPR = 2;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          V_CLAVE_RETENCION := 0;
      END;
      --ACTUALIZAMOS LOS NUMEROS, TIMBRADO Y CONTROL DE LA RETENCION.
      BEGIN
        UPDATE FIN_DOCUMENTO
           SET DOC_NRO_DOC      = REPLACE(R.COMPROBANTE, '-', ''),
               DOC_NRO_TIMBRADO = R.TIMBRADO_FACTURA_COMIS,
               DOC_CTRL_RET_REC = R.CONTROL
         WHERE DOC_CLAVE = V_CLAVE_RETENCION
           AND DOC_EMPR = 2;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          RAISE_APPLICATION_ERROR(-21000, SQLERRM);
      END;
      COMMIT;
      V_CANT := V_CANT + 1;
    END LOOP;

    RETURN NVL(V_CANT, 0);

  END FIN_IMP_RETEN_REC_TAGRO;

END FINP006;
/
