CREATE OR REPLACE PACKAGE FINM027 IS

  -- AUTHOR  : PROGRAMACION4
  -- CREATED : 01/09/2018 9:34:38
  -- PURPOSE :
  
  -- Modified: @PabloACespedes
  -- Date: 03/09/2022 07:19
  -- Crear archivos para Banco Regiona y configuracion dinamica de cuentas bancarias para archivos

  PROCEDURE PP_GUARDAR_AGENDA(P_EMPRESA   IN NUMBER,
                              I_NRO_ORDEN NUMBER,
                              I_CTA_BCO   IN NUMBER);

  PROCEDURE PP_SACAR_AGENDA(P_EMPRESA IN NUMBER);

  PROCEDURE PP_PREPARAR_TXT(I_EMPRESA   IN NUMBER,
                            I_NRO_ORDEN IN NUMBER,
                            I_CTA_BCO   IN NUMBER);

  PROCEDURE PP_VALIDAR_CUENTA_DESTINO(I_EMPRESA   IN NUMBER,
                                      I_NRO_ORDEN IN NUMBER,
                                      I_CTA_BCO   IN NUMBER DEFAULT NULL);

  FUNCTION FP_ORDEN_ABIERTO(I_EMPRESA IN NUMBER, I_NRO_ORDEN IN NUMBER)
    RETURN VARCHAR2;

  PROCEDURE PP_CERRAR_ORDEN(I_EMPRESA IN NUMBER, I_NRO_ORDEN IN NUMBER);

  PROCEDURE PP_REALIZAR_TRANSFERENCIA(I_EMPRESA   IN NUMBER,
                                      I_NRO_ORDEN IN NUMBER);

  PROCEDURE PP_GENERAR_RECIBO(I_EMPRESA IN NUMBER, I_NRO_ORDEN IN NUMBER);

  FUNCTION FP_ORDEN_TRANSFERIDO(I_EMPRESA IN NUMBER, I_NRO_ORDEN IN NUMBER)
    RETURN BOOLEAN;
  PROCEDURE PP_VERIF_INTERES(BSEL_S_HOLDING      IN NUMBER,
                             BSEL_S_MON          IN NUMBER,
                             BSEL_S_PORC_INTERES OUT NUMBER,
                             P_EMPRESA           IN NUMBER);



  PROCEDURE PP_CARGAR_DETALLE(I_HOLDING      IN NUMBER,
                              I_SALDO_CERO   IN VARCHAR2,
                              I_MONEDA       IN NUMBER,
                              I_HISTORICO    IN VARCHAR2,
                              I_FECHA_VTO    IN DATE,
                              I_PORC_INTERES IN NUMBER,
                              I_MON_DEC_IMP  IN NUMBER,
                              O_S_PENDIENTES OUT NUMBER,
                              O_S_VENCIDOS   OUT NUMBER,
                              O_S_INTERESES  OUT NUMBER,
                              I_SESSION_ID   IN NUMBER,
                              P_EMPRESA      IN NUMBER,
                              I_ORDENADO_POR IN VARCHAR2,
                              I_S_TIPO_SALDO IN VARCHAR2,
                              I_USUARIO      IN VARCHAR2,
                              I_DET_CONS     IN VARCHAR2 DEFAULT NULL);

--PROCEDURE PP_RECALCULAR_RETENCION(I_EMPRESA IN NUMBER);

  -- 02/09/2022 10:58:58 @PabloACespedes \(^-^)/
  -- configuracion de cuentas para bancos
  -- se_parametriza todos los bancos y cual hace referencia, 
  -- para aplicar las reglas de programacion
  procedure add_conf_bank(
    in_empresa      in number,
    in_regional_gs  in number,
    in_regional_usd in number
  );
  
  -- 02/09/2022 13:42:51 @PabloACespedes \(^-^)/
  -- obtiene configuracion de archivos bancarios
  -- GENERA UN JSON PORQUE SE_VISUALIZA EN UN POP LOV DE APEX
  procedure get_conf_archive(
    in_empresa            in number
  );
  
  
END FINM027;
/
CREATE OR REPLACE PACKAGE BODY FINM027 IS

  PROCEDURE PP_GUARDAR_AGENDA(P_EMPRESA   IN NUMBER,
                              I_NRO_ORDEN NUMBER,
                              I_CTA_BCO   IN NUMBER) IS
                              
                              
    CURSOR CUR1(I_ROWID VARCHAR2) IS
      SELECT FD.DOC_NRO_DOC,
             FD.DOC_TIPO_MOV,
             FD.DOC_EMPR,
             TM.TMOV_ABREV,
             FD.DOC_FEC_DOC,
             FC.CUO_FEC_VTO,
             FP.PROV_CODIGO,
             FP.PROV_RAZON_SOCIAL,
             FD.DOC_MON,
             M.MON_DEC_IMP,
             M.MON_SIMBOLO,
             M.MON_DESC,
             FC.CUO_IMP_MON,
             FC.CUO_SALDO_MON,
             FC.CUO_CLAVE_DOC,
             FC.CUO_NRO_PAGARE,
             FD.DOC_CANT_PAGARE,
             FD.DOC_CLAVE_RETENCION,
             (NVL(FD.DOC_NETO_GRAV_MON, 0)) GRAVADO_MON,
             (NVL(FD.DOC_NETO_GRAV_LOC, 0)) GRAVADO_LOC,
             (NVL(FD.DOC_NETO_EXEN_MON, 0)) EXENTO,
             (NVL(FD.DOC_NETO_EXEN_LOC, 0)) EXENTO_LOC,
             (NVL(FD.DOC_IVA_MON, 0)) IVA_MON,
             (NVL(FD.DOC_IVA_LOC, 0)) IVA_LOC,
             NVL(FP.PROV_BLOQUEAR_PAGO, 'N') PROV_BLOQUEAR_PAGO
        FROM FIN_DOCUMENTO FD,
             FIN_CUOTA     FC,
             FIN_PROVEEDOR FP,
             GEN_MONEDA    M,
             GEN_TIPO_MOV  TM
       WHERE FD.DOC_EMPR = P_EMPRESA
         AND FD.DOC_EMPR = FC.CUO_EMPR
         AND FD.DOC_EMPR = FP.PROV_EMPR
         AND FD.DOC_EMPR = TM.TMOV_EMPR
         AND FD.DOC_EMPR = M.MON_EMPR
         AND FC.ROWID = I_ROWID
         AND FC.CUO_CLAVE_DOC = FD.DOC_CLAVE
         AND FD.DOC_PROV = FP.PROV_CODIGO
         AND FD.DOC_TIPO_MOV = TM.TMOV_CODIGO
         AND FC.CUO_SALDO_MON > 0
         AND FD.DOC_MON = M.MON_CODIGO
         AND (FD.DOC_CLAVE, FC.CUO_FEC_VTO) NOT IN
             (SELECT D.FOP_CLAVE_DOC, D.FOP_FEC_VTO
                FROM FIN_ORDEN_PAGO_DET D
               WHERE D.FOP_EMPR = P_EMPRESA)
         AND ((FD.DOC_TIPO_MOV IN (2, 17, 14)));

    CURSOR CUR2(I_PROV NUMBER) IS
      SELECT FD.DOC_FEC_DOC,
             D.FOP_NRO,
             FC.CUO_FEC_VTO,
             FD.DOC_PROV,
             FD.DOC_EMPR,
             FD.DOC_MON,
             FC.CUO_IMP_MON,
             FC.CUO_SALDO_MON,
             FC.CUO_CLAVE_DOC,
             FC.CUO_NRO_PAGARE,
             FD.DOC_CANT_PAGARE,
             FD.DOC_CLAVE_RETENCION,
             (NVL(FD.DOC_NETO_GRAV_MON, 0)) GRAVADO_MON,
             (NVL(FD.DOC_NETO_GRAV_LOC, 0)) GRAVADO_LOC,
             (NVL(FD.DOC_NETO_EXEN_MON, 0)) EXENTO,
             (NVL(FD.DOC_NETO_EXEN_LOC, 0)) EXENTO_LOC,
             (NVL(FD.DOC_IVA_MON, 0)) IVA_MON,
             (NVL(FD.DOC_IVA_LOC, 0)) IVA_LOC
        FROM FIN_DOCUMENTO FD, FIN_CUOTA FC, FIN_ORDEN_PAGO_DET D
       WHERE FD.DOC_EMPR = FC.CUO_EMPR
         AND FC.CUO_CLAVE_DOC = FD.DOC_CLAVE
         AND FD.DOC_CLAVE = D.FOP_CLAVE_DOC
         AND FC.CUO_FEC_VTO = D.FOP_FEC_VTO
         AND FD.DOC_EMPR = P_EMPRESA
         AND D.FOP_PROVEEDOR = I_PROV
         AND D.FOP_NRO = I_NRO_ORDEN
         AND D.FOP_EMPR = FD.DOC_EMPR;
         
    

    CURSOR PROVEEDORES IS
      SELECT DISTINCT FINI003_CLAVE_CUO COD_PROVEEDOR
        FROM FIN_FINI003_TEMP;

    V_NCR_EXENTO_LOC    NUMBER;
    V_NCR_EXENTO_MON    NUMBER;
    V_NCR_GRAVADO_LOC   NUMBER;
    V_NCR_GRAVADO_MON   NUMBER;
    V_NCR_IVA_LOC       NUMBER;
    V_NCR_IVA_MON       NUMBER;
    V_SUM_GRAB_ACUM_LOC NUMBER;
    V_TOTAL_FACTURA_LOC NUMBER;
    V_TOTAL_FACTURA_MON NUMBER;
    V_FACTURA_RETENIDA  NUMBER;
    V_EXISTE_RET        NUMBER;
    V_RETENCION_MON     NUMBER := 0;
    V_RETENCION         NUMBER := 0;
    V_RETENCION_LOC     NUMBER := 0;
    V_IMPORTE_PAGO      NUMBER;
    V_BCO_ORIGEN        NUMBER;
    V_BCO_DESTINO       NUMBER;
    V_CTA_DESTINO       NUMBER;

  BEGIN
   
  
       
       
             /* RAISE_APPLICATION_ERROR(-20010,
                              I_NRO_ORDEN||'-'||I_CTA_BCO );      */       
  
  
    FOR I IN 1 .. APEX_APPLICATION.G_F01.COUNT LOOP
      FOR C IN CUR1(APEX_APPLICATION.G_F01(I)) LOOP
        INSERT INTO FIN_FINI003_TEMP
          (FINI003_CLAVE_CUO)
        VALUES
          (C.PROV_CODIGO);

        SELECT T.CTA_BCO
          INTO V_BCO_ORIGEN
          FROM FIN_CUENTA_BANCARIA T
         WHERE T.CTA_CODIGO = I_CTA_BCO
           AND T.CTA_EMPR = P_EMPRESA;
        BEGIN
          SELECT B.BCO_CODIGO, T.BCO_CTA_PROVEEDOR A
            INTO V_BCO_DESTINO, V_CTA_DESTINO
            FROM FIN_CTA_BCO_PROV T, FIN_BANCO B
           WHERE T.BCO_DEFAULT = 'S'
             AND B.BCO_CODIGO = T.BCO_CLAVE_BNCO
             AND B.BCO_EMPR = T.BCO_EMPR
             AND T.BCO_CLAVE_PROV = C.PROV_CODIGO
             AND T.BCO_EMPR = C.DOC_EMPR
             AND T.BCO_MON = C.DOC_MON
             AND ROWNUM = 1;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            V_BCO_DESTINO := NULL;
            V_CTA_DESTINO := NULL;
        END;
        -----*****

        INSERT INTO FIN_ORDEN_PAGO_DET
          (FOP_NRO,
           FOP_PROVEEDOR,
           FOP_CLAVE_DOC,
           FOP_SALDO,
           FOP_MONEDA,
           FOP_IMP_RETENCION,
           FOP_IMP_TOTAL,
           FOP_IMP_IVA,
           FOP_NRO_FACTURA,
           FOP_FEC_EMISION,
           FOP_FEC_VTO,
           FOP_IMP_PAGO,
           FOP_EMPR,
           FOP_CTA_BANCARIA,
           FOP_BCO_PROV,
           FOP_PROV_CTA,
           FOP_BCO_ORIGEN)
        VALUES
          (I_NRO_ORDEN,
           C.PROV_CODIGO,
           C.CUO_CLAVE_DOC,
           C.CUO_SALDO_MON,
           C.DOC_MON,
           0,
           C.CUO_IMP_MON,
           C.IVA_MON,
           C.DOC_NRO_DOC,
           C.DOC_FEC_DOC,
           C.CUO_FEC_VTO,
           0,
           C.DOC_EMPR,
           I_CTA_BCO,
           V_BCO_DESTINO,
           V_CTA_DESTINO,
           V_BCO_ORIGEN);

      END LOOP;
    END LOOP;

    --FOR I IN 1 .. APEX_APPLICATION.G_F01.COUNT LOOP
    FOR I IN PROVEEDORES LOOP

      FOR C IN CUR2(I.COD_PROVEEDOR) LOOP

        FINI003.PP_VERIFICAR_NOTACRED(P_EMPRESA     => C.DOC_EMPR,
                                      P_PROVEEDOR   => C.DOC_PROV,
                                      P_CLAVE_DOC   => C.CUO_CLAVE_DOC,
                                      P_EXENTO_LOC  => V_NCR_EXENTO_LOC,
                                      P_EXENTO_MON  => V_NCR_EXENTO_MON,
                                      P_GRAVADO_LOC => V_NCR_GRAVADO_LOC,
                                      P_GRAVADO_MON => V_NCR_GRAVADO_MON,
                                      P_IVA_LOC     => V_NCR_IVA_LOC,
                                      P_IVA_MON     => V_NCR_IVA_MON);

        SELECT DOC_CLAVE_RETENCION,
               SUM(NVL(DOC_NETO_GRAV_MON, 0)) TOTAL_MON,
               SUM(NVL(DOC_NETO_GRAV_LOC, 0)) TOTAL_LOC
          INTO V_FACTURA_RETENIDA, V_TOTAL_FACTURA_MON, V_TOTAL_FACTURA_LOC
          FROM FIN_DOCUMENTO
         WHERE DOC_CLAVE = C.CUO_CLAVE_DOC
           AND DOC_EMPR = C.DOC_EMPR
         GROUP BY DOC_CLAVE_RETENCION;

        V_TOTAL_FACTURA_LOC := NVL(V_TOTAL_FACTURA_LOC, 0) -
                               NVL(V_NCR_GRAVADO_LOC, 0);
        V_TOTAL_FACTURA_MON := NVL(V_TOTAL_FACTURA_MON, 0) -
                               NVL(V_NCR_GRAVADO_MON, 0);

        SELECT NVL(COUNT(*), 0)
          INTO V_EXISTE_RET
          FROM FIN_RETENCION
         WHERE RET_DOC_CLAVE = C.CUO_CLAVE_DOC
           AND RET_EMPR = C.DOC_EMPR;

        V_RETENCION := NULL;

        IF V_EXISTE_RET = 0 THEN

          SELECT SUM(NVL(FD.DOC_NETO_GRAV_LOC, 0)) GRAVADO_LOC
            INTO V_SUM_GRAB_ACUM_LOC
            FROM FIN_DOCUMENTO FD, FIN_CUOTA FC, FIN_ORDEN_PAGO_DET D
           WHERE FD.DOC_EMPR = FC.CUO_EMPR
             AND FC.CUO_CLAVE_DOC = FD.DOC_CLAVE
             AND FD.DOC_CLAVE = D.FOP_CLAVE_DOC
             AND FC.CUO_FEC_VTO = D.FOP_FEC_VTO
             AND TO_CHAR(FD.DOC_FEC_DOC, 'MM/YYYY') =
                 TO_CHAR(C.DOC_FEC_DOC, 'MM/YYYY')
             AND FD.DOC_EMPR = P_EMPRESA
             AND FC.CUO_CLAVE_DOC <> C.CUO_CLAVE_DOC
             AND D.FOP_PROVEEDOR = I.COD_PROVEEDOR
             AND D.FOP_NRO = I_NRO_ORDEN
             AND D.FOP_EMPR = FD.DOC_EMPR;

          IF FIN_RETENER_IVA_FINI003(P_SUM_GRAB_ACUMULADO => V_SUM_GRAB_ACUM_LOC,
                                     P_MONTO_ACTUAL       => V_TOTAL_FACTURA_LOC,
                                     P_SUMA               => 'N',
                                     P_PROVEEDOR          => C.DOC_PROV,
                                     P_MES_ANHO           => TO_CHAR(C.DOC_FEC_DOC,
                                                                     'MM/YYYY'),
                                     P_DOC_CLAVE          => C.CUO_CLAVE_DOC,
                                     P_PROGRAMA           => 'FINM027',
                                     P_EMPRESA            => C.DOC_EMPR) = 'S' THEN

            FINI003.PP_MONTO_RETENCION(I_EMPRESA       => P_EMPRESA,
                                       I_CLAVE_DOC     => C.CUO_CLAVE_DOC,
                                       O_MONTO_RET_MON => V_RETENCION_MON,
                                       O_MONTO_RET_LOC => V_RETENCION_LOC);

            IF C.DOC_MON = 2 THEN
              V_RETENCION := ROUND(V_RETENCION_MON, 2); --MONEDA EXTRANJERA ES REDONDEADA
            ELSE
              V_RETENCION := ROUND(V_RETENCION_LOC, 0);
            END IF;
          END IF;

        END IF;

        V_IMPORTE_PAGO := (C.CUO_SALDO_MON - NVL(V_RETENCION, 0));

        UPDATE FIN_ORDEN_PAGO_DET A
           SET A.FOP_IMP_PAGO      = V_IMPORTE_PAGO,
               A.FOP_IMP_RETENCION = NVL(V_RETENCION, 0),
               A.FOP_SALDO         = C.CUO_SALDO_MON
         WHERE A.FOP_NRO = C.FOP_NRO
           AND A.FOP_PROVEEDOR = C.DOC_PROV
           AND A.FOP_CLAVE_DOC = C.CUO_CLAVE_DOC
           AND A.FOP_FEC_VTO = C.CUO_FEC_VTO;

      END LOOP;
    END LOOP;

  END PP_GUARDAR_AGENDA;

  PROCEDURE PP_SACAR_AGENDA(P_EMPRESA IN NUMBER) IS
  BEGIN
    FOR I IN 1 .. APEX_APPLICATION.G_F02.COUNT LOOP
      DELETE FROM FIN_ORDEN_PAGO_DET D
       WHERE D.ROWID = APEX_APPLICATION.G_F02(I)
         AND D.FOP_EMPR = P_EMPRESA;
    END LOOP;

  END PP_SACAR_AGENDA;

  PROCEDURE PP_PREPARAR_TXT(I_EMPRESA   IN NUMBER,
                            I_NRO_ORDEN IN NUMBER,
                            I_CTA_BCO   IN NUMBER) IS
                               
    CURSOR CUR_DETALLE(I_PROV_COD NUMBER) IS
      SELECT D.FOP_NRO,
             D.FOP_NRO_FACTURA,
             D.FOP_FEC_EMISION,
             D.FOP_FEC_VTO,
             D.FOP_PROVEEDOR,
             NVL(BP.BCO_RUCC_PROV,P.PROV_RUC) PROV_RUC ,
             P.PROV_EMAIL,
             D.FOP_MONEDA,
             NVL(BP.BCO_PROV_RAZON_SOCIAL,P.PROV_RAZON_SOCIAL)PROV_RAZON_SOCIAL ,
             M.MON_SIMBOLO,
             M.MON_DESC,
             D.FOP_IMP_TOTAL,
             D.FOP_IMP_IVA,
             LENGTH(FP_GEN_EXTRAER_NRO(NVL(BP.BCO_RUCC_PROV,P.PROV_RUC))) RUC_LENGTH,
             D.FOP_IMP_RETENCION,
             D.FOP_SALDO,
             D.FOP_IMP_PAGO,
             D.FOP_BCO_ORIGEN BANCO_ORIGEN,
             D.FOP_CTA_BANCARIA || ' -  ' || CT.CTA_DESC CTA_ORIGEN,
             D.FOP_EMPR,
             D.FOP_BCO_PROV BANCO_DESTINO,
             B.BCO_COD_BBVA,
             D.FOP_PROV_CTA CTA_DESTINO,
             BE.BCO_COD_TRASNF COD_TRANSF,
             B.bco_atla_swift
        FROM FIN_ORDEN_PAGO_DET  D,
             FIN_ORDEN_PAGO_CAB  A,
             FIN_CTA_BCO_PROV    BP,
             FIN_CUENTA_BANCARIA CT,
             FIN_BANCO           BE,
             FIN_BANCO           B,
             FIN_PROVEEDOR       P,
             GEN_MONEDA          M
       WHERE A.FOP_NRO = D.FOP_NRO
         AND A.FOP_EMPR = D.FOP_EMPR

         AND D.FOP_MONEDA = M.MON_CODIGO
         AND D.FOP_EMPR = M.MON_EMPR

         AND D.FOP_PROVEEDOR = P.PROV_CODIGO
         AND D.FOP_EMPR = P.PROV_EMPR

         AND D.FOP_EMPR = CT.CTA_EMPR(+)
         AND D.FOP_CTA_BANCARIA = CT.CTA_CODIGO(+)

         AND BP.BCO_MON(+) = D.FOP_MONEDA
         AND BP.BCO_EMPR(+) = D.FOP_EMPR
         AND BP.BCO_CLAVE_PROV(+) = D.FOP_PROVEEDOR
         AND BP.BCO_CLAVE_BNCO(+) = D.FOP_BCO_PROV
         AND BP.BCO_CTA_PROVEEDOR(+) = D.FOP_PROV_CTA

         AND B.BCO_EMPR(+) = D.FOP_EMPR
         AND B.BCO_CODIGO(+) = D.FOP_BCO_PROV

         AND CT.CTA_EMPR = BE.BCO_EMPR(+)
         AND CT.CTA_BCO = BE.BCO_CODIGO(+)

         AND D.FOP_PROVEEDOR = I_PROV_COD
         AND A.FOP_EMPR = I_EMPRESA
         AND A.FOP_NRO = I_NRO_ORDEN
         AND D.FOP_CTA_BANCARIA = I_CTA_BCO;

    CURSOR CUR_PROV IS
      SELECT DISTINCT D.FOP_PROVEEDOR
        FROM FIN_ORDEN_PAGO_DET D
       WHERE D.FOP_NRO = I_NRO_ORDEN
         AND D.FOP_CTA_BANCARIA = I_CTA_BCO
         AND D.FOP_EMPR = I_EMPRESA;

    V_MON_DESC       VARCHAR2(5); -- DESCRIBCION ABREVIADA USA BBVA
    V_MON_COD        NUMBER; -- CODIGO DE MONEDA USA ITAU
    V_PAGO_MONTO     VARCHAR2(20);
    V_PROV_NOMBRE    VARCHAR2(60);
    V_PROV_RUC       VARCHAR2(20);
    V_DOC_NRO1       VARCHAR2(20);
    V_DOC_NRO2       VARCHAR2(300);
    V_CUENTAPROV     VARCHAR2(20);
    V_TIPO_DOC       VARCHAR2(2);
    V_T_TRANS        VARCHAR2(2); --TIPO DE TRANSFERENCIA SIPAP O BBVA.. SE USA PARA BBVA
    V_COD_SIPAP      VARCHAR2(10); -- CODIGO QUE ASIGNA BBVA A LOS OTROS BANCOS PARA TRANS. SIPAP
    V_CTA_EMPR       VARCHAR2(40);
    V_PROV_MAIL      VARCHAR2(51);
    V_FECHA_VTO      VARCHAR2(10); --ITAU PARA PAGAR EN UNA FECHA ESPECIFICA
    V_ORDEN          NUMBER; -- NRO DE ORDEN USA BBVA
    V_FECHA          VARCHAR2(10) := TO_CHAR(SYSDATE, 'HHMISS'); --FECHA QUE SE CARGO LA TRANSFERENCIA SIN SEPARADORES
    V_HORA           VARCHAR2(10) := TO_CHAR(SYSDATE, 'DDMMYYYY'); --HORA QUE SE CARGO LA TRANSFERENCIA
    V_TIPO_MOV       NUMBER := 2; --USA ITAU
    V_PAGO_TOTAL     NUMBER; --MONTO TOTAL PARA ITAU
    V_PRIORIDAD      VARCHAR2(1); --PARA TRANS. BBVA A SIPAP SE CARGA 'A' SI NO VACIO
    V_CONCEPTO       VARCHAR2(50);
    V_CTA_INTERNO    VARCHAR2(20);
    V_COD_TRANS      VARCHAR2(10) := 10258; -- CODIGO TRANSFERENCIA
    V_CTA_DEST_SIPAP VARCHAR2(35);
    V_B_PAG_DESC     VARCHAR2(50);
    V_MON_P_SIMB     VARCHAR2(50);
    V_TIP_DOC       VARCHAR2(10);--USA ATLAS
    V_TIPO_PAGO      VARCHAR2(10);--USA ATLAS
    V_COD_BANCO      VARCHAR2(23237);--USA ATLAS
    V_CUENTA_DESTINO  VARCHAR2(200);--USA ATLAS
    
    C             CUR_DETALLE%ROWTYPE;
    I             CUR_PROV%ROWTYPE;
    L_BLOB        BLOB;
    L_TEXT        CLOB;
    L_DEST_OFFSET PLS_INTEGER := 1;
    L_SRC_OFFSET  PLS_INTEGER := 1;
    L_LANG_CTX    PLS_INTEGER := DBMS_LOB.DEFAULT_LANG_CTX;
    L_BLOB_WARN   PLS_INTEGER := DBMS_LOB.WARN_INCONVERTIBLE_CHAR;
    
    -- banco regional
    l_reg_gs   fin_conf_archivo_banco.regional_gs%type;
    l_reg_usd  fin_conf_archivo_banco.regional_usd%type;
    l_reg_cont number;
  BEGIN
    PP_VALIDAR_CUENTA_DESTINO(I_EMPRESA   => I_EMPRESA,
                              I_NRO_ORDEN => I_NRO_ORDEN,
                              I_CTA_BCO   => I_CTA_BCO);

    IF FP_ORDEN_ABIERTO(I_EMPRESA => I_EMPRESA, I_NRO_ORDEN => I_NRO_ORDEN) = 'N' THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'Primero debe cerrar la orden para generar el archivo');
    END IF;

    V_CONCEPTO := 'PAGO A PROVEEDOR';
    
    <<ctas_bco_config>>
    begin
      select c.regional_gs,
             c.regional_usd
      into   l_reg_gs,
             l_reg_usd 
      from fin_conf_archivo_banco c
      where c.empresa_id = I_EMPRESA;
    exception
      when no_data_found then
        l_reg_gs  := null;
        l_reg_usd := null;
    end ctas_bco_config;
    
    if I_CTA_BCO in (l_reg_gs, l_reg_usd) then
      l_reg_cont := 0;
      
      for i in(SELECT D.FOP_PROVEEDOR
               FROM FIN_ORDEN_PAGO_DET D
               WHERE D.FOP_NRO = I_NRO_ORDEN
               AND D.FOP_CTA_BANCARIA = I_CTA_BCO
               AND D.FOP_EMPR = I_EMPRESA
               group by D.FOP_PROVEEDOR
               )
      loop         
         for j in (select
                   D.FOP_NRO_FACTURA ||';'||
                   'C;'||
                   D.FOP_PROV_CTA ||';'||
                   TO_CHAR(D.FOP_IMP_PAGO)||'.00;'||
                   NVL(BP.BCO_PROV_RAZON_SOCIAL,P.PROV_RAZON_SOCIAL) ||';'||
                   case when m.mon_codigo = 1 then 'GS' else 'USD' end||';'||
                   B.bco_atla_swift||';'||
                   '6;'||
                   NVL(BP.BCO_RUCC_PROV,P.PROV_RUC)||';'||
                   '61;'||
                   p.prov_dir||';;;'||
                   P.PROV_EMAIL||chr(10)
                   X
        FROM FIN_ORDEN_PAGO_DET  D,
             FIN_ORDEN_PAGO_CAB  A,
             FIN_CTA_BCO_PROV    BP,
             FIN_CUENTA_BANCARIA CT,
             FIN_BANCO           BE,
             FIN_BANCO           B,
             FIN_PROVEEDOR       P,
             GEN_MONEDA          M
       WHERE A.FOP_NRO = D.FOP_NRO
       AND A.FOP_EMPR = D.FOP_EMPR

       AND D.FOP_MONEDA = M.MON_CODIGO
       AND D.FOP_EMPR = M.MON_EMPR

       AND D.FOP_PROVEEDOR = P.PROV_CODIGO
       AND D.FOP_EMPR = P.PROV_EMPR

       AND D.FOP_EMPR = CT.CTA_EMPR(+)
       AND D.FOP_CTA_BANCARIA = CT.CTA_CODIGO(+)

       AND BP.BCO_MON(+) = D.FOP_MONEDA
       AND BP.BCO_EMPR(+) = D.FOP_EMPR
       AND BP.BCO_CLAVE_PROV(+) = D.FOP_PROVEEDOR
       AND BP.BCO_CLAVE_BNCO(+) = D.FOP_BCO_PROV
       AND BP.BCO_CTA_PROVEEDOR(+) = D.FOP_PROV_CTA

       AND B.BCO_EMPR(+) = D.FOP_EMPR
       AND B.BCO_CODIGO(+) = D.FOP_BCO_PROV

       AND CT.CTA_EMPR = BE.BCO_EMPR(+)
       AND CT.CTA_BCO = BE.BCO_CODIGO(+)

       AND D.FOP_PROVEEDOR = i.fop_proveedor
       AND A.FOP_EMPR = I_EMPRESA
       AND A.FOP_NRO = I_NRO_ORDEN
       AND D.FOP_CTA_BANCARIA = I_CTA_BCO
       )
       loop
         l_reg_cont := l_reg_cont + 1;
         
         L_TEXT := L_TEXT|| l_reg_cont ||';'|| j.x;
       end loop;
      end loop f_cur_prov;
    end if;

    IF I_CTA_BCO IN (22, 23) THEN
      ----------------------***********************************************************************************************************
      ------ TXT PARA BBVA
      FOR I IN CUR_PROV LOOP

        OPEN CUR_DETALLE(I.FOP_PROVEEDOR);
        LOOP
          FETCH CUR_DETALLE
            INTO C;
          IF CUR_DETALLE%NOTFOUND OR MOD(CUR_DETALLE%ROWCOUNT, 21) = 0 THEN
            L_TEXT           := L_TEXT || 'C' -- 1 SIEMPRE 'C'
                                || RPAD('01', 2, CHR(32)) -- 2 SIEMPRE '01'(PAGO A PROVEEDORES)
                                || RPAD(V_COD_TRANS, 5, CHR(32)) -- 3 CODIGO DE EMPRESA ASIGNADO POR BBVA
                                || RPAD(CHR(32), 15, CHR(32)) -- 4 EN BLANCO. LO TOMA DE LO PARAMETRIZADO EN BBVA
                                || LPAD(NVL(V_COD_SIPAP, 0), 5, 0) -- 5 COD. BANCO RECEPTOR(PAGO SIPAP)/00000(PAGO BBVA) *VER BANCOS SIPAP
                                || RPAD(V_CTA_INTERNO, 15, CHR(32)) -- 6 NRO.CTA.CREDITO(PAGO BBVA)/EN BLANCO(PAGO SIPAP)
                                || V_T_TRANS -- 7 'I' (INTERBANCARIA SIPAP)/ 'C' (PAGO BBVA)
                                || RPAD(NVL(V_PROV_NOMBRE, CHR(32)),
                                        60,
                                        CHR(32)) -- 8 NOMBRE DEL BENEF. DEL PAGO

                                || LPAD(V_MON_DESC, 3, CHR(32)) -- 9 CODIGO DE LA MONEDA DE PAGO (GS/USD)
                                || LPAD(REPLACE(LTRIM(TO_CHAR(V_PAGO_MONTO,
                                                              '999999999999990D00',
                                                              'NLS_NUMERIC_CHARACTERS = '',.''')),
                                                ',',
                                                NULL),
                                        18,
                                        0) -- 10 IMPORTE A PAGAR(ULTIMOS DOS LUGARES PARA DECIMALES EN CASO DE USD)
                                || LPAD(CHR(32), 18, CHR(32)) -- 11 SIEMPRE EN BLANCO
                                || RPAD(V_PROV_RUC, 20, CHR(32)) -- 12 NRO. DOCUMENTO BENEFICIARIO(RUC/CI)(PAGO SIPAP)/EN BLANCO (PAGO BBVA)
                                || 'C' -- 13 SIEMPRE 'C'
                                ||
                                RPAD(NVL(V_DOC_NRO1, CHR(32)), 20, CHR(32)) -- 14 SI SE PAGA MAS DE UNA FACTURA, SE INGRESA LA PRIMERA AQUI Y LAS DEMAS EN REF_FACTURAS
                                || RPAD(V_TIPO_DOC, 3, CHR(32)) -- 15 TIPO DE DOCUMENTO BENEFICIARIO(PAGO SIPAP)(1- CI/6- RUC)*VER TIPOS DE DOCUMENTOS
                                || LPAD(CHR(32), 8, CHR(32)) -- 16 SIEMPRE EN BLANCO
                                || LPAD(CHR(32), 8, CHR(32)) -- 17 NRO. DE CONTRATO DE CAMBIO PRE-PACTADO BBVA(SINO EXISTE EN BLANCO)(PAGO SIPAP)/EN BLANCO(PAGO BBVA)
                                || LPAD(V_ORDEN, 10, CHR(32)) -- 18 NRO.IDENTIFICADOR(ENTRADA/SALIDA) PARA SEGUIMIENTO EMPRESA.
                                || RPAD(V_CONCEPTO, 35, CHR(32)) -- 19 CONCEPTO DE PAGO(PAGO SIPAP)/ EN BLANCO(PAGO BBVA)
                                || RPAD(NVL(V_CTA_DEST_SIPAP, CHR(32)),
                                        35,
                                        CHR(32)) -- 20 NRO.CTA.CREDITO DEL BENEFICIARIO(PAGO SIPAP)/NULO(PAGO BBVA)
                                || LPAD(CHR(32), 5, CHR(32)) -- 21 SIEMPRE EN BLANCO
                                || LPAD(CHR(32), 5, CHR(32)) -- 22 SIEMPRE EN BLANCO
                                || LPAD(V_PRIORIDAD, 1, CHR(32)) -- 23 ( 'A' ALTA-ON LINE/'B' BAJA- EN BATCH (PAGO SIPAP))/ EN BLANCO (PAGOS BBVA)
                                || LPAD(CHR(32), 8, CHR(32)) -- 24 SIEMPRE EN BLANCO
                                || LPAD(CHR(32), 6, CHR(32)) -- 25 SIEMPRE EN BLANCO
                                || LPAD(CHR(32), 30, CHR(32)) -- 26 SIEMPRE EN BLANCO
                                ||
                                RPAD(SUBSTR(NVL(V_DOC_NRO2,
                                                RPAD(CHR(32), 301, CHR(32))),
                                            2),
                                     300,
                                     CHR(32)) -- 27 REFERENCIAS DE FACTURAS EN CASO DE VARIAS FACTURAS A PAGAR
                                ||
                                RPAD(NVL(V_PROV_MAIL, CHR(32)), 51, CHR(32)) -- 28 DIRECCION DE CORREO PARA ENVIO DE CONFIRMACION DE PAGO A PROVEEDORES
                                || CHR(13) || CHR(10);
            V_MON_DESC       := NULL;
            V_PAGO_MONTO     := NULL;
            V_PROV_MAIL      := NULL;
            V_PROV_NOMBRE    := NULL;
            V_PROV_RUC       := NULL;
            V_DOC_NRO1       := NULL;
            V_DOC_NRO2       := NULL;
            V_CTA_DEST_SIPAP := NULL;
            V_TIPO_DOC       := NULL;
            V_T_TRANS        := NULL;
            V_COD_SIPAP      := NULL;
            V_PRIORIDAD      := NULL;
            EXIT WHEN CUR_DETALLE%NOTFOUND;
          END IF;
          V_COD_TRANS   := C.COD_TRANSF; ---DADO POR EL BANCO A LA EMPRESA
          V_PROV_NOMBRE := C.PROV_RAZON_SOCIAL;
          V_PAGO_MONTO  := NVL(V_PAGO_MONTO, 0) + C.FOP_IMP_PAGO;
          V_PROV_MAIL   := C.PROV_EMAIL;
          V_ORDEN       := C.FOP_NRO;
          V_PROV_RUC    := C.PROV_RUC;
          
          IF C.RUC_LENGTH > 7 THEN
            V_TIPO_DOC := 6; --SI TIENE RUC
          ELSE
            V_TIPO_DOC := 1; --SI ES CI
          END IF;

          IF C.BANCO_ORIGEN = C.BANCO_DESTINO THEN
            V_T_TRANS        := 'C'; --- 'C' (PAGO BBVA)
            V_COD_SIPAP      := '00000';
            V_CTA_DEST_SIPAP := CHR(32); -- NRO.CTA.CREDITO DEL BENEFICIARIO(PAGO SIPAP)/NULO(PAGO BBVA)
            V_CTA_INTERNO    := C.CTA_DESTINO;
            V_PRIORIDAD      := CHR(32); --- EN BLANCO (PAGOS BBVA)
          ELSE
            V_T_TRANS        := 'I'; -- 'I' INTERBANCARIA SIPAP)/
            V_COD_SIPAP      := C.BCO_COD_BBVA;
            V_CTA_DEST_SIPAP := C.CTA_DESTINO;
            V_CTA_INTERNO    := CHR(32);
            V_PRIORIDAD      := 'A'; --( 'A' ALTA-ON LINE/'B' BAJA- EN BATCH (PAGO SIPAP))/
          END IF;

          IF V_DOC_NRO1 IS NULL THEN
            V_DOC_NRO1 := C.FOP_NRO_FACTURA;
          ELSE
            V_DOC_NRO2 := V_DOC_NRO2 || ',' || C.FOP_NRO_FACTURA;
          END IF;
          IF C.FOP_MONEDA = 1 THEN
            V_MON_DESC := 'GS';
          ELSIF C.FOP_MONEDA = 2 THEN
            V_MON_DESC := 'USD';
          END IF;
        END LOOP;
        CLOSE CUR_DETALLE;
      END LOOP;
    END IF;

    -- DBMS_OUTPUT.PUT_LINE(L_TEXT);

    IF I_CTA_BCO IN (26, 27) THEN

      ----------------------***********************************************************************************************************
      ------ TXT PARA ITAU

      OPEN CUR_PROV;
      LOOP
        FETCH CUR_PROV
          INTO I;

        IF CUR_PROV%NOTFOUND THEN
          L_TEXT := L_TEXT || 'C' --TIPO DE REGISTRO  D? REGISTRO DE DETALLE ?C? REGISTRO DE CONTROL
                    || LPAD(2, 2, 0) --TIPO DE TRANFERENCIA?01? PAG SALARIOS?02? PAG PROVEEDORES?03? COBRO FACTURA/CUOTA
                    || LPAD(59, 3, 0) --CODIGO DE EMPRESA   (ASIGNADO POR ELBANCO)
                    || LPAD(V_CTA_EMPR, 10, 0) --NRO DE CUENTA PARA DEBITO/CUENTA EMPRESA
                    || LPAD(17, 3, 0) --NRO. DE BANCO PARA CREDITO OBS: SIEMPRE 017
                    || LPAD(0, 10, 0) -- NRO. DE CUENTA PARA CREDITO OBS: SI PAGO EN CHEQUE RELLENO CON CEROS
                    || 'C' --TIPO DEBITO/CREDITO ?D? DEBITO?C? CREDITO?H? CHEQUE?F? COBRO DE FACTURA/CUOTA
                    || RPAD(CHR(32), 50) --CHEQUE A LA ORDEN DE/CLIENTE FACTURADO/BENEFICIARIO
                    || '1' --MONEDA CORRESPONDIENTE AL MONTO0 GUARANIES1 DOLARES
                    || LPAD(REPLACE(LTRIM(TO_CHAR(V_PAGO_TOTAL,
                                                  '999999999999990D00',
                                                  'NLS_NUMERIC_CHARACTERS = '',.''')),
                                    ',',
                                    NULL),
                            15,
                            0) --MONTO TRANSFERENCIA/MONTO FACTURA,CUOTAOBS: ULTIMOS DOS DIGITOS CORRESPONDE ADECIMALES.
                    || LPAD(0, 15, 0) --MONTO TRANSFERENCIA (SEGUNDOVENCIMIENTO)OBS: ULTIMOS DOS DIGITOS CORRESPONDE ADECIMALES.SOLO PARA F?COBRO DEFACTURA/CUOTA.DEMAS 0
                    || RPAD(CHR(32), 12) --NRO. DE DOCUMENTOOBS: CEDULA DE IDENTIDAD, RUC,PASAPORTE, OTROS. DEL BENEFICIARIO,PROVEEDOR, CLIENTE.
                    || '0' --TIPO FACTURA1 FACTURA CONTADO2 FACTURA CREDITOSOLO PARA PAGO A PROVEEDORES. DEMAS0
                    || RPAD(CHR(32), 20) --NRO. DE FACTURAOBS:PARA PAGO A PROVEEDORES.DEMASBLANCOS
                    || LPAD(0, 3, 0) --NRO. DE CUOTA PAGADA/A COBRAR.SOLO PARA ?F? COBRO DE FACTURA/CUOTA
                    || LPAD(0, 8, 0) -- FECHA PARA REALIZAR EL CREDITO/FECHAVENCIMIENTO-
                    || LPAD(0, 8, 0) --FECHA SEGUNDO VENCIMIENTO.SOLO PARA ? F? COBRO DE FACTURA/CUOTA
                    || RPAD(CHR(32), 50) --COMENTARIO DE CONCEPTO COBRADO/PAGADO
                    || RPAD(CHR(32), 15) --REFERENCIA OPERACION EMPRESA
                    || LPAD(0, 8, 0) --FECHA DE CARGA DE TRANSACCION
                    || LPAD(0, 6, 0) -- HORA DE CARGA DE TRANSACCION
                    || RPAD(CHR(32), 10) --NOMBRE DEL USUARIO QUE CARGO
                    || RPAD(CHR(32), 100) --NRO. DE FACTURAS ADICIONALES,OBS:PARA PAGO A PROVEEDORES, TIPODEBITO/CREDITO = ?H? CHEQUE. DEMASBLANCOS.
                    || RPAD(CHR(32), 15) --NRO. DOCUMENTO DEL COBRADOR.OBS:PARA PAGO A PROVEEDORES, .DEMASBLANCOS
                    || RPAD(CHR(32), 50) --NOMBRE DEL COBRADOR.OBS:PARA PAGO A PROVEEDORES, TIPODEBITO/CREDITO = ?H? CHEQUE. DEMASBLANCOS
                    || RPAD(CHR(32), 15) --NRO. DOCUMENTO DEL COBRADOR.OBS:PARA PAGO A PROVEEDORES, TIPODEBITO/CREDITO = ?H? CHEQUE. DEMASBLANCOS
                    || RPAD(CHR(32), 50) --NOMBRE DEL COBRADOR.BS:PARA PAGO A PROVEEDORES, TIPODEBITO/CREDITO = ?H? CHEQUE. DEMASBLANCOS
                    || RPAD(CHR(32), 100) || CHR(13) || CHR(10); --COMENTARIO ADICIONAL DEL CLIENTE
          EXIT WHEN CUR_PROV%NOTFOUND;
        END IF;

        OPEN CUR_DETALLE(I.FOP_PROVEEDOR);
        LOOP
          FETCH CUR_DETALLE
            INTO C;
          IF CUR_DETALLE%NOTFOUND OR CUR_DETALLE%ROWCOUNT = 10 THEN
            L_TEXT := L_TEXT || 'D' --TIPO DE REGISTRO  D? REGISTRO DE DETALLE ?C? REGISTRO DE CONTROL
                      || LPAD(2, 2, 0) --TIPO DE TRANFERENCIA?01? PAG SALARIOS?02? PAG PROVEEDORES?03? COBRO FACTURA/CUOTA
                      || LPAD(V_COD_TRANS, 3, 0) --CODIGO DE EMPRESA   (ASIGNADO POR ELBANCO)
                      || LPAD(V_CTA_EMPR, 10, 0) --NRO DE CUENTA PARA DEBITO/CUENTA EMPRESA
                      || LPAD(17, 3, 0) --NRO. DE BANCO PARA CREDITO OBS: SIEMPRE 017
                      || LPAD(V_CUENTAPROV, 10, 0) -- NRO. DE CUENTA PARA CREDITO OBS: SI PAGO EN CHEQUE RELLENO CON CEROS
                      || 'C' --TIPO DEBITO/CREDITO ?D? DEBITO?C? CREDITO?H? CHEQUE?F? COBRO DE FACTURA/CUOTA
                      || RPAD(NVL(V_PROV_NOMBRE, ' '), 50) --CHEQUE A LA ORDEN DE/CLIENTE FACTURADO/BENEFICIARIO
                      || V_MON_COD --MONEDA CORRESPONDIENTE AL MONTO0 GUARANIES1 DOLARES
                      || LPAD(REPLACE(LTRIM(TO_CHAR(V_PAGO_MONTO,
                                                    '999999999999990D00',
                                                    'NLS_NUMERIC_CHARACTERS = '',.''')),
                                      ',',
                                      NULL),
                              15,
                              0) --MONTO TRANSFERENCIA/MONTO FACTURA,CUOTAOBS: ULTIMOS DOS DIGITOS CORRESPONDE ADECIMALES.
                      || LPAD('0', 15, 0) --MONTO TRANSFERENCIA (SEGUNDOVENCIMIENTO)OBS: ULTIMOS DOS DIGITOS CORRESPONDE ADECIMALES.SOLO PARA F?COBRO DEFACTURA/CUOTA.DEMAS 0
                      || RPAD(NVL(V_PROV_RUC, CHR(32)), 12) --NRO. DE DOCUMENTOOBS: CEDULA DE IDENTIDAD, RUC,PASAPORTE, OTROS. DEL BENEFICIARIO,PROVEEDOR, CLIENTE.
                      || V_TIPO_MOV --TIPO FACTURA1 FACTURA CONTADO2 FACTURA CREDITOSOLO PARA PAGO A PROVEEDORES. DEMAS0
                      || RPAD(NVL(V_DOC_NRO1, CHR(32)), 20) --NRO. DE FACTURAOBS:PARA PAGO A PROVEEDORES.DEMASBLANCOS
                      || LPAD('0', 3, 0) --NRO. DE CUOTA PAGADA/A COBRAR.SOLO PARA ?F? COBRO DE FACTURA/CUOTA
                      || V_FECHA_VTO -- FECHA PARA REALIZAR EL CREDITO/FECHAVENCIMIENTO-
                      || LPAD(0, 8, 0) --FECHA SEGUNDO VENCIMIENTO.SOLO PARA ? F? COBRO DE FACTURA/CUOTA
                      || RPAD('PAGO A PROVEEDORES', 50) --COMENTARIO DE CONCEPTO COBRADO/PAGADO
                      || RPAD(CHR(32), 15) --REFERENCIA OPERACION EMPRESA
                      || V_FECHA --FECHA DE CARGA DE TRANSACCION
                      || V_HORA -- HORA DE CARGA DE TRANSACCION
                      || RPAD(NVL(GEN_DEVUELVE_USER, USER), 10) --NOMBRE DEL USUARIO QUE CARGO
                      || LPAD(SUBSTR(NVL(V_DOC_NRO2, CHR(32)), 2), 100, 0) --NRO. DE FACTURAS ADICIONALES,OBS:PARA PAGO A PROVEEDORES, TIPODEBITO/CREDITO = ?H? CHEQUE. DEMASBLANCOS.
                      || LPAD(CHR(32), 15, 0) --NRO. DOCUMENTO DEL COBRADOR.OBS:PARA PAGO A PROVEEDORES, .DEMASBLANCOS
                      || RPAD(CHR(32), 50) --NOMBRE DEL COBRADOR.OBS:PARA PAGO A PROVEEDORES, TIPODEBITO/CREDITO = ?H? CHEQUE. DEMASBLANCOS
                      || LPAD(CHR(32), 15, 0) --NRO. DOCUMENTO DEL COBRADOR.OBS:PARA PAGO A PROVEEDORES, TIPODEBITO/CREDITO = ?H? CHEQUE. DEMASBLANCOS
                      || RPAD(CHR(32), 50) --NOMBRE DEL COBRADOR.BS:PARA PAGO A PROVEEDORES, TIPODEBITO/CREDITO = ?H? CHEQUE. DEMASBLANCOS
                      || RPAD(CHR(32), 100) || CHR(13) || CHR(10); --COMENTARIO ADICIONAL DEL CLIENTE

            V_CUENTAPROV  := NULL;
            V_MON_COD     := NULL;
            V_PAGO_MONTO  := NULL;
            V_PROV_NOMBRE := NULL;
            V_PROV_RUC    := NULL;
            V_DOC_NRO1    := NULL;
            V_DOC_NRO2    := NULL;
            V_FECHA_VTO   := NULL;
            EXIT WHEN CUR_DETALLE%NOTFOUND;
          END IF;

          V_COD_TRANS   := C.COD_TRANSF; ---DADO POR EL BANCO A LA EMPRESA
          V_MON_COD     := C.FOP_MONEDA - 1;
          V_PROV_NOMBRE := C.PROV_RAZON_SOCIAL;
          V_FECHA_VTO   := TO_CHAR(SYSDATE, 'YYYYMMDD');
          V_PROV_RUC    := C.PROV_RUC;
          V_PAGO_MONTO  := NVL(V_PAGO_MONTO, 0) + C.FOP_IMP_PAGO;
          V_PAGO_TOTAL  := NVL(V_PAGO_TOTAL, 0) + C.FOP_IMP_PAGO;
          V_CTA_EMPR    := C.CTA_DESTINO;
          V_CUENTAPROV  := C.CTA_DESTINO;
          IF V_DOC_NRO1 IS NULL THEN
            V_DOC_NRO1 := C.FOP_NRO_FACTURA;
          ELSE
            V_DOC_NRO2 := V_DOC_NRO2 || ',' || C.FOP_NRO_FACTURA;
          END IF;

        END LOOP;
        CLOSE CUR_DETALLE;
      END LOOP;
      CLOSE CUR_PROV;

    ELSE
      NULL;
    END IF;
    
      IF I_CTA_BCO IN (35, 96) THEN

      ----------------------***********************************************************************************************************
      ------ TXT PARA ATLAS

        FOR I IN CUR_PROV LOOP
          
        OPEN CUR_DETALLE(I.FOP_PROVEEDOR);
        LOOP
          FETCH CUR_DETALLE
            INTO C;
            
             IF CUR_DETALLE%NOTFOUND OR CUR_DETALLE%ROWCOUNT = 10 THEN
            -- RAISE_APPLICATION_ERROR(-20001,V_FECHA_VTO);
            L_TEXT := L_TEXT ||V_TIPO_PAGO  --FORMA DE PAGO
                      ||RPAD(V_CUENTAPROV,15) --NRO DE CUENTA ATLAS QUE RECIBIRA EL CREDITO
                      ||TO_CHAR(SYSDATE, 'DDMMYYYY') --FECHA DE CARGA DE LA TRANSACCION
                      ||RPAD(V_PROV_NOMBRE, 50) --NOMBRE DEL BENEFICIARIO
                      ||RPAD('PAGO A PROVEEDORES', 60) --CONCEPTO DE PAGO
                      ||RPAD(V_PAGO_TOTAL,18) --MONTOS A PAGAR OBS: ULTIMOS DOS DIGITOS CORRESPONDE ADECIMALES.
                      || RPAD(C.FOP_NRO, 10) -- NRO DE ORDEN DE PAGO
                      || RPAD(NVL(V_DOC_NRO1, CHR(32)), 30) --NRO DE FACTURA
                      || RPAD(CHR(32), 30) --NOMBRE AUTORIZADO A RETIRAR EL CHEQUE
                      || RPAD(CHR(32), 15) --NRO DE DOCUMENTO DEL  AUTORIZADO A RETIRAR EL CHEQUE
                      || RPAD(CHR(32), 3) --NRO DE SUCURSAL DONDE SE EMITIRA EL CHEQUE
                      || RPAD(V_CUENTA_DESTINO, 35) --NRO DE CUENTA QUE RECIBIRA LA TRANFERENCIA "SIPAP"
                      || RPAD(C.BCO_ATLA_SWIFT, 20) --CODIGO SWINF DEL BANCO BENEFICIARIO
                      || RPAD( V_TIP_DOC, 3)  --NRO DEL DOCUMENTO DEL VENEFICIARIO EJEMPLO:1-C.I,2-RUC,3-CRP,9- CRC-EMPRESA SIN RUC O NO REMITENTE,10-SIPAP-DOC REMITENTE
                      || RPAD(V_PROV_RUC, 20)-- NRO DE DOCUMENTO DE IDENTIDAD DEL BENEFICIARIO
                      || RPAD( '', 100)--OBSERVACION DEL PAGO
                      || CHR(13) || CHR(10); 

            V_CUENTAPROV  := NULL;
            V_MON_COD     := NULL;
            V_PAGO_MONTO  := NULL;
            V_PROV_NOMBRE := NULL;
            V_PROV_RUC    := NULL;
            V_DOC_NRO1    := NULL;
            V_DOC_NRO2    := NULL;
            V_FECHA_VTO   := NULL;
            V_TIPO_PAGO   := NULL;
            V_CUENTA_DESTINO:=NULL;
            V_TIP_DOC :=NULL;
            
           EXIT WHEN CUR_DETALLE%NOTFOUND;
          END IF;
             
        BEGIN
          SELECT WM_CONCAT(D.BCO_CODIGO) 
          INTO V_COD_BANCO
          FROM FIN_BANCO d
         WHERE UPPER(d.bco_desc) LIKE '%ATLAS%'
          AND D.BCO_EMPR=I_EMPRESA;
          END;
          
       IF C.BANCO_DESTINO  IN  (V_COD_BANCO) THEN
           V_TIPO_PAGO:='C';
           V_CUENTAPROV  := C.CTA_DESTINO;
           ELSE
             V_TIPO_PAGO:='T';  
             V_PROV_RUC    := C.PROV_RUC;
             V_CUENTAPROV:='        ';
             V_CUENTA_DESTINO:=C.CTA_DESTINO;
             V_TIP_DOC:=2;
          END IF;
        
      
       
        
        
          V_COD_TRANS   := C.COD_TRANSF; ---DADO POR EL BANCO A LA EMPRESA
          V_MON_COD     := C.FOP_MONEDA - 1;
          V_PROV_NOMBRE := C.PROV_RAZON_SOCIAL;
          V_FECHA_VTO   := TO_CHAR(SYSDATE, 'DDMMYYYY');
         
         
           IF C.FOP_MONEDA=1 THEN
               V_PAGO_MONTO  := ROUND(NVL(V_PAGO_MONTO, 0) + C.FOP_IMP_PAGO);
               V_PAGO_TOTAL  :=V_PAGO_MONTO;
           ELSIF  C.FOP_MONEDA=2 THEN
              
                V_PAGO_MONTO  := NVL(V_PAGO_MONTO, 0) + C.FOP_IMP_PAGO;
                
                V_PAGO_TOTAL  := REPLACE(LTRIM(TO_CHAR(V_PAGO_MONTO,
                                                    '999999999999990D00',
                                                    'NLS_NUMERIC_CHARACTERS = '',.''')),
                                      ',',
                                      NULL);
             
            END IF;
          V_CTA_EMPR    := C.CTA_DESTINO;
         
          IF  V_DOC_NRO1 IS NULL THEN
              V_DOC_NRO1 := C.FOP_NRO_FACTURA;
          ELSE
            V_DOC_NRO2 := V_DOC_NRO2 || ',' || C.FOP_NRO_FACTURA;
          END IF;

        END LOOP;
        CLOSE CUR_DETALLE;
      END LOOP;
    END IF;

    -- DBMS_OUTPUT.PUT_LINE(L_TEXT);

    SELECT T.BCO_DESC, M.MON_SIMBOLO
      INTO V_B_PAG_DESC, V_MON_P_SIMB
      FROM FIN_BANCO T, FIN_CUENTA_BANCARIA B, GEN_MONEDA M
     WHERE T.BCO_CODIGO = B.CTA_BCO
       AND T.BCO_EMPR = B.CTA_EMPR
       AND B.CTA_MON = M.MON_CODIGO
       AND B.CTA_EMPR = M.MON_EMPR
       AND B.CTA_EMPR = I_EMPRESA
       AND B.CTA_CODIGO = I_CTA_BCO;

    IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => 'FINM027_DESCARGAR_ARCHIVO') THEN
      APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => 'FINM027_DESCARGAR_ARCHIVO');
    END IF;

    APEX_COLLECTION.CREATE_COLLECTION(P_COLLECTION_NAME => 'FINM027_DESCARGAR_ARCHIVO');

    IF L_TEXT IS NOT NULL THEN

      DBMS_LOB.CREATETEMPORARY(L_BLOB, TRUE);

      DBMS_LOB.CONVERTTOBLOB(L_BLOB,
                             L_TEXT,
                             DBMS_LOB.LOBMAXSIZE,
                             L_DEST_OFFSET,
                             L_SRC_OFFSET,
                             DBMS_LOB.DEFAULT_CSID,
                             L_LANG_CTX,
                             L_BLOB_WARN);

      APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'FINM027_DESCARGAR_ARCHIVO',
                                 P_C001            => SUBSTR(V_CONCEPTO,
                                                             1,
                                                             3) || '-' ||
                                                      V_MON_P_SIMB ||
                                                      I_NRO_ORDEN || '-' ||
                                                      V_B_PAG_DESC,
                                 P_C002            => V('APP_USER'),
                                 P_C003            => '.txt',
                                 P_C004            => '',
                                 P_BLOB001         => L_BLOB);

    ELSE
      RAISE_APPLICATION_ERROR(-20010,
                              'No se ha encontrado registros para exportar');
    END IF;

  END PP_PREPARAR_TXT;

  PROCEDURE PP_VALIDAR_CUENTA_DESTINO(I_EMPRESA   IN NUMBER,
                                      I_NRO_ORDEN IN NUMBER,
                                      I_CTA_BCO   IN NUMBER DEFAULT NULL) IS
    V_COUNT NUMBER;
  BEGIN
    SELECT COUNT(*)
      INTO V_COUNT
      FROM FIN_ORDEN_PAGO_DET T
     WHERE T.FOP_NRO = I_NRO_ORDEN
       AND T.FOP_PROV_CTA IS NULL
       AND (T.FOP_CTA_BANCARIA = I_CTA_BCO OR I_CTA_BCO IS NULL)
       AND T.FOP_EMPR = I_EMPRESA;
    IF V_COUNT > 0 THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'No se puede realizar la operacion.Hay documentos que no tienen cta destino');
    END IF;
  END PP_VALIDAR_CUENTA_DESTINO;

  FUNCTION FP_ORDEN_ABIERTO(I_EMPRESA IN NUMBER, I_NRO_ORDEN IN NUMBER)
    RETURN VARCHAR2 AS
    V_ORDEN NUMBER;
  BEGIN
    IF I_NRO_ORDEN IS NOT NULL THEN
      SELECT FOP_NRO A
        INTO V_ORDEN
        FROM FIN_ORDEN_PAGO_CAB T
       WHERE T.FOP_NRO = I_NRO_ORDEN
         AND T.FOP_EMPR = I_EMPRESA
         AND T.FOP_BLOCK_ORDEN = 'S';
      RETURN 'Y';
    ELSE
      RETURN NULL;
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 'N';
  END FP_ORDEN_ABIERTO;

  PROCEDURE PP_CERRAR_ORDEN(I_EMPRESA IN NUMBER, I_NRO_ORDEN IN NUMBER) AS
  BEGIN
    PP_VALIDAR_CUENTA_DESTINO(I_EMPRESA   => I_EMPRESA,
                              I_NRO_ORDEN => I_NRO_ORDEN);

    UPDATE FIN_ORDEN_PAGO_CAB
       SET FOP_BLOCK_ORDEN = 'S'
     WHERE FOP_NRO = I_NRO_ORDEN
       AND FOP_EMPR = I_EMPRESA;

  END PP_CERRAR_ORDEN;

  PROCEDURE PP_REALIZAR_TRANSFERENCIA(I_EMPRESA   IN NUMBER,
                                      I_NRO_ORDEN IN NUMBER) IS

    CURSOR CUR1 IS
      SELECT T.FOP_EMPR DOC_EMPR,
             T.FOP_CTA_BANCARIA DOC_CTA_BCO,
             T.FOP_MONEDA DOC_MON,
             SUM(T.FOP_IMP_PAGO) DOC_NETO_EXEN_MON,
             T.FOP_NRO DOC_NRO_DOC,
             T.FOP_PROVEEDOR,
             TRUNC(C.FOP_FECHA) DOC_FEC_OPER,
             TRUNC(C.FOP_FECHA) DOC_FEC_DOC
        FROM FIN_ORDEN_PAGO_DET T, FIN_ORDEN_PAGO_CAB C, FIN_CUOTA C
       WHERE T.FOP_EMPR = I_EMPRESA
         AND T.FOP_NRO = I_NRO_ORDEN
         AND T.FOP_NRO = C.FOP_NRO
         AND T.FOP_CLAVE_DOC = C.CUO_CLAVE_DOC
         AND T.FOP_FEC_VTO = C.CUO_FEC_VTO
         AND C.CUO_SALDO_MON > 0
         AND T.FOP_EMPR = C.FOP_EMPR
       GROUP BY T.FOP_PROVEEDOR,
                T.FOP_MONEDA,
                T.FOP_NRO,
                T.FOP_CTA_BANCARIA,
                C.FOP_FECHA,
                T.FOP_EMPR;

    V_DOC_NETO_EXEN_LOC NUMBER;
    V_CLAVE_DEP         NUMBER;
    V_CLAVE_EXT         NUMBER;
    V_CUENTA            NUMBER;
    W_CTA_CONT          NUMBER;
    V_PROV              FIN_PROVEEDOR%ROWTYPE;
    V_TASA              NUMBER;
    V_PROGRAMA          VARCHAR2(15) := 'FINM027';

  BEGIN

    PP_VALIDAR_CUENTA_DESTINO(I_EMPRESA   => I_EMPRESA,
                              I_NRO_ORDEN => I_NRO_ORDEN);

    IF FP_ORDEN_ABIERTO(I_EMPRESA => I_EMPRESA, I_NRO_ORDEN => I_NRO_ORDEN) = 'N' THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'Primero debe cerrar la orden para realizar la transferencia');
    END IF;

    IF FP_ORDEN_TRANSFERIDO(I_EMPRESA   => I_EMPRESA,
                            I_NRO_ORDEN => I_NRO_ORDEN) THEN

      RAISE_APPLICATION_ERROR(-20010,
                              'Esta orden ya ha sido transferido a la caja.');

    END IF;

    FOR C IN CUR1 LOOP

      -----------**********************    DEPOSITO

      V_CLAVE_DEP := FIN_SEQ_DOC_NEXTVAL;

      SELECT *
        INTO V_PROV
        FROM FIN_PROVEEDOR T
       WHERE T.PROV_CODIGO = C.FOP_PROVEEDOR
         AND T.PROV_EMPR = I_EMPRESA;

      IF C.DOC_MON = 1 THEN
        V_CUENTA := 94;
      ELSE
        V_CUENTA := 95;
      END IF;

      V_TASA              := FIN_BUSCAR_COTIZACION_FEC(FEC  => C.DOC_FEC_OPER,
                                                       MON  => C.DOC_MON,
                                                       EMPR => C.DOC_EMPR);
      V_DOC_NETO_EXEN_LOC := C.DOC_NETO_EXEN_MON * V_TASA;

      INSERT INTO FIN_DOCUMENTO
        (DOC_CLAVE,
         DOC_CTA_BCO,
         DOC_EMPR,
         DOC_TIPO_MOV,
         DOC_TIPO_SALDO,
         DOC_NRO_DOC,
         DOC_SUC,
         DOC_MON,
         DOC_FEC_OPER,
         DOC_FEC_DOC,
         DOC_BRUTO_GRAV_LOC,
         DOC_BRUTO_GRAV_MON,
         DOC_BRUTO_EXEN_LOC,
         DOC_BRUTO_EXEN_MON,
         DOC_NETO_GRAV_LOC,
         DOC_NETO_GRAV_MON,
         DOC_NETO_EXEN_LOC,
         DOC_NETO_EXEN_MON,
         DOC_IVA_LOC,
         DOC_IVA_MON,
         DOC_OBS,
         DOC_LOGIN,
         DOC_FEC_GRAB,
         DOC_SIST,
         DOC_TASA,
         DOC_SISTEMA_AUX)
      VALUES
        (V_CLAVE_DEP,
         V_CUENTA,
         C.DOC_EMPR,
         12,
         'D',
         C.DOC_NRO_DOC,
         1,
         C.DOC_MON,
         C.DOC_FEC_OPER,
         C.DOC_FEC_DOC,
         0,
         0,
         V_DOC_NETO_EXEN_LOC,
         C.DOC_NETO_EXEN_MON,
         0,
         0,
         V_DOC_NETO_EXEN_LOC,
         C.DOC_NETO_EXEN_MON,
         0,
         0,
         V_PROV.PROV_RAZON_SOCIAL,
         GEN_DEVUELVE_USER,
         SYSDATE,
         'FIN',
         V_TASA,
         V_PROGRAMA);

      -- INSERTAR UN CONCEPTO EN FIN_DOC_CONCEPTO QUE HAGA REFERENCIA AL DOCUMENTO
      -- DE DEPOSITO. EN EL MISMO REGISTRO DEJAR EN BLANCO LA CUENTA CONTABLE,
      -- PARA NO HACER UN CONTRA-ASIENTO.

      --CLAVE CONCEPTO 3 DEPOSITO D
      INSERT INTO FIN_DOC_CONCEPTO
        (DCON_CLAVE_DOC,
         DCON_ITEM,
         DCON_CLAVE_CONCEPTO,
         DCON_CLAVE_CTACO,
         DCON_TIPO_SALDO,
         DCON_EXEN_LOC,
         DCON_EXEN_MON,
         DCON_GRAV_LOC,
         DCON_GRAV_MON,
         DCON_IVA_LOC,
         DCON_IVA_MON,
         DCON_EMPR)
      VALUES
        (V_CLAVE_DEP,
         1,
         3,
         NULL,
         'D',
         V_DOC_NETO_EXEN_LOC,
         C.DOC_NETO_EXEN_MON,
         0,
         0,
         0,
         0,
         C.DOC_EMPR);

      -------------***************** EXTRACCION

      V_CLAVE_EXT := FIN_SEQ_DOC_NEXTVAL;

      INSERT INTO FIN_DOCUMENTO
        (DOC_CLAVE,
         DOC_CTA_BCO,
         DOC_EMPR,
         DOC_TIPO_MOV,
         DOC_TIPO_SALDO,
         DOC_NRO_DOC,
         DOC_SUC,
         DOC_MON,
         DOC_FEC_OPER,
         DOC_FEC_DOC,
         DOC_BRUTO_GRAV_LOC,
         DOC_BRUTO_GRAV_MON,
         DOC_BRUTO_EXEN_LOC,
         DOC_BRUTO_EXEN_MON,
         DOC_NETO_GRAV_LOC,
         DOC_NETO_GRAV_MON,
         DOC_NETO_EXEN_LOC,
         DOC_NETO_EXEN_MON,
         DOC_IVA_LOC,
         DOC_IVA_MON,
         DOC_OBS,
         DOC_LOGIN,
         DOC_FEC_GRAB,
         DOC_SIST,
         DOC_CLAVE_PADRE,
         DOC_TASA,
         DOC_SISTEMA_AUX)
      VALUES
        (V_CLAVE_EXT,
         C.DOC_CTA_BCO,
         C.DOC_EMPR,
         13,
         'C',
         C.DOC_NRO_DOC,
         1,
         C.DOC_MON,
         C.DOC_FEC_OPER,
         C.DOC_FEC_DOC,
         0,
         0,
         V_DOC_NETO_EXEN_LOC,
         C.DOC_NETO_EXEN_MON,
         0,
         0,
         V_DOC_NETO_EXEN_LOC,
         C.DOC_NETO_EXEN_MON,
         0,
         0,
         V_PROV.PROV_RAZON_SOCIAL,
         GEN_DEVUELVE_USER,
         SYSDATE,
         'FIN',
         V_CLAVE_DEP,
         V_TASA,
         V_PROGRAMA);

      --------------------------------***************  CONCEPTO

      -- INSERTAR UN CONCEPTO EN FIN_DOC_CONCEPTO QUE HAGA REFERENCIA AL DOCUMENTO
      -- DE EXTRACCION. EN EL MISMO REGISTRO PONER LA CUENTA CONTABLE CORRESP.
      -- A LA CUENTA BANCARIA EN DONDE SE DEPOSITA;

      SELECT CTA_CLAVE_CTACO
        INTO W_CTA_CONT
        FROM FIN_CUENTA_BANCARIA
       WHERE CTA_EMPR = C.DOC_EMPR
         AND CTA_CODIGO = V_CUENTA;

      --
      --CLAVE CONCEPTO  5 EXTRACCION C

      INSERT INTO FIN_DOC_CONCEPTO
        (DCON_CLAVE_DOC,
         DCON_ITEM,
         DCON_CLAVE_CONCEPTO,
         DCON_CLAVE_CTACO,
         DCON_TIPO_SALDO,
         DCON_EXEN_LOC,
         DCON_EXEN_MON,
         DCON_GRAV_LOC,
         DCON_GRAV_MON,
         DCON_IVA_LOC,
         DCON_IVA_MON,
         DCON_EMPR)
      VALUES
        (V_CLAVE_EXT,
         1,
         5,
         W_CTA_CONT,
         'C',
         V_DOC_NETO_EXEN_LOC,
         C.DOC_NETO_EXEN_MON,
         0,
         0,
         0,
         0,
         C.DOC_EMPR);

    END LOOP;

    PP_GENERAR_RECIBO(I_EMPRESA => I_EMPRESA, I_NRO_ORDEN => I_NRO_ORDEN);
  END PP_REALIZAR_TRANSFERENCIA;

  PROCEDURE PP_VAL_PROV_CTA(I_EMPRESA IN NUMBER) AS
  BEGIN
    NULL;
    /*  SELECT C.BCO_CODIGO, REGEXP_REPLACE(T.CTA_NRO, '[^0-9]') A
        INTO V_BCO_DESTINO, V_CTA_DESTINO
        FROM FIN_CLI_CTA_BANC T, FIN_BANCO C
       WHERE T.CTA_BCO = C.BCO_CODIGO
         AND T.CTA_EMPR = C.BCO_EMPR
         AND T.CTA_EMPR = C.DOC_EMPR
         AND T.CTA_CLI = C.PROV_CODIGO
         AND T.CTA_MON = C.DOC_MON
         AND T.CTA_PREDET = 'S'
         AND ROWNUM = 1;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_BCO_DESTINO := NULL;
        V_CTA_DESTINO := NULL;*/
  END PP_VAL_PROV_CTA;

  PROCEDURE PP_GENERAR_RECIBO(I_EMPRESA IN NUMBER, I_NRO_ORDEN IN NUMBER) AS
    CURSOR CUR_PROV IS
      SELECT T.FOP_NRO,
             T.FOP_PROVEEDOR,
             T.FOP_MONEDA,
             SUM(T.FOP_SALDO) FOP_SALDO,
             T.FOP_CTA_BANCARIA,
             C.FOP_FECHA
        FROM FIN_ORDEN_PAGO_DET T, FIN_ORDEN_PAGO_CAB C, FIN_CUOTA C
       WHERE T.FOP_EMPR = I_EMPRESA
         AND T.FOP_NRO = I_NRO_ORDEN
         AND T.FOP_NRO = C.FOP_NRO
         AND T.FOP_CLAVE_DOC = C.CUO_CLAVE_DOC
         AND T.FOP_FEC_VTO = C.CUO_FEC_VTO
         AND C.CUO_SALDO_MON > 0
         AND T.FOP_EMPR = C.FOP_EMPR
       GROUP BY T.FOP_PROVEEDOR,
                T.FOP_MONEDA,
                T.FOP_NRO,
                T.FOP_CTA_BANCARIA,
                C.FOP_FECHA;

    CURSOR CUR_DET(I_PROV    IN NUMBER,
                   I_CTA_BCO IN NUMBER,
                   I_MONEDA  IN NUMBER) IS
      SELECT T.FOP_NRO,
             T.FOP_PROVEEDOR,
             T.FOP_CLAVE_DOC,
             T.FOP_SALDO,
             T.FOP_MONEDA,
             T.FOP_IMP_RETENCION,
             T.FOP_IMP_AUT,
             T.FOP_IMP_IVA,
             T.FOP_NRO_FACTURA,
             T.FOP_FEC_EMISION,
             T.FOP_FEC_VTO,
             T.FOP_IMP_PAGO,
             T.FOP_CTA_BANCARIA,
             T.FOP_EMPR,
             T.FOP_BCO_PROV,
             T.FOP_PROV_CTA,
             T.FOP_BCO_ORIGEN,
             T.FOP_CLAVE_DOC_CONC_DEP,
             T.FOP_CLAVE_DOC_CONC_EXP,
             T.FOP_CLAVE_PLAPA,
             F.*
        FROM FIN_ORDEN_PAGO_DET T, FIN_DOCUMENTO F, FIN_CUOTA C
       WHERE T.FOP_EMPR = I_EMPRESA

         AND F.DOC_CLAVE = C.CUO_CLAVE_DOC
         AND T.FOP_FEC_VTO = C.CUO_FEC_VTO

         AND T.FOP_NRO = I_NRO_ORDEN
         AND T.FOP_PROVEEDOR = I_PROV
         AND T.FOP_CLAVE_DOC = F.DOC_CLAVE
         AND T.FOP_EMPR = F.DOC_EMPR
         AND T.FOP_CTA_BANCARIA = I_CTA_BCO
         AND T.FOP_MONEDA = I_MONEDA
         AND C.CUO_SALDO_LOC > 0;

    V_DCON_CLAVE_CONCEPTO NUMBER;
    V_DCON_CLAVE_CTACO    NUMBER;
    V_DCON_TIPO_SALDO     VARCHAR2(5);
    V_DOC                 FIN_DOCUMENTO%ROWTYPE;
    V_PROV                FIN_PROVEEDOR%ROWTYPE;
    V_PROGRAMA            VARCHAR2(15) := 'FINM027';
    V_IMPR                GEN_IMPRESORA%ROWTYPE;
    V_CONFIG              FIN_CONFIGURACION%ROWTYPE;
    V_MON                 GEN_MONEDA%ROWTYPE;
    V_CUENTA              NUMBER;

  BEGIN
    BEGIN
      SELECT * --T.IMP_NRO_TIMBRADO_RET, T.IMPR_IP,IMP_ULT_RETENCION
        INTO V_IMPR
        FROM GEN_IMPRESORA T
       WHERE IMPR_IP = OWA_UTIL.GET_CGI_ENV('REMOTE_ADDR') --
            --IMP_CODIGO = 38
         AND IMP_EMPR = I_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'Falta configurar ip en impresora para calcular numero tesaka');
    END;
    SELECT *
      INTO V_CONFIG
      FROM FIN_CONFIGURACION FC
     WHERE FC.CONF_EMPR = I_EMPRESA;

    FOR C IN CUR_PROV LOOP

      -------------------------------------
      SELECT FCON_CLAVE, FCON_CLAVE_CTACO, FCON_TIPO_SALDO
        INTO V_DCON_CLAVE_CONCEPTO, V_DCON_CLAVE_CTACO, V_DCON_TIPO_SALDO
        FROM FIN_CONCEPTO
       WHERE FCON_CLAVE = V_CONFIG.CONF_CONCEPTO_PAGO
         AND FCON_EMPR = I_EMPRESA;

      SELECT *
        INTO V_MON
        FROM GEN_MONEDA M
       WHERE M.MON_CODIGO = C.FOP_MONEDA
         AND M.MON_EMPR = I_EMPRESA;

      SELECT *
        INTO V_PROV
        FROM FIN_PROVEEDOR T
       WHERE T.PROV_CODIGO = C.FOP_PROVEEDOR
         AND T.PROV_EMPR = I_EMPRESA;
      --******** ASIGNACION DE VARIABLES

      V_DOC.DOC_TIPO_MOV := V_CONFIG.CONF_RECIBO_PAGO_REC; --*** RECIBO DE PAGO RECIBIDO

      V_DOC.DOC_CLAVE := FIN_SEQ_DOC_NEXTVAL;

      IF C.FOP_MONEDA = 1 THEN
        V_CUENTA := 94;
      ELSE
        V_CUENTA := 95;
      END IF;

      V_DOC.DOC_FEC_OPER      := TRUNC(C.FOP_FECHA);
      V_DOC.DOC_FEC_DOC       := TRUNC(C.FOP_FECHA);
      V_DOC.DOC_MON           := C.FOP_MONEDA;
      V_DOC.DOC_CTA_BCO       := V_CUENTA;
      V_DOC.DOC_NRO_DOC       := C.FOP_NRO;
      V_DOC.DOC_SUC           := 1;
      V_DOC.DOC_TASA          := FIN_BUSCAR_COTIZACION_FEC(FEC  => C.FOP_FECHA,
                                                           MON  => C.FOP_MONEDA,
                                                           EMPR => I_EMPRESA);

      V_DOC.DOC_NETO_EXEN_LOC := ROUND(C.FOP_SALDO * V_DOC.DOC_TASA,
                                       V_MON.MON_DEC_IMP);

      V_DOC.DOC_NETO_EXEN_MON := C.FOP_SALDO;

      INSERT INTO FIN_DOCUMENTO
        (DOC_CLAVE,
         DOC_EMPR,
         DOC_SUC,
         DOC_CTA_BCO,
         DOC_TIPO_SALDO,
         DOC_NRO_DOC,
         DOC_MON,
         DOC_TIPO_MOV,
         DOC_PROV,
         DOC_FEC_DOC,
         DOC_FEC_OPER,
         DOC_NETO_EXEN_LOC,
         DOC_NETO_EXEN_MON,
         DOC_BRUTO_EXEN_LOC,
         DOC_BRUTO_EXEN_MON,
         DOC_BRUTO_GRAV_LOC,
         DOC_BRUTO_GRAV_MON,
         DOC_NETO_GRAV_LOC,
         DOC_NETO_GRAV_MON,
         DOC_IVA_LOC,
         DOC_IVA_MON,
         DOC_LOGIN,
         DOC_FEC_GRAB,
         DOC_SIST,
         DOC_CLI_NOM,
         DOC_SISTEMA_AUX,
         DOC_TASA,
         DOC_OBS)
      VALUES
        (V_DOC.DOC_CLAVE,
         I_EMPRESA,
         V_DOC.DOC_SUC,
         V_CUENTA,
         'C',
         V_DOC.DOC_NRO_DOC,
         V_DOC.DOC_MON,
         V_DOC.DOC_TIPO_MOV,
         V_PROV.PROV_CODIGO,
         V_DOC.DOC_FEC_DOC,
         V_DOC.DOC_FEC_OPER,
         V_DOC.DOC_NETO_EXEN_LOC,
         V_DOC.DOC_NETO_EXEN_MON,
         V_DOC.DOC_NETO_EXEN_LOC,
         V_DOC.DOC_NETO_EXEN_MON,
         0,
         0,
         0,
         0,
         0,
         0,
         GEN_DEVUELVE_USER,
         SYSDATE,
         SUBSTR(V_PROGRAMA, 1, 3),
         V_PROV.PROV_RAZON_SOCIAL,
         V_PROGRAMA,
         V_DOC.DOC_TASA,
         UPPER('PAGO A ' || V_PROV.PROV_RAZON_SOCIAL || ' OP=' || C.FOP_NRO));

      INSERT INTO FIN_DOC_CONCEPTO
        (DCON_CLAVE_DOC,
         DCON_ITEM,
         DCON_CLAVE_CONCEPTO,
         DCON_CLAVE_CTACO,
         DCON_TIPO_SALDO,
         DCON_EXEN_LOC,
         DCON_EXEN_MON,
         DCON_GRAV_LOC,
         DCON_GRAV_MON,
         DCON_IVA_LOC,
         DCON_IVA_MON,
         DCON_EMPR)
      VALUES
        (V_DOC.DOC_CLAVE,
         1,
         V_DCON_CLAVE_CONCEPTO,
         V_DCON_CLAVE_CTACO,
         V_DCON_TIPO_SALDO,
         V_DOC.DOC_NETO_EXEN_LOC,
         V_DOC.DOC_NETO_EXEN_MON,
         0,
         0,
         0,
         0,
         I_EMPRESA);

      FOR DET IN CUR_DET(I_PROV    => C.FOP_PROVEEDOR,
                         I_CTA_BCO => C.FOP_CTA_BANCARIA,
                         I_MONEDA  => C.FOP_MONEDA) LOOP
        DECLARE

        BEGIN
          INSERT INTO FIN_PAGO
            (PAG_CLAVE_DOC,
             PAG_FEC_VTO,
             PAG_CLAVE_PAGO,
             PAG_FEC_PAGO,
             PAG_IMP_LOC,
             PAG_IMP_MON,
             PAG_LOGIN,
             PAG_FEC_GRAB,
             PAG_SIST,
             PAG_EMPR)
          VALUES
            (DET.FOP_CLAVE_DOC,
             DET.FOP_FEC_VTO,
             V_DOC.DOC_CLAVE,
             V_DOC.DOC_FEC_DOC,
             ROUND(DET.FOP_SALDO * V_DOC.DOC_TASA, V_MON.MON_DEC_IMP),
             DET.FOP_SALDO,
             GEN_DEVUELVE_USER,
             SYSDATE,
             SUBSTR(V_PROGRAMA, 1, 3),
             I_EMPRESA);
        END;
        DECLARE
          V_BRET              FIN_DOCUMENTO%ROWTYPE;
          V_RETCONC           FIN_DOC_CONCEPTO%ROWTYPE;
          V_IMP_ULT_RETENCION NUMBER;
          V_SUCURSAL          NUMBER := 1;
          SALIR EXCEPTION;
        BEGIN
          IF DET.DOC_CLAVE_RETENCION IS NOT NULL OR
             NVL(DET.FOP_IMP_RETENCION, 0) = 0 THEN
            RAISE SALIR;
          END IF;

          V_BRET.DOC_TIPO_MOV := V_CONFIG.CONF_TMOV_RETENCION; ---RETENCIONES A PAGAR HILAGRO

          SELECT IMP_ULT_RETENCION + 1
            INTO V_IMP_ULT_RETENCION
            FROM GEN_IMPRESORA T
           WHERE T.IMP_CODIGO = V_IMPR.IMP_CODIGO
             AND IMP_EMPR = I_EMPRESA;

          -- ACTUALIZAR DOC_FIN.DOC_TIPO_SALDO
          SELECT TMOV_TIPO
            INTO V_BRET.DOC_TIPO_SALDO
            FROM GEN_TIPO_MOV
           WHERE TMOV_CODIGO = V_BRET.DOC_TIPO_MOV
             AND TMOV_EMPR = I_EMPRESA;

          V_BRET.DOC_CLAVE := FIN_SEQ_DOC_NEXTVAL;

          ---**** VARIABLES

          V_BRET.DOC_FEC_OPER      := TRUNC(C.FOP_FECHA);
          V_BRET.DOC_FEC_DOC       := TRUNC(C.FOP_FECHA);
          V_BRET.DOC_FEC_GRAB      := SYSDATE;
          V_BRET.DOC_MON           := C.FOP_MONEDA;
          V_BRET.DOC_CTA_BCO       := V_CUENTA;
          V_BRET.DOC_NRO_DOC       := V_IMP_ULT_RETENCION;
          V_BRET.DOC_SUC           := 1;
          V_BRET.DOC_CLAVE_PADRE   := V_DOC.DOC_CLAVE;
          V_BRET.DOC_TASA          := V_DOC.DOC_TASA;
          V_BRET.DOC_NETO_EXEN_LOC := ROUND(DET.FOP_IMP_RETENCION *
                                            V_DOC.DOC_TASA,
                                            V_MON.MON_DEC_IMP);
          V_BRET.DOC_NETO_EXEN_MON := ROUND(DET.FOP_IMP_RETENCION,
                                            V_MON.MON_DEC_IMP);

          INSERT INTO FIN_DOCUMENTO
            (DOC_CLAVE,
             DOC_EMPR,
             DOC_SUC,
             DOC_NRO_DOC,
             DOC_TIMBRADO,
             DOC_MON,
             DOC_PROV,
             DOC_FEC_OPER,
             DOC_FEC_DOC,
             DOC_NETO_EXEN_LOC,
             DOC_NETO_EXEN_MON,
             DOC_BRUTO_EXEN_LOC,
             DOC_BRUTO_EXEN_MON,
             DOC_BRUTO_GRAV_LOC,
             DOC_BRUTO_GRAV_MON,
             DOC_NETO_GRAV_LOC,
             DOC_NETO_GRAV_MON,
             DOC_IVA_LOC,
             DOC_IVA_MON,
             DOC_OBS,
             DOC_LOGIN,
             DOC_FEC_GRAB,
             DOC_SIST,
             DOC_CTACO,
             DOC_CLAVE_PADRE,
             DOC_TIPO_MOV,
             DOC_TIPO_SALDO,
             DOC_TASA,
             DOC_CTA_BCO,
             DOC_SISTEMA_AUX)
          VALUES
            (V_BRET.DOC_CLAVE,
             I_EMPRESA,
             V_BRET.DOC_SUC,
             V_BRET.DOC_NRO_DOC,
             V_IMPR.IMP_NRO_TIMBRADO_RET,
             V_BRET.DOC_MON,
             V_PROV.PROV_CODIGO,
             V_BRET.DOC_FEC_OPER,
             V_BRET.DOC_FEC_DOC,
             V_BRET.DOC_NETO_EXEN_LOC,
             V_BRET.DOC_NETO_EXEN_MON,
             V_BRET.DOC_NETO_EXEN_LOC,
             V_BRET.DOC_NETO_EXEN_MON,
             0,
             0,
             0,
             0,
             0,
             0,
             UPPER('RET A PAGAR: ' || V_PROV.PROV_RAZON_SOCIAL),
             GEN_DEVUELVE_USER,
             V_BRET.DOC_FEC_GRAB,
             SUBSTR(V_PROGRAMA, 1, 3),
             NULL,
             V_BRET.DOC_CLAVE_PADRE,
             V_BRET.DOC_TIPO_MOV,
             V_BRET.DOC_TIPO_SALDO,
             V_BRET.DOC_TASA,
             V_BRET.DOC_CTA_BCO,
             V_PROGRAMA);

          V_RETCONC.DCON_CLAVE_DOC := V_BRET.DOC_CLAVE;
          V_RETCONC.DCON_ITEM      := 1;

          IF V_SUCURSAL = 1 THEN
            V_RETCONC.DCON_CLAVE_CONCEPTO := V_CONFIG.CONF_CONC_RETEN_CENTRAL;
          ELSIF V_SUCURSAL = 2 THEN
            V_RETCONC.DCON_CLAVE_CONCEPTO := V_CONFIG.CONF_CONC_RETEN_ASUNCION;
          ELSIF V_SUCURSAL = 3 THEN
            V_RETCONC.DCON_CLAVE_CONCEPTO := V_CONFIG.CONF_CONC_RETEN_CONCEPCION;
          ELSIF V_SUCURSAL = 4 THEN
            V_RETCONC.DCON_CLAVE_CONCEPTO := V_CONFIG.CONF_CONC_RETEN_LOMA;
          ELSIF V_SUCURSAL = 6 THEN
            V_RETCONC.DCON_CLAVE_CONCEPTO := V_CONFIG.CONF_CONC_RETEN_SANTAROSA;
          END IF;

          DECLARE
          BEGIN
            SELECT FCON_CLAVE, FCON_CLAVE_CTACO
              INTO V_RETCONC.DCON_CLAVE_CONCEPTO,
                   V_RETCONC.DCON_CLAVE_CTACO
              FROM FIN_CONCEPTO
             WHERE FCON_CLAVE = V_RETCONC.DCON_CLAVE_CONCEPTO
               AND FCON_EMPR = I_EMPRESA;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              NULL;
          END;

          V_RETCONC.DCON_EMPR       := I_EMPRESA;
          V_RETCONC.DCON_TIPO_SALDO := 'D';
          V_RETCONC.DCON_EXEN_LOC   := V_BRET.DOC_NETO_EXEN_LOC;
          V_RETCONC.DCON_EXEN_MON   := V_BRET.DOC_NETO_EXEN_MON;
          V_RETCONC.DCON_GRAV_LOC   := 0;
          V_RETCONC.DCON_GRAV_MON   := 0;
          V_RETCONC.DCON_IVA_LOC    := 0;
          V_RETCONC.DCON_IVA_MON    := 0;
          V_RETCONC.DCON_OBS        := 'Ret. a Pagar:  ' ||
                                       V_PROV.PROV_RAZON_SOCIAL;
          --***
          INSERT INTO FIN_DOC_CONCEPTO
            (DCON_CLAVE_DOC,
             DCON_ITEM,
             DCON_CLAVE_CONCEPTO,
             DCON_CLAVE_CTACO,
             DCON_EMPR,
             DCON_TIPO_SALDO,
             DCON_EXEN_LOC,
             DCON_EXEN_MON,
             DCON_GRAV_LOC,
             DCON_GRAV_MON,
             DCON_IVA_LOC,
             DCON_IVA_MON,
             DCON_OBS)
          VALUES
            (V_BRET.DOC_CLAVE,
             V_RETCONC.DCON_ITEM,
             V_RETCONC.DCON_CLAVE_CONCEPTO,
             V_RETCONC.DCON_CLAVE_CTACO,
             V_RETCONC.DCON_EMPR,
             V_RETCONC.DCON_TIPO_SALDO,
             V_RETCONC.DCON_EXEN_LOC,
             V_RETCONC.DCON_EXEN_MON,
             V_RETCONC.DCON_GRAV_LOC,
             V_RETCONC.DCON_GRAV_MON,
             V_RETCONC.DCON_IVA_LOC,
             V_RETCONC.DCON_IVA_MON,
             V_RETCONC.DCON_OBS);

          DECLARE
          BEGIN
            UPDATE GEN_IMPRESORA
               SET IMP_ULT_RETENCION = V_IMP_ULT_RETENCION
             WHERE IMP_CODIGO = V_IMPR.IMP_CODIGO
               AND IMP_EMPR = I_EMPRESA;

            COMMIT;
          EXCEPTION
            WHEN OTHERS THEN
              NULL;
          END;

          UPDATE FIN_DOCUMENTO
             SET DOC_CLAVE_RETENCION = V_BRET.DOC_CLAVE
           WHERE DOC_CLAVE = DET.FOP_CLAVE_DOC
             AND DOC_EMPR = I_EMPRESA;

          UPDATE FIN_CUOTA
             SET CUO_CLAVE_RETENCION = V_BRET.DOC_CLAVE
           WHERE CUO_CLAVE_DOC = DET.FOP_CLAVE_DOC
             AND CUO_EMPR = I_EMPRESA;

          ------------------------------------------------------------------------------------------------------------------------
          --GRABAR REGISTRO PARA ENVIAR A LA SET A TRAVES DEL TESAKA.
          ------------------------------------------------------------------------------------------------------------------------
          IF V_PROV.PROV_RUC_DV = '99999901' THEN
            --VALIDAR RUC PARA NO DOMICILIADOS.
            V_PROV.PROV_RUC_DV := 0;
          END IF;

          INSERT INTO FIN_RETENCION
            (RET_DOC_NRO,
             RET_DOC_FEC_DOC,
             RET_CONTRIBUYENTE,
             RET_TIPO_ID_CONTRIBUYENTE,
             RET_ID_CONTRIBUYENTE,
             RET_TOTAL,
             RET_ESTADO,
             RET_USER_EMIT,
             RET_FEC_CONF,
             RET_USER_CONF,
             RET_DOC_CLAVE,
             RET_RET_CLAVE,
             RET_FECHA,
             RET_TIPO,
             RET_PORCENTAJE,
             RET_EMPR)
          VALUES
            (DET.DOC_NRO_DOC,
             TO_DATE(V_BRET.DOC_FEC_DOC, 'DD/MM/YYYY'),
             V_PROV.PROV_RAZON_SOCIAL,
             'RUC',
             V_PROV.PROV_RUC_DV,
             V_BRET.DOC_NETO_EXEN_LOC,
             'P',
             GEN_DEVUELVE_USER,
             NULL,
             NULL,
             DET.FOP_CLAVE_DOC,
             V_BRET.DOC_CLAVE,
             V_BRET.DOC_FEC_DOC,
             'I',
             NULL,
             I_EMPRESA);

        EXCEPTION
          WHEN SALIR THEN
            NULL;
        END;
      END LOOP;
      COMMIT;
    END LOOP;

  END PP_GENERAR_RECIBO;

  FUNCTION FP_ORDEN_TRANSFERIDO(I_EMPRESA IN NUMBER, I_NRO_ORDEN IN NUMBER)
    RETURN BOOLEAN AS
    V_CANT     NUMBER;
    V_PROGRAMA VARCHAR2(10) := 'FINM027';
  BEGIN

    SELECT COUNT(*) CANT
      INTO V_CANT
      FROM FIN_DOCUMENTO T
     WHERE DOC_TIPO_MOV = 13
       AND T.DOC_SISTEMA_AUX = V_PROGRAMA
       AND (DOC_NRO_DOC, T.DOC_FEC_DOC, DOC_EMPR) IN
           (SELECT O.FOP_NRO, O.FOP_FECHA, O.FOP_EMPR
              FROM FIN_ORDEN_PAGO_CAB O
             WHERE FOP_NRO = I_NRO_ORDEN
               AND FOP_EMPR = I_EMPRESA);

    IF V_CANT > 0 THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;

  END FP_ORDEN_TRANSFERIDO;
  
PROCEDURE PP_VERIF_INTERES(BSEL_S_HOLDING      IN NUMBER,
                             BSEL_S_MON          IN NUMBER,
                             BSEL_S_PORC_INTERES OUT NUMBER,
                             P_EMPRESA           IN NUMBER) IS

  BEGIN

    SELECT HOL_PORC_INTERES_MOROSIDAD
      INTO BSEL_S_PORC_INTERES
      FROM FIN_FICHA_HOLDING
     WHERE HOL_CODIGO = BSEL_S_HOLDING
       AND HOL_EMPR = P_EMPRESA;

    IF BSEL_S_PORC_INTERES IS NULL AND BSEL_S_MON <> 0 THEN
      ----LV
      SELECT NVL(M.MON_PORC_INTERES_MOROSIDAD, 0)
        INTO BSEL_S_PORC_INTERES
        FROM GEN_MONEDA M
       WHERE M.MON_CODIGO = BSEL_S_MON
         AND M.MON_EMPR = P_EMPRESA;

      /* IF BSEL_S_PORC_INTERES = 0 THEN
        RAISE_APPLICATION_ERROR(-20005,
                                'El porcentaje de interes por morosidad esta definido a cero!');
      END IF;*/

    ELSE
      SELECT NVL(M.MON_PORC_INTERES_MOROSIDAD, 0) ----LV
        INTO BSEL_S_PORC_INTERES
        FROM GEN_MONEDA M
       WHERE M.MON_CODIGO = 1 -- BSEL_S_MON
         AND M.MON_EMPR = P_EMPRESA;
    END IF;
  END;




  PROCEDURE PP_CARGAR_DETALLE(I_HOLDING      IN NUMBER,
                              I_SALDO_CERO   IN VARCHAR2,
                              I_MONEDA       IN NUMBER,
                              I_HISTORICO    IN VARCHAR2,
                              I_FECHA_VTO    IN DATE,
                              I_PORC_INTERES IN NUMBER,
                              I_MON_DEC_IMP  IN NUMBER,
                              O_S_PENDIENTES OUT NUMBER,
                              O_S_VENCIDOS   OUT NUMBER,
                              O_S_INTERESES  OUT NUMBER,
                              I_SESSION_ID   IN NUMBER,
                              P_EMPRESA      IN NUMBER,
                              I_ORDENADO_POR IN VARCHAR2,
                              I_S_TIPO_SALDO IN VARCHAR2,
                              I_USUARIO      IN VARCHAR2,
                              I_DET_CONS     IN VARCHAR2 DEFAULT NULL) IS
    P_DIAS_GRACIA NUMBER;
    V_SQL         VARCHAR2(20000);
    V_MON_SQL     VARCHAR2(2000);
    --V_SQL NCLOB;
    TYPE CURTYP IS REF CURSOR;
    CV CURTYP;

    TYPE T_REC IS RECORD(
      DOC_NRO_DOC       FIN_DOCUMENTO.DOC_NRO_DOC%TYPE,
      DOC_CLI           FIN_DOCUMENTO.DOC_CLI%TYPE,
      DOC_FEC_DOC       FIN_DOCUMENTO.DOC_FEC_DOC%TYPE,
      DOC_CLAVE         FIN_DOCUMENTO.DOC_CLAVE%TYPE,
      DOC_CANT_PAGARE   FIN_DOCUMENTO.DOC_CANT_PAGARE%TYPE,
      TOT_COMPROB       NUMBER,
      CUO_FEC_VTO       FIN_CUOTA.CUO_FEC_VTO%TYPE,
      CUO_IMP_MON       FIN_CUOTA.CUO_IMP_MON%TYPE,
      NRO_PAGARE        VARCHAR2(4300),
      PAG_FEC_PAGO      FIN_PAGO.PAG_FEC_PAGO%TYPE,
      PAG_IMP_MON       FIN_PAGO.PAG_IMP_MON%TYPE,
      CUO_SALDO_MON     FIN_CUOTA.CUO_SALDO_MON%TYPE,
      TMOV_IND_DBCR_CTA GEN_TIPO_MOV.TMOV_IND_DBCR_CTA%TYPE,
      TMOV_ABREV        GEN_TIPO_MOV.TMOV_ABREV%TYPE,
      NRO_PAG           FIN_DOCUMENTO.DOC_NRO_DOC%TYPE,
      TIPO_MOV_PAGO     GEN_TIPO_MOV.TMOV_ABREV%TYPE,
      MAX_FEC           DATE,
      MONEDA            NUMBER,
      IMPORTE_CUOTA     FIN_CUOTA.CUO_IMP_MON%TYPE);
    V_REG         T_REC;
    V_DOC_ACT     NUMBER := 0;
    V_VTO_ACT     DATE := TO_DATE('01/01/1900', 'DD/MM/YYYY');
    V_INTERES     NUMBER := 0;
    V_DIAS_ATRASO NUMBER;
    -- V_OBS         FIN_DOC_CONCEPTO.DCON_OBS%TYPE;
    V_OBS NCLOB; --NO CAMBIAR ESTO, PORQUE HAY ALGUNOS CLIENTES, CUYAS FACTURAS LE CARGAN CUALQUIER COMENTARIO EN LA OBSERVACION Y SUELE TRANCAR CUANDO ES LARGO. 20/11/2019

    PROCEDURE INS_FIN_FINC004_TEMP(V_TMOV_ABREV       IN FIN_FINC004_TEMP.TMOV_ABREV%TYPE,
                                   V_DOC_CLIENTE      IN FIN_FINC004_TEMP.DOC_CLIENTE%TYPE,
                                   V_DOC_NRO_DOC      IN FIN_FINC004_TEMP.DOC_NRO_DOC%TYPE,
                                   V_DOC_FEC_DOC      IN FIN_FINC004_TEMP.DOC_FEC_DOC%TYPE,
                                   V_CUO_NRO_PAGARE   IN FIN_FINC004_TEMP.CUO_NRO_PAGARE%TYPE,
                                   V_CUO_FEC_VTO      IN FIN_FINC004_TEMP.CUO_FEC_VTO%TYPE,
                                   V_DCON_OBS         IN FIN_FINC004_TEMP.DCON_OBS%TYPE,
                                   V_S_DIAS_ATRASO    IN FIN_FINC004_TEMP.S_DIAS_ATRASO%TYPE,
                                   V_CUO_IMP_MON      IN FIN_FINC004_TEMP.CUO_IMP_MON%TYPE,
                                   V_S_NRO_PAGO       IN FIN_FINC004_TEMP.S_NRO_PAGO%TYPE,
                                   V_TIPO_MOV_PAGO    IN FIN_FINC004_TEMP.TIPO_MOV_PAGO%TYPE,
                                   V_PAG_FEC_PAGO     IN FIN_FINC004_TEMP.PAG_FEC_PAGO%TYPE,
                                   V_PAG_IMP_MON      IN FIN_FINC004_TEMP.PAG_IMP_MON%TYPE,
                                   V_S_PAG_INT        IN FIN_FINC004_TEMP.S_PAG_INT%TYPE,
                                   V_S_SALDO_MON_ACUM IN FIN_FINC004_TEMP.S_SALDO_MON_ACUM%TYPE,
                                   V_S_IMP_INTERES    IN FIN_FINC004_TEMP.S_IMP_INTERES%TYPE,
                                   V_EMPR             IN FIN_FINC004_TEMP.EMPR%TYPE,
                                   V_DOC_CLAVE        IN FIN_FINC004_TEMP.DOC_CLAVE%TYPE,
                                   V_USUARIO          IN FIN_FINC004_TEMP.USUARIO%TYPE,
                                   V_TIPO_MON         IN FIN_FINC004_TEMP.TIPO_MON%TYPE,
                                   V_IMPORTE_CUOTA    IN FIN_FINC004_TEMP.CUO_IMP_MON%TYPE) IS
    BEGIN

      INSERT INTO FIN_FINC004_TEMP
        (TMOV_ABREV,
         DOC_CLIENTE,
         DOC_NRO_DOC,
         DOC_FEC_DOC,
         CUO_NRO_PAGARE,
         CUO_FEC_VTO,
         DCON_OBS,
         S_DIAS_ATRASO,
         CUO_IMP_MON,
         S_NRO_PAGO,
         TIPO_MOV_PAGO,
         PAG_FEC_PAGO,
         PAG_IMP_MON,
         S_PAG_INT,
         S_SALDO_MON_ACUM,
         S_IMP_INTERES,
         SESSION_ID,
         EMPR,
         DOC_CLAVE,
         USUARIO,
         TIPO_MON,
         IMPORTE_CUOTA,
         DET_CONSOLIDADO)
      VALUES
        (V_TMOV_ABREV,
         V_DOC_CLIENTE,
         V_DOC_NRO_DOC,
         V_DOC_FEC_DOC,
         V_CUO_NRO_PAGARE,
         V_CUO_FEC_VTO,
         SUBSTR(V_DCON_OBS, 0, 50), --CUANDO LA OBS DE LA FACTURA PASAN 50 O MAS CARACTERES, DEVUELVE UN ERROR EN EL JASPER, CREO QUE CUANDO LOS TOMA COMO VARIAS LINEAS, RETORNO DE CARRO Y POR ESO SE LEVANTA LA EXCEPCION. MIENTRAS SE RESUELVE ESA EXCEPCION, SE CORTA A 50 NOMAS LAS OBS. 17/06/2020
         V_S_DIAS_ATRASO,
         V_CUO_IMP_MON,
         V_S_NRO_PAGO,
         V_TIPO_MOV_PAGO,
         V_PAG_FEC_PAGO,
         V_PAG_IMP_MON,
         V_S_PAG_INT,
         V_S_SALDO_MON_ACUM,
         V_S_IMP_INTERES,
         I_SESSION_ID,
         V_EMPR,
         V_DOC_CLAVE,
         V_USUARIO,
         V_TIPO_MON,
         V_IMPORTE_CUOTA,
         I_DET_CONS);
      --
      INSERT INTO FIN_FINC004_TEMP_AUD
        (TMOV_ABREV,
         DOC_CLIENTE,
         DOC_NRO_DOC,
         DOC_FEC_DOC,
         CUO_NRO_PAGARE,
         CUO_FEC_VTO,
         DCON_OBS,
         S_DIAS_ATRASO,
         CUO_IMP_MON,
         S_NRO_PAGO,
         TIPO_MOV_PAGO,
         PAG_FEC_PAGO,
         PAG_IMP_MON,
         S_PAG_INT,
         S_SALDO_MON_ACUM,
         S_IMP_INTERES,
         SESSION_ID,
         EMPR,
         DOC_CLAVE,
         FECHA,
         TIPO_MON,
         IMPORTE_CUOTA,
         DET_CONSOLIDADO)
      VALUES
        (V_TMOV_ABREV,
         V_DOC_CLIENTE,
         V_DOC_NRO_DOC,
         V_DOC_FEC_DOC,
         V_CUO_NRO_PAGARE,
         V_CUO_FEC_VTO,
         V_DCON_OBS,
         V_S_DIAS_ATRASO,
         V_CUO_IMP_MON,
         V_S_NRO_PAGO,
         V_TIPO_MOV_PAGO,
         V_PAG_FEC_PAGO,
         V_PAG_IMP_MON,
         V_S_PAG_INT,
         V_S_SALDO_MON_ACUM,
         V_S_IMP_INTERES,
         I_SESSION_ID,
         V_EMPR,
         V_DOC_CLAVE,
         SYSDATE,
         V_TIPO_MON,
         V_IMPORTE_CUOTA,
         I_DET_CONS);
    END;

  BEGIN
    ----------------------------------------------30/06/2020 LV
    -------------Pedido Deisi: consolidar a Gs , sumar intereses en deuda total  : intereses + cheques retenidos

    V_SQL := 'SELECT  DO.DOC_NRO_DOC,
                      DO.DOC_CLI,
                      DO.DOC_FEC_DOC,
                      DO.DOC_CLAVE,
                      DO.DOC_CANT_PAGARE,
                       CASE
                        WHEN ' || I_MONEDA ||
             ' = 0 THEN
                          (DO.DOC_NETO_EXEN_LOC + DO.DOC_NETO_GRAV_LOC + DO.DOC_IVA_LOC)
                        ELSE
                          (DO.DOC_NETO_EXEN_MON + DO.DOC_NETO_GRAV_MON + DO.DOC_IVA_MON)
                        END TOT_COMPROB,
                       -- (DO.DOC_NETO_EXEN_MON + DO.DOC_NETO_GRAV_MON + DO.DOC_IVA_MON) TOT_COMPROB,
                      CUO_FEC_VTO,
                      CASE
                        WHEN ' || I_MONEDA ||
             ' = 0 THEN
                           CUO_IMP_LOC * DECODE(G.TMOV_IND_DBCR_CTA,' ||
             CHR(39) || 'D' || CHR(39) ||
             ',1,-1)
                        ELSE
                           CUO_IMP_MON * DECODE(G.TMOV_IND_DBCR_CTA,' ||
             CHR(39) || 'D' || CHR(39) ||
             ',1,-1)
                        END CUO_IMP_MON,
                      -- CUO_IMP_MON * DECODE(G.TMOV_IND_DBCR_CTA,' ||
             CHR(39) || 'D' || CHR(39) ||
             ',1,-1) CUO_IMP_MON,
                      TO_CHAR(CUO_NRO_PAGARE) ||' || CHR(39) || '/' ||
             CHR(39) || '|| TO_CHAR(DO.DOC_CANT_PAGARE),
                      PAG_FEC_PAGO,
                      CASE
                        WHEN ' || I_MONEDA ||
             ' = 0 THEN
                          DECODE(PAG_IMP_LOC, 0, PAG_IMP_INT_MON, PAG_IMP_LOC) * DECODE(G.TMOV_IND_DBCR_CTA,' ||
             CHR(39) || 'D' || CHR(39) ||
             ',-1,1)
                        ELSE
                          DECODE(PAG_IMP_MON, 0, PAG_IMP_INT_MON, PAG_IMP_MON) * DECODE(G.TMOV_IND_DBCR_CTA,' ||
             CHR(39) || 'D' || CHR(39) ||
             ',-1,1)
                        END PAG_IMP_MON,
                      --DECODE(PAG_IMP_MON, 0, PAG_IMP_INT_MON, PAG_IMP_MON) * DECODE(G.TMOV_IND_DBCR_CTA,' ||
             CHR(39) || 'D' || CHR(39) || ',-1,1) PAG_IMP_MON,
                      CASE
                        WHEN ' || I_MONEDA ||
             ' = 0 THEN
                          CUO_SALDO_LOC * DECODE(G.TMOV_IND_DBCR_CTA,' ||
             CHR(39) || 'D' || CHR(39) ||
             ',1,-1)
                        ELSE
                          CUO_SALDO_MON * DECODE(G.TMOV_IND_DBCR_CTA,' ||
             CHR(39) || 'D' || CHR(39) ||
             ',1,-1)
                        END SALDO,
                      --CUO_SALDO_MON * DECODE(G.TMOV_IND_DBCR_CTA,' ||
             CHR(39) || 'D' || CHR(39) ||
             ',1,-1),
                      G.TMOV_IND_DBCR_CTA,
                      G.TMOV_ABREV,
                      DP.DOC_NRO_DOC NRO_PAG,
                      M.TMOV_ABREV TIPO_MOV_PAGO,
                      CASE WHEN (CUO_SALDO_MON>0) THEN
                              TRUNC(SYSDATE)
                            ELSE
                               (SELECT MAX(PAG_FEC_PAGO) MAX_FEC
                               FROM FIN_UNION_PAGO
                               WHERE FIN_UNION_PAGO.pag_clave_doc = CUO_CLAVE_DOC
                               AND FIN_UNION_PAGO.pag_fec_vto = CUO_FEC_VTO)
                           END MAX_FEC,
                           DO.DOC_MON MONEDA,
                        CUO_IMP_MON * DECODE(G.TMOV_IND_DBCR_CTA,' ||
             CHR(39) || 'D' || CHR(39) ||
             ',1,-1) IMPORTE_CUOTA
                      FROM GEN_TIPO_MOV  G,
                           FIN_DOCUMENTO DO,
                           FIN_DOCUMENTO DP,';
    IF I_HISTORICO = 'N' THEN
      V_SQL := V_SQL || 'FIN_CUOTA,FIN_PAGO,';
    ELSE
      V_SQL := V_SQL || 'FIN_UNION_CUOTA,FIN_UNION_PAGO,';
    END IF;
    IF I_MONEDA NOT IN (0, 100) THEN
      V_MON_SQL := 'AND DO.DOC_MON = ' || I_MONEDA;
    END IF;

    V_SQL := V_SQL || ' GEN_TIPO_MOV  M,
       (SELECT D.DOC_CLAVE_PADRE CLAVE,D.DOC_EMPR EMPR FROM FIN_DOCUMENTO D WHERE D.DOC_TIPO_MOV IN (47,48))ANUL
                                       WHERE G.TMOV_CODIGO = DO.DOC_TIPO_MOV AND G.TMOV_EMPR = DO.DOC_EMPR

                                         ' || V_MON_SQL || '
                                         AND DO.DOC_EMPR = ' ||
             P_EMPRESA ||
             ' AND M.TMOV_CODIGO(+) = DP.DOC_TIPO_MOV AND M.TMOV_EMPR(+) = DP.DOC_EMPR
                                         AND DO.DOC_TIPO_MOV NOT IN (71, 72)
                                         AND ANUL.CLAVE(+)= DO.DOC_CLAVE
                                         AND ANUL.EMPR(+) = DO.DOC_EMPR
                                         AND ANUL.CLAVE IS NULL
                                         AND (CUO_FEC_VTO = PAG_FEC_VTO(+) AND CUO_CLAVE_DOC = PAG_CLAVE_DOC(+)  AND CUO_EMPR = PAG_EMPR(+) )
                                         AND (DO.DOC_CLAVE = CUO_CLAVE_DOC AND DO.DOC_EMPR = CUO_EMPR)
                                         AND DP.DOC_CLAVE(+) = PAG_CLAVE_PAGO AND DP.DOC_EMPR(+) = PAG_EMPR
                                         AND DO.DOC_CLI IN
                                             (SELECT F.CLI_CODIGO
                                                FROM FIN_CLIENTE F
                                               WHERE F.CLI_EMPR = ' ||
             P_EMPRESA || ' AND F.CLI_COD_FICHA_HOLDING = ' || I_HOLDING || ')
                                         AND (G.TMOV_IND_AFECTA_SALDO = ' ||
             CHR(39) || 'C' || CHR(39) ||
             ' -- C=Cliente, P=Proveedor, N=No
                                             OR DO.DOC_TIPO_MOV = 18)
                                         AND NVL(G.TMOV_IND_DOC_PREST, ' ||
             CHR(39) || 'N' || CHR(39) || ') = ' || CHR(39) || 'N' ||
             CHR(39) || '
                                         AND CUO_FEC_VTO <= NVL(TO_DATE(' ||
             CHR(39) || TO_CHAR(I_FECHA_VTO, 'DD/MM/YYYY') || CHR(39) || ',' ||
             CHR(39) || 'DD/MM/YYYY' || CHR(39) || '),TO_DATE(' || CHR(39) ||
             '31/12/2999' || CHR(39) || ',' || CHR(39) || 'DD/MM/YYYY' ||
             CHR(39) || '))';

    IF I_SALDO_CERO = 'N' THEN
      V_SQL := V_SQL || 'AND DO.DOC_SALDO_MON <> 0';
    END IF;

    IF I_ORDENADO_POR = 'D' THEN
      V_SQL := V_SQL || 'ORDER BY DO.DOC_MON
                            DO.DOC_FEC_DOC,
                            G.TMOV_ABREV,
                            DO.DOC_NRO_DOC,
                            DO.DOC_CLAVE,
                            CUO_FEC_VTO,
                            PAG_FEC_PAGO';
    ELSE
      V_SQL := V_SQL || ' ORDER BY DO.DOC_MON,
                            CUO_FEC_VTO,
                            DO.DOC_CLAVE,
                            G.TMOV_ABREV,
                            DO.DOC_NRO_DOC,
                            PAG_FEC_PAGO';

    END IF;

   /* insert into x
      (campo1, otro)
    values
      ('',
       to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss') || ' hold:' || I_HOLDING);*/

    --DBMS_OUTPUT.PUT_LINE('sql: ' || V_SQL);

    SELECT CONF_DIAS_GRACIA
      INTO P_DIAS_GRACIA
      FROM FIN_CONFIGURACION
     WHERE CONF_EMPR = P_EMPRESA;

    OPEN CV FOR V_SQL; -- OPEN CURSOR VARIABLE

   -- DELETE FIN_FINC004_TEMP WHERE USUARIO = I_USUARIO;

    --SE BUSCA EL PRIMER LUGAR LIBRE DE LA LISTA DE CODIGOS
    LOOP
      FETCH CV
        INTO V_REG; -- LEER SGTE.REGISTRO
      EXIT WHEN CV%NOTFOUND; -- SALE SI NO EXISTEN MAS REGISTROS

      IF V_DOC_ACT <> V_REG.DOC_CLAVE OR V_VTO_ACT <> V_REG.CUO_FEC_VTO THEN

        V_DOC_ACT := V_REG.DOC_CLAVE;
        V_VTO_ACT := V_REG.CUO_FEC_VTO;

        IF V_REG.TMOV_IND_DBCR_CTA = 'D' THEN
          V_DIAS_ATRASO := V_REG.MAX_FEC - TRUNC(V_REG.CUO_FEC_VTO) +
                           NVL(P_DIAS_GRACIA, 0);
          IF V_DIAS_ATRASO > 0 THEN
            V_INTERES := ROUND(V_REG.CUO_SALDO_MON * V_DIAS_ATRASO *
                               I_PORC_INTERES / 36500,
                               I_MON_DEC_IMP);
          ELSE
            V_DIAS_ATRASO := NULL;
            V_INTERES     := NULL;
          END IF;
          ----

        ELSE
          V_DIAS_ATRASO := NULL;
          V_INTERES     := NULL;
        END IF;
        SELECT WM_CONCAT(DCON_OBS)
          INTO V_OBS
          FROM FIN_DOC_CONCEPTO
         WHERE DCON_CLAVE_DOC = V_REG.DOC_CLAVE
           AND DCON_EMPR = P_EMPRESA;

        INS_FIN_FINC004_TEMP(V_TMOV_ABREV       => V_REG.TMOV_ABREV,
                             V_DOC_CLIENTE      => V_REG.DOC_CLI,
                             V_DOC_NRO_DOC      => V_REG.DOC_NRO_DOC,
                             V_DOC_FEC_DOC      => V_REG.DOC_FEC_DOC,
                             V_CUO_NRO_PAGARE   => V_REG.NRO_PAGARE,
                             V_CUO_FEC_VTO      => V_REG.CUO_FEC_VTO,
                             V_DCON_OBS         => V_OBS,
                             V_S_DIAS_ATRASO    => V_DIAS_ATRASO,
                             V_CUO_IMP_MON      => V_REG.CUO_IMP_MON,
                             V_S_NRO_PAGO       => V_REG.NRO_PAG,
                             V_TIPO_MOV_PAGO    => V_REG.TIPO_MOV_PAGO,
                             V_PAG_FEC_PAGO     => V_REG.PAG_FEC_PAGO,
                             V_PAG_IMP_MON      => V_REG.PAG_IMP_MON,
                             V_S_PAG_INT        => NULL, --V_REG. ,
                             V_S_SALDO_MON_ACUM => V_REG.CUO_SALDO_MON,
                             V_S_IMP_INTERES    => V_INTERES,
                             V_EMPR             => P_EMPRESA,
                             V_DOC_CLAVE        => V_REG.DOC_CLAVE,
                             V_USUARIO          => I_USUARIO,
                             V_TIPO_MON         => V_REG.MONEDA,
                             V_IMPORTE_CUOTA    => V_REG.IMPORTE_CUOTA);

        O_S_PENDIENTES := NVL(O_S_PENDIENTES, 0) +
                          NVL(V_REG.CUO_SALDO_MON, 0);

        IF V_REG.CUO_FEC_VTO < TRUNC(SYSDATE) THEN
          O_S_VENCIDOS := NVL(O_S_VENCIDOS, 0) +
                          NVL(V_REG.CUO_SALDO_MON, 0);
        END IF;
        O_S_INTERESES := NVL(O_S_INTERESES, 0) + NVL(V_INTERES, 0);

      ELSE
        V_DIAS_ATRASO := NULL;
        V_INTERES     := NULL;

        INS_FIN_FINC004_TEMP(V_TMOV_ABREV       => NULL,
                             V_DOC_CLIENTE      => V_REG.DOC_CLI,
                             V_DOC_NRO_DOC      => NULL,
                             V_DOC_FEC_DOC      => NULL,
                             V_CUO_NRO_PAGARE   => NULL,
                             V_CUO_FEC_VTO      => NULL,
                             V_DCON_OBS         => NULL,
                             V_S_DIAS_ATRASO    => NULL,
                             V_CUO_IMP_MON      => NULL,
                             V_S_NRO_PAGO       => V_REG.NRO_PAG,
                             V_TIPO_MOV_PAGO    => V_REG.TIPO_MOV_PAGO,
                             V_PAG_FEC_PAGO     => V_REG.PAG_FEC_PAGO,
                             V_PAG_IMP_MON      => V_REG.PAG_IMP_MON,
                             V_S_PAG_INT        => NULL, --V_REG. ,
                             V_S_SALDO_MON_ACUM => NULL,
                             V_S_IMP_INTERES    => NULL, --V_REG.
                             V_EMPR             => P_EMPRESA,
                             V_DOC_CLAVE        => V_REG.DOC_CLAVE,
                             V_USUARIO          => I_USUARIO,
                             V_TIPO_MON         => V_REG.MONEDA,
                             V_IMPORTE_CUOTA    => V_REG.IMPORTE_CUOTA);

      END IF;

    END LOOP;

    O_S_VENCIDOS := O_S_VENCIDOS; -- + O_S_INTERESES;
    IF O_S_VENCIDOS < 0 OR O_S_VENCIDOS IS NULL THEN
      O_S_VENCIDOS := 0;
    END IF;
    IF O_S_INTERESES IS NULL THEN
      O_S_INTERESES := 0;
    END IF;
    IF O_S_PENDIENTES IS NULL THEN
      O_S_PENDIENTES := 0;
    END IF;
    -- EXCEPTION
    -- WHEN OTHERS THEN
    --  RAISE_APPLICATION_ERROR(-20001,
    --                        'ERROR AL BUSCAR CODIGO... VERIFIQUE LA SINTAXIS DE LA LLAMADA!' ||
    --                         SQLCODE || SQLERRM);
  end pp_cargar_detalle;
  
  ----
  procedure add_conf_bank(
    in_empresa      in number,
    in_regional_gs  in number,
    in_regional_usd in number
  )is
  begin
     merge into fin_conf_archivo_banco fa
     using (select 
               in_empresa      empresa_id,
               in_regional_gs  regional_gs,
               in_regional_usd regional_usd
       from dual) fan
     on (fa.empresa_id = fan.empresa_id)
     when matched then update set fa.regional_gs  = fan.regional_gs,
                                  fa.regional_usd = fan.regional_usd
     when not matched then insert(empresa_id, 
                                  regional_gs,
                                  regional_usd
                                  )values(
                                  fan.empresa_id,
                                  fan.regional_gs,
                                  fan.regional_usd
                                  );
  end add_conf_bank;
  ---
  procedure get_conf_archive(
    in_empresa            in number
  )as
    l_cod_reg_gs   fin_conf_archivo_banco.regional_gs%type;
    l_desc_reg_gs  fin_cuenta_bancaria.cta_desc%type;

    l_cod_reg_usd  fin_conf_archivo_banco.regional_usd%type;
    l_desc_reg_usd fin_cuenta_bancaria.cta_desc%type;
  begin
    <<get_configuration>>
    begin
      select a.regional_gs,
             reg_gs.cta_desc reg_gs,
                 
             a.regional_usd,
             reg_usd.cta_desc reg_usd
      into l_cod_reg_gs,
           l_desc_reg_gs,
             
           l_cod_reg_usd,
           l_desc_reg_usd
      from fin_conf_archivo_banco a
      left join fin_cuenta_bancaria reg_gs  on ( reg_gs.cta_codigo = a.regional_gs and reg_gs.cta_empr = a.empresa_id )
      left join fin_cuenta_bancaria reg_usd on ( reg_usd.cta_codigo = a.regional_usd and reg_usd.cta_empr = a.empresa_id )
      where a.empresa_id = in_empresa;
    exception
      when no_data_found then      
        l_cod_reg_gs   := null;
        l_desc_reg_gs  := null;
        l_cod_reg_usd  := null;
        l_desc_reg_usd := null;
    end get_configuration;  
      
    apex_json.open_object;
      apex_json.write(p_name => 'ind_error', p_value => 0);
      apex_json.write(p_name => 'l_cod_reg_gs', p_value => l_cod_reg_gs);
      apex_json.write(p_name => 'l_desc_reg_gs', p_value => l_desc_reg_gs);
      apex_json.write(p_name => 'l_cod_reg_usd', p_value => l_cod_reg_usd);
      apex_json.write(p_name => 'l_desc_reg_usd', p_value => l_desc_reg_usd);
    apex_json.close_object;
  exception
    when others then
        apex_json.open_object;
            apex_json.write(p_name => 'error', p_value=> replace(sqlerrm, 'ORA-20000:',''));
            apex_json.write(p_name => 'ind_error', p_value=> 1);
        apex_json.close_object;
  end get_conf_archive;
  
END FINM027;
/
