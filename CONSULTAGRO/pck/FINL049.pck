CREATE OR REPLACE PACKAGE FINL049 IS

  -- Author  : PROGRAMACION12
  -- Created : 20/01/2020 13:53:37
  -- Purpose : PACKAGE para conciliaci?in bancaria

  PROCEDURE CARGAR_CAMPOS_TEX(I_EMPRESA     IN NUMBER,
                              I_CTA_CODIGO  IN NUMBER,
                              DESCRIPCION   OUT VARCHAR2,
                              CUENTA        OUT VARCHAR2,
                              P_DEC_MON_IMP OUT NUMBER,
                              V_MONEDA      OUT VARCHAR2);

  PROCEDURE CARGAR_CAMPOS_TIPOFEC(I_VALOR      IN VARCHAR2,
                                  I_DEVOLUCION OUT VARCHAR2);

  PROCEDURE LLAMAR_REPORTE_CONCILIACION(I_EMPRESA     IN NUMBER,
                                        V_SESION      IN NUMBER,
                                        I_CONSOLIDADO IN VARCHAR2,
                                        V_vALOR       IN NUMBER);

  PROCEDURE CARGA_DE_PARAMETROS(V_EMPRESA            IN NUMBER,
                                V_CUENTABANCO_CODIGO IN NUMBER,
                                CHEQUE_DIFERIDO      OUT VARCHAR2,
                                MON_DEC_IMP          OUT NUMBER);

  PROCEDURE LLAMAR_CONCILIACION(V_CONSOLIDADO     IN VARCHAR2,
                                V_CUENTA_BANCARIA IN NUMBER,
                                V_EMPRESA         IN NUMBER,
                                V_FECHA_DESDE     IN DATE,
                                V_FECHA_HASTA     IN DATE,
                                V_TIPO_FECHA      IN VARCHAR2,
                                V_SQLL            OUT VARCHAR2);

  PROCEDURE LLAMAR_PROCESO(V_CONSOLIDADO     IN VARCHAR2,
                           V_CUENTA_BANCARIA IN NUMBER,
                           V_EMPRESA         IN NUMBER,
                           V_FECHA_DESDE     IN DATE,
                           V_FECHA_HASTA     IN DATE,
                           V_TIPO_FECHA      IN VARCHAR2,
                           V_SQLL            OUT VARCHAR2,
                           V_SESION          IN NUMBER,
                           V_SUCURSAL        IN NUMBER,
                           V_VALOR           OUT NUMBER);

  PROCEDURE INSERTAR_TABLA_AUX(V_APP_SECCION IN NUMBER,
                               V_EMPRES      IN NUMBER,
                               V_CONSOLIDADO IN VARCHAR2,
                               V_TIPO_FECHA  IN VARCHAR2,
                               V_FECHA_DESDE IN VARCHAR2,
                               V_FECHA_HASTA IN VARCHAR2);

END FINL049;
/
CREATE OR REPLACE PACKAGE BODY FINL049 IS

  PROCEDURE CARGAR_CAMPOS_TEX(I_EMPRESA     IN NUMBER,
                              I_CTA_CODIGO  IN NUMBER,
                              DESCRIPCION   OUT VARCHAR2,
                              CUENTA        OUT VARCHAR2,
                              P_DEC_MON_IMP OUT NUMBER,
                              V_MONEDA      OUT VARCHAR2) AS

    DESCRIPCION_CUENTA VARCHAR2(40);
    V_CHEQ_DIF         VARCHAR2(40);
    BANCO              VARCHAR2(40);
    P_DEC_MON_IMPA     NUMBER;
    P_BANCO            NUMBER;
    CODIGO_BANCO       NUMBER;
    MONEDA             VARCHAR2(5);
    V_SALD_TIP         NUMBER;
  BEGIN

    SELECT CTA_DESC,
           NVL(CTA_IND_CHEQ_DIF, 'N'),
           BCO_DESC,
           MON_DEC_IMP,
           CTA_BCO,
           CTA_CODIGO,
           MON_SIMBOLO
      INTO DESCRIPCION_CUENTA,
           V_CHEQ_DIF,
           BANCO,
           P_DEC_MON_IMPA,
           P_BANCO,
           CODIGO_BANCO,
           MONEDA
      FROM FIN_CUENTA_BANCARIA, FIN_BANCO, GEN_MONEDA
     WHERE CTA_BCO = BCO_CODIGO(+)
       AND CTA_MON = MON_CODIGO
       AND CTA_CODIGO = I_CTA_CODIGO
       AND CTA_EMPR = I_EMPRESA
       AND CTA_EMPR = MON_EMPR
       AND CTA_EMPR = BCO_EMPR(+);

    IF V_CHEQ_DIF = 'S' THEN
      DESCRIPCION := NULL;
      CUENTA      := NULL;
      RAISE_APPLICATION_ERROR(-20010,
                              'La cuenta bancaria seleccionada no puede ser una cuenta bancaria de cheques emitidos en diferido!.');
    END IF;

    P_DEC_MON_IMP := P_DEC_MON_IMPA;
    DESCRIPCION   := BANCO;
    CUENTA        := DESCRIPCION_CUENTA;
    V_MONEDA      := MONEDA;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20010, 'Cuenta bancaria Inexistente!.');

  END CARGAR_CAMPOS_TEX;

  PROCEDURE CARGAR_CAMPOS_TIPOFEC(I_VALOR      IN VARCHAR2,
                                  I_DEVOLUCION OUT VARCHAR2) AS

  BEGIN

    IF I_VALOR = 'O' THEN
      I_DEVOLUCION := 'O';
    ELSE
      I_DEVOLUCION := 'D';
    END IF;

  END CARGAR_CAMPOS_TIPOFEC;

  PROCEDURE CARGA_DE_PARAMETROS(V_EMPRESA            IN NUMBER,
                                V_CUENTABANCO_CODIGO IN NUMBER,
                                CHEQUE_DIFERIDO      OUT VARCHAR2,
                                MON_DEC_IMP          OUT NUMBER) AS

    V_CHEQ_DIF    VARCHAR2(10);
    P_DEC_MON_IMP NUMBER;

  BEGIN

    SELECT NVL(CTA_IND_CHEQ_DIF, 'N'), MON_DEC_IMP
      INTO V_CHEQ_DIF, P_DEC_MON_IMP
      FROM FIN_CUENTA_BANCARIA, FIN_BANCO, GEN_MONEDA
     WHERE CTA_BCO = BCO_CODIGO(+)
       AND CTA_MON = MON_CODIGO
       AND CTA_CODIGO = V_CUENTABANCO_CODIGO
       AND CTA_EMPR = V_EMPRESA
       AND CTA_EMPR = MON_EMPR
       AND CTA_EMPR = BCO_EMPR(+);

    IF V_CHEQ_DIF = 'S' THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'La cuenta bancaria seleccionada no puede ser una cuenta bancaria de cheques emitidos en diferido!.');
    END IF;

    CHEQUE_DIFERIDO := V_CHEQ_DIF;
    MON_DEC_IMP     := P_DEC_MON_IMP;

  END CARGA_DE_PARAMETROS;

  PROCEDURE LLAMAR_REPORTE_CONCILIACION(I_EMPRESA     IN NUMBER,
                                        V_SESION      IN NUMBER,
                                        I_CONSOLIDADO IN VARCHAR2,
                                        V_VALOR       IN NUMBER) AS

    V_PARAMETROS        VARCHAR2(600);
    V_IDENTIFICADOR     VARCHAR2(2) := '&';
    V_NOMBRE_FORMULARIO VARCHAR2(50);
    MONEDAS             VARCHAR2(25);
    A                   number;

  BEGIN
    --   RAISE_APPLICATION_ERROR(-20001,V_VALOR);

    SELECT PROG_NOMBRE_FORMULARIO
      INTO V_NOMBRE_FORMULARIO
      FROM GEN_PROGRAMA
     WHERE PROG_CLAVE = 1936;
     
    IF I_EMPRESA = 2 THEN
      SELECT PROG_NOMBRE_FORMULARIO
        INTO V_NOMBRE_FORMULARIO
        FROM GEN_PROGRAMA
       WHERE PROG_CLAVE = 5936;
    END IF;
    
    SELECT MONEDA, COUNT(*)
      INTO MONEDAS, A
      FROM FIN_FINL049_TEMP T
     WHERE EMPRESA = I_EMPRESA
       AND SESION_ID = V_SESION
     GROUP BY MONEDA;
    
    V_PARAMETROS := 'P_FORMATO=' || APEX_UTIL.URL_ENCODE('pdf');
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_EMPRESA=' ||
                    APEX_UTIL.URL_ENCODE(I_EMPRESA);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_SESSION=' ||
                    APEX_UTIL.URL_ENCODE(V_SESION);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_FORMULARIO=' ||
                    APEX_UTIL.URL_ENCODE(V_NOMBRE_FORMULARIO);         
                     
    IF MONEDAS = 'DOLARES AMERICANOS' AND I_CONSOLIDADO = 'N' THEN
      IF V_VALOR = 1 THEN
        DELETE FROM GEN_PARAMETROS_REPORT
         WHERE USUARIO = GEN_DEVUELVE_USER;
        INSERT INTO GEN_PARAMETROS_REPORT
          (PARAMETROS, USUARIO, NOMBRE_REPORTE, FORMATO_SALIDA)
        VALUES
          (V_PARAMETROS, GEN_DEVUELVE_USER, 'FINL049_Sin_Mov_USD', 'pdf');

      ELSIF V_VALOR = 2 THEN
        DELETE FROM GEN_PARAMETROS_REPORT
         WHERE USUARIO = GEN_DEVUELVE_USER;
         
        INSERT INTO GEN_PARAMETROS_REPORT
          (PARAMETROS, USUARIO, NOMBRE_REPORTE, FORMATO_SALIDA)
        VALUES
          (V_PARAMETROS, GEN_DEVUELVE_USER, 'FINL049_P_USD', 'pdf');
      END IF;
    ELSE

      IF V_VALOR = 1 THEN
        DELETE FROM GEN_PARAMETROS_REPORT
         WHERE USUARIO = GEN_DEVUELVE_USER;
        INSERT INTO GEN_PARAMETROS_REPORT
          (PARAMETROS, USUARIO, NOMBRE_REPORTE, FORMATO_SALIDA)
        VALUES
          (V_PARAMETROS, GEN_DEVUELVE_USER, 'FINL049_Sin_Mov', 'pdf');

      ELSIF V_VALOR = 2 then        
        DELETE FROM GEN_PARAMETROS_REPORT
         WHERE USUARIO = GEN_DEVUELVE_USER;
         
        INSERT INTO GEN_PARAMETROS_REPORT
          (PARAMETROS, USUARIO, NOMBRE_REPORTE, FORMATO_SALIDA)
        VALUES
          (V_PARAMETROS, GEN_DEVUELVE_USER, 'FINL049_ppp', 'pdf');

      END IF;

    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20010, SQLERRM);

  END LLAMAR_REPORTE_CONCILIACION;

  PROCEDURE LLAMAR_CONCILIACION(V_CONSOLIDADO     IN VARCHAR2,
                                V_CUENTA_BANCARIA IN NUMBER,
                                V_EMPRESA         IN NUMBER,
                                V_FECHA_DESDE     IN DATE,
                                V_FECHA_HASTA     IN DATE,
                                V_TIPO_FECHA      IN VARCHAR2,
                                V_SQLL            OUT VARCHAR2) AS
    V_WHERE         VARCHAR2(30000);
    V_SQL           VARCHAR2(30000);
    V_SALDO_INICIAL VARCHAR2(50);

  BEGIN

    IF V_TIPO_FECHA = 'O' THEN
      V_WHERE := 'DOC_FEC_OPER';

      IF V_CONSOLIDADO = 'S' THEN
        V_SALDO_INICIAL := 'SAL_INI_LOC_OPER';
      ELSE
        V_SALDO_INICIAL := 'SAL_INI_MON_OPER';
      END IF;

    ELSE

      V_WHERE := 'DOC_FEC_DOC';

      IF V_CONSOLIDADO = 'S' THEN
        V_SALDO_INICIAL := 'SAL_INI_LOC_DOC';
      ELSE
        V_SALDO_INICIAL := 'SAL_INI_MON_DOC';
      END IF;

    END IF;

    V_SQL := 'SELECT
       DOC_CLAVE DOC_CLAVE,
       DOC_CTA_BCO DOC_CTA_BCO,
       CTA_DESC CTA_DESC,
       BCO_DESC BCO_DESC,
         DECODE(' || CHR(39) || V_TIPO_FECHA || CHR(39) ||
             ',  ''O''
             ,DOC_FEC_OPER,DOC_FEC_DOC) FECHA,
       NVL(DECODE(TMOV_TIPO, ''D'', DOC_NRO_DOC),0) NRO_COMPR_DEBITO,
       NVL(DECODE(TMOV_TIPO, ''C'', DOC_NRO_DOC),0) NRO_COMPR_CREDITO,
      DECODE(' || CHR(39) || V_CONSOLIDADO || CHR(39) ||
             ' , ''N'',DECODE(TMOV_TIPO,''D'',
              DOC_NETO_EXEN_MON + DOC_NETO_GRAV_MON + DOC_IVA_MON,
              0),(DECODE(TMOV_TIPO, ''D'' ,
              DOC_NETO_EXEN_LOC + DOC_NETO_GRAV_LOC + DOC_IVA_LOC,
              0))) DEBITODE,
       DECODE(' || CHR(39) || V_CONSOLIDADO || CHR(39) ||
             ' , ''N'',DECODE(TMOV_TIPO, ''C'',
              (DOC_NETO_EXEN_MON + DOC_NETO_GRAV_MON + DOC_IVA_MON),
              0),(DECODE(TMOV_TIPO, ''C'',
              (DOC_NETO_EXEN_LOC + DOC_NETO_GRAV_LOC + DOC_IVA_LOC),
              0)) ) CREDITODE ,
       DECODE(CH_EMIT_BENEFICIARIO,
              NULL,
              (DECODE(DOC_CLI_NOM, NULL, DOC_OBS, DOC_CLI_NOM)),
              CH_EMIT_BENEFICIARIO) DOC_CLI_NOM,
       DECODE(TMOV_TIPO,''D'',
              DOC_NETO_EXEN_LOC + DOC_NETO_GRAV_LOC + DOC_IVA_LOC,
              0) CREDITOS_LOC,
       DECODE(TMOV_TIPO,''C'',
              (DOC_NETO_EXEN_LOC + DOC_NETO_GRAV_LOC + DOC_IVA_LOC),
              0) DEBITOS_LOC,
       DOC_OBS,
        DECODE(' || CHR(39) || V_CONSOLIDADO || CHR(39) ||
             ' , ''N'', MON_DESC ,''GUARANIES''),

        (SELECT SUM(DECODE(' || CHR(39) || V_CONSOLIDADO ||
             CHR(39) || ' , ''N'' ,DECODE(TMOV_TIPO,''D'',
              (DOC_NETO_EXEN_MON + DOC_NETO_GRAV_MON + DOC_IVA_MON),
              0),(DECODE(TMOV_TIPO,
             ''D'',
              (DOC_NETO_EXEN_LOC + DOC_NETO_GRAV_LOC + DOC_IVA_LOC),
              0))) )
          FROM GEN_TIPO_MOV, FIN_DOCUMENTO DO
         WHERE TMOV_EMPR = DO.DOC_EMPR
           AND TMOV_EMPR = ' || V_EMPRESA || '
           AND TMOV_CODIGO = DO.DOC_TIPO_MOV
           AND DOC_EMPR = ' || V_EMPRESA || '
           AND ' || V_WHERE || ' BETWEEN TO_DATE(' || CHR(39) ||
             V_FECHA_DESDE || CHR(39) || ') AND
               TO_DATE(' || CHR(39) || V_FECHA_HASTA || CHR(39) || ')
           AND DOC_CTA_BCO =' || V_CUENTA_BANCARIA ||
             ' )  SDEBITOS,

       (SELECT SUM(DECODE(' || CHR(39) || V_CONSOLIDADO ||
             CHR(39) || ' , ''N'' ,DECODE(TMOV_TIPO,
             ''C'',
              (DOC_NETO_EXEN_MON + DOC_NETO_GRAV_MON + DOC_IVA_MON),
              0),(DECODE(TMOV_TIPO,
            ''C'',
              (DOC_NETO_EXEN_LOC + DOC_NETO_GRAV_LOC + DOC_IVA_LOC),
              0)) ) )
       FROM
        GEN_TIPO_MOV, FIN_DOCUMENTO DO
        WHERE
        TMOV_EMPR = DO.DOC_EMPR
        AND TMOV_EMPR = ' || V_EMPRESA || '
        AND TMOV_CODIGO = DO.DOC_TIPO_MOV
        AND DOC_EMPR = ' || V_EMPRESA || '
        AND ' || V_WHERE || ' BETWEEN TO_DATE(' || CHR(39) ||
             V_FECHA_DESDE || CHR(39) || ') AND
        TO_DATE(' || CHR(39) || V_FECHA_HASTA || CHR(39) || ')
        AND DOC_CTA_BCO = ' || V_CUENTA_BANCARIA || ' ) SCREDITO,
             CH_EMIT_BENEFICIARIO,
             CH_EMIT_SERIE CH_EMIT_SERIE,
       ' || V_SALDO_INICIAL || ' SALDO_INICIAL

  FROM GEN_TIPO_MOV,
       FIN_DOCUMENTO DO,
       FIN_CHEQUE_EMIT,
       FIN_CUENTA_BANCARIA,
       FIN_SAL_CTA_BCO,
       FIN_BANCO,
       GEN_MONEDA
 WHERE TMOV_EMPR = ' || V_EMPRESA || '
   AND TMOV_EMPR = DO.DOC_EMPR
   AND DO.DOC_EMPR = CTA_EMPR
   AND CTA_EMPR = BCO_EMPR(+)
   AND SAL_EMPR = CTA_EMPR
   AND DOC_EMPR = MON_EMPR
   AND DO.DOC_EMPR = CH_EMIT_EMPR(+)
   AND TMOV_CODIGO = DO.DOC_TIPO_MOV
   AND CTA_CODIGO = DOC_CTA_BCO
   AND CTA_BCO = BCO_CODIGO(+)
   AND SAL_CTA_BCO = CTA_CODIGO
   AND DOC_MON = MON_CODIGO
   AND SAL_FEC = TO_DATE(' || CHR(39) || V_FECHA_DESDE ||
             CHR(39) || ')
   AND DO.DOC_CLAVE = CH_EMIT_CLAVE_FIN_CAN(+)
   AND (CTA_IND_CHEQ_DIF <> ''S''  OR CTA_IND_CHEQ_DIF IS NULL)
   AND DOC_EMPR =  ' || V_EMPRESA || '
   and ' || V_WHERE || ' between TO_DATE(' || CHR(39) ||
             V_FECHA_DESDE || CHR(39) || ') and TO_DATE(' || CHR(39) ||
             V_FECHA_HASTA || CHR(39) || ')
   and  DOC_CTA_BCO = ' || V_CUENTA_BANCARIA || '
ORDER BY ' || V_WHERE;

    V_SQLL := V_SQL;
    
     INSERT INTO X (CAMPO1) VALUES (V_SQL);
        COMMIT;

    IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => 'FINL049') THEN
      APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => 'FINL049');
    END IF;

    APEX_COLLECTION.CREATE_COLLECTION_FROM_QUERY_B(P_COLLECTION_NAME => 'FINL049',
                                                   P_QUERY           => V_SQL);
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20010, SQLERRM);
  END LLAMAR_CONCILIACION;

  PROCEDURE LLAMAR_PROCESO(V_CONSOLIDADO     IN VARCHAR2,
                           V_CUENTA_BANCARIA IN NUMBER,
                           V_EMPRESA         IN NUMBER,
                           V_FECHA_DESDE     IN DATE,
                           V_FECHA_HASTA     IN DATE,
                           V_TIPO_FECHA      IN VARCHAR2,
                           V_SQLL            OUT VARCHAR2,
                           V_SESION          IN NUMBER,
                           V_SUCURSAL        IN NUMBER,
                           V_VALOR           OUT NUMBER) AS

    V_WHERE           VARCHAR2(30000);
    V_SQL             VARCHAR2(30000);
    V_SALDO_INICIAL   VARCHAR2(50);
    CLAVE             NUMBER;
    V_WHERE_SUCURSAL  VARCHAR2(50);
    FECHA_DESDE       VARCHAR2(30);
    FECHA_HASTA       VARCHAR2(30);
    FECHA_INICIO      VARCHAR2(20);
    FECHA_FINAL       VARCHAR2(20);
    FECHAHASTA        VARCHAR2(20);
    FECHADESDE        VARCHAR2(20);
    V_BANCO_DESC      VARCHAR2(3000);
    V_WHERE1          VARCHAR2(30000);
    V_TIPO_S          VARCHAR2(2500);
    V_TIPO_FECH       VARCHAR2(2500);
    V_COND_DIF_CAMBIO VARCHAR2(2500);

    TYPE CURTYP IS REF CURSOR;
    FINL249 CURTYP;

    TYPE TAGRO IS RECORD(

      DOC_LOGIN            VARCHAR2(100),
      DOC_CLAVE            NUMBER,
      DOC_CTA_BCO          NUMBER,
      CTA_DESC             VARCHAR2(550),
      BCO_DESC             VARCHAR2(550),
      MON_DEC_IMP          NUMBER,
      MON_SIMBOLO          VARCHAR2(550),
      DOC_MON              NUMBER,
      MON_DESC             VARCHAR2(550),
      SALDO_INICIAL        NUMBER,
      FECHA                DATE, --VARCHAR2(50),
      NRO_COMPR_CREDITO    NUMBER,
      CH_EMIT_SERIE        VARCHAR2(550),
      NRO_COMPR_DEBITO     NUMBER,
      CREDITOS             NUMBER,
      DEBITOS              NUMBER,
      DOC_CLI_NOM          VARCHAR2(550),
      CH_EMIT_BENEFICIARIO VARCHAR2(550),
      CREDITOS_LOC         NUMBER,
      DEBITOS_LOC          NUMBER,
      CH_EMIT_NRO          NUMBER);
    C TAGRO;

    V_EMPR_DESC VARCHAR2(250);

    X          FIN_SAL_CTA_BCO %ROWTYPE;
    V_SES      NUMBER;
    V_MON_DESC VARCHAR2(250);
    V_CONTADOR NUMBER := 0;
  begin
    IF V_TIPO_FECHA = 'O' THEN
      V_WHERE := 'DOC_FEC_OPER';

      IF V_CONSOLIDADO = 'S' THEN
        V_SALDO_INICIAL := 'SAL_INI_LOC_OPER';
      ELSE
        V_SALDO_INICIAL := 'SAL_INI_MON_OPER';
      END IF;

    ELSE

      V_WHERE := 'DOC_FEC_DOC';

      IF V_CONSOLIDADO = 'S' THEN
        V_SALDO_INICIAL := 'SAL_INI_LOC_DOC';
        IF V_EMPRESA = 2 THEN
          V_COND_DIF_CAMBIO := ' ---AND DOC_TIPO_MOV <> 27 ';
        ELSE
          V_COND_DIF_CAMBIO := ' AND 1=1 ';
        END IF;
      ELSE
        IF V_EMPRESA = 2 THEN
          V_COND_DIF_CAMBIO := ' AND DOC_TIPO_MOV <> 27 ';
        ELSE
          V_COND_DIF_CAMBIO := ' AND 1=1 ';
        END IF;
        V_SALDO_INICIAL := 'SAL_INI_MON_DOC';

      END IF;

    END IF;

    DECLARE
      CONTADOR    NUMBER := 0;
      FECHA_DESDE VARCHAR2(50) := TO_CHAR(V_FECHA_DESDE, 'DD/MM/YYYY');
      FECHA_HASTA VARCHAR2(50) := TO_CHAR(V_FECHA_HASTA, 'DD/MM/YYYY');
    BEGIN

      LOOP

        SELECT TO_CHAR(ADD_MONTHS(TRUNC(TO_DATE(FECHA_DESDE, 'dd/mm/yyyy')),
                                  -CONTADOR),
                       'DD/MM/YYYY')
          INTO FECHA_INICIO
          FROM DUAL;
          
        SELECT TO_CHAR(ADD_MONTHS(LAST_DAY(TRUNC(TO_DATE(FECHA_HASTA,
                                                         'dd/mm/yyyy'))),
                                  -CONTADOR),
                       'DD/MM/YYYY')
          INTO FECHA_FINAL
          FROM DUAL;
          
        SELECT NVL(SUM(B), 0)
          INTO CLAVE
          FROM (SELECT COUNT(DOC_CLAVE) B
                  FROM GEN_TIPO_MOV, FIN_DOCUMENTO DO
                 WHERE TMOV_EMPR = DO.DOC_EMPR
                   AND TMOV_EMPR = V_EMPRESA
                   AND TMOV_CODIGO = DO.DOC_TIPO_MOV
                   AND DOC_EMPR = V_EMPRESA
                   AND trunc(DOC_FEC_DOC) BETWEEN to_Date(FECHA_INICIO, 'dd/mm/yyyy') AND to_date(FECHA_FINAL, 'dd/mm/yyyy')
                   AND DOC_CTA_BCO = V_CUENTA_BANCARIA
                 GROUP BY DOC_CLAVE
                 );
                 
        
        
        IF CLAVE = 0 then
          
          CONTADOR := CONTADOR + 1;
          -- se_agrega tope de meses para calculo
          -- hasta 10 anios (120 meses)
        ELSE
          FECHADESDE := FECHA_INICIO;
          FECHAHASTA := FECHA_FINAL;
          
          EXIT WHEN CLAVE > 0;
        END IF;
      END LOOP;
       
      IF CONTADOR = 1 OR CONTADOR >= 2 THEN
        V_VALOR := 1;
      ELSE
        V_VALOR := 2;
      END IF;
      
    END;
   
    IF V_SUCURSAL IS NULL THEN
      V_WHERE_SUCURSAL := ' ';
    ELSE
      V_WHERE_SUCURSAL := 'AND SUC_CODIGO = ' || V_SUCURSAL;
    END IF;
    
    IF V_EMPRESA <> 2 THEN
      V_SQL := ' SELECT
       DOC_CLAVE DOC_CLAVE,
       DOC_CTA_BCO DOC_CTA_BCO,
       CTA_DESC CTA_DESC,
       BCO_DESC BCO_DESC,
         DECODE(' || CHR(39) || V_TIPO_FECHA || CHR(39) ||
               ',  ''O''
             ,DOC_FEC_OPER,DOC_FEC_DOC) FECHA,
       NVL(DECODE(TMOV_TIPO, ''D'', DOC_NRO_DOC),0) NRO_COMPR_DEBITO,
       NVL(DECODE(TMOV_TIPO, ''C'', DOC_NRO_DOC),0) NRO_COMPR_CREDITO,
      DECODE(' || CHR(39) || V_CONSOLIDADO || CHR(39) ||
               ' , ''N'',DECODE(TMOV_TIPO,''D'',
              DOC_NETO_EXEN_MON + DOC_NETO_GRAV_MON + DOC_IVA_MON,
              0),(DECODE(TMOV_TIPO, ''D'' ,
              DOC_NETO_EXEN_LOC + DOC_NETO_GRAV_LOC + DOC_IVA_LOC,
              0))) DEBITODE,
       DECODE(' || CHR(39) || V_CONSOLIDADO || CHR(39) ||
               ' , ''N'',DECODE(TMOV_TIPO, ''C'',
              (DOC_NETO_EXEN_MON + DOC_NETO_GRAV_MON + DOC_IVA_MON),
              0),(DECODE(TMOV_TIPO, ''C'',
              (DOC_NETO_EXEN_LOC + DOC_NETO_GRAV_LOC + DOC_IVA_LOC),
              0)) ) CREDITODE ,
       DECODE(CH_EMIT_BENEFICIARIO,
              NULL,
              (DECODE(DOC_CLI_NOM, NULL, DOC_OBS, DOC_CLI_NOM)),
              CH_EMIT_BENEFICIARIO) DOC_CLI_NOM,
       DECODE(TMOV_TIPO,''D'',
              DOC_NETO_EXEN_LOC + DOC_NETO_GRAV_LOC + DOC_IVA_LOC,
              0) CREDITOS_LOC,
       DECODE(TMOV_TIPO,''C'',
              (DOC_NETO_EXEN_LOC + DOC_NETO_GRAV_LOC + DOC_IVA_LOC),
              0) DEBITOS_LOC,
       DOC_OBS,
        DECODE(' || CHR(39) || V_CONSOLIDADO || CHR(39) ||
               ' , ''N'', MON_DESC ,''GUARANIES''),
        (SELECT SUM(DECODE(' || CHR(39) || V_CONSOLIDADO ||
               CHR(39) || ' , ''N'' ,DECODE(TMOV_TIPO,''D'',
              (DOC_NETO_EXEN_MON + DOC_NETO_GRAV_MON + DOC_IVA_MON),
              0),(DECODE(TMOV_TIPO,
             ''D'',
              (DOC_NETO_EXEN_LOC + DOC_NETO_GRAV_LOC + DOC_IVA_LOC),
              0))) )
          FROM GEN_TIPO_MOV, FIN_DOCUMENTO DO
         WHERE TMOV_EMPR = DO.DOC_EMPR
           AND TMOV_EMPR = ' || V_EMPRESA || '
           AND TMOV_CODIGO = DO.DOC_TIPO_MOV
           AND DOC_EMPR = ' || V_EMPRESA || '
           AND ' || V_WHERE || ' BETWEEN TO_DATE(' || CHR(39) ||
               FECHADESDE || CHR(39) || ') AND
               TO_DATE(' || CHR(39) || FECHAHASTA || CHR(39) || ')
           AND DOC_CTA_BCO =' || V_CUENTA_BANCARIA ||
               ' )  SDEBITOS,
       (SELECT SUM(DECODE(' || CHR(39) || V_CONSOLIDADO ||
               CHR(39) || ' , ''N'' ,DECODE(TMOV_TIPO,
             ''C'',
              (DOC_NETO_EXEN_MON + DOC_NETO_GRAV_MON + DOC_IVA_MON),
              0),(DECODE(TMOV_TIPO,
            ''C'',
              (DOC_NETO_EXEN_LOC + DOC_NETO_GRAV_LOC + DOC_IVA_LOC),
              0)) ) )
       FROM
        GEN_TIPO_MOV, FIN_DOCUMENTO DO
        WHERE
        TMOV_EMPR = DO.DOC_EMPR
        AND TMOV_EMPR = ' || V_EMPRESA || '
        AND TMOV_CODIGO = DO.DOC_TIPO_MOV
        AND DOC_EMPR = ' || V_EMPRESA || '
        AND ' || V_WHERE || ' BETWEEN TO_DATE(' || CHR(39) ||
               FECHADESDE || CHR(39) || ') AND
        TO_DATE(' || CHR(39) || FECHAHASTA || CHR(39) || ')
        AND DOC_CTA_BCO = ' || V_CUENTA_BANCARIA || ' ) SCREDITO,
             CH_EMIT_BENEFICIARIO,
             CH_EMIT_SERIE CH_EMIT_SERIE,
        ' || V_SALDO_INICIAL || ' SALDO_INICIAL,
        DOC_MON,
        SUC_DESC,
        EMPR_RAZON_SOCIAL,
       DOC_LOGIN,
       DOC_TIPO_MOV,
       DOC_FEC_GRAB
  FROM GEN_TIPO_MOV,
       FIN_DOCUMENTO DO,
       FIN_CHEQUE_EMIT,
       FIN_CUENTA_BANCARIA,
       FIN_SAL_CTA_BCO,
       FIN_BANCO,
       GEN_MONEDA,
       GEN_EMPRESA,
       GEN_SUCURSAL
 WHERE TMOV_EMPR = ' || V_EMPRESA || '
   AND EMPR_CODIGO = ' || V_EMPRESA || '
   ' || V_WHERE_SUCURSAL || '
   AND SUC_EMPR = ' || V_EMPRESA || '
   AND TMOV_EMPR = DO.DOC_EMPR
   AND DO.DOC_EMPR = CTA_EMPR
   AND CTA_EMPR = BCO_EMPR(+)
   AND SAL_EMPR = CTA_EMPR
   AND DOC_EMPR = MON_EMPR
   AND DO.DOC_EMPR = CH_EMIT_EMPR(+)
   AND TMOV_CODIGO = DO.DOC_TIPO_MOV
   AND CTA_CODIGO = DOC_CTA_BCO
   AND CTA_BCO = BCO_CODIGO(+)
   AND SAL_CTA_BCO = CTA_CODIGO
   AND DOC_MON = MON_CODIGO
   AND SAL_FEC = TO_DATE(' || CHR(39) || FECHA_INICIO ||
               CHR(39) || ')--DOC_FEC_DOC
   AND DO.DOC_CLAVE = CH_EMIT_CLAVE_FIN_CAN(+)
   AND (CTA_IND_CHEQ_DIF <> ''S''  OR CTA_IND_CHEQ_DIF IS NULL)
   AND DOC_EMPR =  ' || V_EMPRESA || '
   and ' || V_WHERE || ' BETWEEN TO_DATE(' || CHR(39) ||
               FECHADESDE || CHR(39) || ') and TO_DATE(' || CHR(39) ||
               FECHAHASTA || CHR(39) || ')
   and  DOC_CTA_BCO = ' || V_CUENTA_BANCARIA || '
ORDER BY ' || V_WHERE;

 delete from x where otro = 'AAAA';
 INSERT INTO X (CAMPO1, OTRO) VALUES (V_SQL, 'AAAA');

    ELSE
      BEGIN
        SELECT BCO_DESC, MON_DESC
          INTO V_BANCO_DESC, V_MON_DESC
          FROM FIN_CUENTA_BANCARIA, FIN_BANCO, GEN_MONEDA m
         WHERE CTA_BCO    = BCO_CODIGO(+)
           AND CTA_EMPR   = BCO_EMPR(+)
           AND CTA_MON    = MON_CODIGO
           AND CTA_EMPR   = MON_EMPR
           AND CTA_EMPR   = V_EMPRESA
           AND CTA_CODIGO = V_CUENTA_BANCARIA;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
          V_BANCO_DESC := NULL;
      END;

      V_WHERE := ' ';
      --
      V_WHERE := 'DOC_EMPR= ' || TO_CHAR(V_EMPRESA);

      IF V_TIPO_FECHA = 'O' THEN
        V_TIPO_FECH := 'DOC_FEC_OPER';
        V_WHERE     := V_WHERE || ' AND DOC_FEC_OPER BETWEEN ' || '''' ||
                       (V_FECHA_DESDE) || '''' || ' AND ' || '''' ||
                       (V_FECHA_HASTA) || '''';
        IF V_CONSOLIDADO = 'S' THEN
          V_TIPO_S := 'SAL_INI_LOC_OPER';
        ELSE
          V_TIPO_S := 'SAL_INI_MON_OPER';
        END IF;
      ELSE
        V_TIPO_FECH := 'DOC_FEC_DOC';
        V_WHERE     := V_WHERE || ' AND DOC_FEC_DOC BETWEEN ' || '''' ||
                       (V_FECHA_DESDE) || '''' || ' AND ' || '''' ||
                       (V_FECHA_HASTA) || '''';
        IF V_CONSOLIDADO = 'S' THEN
          V_TIPO_S := 'SAL_INI_LOC_DOC';
        ELSE
          V_TIPO_S := 'SAL_INI_MON_DOC';
        END IF;
      END IF;
      IF V_SUCURSAL IS NOT NULL THEN
        V_WHERE  := V_WHERE || ' AND DOC_SUC = ' || TO_CHAR(V_SUCURSAL);
        V_WHERE1 := V_WHERE1 || ' AND DO.DOC_SUC = ' || TO_CHAR(V_SUCURSAL);
      END IF;

      IF V_CUENTA_BANCARIA IS NOT NULL THEN
        V_WHERE := V_WHERE || ' AND CTA_CODIGO = ' ||
                   TO_CHAR(V_CUENTA_BANCARIA);
      END IF;

      IF V_BANCO_DESC IS NOT NULL THEN
        V_WHERE1 := V_WHERE || ' AND CTA_BCO IS NOT NULL';
      ELSE
        V_WHERE1 := V_WHERE;
      END IF;

      V_SQL := 'SELECT DOC_LOGIN,
      DOC_CLAVE DOC_CLAVE,
       DOC_CTA_BCO DOC_CTA_BCO,
       CTA_DESC CTA_DESC,
       BCO_DESC BCO_DESC,
       DECODE(''' || V_CONSOLIDADO || ''', ''N'', MON_DEC_IMP, 0) MON_DEC_IMP,
       MON_SIMBOLO MON_SIMBOLO,
       DOC_MON DOC_MON,
       MON_DESC MON_DESC,
       NULL SALDO_INICIAL,
       TO_DATE(' || V_TIPO_FECH || ',''DD/MM/YYYY'') FECHA,
       DECODE(TMOV_TIPO,''C'', DOC_NRO_DOC) NRO_COMPR_CREDITO,
       CH_EMIT_SERIE CH_EMIT_SERIE,
       DECODE(TMOV_TIPO, ''D'', CH_EMIT_NRO) NRO_COMPR_DEBITO,
       DECODE(TMOV_TIPO, ''C'', CH_EMIT_IMPORTE/*DOC_NETO_EXEN_MON*/, 0) CREDITOS,
       DECODE(TMOV_TIPO, ''D'', CH_EMIT_IMPORTE, 0) DEBITOS,
       DECODE(CH_EMIT_BENEFICIARIO,
              NULL,
              (DECODE(DOC_CLI_NOM, NULL, DOC_OBS, DOC_CLI_NOM)),
              CH_EMIT_BENEFICIARIO) DOC_CLI_NOM,
       CH_EMIT_BENEFICIARIO,
       DECODE(TMOV_TIPO, ''C'', CH_EMIT_IMPORTE/*DOC_NETO_EXEN_LOC*/, 0) CREDITOS_LOC,
       DECODE(TMOV_TIPO,
              ''D'',
              ((DOC_NETO_EXEN_LOC + DOC_NETO_GRAV_LOC + DOC_IVA_LOC) /
              (DOC_NETO_EXEN_MON + DOC_NETO_GRAV_MON + DOC_IVA_MON)) *
              CH_EMIT_IMPORTE,
              0) DEBITOS_LOC  ,
              CH_EMIT_NRO
  FROM GEN_TIPO_MOV,
       FIN_DOCUMENTO DO,
       FIN_CHEQUE_EMIT,
       FIN_CUENTA_BANCARIA,
     --  FIN_SAL_CTA_BCO,
       FIN_BANCO,
       GEN_MONEDA
 WHERE TMOV_CODIGO = DO.DOC_TIPO_MOV
   AND TMOV_EMPR = DO.DOC_EMPR
   AND CTA_CODIGO = DOC_CTA_BCO
   AND CTA_EMPR = DOC_EMPR
   AND CTA_BCO = BCO_CODIGO
   AND CTA_EMPR = BCO_EMPR
   --AND SAL_CTA_BCO = CTA_CODIGO
  --AND SAL_EMPR = CTA_EMPR
   AND DOC_MON = MON_CODIGO
   AND DOC_EMPR = MON_EMPR
  -- AND SAL_FEC = ''' || V_FECHA_DESDE || '''
   AND DO.DOC_CLAVE = CH_EMIT_CLAVE_FIN_CAN(+)
   AND DO.DOC_EMPR = CH_EMIT_EMPR(+)
   AND DO.DOC_EMPR = ' || V_EMPRESA || '
   AND CH_EMIT_CLAVE_FIN IS NOT NULL
   AND CTA_BCO IS NOT NULL
  ' || V_COND_DIF_CAMBIO || '
   AND (CTA_IND_CHEQ_DIF <> ''S'' OR CTA_IND_CHEQ_DIF IS NULL)
   AND ' || V_WHERE || '
UNION
SELECT DOC_LOGIN,
       DOC_CLAVE DOC_CLAVE,
       DOC_CTA_BCO DOC_CTA_BCO,
       CTA_DESC CTA_DESC,
       BCO_DESC BCO_DESC,
       DECODE(''' || V_CONSOLIDADO || ''', ''N'', MON_DEC_IMP, 0) MON_DEC_IMP,
       MON_SIMBOLO MON_SIMBOLO,
       DOC_MON DOC_MON,
       MON_DESC MON_DESC,
       NULL SALDO_INICIAL,
       ' || V_TIPO_FECH || ' FECHA,
       DECODE(TMOV_TIPO, ''C'', DOC_NRO_DOC) NRO_COMPR_CREDITO,
       CH_EMIT_SERIE CH_EMIT_SERIE,
       DECODE(TMOV_TIPO, ''D'', DOC_NRO_DOC) NRO_COMPR_DEBITO,
       DECODE(TMOV_TIPO,
              ''C'',
              DOC_NETO_EXEN_MON + DOC_NETO_GRAV_MON + DOC_IVA_MON,
              0) CREDITOS,
       DECODE(TMOV_TIPO,
              ''D'',
              (DOC_NETO_EXEN_MON + DOC_NETO_GRAV_MON + DOC_IVA_MON),
              0) DEBITOS,
       DECODE(CH_EMIT_BENEFICIARIO,
              NULL,
              (DECODE(DOC_CLI_NOM, NULL, DOC_OBS, DOC_CLI_NOM)),
              CH_EMIT_BENEFICIARIO) DOC_CLI_NOM,
       CH_EMIT_BENEFICIARIO,
       DECODE(TMOV_TIPO,
              ''C'',
              DOC_NETO_EXEN_LOC + DOC_NETO_GRAV_LOC + DOC_IVA_LOC,
              0) CREDITOS_LOC,
       DECODE(TMOV_TIPO,
              ''D'',
              (DOC_NETO_EXEN_LOC + DOC_NETO_GRAV_LOC + DOC_IVA_LOC),
              0) DEBITOS_LOC  ,
              CH_EMIT_NRO
  FROM GEN_TIPO_MOV,
       FIN_DOCUMENTO DO,
       FIN_CHEQUE_EMIT,
       FIN_CUENTA_BANCARIA,
      -- FIN_SAL_CTA_BCO,
       FIN_BANCO,
       GEN_MONEDA
 WHERE TMOV_CODIGO = DO.DOC_TIPO_MOV
   AND TMOV_EMPR = DO.DOC_EMPR
   AND CTA_CODIGO = DOC_CTA_BCO
   AND CTA_EMPR = DOC_EMPR
   AND CTA_BCO = BCO_CODIGO(+)
   AND CTA_EMPR = BCO_EMPR(+)
  -- AND SAL_CTA_BCO = CTA_CODIGO
  -- AND SAL_EMPR = CTA_EMPR
   AND DOC_MON = MON_CODIGO
   AND DOC_EMPR = MON_EMPR
  ' || V_COND_DIF_CAMBIO || '

  -- AND SAL_FEC = ''' || V_FECHA_DESDE || '''
   AND DO.DOC_CLAVE = CH_EMIT_CLAVE_FIN_CAN(+)
   AND DO.DOC_EMPR = CH_EMIT_EMPR(+)
   AND CH_EMIT_CLAVE_FIN IS NULL
   AND DO.DOC_EMPR = ' || V_EMPRESA || '
   AND (CTA_IND_CHEQ_DIF <> ''S'' OR CTA_IND_CHEQ_DIF IS NULL)
   AND (DOC_TIPO_MOV NOT IN
       (SELECT CONF_RETENCION_REC
           FROM FIN_CONFIGURACION
          WHERE CONF_EMPR = ' || V_EMPRESA || '
         UNION
         SELECT CONF_RETENCION_EMIT
           FROM FIN_CONFIGURACION
          WHERE CONF_EMPR = ' || V_EMPRESA || '))
   AND ' || V_WHERE1 || '
  UNION ALL
SELECT NULL DOC_LOGIN,
       NULL DOC_CLAVE,
       SAL_CTA_BCO DOC_CTA_BCO,
       CTA_DESC CTA_DESC,
       BCO_DESC BCO_DESC,
       NULL MON_DEC_IMP,
       NULL MON_SIMBOLO,
       NULL DOC_MON,
       ''' || V_MON_DESC || ''' MON_DESC,
       DECODE(''' || V_CONSOLIDADO || ''', ''S'', DECODE(' ||
               V_TIPO_FECHA ||
               ',''O'',S.SAL_INI_LOC_OPER,S.SAL_INI_LOC_DOC),DECODE(' ||
               V_TIPO_FECHA || ',''O'',S.SAL_INI_MON_OPER,S.SAL_INI_MON_DOC)) SALDO_INICIAL,
       SAL_FEC FECHA,
       NULL NRO_COMPR_CREDITO,
       NULL CH_EMIT_SERIE,
       NULL NRO_COMPR_DEBITO,
       NULL CREDITOS,
       NULL DEBITOS,
       NULL DOC_CLI_NOM,
       NULL CH_EMIT_BENEFICIARIO,
       NULL CREDITOS_LOC,
       NULL DEBITOS_LOC ,
       NULL CH_EMIT_NRO
  FROM FIN_SAL_CTA_BCO S, FIN_CUENTA_BANCARIA B,FIN_BANCO
 WHERE S.SAL_EMPR = B.CTA_EMPR
   AND S.SAL_CTA_BCO = B.CTA_CODIGO
   AND BCO_CODIGO(+) = B.CTA_BCO
   AND BCO_EMPR(+) = B.CTA_EMPR
   AND S.SAL_EMPR = ' || V_EMPRESA || '
   AND S.SAL_CTA_BCO = ' || V_CUENTA_BANCARIA || '
   AND S.SAL_FEC = ''' || V_FECHA_DESDE || ''' ';

      INSERT INTO X (CAMPO1, OTRO) VALUES (V_SQL, 'AAAA');
      COMMIT;

      BEGIN
        SELECT E.EMPR_RAZON_SOCIAL
          INTO V_EMPR_DESC
          FROM GEN_EMPRESA E
         WHERE E.EMPR_CODIGO = V_EMPRESA;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
          V_EMPR_DESC := NULL;
      END;

      DELETE FROM FIN_FINL049_TEMP P WHERE P.SESION_ID = V_SESION;
      COMMIT;

      OPEN FINL249 FOR V_SQL;
      LOOP
        FETCH FINL249
          INTO C;
        EXIT WHEN FINL249%NOTFOUND;

        IF C.DOC_CLAVE IS NOT NULL THEN
          V_CONTADOR := V_CONTADOR + 1;
        END IF;

        INSERT INTO FIN_FINL049_TEMP
          (CLAVE,
           CUENTA_BANCARIA,
           DESCRIPCION_CUENTA,
           BANCO,
           FECHA,
           COMPR_DEBITO,
           COMPR_CREDITO,
           SUMA_DEBITOS,
           SUMA_CREDITOS,
           CH_EMIT_SERIE,
           SALDO_INICIAL,
           SESION_ID,
           CREDITOS_LOC,
           DEBITOS_LOC,
           MONEDA,
           SDEBITO,
           SCREDITO,
           CHE_EMI_BENEFI,
           EMPRESA,
           FECHA_DESDE,
           FECHA_HASTA,
           CODMON,
           TIPO_FECHA,
           SUCURSAL,
           EMPRESA_DESC,
           CLIENTE,
           LOGIN)
        VALUES
          (C.DOC_CLAVE,
           C.DOC_CTA_BCO,
           C.CTA_DESC,
           C.BCO_DESC,
           C.FECHA,
           C.NRO_COMPR_DEBITO,
           C.NRO_COMPR_CREDITO,
           DECODE(V_CONSOLIDADO, 'S', C.DEBITOS_LOC, C.DEBITOS),
           DECODE(V_CONSOLIDADO, 'S', C.CREDITOS_LOC, C.CREDITOS),
           C.CH_EMIT_SERIE,
           C.SALDO_INICIAL,
           V_SESION,
           C.CREDITOS_LOC,
           C.DEBITOS_LOC,
           C.MON_DESC,
           DECODE(V_CONSOLIDADO, 'S', C.DEBITOS_LOC, C.DEBITOS),
           DECODE(V_CONSOLIDADO, 'S', C.CREDITOS_LOC, C.CREDITOS),
           C.CH_EMIT_BENEFICIARIO,
           V_EMPRESA,
           V_FECHA_DESDE,
           V_FECHA_HASTA,
           C.DOC_MON,
           V_TIPO_FECH,
           V_SUCURSAL,
           V_EMPR_DESC,
           C.DOC_CLI_NOM,
           C.DOC_LOGIN);

      END LOOP;

      BEGIN
        SELECT COUNT(1)
          INTO V_SES
          FROM FIN_FINL049_TEMP
         WHERE SESION_ID = V_SESION;

        IF V_SES = 0 THEN

          SELECT SA.*
            INTO X
            FROM FIN_CUENTA_BANCARIA B, FIN_SAL_CTA_BCO SA
           WHERE B.CTA_EMPR = SA.SAL_EMPR
             AND B.CTA_CODIGO = SA.SAL_CTA_BCO
             AND SA.SAL_FEC = V_FECHA_HASTA
             AND B.CTA_CODIGO = V_CUENTA_BANCARIA
             AND B.CTA_EMPR = V_EMPRESA;

          INSERT INTO FIN_FINL049_TEMP
            (CUENTA_BANCARIA, SALDO_INICIAL, SESION_ID)
          VALUES
            (X.SAL_CTA_BCO,
             DECODE(V_CONSOLIDADO,
                    'S',
                    X.SAL_INI_LOC_OPER,
                    X.SAL_INI_MON_OPER),
             V_SESION);

          V_VALOR := 1;
        END IF;

        IF V_CONTADOR >= 1 THEN
          V_VALOR := 2;
        ELSE
          V_VALOR := 1;
        END IF;

      END;
      COMMIT;

    END IF;

    IF V_EMPRESA <> 2 THEN
      V_SQLL := V_SQL;

      IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => 'FINL049') THEN
        APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => 'FINL049');
      END IF;
      DECLARE
        E_20104 EXCEPTION;
        PRAGMA EXCEPTION_INIT(E_20104, -20104);
      BEGIN
        APEX_COLLECTION.CREATE_COLLECTION_FROM_QUERY(P_COLLECTION_NAME => 'FINL049',
                                                     P_QUERY           => V_SQL);

      EXCEPTION
        WHEN E_20104 THEN
          NULL;
      END;

      BEGIN
        -- CALL THE PROCEDURE
        FINL049.INSERTAR_TABLA_AUX(V_APP_SECCION => V_SESION,
                                   V_EMPRES      => V_EMPRESA,
                                   V_CONSOLIDADO => V_CONSOLIDADO,
                                   V_TIPO_FECHA  => V_TIPO_FECHA,
                                   V_FECHA_DESDE => V_FECHA_DESDE,
                                   V_FECHA_HASTA => V_FECHA_HASTA);
      END;

    END IF;
    
  END LLAMAR_PROCESO;

  PROCEDURE INSERTAR_TABLA_AUX(V_APP_SECCION IN NUMBER,
                               V_EMPRES      IN NUMBER,
                               V_CONSOLIDADO IN VARCHAR2,
                               V_TIPO_FECHA  IN VARCHAR2,
                               V_FECHA_DESDE IN VARCHAR2,
                               V_FECHA_HASTA IN VARCHAR2) AS

    CURSOR C IS
      SELECT TO_NUMBER(C001) CLAVE,
             TO_NUMBER(C002) CUENTA_BANCARIA,
             C003 DESCRIPCION_CUENTA,
             C004 BANCO,
             C005 FECHA,
             TO_NUMBER(C006) COMPR_DEBITO,
             TO_NUMBER(C007) COMPR_CREDITO,
             TO_NUMBER(C008) DEBITOS,
             TO_NUMBER(C009) CREDITOS,
             C010 CLIENTE,
             TO_NUMBER(C011) CREDITOS_LOC,
             TO_NUMBER(C012) DEBITOS_LOC,
             C013 OBS,
             C014 MONEDA,
             TO_NUMBER(C015) SDEBITO,
             TO_NUMBER(C016) SCREDITOS,
             C017 CH_EMIT_BENEFICIARIO,
             C018 CH_EMIT_SERIE,
             TO_NUMBER(C019) SALDO_INICIAL,
             C020 CODMON,
             C021 SUCURSAL,
             C022 EMPRESA,
             C023 DOC_LOGIN,
             C024 DOC_TIPO_MOV,
             C025 DOC_FEC_GRAB

        FROM APEX_COLLECTIONS
       WHERE COLLECTION_NAME = 'FINL049';

  BEGIN

    DELETE FROM FIN_FINL049_TEMP P WHERE P.SESION_ID = V_APP_SECCION;
    COMMIT;

    FOR I IN C LOOP

      INSERT INTO FIN_FINL049_TEMP
        (CLAVE,
         CUENTA_BANCARIA,
         DESCRIPCION_CUENTA,
         BANCO,
         FECHA,
         COMPR_DEBITO,
         COMPR_CREDITO,
         SUMA_DEBITOS,
         SUMA_CREDITOS,
         CH_EMIT_SERIE,
         SALDO_INICIAL,
         SESION_ID,
         CLIENTE,
         CREDITOS_LOC,
         DEBITOS_LOC,
         OBSS,
         MONEDA,
         SDEBITO,
         SCREDITO,
         CHE_EMI_BENEFI,
         EMPRESA,
         CONSOLIDADO,
         FECHA_DESDE,
         FECHA_HASTA,
         CODMON,
         TIPO_FECHA,
         SUCURSAL,
         EMPRESA_DESC,
         LOGIN,
         TM,
         FECHA_GRABACION)
      VALUES
        (I.CLAVE,
         I.CUENTA_BANCARIA,
         I.DESCRIPCION_CUENTA,
         I.BANCO,
         I.FECHA,
         I.COMPR_DEBITO,
         I.COMPR_CREDITO,
         I.DEBITOS,
         I.CREDITOS,
         I.CH_EMIT_SERIE,
         I.SALDO_INICIAL,
         V_APP_SECCION,
         I.CLIENTE,
         I.CREDITOS_LOC,
         I.DEBITOS_LOC,
         I.OBS,
         I.MONEDA,
         I.SDEBITO,
         I.SCREDITOS,
         I.CH_EMIT_BENEFICIARIO,
         V_EMPRES,
         V_CONSOLIDADO,
         V_FECHA_DESDE,
         V_FECHA_HASTA,
         I.CODMON,
         V_TIPO_FECHA,
         I.SUCURSAL,
         I.EMPRESA,
         I.DOC_LOGIN,
         I.DOC_TIPO_MOV,
         I.DOC_FEC_GRAB);

    END LOOP;
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20010, SQLERRM);
  END;

END FINL049;
/
