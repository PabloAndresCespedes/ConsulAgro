CREATE OR REPLACE PACKAGE FIN_AUT_ESP_PER_ANT AS

  PROCEDURE PED_INSER_DOCUMENTO(P_CLAVE_DOC    IN NUMBER,
                                P_EMPRESA      IN NUMBER,
                                P_LOGIN        IN VARCHAR2,
                                P_OBSERVACION  IN VARCHAR2,
                                P_SISTEMA      IN VARCHAR2,
                                P_FECHA_PEDIDO IN DATE);

  PROCEDURE PP_ANULAR_DOCUMENTO_FIN(P_CLAVE_DOC        IN NUMBER,
                                    P_EMPRESA          IN NUMBER,
                                    P_MOTIVO_ANULACION IN NUMBER);
  PROCEDURE PED_ANUL_DOCUMENTO(P_CLAVE_DOC    IN NUMBER,
                               P_EMPRESA      IN NUMBER,
                               P_LOGIN        IN VARCHAR2,
                               P_OBSERVACION  IN VARCHAR2,
                               P_SISTEMA      IN VARCHAR2,
                               P_MOTIVO_ANUL  IN NUMBER,
                               P_FECHA_PEDIDO IN DATE);

  PROCEDURE PP_GUARDAR_DOC_ANULADO(P_DOC_TIPO_MOV IN NUMBER,
                                   P_DOC_NRO_DOC  IN NUMBER,
                                   P_EMPRESA      IN NUMBER,
                                   P_DOC_SUC      IN NUMBER,
                                   P_CUENTA       IN NUMBER,
                                   P_DOC_FEC_DOC  IN DATE,
                                   P_ANUL_MOTIVO  IN NUMBER,
                                   P_ANUL_OBS     IN VARCHAR2,
                                   P_DOC_TIMBRADO IN NUMBER);

  PROCEDURE PP_VERIF_CAN_CHE_EMIT(P_CLAVE_DOC IN NUMBER,
                                  P_EMPRESA   IN NUMBER);

  PROCEDURE PP_VERIF_CH_EMIT(P_CLAVE_DOC IN NUMBER, P_EMPRESA IN NUMBER);

  PROCEDURE PP_ACT_CLAVE_RETENCION(P_CLAVE_DOC IN NUMBER,
                                   P_EMPRESA   IN NUMBER);

  PROCEDURE PP_BORRAR_DOC_HIJO(P_CLAVE_DOC IN NUMBER,
                               P_TIPO_MOV  IN NUMBER,
                               P_EMPRESA   IN NUMBER);
  PROCEDURE PP_ELIMINAR_CHEQUE_1(P_CLAVE_DOC IN NUMBER,
                                 P_EMPRESA   IN NUMBER);

  PROCEDURE PP_BORRA_PAGO_CUO(P_CLAVE_DOC IN NUMBER, P_EMPRESA IN NUMBER);

  PROCEDURE PP_VERIFICA_PAGO(P_CLAVE_DOC IN NUMBER,
                             P_EMPRESA   IN NUMBER,
                             P_INDI_PAGO OUT NUMBER);

  PROCEDURE PP_BORRAR_CHEQUES_ASIGNADOS(P_CLAVE_DOC IN NUMBER,
                                        P_EMPRESA   IN NUMBER);

  PROCEDURE PP_VERIF_TARJETA_CREDITO(P_CLAVE_DOC IN NUMBER,
                                     P_EMPRESA   IN NUMBER);

  PROCEDURE PP_VERIF_EXIST_PAGOS(P_CLAVE_DOC IN NUMBER,
                                 P_EMPRESA   IN NUMBER);

  FUNCTION FP_EXISTE_DOCUMENTO(P_CLAVE_DOC IN NUMBER, P_EMPRESA IN NUMBER)
    RETURN BOOLEAN;

  PROCEDURE PP_BORRAR_PAGO(P_CLAVE_DOC IN NUMBER, P_EMPRESA IN NUMBER);

  PROCEDURE PP_VERIF_CH_REC(P_CLAVE_DOC IN NUMBER, P_EMPRESA IN NUMBER);

  PROCEDURE PP_HABILITAR_SELECION(P_EMPRESA IN NUMBER, P_LOGIN IN VARCHAR);

  PROCEDURE PED_ANUL_DOCUMENTO_FAC(P_CLAVE_DOC       IN NUMBER,
                                   P_EMPRESA         IN NUMBER,
                                   P_LOGIN           IN VARCHAR2,
                                   P_OBSERVACION     IN VARCHAR2,
                                   P_SISTEMA         IN VARCHAR2,
                                   P_MOTIVO_ANUL     IN NUMBER,
                                   P_FECHA_PEDIDO    IN DATE,
                                   P_SUCURSAL        IN NUMBER,
                                   P_DEPOSITO        IN NUMBER,
                                   P_FECHA_OPERACION IN DATE);

  PROCEDURE PP_BORRAR_DOCUMENTOS_HIJO_STK(P_CLAVE_DOC IN NUMBER,
                                          P_EMPRESA   IN NUMBER);

  PROCEDURE PP_ANULAR_DOCUMENTO_STK(P_CLAVE_DOC        IN NUMBER,
                                    P_EMPRESA          IN NUMBER,
                                    P_MOTIVO_ANULACION IN NUMBER);
  PROCEDURE PP_CAMBIAR_ESTADO_TRASLADO_STK(P_CLAVE_DOC IN NUMBER,
                                           P_EMPRESA   IN NUMBER);
  PROCEDURE PP_VERIFICAR_MOD_PER_ANT(P_EMPRESA IN NUMBER DEFAULT 1);

  PROCEDURE PED_ACTUALIZACION_DOCUMENTO(P_CLAVE_DOC       IN NUMBER,
                                        P_EMPRESA         IN NUMBER,
                                        P_LOGIN           IN VARCHAR2,
                                        P_OBSERVACION     IN VARCHAR2,
                                        P_SISTEMA         IN VARCHAR2,
                                        P_MOTIVO_ANUL     IN NUMBER,
                                        P_FECHA_PEDIDO    IN DATE,
                                        P_OPERACION       IN VARCHAR2,
                                        P_FECHA_DOC       IN DATE,
                                        P_NRO_TIMBRADO    IN NUMBER,
                                        P_CANAL_DOC       IN NUMBER,
                                        P_CLAVE_CTO       IN NUMBER,
                                        P_CLAVE_CTACO     IN NUMBER,
                                        P_DCON_CANAL      IN NUMBER,
                                        P_DCON_NRO_ITEM   IN NUMBER,
                                        P_NRO_IMPORTACION IN NUMBER,
                                        P_NRO_DOCUMENTO   IN NUMBER,
                                        P_DCON_OBS        IN VARCHAR2,
                                        P_DCON_TIPO_IVA   IN NUMBER DEFAULT NULL);
  PROCEDURE PP_MODIFICAR_CONCEPTO(P_EMPRESA       IN NUMBER,
                                  P_OPERACION     IN VARCHAR2,
                                  P_COD_SOLICITUD IN NUMBER);

END;
/
CREATE OR REPLACE PACKAGE BODY FIN_AUT_ESP_PER_ANT AS

  PROCEDURE PP_ANULAR_DOCUMENTO_FIN(P_CLAVE_DOC        IN NUMBER,
                                    P_EMPRESA          IN NUMBER,
                                    P_MOTIVO_ANULACION IN NUMBER) IS
    --OBS: NUNCA CAMBIAR EL ORDEN DE LA ELIMINACION DE CANCELACION DE DOCUMENTOS
    --     PORQUE TENDRA EFECTOS NO DESEADOS EN EL PACKAGE:
    --     FIN_P_FINS001_PACK.PROCESAR_FINS001_PAGO

    SALIR       EXCEPTION;
    OTROSISTEMA EXCEPTION;
    V_RESU               INTEGER;
    V_CLAVE_STK          NUMBER;
    TM                   NUMBER; --TIPO DE MOVIMIENTO
    NRO_DOC_PADRE        VARCHAR2(20); --NUMERO DOC DEL PADRE
    V_DOC_NRO_DOC        NUMBER;
    V_DOC_TIPO_MOV       NUMBER;
    V_DOC_SUC            NUMBER;
    V_DOC_CTABCO         NUMBER;
    V_DOC_FEC_DOC        DATE;
    V_DOC_CLAVE_PADRE    NUMBER;
    V_DOC_TIPO_MOV_PADRE NUMBER;
    V_NRO_DOC_PADRE      NUMBER;
    V_DOC_SIST           VARCHAR2(45);
    V_DOC_FEC_OPER       DATE;
    V_DOC_LOGIN          VARCHAR(450);
    V_DOC_CLAVE_STK      NUMBER;
    V_ANUL_MOTIVO        NUMBER := 1;
    V_DOC_TIMBRADO       NUMBER;
    V_DOC_CLI            NUMBER;
    V_DOC_PROV           NUMBER;
    V_INDI_PAGO          NUMBER;
    V_MENSAJE_CORREO     CLOB;
    DESTINATARIO         VARCHAR2(600);
    COPIA                VARCHAR2(600);
    OCULTA               VARCHAR2(150);

    V_CONF_RECIBO_PAGO_REC     NUMBER;
    V_CONF_RECIBO_PAGO_EMIT    NUMBER;
    V_CONF_RECIBO_CNCR_REC     NUMBER;
    V_CONF_RECIBO_CNCR_EMIT    NUMBER;
    V_CONF_RECIBO_CADCLI_EMIT  NUMBER;
    V_CONF_RECIBO_CADPRO_REC   NUMBER;
    V_CONF_TMOV_DEVOL_ADEL_CLI NUMBER;
    V_CONF_TMOV_DEVOL_ADEL_PRO NUMBER;
    V_CONF_TMOV_PAGO_NC        NUMBER;
    V_CONF_TMOV_PAGO_NC_REC    NUMBER;
    V_CONF_RECIBO_CAN_FAC_EMIT NUMBER;
    V_CONF_RECIBO_CAN_FAC_REC  NUMBER;
    V_CONF_FACT_CO_EMIT        NUMBER;
    V_CONF_COD_DEP             NUMBER;

  BEGIN
    V_ANUL_MOTIVO := NVL(P_MOTIVO_ANULACION, 1);
    --RAISE_APPLICATION_ERROR(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
    SELECT CONF_RECIBO_PAGO_REC,
           CONF_RECIBO_PAGO_EMIT,
           CONF_RECIBO_CNCR_REC,
           CONF_RECIBO_CNCR_EMIT,
           CONF_RECIBO_CADCLI_EMIT,
           CONF_RECIBO_CADPRO_REC,
           CONF_TMOV_DEVOL_ADEL_CLI,
           CONF_TMOV_DEVOL_ADEL_PRO,
           CONF_TMOV_PAGO_NC,
           CONF_TMOV_PAGO_NC_REC,
           CONF_RECIBO_CAN_FAC_EMIT,
           CONF_RECIBO_CAN_FAC_REC,
           CONF_FACT_CO_EMIT,
           CONF_COD_DEP
      INTO V_CONF_RECIBO_PAGO_REC,
           V_CONF_RECIBO_PAGO_EMIT,
           V_CONF_RECIBO_CNCR_REC,
           V_CONF_RECIBO_CNCR_EMIT,
           V_CONF_RECIBO_CADCLI_EMIT,
           V_CONF_RECIBO_CADPRO_REC,
           V_CONF_TMOV_DEVOL_ADEL_CLI,
           V_CONF_TMOV_DEVOL_ADEL_PRO,
           V_CONF_TMOV_PAGO_NC,
           V_CONF_TMOV_PAGO_NC_REC,
           V_CONF_RECIBO_CAN_FAC_EMIT,
           V_CONF_RECIBO_CAN_FAC_REC,
           V_CONF_FACT_CO_EMIT,
           V_CONF_COD_DEP
      FROM FIN_CONFIGURACION C
     WHERE C.CONF_EMPR = P_EMPRESA;
    BEGIN

      SELECT DOC_NRO_DOC,
             DOC_TIPO_MOV,
             DOC_SUC,
             DOC_CTA_BCO,
             DOC_FEC_DOC,
             DOC_CLAVE_PADRE,
             DOC_FEC_OPER,
             DOC_SIST,
             DOC_LOGIN,
             DOC_CLAVE_STK,
             DOC_TIMBRADO,
             DOC_CLI,
             DOC_PROV
        INTO V_DOC_NRO_DOC,
             V_DOC_TIPO_MOV,
             V_DOC_SUC,
             V_DOC_CTABCO,
             V_DOC_FEC_DOC,
             V_DOC_CLAVE_PADRE,
             V_DOC_FEC_OPER,
             V_DOC_SIST,
             V_DOC_LOGIN,
             V_DOC_CLAVE_STK,
             V_DOC_TIMBRADO,
             V_DOC_CLI,
             V_DOC_PROV
        FROM FIN_DOCUMENTO
       WHERE DOC_CLAVE = P_CLAVE_DOC
         AND DOC_EMPR = P_EMPRESA;

    EXCEPTION
      WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,
                                'Error al cargar datos del documento!');
    END;

    IF V_DOC_SIST   NOT IN  ('COM','FIN')  THEN
      RAISE OTROSISTEMA;
    END IF;

    DECLARE
      RESUL CHAR(1);
    BEGIN
      SELECT 'X'
        INTO RESUL
        FROM FIN_DOC_CHEQ_REL
       WHERE DOCHE_DOCU = P_CLAVE_DOC
         AND DOCHE_EMPR = P_EMPRESA;
      IF RESUL = 'X' THEN
        RAISE_APPLICATION_ERROR(-20001,
                                'Primero debe borrar el cheque relacionado al documento!');
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
      WHEN TOO_MANY_ROWS THEN
        RAISE_APPLICATION_ERROR(-20002,
                                'Primero debe borrar el cheque relacionado al documento!');
      WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20003,
                                'Error no tratado al busca relacion con cheque!' ||
                                SQLERRM);

    END;

    DECLARE
      V_CHE_COD NUMBER;
    BEGIN
      SELECT FHC.CHDO_CLAVE_CHEQ
        INTO V_CHE_COD
        FROM FIN_CHEQUE_DOCUMENTO FHC
       WHERE FHC.CHDO_CLAVE_DOC = P_CLAVE_DOC
         AND FHC.CHDO_EMPR = P_EMPRESA;


      DELETE FROM FIN_DOC_CHEQ_REL
       WHERE DOCHE_CHEQ = V_CHE_COD
         AND DOCHE_EMPR = P_EMPRESA;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
      WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20004,
                                'Error no tratado al eliminar relacion con cheque!');
    END;

    IF P_CLAVE_DOC IS NULL THEN
      RAISE SALIR;
    END IF;
    IF NOT FP_EXISTE_DOCUMENTO(P_CLAVE_DOC, P_EMPRESA) THEN
      RAISE_APPLICATION_ERROR(-20005,
                              'El documento ya ha sido anulado por otro usuario!');
    END IF;

    IF V_DOC_CLAVE_PADRE IS NOT NULL AND V_DOC_TIPO_MOV != 12 THEN

      SELECT F.DOC_TIPO_MOV, F.DOC_NRO_DOC
        INTO V_DOC_TIPO_MOV_PADRE, V_NRO_DOC_PADRE
        FROM FIN_DOCUMENTO F
       WHERE F.DOC_EMPR = P_EMPRESA
         AND F.DOC_CLAVE = V_DOC_CLAVE_PADRE;

      RAISE_APPLICATION_ERROR(-20006,
                              'Debe eliminar el documento padre para eliminar este! ' || ' ' ||
                              ' El doc. Padre: ' || ' TM: ' ||
                              V_DOC_TIPO_MOV_PADRE || ' Doc.: ' ||
                              V_NRO_DOC_PADRE);

    END IF;

    PP_VERIF_EXIST_PAGOS(P_CLAVE_DOC, P_EMPRESA); --BUSCAR PAGOS
    --LAS TARJETAS DE CREDITO NO DEBEN ESTAR DEPOSITADAS
    --NI HABER SIDO LIQUIDADAS
    PP_VERIF_TARJETA_CREDITO(P_CLAVE_DOC, P_EMPRESA);

    -- PARA DESASIGNAR CHEQUES DEL DEPOSITO A ANULAR.
    IF V_DOC_TIPO_MOV IN (V_CONF_COD_DEP) THEN
      PP_BORRAR_CHEQUES_ASIGNADOS(P_CLAVE_DOC, P_EMPRESA);
    END IF;

    IF V_DOC_TIPO_MOV IN (V_CONF_RECIBO_PAGO_REC,
                          V_CONF_RECIBO_PAGO_EMIT,
                          V_CONF_RECIBO_CNCR_REC,
                          V_CONF_RECIBO_CNCR_EMIT,
                          V_CONF_RECIBO_CADCLI_EMIT,
                          V_CONF_RECIBO_CADPRO_REC,
                          V_CONF_TMOV_DEVOL_ADEL_CLI,
                          V_CONF_TMOV_DEVOL_ADEL_PRO,
                          V_CONF_TMOV_PAGO_NC,
                          V_CONF_TMOV_PAGO_NC_REC,
                          V_CONF_RECIBO_CAN_FAC_EMIT,
                          V_CONF_RECIBO_CAN_FAC_REC,
                          V_CONF_FACT_CO_EMIT) THEN

      V_INDI_PAGO := 0;

      IF V_DOC_TIPO_MOV = V_CONF_FACT_CO_EMIT THEN
        PP_VERIFICA_PAGO(P_CLAVE_DOC, P_EMPRESA, V_INDI_PAGO); --SI ES TIPO PAGO SE BUSCAR EL DOCUMENTO
        IF V_INDI_PAGO <> 0 THEN
          PP_BORRA_PAGO_CUO(P_CLAVE_DOC, P_EMPRESA);
        END IF;
      END IF;

      PP_BORRAR_PAGO(P_CLAVE_DOC, P_EMPRESA);
    END IF;

    --CUANDO SE ELIMINA UN DOCUMENTO DE LIQUIDACION DE
    --TARJETA,SE DEBE MODIFICAR LA COLUMNA CUP_CLAVE_FIN_LIQUIDACION
    -- EN LA LA TABLA FIN_TC_CUPON
     BEGIN
    UPDATE FIN_TC_CUPON
       SET CUP_CLAVE_FIN_LIQUIDACION = NULL
     WHERE CUP_CLAVE_FIN_LIQUIDACION = P_CLAVE_DOC
       AND CUP_EMPR = P_EMPRESA;
       EXCEPTION
     WHEN NO_DATA_FOUND THEN
       NULL;
     END;

    -- ELIMINA LAS CUOTAS ASOCIADAS AL DOCUMENTO ELIMINADO
    BEGIN
    DELETE FROM FIN_CUOTA
     WHERE CUO_CLAVE_DOC = P_CLAVE_DOC
       AND CUO_EMPR = P_EMPRESA;
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
       NULL;
     END;
    ---ELIMINA LOS REGISTROS DE FACTURAS ESCANEADAS

    IF P_EMPRESA = 1 THEN
      --EN EL FINI006 SOLO APLICABA PARA HILAGRO ENTONCES LE DEJE COMO EMPRESA 1

      DECLARE
        V_NRO_DOC NUMBER;
      BEGIN

        SELECT FAC_CLAVE
          INTO V_NRO_DOC
          FROM COM_FACTURA_REC
         WHERE FAC_CLAVE_DOC_FIN = P_CLAVE_DOC
           AND FAC_EMPR = P_EMPRESA;

        UPDATE COM_FACTURA_REC
           SET FAC_CLAVE_DOC_FIN = NULL
         WHERE FAC_CLAVE = V_NRO_DOC
           AND FAC_EMPR = P_EMPRESA;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20002, SQLERRM);
      END;
      ----------------------------------------------
      ---------------------------------------------

      -- ELIMINA LOS CONCEPTOS ASOCIADOS AL DOCUMENTO ELIMINADO
    BEGIN
      DELETE FROM FIN_DOCUMENTO_FINI002
       WHERE DOC_CLAVE_NTCR = P_CLAVE_DOC
         AND DOC_EMPR = P_EMPRESA;
         EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
    END;
    BEGIN
      DELETE FROM STK_FLETES_REMIS_FINAN T
       WHERE T.FLEREMFIN_FINAN = P_CLAVE_DOC
         AND T.FLEREMFIN_EMPR = P_EMPRESA;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
    END;
    END IF;
    BEGIN
    DELETE FROM FIN_DOC_CONCEPTO
     WHERE DCON_CLAVE_DOC = P_CLAVE_DOC
       AND DCON_EMPR = P_EMPRESA;
       EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
    END;

    -- ELIMINA LOS REGISTROS DE CUENTA CORRIENTES ASOCIADOS
    BEGIN
    DELETE FROM CCS_DOCUMENTO
     WHERE DOC_CLAVE_FIN = P_CLAVE_DOC
       AND DOC_EMPR = P_EMPRESA;
     EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
    END;

    BEGIN
    DELETE FROM FIN_TC_CUPON
     WHERE CUP_CLAVE_FIN = P_CLAVE_DOC
       AND CUP_EMPR = P_EMPRESA;
       EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
    END;
    BEGIN
    DELETE FROM FAC_AVISOS_PRESUSPUESTOS
     WHERE AVPRE_DOC_CLAVE = P_CLAVE_DOC
       AND AVPRE_EMPRESA = P_EMPRESA;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
    END;
    --ELIMINA LOS REGISTROS DE FLETES PAGADOS CON EL DOCUMENTO

    PP_BORRAR_DOC_HIJO(P_CLAVE_DOC, V_DOC_TIPO_MOV, P_EMPRESA);

    --  V_CLAVE_EXT := (P_CLAVE_DOC , P_EMPRESA );
    -- V_CLAVE_STK            := V_DOC_CLAVE_STK  ;
    PP_VERIF_CH_EMIT(P_CLAVE_DOC, P_EMPRESA);
    PP_VERIF_CAN_CHE_EMIT(P_CLAVE_DOC, P_EMPRESA);
    PP_VERIF_CH_REC(P_CLAVE_DOC, P_EMPRESA);

    PP_GUARDAR_DOC_ANULADO(V_DOC_TIPO_MOV,
                           V_DOC_NRO_DOC,
                           P_EMPRESA,
                           V_DOC_SUC,
                           NVL(V_DOC_CLI, V_DOC_PROV),
                           V_DOC_FEC_DOC,
                           V_ANUL_MOTIVO,
                           'ANUL. DOCUMENTO PER. ANT.',
                           V_DOC_TIMBRADO);
    --TM 31-05-2002 ESTA MODIFICACION SE REALIZA PARA QUE
    --EL MOTIVO DE ANULACION SE GRABE EN EL FIN_AUD_DOCUMENTO (ARCHIVO DE AUDITORIA)
    BEGIN
      IF V_DOC_CLAVE_STK IS NOT NULL THEN
        BEGIN
          /*ELIMINAR DETALLES SI HAY ERROR NO SE PROPAGA SOLO SE TRANCA AL BORRAR EL DOCUMENTO PADRE*/
          DELETE STK_DOCUMENTO_DET
           WHERE DETA_CLAVE_DOC = V_DOC_CLAVE_STK
             AND DETA_EMPR = P_EMPRESA;
        EXCEPTION
          WHEN OTHERS THEN
            NULL;
        END;
        BEGIN
          /*BORRAR DOCUMENTO Y PROPAGAR EL ERROR SI EXISTE*/
          DELETE STK_DOCUMENTO
           WHERE DOCU_CLAVE = V_DOC_CLAVE_STK
             AND DOCU_EMPR = P_EMPRESA;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
          WHEN OTHERS THEN
     --       --rollback;
            RAISE_APPLICATION_ERROR(-20002,
                                    'ERROR AL ELIMINAR EL DOCUMENTO DE STOCK, SE REVERTIRA TODO!');
        END;

      END IF;
      /*ELIMINAR DETALLES SI HAY ERROR NO SE PROPAGA SOLO SE TRANCA AL BORRAR EL DOCUMENTO PADRE*/
      BEGIN
        DELETE FAC_DOCUMENTO_DET
         WHERE DET_CLAVE_DOC = P_CLAVE_DOC
           AND DET_EMPR = P_EMPRESA;

        DELETE FIN_DOC_CONCEPTO
         WHERE DCON_CLAVE_DOC = P_CLAVE_DOC
           AND DCON_EMPR = P_EMPRESA;
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;

    END;

    /*ELIMINAR EL DOCUMENTO FINAR DE FIN_DOCUMENTO SI HAY UN ERROR REVERTIR TODO EL CIRCUITO*/
    BEGIN
      DELETE FIN_DOCUMENTO
       WHERE DOC_CLAVE = P_CLAVE_DOC
         AND DOC_EMPR = P_EMPRESA;
    EXCEPTION
      WHEN OTHERS THEN
        --  --rollback;
        RAISE_APPLICATION_ERROR(-20002,
                                'ERROL AL ELIMINAR EL DOCUMENTO DE FINANZAS, SE REVERTIRA TODO!');
    END;

    UPDATE FIN_AUD_DOCUMENTO
       SET ADOC_MOTIVO_ANULACION = V_ANUL_MOTIVO
     WHERE ADOC_CLAVE = P_CLAVE_DOC
       AND ADOC_EMPR = P_EMPRESA;

    IF V_DOC_CLAVE_STK IS NOT NULL THEN
      UPDATE STK_AUD_DOCUMENTO
         SET ADOCU_MOTIVO_ANULACION = V_ANUL_MOTIVO
       WHERE ADOCU_CLAVE = V_DOC_CLAVE_STK
         AND ADOCU_EMPR = P_EMPRESA;
    END IF;

    /*ENVIAR CORREO DE AVISO*/
    BEGIN
      SELECT T.CO_PARA, T.CO_CC, T.CO_CCO
        INTO DESTINATARIO, COPIA, OCULTA
        FROM GEN_CORREOS T
       WHERE T.CO_ACCION = 'Anulacion_Documento';

       IF DESTINATARIO IS NOT NULL THEN
      V_MENSAJE_CORREO := 'Se ha Autorizado la Anulacion del Documento Nro:' ||
                          V_DOC_NRO_DOC || ' Tipo Mov:' || V_DOC_TIPO_MOV ||
                          ' Fecha:' || V_DOC_FEC_DOC;
      SEND_EMAIL(P_SENDER     => 'NOREPLY',
                 P_RECIPIENTS => DESTINATARIO,
                 P_CC         => COPIA,
                 P_BCC        => OCULTA,
                 P_SUBJECT    => 'ANULACION DE DOCUMENTO DEL PERIODO ANTERIOR',
                 P_MESSAGE    => TO_CHAR(V_MENSAJE_CORREO));
       END IF;

    EXCEPTION
      WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,
                                'Error eliminando el documento- ' ||
                                SQLCODE || ' -ERROR- ' || SQLERRM);
    END;
    --COMMIT; --GUARDAR TODA LA ANULACION

  EXCEPTION
    WHEN SALIR THEN
      NULL;
    WHEN OTROSISTEMA THEN

      RAISE_APPLICATION_ERROR(-20010,
                              'El documento fue cargado en otro sistema');
  END;

  PROCEDURE PED_INSER_DOCUMENTO(P_CLAVE_DOC    IN NUMBER,
                                P_EMPRESA      IN NUMBER,
                                P_LOGIN        IN VARCHAR2,
                                P_OBSERVACION  IN VARCHAR2,
                                P_SISTEMA      IN VARCHAR2,
                                P_FECHA_PEDIDO IN DATE) AS
    V_CLAVE_APROB NUMBER;
  BEGIN

    SELECT SEQ_FINAPROESPDOC.NEXTVAL INTO V_CLAVE_APROB FROM DUAL;

    INSERT INTO FIN_APROB_ESP_DOC
      (AED_CLAVE_APROB,
       AED_EMPR,
       AED_FEC_APROBACION,
       AED_LOGIN_APROB,
       AED_OPERACION,
       AED_ESTADO,
       AED_LOGIN_PED,
       AED_OBSERVACION,
       AED_FECHA_PED)
    VALUES
      (V_CLAVE_APROB,
       P_EMPRESA,
       NULL,
       NULL,
       'INSERTAR',
       'P',
       P_LOGIN,
       P_OBSERVACION,
       P_FECHA_PEDIDO);

    INSERT INTO FIN_APROB_ESP_DOC_DET
      (AEDD_CLAVE_APROB, AEDD_EMPR, AEDD_CLAVE_DOC, AEDD_SISTEMA)
    VALUES
      (V_CLAVE_APROB, P_EMPRESA, P_CLAVE_DOC, P_SISTEMA);
    -- COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      --rollback;
      RAISE_APPLICATION_ERROR(-20001, 'Hubo un error en la operacion');
  END;

  PROCEDURE PED_ANUL_DOCUMENTO(P_CLAVE_DOC    IN NUMBER,
                               P_EMPRESA      IN NUMBER,
                               P_LOGIN        IN VARCHAR2,
                               P_OBSERVACION  IN VARCHAR2,
                               P_SISTEMA      IN VARCHAR2,
                               P_MOTIVO_ANUL  IN NUMBER,
                               P_FECHA_PEDIDO IN DATE) AS
    V_CLAVE_APROB  NUMBER;
    V_ESTADO_APROB VARCHAR2(10);
  BEGIN

    BEGIN
      SELECT DISTINCT NVL(AED_ESTADO, 'P')
        INTO V_ESTADO_APROB
        FROM FIN_APROB_ESP_DOC A, FIN_APROB_ESP_DOC_DET B
       WHERE A.AED_EMPR = P_EMPRESA
         AND B.AEDD_CLAVE_DOC = P_CLAVE_DOC
         AND A.AED_CLAVE_APROB = B.AEDD_CLAVE_APROB
         AND A.AED_OPERACION = 'ANULAR'
         AND A.AED_EMPR = B.AEDD_EMPR;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;

    IF V_ESTADO_APROB = 'P' THEN
      RAISE_APPLICATION_ERROR(-20002,
                              'Existe una aprobacion pendiente del Documnento!');
    ELSIF V_ESTADO_APROB = 'T' THEN
      RAISE_APPLICATION_ERROR(-20002, 'Ya se anulo el Documento!');
    ELSE

      SELECT SEQ_FINAPROESPDOC.NEXTVAL INTO V_CLAVE_APROB FROM DUAL;
      INSERT INTO FIN_APROB_ESP_DOC
        (AED_CLAVE_APROB,
         AED_EMPR,
         AED_FEC_APROBACION,
         AED_LOGIN_APROB,
         AED_OPERACION,
         AED_ESTADO,
         AED_LOGIN_PED,
         AED_OBSERVACION,
         AED_FECHA_PED)
      VALUES
        (V_CLAVE_APROB,
         P_EMPRESA,
         NULL,
         NULL,
         'ANULAR',
         'P',
         P_LOGIN,
         P_OBSERVACION,
         P_FECHA_PEDIDO);

      INSERT INTO FIN_APROB_ESP_DOC_DET
        (AEDD_CLAVE_APROB,
         AEDD_EMPR,
         AEDD_CLAVE_DOC,
         AEDD_SISTEMA,
         AEDD_MOTIVO_ANUL)
      VALUES
        (V_CLAVE_APROB, P_EMPRESA, P_CLAVE_DOC, P_SISTEMA, P_MOTIVO_ANUL);
     -- COMMIT;
    END IF;

    --EXCEPTION
    --  WHEN OTHERS THEN
    --    --rollback;
    --   RAISE_APPLICATION_ERROR(-20002, 'Hubo un error en la operacion');
  END;

  FUNCTION FP_EXISTE_DOCUMENTO(P_CLAVE_DOC IN NUMBER, P_EMPRESA IN NUMBER)
    RETURN BOOLEAN IS

    V_CLAVE NUMBER;
  BEGIN
    SELECT DOC_CLAVE
      INTO V_CLAVE
      FROM FIN_DOCUMENTO
     WHERE DOC_CLAVE = P_CLAVE_DOC
       AND DOC_EMPR = P_EMPRESA;

    RETURN TRUE;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN FALSE;
  END;

  PROCEDURE PP_VERIF_EXIST_PAGOS(P_CLAVE_DOC IN NUMBER,
                                 P_EMPRESA   IN NUMBER) IS
    W_CLAVE_DOC NUMBER;
  BEGIN
    SELECT PAG_CLAVE_DOC
      INTO W_CLAVE_DOC
      FROM FIN_PAGO
     WHERE PAG_CLAVE_DOC = P_CLAVE_DOC
       AND PAG_EMPR = P_EMPRESA;

    RAISE_APPLICATION_ERROR(-20002,
                            'Existe/n pago/s asignado/s al documento!');
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
    WHEN TOO_MANY_ROWS THEN

      RAISE_APPLICATION_ERROR(-20002,
                              'Existe/n pago/s asignado/s al documento!');
    WHEN OTHERS THEN

      RAISE_APPLICATION_ERROR(-20002,
                              'Hubo un error no tratado al buscar pagos!');
  END;

  PROCEDURE PP_VERIF_TARJETA_CREDITO(P_CLAVE_DOC IN NUMBER,
                                     P_EMPRESA   IN NUMBER) IS
    V_LOTES VARCHAR2(500) := NULL;
  BEGIN
    --LAS TARJETAS DE CREDITO NO PUEDEN ESTAR DEPOSITADAS
    FOR R IN (SELECT DISTINCT CUP_NRO_LOTE
                FROM FIN_TC_CUPON
               WHERE CUP_CLAVE_FIN = P_CLAVE_DOC
                 AND CUP_EMPR = P_EMPRESA) LOOP
      --SI R.CUP_NRO_LOTE ES NULO ES PORQUE NO SE DEPOSITO NINGUN CUPON
      IF R.CUP_NRO_LOTE IS NOT NULL THEN
        --CONCATENAMOS LOS NUMEROS DE LOTE DE DEPOSITO
        V_LOTES := V_LOTES || R.CUP_NRO_LOTE || ',';
      END IF;
    END LOOP;
    --QUITAMOS LA COMA DE LA ULTIMA POSICION
    IF V_LOTES IS NOT NULL THEN
      V_LOTES := SUBSTR(V_LOTES, 1, LENGTH(V_LOTES) - 1);
      RAISE_APPLICATION_ERROR(-20002,
                              'Primero debe eliminar los siguientes lotes de depositos: ' ||
                              V_LOTES);
    END IF;

    --LAS TARJETAS DE CREDITO NO PUEDEN ESTAR LIQUIDADAS
    FOR R IN (SELECT DISTINCT DOC_TIPO_MOV, DOC_NRO_DOC
                FROM FIN_TC_CUPON, FIN_DOCUMENTO
               WHERE CUP_CLAVE_FIN_LIQUIDACION = DOC_CLAVE
                 AND CUP_CLAVE_FIN = P_CLAVE_DOC
                 AND CUP_EMPR = P_EMPRESA) LOOP
      --EXISTEN CUPONES LIQUIDADOS
      RAISE_APPLICATION_ERROR(-20002,
                              'Primero debe eliminar el siguiente documento de liquidacion de cupones: ' ||
                              'Tipo Movimiento: ' || R.DOC_TIPO_MOV ||
                              'Nro.documento: ' || R.DOC_NRO_DOC);
    END LOOP;

  END;

  PROCEDURE PP_BORRAR_CHEQUES_ASIGNADOS(P_CLAVE_DOC IN NUMBER,
                                        P_EMPRESA   IN NUMBER) IS
  BEGIN
    -- ESTA PROCEDURE HACE QUE LOS CHEQUES ASIGNADOS
    -- AL DEPOSITO A ANULAR NO ESTEN MAS BAJO ESE DEPOSITO
    UPDATE FIN_DOCUMENTO
       SET DOC_NRO_DOC_DEP    = NULL,
           DOC_EST_CHEQUE     = 'I',
           DOC_FEC_DEP_CHEQUE = NULL
     WHERE DOC_NRO_DOC_DEP = P_CLAVE_DOC
       AND DOC_EMPR = P_EMPRESA;
    --
  EXCEPTION

    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20002,
                              'Error al eliminar cheques adelantados!');
  END;

  PROCEDURE PP_VERIFICA_PAGO(P_CLAVE_DOC IN NUMBER,
                             P_EMPRESA   IN NUMBER,
                             P_INDI_PAGO OUT NUMBER) IS
  BEGIN
    SELECT UNIQUE PAG_CLAVE_PAGO
      INTO P_INDI_PAGO
      FROM FIN_PAGO
     WHERE PAG_CLAVE_PAGO = P_CLAVE_DOC
       AND PAG_EMPR = P_EMPRESA;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      P_INDI_PAGO := 0;

  END;

  PROCEDURE PP_BORRA_PAGO_CUO(P_CLAVE_DOC IN NUMBER, P_EMPRESA IN NUMBER) IS
  BEGIN
    DELETE FROM FIN_PAGO
     WHERE PAG_CLAVE_PAGO = P_CLAVE_DOC
       AND PAG_EMPR = P_EMPRESA;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20002, 'Error al borrar pago!' || SQLERRM);
  END;

  PROCEDURE PP_BORRAR_DOC_HIJO(P_CLAVE_DOC IN NUMBER,
                               P_TIPO_MOV  IN NUMBER,
                               P_EMPRESA   IN NUMBER) IS
    V_CLAVE_EXT    NUMBER;
    V_CLAVE_HIJO_2 NUMBER;
    CURSOR CUR_HIJO IS
      SELECT DOC_TIPO_MOV, DOC_CLAVE
        FROM FIN_DOCUMENTO
       WHERE DOC_CLAVE_PADRE = P_CLAVE_DOC
         AND DOC_EMPR = P_EMPRESA;
  BEGIN
    --ELIMINAR EL DOCUMENTO RELACIONADO PORQUE AL HACER UNA
    --CANCELACION DE UN DOCUMENTO SIEMPRE SE GRABAN EN REALIDAD
    --DOS DOCUMENTOS. EL CODIGO DEL DOCUMENTO RELACIONADO ESTA
    --GRABADO EN DOC_CLAVE_PADRE
    -- MESSAGE(:BDOC.DOC_CLAVE);PAUSE;
    FOR RHIJO IN CUR_HIJO LOOP
      IF P_TIPO_MOV = 6 THEN
        IF RHIJO.DOC_TIPO_MOV = 12 THEN
          --BORRAR EL HIJO QUE ES EL PADRE DEL 13
          --  PL_EXHIBIR_MENSAJE('Borrar el hijo del hijo');
          BEGIN
          SELECT DOC_CLAVE
            INTO V_CLAVE_HIJO_2
            FROM FIN_DOCUMENTO
           WHERE DOC_CLAVE_PADRE = RHIJO.DOC_CLAVE
             AND DOC_EMPR = P_EMPRESA;
           EXCEPTION
             WHEN no_data_found THEN
               NULL;
               END;
          DELETE FROM FIN_DOC_CONCEPTO
           WHERE DCON_CLAVE_DOC = V_CLAVE_HIJO_2
             AND DCON_EMPR = P_EMPRESA;

          DELETE FROM FIN_DOCUMENTO
           WHERE DOC_CLAVE = V_CLAVE_HIJO_2
             AND DOC_EMPR = P_EMPRESA;
        END IF;
      END IF;

      DELETE FROM FIN_PAGO
       WHERE PAG_CLAVE_PAGO = RHIJO.DOC_CLAVE
         AND PAG_EMPR = P_EMPRESA;

      BEGIN

        DELETE FROM FIN_PAGO
         WHERE PAG_CLAVE_DOC = RHIJO.DOC_CLAVE
           AND PAG_EMPR = P_EMPRESA;

        DELETE FROM FIN_CUOTA
         WHERE CUO_CLAVE_DOC = RHIJO.DOC_CLAVE
           AND CUO_EMPR = P_EMPRESA; --PARA PRESTAMOS TMOV IN (71,72)
      EXCEPTION
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20002,
                                  'Error al borrar doc hijo!' || SQLERRM);

      END;

      DELETE FROM FIN_DOC_CONCEPTO
       WHERE DCON_CLAVE_DOC = RHIJO.DOC_CLAVE
         AND DCON_EMPR = P_EMPRESA;
      -- V_CLAVE_EXT := RHIJO.DOC_CLAVE;
      PP_ACT_CLAVE_RETENCION(RHIJO.DOC_CLAVE, P_EMPRESA);

      PP_VERIF_CH_EMIT(RHIJO.DOC_CLAVE, P_EMPRESA);

      PP_VERIF_CAN_CHE_EMIT(RHIJO.DOC_CLAVE, P_EMPRESA);
      ----BORRAR CHEQUES DE RESPALDO---------
      DECLARE
      BEGIN

        DELETE FROM FIN_CHEQUE_DOCU_RESP
         WHERE CHDO_CLAVE_DOC = RHIJO.DOC_CLAVE
           AND CHDO_EMPR = P_EMPRESA;
        --MESSAGE('p7');PAUSE;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20002,
                                  'Error no tratado al borrar pago!' ||
                                  SQLERRM);
      END;
      ----BORRAR CHEQUES RECIBIDOS---------
      DECLARE
      BEGIN
        DELETE FROM FIN_CHEQUE_DOCUMENTO
         WHERE CHDO_CLAVE_DOC = RHIJO.DOC_CLAVE
           AND CHDO_EMPR = P_EMPRESA;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20002,
                                  'Error no tratado al Cheque Documento!' ||
                                  SQLERRM);
      END;
      PP_ELIMINAR_CHEQUE_1(P_CLAVE_DOC, P_EMPRESA);
      DECLARE
      BEGIN
        DELETE FROM FIN_DOCUMENTO
         WHERE DOC_CLAVE = RHIJO.DOC_CLAVE
           AND DOC_EMPR = P_EMPRESA;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20002, SQLERRM);

      END;

    END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20002, SQLERRM);

  END;

  PROCEDURE PP_ELIMINAR_CHEQUE_1(P_CLAVE_DOC IN NUMBER,
                                 P_EMPRESA   IN NUMBER) IS
    CURSOR CUR_HIJO IS
      SELECT DOC_CLAVE
        FROM FIN_DOCUMENTO
       WHERE DOC_CLAVE_PADRE = P_CLAVE_DOC
         AND DOC_EMPR = P_EMPRESA;

  BEGIN

    --CASO 2. EN FIN_CHEQUE_DOCUMENTO, ESTA RELACIONADO CON EL PADRE Y AL ANULAR EL MOVIMIENTO, NO SE ANULABAN LOS CHEQUES
    --EL PAQUETE DE CAST, HACIA MAL LA RELACION. POR ESE MOTIVO SE INCLUYE ESTE ESCENARIO.
    DECLARE
      V_NUMERO_DEPOSITO VARCHAR2(50);
      V_FECHA_DEPOSITO  DATE;
      V_DEPOSITO        NUMBER;
      V_CLAVE_CHEQ      VARCHAR2(50);
    BEGIN

      DELETE FROM FIN_CHEQUE_DOCUMENTO
       WHERE CHDO_CLAVE_DOC = P_CLAVE_DOC
         AND CHDO_EMPR = P_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
      WHEN TOO_MANY_ROWS THEN
        --CUANDO UN DOCUMENTO ESTA RELACIONAD CON DOS CHEQUES O MAS.
        FOR C IN (SELECT DISTINCT T.CHDO_CLAVE_CHEQ
                    FROM FIN_CHEQUE_DOCUMENTO T
                   WHERE T.CHDO_CLAVE_DOC = P_CLAVE_DOC
                     AND T.CHDO_EMPR = P_EMPRESA) LOOP

          DELETE FROM FIN_CHEQUE_DOCUMENTO
           WHERE CHDO_CLAVE_CHEQ = C.CHDO_CLAVE_CHEQ
             AND CHDO_EMPR = P_EMPRESA;
        END LOOP;
      WHEN OTHERS THEN
        --DISPLAY_ERROR;
        --PL_EXHIBIR_ERROR_PLSQL;

        SELECT DISTINCT T.CHDO_CLAVE_CHEQ
          INTO V_CLAVE_CHEQ
          FROM FIN_CHEQUE_DOCUMENTO T
         WHERE T.CHDO_CLAVE_DOC = P_CLAVE_DOC
           AND T.CHDO_EMPR = P_EMPRESA;

        DELETE FROM FIN_CHEQUE_DOCUMENTO
         WHERE CHDO_CLAVE_CHEQ = V_CLAVE_CHEQ
           AND CHDO_EMPR = P_EMPRESA;
    END;
    --COMMIT;

    --ELIMINAR EL DOCUMENTO RELACIONADO PORQUE AL HACER UNA
    --CANCELACION DE UN DOCUMENTO SIEMPRE SE GRABAN EN REALIDAD
    --DOS DOCUMENTOS. EL CODIGO DEL DOCUMENTO RELACIONADO ESTA
    --GRABADO EN DOC_CLAVE_PADRE
    -- MESSAGE(:BDOC.DOC_CLAVE);PAUSE;
    -- PL_EXHIBIR_MENSAJE('Clave padre uno'||:BDOC.DOC_CLAVE);
    FOR RHIJO IN CUR_HIJO LOOP
      --  MESSAGE('p1');PAUSE;
      DELETE FROM FIN_PAGO
       WHERE PAG_CLAVE_PAGO = RHIJO.DOC_CLAVE
         AND PAG_EMPR = P_EMPRESA;
      --MESSAGE('p2');PAUSE;

      BEGIN

        DELETE FROM FIN_PAGO
         WHERE PAG_CLAVE_DOC = RHIJO.DOC_CLAVE
           AND PAG_EMPR = P_EMPRESA;

        DELETE FROM FIN_CUOTA
         WHERE CUO_CLAVE_DOC = RHIJO.DOC_CLAVE
           AND CUO_EMPR = P_EMPRESA; --PARA PRESTAMOS TMOV IN (71,72)

      EXCEPTION
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20002,
                                  'Error borrando fin_cuota, fin_pago en cheque_1' ||
                                  SQLERRM);
          --DISPLAY_ERROR;

      END;

      DELETE FROM FIN_DOC_CONCEPTO
       WHERE DCON_CLAVE_DOC = RHIJO.DOC_CLAVE
         AND DCON_EMPR = P_EMPRESA;
      --MESSAGE('p3');PAUSE;
      --:PARAMETER.P_CLAVE_EXT := RHIJO.DOC_CLAVE;
      PP_ACT_CLAVE_RETENCION(RHIJO.DOC_CLAVE, P_EMPRESA);

      --MESSAGE('p4');PAUSE;
      --PP_VERIF_CH_EMIT;
      --MESSAGE('p5');PAUSE;
      -- PP_VERIF_CAN_CHE_EMIT;
      --MESSAGE('p6');PAUSE;
      ----BORRAR CHEQUES DE RESPALDO---------
      --CASO 1
      PP_VERIF_CH_EMIT(P_CLAVE_DOC, P_EMPRESA);
      PP_VERIF_CAN_CHE_EMIT(P_CLAVE_DOC, P_EMPRESA);
      DECLARE
      BEGIN

        DELETE FROM FIN_CHEQUE_DOCU_RESP
         WHERE CHDO_CLAVE_DOC = RHIJO.DOC_CLAVE
           AND CHDO_EMPR = P_EMPRESA;
        --MESSAGE('p7');PAUSE;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20002,
                                  'Error borrando FIN_CHEQUE_DOCU_RESP en cheque_1' ||
                                  SQLERRM);
          --DISPLAY_ERROR;
      END;

      --CASO 2. EN FIN_CHEQUE_DOCU_RESP, ESTA RELACIONADO CON EL PADRE Y AL ANULAR EL MOVIMIENTO, NO SE ANULABAN LOS CHEQUES
      --EL PAQUETE DE CAST, HACIA MAL LA RELACION. POR ESE MOTIVO SE INCLUYE ESTE ESCENARIO.
      ----BORRAR CHEQUES DE RESPALDO---------
      DECLARE
      BEGIN

        DELETE FROM FIN_CHEQUE_DOCU_RESP
         WHERE CHDO_CLAVE_DOC = P_CLAVE_DOC --CLAVE
           AND CHDO_EMPR = P_EMPRESA;
        --MESSAGE('p7');PAUSE;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20002,
                                  'Error borrando FIN_CHEQUE_DOCU_RESP en cheque_1' ||
                                  SQLERRM);
          --DISPLAY_ERROR;
      END;

      ----BORRAR CHEQUES RECIBIDOS  PERO PRIMERO PREGUNTAR, SI YA FUERON DEPOSITADOS O NO---------
      --CASO 1
      DECLARE
        V_NUMERO_DEPOSITO VARCHAR2(50);
        V_FECHA_DEPOSITO  DATE;
        V_DEPOSITO        NUMBER;
      BEGIN
        DELETE FROM FIN_CHEQUE_DOCUMENTO
         WHERE CHDO_CLAVE_DOC = RHIJO.DOC_CLAVE
           AND CHDO_EMPR = P_EMPRESA;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20002,
                                  'Error borrando FIN_CHEQUE_DOCUMENTO en cheque_1' ||
                                  SQLERRM);
          --DISPLAY_ERROR;
      END;

      --CASO 2. EN FIN_CHEQUE_DOCUMENTO, ESTA RELACIONADO CON EL PADRE Y AL ANULAR EL MOVIMIENTO, NO SE ANULABAN LOS CHEQUES
      --EL PAQUETE DE CAST, HACIA MAL LA RELACION. POR ESE MOTIVO SE INCLUYE ESTE ESCENARIO.
      DECLARE
        V_NUMERO_DEPOSITO VARCHAR2(50);
        V_FECHA_DEPOSITO  DATE;
        V_DEPOSITO        NUMBER;
      BEGIN
        DELETE FROM FIN_CHEQUE_DOCUMENTO
         WHERE CHDO_CLAVE_DOC = P_CLAVE_DOC
           AND CHDO_EMPR = P_EMPRESA;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20002,
                                  'Error borrando FIN_CHEQUE_DOCUMENTO2 en cheque_1' ||
                                  SQLERRM);
          --DISPLAY_ERROR;
      END;
      --MESSAGE('p8');PAUSE;

      DECLARE
      BEGIN
        DELETE FROM FIN_DOCUMENTO
         WHERE DOC_CLAVE = RHIJO.DOC_CLAVE
           AND DOC_EMPR = P_EMPRESA;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
        WHEN OTHERS THEN
          --PL_EXHIBIR_ERROR_PLSQL;
          --DISPLAY_ERROR;
          RAISE_APPLICATION_ERROR(-20002,
                                  'Error borrando fin_documento en cheque_1' ||
                                  SQLERRM);
      END;
      --MESSAGE('p9');PAUSE;

    END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20002, '-1' || SQLERRM);

  END;

  PROCEDURE PP_ACT_CLAVE_RETENCION(P_CLAVE_DOC IN NUMBER,
                                   P_EMPRESA   IN NUMBER) IS
  BEGIN
    UPDATE FIN_DOCUMENTO
       SET DOC_CLAVE_RETENCION = NULL
     WHERE DOC_CLAVE_RETENCION = P_CLAVE_DOC
       AND DOC_EMPR = P_EMPRESA;
    UPDATE FIN_CUOTA
       SET CUO_CLAVE_RETENCION = NULL
     WHERE CUO_CLAVE_RETENCION = P_CLAVE_DOC
       AND CUO_EMPR = P_EMPRESA;
    --COMMIT;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
  END;

  PROCEDURE PP_VERIF_CH_EMIT(P_CLAVE_DOC IN NUMBER, P_EMPRESA IN NUMBER) IS
    W_CLAVE_FIN_CAN NUMBER;
    W_CLAVE_FIN     NUMBER;
  BEGIN
    SELECT DISTINCT CH_EMIT_CLAVE_FIN_CAN, CH_EMIT_CLAVE_FIN
      INTO W_CLAVE_FIN_CAN, W_CLAVE_FIN
      FROM FIN_CHEQUE_EMIT
     WHERE CH_EMIT_CLAVE_FIN = P_CLAVE_DOC
       AND CH_EMIT_CLAVE_FIN_CAN IS NOT NULL
       AND CH_EMIT_EMPR = P_EMPRESA;

    IF W_CLAVE_FIN_CAN = W_CLAVE_FIN THEN
      --SI LAS CLAVES SON IGUALES ENTONCES EL DOCUMENTO ES SOBRE
      --UN BANCO CHEQUE DIA Y NO EXISTE DOCUMENTO DE CANCELACION DE CHEQ.DIF.
      DELETE FROM FIN_CHEQUE_EMIT WHERE CH_EMIT_CLAVE_FIN = P_CLAVE_DOC;
    ELSE
      RAISE_APPLICATION_ERROR(-20002,
                              'Debe anular primero las cancelaciones imputadas a la Extraccion!.'
                              --  EL ULTIMO NUMERO DE CANCELACION DE ESTA CUENTA = '||:PARAMETER.P_ULT_NRO_CAN
                              );
    END IF;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      DELETE FROM FIN_CHEQUE_EMIT
       WHERE CH_EMIT_CLAVE_FIN = P_CLAVE_DOC
         AND CH_EMIT_EMPR = P_EMPRESA;
    WHEN TOO_MANY_ROWS THEN

      IF W_CLAVE_FIN_CAN = W_CLAVE_FIN THEN
        DELETE FROM FIN_CHEQUE_EMIT
         WHERE CH_EMIT_CLAVE_FIN = P_CLAVE_DOC
           AND CH_EMIT_EMPR = P_EMPRESA;
      ELSE
        RAISE_APPLICATION_ERROR(-20002,
                                'DEBE ANULAR PRIMERO LAS CANCELACIONES IMPUTADAS A LA EXTRACCION!.'
                                --El ultimo numero de cancelacion de esta cta. es ='||:PARAMETER.P_ULT_NRO_CAN
                                );
      END IF;

    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20002, SQLERRM);
  END;

  PROCEDURE PP_VERIF_CAN_CHE_EMIT(P_CLAVE_DOC IN NUMBER,
                                  P_EMPRESA   IN NUMBER) IS
    W_CLAVE_FIN_CAN NUMBER;
    W_CLAVE_FIN     NUMBER;
  BEGIN

    SELECT DISTINCT CH_EMIT_CLAVE_FIN_CAN
      INTO W_CLAVE_FIN_CAN
      FROM FIN_CHEQUE_EMIT
     WHERE CH_EMIT_CLAVE_FIN_CAN = P_CLAVE_DOC
       AND CH_EMIT_EMPR = P_EMPRESA;

    --SI EL DOCUMENTO QUE SE ANULA ES EL DOCUMENTO DE CANCELACION DE CHEQUE
    UPDATE FIN_CHEQUE_EMIT
       SET CH_EMIT_CLAVE_FIN_CAN = NULL
     WHERE CH_EMIT_CLAVE_FIN_CAN = P_CLAVE_DOC
       AND CH_EMIT_EMPR = P_EMPRESA;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20002, SQLERRM);
  END;

  PROCEDURE PP_GUARDAR_DOC_ANULADO(P_DOC_TIPO_MOV IN NUMBER,
                                   P_DOC_NRO_DOC  IN NUMBER,
                                   P_EMPRESA      IN NUMBER,
                                   P_DOC_SUC      IN NUMBER,
                                   P_CUENTA       IN NUMBER,
                                   P_DOC_FEC_DOC  IN DATE,
                                   P_ANUL_MOTIVO  IN NUMBER,
                                   P_ANUL_OBS     IN VARCHAR2,
                                   P_DOC_TIMBRADO IN NUMBER) IS
  BEGIN

    INSERT INTO FIN_DOC_ANULADO
      (ANUL_TIPO_MOV,
       ANUL_NRO_DOC,
       ANUL_EMPR,
       ANUL_SUC,
       ANUL_CLI_PROV,
       ANUL_FEC_DOC,
       ANUL_MOTIVO,
       ANUL_OBS,
       ANUL_NRO_TIMBRADO)
    VALUES
      (P_DOC_TIPO_MOV,
       P_DOC_NRO_DOC,
       P_EMPRESA,
       P_DOC_SUC,
       P_CUENTA,
       P_DOC_FEC_DOC,
       P_ANUL_MOTIVO,
       P_ANUL_OBS,
       P_DOC_TIMBRADO);

  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20002, SQLERRM);
  END;

  PROCEDURE PP_BORRAR_PAGO(P_CLAVE_DOC IN NUMBER, P_EMPRESA IN NUMBER) IS
  BEGIN
    -- ESTE PROCEDIMIENTO ELIMINA LOS
    -- REGISTROS DE LA TABLA FIN_PAGO

    DELETE FROM FIN_PAGO
     WHERE PAG_CLAVE_PAGO = P_CLAVE_DOC
       AND PAG_EMPR = P_EMPRESA;
    --
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20002, SQLERRM);
  END;

  PROCEDURE PP_VERIF_CH_REC(P_CLAVE_DOC IN NUMBER, P_EMPRESA IN NUMBER) IS
    WMAXCLAVE    NUMBER;
    WCLAVECHEQUE NUMBER;
    SALIR EXCEPTION;
    V_CLAVE_CHEQUE NUMBER;
    --PARA BORRAR LA NOTA DE DEPOSITO DETALLE
    V_CLAVE_NOTA_DEP NUMBER;
    V_CLAVE_NOTA_AUX NUMBER;
  BEGIN
    --VALIDAR SI TODOS LOS CHEQUES REALCIONADOS AL DOCUMENTO
    -- NO TIENEN OPERACIONES POSTERIORES
    SELECT MAX(CHDO_CLAVE_DOC)
      INTO WMAXCLAVE
      FROM FIN_CHEQUE_DOCUMENTO
     WHERE CHDO_CLAVE_CHEQ IN
           (SELECT CHDO_CLAVE_CHEQ
              FROM FIN_CHEQUE_DOCUMENTO
             WHERE CHDO_CLAVE_DOC = P_CLAVE_DOC
               AND CHDO_EMPR = P_EMPRESA)
       AND CHDO_EMPR = P_EMPRESA;
    --
    IF WMAXCLAVE IS NULL THEN
      RAISE SALIR;
    ELSE
      IF WMAXCLAVE > P_CLAVE_DOC THEN
        /*QUIERE DECIR QUE EXISTEN OPERACIONES POSTERIORES RELACIONADAS A ALGUN CHEQUE DEL DOCUMENTO*/
        RAISE_APPLICATION_ERROR(-20002,
                                'Existen cheques pertenecientes a esta operacion que han tenido movimientos posteriores.');
      END IF;
      --PRIMERO SE BORRA LA NOTA DE DEPOSITO Y DESPUES EL CHEQUE DOCUMENTO

      /*=======================================================*/
      --SE LE AGREGO PARA QUE BORRE LA NOTA DE DEPOSITO QUE SON DEL PROCESO VIEJO
      --TAMBIEN PARA QUE ACTUALIZE LOS CHEQUES A SUS ESTADOS ANTERIORES.

      /*=======================================================*/
      --DEPOSITOS O EXTRACCIONES QUE TENGA RELACION CON CHEQUES

      DELETE FIN_CHEQUE_DOCUMENTO
       WHERE CHDO_CLAVE_DOC = P_CLAVE_DOC
         AND CHDO_EMPR = P_EMPRESA;
      --DEPOSITOS O EXTRACCIONES QUE TENGA RELACION CON CHEQUES, PROCESO ESPECIAL

      DELETE FIN_CHEQUE_DOCU_RESP
       WHERE CHDO_CLAVE_DOC = P_CLAVE_DOC
         AND CHDO_EMPR = P_EMPRESA;

    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      DELETE FIN_CHEQUE_DOCUMENTO
       WHERE CHDO_CLAVE_DOC = P_CLAVE_DOC
         AND CHDO_EMPR = P_EMPRESA;
      --DEPOSITOS O EXTRACCIONES QUE TENGA RELACION CON CHEQUES
      DELETE FIN_CHEQUE_DOCU_RESP
       WHERE CHDO_CLAVE_DOC = P_CLAVE_DOC
         AND CHDO_EMPR = P_EMPRESA;

    WHEN SALIR THEN
      NULL;
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20002, SQLERRM);
  END;

  PROCEDURE PP_HABILITAR_SELECION(P_EMPRESA IN NUMBER, P_LOGIN IN VARCHAR) IS
    V_IND_DETALLE VARCHAR2(2) := 'N';
  BEGIN

    FOR I IN 1 .. APEX_APPLICATION.G_F01.COUNT LOOP
      V_IND_DETALLE := 'S';
      BEGIN
        UPDATE FIN_APROB_ESP_DOC A
           SET A.AED_FEC_APROBACION = SYSDATE,
               A.AED_LOGIN_APROB    = P_LOGIN,
               A.AED_ESTADO         = 'T'
         WHERE A.AED_EMPR = P_EMPRESA
           AND A.AED_CLAVE_APROB = APEX_APPLICATION.G_F01(I);
      --  COMMIT;
      EXCEPTION
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,
                                  'An error was encountered - ' || SQLCODE ||
                                  ' -ERROR- ' || SQLERRM); --NO PROPAGABA EL ERROR...
          --rollback; --SI HAY UN ERROR EN ALGUN COMPROBANTE HACER R--rollback DEL COMPROBANTE LOS DEMAS SE GUARDAN
        --NULL;
      END;
    END LOOP;

    IF V_IND_DETALLE != 'S' THEN
      RAISE_APPLICATION_ERROR(-20343,
                              'No se ha seleccionado ningun registro de la lista!');
    END IF;
--    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20343, SQLERRM);
  END;

  PROCEDURE PED_ANUL_DOCUMENTO_FAC(P_CLAVE_DOC       IN NUMBER,
                                   P_EMPRESA         IN NUMBER,
                                   P_LOGIN           IN VARCHAR2,
                                   P_OBSERVACION     IN VARCHAR2,
                                   P_SISTEMA         IN VARCHAR2,
                                   P_MOTIVO_ANUL     IN NUMBER,
                                   P_FECHA_PEDIDO    IN DATE,
                                   P_SUCURSAL        IN NUMBER,
                                   P_DEPOSITO        IN NUMBER,
                                   P_FECHA_OPERACION IN DATE) AS
    V_CLAVE_APROB  NUMBER;
    V_ESTADO_APROB VARCHAR2(10);
  BEGIN

    BEGIN
      SELECT DISTINCT NVL(AED_ESTADO, 'P')
        INTO V_ESTADO_APROB
        FROM FIN_APROB_ESP_DOC A, FIN_APROB_ESP_DOC_DET B
       WHERE A.AED_EMPR = P_EMPRESA
         AND B.AEDD_CLAVE_DOC = P_CLAVE_DOC
         AND A.AED_CLAVE_APROB = B.AEDD_CLAVE_APROB
         AND A.AED_EMPR = B.AEDD_EMPR;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;

    IF V_ESTADO_APROB = 'P' THEN
      RAISE_APPLICATION_ERROR(-20002,
                              'Existe una aprobacion pendiente del Documento!');
    ELSIF V_ESTADO_APROB = 'T' THEN
      RAISE_APPLICATION_ERROR(-20002, 'Ya se anulo el Documento!');
    ELSE

      SELECT SEQ_FINAPROESPDOC.NEXTVAL INTO V_CLAVE_APROB FROM DUAL;

      INSERT INTO FIN_APROB_ESP_DOC
        (AED_CLAVE_APROB,
         AED_EMPR,
         AED_FEC_APROBACION,
         AED_LOGIN_APROB,
         AED_OPERACION,
         AED_ESTADO,
         AED_LOGIN_PED,
         AED_OBSERVACION,
         AED_FECHA_PED)
      VALUES
        (V_CLAVE_APROB,
         P_EMPRESA,
         NULL,
         NULL,
         'ANULAR',
         'P',
         P_LOGIN,
         P_OBSERVACION,
         P_FECHA_PEDIDO);

      INSERT INTO FIN_APROB_ESP_DOC_DET B
        (AEDD_CLAVE_APROB,
         AEDD_EMPR,
         AEDD_CLAVE_DOC,
         AEDD_SISTEMA,
         AEDD_MOTIVO_ANUL,
         AEDD_DEPOSITO,
         AEDD_OBSERVACION_ANUL,
         AEDD_FECHA_OPER,
         AEDD_SUCURSAL)
      VALUES
        (V_CLAVE_APROB,
         P_EMPRESA,
         P_CLAVE_DOC,
         P_SISTEMA,
         P_MOTIVO_ANUL,
         P_DEPOSITO,
         P_OBSERVACION,
         P_FECHA_OPERACION,
         P_SUCURSAL);

      ---COMMIT;
    END IF;

    --EXCEPTION
    --  WHEN OTHERS THEN
    --    --rollback;
    --   RAISE_APPLICATION_ERROR(-20002, 'Hubo un error en la operacion');
  END;

  PROCEDURE PP_ANULAR_DOCUMENTO_STK(P_CLAVE_DOC        IN NUMBER,
                                    P_EMPRESA          IN NUMBER,
                                    P_MOTIVO_ANULACION IN NUMBER) AS
  BEGIN

    BEGIN

      PP_BORRAR_DOCUMENTOS_HIJO_STK(P_CLAVE_DOC, P_EMPRESA);

      BEGIN
        DELETE STK_FLETES_REMIS_FINAN T
         WHERE T.FLEREMFIN_REMISION = P_CLAVE_DOC
           AND T.FLEREMFIN_EMPR = P_EMPRESA;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
      END;

      --======================BORRRAR FLETE======================================
      DECLARE
        V_NUM_DOC NUMBER;
      BEGIN
        SELECT COUNT(*)
          INTO V_NUM_DOC
          FROM FIN_DOCUMENTO D
         WHERE D.DOC_TIPO_MOV = 83
           AND D.DOC_CLAVE_STK_FLETE = P_CLAVE_DOC
           AND DOC_EMPR = P_EMPRESA;

        IF V_NUM_DOC > 0 THEN

          DELETE FROM FIN_DOC_CONCEPTO C
           WHERE C.DCON_CLAVE_DOC =
                 (SELECT DC.DOC_CLAVE
                    FROM FIN_DOCUMENTO DC
                   WHERE DC.DOC_TIPO_MOV = 83
                     AND DC.DOC_CLAVE_STK_FLETE = P_CLAVE_DOC
                     AND DOC_EMPR = P_EMPRESA)
             AND DCON_EMPR = P_EMPRESA;

          DELETE FROM FIN_DOCUMENTO
           WHERE DOC_TIPO_MOV = 83
             AND DOC_CLAVE_STK_FLETE = P_CLAVE_DOC
             AND DOC_EMPR = P_EMPRESA;
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
        WHEN OTHERS THEN
          NULL;
      END;

      ----=================================================================================
      BEGIN

        PP_CAMBIAR_ESTADO_TRASLADO_STK(P_CLAVE_DOC, P_EMPRESA); --CAMBIAR ESTADO TRASLADO
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;

      DELETE STK_DOCUMENTO_DET
       WHERE DETA_CLAVE_DOC = P_CLAVE_DOC
         AND DETA_EMPR = P_EMPRESA;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;

    BEGIN
      /*BORRAR DOCUMENTO Y PROPAGAR EL ERROR SI EXISTE*/
      DELETE STK_DOCUMENTO
       WHERE DOCU_CLAVE = P_CLAVE_DOC
         AND DOCU_EMPR = P_EMPRESA;

    END;

  END PP_ANULAR_DOCUMENTO_STK;

  PROCEDURE PP_BORRAR_DOCUMENTOS_HIJO_STK(P_CLAVE_DOC IN NUMBER,
                                          P_EMPRESA   IN NUMBER) IS
    V_CLAVE NUMBER;
  BEGIN
    SELECT DOCU_CLAVE
      INTO V_CLAVE
      FROM STK_DOCUMENTO
     WHERE DOCU_CLAVE_PADRE = P_CLAVE_DOC
       AND DOCU_EMPR = P_EMPRESA;

    DELETE FROM STK_PED_CANC
     WHERE PECA_CLAVE_DOC = V_CLAVE
       AND PECA_EMPR = P_EMPRESA;

    DELETE FROM STK_DOCUMENTO_DET
     WHERE DETA_CLAVE_DOC = V_CLAVE
       AND DETA_EMPR = P_EMPRESA;

    DELETE FROM STK_DOCUMENTO
     WHERE DOCU_CLAVE = V_CLAVE
       AND DOCU_EMPR = P_EMPRESA;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
    WHEN TOO_MANY_ROWS THEN
      RAISE_APPLICATION_ERROR(-20002,
                              'Documento  a eliminar tiene mas de un documento hijo !');

  END;

  PROCEDURE PP_CAMBIAR_ESTADO_TRASLADO_STK(P_CLAVE_DOC IN NUMBER,
                                           P_EMPRESA   IN NUMBER) IS
    V_CLAVE_PADRE NUMBER;
    ----------ESTO ES PARA QUE VUELVA A ESTADO PENDIENTE UNA VEZ ANULADO EL TRASLADO
  BEGIN

    SELECT DOCU_CLAVE_PADRE
      INTO V_CLAVE_PADRE
      FROM STK_DOCUMENTO
     WHERE DOCU_CLAVE = P_CLAVE_DOC
       AND DOCU_EMPR = P_EMPRESA;

    IF V_CLAVE_PADRE IS NOT NULL THEN
      UPDATE STK_DOCUMENTO
         SET DOCU_EST_TRASLADO = 'P'
       WHERE DOCU_CLAVE = V_CLAVE_PADRE
         AND DOCU_EMPR = P_EMPRESA;
    END IF;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
  END PP_CAMBIAR_ESTADO_TRASLADO_STK;

  PROCEDURE PP_VERIFICAR_MOD_PER_ANT(P_EMPRESA IN NUMBER  DEFAULT 1 ) IS
    V_CANT       NUMBER;
    V_TEXTO      VARCHAR2(600);
    DESTINATARIO VARCHAR2(600);
    COPIA        VARCHAR2(600);
    OCULTA       VARCHAR2(150);
    ----------ESTO ES PARA QUE VUELVA A ESTADO PENDIENTE UNA VEZ ANULADO EL TRASLADO
  BEGIN
    SELECT COUNT(*)
      INTO V_CANT
      FROM FIN_AUD_DOC_V A, FIN_CONFIGURACION B
     WHERE EMPRESA = 1
       AND EVENTO <> 'MODIFICACION DE NRO DE DOCUMENTO'
       AND TRUNC(A.FECHA_GRABACION) >= TO_DATE(SYSDATE, 'dd/mm/yyyy') - 1
       AND EMPRESA = CONF_EMPR
       AND TRUNC(A.FECHA_GRABACION) >
           B.CONF_PER_ACT_FIN + B.CONF_DIAS_CIERRE_AUX;

    SELECT T.CO_PARA, T.CO_CC, T.CO_CCO
      INTO DESTINATARIO, COPIA, OCULTA
      FROM GEN_CORREOS T
     WHERE T.CO_ACCION = 'Mod_Per_Ant';

    IF V_CANT > 1 THEN
      V_TEXTO := 'Existen ' || V_CANT ||
                 ' modificaciones del periodo Anterior en los ultimos 2 dias, el detalle lo puede visualizar en el programa (2-4-380) en Apex';
    ELSIF V_CANT = 1 THEN
      V_TEXTO := 'Existe ' || V_CANT ||
                 ' modificacion del periodo Anterior en los ultimos 2 dias, el detalle lo puede visualizar en el programa (2-4-380) en Apex';

    END IF;


    IF V_CANT > 0 THEN
    IF DESTINATARIO IS NOT NULL THEN
        SEND_EMAIL(P_SENDER     => 'NOREPLY',
                   P_RECIPIENTS => DESTINATARIO,
                   P_CC         => COPIA,
                   P_BCC        => OCULTA,
                   P_SUBJECT    => 'MODIFICACIONES DEL PERIODO ANTERIOR',
                   P_MESSAGE    => V_TEXTO);
     END IF;
     END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
  END;

  PROCEDURE PED_ACTUALIZACION_DOCUMENTO(P_CLAVE_DOC       IN NUMBER,
                                        P_EMPRESA         IN NUMBER,
                                        P_LOGIN           IN VARCHAR2,
                                        P_OBSERVACION     IN VARCHAR2,
                                        P_SISTEMA         IN VARCHAR2,
                                        P_MOTIVO_ANUL     IN NUMBER,
                                        P_FECHA_PEDIDO    IN DATE,
                                        P_OPERACION       IN VARCHAR2,
                                        P_FECHA_DOC       IN DATE,
                                        P_NRO_TIMBRADO    IN NUMBER,
                                        P_CANAL_DOC       IN NUMBER,
                                        P_CLAVE_CTO       IN NUMBER,
                                        P_CLAVE_CTACO     IN NUMBER,
                                        P_DCON_CANAL      IN NUMBER,
                                        P_DCON_NRO_ITEM   IN NUMBER,
                                        P_NRO_IMPORTACION IN NUMBER,
                                        P_NRO_DOCUMENTO   IN NUMBER,
                                        P_DCON_OBS        IN VARCHAR2,
                                        P_DCON_TIPO_IVA   IN NUMBER DEFAULT NULL) AS
    V_CLAVE_APROB  NUMBER;
    V_ESTADO_APROB VARCHAR2(10);
  BEGIN

    BEGIN
      SELECT DISTINCT NVL(AED_ESTADO, 'P')
        INTO V_ESTADO_APROB
        FROM FIN_APROB_ESP_DOC A, FIN_APROB_ESP_DOC_DET B
       WHERE A.AED_EMPR = P_EMPRESA
         AND B.AEDD_CLAVE_DOC = P_CLAVE_DOC
         AND A.AED_CLAVE_APROB = B.AEDD_CLAVE_APROB
         AND A.AED_OPERACION = P_OPERACION
         AND A.AED_EMPR = B.AEDD_EMPR;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;

    -- RAISE_APPLICATION_ERROR(-20002,P_OPERACION);

    IF V_ESTADO_APROB = 'P' AND P_OPERACION != 'ACTUALIZAR CONCEPTO' THEN
      RAISE_APPLICATION_ERROR(-20002,
                              'Existe una aprobacion pendiente del Documento!');

    ELSE
   -- RAISE_APPLICATION_ERROR(-20002, P_CLAVE_CTO);


      SELECT SEQ_FINAPROESPDOC.NEXTVAL INTO V_CLAVE_APROB FROM DUAL;
      INSERT INTO FIN_APROB_ESP_DOC
        (AED_CLAVE_APROB,
         AED_EMPR,
         AED_FEC_APROBACION,
         AED_LOGIN_APROB,
         AED_OPERACION,
         AED_ESTADO,
         AED_LOGIN_PED,
         AED_OBSERVACION,
         AED_FECHA_PED)
      VALUES
        (V_CLAVE_APROB,
         P_EMPRESA,
         NULL,
         NULL,
         P_OPERACION,
         'P',
         P_LOGIN,
         P_OBSERVACION,
         P_FECHA_PEDIDO);

      INSERT INTO FIN_APROB_ESP_DOC_DET
        (AEDD_CLAVE_APROB,
         AEDD_EMPR,
         AEDD_CLAVE_DOC,
         AEDD_SISTEMA,
         AEDD_MOTIVO_ANUL,
         AEDD_DEPOSITO,
         AEDD_OBSERVACION_ANUL,
         AEDD_FECHA_OPER,
         AEDD_SUCURSAL,
         AEDD_NRO_ITEM,
         AEDD_CONCEPTO,
         AEDD_CTA_CONT,
         AEDD_DCON_CANAL,
         AEDD_IMPORTACION,
         AEDD_DCON_OBS,
         AEDD_DOC_NRO_DOC,
         AEDD_DOC_NRO_TIMB,
         AEDD_DOC_FEC_DOC,
         AEDD_DOC_CANAL,
         AEDD_DCON_TIPO_IVA)
  SELECT
         V_CLAVE_APROB,
         P_EMPRESA,
         P_CLAVE_DOC,
         P_SISTEMA,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         C004 DCON_ITEM,
         C001 DCON_CLAVE_CONCEPTO,
         C002 CUENTA,
         C003 DCON_CANAL,
         C005 NRO_IMPORTACION,
         C007 DCON_OBS,
         P_NRO_DOCUMENTO,
         P_NRO_TIMBRADO,
         P_FECHA_DOC,
         P_CANAL_DOC,
         P_DCON_TIPO_IVA
    FROM APEX_COLLECTIONS
    WHERE COLLECTION_NAME = 'CONCEPTOS' ;


      /*
      VALUES
        (V_CLAVE_APROB,
         P_EMPRESA,
         P_CLAVE_DOC,
         P_SISTEMA,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         P_DCON_NRO_ITEM,
         P_CLAVE_CTO,
         P_CLAVE_CTACO,
         P_DCON_CANAL,
         P_NRO_IMPORTACION,
         P_DCON_OBS,
         P_NRO_DOCUMENTO,
         P_NRO_TIMBRADO,
         P_FECHA_DOC,
         P_CANAL_DOC,
         P_DCON_TIPO_IVA);*/
  --    COMMIT;
    END IF;

    --EXCEPTION
    --  WHEN OTHERS THEN
    --    --rollback;
    --   RAISE_APPLICATION_ERROR(-20002, 'Hubo un error en la operacion');
  END;

  PROCEDURE PP_MODIFICAR_CONCEPTO(P_EMPRESA       IN NUMBER,
                                  P_OPERACION     IN VARCHAR2,
                                  P_COD_SOLICITUD IN NUMBER) IS
    V_AEDD_CLAVE_DOC     NUMBER;
    V_AEDD_NRO_ITEM      NUMBER;
    V_AEDD_CONCEPTO      NUMBER;
    V_AEDD_CTA_CONT      NUMBER;
    V_AEDD_DCON_CANAL    NUMBER;
    V_AEDD_IMPORTACION   NUMBER;
    V_AEDD_DCON_OBS      VARCHAR2(200);
    V_AEDD_DOC_NRO_DOC   NUMBER;
    V_AEDD_DOC_NRO_TIMB  NUMBER;
    V_AEDD_DOC_FEC_DOC   DATE;
    V_AEDD_DOC_CANAL     NUMBER;
    V_AEDD_DCON_TIPO_IVA NUMBER;
    V_MODIFICACION_AUX   VARCHAR2(600);

  CURSOR V_CONCEPTO  IS  (SELECT NVL(T.AEDD_NRO_ITEM,0) NRO_ITEM , AEDD_CLAVE_APROB, T.AEDD_EMPR
                            FROM FIN_APROB_ESP_DOC_DET T
                            WHERE T.AEDD_CLAVE_APROB =  P_COD_SOLICITUD
                            AND   T.AEDD_EMPR = P_EMPRESA);


  BEGIN


 FOR X IN V_CONCEPTO LOOP
    SELECT AEDD_CLAVE_DOC,
           AEDD_NRO_ITEM,
           AEDD_CONCEPTO,
           CN.CTAC_CLAVE,
           AEDD_DCON_CANAL,
           AEDD_IMPORTACION,
           AEDD_DCON_OBS,
           AEDD_DOC_NRO_DOC,
           AEDD_DOC_NRO_TIMB,
           AEDD_DOC_FEC_DOC,
           AEDD_DOC_CANAL,
           AEDD_DCON_TIPO_IVA
      INTO V_AEDD_CLAVE_DOC,
           V_AEDD_NRO_ITEM,
           V_AEDD_CONCEPTO,
           V_AEDD_CTA_CONT,
           V_AEDD_DCON_CANAL,
           V_AEDD_IMPORTACION,
           V_AEDD_DCON_OBS,
           V_AEDD_DOC_NRO_DOC,
           V_AEDD_DOC_NRO_TIMB,
           V_AEDD_DOC_FEC_DOC,
           V_AEDD_DOC_CANAL,
           V_AEDD_DCON_TIPO_IVA
      FROM FIN_APROB_ESP_DOC_DET,CNT_CUENTA CN
     WHERE AEDD_CLAVE_APROB = P_COD_SOLICITUD
       AND CN.CTAC_NRO=AEDD_CTA_CONT
       AND CN.CTAC_EMPR=AEDD_EMPR
       AND AEDD_EMPR = P_EMPRESA
       AND NVL(AEDD_NRO_ITEM,0)= X.NRO_ITEM;



    IF P_OPERACION = 'ACTUALIZAR FECHA' THEN

      UPDATE FIN_DOCUMENTO
         SET DOC_FEC_DOC = V_AEDD_DOC_FEC_DOC
       WHERE DOC_CLAVE = V_AEDD_CLAVE_DOC
         AND DOC_EMPR = P_EMPRESA;

      UPDATE STK_DOCUMENTO
         SET DOCU_FEC_EMIS = V_AEDD_DOC_FEC_DOC
       WHERE DOCU_CLAVE IN (SELECT DOC_CLAVE_STK
                              FROM FIN_DOCUMENTO D
                             WHERE D.DOC_CLAVE = V_AEDD_CLAVE_DOC
                               AND DOC_EMPR = P_EMPRESA)
         AND DOCU_EMPR = P_EMPRESA;
      V_MODIFICACION_AUX := 'ACTUALIZACION DE FECHA';

    ELSIF P_OPERACION = 'ACTUALIZAR TIMBRADO' THEN

      UPDATE FIN_DOCUMENTO
         SET DOC_TIMBRADO = V_AEDD_DOC_NRO_TIMB
       WHERE DOC_CLAVE = V_AEDD_CLAVE_DOC
         AND DOC_EMPR = P_EMPRESA;
      V_MODIFICACION_AUX := 'ACTUALIZACION DE TIMBRADO';
    ELSIF P_OPERACION = 'ACTUALIZAR CANAL DOC' THEN

      UPDATE FIN_DOCUMENTO
         SET DOC_CANAL = V_AEDD_DOC_CANAL
       WHERE DOC_CLAVE = V_AEDD_CLAVE_DOC
         AND DOC_EMPR = P_EMPRESA;
      V_MODIFICACION_AUX := 'ACTUALIZACION DE CANAL';
    ELSIF P_OPERACION = 'ACTUALIZAR CONCEPTO' THEN

      IF V_AEDD_CONCEPTO IS NOT NULL THEN

        UPDATE FIN_DOC_CONCEPTO C
           SET C.DCON_CLAVE_CONCEPTO = V_AEDD_CONCEPTO,
               C.DCON_CLAVE_CTACO = V_AEDD_CTA_CONT
         WHERE C.DCON_CLAVE_DOC = V_AEDD_CLAVE_DOC
           AND C.DCON_EMPR = P_EMPRESA
           AND C.DCON_ITEM = V_AEDD_NRO_ITEM;
        V_MODIFICACION_AUX := 'ACTUALIZACION DE CONCEPTO';

      ELSIF V_AEDD_CTA_CONT IS NOT NULL THEN
        -- RAISE_APPLICATION_ERROR(-20002,V_AEDD_CLAVE_DOC||'  -  '||P_EMPRESA||'  -  '||V_AEDD_NRO_ITEM||'  -  '|| V_AEDD_CTA_CONT);
        UPDATE FIN_DOC_CONCEPTO C
           SET C.DCON_CLAVE_CTACO = V_AEDD_CTA_CONT
         WHERE C.DCON_CLAVE_DOC = V_AEDD_CLAVE_DOC
           AND C.DCON_EMPR = P_EMPRESA
           AND C.DCON_ITEM = V_AEDD_NRO_ITEM;
        V_MODIFICACION_AUX := 'ACTUALIZACION DE CTA. CONTABLE';

      ELSIF V_AEDD_DCON_CANAL IS NOT NULL THEN

        UPDATE FIN_DOC_CONCEPTO C
           SET C.DCON_CANAL = V_AEDD_DCON_CANAL
         WHERE C.DCON_CLAVE_DOC = V_AEDD_CLAVE_DOC
           AND C.DCON_EMPR = P_EMPRESA
           AND C.DCON_ITEM = V_AEDD_NRO_ITEM;
        V_MODIFICACION_AUX := 'ACTUALIZACION DE CANAL';

      ELSIF V_AEDD_IMPORTACION IS NOT NULL THEN
        UPDATE FIN_DOC_CONCEPTO C
           SET C.DCON_CLAVE_IMP = V_AEDD_IMPORTACION
         WHERE C.DCON_CLAVE_DOC = V_AEDD_CLAVE_DOC
           AND C.DCON_EMPR = P_EMPRESA
           AND C.DCON_ITEM = V_AEDD_NRO_ITEM;
        V_MODIFICACION_AUX := 'ACTUALIZACION DE NRO. IMPORTACION';

      ELSIF V_AEDD_DCON_OBS IS NOT NULL THEN
        UPDATE FIN_DOC_CONCEPTO C
           SET C.DCON_OBS = V_AEDD_DCON_OBS
         WHERE C.DCON_CLAVE_DOC = V_AEDD_CLAVE_DOC
           AND C.DCON_EMPR = P_EMPRESA
           AND C.DCON_ITEM = V_AEDD_NRO_ITEM;
        V_MODIFICACION_AUX := 'ACTUALIZACION DE OBSERVACION CONCEPTO';

      ELSIF V_AEDD_DCON_TIPO_IVA IS NOT NULL THEN

        UPDATE FIN_DOC_CONCEPTO C
           SET C.DCON_IND_TIPO_IVA_COMPRA = V_AEDD_DCON_TIPO_IVA
         WHERE C.DCON_CLAVE_DOC = V_AEDD_CLAVE_DOC
           AND C.DCON_EMPR = P_EMPRESA
           AND C.DCON_ITEM = V_AEDD_NRO_ITEM;
        V_MODIFICACION_AUX := 'ACTUALIZACION DE TIPO DE IVA';

      END IF;

    END IF;

 END LOOP;

    /*ENVIAR CORREO DE AVISO*/
    DECLARE
      V_DOC_NRO_DOC    NUMBER;
      V_DOC_TIPO_MOV   NUMBER;
      V_DOC_FEC_DOC    DATE;
      V_MENSAJE_CORREO CLOB;
      DESTINATARIO     VARCHAR2(600);
      COPIA            VARCHAR2(600);
      OCULTA           VARCHAR2(150);
    BEGIN

      SELECT DOC_NRO_DOC, DOC_TIPO_MOV, DOC_FEC_DOC
        INTO V_DOC_NRO_DOC, V_DOC_TIPO_MOV, V_DOC_FEC_DOC
        FROM FIN_DOCUMENTO
       WHERE DOC_CLAVE = V_AEDD_CLAVE_DOC
         AND DOC_EMPR = 1;

      SELECT T.CO_PARA, T.CO_CC, T.CO_CCO
        INTO DESTINATARIO, COPIA, OCULTA
        FROM GEN_CORREOS T
       WHERE T.CO_ACCION = 'Modificar_Doc';

      V_MENSAJE_CORREO := 'Se ha Autorizado la Modificacion del Documento Nro:' ||
                          V_DOC_NRO_DOC || ' Tipo Mov:' || V_DOC_TIPO_MOV ||
                          ' Fecha:' || V_DOC_FEC_DOC || CHR(13) || '
                            TIPO: ' ||
                          V_MODIFICACION_AUX;

      IF DESTINATARIO IS NOT NULL THEN
        SEND_EMAIL(P_SENDER     => 'NOREPLY',
                   P_RECIPIENTS => DESTINATARIO,
                   P_CC         => COPIA,
                   P_BCC        => OCULTA,
                   P_SUBJECT    => 'MODIFICACION DE DOCUMENTO DEL PERIODO ANTERIOR',
                   P_MESSAGE    => TO_CHAR(V_MENSAJE_CORREO));
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;

  END;

END;
/
