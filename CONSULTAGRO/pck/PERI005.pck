create or replace package PERI005 is

  -- Author  : PROGRAMACION9
  -- Created : 28/07/2020 11:00:16

  -- Purpose :
  PROCEDURE PP_CARGAR_DATOS           (I_S_TIP_MOV_ADEL_PRO      OUT NUMBER     ,
                                       I_EMPRESA                 IN  NUMBER     ,
                                       I_CONF_CTABCO             OUT NUMBER     );
  FUNCTION FP_CLAVE_DOC               (I_EMPRESA                 IN  NUMBER     ) RETURN NUMBER;
  FUNCTION PP_HABILITAR_BCO           (I_EMPRESA                 IN  NUMBER     ,
                                       I_CONC_CLAVE              IN  NUMBER     ) RETURN NUMBER;
  PROCEDURE PP_TRAER_FORMA_PAGO       (I_EMPL_FORMA_PAGO         IN  NUMBER     ,
                                       I_EMPRESA                 IN  NUMBER     ,
                                       I_FORMA_DESC              OUT VARCHAR2   );
  PROCEDURE PP_BUSCAR_CONCEPTO        (I_EMPRESA                 IN  NUMBER     ,
                                       I_PCON_DESC               OUT VARCHAR2   ,
                                       S_TIPO_MOV                IN  OUT NUMBER ,
                                       S_TMOV_DESC               OUT VARCHAR2   ,
                                       S_CLAVE_CONCEPTO          IN  NUMBER     ,
                                       I_PCON_IND_SUMA_IPS       OUT VARCHAR2   ,
                                       I_PCON_IND_IPS            OUT VARCHAR2   ,
                                       P_MOSTAR_CAJA             OUT VARCHAR2);
  PROCEDURE PP_VERIF_MOSTRAR_CTA_BANC (I_BCO_DESC                OUT VARCHAR2   ,
                                       I_CTA_DESC                OUT VARCHAR2   ,
                                       S_MON                     OUT NUMBER     ,
                                       I_EMPRESA                 IN  NUMBER     ,
                                       I_DOC_CTA_BCO             IN  NUMBER     ,
                                       P_OPCION_BCO              IN NUMBER DEFAULT NULL);
  PROCEDURE PL_VALIDAR_OPCTA          (V_CTA                     IN  NUMBER     ,
                                       V_EMPR                    IN  NUMBER     ,
                                       V_LOGIN                   IN  VARCHAR2   ,
                                       V_CTA_DESC                IN  VARCHAR2   ,
                                       V_OPERACION               IN  VARCHAR2   );
  PROCEDURE PP_TRAER_MONEDA           (S_MON_DESC                OUT VARCHAR2   ,
                                       S_MON                     IN  NUMBER     ,
                                       I_EMPRESA                 IN  NUMBER     ,
                                       S_TASA_OFIC               OUT NUMBER     ,
                                       I_PDOC_FEC                IN  DATE       );
  PROCEDURE PP_MOSTRAR_SUCURSAL       (S_DESCRIPCION             OUT VARCHAR2   ,
                                       I_EMPRESA                 IN  NUMBER     ,
                                       S_SUCURSAL                IN  NUMBER     );
  PROCEDURE PP_CARGAR_EMPLEADO        (I_PER_NOMBRE              OUT VARCHAR2   ,
                                       I_S_SECTOR                OUT VARCHAR2   ,
                                       I_FIN_CODIGO_PROV         OUT NUMBER     ,
                                       I_PDOC_DEPARTAMENTO       OUT NUMBER     ,
                                       I_PDOC_EMPLEADO           IN  NUMBER     ,
                                       I_EMPRESA                 IN  NUMBER     ,
                                       I_CLAVE_CONCEPTO          IN  NUMBER     ,
                                       I_PCON_CLAVE_CONCEPTO_FIN OUT NUMBER     ,
                                       I_PCON_CLAVE_CTACO        OUT NUMBER     ,
                                       I_FCON_TIPO_SALDO         OUT VARCHAR2   ,
                                       I_EMPL_FORMA_PAGO         OUT NUMBER     ,
                                       I_EMPL_CLIENTE            OUT NUMBER     ,
                                       I_LIMITE_CREDITO          OUT NUMBER,
                                       I_FECHA                   IN DATE);


  PROCEDURE PP_VALIDAR_PERIODO        (FECHA                     IN  DATE       ,
                                       I_EMPRESA                 IN  NUMBER     ,
                                       W_PERIODO                 OUT NUMBER     );
  PROCEDURE PP_VALIDAR_PERIODO_FIN    (FECHA                     IN  DATE       ,
                                       I_EMPRESA                 IN  NUMBER     );
  PROCEDURE PP_TRAER_DESC_MONE        (I_MON_DESC                OUT VARCHAR2   ,
                                       I_MON_SIMBOLO             OUT VARCHAR2   ,
                                       I_W_MON_DEC_IMP           OUT NUMBER     ,
                                       I_W_MON_DEC_PRECIO        OUT NUMBER     ,
                                       I_MON_LOC                 IN  NUMBER     ,
                                       I_EMPRESA                 IN  NUMBER     ,
                                       I_W_DOC_TASA_US           OUT NUMBER     ,
                                       S_MON                     IN  NUMBER     );
  PROCEDURE PP_CARGAR_DETALLE         (S_SUCURSAL                IN  NUMBER     ,
                                       S_CLAVE_CONCEPTO          IN  NUMBER     ,
                                       I_PDOC_FEC                IN  DATE       ,
                                       S_MON                     IN  NUMBER     ,
                                       I_EMPL_FORMA_PAGO         IN  VARCHAR2   ,
                                       I_EMPRESA                 IN  NUMBER     ,
                                       I_MON_DESC                OUT VARCHAR2   ,
                                       I_MON_SIMBOLO             OUT VARCHAR2   ,
                                       I_W_MON_DEC_IMP           IN  OUT NUMBER ,
                                       I_W_MON_DEC_PRECIO        OUT NUMBER     ,
                                       I_MON_LOC                 IN  NUMBER     ,
                                       I_W_DOC_TASA_US           OUT NUMBER     ,
                                       I_BCO_PAGO                IN NUMBER);
  PROCEDURE PP_ACTUALIZAR_REGISTRO    (I_EMPRESA                 IN  NUMBER     ,
                                       I_DESC_EMPRESA            IN  VARCHAR2   ,
                                       I_DOC_CTA_BCO             IN  NUMBER     ,
                                       I_PCON_DESC               IN  VARCHAR2   ,
                                       I_PDOC_FEC                IN  DATE       ,
                                       S_TASA_OFIC               IN  NUMBER     ,
                                       I_S_TIP_MOV_ADEL_PRO      IN  NUMBER     ,
                                       I_S_TIPO_MOV              IN  NUMBER     ,
                                       I_SUCURSAL                IN  NUMBER     ,
                                       I_PDOC_NRO_DOC            IN  NUMBER     ,
                                       I_DOC_OBS                 IN  VARCHAR2   ,
                                       I_PDOC_QUINCENA           IN  NUMBER     ,
                                       W_PERIODO                 IN  NUMBER     ,
                                       I_CLAVE_CONCEPTO          IN  NUMBER     ,
                                       I_S_MON                   IN  NUMBER     ,
                                       I_PDOC_FEC_OPER           IN  DATE       ,
                                       I_MON_LOC                 IN  NUMBER     ,
                                       I_CUOTA                   IN  VARCHAR2   ,
                                       I_FEC_VTO_ADELANTO        IN  DATE);
  FUNCTION PP_CARGAR_DOCUMENTO_FIN    (I_PDOC_FEC                IN  DATE       ,
                                       S_TASA_OFIC               IN  NUMBER     ,
                                       I_EMPRESA                 IN  NUMBER     ,
                                       I_S_TIP_MOV_ADEL_PRO      IN  NUMBER     ,
                                       I_S_TIPO_MOV              IN  NUMBER     ,
                                       I_DESC_EMPRESA            IN  VARCHAR2   ,
                                       I_DOC_CTA_BCO             IN  NUMBER     ,
                      /*PARAMETRO*/    I_SUCURSAL                IN  NUMBER     ,
                                       I_PDOC_NRO_DOC            IN  NUMBER     ,
                                       I_DOC_OBS                 IN  VARCHAR2   ,
                                       I_PDOC_FEC_OPER           IN  DATE       ,
                                       I_MON_LOC                 IN  NUMBER     ,
                                       S_MON                     IN  NUMBER     ,
                                       I_CUOTA                   IN  VARCHAR2   ,
                                       I_FEC_VTO_ADELANTO        IN  DATE,
                                       I_PDOC_EMPLEADO           IN NUMBER,
                                       I_FIN_CODIGO_PROV         IN NUMBER,
                                       I_S_IMPORTE               IN NUMBER,
                                       I_S_IMP_IPS               IN NUMBER,
                                       I_PCON_CLAVE_CTACO        IN NUMBER,
                                       I_PER_NOMBRE              IN VARCHAR2,
                                       I_S_SECTOR                IN VARCHAR2,
                                       I_PCON_CLAVE_CONCEPTO_FIN IN NUMBER,
                                       I_FCON_TIPO_SALDO          IN VARCHAR2,
                                       I_PDOC_DEPARTAMENTO        IN NUMBER,
                                       I_S_FORMA_PAGO             IN NUMBER) RETURN NUMBER;

  PROCEDURE PP_INS_PER_DOC            (IPDOC_CLAVE               IN  NUMBER     ,
                                       IPDOC_QUINCENA            IN  NUMBER     ,
                                       IPDOC_EMPLEADO            IN  NUMBER     ,
                                       IPDOC_FEC                 IN  DATE       ,
                                       IPDOC_NRO_DOC             IN  NUMBER     ,
                                       IPDOC_FEC_GRAB            IN  DATE       ,
                                       IPDOC_LOGIN               IN  VARCHAR2   ,
                                       IPDOC_FORM                IN  VARCHAR2   ,
                                       IPDOC_PERIODO             IN  NUMBER     ,
                                       IPDOC_CLAVE_FIN           IN  NUMBER     ,
                                       IPDOC_NRO_ITEM            IN  NUMBER     ,
                                       IPDOC_CONCEPTO            IN  NUMBER     ,
                                       IPDOC_IMPORTE             IN  NUMBER     ,
                                       IPDOC_IMPORTE_LOC         IN  NUMBER     ,
                                       IPDOC_MON                 IN  NUMBER     ,
                                       IPDOC_DEPARTAMENTO        IN  NUMBER     ,
                                       IPDOC_EMPRESA             IN  NUMBER     ,
                                       I_S_TIPO_MOV              IN  NUMBER     ,
                                       I_S_TIP_MOV_ADEL_PRO      IN  NUMBER     ,
                                       I_DESC_EMPRESA            IN  VARCHAR2   ,
                                       I_OBS                     IN VARCHAR2,
                                       I_CANT_DIAS_TRA           IN NUMBER );---LV09/04/2021);

  PROCEDURE PP_FEC_PROX_VTO           (I_DOCU_FEC_EMIS           IN  DATE       ,
                                       IO_FEC_PRIM_VTO           OUT DATE       );
  PROCEDURE PP_GENERAR_CUOTA          (I_FEC_PRIM_VTO            IN  DATE       ,
                                      I_FEC_OPER                 IN  DATE       ,
                                      I_TOTAL                    IN  NUMBER     ,
                                      I_OP_CUOTA                 IN  VARCHAR2   ,
                                      I_CANT_CUOTAS              IN  NUMBER     ,
                                      I_TIPO_VENCIMIENTO         IN  VARCHAR2   ,
                                      I_DIAS_ENTRE_CUOTAS        IN  NUMBER     ,
                                      I_MON_DEC_IMP              IN  NUMBER     ,
                                      I_PLAZO_PAGO               IN  VARCHAR2   );
  PROCEDURE PP_ADICIONAR_CUOTA        (I_CUO_FEC_VTO             IN  DATE       ,
                                       I_CUO_IMP_MON             IN  NUMBER     ,
                                       I_CUO_EMPL                IN  NUMBER);


  PROCEDURE PP_BLOQ_PROV_CLI (P_EMPRESA            IN NUMBER,
                              P_LEGAJO             IN NUMBER,
                              P_CONCEPTO           IN NUMBER);

   PROCEDURE PP_BLOQUEAR_SISTEMAS (P_EMPRESA IN NUMBER,
                                   P_FEC_VTO IN DATE DEFAULT NULL,
                                   P_PROGRAMA IN VARCHAR2 DEFAULT NULL);


    PROCEDURE PP_LLAMAR_REPORTE  (P_EMPRESA NUMBER,
                                P_PERIODO NUMBER,
                                P_NRO_DOC NUMBER);
                                
                                
 FUNCTION PP_DIAS_TRABA_MONTA (P_LEGAJO  NUMBER,
                              P_EMPRESA NUMBER,
                              P_FEC_INGRESO DATE,
                              P_FEC_SALIDA DATE,
                              P_FORMA_PAGO NUMBER,
                              P_DESDE      DATE,
                              P_HASTA      DATE)RETURN NUMBER;                               
end PERI005;
/
CREATE OR REPLACE PACKAGE BODY PERI005 IS
  co_mnd_gs constant gen_moneda.mon_codigo%type := 1;
       
  PROCEDURE PP_CARGAR_DATOS(I_S_TIP_MOV_ADEL_PRO OUT NUMBER,
                            I_EMPRESA            IN NUMBER,
                            I_CONF_CTABCO        OUT NUMBER) IS

  BEGIN

    SELECT T.CONF_ADELANTO_PRO
      INTO I_S_TIP_MOV_ADEL_PRO
      FROM FIN_CONFIGURACION T
     WHERE CONF_EMPR = I_EMPRESA;

    SELECT CONF_CTABCO
      INTO I_CONF_CTABCO
      FROM PER_CONFIGURACION
     WHERE CONF_EMPR = I_EMPRESA;

    --  :PER_DOCUMENTO.PDOC_EMPR := :PARAMETER.P_EMPRESA;

    -- GO_ITEM('BSEL.PDOC_NRO_DOC');

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'No se ha cargado la Configuracion del Modulo de Recursos Humanos');
  END;

  FUNCTION PP_HABILITAR_BCO(I_EMPRESA IN NUMBER, I_CONC_CLAVE IN NUMBER)
    RETURN NUMBER IS
    V_PCON_DESC   VARCHAR2(100);
    V_EMPRESA_DES VARCHAR2(100);
  BEGIN
    SELECT A.PCON_DESC
      INTO V_PCON_DESC
      FROM PER_CONCEPTO A
     WHERE PCON_EMPR = I_EMPRESA
       AND A.PCON_CLAVE = I_CONC_CLAVE
     ORDER BY PCON_CLAVE;

    SELECT V.EMPR_RAZON_SOCIAL
      INTO V_EMPRESA_DES
      FROM GEN_EMPRESA V
     WHERE V.EMPR_CODIGO = I_EMPRESA;
    IF V_PCON_DESC LIKE '%ADELANTO%' OR V_PCON_DESC LIKE '%ALMUERZO%' OR
       (V_PCON_DESC LIKE '%AGUINALDO%' AND I_EMPRESA != 2) OR
       V_PCON_DESC LIKE '%UNIFORME%' OR V_PCON_DESC LIKE '%VIATICO%' OR
       V_PCON_DESC LIKE '%COMESTIBLE%' OR V_PCON_DESC LIKE '%COMBUSTIBLE%' OR
       V_PCON_DESC LIKE '%APORTE%' OR V_PCON_DESC LIKE '%EGRESOS VARIOS%' THEN
      RETURN 1;
    ELSIF I_EMPRESA = 2 AND V_PCON_DESC LIKE '%AGUINALDO%' THEN

      RETURN 2;
    ELSE
      RETURN 0;
    END IF;
  END;

  FUNCTION FP_CLAVE_DOC(I_EMPRESA IN NUMBER) RETURN NUMBER IS
    V_CLAVE NUMBER;
  BEGIN
    SELECT MAX(DOC_MAX)
      INTO V_CLAVE
      FROM (SELECT NVL(MAX(PDOC_CLAVE), 0) + 1 DOC_MAX
              FROM PER_DOCUMENTO
             WHERE PDOC_EMPR = I_EMPRESA
            UNION ALL
            SELECT NVL(MAX(PDOC_CLAVE), 0) + 1 DOC_MAX
              FROM PER_DOCUMENTO_ADEL_TEMP
             WHERE PDOC_EMPR = I_EMPRESA);

    RETURN V_CLAVE;

  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20017, SQLERRM);

  END;

  PROCEDURE PP_TRAER_FORMA_PAGO(I_EMPL_FORMA_PAGO IN NUMBER,
                                I_EMPRESA         IN NUMBER,
                                I_FORMA_DESC      OUT VARCHAR2) IS
  BEGIN
    SELECT FORMA_DESC
      INTO I_FORMA_DESC
      FROM PER_FORMA_PAGO
     WHERE FORMA_CODIGO = I_EMPL_FORMA_PAGO
       AND FORMA_EMPR = I_EMPRESA;
    --
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      I_FORMA_DESC := NULL;
      RAISE_APPLICATION_ERROR(-20001, 'Forma de Pago inexistente!');
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001, SQLERRM);
  END;

  PROCEDURE PP_BUSCAR_CONCEPTO(I_EMPRESA           IN NUMBER,
                               I_PCON_DESC         OUT VARCHAR2,
                               S_TIPO_MOV          IN OUT NUMBER,
                               S_TMOV_DESC         OUT VARCHAR2,
                               S_CLAVE_CONCEPTO    IN NUMBER,
                               I_PCON_IND_SUMA_IPS OUT VARCHAR2,
                               I_PCON_IND_IPS      OUT VARCHAR2,
                               P_MOSTAR_CAJA       OUT VARCHAR2) IS
  BEGIN
    IF I_EMPRESA != 2 THEN
      SELECT A.PCON_DESC CONCEPTORH, A.PCON_FIN_TMOV
        INTO I_PCON_DESC, S_TIPO_MOV
        FROM PER_CONCEPTO A
       WHERE A.PCON_CLAVE = S_CLAVE_CONCEPTO
         AND A.PCON_EMPR = I_EMPRESA;

    ELSE
      SELECT A.PCON_DESC CONCEPTORH,
             A.PCON_FIN_TMOV,
             A.PCON_IND_SUMA_IPS,
             A.PCON_IND_IPS
        INTO I_PCON_DESC, S_TIPO_MOV, I_PCON_IND_SUMA_IPS, I_PCON_IND_IPS
        FROM PER_CONCEPTO A
       WHERE A.PCON_CLAVE = S_CLAVE_CONCEPTO
         AND PCON_EMPR = I_EMPRESA;
    END IF;

    IF S_TIPO_MOV IS NOT NULL THEN
      SELECT TMOV_DESC
        INTO S_TMOV_DESC
        FROM GEN_TIPO_MOV
       WHERE TMOV_CODIGO = S_TIPO_MOV
         AND TMOV_EMPR = I_EMPRESA;
    END IF;

    IF S_TMOV_DESC LIKE '%ADELANTO%' OR S_TMOV_DESC LIKE '%ALMUERZO%' OR
       S_TMOV_DESC LIKE '%AGUINALDO%' OR S_TMOV_DESC LIKE '%UNIFORME%' OR
       S_TMOV_DESC LIKE '%VIATICO%' OR S_TMOV_DESC LIKE '%COMESTIBLE%' OR
       S_TMOV_DESC LIKE '%COMBUSTIBLE%' OR S_TMOV_DESC LIKE '%APORTE%' OR
       S_TMOV_DESC LIKE '%EGRESOS VARIOS%' OR S_TMOV_DESC LIKE '%APORTE%' THEN

      P_MOSTAR_CAJA := 1;
    ELSE
      P_MOSTAR_CAJA := 2;
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20003, 'El concepto no existe!');

  END;

  PROCEDURE PP_VERIF_MOSTRAR_CTA_BANC(I_BCO_DESC    OUT VARCHAR2,
                                      I_CTA_DESC    OUT VARCHAR2,
                                      S_MON         OUT NUMBER,
                                      I_EMPRESA     IN NUMBER,
                                      I_DOC_CTA_BCO IN NUMBER,
                                      P_OPCION_BCO   IN NUMBER DEFAULT NULL) IS
    V_MON NUMBER;
  BEGIN

     IF   P_OPCION_BCO = 0 THEN
    SELECT BCO_DESC
    INTO I_BCO_DESC
    FROM FIN_BANCO
    WHERE BCO_EMPR =I_EMPRESA
     AND  BCO_CODIGO  =I_DOC_CTA_BCO;
     ELSE
    SELECT CTA_MON, BCO_DESC, CTA_DESC, CTA_MON
      INTO V_MON, I_BCO_DESC, I_CTA_DESC, S_MON
      FROM FIN_CUENTA_BANCARIA, FIN_BANCO
     WHERE CTA_BCO = BCO_CODIGO(+)
       AND CTA_EMPR = BCO_EMPR(+)
       AND CTA_CODIGO = I_DOC_CTA_BCO
       AND CTA_EMPR = I_EMPRESA;

           PL_VALIDAR_OPCTA(I_DOC_CTA_BCO,
                     I_EMPRESA,
                     GEN_DEVUELVE_USER,
                     I_CTA_DESC,
                     'C');

  END IF;


    /*
    IF V_MON <> 1 THEN
      PL_EXHIBIR_ERROR('Moneda solo puede ser Guaranies');
    END IF;
    */

    --


  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      I_BCO_DESC := NULL;
      I_CTA_DESC := NULL;
      RAISE_APPLICATION_ERROR(-20004, 'Cuenta bancaria inexistente');
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20004, SQLERRM);
  END;

  PROCEDURE PL_VALIDAR_OPCTA(V_CTA       IN NUMBER,
                             V_EMPR      IN NUMBER,
                             V_LOGIN     IN VARCHAR2,
                             V_CTA_DESC  IN VARCHAR2,
                             V_OPERACION IN VARCHAR2) IS
    V_DEP  VARCHAR2(1);
    V_EXT  VARCHAR2(1);
    V_PROP VARCHAR2(1);
    V_FEC  DATE;

    V_SUCURSAL NUMBER;

  BEGIN
    --BUSCAR LA SUCURSAL
    BEGIN
      SELECT CONF_SUC
        INTO V_SUCURSAL
        FROM GEN_CONFIGURACION
       WHERE CONF_EMPR = V_EMPR;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    --IF SUBSTR(NAME_IN(P_PROGRAMA), 1, 3) = 'FIN' THEN

    IF V_SUCURSAL = 1 THEN
      --BUSCAR CUAL FUE LA MAX FECHA MENOR AL DIA DEL HOY
      BEGIN
        SELECT CTA_ULT_FEC_MOVCR
          INTO V_FEC
          FROM FIN_CUENTA_BANCARIA
         WHERE CTA_CODIGO = V_CTA
           AND CTA_EMPR = V_EMPR;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
      END;
    END IF;
    -- END IF;

    SELECT OP_CTA_PROPIETARIO, OP_CTA_IND_PERM_DEP, OP_CTA_IND_PERM_EXT
      INTO V_PROP, V_DEP, V_EXT
      FROM GEN_OPERADOR_EMPRESA,
           GEN_OPERADOR,
           FIN_OPER_CTA_BCO,
           FIN_CUENTA_BANCARIA
     WHERE OPER_CODIGO = OP_CTA_OPER
       AND OP_CTA_EMPR = CTA_EMPR
       AND OP_CTA_CTA_CODIGO = CTA_CODIGO
       AND OPER_LOGIN = V_LOGIN
       AND OP_CTA_EMPR = V_EMPR
       AND OP_CTA_CTA_CODIGO = V_CTA
       AND OPER_CODIGO = OPEM_OPER
       AND OPEM_EMPR = V_EMPR;

    IF V_OPERACION = 'D' AND ((V_EMPR <> 2 AND NVL(V_PROP, 'N') <> 'S') OR
       (V_EMPR = 2 AND NVL(V_DEP, 'N') <> 'S')) THEN
      RAISE_APPLICATION_ERROR(-20005,
                              'No tiene permiso para operar con la Cta. ' ||
                              V_CTA_DESC);
    ELSE
      IF V_OPERACION = 'C' AND V_EXT <> 'S' THEN
        RAISE_APPLICATION_ERROR(-20006,
                                'No tiene permiso para operar con la Cta. ' ||
                                V_CTA_DESC);
      END IF;
    END IF;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20007,
                              'No puede realizar operaciones con la Cta. ' ||
                              V_CTA_DESC);
  END;

  PROCEDURE PP_TRAER_MONEDA(S_MON_DESC  OUT VARCHAR2,
                            S_MON       IN NUMBER,
                            I_EMPRESA   IN NUMBER,
                            S_TASA_OFIC OUT NUMBER,
                            I_PDOC_FEC  IN DATE) IS
  BEGIN
    SELECT MON_DESC
      INTO S_MON_DESC
      FROM GEN_MONEDA
     WHERE MON_CODIGO = S_MON
     AND MON_EMPR = I_EMPRESA;

     ----S_TASA_OFIC := FIN_BUSCAR_COTIZACION_FEC(I_PDOC_FEC, S_MON, I_EMPRESA);
      S_TASA_OFIC := GEN_COTIZACION(I_EMPRESA,S_MON,I_PDOC_FEC,'V');

    --
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20008, 'Moneda inexistente!');
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20008, SQLERRM);
  END;

  PROCEDURE PP_MOSTRAR_SUCURSAL(S_DESCRIPCION OUT VARCHAR2,
                                I_EMPRESA     IN NUMBER,
                                S_SUCURSAL    IN NUMBER) IS
  BEGIN
    SELECT SUC_DESC
      INTO S_DESCRIPCION
      FROM GEN_SUCURSAL
     WHERE SUC_CODIGO = S_SUCURSAL
       AND SUC_EMPR = I_EMPRESA;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20009, 'Codigo de sucursal no encontrado');
  END;

  PROCEDURE PP_CARGAR_EMPLEADO(I_PER_NOMBRE              OUT VARCHAR2,
                               I_S_SECTOR                OUT VARCHAR2,
                               I_FIN_CODIGO_PROV         OUT NUMBER,
                               I_PDOC_DEPARTAMENTO       OUT NUMBER,
                               I_PDOC_EMPLEADO           IN NUMBER,
                               I_EMPRESA                 IN NUMBER,
                               I_CLAVE_CONCEPTO          IN NUMBER,
                               I_PCON_CLAVE_CONCEPTO_FIN OUT NUMBER,
                               I_PCON_CLAVE_CTACO        OUT NUMBER,
                               I_FCON_TIPO_SALDO         OUT VARCHAR2,
                               I_EMPL_FORMA_PAGO         OUT NUMBER,
                               I_EMPL_CLIENTE            OUT NUMBER,
                               I_LIMITE_CREDITO          OUT NUMBER,
                               I_FECHA                   IN DATE) IS

    V_IMP_LIM_CR_EMPR    NUMBER;
    V_IMP_LIM_DISP_GRUPO NUMBER;
    V_IMP_CHEQ_DIFERIDO  NUMBER;
    V_IMP_CHEQ_RECHAZADO NUMBER;

  BEGIN

    IF I_PDOC_EMPLEADO IS NOT NULL THEN
      --   BEGIN
      SELECT EMPL_NOMBRE || ' ' || EMPL_APE,
             DPTO_DESC,
             EMPL_CODIGO_PROV,
             EMPL_DEPARTAMENTO,
             EMPL_FORMA_PAGO,
             CASE
               WHEN I_EMPRESA = 1 THEN
                EMPL_COD_CLIENTE
               WHEN I_EMPRESA = 2 THEN
                EMPL_CODIGO_CLI
             END CLIENTE
        INTO I_PER_NOMBRE,
             I_S_SECTOR,
             I_FIN_CODIGO_PROV,
             I_PDOC_DEPARTAMENTO,
             I_EMPL_FORMA_PAGO,
             I_EMPL_CLIENTE
        FROM PER_EMPLEADO DS, GEN_DEPARTAMENTO
       WHERE EMPL_LEGAJO = I_PDOC_EMPLEADO
         AND EMPL_SITUACION = 'A'
         AND EMPL_DEPARTAMENTO = DPTO_CODIGO(+)
         AND EMPL_EMPRESA = DPTO_EMPR(+)
         AND EMPL_EMPRESA = I_EMPRESA;

      IF I_FIN_CODIGO_PROV IS NULL THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'Falta configurar codigo de proveedor en 17-1-5');
      END IF;
    END IF;

    IF I_PDOC_EMPLEADO IS NOT NULL  and I_EMPRESA = 1 THEN
      FIN_LIM_CREDITO_EMPLEADO(V_FEC_DOC            => LAST_DAY(TO_DATE(I_FECHA)),
                               V_CLI                => null,
                               V_IMP_LIM_CR_EMPR    => V_IMP_LIM_CR_EMPR,
                               V_EMPR               => I_EMPRESA,
                               V_IMP_LIM_DISP_GRUPO => V_IMP_LIM_DISP_GRUPO,
                               V_IMP_LIM_DISP_EMPR  => I_LIMITE_CREDITO,
                               V_IMP_CHEQ_DIFERIDO  => V_IMP_CHEQ_DIFERIDO,
                               V_IMP_CHEQ_RECHAZADO => V_IMP_CHEQ_RECHAZADO,
                               V_EMPL_LEGAJO        => I_PDOC_EMPLEADO);

    ELSE
      I_EMPL_CLIENTE := 0;
    END IF;

    IF I_PDOC_EMPLEADO IS NOT NULL THEN
      BEGIN

        SELECT FCON_CLAVE, FCON_CLAVE_CTACO, FCON_TIPO_SALDO
          INTO I_PCON_CLAVE_CONCEPTO_FIN,
               I_PCON_CLAVE_CTACO,
               I_FCON_TIPO_SALDO
          FROM PER_EMPLEADO, PER_DPTO_CONC, FIN_CONCEPTO, PER_CONCEPTO
         WHERE DPTOC_DPTO = EMPL_DEPARTAMENTO
           AND DPTOC_EMPR = EMPL_EMPRESA

           AND DPTOC_FIN_CONC = FCON_CLAVE
           AND DPTOC_EMPR = FCON_EMPR

           AND DPTOC_PER_CONC = PCON_CLAVE
           AND DPTOC_EMPR = PCON_EMPR

           AND DPTOC_PER_CONC = I_CLAVE_CONCEPTO
           AND EMPL_LEGAJO = I_PDOC_EMPLEADO

           AND EMPL_EMPRESA = I_EMPRESA;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          ---I_PER_NOMBRE:=NULL;
          -- I_S_SECTOR:=NULL;
          I_FIN_CODIGO_PROV := NULL;
          RAISE_APPLICATION_ERROR(-20012,
                                  'Falta definir el departamento/concepto para este empleado!');
      END;
    END IF;
  END PP_CARGAR_EMPLEADO;

  PROCEDURE PP_VALIDAR_PERIODO(FECHA     IN DATE,
                               I_EMPRESA IN NUMBER,
                               W_PERIODO OUT NUMBER) IS

    V_FEC_INI DATE;
    V_FEC_FIN DATE;

    V_PERIODO NUMBER;

  BEGIN

    SELECT PERI_FEC_INI
      INTO V_FEC_INI
      FROM PER_CONFIGURACION, PER_PERIODO, GEN_EMPRESA
     WHERE CONF_PERI_ACT = PERI_CODIGO
       AND CONF_EMPR = EMPR_CODIGO
       AND PERI_EMPR = EMPR_CODIGO
       AND EMPR_CODIGO = I_EMPRESA;

    SELECT PERI_FEC_FIN
      INTO V_FEC_FIN
      FROM PER_CONFIGURACION, PER_PERIODO, GEN_EMPRESA
     WHERE CONF_PERI_SGTE = PERI_CODIGO
       AND CONF_EMPR = EMPR_CODIGO
       AND PERI_EMPR = EMPR_CODIGO
       AND CONF_EMPR = I_EMPRESA;

    IF FECHA NOT BETWEEN V_FEC_INI AND V_FEC_FIN THEN
      RAISE_APPLICATION_ERROR(-20013,
                              'La fecha de documento debe estar comprendida entre ' ||
                              TO_CHAR(V_FEC_INI, 'dd-mm-yyyy') || ' y ' ||
                              TO_CHAR(V_FEC_FIN, 'dd-mm-yyyy'));
    END IF;

    SELECT max(PERI_CODIGO)
      INTO V_PERIODO
      FROM PER_PERIODO
     WHERE PERI_FEC_INI <= FECHA
       AND PERI_FEC_FIN >= FECHA
       AND PERI_EMPR = I_EMPRESA;

    W_PERIODO := V_PERIODO;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20014,
                              'No ha sido cargada la configuracion de Recursos Humanos!');
  END;

  PROCEDURE PP_VALIDAR_PERIODO_FIN(FECHA IN DATE, I_EMPRESA IN NUMBER) IS

    V_FEC_INI DATE;
    V_FEC_FIN DATE;

    V_PERIODO NUMBER;

  BEGIN

    SELECT PERI_FEC_INI
      INTO V_FEC_INI
      FROM FIN_CONFIGURACION, FIN_PERIODO, GEN_EMPRESA
     WHERE CONF_PERIODO_ACT = PERI_CODIGO
       AND CONF_EMPR = EMPR_CODIGO
       AND PERI_EMPR = EMPR_CODIGO
       AND EMPR_CODIGO = I_EMPRESA;

    SELECT PERI_FEC_FIN
      INTO V_FEC_FIN
      FROM FIN_CONFIGURACION, FIN_PERIODO, GEN_EMPRESA
     WHERE CONF_PERIODO_SGTE = PERI_CODIGO
       AND CONF_EMPR = EMPR_CODIGO
       AND PERI_EMPR = EMPR_CODIGO
       AND EMPR_CODIGO = I_EMPRESA;

    IF FECHA NOT BETWEEN V_FEC_INI AND V_FEC_FIN THEN
      RAISE_APPLICATION_ERROR(-20015,
                              'La fecha de documento debe estar comprendida entre ' ||
                              TO_CHAR(V_FEC_INI, 'dd-mm-yyyy') || ' y ' ||
                              TO_CHAR(V_FEC_FIN, 'dd-mm-yyyy') ||
                              ', si se esta relacionando con FINANZAS!');
    END IF;

    SELECT PERI_CODIGO
      INTO V_PERIODO
      FROM FIN_PERIODO
     WHERE PERI_FEC_INI <= FECHA
       AND PERI_FEC_FIN >= FECHA
       AND PERI_EMPR = I_EMPRESA;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20016,
                              'No ha sido cargada la configuracion de Finanzas!');
  END;

  PROCEDURE PP_TRAER_DESC_MONE(I_MON_DESC         OUT VARCHAR2,
                               I_MON_SIMBOLO      OUT VARCHAR2,
                               I_W_MON_DEC_IMP    OUT NUMBER,
                               I_W_MON_DEC_PRECIO OUT NUMBER,
                               I_MON_LOC          IN NUMBER,
                               I_EMPRESA          IN NUMBER,
                               I_W_DOC_TASA_US    OUT NUMBER,
                               S_MON              IN NUMBER) IS
  BEGIN

    SELECT MON_DESC, MON_SIMBOLO, MON_DEC_IMP, MON_DEC_PRECIO
      INTO I_MON_DESC, I_MON_SIMBOLO, I_W_MON_DEC_IMP, I_W_MON_DEC_PRECIO

      FROM GEN_MONEDA
     WHERE MON_CODIGO = S_MON
       AND MON_EMPR = I_EMPRESA;
    IF S_MON = I_MON_LOC THEN
      I_W_DOC_TASA_US := 1;
    ELSE
      NULL;
    END IF;

    --
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20018, SQLERRM);
  END;

  PROCEDURE PP_CARGAR_DETALLE(S_SUCURSAL         IN NUMBER,
                              S_CLAVE_CONCEPTO   IN NUMBER,
                              I_PDOC_FEC         IN DATE,
                              S_MON              IN NUMBER,
                              I_EMPL_FORMA_PAGO  IN VARCHAR2,
                              I_EMPRESA          IN NUMBER,
                              I_MON_DESC         OUT VARCHAR2,
                              I_MON_SIMBOLO      OUT VARCHAR2,
                              I_W_MON_DEC_IMP    IN OUT NUMBER,
                              I_W_MON_DEC_PRECIO OUT NUMBER,
                              I_MON_LOC          IN NUMBER,
                              I_W_DOC_TASA_US    OUT NUMBER,
                              I_BCO_PAGO         IN NUMBER) IS

    V_PORCENTAJE_SEGURO       NUMBER;
    V_S_SECTOR                VARCHAR2(100);
    V_FIN_CODIGO_PROV         NUMBER;
    V_PCON_CLAVE_CONCEPTO_FIN NUMBER;
    V_PCON_CLAVE_CTACO        NUMBER;
    V_FCON_TIPO_SALDO         VARCHAR2(100);
    V_PDOC_DEPARTAMENTO       VARCHAR2(100);
    V_S_IMP_IPS               NUMBER;
    V_PDOC_EMPLEADO           NUMBER;
    V_PER_NOMBRE              VARCHAR2(100);
    V_S_FORMA_PAGO            NUMBER;
    V_S_IMPORTE               NUMBER;

    V_IMP_LIM_CR_EMPR    NUMBER;
    V_IMP_LIM_DISP_GRUPO NUMBER;
    V_IMP_LIM_DISP_EMPR  NUMBER;
    V_IMP_CHEQ_DIFERIDO  NUMBER;
    V_IMP_CHEQ_RECHAZADO NUMBER;

    V_IMPORTE_CONCEPTO NUMBER;

  BEGIN

    BEGIN
      -- CALL THE PROCEDURE
      PERI005.PP_TRAER_DESC_MONE(I_MON_DESC         => I_MON_DESC,
                                 I_MON_SIMBOLO      => I_MON_SIMBOLO,
                                 I_W_MON_DEC_IMP    => I_W_MON_DEC_IMP,
                                 I_W_MON_DEC_PRECIO => I_W_MON_DEC_PRECIO,
                                 I_MON_LOC          => I_MON_LOC,
                                 I_EMPRESA          => I_EMPRESA,
                                 I_W_DOC_TASA_US    => I_W_DOC_TASA_US,
                                 S_MON              => S_MON);
    END;

    IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => 'PERI005') THEN
      APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => 'PERI005');
    END IF;
    APEX_COLLECTION.CREATE_COLLECTION(P_COLLECTION_NAME => 'PERI005');


     IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => 'PERI005_ADE') THEN
      APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => 'PERI005_ADE');
    END IF;
    APEX_COLLECTION.CREATE_COLLECTION(P_COLLECTION_NAME => 'PERI005_ADE');


    IF I_EMPRESA != 2 THEN

      -------------------------

     -------------------------------------
      IF S_SUCURSAL IS NOT NULL THEN
        -- RAISE_APPLICATION_ERROR(-20020,'yuyui');
        FOR R IN (SELECT EMPL_LEGAJO,
                         EMPL_NOMBRE || ' ' || EMPL_APE NOMBRE,
                         DPTO_DESC,
                         EMPL_CODIGO_PROV,
                         NVL(EMPL_PAGA_IPS, 'N') EMPL_PAGA_IPS,
                         NVL(PCON_IND_IPS, 'N') PCON_IND_IPS,
                         PC.PERCON_IMP,
                         FCON_CLAVE,
                         FCON_CLAVE_CTACO,
                         FCON_TIPO_SALDO,
                         NVL(PCON_IND_SUMA_IPS, 'N') PCON_IND_SUMA_IPS,
                         EMPL_DEPARTAMENTO,
                         EMPL_FORMA_PAGO,
                         EMPL_COD_CLIENTE CLIENTE,
                         CANT_DIAS,
                         CASE WHEN PERCON_CONCEPTO = 12 THEN
                          
                       
                               PERI005.PP_DIAS_TRABA_MONTA(P_LEGAJO       => EMPL_LEGAJO,
                                                           P_EMPRESA      => EMPL_EMPRESA,
                                                           P_FEC_INGRESO  => EMPL_FEC_INGRESO,
                                                           P_FEC_SALIDA   => EMPL_FEC_SALIDA,
                                                           P_FORMA_PAGO   => EMPL_FORMA_PAGO,
                                                           P_DESDE        => TRUNC(I_PDOC_FEC,'MM'),
                                                           P_HASTA        => LAST_DAY(I_PDOC_FEC))
                                                           
                        END CANT_DIAS_MONTA    
                    FROM PER_EMPLEADO,
                         (SELECT P.PERCON_CONCEPTO,
                                 P.PERCON_EMPR,
                                 P.PERCON_EMPLEADO,
                                 P.PERCON_MONEDA,
                                 P.PERCON_IMP,
                                 NULL DIA,
                                 P.PERCON_CANT_DIAS CANT_DIAS
                            FROM PER_EMPL_CONC P
                          UNION ALL
                          SELECT PD.PERCONDIA_CONC     PERCON_CONCEPTO,
                                 PD.PERCONDIA_EMPR     PERCON_EMPR,
                                 PD.PERCONDIA_EMPLEADO PERCON_EMPLEADO,
                                 PD.PERCONDIA_MON      PERCON_MONEDA,
                                 PD.PERCONDIA_IMP      PERCON_IMP,
                                 PD.PERCONDIA_DIA      DIA,
                                 NULL
                            FROM PER_EMPL_CONC_DIA PD) PC,
                         GEN_DEPARTAMENTO,
                         PER_DPTO_CONC,
                         FIN_CONCEPTO,
                         PER_CONCEPTO
                   WHERE EMPL_DEPARTAMENTO = DPTO_CODIGO
                     AND EMPL_EMPRESA = DPTO_EMPR
                     AND DPTOC_DPTO = DPTO_CODIGO
                     AND DPTOC_EMPR = DPTO_EMPR
                     AND DPTOC_PER_CONC = PC.PERCON_CONCEPTO
                     AND DPTOC_EMPR = PC.PERCON_EMPR
                     AND DPTOC_FIN_CONC = FCON_CLAVE
                     AND DPTOC_EMPR = FCON_EMPR
                     AND DPTOC_PER_CONC = PCON_CLAVE
                     AND DPTOC_EMPR = PCON_EMPR
                     AND EMPL_LEGAJO = PC.PERCON_EMPLEADO
                     AND EMPL_EMPRESA = PC.PERCON_EMPR
                     AND PER_EMPLEADO.EMPL_SUCURSAL = S_SUCURSAL
                     AND PC.PERCON_CONCEPTO = S_CLAVE_CONCEPTO
                     AND PC.PERCON_MONEDA = S_MON
                     AND (EMPL_FORMA_PAGO = I_EMPL_FORMA_PAGO OR
                         I_EMPL_FORMA_PAGO IS NULL)
                     AND (PC.DIA = TO_NUMBER(TO_CHAR(I_PDOC_FEC, 'DD')) OR
                         PC.DIA IS NULL)
                     AND EMPL_EMPRESA = I_EMPRESA
                     AND EMPL_SITUACION = 'A'
                     AND (EMPL_BCO_PAGO = I_BCO_PAGO OR I_BCO_PAGO IS NULL)
                   /*ESTA PARTE SIRVE PARA VER QUE EMPLEADO TRABAJO DURANTE LOS DIAS FERIADOS QUE SEAN MENSUALEROS
                     SE CALCULA POR EL IMPORTE DE HORA NORMAL YA QUE DEL SALARIO NO SE DESCUENTA ESE FERIADO Y YA SE PAGA
                     POR EL DIA DE MANERA NORMAL, ESTO ES SOLO PARA PAGAR POR EL RESTO DEL IMPORTE
                   */
                     ---
                     UNION ALL
                      SELECT EMPL_LEGAJO,
                             EMPL_NOMBRE || ' ' || EMPL_APE NOMBRE,
                             DPTO_DESC,
                             EMPL_CODIGO_PROV,
                             NVL(A.EMPL_PAGA_IPS, 'N')EMPL_PAGA_IPS,
                             NVL(C.PCON_IND_IPS, 'N')PCON_IND_IPS,
                             (A.EMPL_IMP_HORA_N_D * 8) * COUNT(DISTINCT(T.MARC_FECHA)) IMPORTE,
                             FCON_CLAVE,
                             FCON_CLAVE_CTACO,
                             FCON_TIPO_SALDO,
                             NVL(C.PCON_IND_SUMA_IPS, 'N'),
                             EMPL_DEPARTAMENTO,
                             EMPL_FORMA_PAGO,
                             EMPL_COD_CLIENTE,
                             NULL,
                             NULL
                        FROM PER_MARCACION_DIARIA T,
                             PER_EMPLEADO         A,
                             GEN_DEPARTAMENTO     D,
                             PER_CONCEPTO         C,
                             PER_DPTO_CONC        ,
                             FIN_CONCEPTO
                       WHERE T.MARC_EMPLEADO = A.EMPL_LEGAJO
                         AND T.MARC_EMPR = A.EMPL_EMPRESA
                         AND A.EMPL_DEPARTAMENTO = D.DPTO_CODIGO
                         AND A.EMPL_EMPRESA = D.DPTO_EMPR
                         AND EMPL_EMPRESA = C.PCON_EMPR

                         AND DPTOC_DPTO = DPTO_CODIGO
                         AND DPTOC_EMPR = DPTO_EMPR
                         AND DPTOC_FIN_CONC = FCON_CLAVE
                         AND DPTOC_EMPR = FCON_EMPR
                         AND DPTOC_PER_CONC = PCON_CLAVE
                         AND DPTOC_EMPR = PCON_EMPR
                         and t.marc_origen <> 'C'
                         AND C.PCON_CLAVE =5
                         AND T.MARC_EMPR = 1
                         AND T.MARC_EMPLEADO IN (SELECT EMPL_LEGAJO
                                                   FROM PER_EMPLEADO A
                                                  WHERE EMPL_EMPRESA = I_EMPRESA
                                                    AND A.EMPL_SITUACION = 'A'
                                                    AND A.EMPL_FORMA_PAGO = 2
                                                    AND A.EMPL_TIPO_SALARIO = 1
                                                   --- AND NVL(A.EMPL_CALC_HR_EXT, 'N') = 'N'
                                                    AND A.EMPL_SUCURSAL <> 2)
                         AND MARC_FECHA IN
                             (SELECT FER_FEC
                                FROM GEN_FERIADO_V A
                               WHERE FER_EMPR = 1
                                 AND TO_CHAR(A.FER_FEC, 'MM/YYYY') =TO_CHAR(I_PDOC_FEC, 'MM/YYYY')
                                 )
                         AND EMPL_SUCURSAL  = S_SUCURSAL
                         AND PCON_CLAVE = S_CLAVE_CONCEPTO
                          AND (EMPL_FORMA_PAGO = I_EMPL_FORMA_PAGO OR I_EMPL_FORMA_PAGO IS NULL)
                       GROUP BY EMPL_LEGAJO,
                             EMPL_NOMBRE || ' ' || EMPL_APE ,
                             DPTO_DESC,
                             EMPL_CODIGO_PROV,
                             NVL(A.EMPL_PAGA_IPS, 'N'),
                             NVL(C.PCON_IND_IPS, 'N'),
                             A.EMPL_IMP_HORA_N_D ,
                             FCON_CLAVE,
                             FCON_CLAVE_CTACO,
                             FCON_TIPO_SALDO,
                             NVL(C.PCON_IND_SUMA_IPS, 'N'),
                             EMPL_DEPARTAMENTO,
                             EMPL_FORMA_PAGO,
                             EMPL_COD_CLIENTE
                   ORDER BY 3, 2) LOOP
          --  RAISE_APPLICATION_ERROR(-20020, 'yuyui');
          V_PDOC_EMPLEADO := R.EMPL_LEGAJO;
          V_PER_NOMBRE    := R.NOMBRE;
          V_S_FORMA_PAGO  := R.EMPL_FORMA_PAGO;

          PERI005.PP_BLOQ_PROV_CLI(P_EMPRESA  => I_EMPRESA,
                                   P_LEGAJO   => R.EMPL_LEGAJO,
                                   P_CONCEPTO => S_CLAVE_CONCEPTO);

          IF S_CLAVE_CONCEPTO = 1 AND I_EMPRESA = 1 THEN
            -----------------------------------****

            FIN_LIM_CREDITO_EMPLEADO(V_FEC_DOC            => LAST_DAY(TO_DATE(I_PDOC_FEC)),
                                     V_CLI                => NULL, -- R.CLIENTE,
                                     V_IMP_LIM_CR_EMPR    => V_IMP_LIM_CR_EMPR,
                                     V_EMPR               => I_EMPRESA,
                                     V_IMP_LIM_DISP_GRUPO => V_IMP_LIM_DISP_GRUPO,
                                     V_IMP_LIM_DISP_EMPR  => V_IMP_LIM_DISP_EMPR,
                                     V_IMP_CHEQ_DIFERIDO  => V_IMP_CHEQ_DIFERIDO,
                                     V_IMP_CHEQ_RECHAZADO => V_IMP_CHEQ_RECHAZADO,
                                     V_EMPL_LEGAJO        => R.EMPL_LEGAJO);

            ------------------------------------------******
            IF V_IMP_LIM_DISP_EMPR < R.PERCON_IMP THEN
              V_IMPORTE_CONCEPTO := V_IMP_LIM_DISP_EMPR;
            ELSE
              V_IMPORTE_CONCEPTO := R.PERCON_IMP;

            END IF;
          ELSE
            V_IMPORTE_CONCEPTO := R.PERCON_IMP;
          END IF;


   ----------MONTACARGISTA
          IF  R.CANT_DIAS_MONTA > 0 AND S_CLAVE_CONCEPTO = 12 THEN
        
           V_IMPORTE_CONCEPTO := ROUND((V_IMPORTE_CONCEPTO/30)* (30-R.CANT_DIAS_MONTA));
            --  RAISE_APPLICATION_ERROR (-20001,V_S_IMPORTE); 
          ELSE
            
          V_IMPORTE_CONCEPTO := V_IMPORTE_CONCEPTO;
          END IF;  
          
          IF V_S_FORMA_PAGO = 5 THEN
            V_PORCENTAJE_SEGURO := 0.05;
          ELSE
            V_PORCENTAJE_SEGURO := 0.09;
          END IF;

          IF R.PCON_IND_SUMA_IPS = 'S' THEN
            V_S_IMPORTE := ROUND(V_IMPORTE_CONCEPTO /
                                 (1 - V_PORCENTAJE_SEGURO),
                                 I_W_MON_DEC_IMP); -- ROUND(R.PERCON_IMP/0.91,:W_MON_DEC_IMP);
          ELSE
            V_S_IMPORTE := V_IMPORTE_CONCEPTO;
          END IF;

          V_S_SECTOR                := R.DPTO_DESC;
          V_FIN_CODIGO_PROV         := R.EMPL_CODIGO_PROV;
          V_PCON_CLAVE_CONCEPTO_FIN := R.FCON_CLAVE;
          V_PCON_CLAVE_CTACO        := R.FCON_CLAVE_CTACO;
          V_FCON_TIPO_SALDO         := R.FCON_TIPO_SALDO;
          V_PDOC_DEPARTAMENTO       := R.EMPL_DEPARTAMENTO;

          IF (R.EMPL_PAGA_IPS = 'S' OR V_S_FORMA_PAGO = 5) AND
             R.PCON_IND_IPS = 'S' THEN
            V_S_IMP_IPS := ROUND(V_S_IMPORTE * V_PORCENTAJE_SEGURO,
                                 I_W_MON_DEC_IMP);
           else
             
              V_S_IMP_IPS := 0;                   
                                 
          END IF;

          IF S_CLAVE_CONCEPTO = 2 AND I_EMPRESA = 1 THEN
            BEGIN
           SELECT S.EMPL_DEPARTAMENTO, S.EMPL_FORMA_PAGO, D.DPTO_DESC
             INTO V_PDOC_DEPARTAMENTO, V_S_FORMA_PAGO, V_S_SECTOR
              FROM PER_EMPLEADO S,
                   GEN_DEPARTAMENTO  D,
                   PER_FORMA_PAGO    P
             WHERE S.EMPL_DEPARTAMENTO = D.DPTO_CODIGO
               AND S.EMPL_EMPRESA = D.DPTO_EMPR
               AND S.EMPL_FORMA_PAGO = P.FORMA_CODIGO
               AND S.EMPL_EMPRESA = P.FORMA_EMPR
               AND S.EMPL_LEGAJO = R.EMPL_LEGAJO
               AND S.EMPL_FORMA_PAGO IN (5, 2)
               AND S.EMPL_EMPRESA = 1;
          EXCEPTION WHEN NO_DATA_FOUND THEN
             SELECT S.EMPL_DEPARTAMENTO, S.EMPL_FORMA_PAGO, D.DPTO_DESC
              INTO V_PDOC_DEPARTAMENTO, V_S_FORMA_PAGO, V_S_SECTOR
              FROM PER_EMPL_PAGO_HIS S,
                   GEN_DEPARTAMENTO  D,
                   PER_FORMA_PAGO    P
             WHERE S.EMPL_DEPARTAMENTO = D.DPTO_CODIGO
               AND S.EMPL_EMPR = D.DPTO_EMPR
               AND S.EMPL_FORMA_PAGO = P.FORMA_CODIGO
               AND S.EMPL_EMPR = P.FORMA_EMPR
               AND S.EMPL_LEGAJO = R.EMPL_LEGAJO
               AND S.EMPL_FORMA_PAGO IN (5, 2)
               AND S.EMPL_EMPR = 1
               AND S.EMPL_FECHA_MOD =
                   (SELECT MAX(EMPL_FECHA_MOD)
                      FROM PER_EMPL_PAGO_HIS
                     WHERE EMPL_LEGAJO = R.EMPL_LEGAJO
                       AND EMPL_FORMA_PAGO IN (5, 2)
                       AND EMPL_EMPR = 1);


          END;


          END IF;

         IF S_CLAVE_CONCEPTO = 1 AND I_EMPRESA = 1 AND V_S_IMPORTE <20000 THEN
          APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'PERI005_ADE',
                                     P_N001            => V_PDOC_EMPLEADO,
                                     P_C001            => V_PER_NOMBRE,
                                     P_C002            => V_S_SECTOR,
                                     P_N002            => V_FIN_CODIGO_PROV,
                                     P_N003            => TO_NUMBER(V_S_IMPORTE,
                                                                    '9999999999999D99',
                                                                    'NLS_NUMERIC_CHARACTERS='',.'''),
                                     P_N004            => V_S_IMP_IPS,
                                     P_C003            => V_PCON_CLAVE_CONCEPTO_FIN,
                                     P_N005            => V_PCON_CLAVE_CTACO,
                                     P_C004            => V_FCON_TIPO_SALDO,
                                     P_C005            => V_PDOC_DEPARTAMENTO,
                                     P_C006            => V_S_FORMA_PAGO,
                                     P_C007            => R.CANT_DIAS,
                                     P_C010            => R.CANT_DIAS_MONTA);



         ELSE
           APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'PERI005',
                                     P_N001            => V_PDOC_EMPLEADO,
                                     P_C001            => V_PER_NOMBRE,
                                     P_C002            => V_S_SECTOR,
                                     P_N002            => V_FIN_CODIGO_PROV,
                                     P_N003            => TO_NUMBER(V_S_IMPORTE,
                                                                    '9999999999999D99',
                                                                    'NLS_NUMERIC_CHARACTERS='',.'''),
                                     P_N004            => V_S_IMP_IPS,
                                     P_C003            => V_PCON_CLAVE_CONCEPTO_FIN,
                                     P_N005            => V_PCON_CLAVE_CTACO,
                                     P_C004            => V_FCON_TIPO_SALDO,
                                     P_C005            => V_PDOC_DEPARTAMENTO,
                                     P_C006            => V_S_FORMA_PAGO,
                                     P_C007            => R.CANT_DIAS,
                                     P_C010            => R.CANT_DIAS_MONTA);

         END IF;

        END LOOP;
      ELSE

        FOR R IN (SELECT EMPL_LEGAJO,
                         EMPL_NOMBRE || ' ' || EMPL_APE NOMBRE,
                         DPTO_DESC,
                         EMPL_CODIGO_PROV,
                         NVL(EMPL_PAGA_IPS, 'N') EMPL_PAGA_IPS,
                         NVL(PCON_IND_IPS, 'N') PCON_IND_IPS,
                         PC.PERCON_IMP,
                         FCON_CLAVE,
                         FCON_CLAVE_CTACO,
                         FCON_TIPO_SALDO,
                         NVL(PCON_IND_SUMA_IPS, 'N') PCON_IND_SUMA_IPS,
                         EMPL_DEPARTAMENTO,
                         EMPL_FORMA_PAGO,
                         EMPL_COD_CLIENTE CLIENTE,
                         CANT_DIAS,
                         CASE WHEN PERCON_CONCEPTO = 12 THEN
                          
                       
                               PERI005.PP_DIAS_TRABA_MONTA(P_LEGAJO       => EMPL_LEGAJO,
                                                           P_EMPRESA      => EMPL_EMPRESA,
                                                           P_FEC_INGRESO  => EMPL_FEC_INGRESO,
                                                           P_FEC_SALIDA   => EMPL_FEC_SALIDA,
                                                           P_FORMA_PAGO   => EMPL_FORMA_PAGO,
                                                           P_DESDE        => TRUNC(I_PDOC_FEC,'MM'),
                                                           P_HASTA        => LAST_DAY(I_PDOC_FEC))
                                                           
                        END CANT_DIAS_MONTA    
                    FROM PER_EMPLEADO,
                         (SELECT P.PERCON_CONCEPTO,
                                 P.PERCON_EMPR,
                                 P.PERCON_EMPLEADO,
                                 P.PERCON_MONEDA,
                                 P.PERCON_IMP,
                                 NULL DIA,
                                 P.PERCON_CANT_DIAS CANT_DIAS
                            FROM PER_EMPL_CONC P
                          UNION ALL
                          SELECT PD.PERCONDIA_CONC     PERCON_CONCEPTO,
                                 PD.PERCONDIA_EMPR     PERCON_EMPR,
                                 PD.PERCONDIA_EMPLEADO PERCON_EMPLEADO,
                                 PD.PERCONDIA_MON      PERCON_MONEDA,
                                 PD.PERCONDIA_IMP      PERCON_IMP,
                                 PD.PERCONDIA_DIA      DIA,
                                 NULL
                            FROM PER_EMPL_CONC_DIA PD) PC,
                         GEN_DEPARTAMENTO,
                         PER_DPTO_CONC,
                         FIN_CONCEPTO,
                         PER_CONCEPTO
                   WHERE EMPL_DEPARTAMENTO = DPTO_CODIGO
                     AND EMPL_EMPRESA = DPTO_EMPR
                     AND DPTOC_DPTO = DPTO_CODIGO
                     AND DPTOC_EMPR = DPTO_EMPR
                     AND DPTOC_PER_CONC = PC.PERCON_CONCEPTO
                     AND DPTOC_EMPR = PC.PERCON_EMPR
                     AND DPTOC_FIN_CONC = FCON_CLAVE
                     AND DPTOC_EMPR = FCON_EMPR
                     AND DPTOC_PER_CONC = PCON_CLAVE
                     AND DPTOC_EMPR = PCON_EMPR
                     AND EMPL_LEGAJO = PC.PERCON_EMPLEADO
                     AND EMPL_EMPRESA = PC.PERCON_EMPR
                     AND PC.PERCON_CONCEPTO = S_CLAVE_CONCEPTO
                     AND PC.PERCON_MONEDA = S_MON
                     AND (EMPL_FORMA_PAGO = I_EMPL_FORMA_PAGO OR
                         I_EMPL_FORMA_PAGO IS NULL)
                     AND (PC.DIA = TO_NUMBER(TO_CHAR(I_PDOC_FEC, 'DD')) OR
                         PC.DIA IS NULL)
                     AND (EMPL_BCO_PAGO = I_BCO_PAGO OR I_BCO_PAGO IS NULL)
                     AND EMPL_EMPRESA = I_EMPRESA
                     AND EMPL_SITUACION = 'A'
                     UNION ALL
                      /*ESTA PARTE SIRVE PARA VER QUE EMPLEADO TRABAJO DURANTE LOS DIAS FERIADOS QUE SEAN MENSUALEROS
                     SE CALCULA POR EL IMPORTE DE HORA NORMAL YA QUE DEL SALARIO NO SE DESCUENTA ESE FERIADO Y YA SE PAGA
                     POR EL DIA DE MANERA NORMAL, ESTO ES SOLO PARA PAGAR POR EL RESTO DEL IMPORTE
                   */
                     ---

                      SELECT EMPL_LEGAJO,
                             EMPL_NOMBRE || ' ' || EMPL_APE NOMBRE,
                             DPTO_DESC,
                             EMPL_CODIGO_PROV,
                             NVL(A.EMPL_PAGA_IPS, 'N')EMPL_PAGA_IPS,
                             NVL(C.PCON_IND_IPS, 'N')PCON_IND_IPS,
                             (A.EMPL_IMP_HORA_N_D * 8) * COUNT(DISTINCT(T.MARC_FECHA)) IMPORTE,
                             FCON_CLAVE,
                             FCON_CLAVE_CTACO,
                             FCON_TIPO_SALDO,
                             NVL(C.PCON_IND_SUMA_IPS, 'N'),
                             EMPL_DEPARTAMENTO,
                             EMPL_FORMA_PAGO,
                             EMPL_COD_CLIENTE,
                             NULL,
                             NULL
                        FROM PER_MARCACION_DIARIA T,
                             PER_EMPLEADO         A,
                             GEN_DEPARTAMENTO     D,
                             PER_CONCEPTO         C,
                             PER_DPTO_CONC        ,
                             FIN_CONCEPTO
                       WHERE T.MARC_EMPLEADO = A.EMPL_LEGAJO
                         AND T.MARC_EMPR = A.EMPL_EMPRESA
                         AND A.EMPL_DEPARTAMENTO = D.DPTO_CODIGO
                         AND A.EMPL_EMPRESA = D.DPTO_EMPR
                         AND EMPL_EMPRESA = C.PCON_EMPR

                         AND DPTOC_DPTO = DPTO_CODIGO
                         AND DPTOC_EMPR = DPTO_EMPR
                         AND DPTOC_FIN_CONC = FCON_CLAVE
                         AND DPTOC_EMPR = FCON_EMPR
                         AND DPTOC_PER_CONC = PCON_CLAVE
                         AND DPTOC_EMPR = PCON_EMPR

                         AND C.PCON_CLAVE =5
                         AND T.MARC_EMPR = 1
                         AND T.MARC_EMPLEADO IN (SELECT EMPL_LEGAJO
                                                   FROM PER_EMPLEADO A
                                                  WHERE EMPL_EMPRESA = I_EMPRESA
                                                    AND A.EMPL_SITUACION = 'A'
                                                    AND A.EMPL_FORMA_PAGO = 2
                                                    AND A.EMPL_TIPO_SALARIO = 1
                                                   -- AND NVL(A.EMPL_CALC_HR_EXT, 'N') = 'N'
                                                    AND A.EMPL_SUCURSAL <> 2)
                         AND MARC_FECHA IN
                             (SELECT FER_FEC
                                FROM GEN_FERIADO_V A
                               WHERE FER_EMPR = 1
                                 AND TO_CHAR(A.FER_FEC, 'MM/YYYY') = TO_CHAR(I_PDOC_FEC, 'MM/YYYY'))
                          AND PCON_CLAVE = S_CLAVE_CONCEPTO
                          AND (EMPL_FORMA_PAGO = I_EMPL_FORMA_PAGO OR I_EMPL_FORMA_PAGO IS NULL)
                       GROUP BY EMPL_LEGAJO,
                             EMPL_NOMBRE || ' ' || EMPL_APE ,
                             DPTO_DESC,
                             EMPL_CODIGO_PROV,
                             NVL(A.EMPL_PAGA_IPS, 'N'),
                             NVL(C.PCON_IND_IPS, 'N'),
                             A.EMPL_IMP_HORA_N_D ,
                             FCON_CLAVE,
                             FCON_CLAVE_CTACO,
                             FCON_TIPO_SALDO,
                             NVL(C.PCON_IND_SUMA_IPS, 'N'),
                             EMPL_DEPARTAMENTO,
                             EMPL_FORMA_PAGO,
                             EMPL_COD_CLIENTE

                   ORDER BY 3, 2) LOOP

          V_PDOC_EMPLEADO := R.EMPL_LEGAJO;
          V_PER_NOMBRE    := R.NOMBRE;

          PERI005.PP_BLOQ_PROV_CLI(P_EMPRESA  => I_EMPRESA,
                                   P_LEGAJO   => R.EMPL_LEGAJO,
                                   P_CONCEPTO => S_CLAVE_CONCEPTO);

          IF S_CLAVE_CONCEPTO = 1 AND I_EMPRESA  = 1 THEN
            -----------------------------------****

            FIN_LIM_CREDITO_EMPLEADO(V_FEC_DOC            => LAST_DAY(TO_DATE(I_PDOC_FEC)),
                                     V_CLI                => R.CLIENTE,
                                     V_IMP_LIM_CR_EMPR    => V_IMP_LIM_CR_EMPR,
                                     V_EMPR               => I_EMPRESA,
                                     V_IMP_LIM_DISP_GRUPO => V_IMP_LIM_DISP_GRUPO,
                                     V_IMP_LIM_DISP_EMPR  => V_IMP_LIM_DISP_EMPR,
                                     V_IMP_CHEQ_DIFERIDO  => V_IMP_CHEQ_DIFERIDO,
                                     V_IMP_CHEQ_RECHAZADO => V_IMP_CHEQ_RECHAZADO,
                                     V_EMPL_LEGAJO        => R.EMPL_LEGAJO);

            ------------------------------------------******

            IF V_IMP_LIM_DISP_EMPR < R.PERCON_IMP THEN
              V_IMPORTE_CONCEPTO := V_IMP_LIM_DISP_EMPR;
            ELSE
              V_IMPORTE_CONCEPTO := R.PERCON_IMP;

            END IF;
            --   IF R.CLIENTE IS NULL THEN
            --      V_IMPORTE_CONCEPTO := 0;
            -- END IF;
          ELSE
            V_IMPORTE_CONCEPTO := R.PERCON_IMP;
          END IF;
          
          
             ----------MONTACARGISTA
          IF  R.CANT_DIAS_MONTA > 0 AND S_CLAVE_CONCEPTO = 12 THEN
        
           V_IMPORTE_CONCEPTO := ROUND((V_IMPORTE_CONCEPTO/30)* (30-R.CANT_DIAS_MONTA));
            --  RAISE_APPLICATION_ERROR (-20001,V_S_IMPORTE); 
          ELSE
            
          V_IMPORTE_CONCEPTO := V_IMPORTE_CONCEPTO;
          END IF;  
          

          IF R.PCON_IND_SUMA_IPS = 'S' THEN
            V_S_IMPORTE := ROUND(V_IMPORTE_CONCEPTO / 0.91, I_W_MON_DEC_IMP);
          ELSE
            V_S_IMPORTE := V_IMPORTE_CONCEPTO;
          END IF;

          V_S_SECTOR                := R.DPTO_DESC;
          V_FIN_CODIGO_PROV         := R.EMPL_CODIGO_PROV;
          V_PCON_CLAVE_CONCEPTO_FIN := R.FCON_CLAVE;
          V_PCON_CLAVE_CTACO        := R.FCON_CLAVE_CTACO;
          V_FCON_TIPO_SALDO         := R.FCON_TIPO_SALDO;
          V_PDOC_DEPARTAMENTO       := R.EMPL_DEPARTAMENTO;
          V_S_FORMA_PAGO            := R.EMPL_FORMA_PAGO;

          IF V_S_FORMA_PAGO = 5 THEN
            V_PORCENTAJE_SEGURO := 0.05;
          ELSE
            V_PORCENTAJE_SEGURO := 0.09;
          END IF;

          IF (R.EMPL_PAGA_IPS = 'S' OR V_S_FORMA_PAGO = 5) AND
             R.PCON_IND_IPS = 'S' THEN
            V_S_IMP_IPS := ROUND(V_S_IMPORTE * V_PORCENTAJE_SEGURO,
                                 I_W_MON_DEC_IMP);
          END IF;

          IF S_CLAVE_CONCEPTO = 2 AND I_EMPRESA = 1 THEN

        BEGIN
           SELECT S.EMPL_DEPARTAMENTO, S.EMPL_FORMA_PAGO, D.DPTO_DESC
             INTO V_PDOC_DEPARTAMENTO, V_S_FORMA_PAGO, V_S_SECTOR
              FROM PER_EMPLEADO S,
                   GEN_DEPARTAMENTO  D,
                   PER_FORMA_PAGO    P
             WHERE S.EMPL_DEPARTAMENTO = D.DPTO_CODIGO
               AND S.EMPL_EMPRESA = D.DPTO_EMPR
               AND S.EMPL_FORMA_PAGO = P.FORMA_CODIGO
               AND S.EMPL_EMPRESA = P.FORMA_EMPR
               AND S.EMPL_LEGAJO = R.EMPL_LEGAJO
               AND S.EMPL_FORMA_PAGO IN (5, 2)
               AND S.EMPL_EMPRESA = 1;
          EXCEPTION WHEN NO_DATA_FOUND THEN
             SELECT S.EMPL_DEPARTAMENTO, S.EMPL_FORMA_PAGO, D.DPTO_DESC
              INTO V_PDOC_DEPARTAMENTO, V_S_FORMA_PAGO, V_S_SECTOR
              FROM PER_EMPL_PAGO_HIS S,
                   GEN_DEPARTAMENTO  D,
                   PER_FORMA_PAGO    P
             WHERE S.EMPL_DEPARTAMENTO = D.DPTO_CODIGO
               AND S.EMPL_EMPR = D.DPTO_EMPR
               AND S.EMPL_FORMA_PAGO = P.FORMA_CODIGO
               AND S.EMPL_EMPR = P.FORMA_EMPR
               AND S.EMPL_LEGAJO = R.EMPL_LEGAJO
               AND S.EMPL_FORMA_PAGO IN (5, 2)
               AND S.EMPL_EMPR = 1
               AND S.EMPL_FECHA_MOD =
                   (SELECT MAX(EMPL_FECHA_MOD)
                      FROM PER_EMPL_PAGO_HIS
                     WHERE EMPL_LEGAJO = R.EMPL_LEGAJO
                       AND EMPL_FORMA_PAGO IN (5, 2)
                       AND EMPL_EMPR = 1);


          END;

          END IF;
      IF S_CLAVE_CONCEPTO = 1 AND I_EMPRESA = 1 AND V_S_IMPORTE <20000 THEN
          APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'PERI005_ADE',
                                     P_N001            => V_PDOC_EMPLEADO,
                                     P_C001            => V_PER_NOMBRE,
                                     P_C002            => V_S_SECTOR,
                                     P_N002            => V_FIN_CODIGO_PROV,
                                     P_N003            => TO_NUMBER(V_S_IMPORTE,
                                                                    '9999999999999D99',
                                                                    'NLS_NUMERIC_CHARACTERS='',.'''),
                                     P_N004            => V_S_IMP_IPS,
                                     P_C003            => V_PCON_CLAVE_CONCEPTO_FIN,
                                     P_N005            => V_PCON_CLAVE_CTACO,
                                     P_C004            => V_FCON_TIPO_SALDO,
                                     P_C005            => V_PDOC_DEPARTAMENTO,
                                     P_C006            => V_S_FORMA_PAGO,
                                     P_C007            => R.CANT_DIAS,
                                     P_C010            => R.CANT_DIAS_MONTA);

      ELSE
          APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'PERI005',
                                     P_N001            => V_PDOC_EMPLEADO,
                                     P_C001            => V_PER_NOMBRE,
                                     P_C002            => V_S_SECTOR,
                                     P_N002            => V_FIN_CODIGO_PROV,
                                     P_N003            => TO_NUMBER(V_S_IMPORTE,
                                                                    '9999999999999D99',
                                                                    'NLS_NUMERIC_CHARACTERS='',.'''),
                                     P_N004            => V_S_IMP_IPS,
                                     P_C003            => V_PCON_CLAVE_CONCEPTO_FIN,
                                     P_N005            => V_PCON_CLAVE_CTACO,
                                     P_C004            => V_FCON_TIPO_SALDO,
                                     P_C005            => V_PDOC_DEPARTAMENTO,
                                     P_C006            => V_S_FORMA_PAGO,
                                     P_C007            => R.CANT_DIAS,
                                     P_C010            => R.CANT_DIAS_MONTA);

      END IF;


        END LOOP;

      END IF;

    ELSE

      IF S_SUCURSAL IS NOT NULL THEN
    --     RAISE_APPLICATION_ERROR(-20001,I_PDOC_FEC);
      --   RAISE_APPLICATION_ERROR(-20020,S_SUCURSAL);
        FOR R IN (SELECT EMPL_LEGAJO,
                         EMPL_NOMBRE || ' ' || EMPL_APE NOMBRE,
                         DPTO_DESC,
                         EMPL_CODIGO_PROV,
                         NVL(EMPL_PAGA_IPS, 'N') EMPL_PAGA_IPS,
                         NVL(PCON_IND_IPS, 'N') PCON_IND_IPS,
                         PERCON_IMP,
                         FCON_CLAVE,
                         FCON_CLAVE_CTACO,
                         FCON_TIPO_SALDO,
                         NVL(PCON_IND_SUMA_IPS, 'N') PCON_IND_SUMA_IPS,
                         EMPL_DEPARTAMENTO,
                         EMPL_FORMA_PAGO
                    FROM PER_EMPLEADO,
                         --PER_EMPL_CONC,
                         (SELECT P.PERCON_CONCEPTO,
                                 P.PERCON_EMPR,
                                 P.PERCON_EMPLEADO,
                                 P.PERCON_MONEDA,
                                 P.PERCON_IMP,
                                 NULL DIA
                            FROM PER_EMPL_CONC P
                          UNION ALL
                          SELECT PD.PERCONDIA_CONC     PERCON_CONCEPTO,
                                 PD.PERCONDIA_EMPR     PERCON_EMPR,
                                 PD.PERCONDIA_EMPLEADO PERCON_EMPLEADO,
                                 PD.PERCONDIA_MON      PERCON_MONEDA,
                                 PD.PERCONDIA_IMP      PERCON_IMP,
                                 PD.PERCONDIA_DIA      DIA
                            FROM PER_EMPL_CONC_DIA PD) PC,
                         GEN_DEPARTAMENTO,
                         PER_DPTO_CONC,
                         FIN_CONCEPTO,
                         PER_CONCEPTO
                   WHERE EMPL_EMPRESA = 2---I_EMPRESA

                     AND EMPL_DEPARTAMENTO = DPTO_CODIGO
                     AND EMPL_EMPRESA = DPTO_EMPR

                     AND DPTOC_DPTO = DPTO_CODIGO
                     AND DPTOC_EMPR = DPTO_EMPR

                     AND DPTOC_PER_CONC = PERCON_CONCEPTO
                     AND DPTOC_EMPR = PERCON_EMPR

                     AND DPTOC_FIN_CONC = FCON_CLAVE
                     AND DPTOC_EMPR = FCON_EMPR

                     AND DPTOC_PER_CONC = PCON_CLAVE
                     AND DPTOC_EMPR = PCON_EMPR

                     AND EMPL_LEGAJO = PERCON_EMPLEADO
                     AND EMPL_EMPRESA = PERCON_EMPR
                     AND EMPL_SITUACION = 'A'
                     AND PER_EMPLEADO.EMPL_SUCURSAL = S_SUCURSAL
                     AND PERCON_CONCEPTO = S_CLAVE_CONCEPTO
                     AND PERCON_MONEDA = S_MON
                     AND CASE
                           WHEN PC.DIA = TO_NUMBER(TO_CHAR(I_PDOC_FEC, 'DD')) THEN
                            1
                           WHEN 15 != TO_NUMBER(TO_CHAR(I_PDOC_FEC, 'DD')) AND
                                PC.DIA IS NULL THEN
                            1
                           ELSE
                            0
                         END = 1
                     AND (EMPL_FORMA_PAGO = I_EMPL_FORMA_PAGO OR
                         I_EMPL_FORMA_PAGO IS NULL)
                   ORDER BY 3, 2

                  ) LOOP
--RAISE_APPLICATION_ERROR(-20001,'ADFADSF');
          V_PDOC_EMPLEADO := R.EMPL_LEGAJO;
          V_PER_NOMBRE    := R.NOMBRE;
          V_S_FORMA_PAGO  := R.EMPL_FORMA_PAGO;

          IF V_S_FORMA_PAGO = 5 THEN
            V_PORCENTAJE_SEGURO := 0.05;
          ELSE
            V_PORCENTAJE_SEGURO := 0.09;
          END IF;

          IF R.PCON_IND_SUMA_IPS = 'S' THEN
            V_S_IMPORTE := ROUND(R.PERCON_IMP / (1 - V_PORCENTAJE_SEGURO),
                                 I_W_MON_DEC_IMP); -- ROUND(R.PERCON_IMP/0.91,:W_MON_DEC_IMP);
          ELSE
            V_S_IMPORTE := R.PERCON_IMP;
          END IF;

          V_S_SECTOR                := R.DPTO_DESC;
          V_FIN_CODIGO_PROV         := R.EMPL_CODIGO_PROV;
          V_PCON_CLAVE_CONCEPTO_FIN := R.FCON_CLAVE;
          V_PCON_CLAVE_CTACO        := R.FCON_CLAVE_CTACO;
          V_FCON_TIPO_SALDO         := R.FCON_TIPO_SALDO;
          V_PDOC_DEPARTAMENTO       := R.EMPL_DEPARTAMENTO;

          IF R.EMPL_PAGA_IPS = 'S' AND R.PCON_IND_IPS = 'S' THEN
            V_S_IMP_IPS := ROUND(V_S_IMPORTE * V_PORCENTAJE_SEGURO,
                                 I_W_MON_DEC_IMP);
          END IF;


          APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'PERI005',
                                     P_N001            => V_PDOC_EMPLEADO,
                                     P_C001            => V_PER_NOMBRE,
                                     P_C002            => V_S_SECTOR,
                                     P_N002            => V_FIN_CODIGO_PROV,
                                     P_N003            => TO_NUMBER(V_S_IMPORTE,
                                                                    '9999999999999D99',
                                                                    'NLS_NUMERIC_CHARACTERS='',.'''),
                                     P_N004            => V_S_IMP_IPS,
                                     P_C003            => V_PCON_CLAVE_CONCEPTO_FIN,
                                     P_N005            => V_PCON_CLAVE_CTACO,
                                     P_C004            => V_FCON_TIPO_SALDO,
                                     P_C005            => V_PDOC_DEPARTAMENTO,
                                     P_C006            => V_S_FORMA_PAGO);
        END LOOP;

      ELSE

        FOR R IN (SELECT EMPL_LEGAJO,
                         EMPL_NOMBRE || ' ' || EMPL_APE NOMBRE,
                         DPTO_DESC,
                         EMPL_CODIGO_PROV,
                         NVL(EMPL_PAGA_IPS, 'N') EMPL_PAGA_IPS,
                         NVL(PCON_IND_IPS, 'N') PCON_IND_IPS,
                         PERCON_IMP,
                         FCON_CLAVE,
                         FCON_CLAVE_CTACO,
                         FCON_TIPO_SALDO,
                         NVL(PCON_IND_SUMA_IPS, 'N') PCON_IND_SUMA_IPS,
                         EMPL_DEPARTAMENTO,
                         EMPL_FORMA_PAGO
                    FROM PER_EMPLEADO,
                         --PER_EMPL_CONC,
                         (SELECT P.PERCON_CONCEPTO,
                                 P.PERCON_EMPR,
                                 P.PERCON_EMPLEADO,
                                 P.PERCON_MONEDA,
                                 P.PERCON_IMP,
                                 NULL DIA
                            FROM PER_EMPL_CONC P
                          UNION ALL
                          SELECT PD.PERCONDIA_CONC     PERCON_CONCEPTO,
                                 PD.PERCONDIA_EMPR     PERCON_EMPR,
                                 PD.PERCONDIA_EMPLEADO PERCON_EMPLEADO,
                                 PD.PERCONDIA_MON      PERCON_MONEDA,
                                 PD.PERCONDIA_IMP      PERCON_IMP,
                                 PD.PERCONDIA_DIA      DIA
                            FROM PER_EMPL_CONC_DIA PD) PC,
                         GEN_DEPARTAMENTO,
                         PER_DPTO_CONC,
                         FIN_CONCEPTO,
                         PER_CONCEPTO
                   WHERE EMPL_EMPRESA = I_EMPRESA

                     AND EMPL_DEPARTAMENTO = DPTO_CODIGO
                     AND EMPL_EMPRESA = DPTO_EMPR

                     AND DPTOC_DPTO = DPTO_CODIGO
                     AND DPTOC_EMPR = DPTO_EMPR

                     AND DPTOC_PER_CONC = PERCON_CONCEPTO
                     AND DPTOC_EMPR = PERCON_EMPR

                     AND DPTOC_FIN_CONC = FCON_CLAVE
                     AND DPTOC_EMPR = FCON_EMPR

                     AND DPTOC_PER_CONC = PCON_CLAVE
                     AND DPTOC_EMPR = PCON_EMPR

                     AND EMPL_LEGAJO = PERCON_EMPLEADO
                     AND EMPL_EMPRESA = PERCON_EMPR
                     AND EMPL_SITUACION = 'A'
                     AND PERCON_CONCEPTO = S_CLAVE_CONCEPTO
                     AND PERCON_MONEDA = S_MON
                     AND CASE
                           WHEN PC.DIA = TO_NUMBER(TO_CHAR(I_PDOC_FEC, 'DD')) THEN
                            1
                           WHEN 15 != TO_NUMBER(TO_CHAR(I_PDOC_FEC, 'DD')) AND
                                PC.DIA IS NULL THEN
                            1
                           ELSE
                            0
                         END = 1
                     AND (EMPL_FORMA_PAGO = I_EMPL_FORMA_PAGO OR
                         I_EMPL_FORMA_PAGO IS NULL)
                   ORDER BY 3, 2

                  ) LOOP

          V_PDOC_EMPLEADO := R.EMPL_LEGAJO;
          V_PER_NOMBRE    := R.NOMBRE;

          IF R.PCON_IND_SUMA_IPS = 'S' THEN
            V_S_IMPORTE := ROUND(R.PERCON_IMP / 0.91, I_W_MON_DEC_IMP);
          ELSE
            V_S_IMPORTE := R.PERCON_IMP;
          END IF;

          V_S_SECTOR                := R.DPTO_DESC;
          V_FIN_CODIGO_PROV         := R.EMPL_CODIGO_PROV;
          V_PCON_CLAVE_CONCEPTO_FIN := R.FCON_CLAVE;
          V_PCON_CLAVE_CTACO        := R.FCON_CLAVE_CTACO;
          V_FCON_TIPO_SALDO         := R.FCON_TIPO_SALDO;
          V_PDOC_DEPARTAMENTO       := R.EMPL_DEPARTAMENTO;
          V_S_FORMA_PAGO            := R.EMPL_FORMA_PAGO;

          IF V_S_FORMA_PAGO = 5 THEN
            V_PORCENTAJE_SEGURO := 0.05;
          ELSE
            V_PORCENTAJE_SEGURO := 0.09;
          END IF;

          IF R.EMPL_PAGA_IPS = 'S' AND R.PCON_IND_IPS = 'S' THEN
            V_S_IMP_IPS := ROUND(V_S_IMPORTE * V_PORCENTAJE_SEGURO,
                                 I_W_MON_DEC_IMP);
          END IF;
          -- END IF;
          APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'PERI005',
                                     P_N001            => V_PDOC_EMPLEADO,
                                     P_C001            => V_PER_NOMBRE,
                                     P_C002            => V_S_SECTOR,
                                     P_N002            => V_FIN_CODIGO_PROV,
                                     P_N003            => TO_NUMBER(V_S_IMPORTE,
                                                                    '9999999999999D99',
                                                                    'NLS_NUMERIC_CHARACTERS='',.'''),
                                     P_N004            => V_S_IMP_IPS,
                                     P_C003            => V_PCON_CLAVE_CONCEPTO_FIN,
                                     P_N005            => V_PCON_CLAVE_CTACO,
                                     P_C004            => V_FCON_TIPO_SALDO,
                                     P_C005            => V_PDOC_DEPARTAMENTO,
                                     P_C006            => V_S_FORMA_PAGO);

        END LOOP;
      END IF;
    END IF;

  END;

  PROCEDURE PP_ACTUALIZAR_REGISTRO(I_EMPRESA            IN NUMBER,
                                   I_DESC_EMPRESA       IN VARCHAR2,
                                   I_DOC_CTA_BCO        IN NUMBER,
                                   I_PCON_DESC          IN VARCHAR2,
                                   I_PDOC_FEC           IN DATE,
                                   S_TASA_OFIC          IN NUMBER,
                                   I_S_TIP_MOV_ADEL_PRO IN NUMBER,
                                   I_S_TIPO_MOV         IN NUMBER,
                                   I_SUCURSAL           IN NUMBER,
                                   I_PDOC_NRO_DOC       IN NUMBER,
                                   I_DOC_OBS            IN VARCHAR2,
                                   I_PDOC_QUINCENA      IN NUMBER,
                                   W_PERIODO            IN NUMBER,
                                   I_CLAVE_CONCEPTO     IN NUMBER,
                                   I_S_MON              IN NUMBER,
                                   I_PDOC_FEC_OPER      IN DATE,
                                   I_MON_LOC            IN NUMBER,
                                   I_CUOTA              IN VARCHAR2,
                                   I_FEC_VTO_ADELANTO   IN DATE) IS

    V_CLAVE_FIN NUMBER;
    V_CLAVE     NUMBER;
    -- V_PRI_CLAVE   NUMBER;
    V_CONCEPTO    NUMBER;
    S_TOT_IMPORTE NUMBER := 0;

    V_CAJA_DESC  VARCHAR2(50);
    V_CAJA_COD   VARCHAR2(50);
    V_CAJA_DESC2 VARCHAR2(50);
    V_CONCEPTO_IPS VARCHAR2(1);
  BEGIN
    
            SELECT NVL(A.PCON_IND_IPS,'N')
              INTO  V_CONCEPTO_IPS
              FROM PER_CONCEPTO A
              WHERE A.PCON_CLAVE = I_CLAVE_CONCEPTO
              AND A.PCON_EMPR = I_EMPRESA;

    IF I_S_TIPO_MOV = 31 then -->ADELANTO A PROVEEDOR
      PP_VERIF_MOSTRAR_CTA_BANC(I_BCO_DESC    => V_CAJA_DESC2,
                                I_CTA_DESC    => V_CAJA_DESC,
                                S_MON         => V_CAJA_COD,
                                I_EMPRESA     => I_EMPRESA,
                                I_DOC_CTA_BCO => I_DOC_CTA_BCO);

    END IF;


    SELECT SUM(NVL(N003, 0))
      INTO S_TOT_IMPORTE
      FROM APEX_COLLECTIONS A
     WHERE COLLECTION_NAME = 'PERI005';



    IF S_TOT_IMPORTE = 0 THEN
      RAISE_APPLICATION_ERROR(-20019,
                              'Debe elegir por lo menos un empleado!');
    END IF;

    FOR V IN (SELECT SEQ_ID,
                     N001   PDOC_EMPLEADO,
                     N002   FIN_CODIGO_PROV,
                     N003   S_IMPORTE,
                     N004   S_IMP_IPS,
                     C001   PER_NOMBRE
                FROM APEX_COLLECTIONS A
               WHERE COLLECTION_NAME = 'PERI005') LOOP
      IF V.FIN_CODIGO_PROV IS NULL AND V.S_IMPORTE > 0 THEN
        RAISE_APPLICATION_ERROR(-20018,
                                'El Empleado ' || V.PDOC_EMPLEADO || '-' ||
                                V.PER_NOMBRE ||
                                ', debe tener un codigo de proveedor');
      END IF;
      
      IF V_CONCEPTO_IPS  = 'S' AND  NVL(V.S_IMP_IPS,0) <=0 then 
        RAISE_APPLICATION_ERROR(-20018, 'El concepto ingresado debe tener IPS, favor revisar');
      END IF;

    END LOOP;

    V_CLAVE := PERI005.FP_CLAVE_DOC(I_EMPRESA => I_EMPRESA);

    --V_PRI_CLAVE := V_CLAVE;
    FOR K IN (SELECT SEQ_ID,
                     N001 PDOC_EMPLEADO,
                     N002 FIN_CODIGO_PROV,
                     N003 S_IMPORTE,
                     N004 S_IMP_IPS,
                     N005 PCON_CLAVE_CTACO,
                     C001 PER_NOMBRE,
                     C002 S_SECTOR,
                     C003 PCON_CLAVE_CONCEPTO_FIN,
                     C004 FCON_TIPO_SALDO,
                     TO_NUMBER(C005) PDOC_DEPARTAMENTO,
                     C006 S_FORMA_PAGO,
                     C007 CANT_DIAS
                FROM APEX_COLLECTIONS A
               WHERE COLLECTION_NAME = 'PERI005'
               )
     LOOP
    IF NVL(K.S_IMPORTE, 0) > 0 THEN
   --   IF (I_S_TIPO_MOV = 31 AND NVL(K.S_IMPORTE, 0) >= 20000 AND I_EMPRESA = 1 ) OR I_S_TIPO_MOV <> 31 THEN
        IF I_DOC_CTA_BCO IS NOT NULL OR (I_DESC_EMPRESA LIKE '%TRANSAGRO%' AND
           I_PCON_DESC LIKE '%AGUINALDO%') THEN
          ---RAISE_APPLICATION_ERROR(-20015,K.S_FORMA_PAGO);
          V_CLAVE_FIN := PERI005.PP_CARGAR_DOCUMENTO_FIN(I_PDOC_FEC                => I_PDOC_FEC,
                                                         S_TASA_OFIC               => S_TASA_OFIC,
                                                         I_EMPRESA                 => I_EMPRESA,
                                                         I_S_TIP_MOV_ADEL_PRO      => I_S_TIP_MOV_ADEL_PRO,
                                                         I_S_TIPO_MOV              => I_S_TIPO_MOV,
                                                         I_DESC_EMPRESA            => I_DESC_EMPRESA,
                                                         I_DOC_CTA_BCO             => I_DOC_CTA_BCO,
                                                         I_SUCURSAL                => I_SUCURSAL,
                                                         I_PDOC_NRO_DOC            => I_PDOC_NRO_DOC,
                                                         I_DOC_OBS                 => I_DOC_OBS,
                                                         I_PDOC_FEC_OPER           => I_PDOC_FEC_OPER,
                                                         I_MON_LOC                 => I_MON_LOC,
                                                         S_MON                     => I_S_MON,
                                                         I_CUOTA                   => I_CUOTA,
                                                         I_FEC_VTO_ADELANTO        => I_FEC_VTO_ADELANTO,
                                                         I_PDOC_EMPLEADO           => K.PDOC_EMPLEADO,
                                                         I_FIN_CODIGO_PROV         => K.FIN_CODIGO_PROV,
                                                         I_S_IMPORTE               => K.S_IMPORTE,
                                                         I_S_IMP_IPS               => K.S_IMP_IPS,
                                                         I_PCON_CLAVE_CTACO        => K.PCON_CLAVE_CTACO,
                                                         I_PER_NOMBRE              => K.PER_NOMBRE,
                                                         I_S_SECTOR                => K.S_SECTOR,
                                                         I_PCON_CLAVE_CONCEPTO_FIN => K.PCON_CLAVE_CONCEPTO_FIN,
                                                         I_FCON_TIPO_SALDO         => K.FCON_TIPO_SALDO,
                                                         I_PDOC_DEPARTAMENTO       => K.PDOC_DEPARTAMENTO,
                                                         I_S_FORMA_PAGO            => K.S_FORMA_PAGO);

        END IF;

        --  RAISE_APPLICATION_ERROR (-20001,V_CLAVE_FIN);
        V_CLAVE := V_CLAVE + 1;

        PP_INS_PER_DOC(V_CLAVE,
                       I_PDOC_QUINCENA,
                       K.PDOC_EMPLEADO,
                       I_PDOC_FEC,
                       I_PDOC_NRO_DOC,
                       SYSDATE,
                       GEN_DEVUELVE_USER,
                       'PERI005',
                       W_PERIODO,
                       V_CLAVE_FIN,
                       1,
                       I_CLAVE_CONCEPTO,
                       K.S_IMPORTE,
                       K.S_IMPORTE * S_TASA_OFIC,
                       I_S_MON,
                       K.PDOC_DEPARTAMENTO,
                       I_EMPRESA,
                       I_S_TIPO_MOV,
                       I_S_TIP_MOV_ADEL_PRO,
                       I_DESC_EMPRESA,
                       I_DOC_OBS,
                       K.CANT_DIAS);

        IF NVL(K.S_IMP_IPS, 0) > 0 THEN
          IF K.S_FORMA_PAGO = 5 THEN
            V_CONCEPTO := 31;
          ELSE
            V_CONCEPTO := 4;
          END IF;

          V_CLAVE := V_CLAVE + 1;
          PP_INS_PER_DOC(V_CLAVE,
                         I_PDOC_QUINCENA,
                         K.PDOC_EMPLEADO,
                         I_PDOC_FEC,
                         I_PDOC_NRO_DOC,
                         SYSDATE,
                         GEN_DEVUELVE_USER,
                         'PERI005',
                         W_PERIODO,
                         NULL,
                         1,
                         V_CONCEPTO, --CLAVE DE I.P.S. OBRERO
                         K.S_IMP_IPS,
                         K.S_IMP_IPS * S_TASA_OFIC,
                         I_S_MON,
                         K.PDOC_DEPARTAMENTO,
                         I_EMPRESA,
                         I_S_TIPO_MOV,
                         I_S_TIP_MOV_ADEL_PRO,
                         I_DESC_EMPRESA,
                         I_DOC_OBS,
                         K.CANT_DIAS);
        END IF;
     --  END IF;
      END IF;

    END LOOP;

    PERI005.PP_LLAMAR_REPORTE(P_EMPRESA => I_EMPRESA,
                              P_PERIODO => W_PERIODO,
                              P_NRO_DOC => I_PDOC_NRO_DOC);

    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20015, SQLERRM);
  END;

  FUNCTION PP_CARGAR_DOCUMENTO_FIN(I_PDOC_FEC           IN DATE,
                                   S_TASA_OFIC          IN NUMBER,
                                   I_EMPRESA            IN NUMBER,
                                   I_S_TIP_MOV_ADEL_PRO IN NUMBER,
                                   I_S_TIPO_MOV         IN NUMBER,
                                   I_DESC_EMPRESA       IN VARCHAR2,
                                   I_DOC_CTA_BCO        IN NUMBER,
                                   /*PARAMETRO*/
                                   I_SUCURSAL                IN NUMBER,
                                   I_PDOC_NRO_DOC            IN NUMBER,
                                   I_DOC_OBS                 IN VARCHAR2,
                                   I_PDOC_FEC_OPER           IN DATE,
                                   I_MON_LOC                 IN NUMBER,
                                   S_MON                     IN NUMBER,
                                   I_CUOTA                   IN VARCHAR2,
                                   I_FEC_VTO_ADELANTO        IN DATE,
                                   I_PDOC_EMPLEADO           IN NUMBER,
                                   I_FIN_CODIGO_PROV         IN NUMBER,
                                   I_S_IMPORTE               IN NUMBER,
                                   I_S_IMP_IPS               IN NUMBER,
                                   I_PCON_CLAVE_CTACO        IN NUMBER,
                                   I_PER_NOMBRE              IN VARCHAR2,
                                   I_S_SECTOR                IN VARCHAR2,
                                   I_PCON_CLAVE_CONCEPTO_FIN IN NUMBER,
                                   I_FCON_TIPO_SALDO         IN VARCHAR2,
                                   I_PDOC_DEPARTAMENTO       IN NUMBER,
                                   I_S_FORMA_PAGO            IN NUMBER)
    RETURN NUMBER IS
    V_CLAVE       NUMBER := FIN_SEQ_DOC_NEXTVAL;
    V_CLAVE_IMG   NUMBER;
    X_IMPORTE_CUO NUMBER;
    l_redondeo    gen_moneda.mon_dec_imp%type;
  begin
    <<obt_redondeo_mon>>
    begin
      select m.mon_dec_imp
      into l_redondeo
      from gen_moneda m
      where m.mon_codigo = nvl(I_MON_LOC, 1)
      and   m.mon_empr = I_EMPRESA;
          
    exception
      when no_Data_found then
        Raise_application_error(-20000, 'No se obtiene el redondeo de la moneda en la empresa');
    end obt_redondeo_mon;
    
    IF I_EMPRESA != 2 THEN

      IF I_S_TIPO_MOV = I_S_TIP_MOV_ADEL_PRO AND I_EMPRESA LIKE 1 then
        

        INSERT INTO FIN_DOCUMENTO_COMI015_TEMP
          (DOC_CLAVE,
           DOC_EMPLEADO,
           DOC_EMPR,
           DOC_CTA_BCO,
           DOC_SUC,
           DOC_TIPO_MOV,
           DOC_NRO_DOC,
           DOC_TIPO_SALDO,
           DOC_MON,
           DOC_PROV,
           DOC_CLI_NOM,
           DOC_FEC_OPER,
           DOC_FEC_DOC,
           DOC_BRUTO_EXEN_LOC,
           DOC_BRUTO_EXEN_MON,
           DOC_BRUTO_GRAV_LOC,
           DOC_BRUTO_GRAV_MON,
           DOC_NETO_EXEN_LOC,
           DOC_NETO_EXEN_MON,
           DOC_NETO_GRAV_LOC,
           DOC_NETO_GRAV_MON,
           DOC_IVA_LOC,
           DOC_IVA_MON,
           DOC_SALDO_INI_MON,
           DOC_SALDO_LOC,
           DOC_SALDO_MON,
           DOC_SALDO_PER_ACT_LOC,
           DOC_SALDO_PER_ACT_MON,
           DOC_OBS,
           DOC_BASE_IMPON_LOC,
           DOC_BASE_IMPON_MON,
           DOC_LOGIN,
           DOC_FEC_GRAB,
           DOC_SIST,
           DOC_OPERADOR)
        VALUES
          (V_CLAVE,
           I_PDOC_EMPLEADO,
           I_EMPRESA,
           I_DOC_CTA_BCO,
           I_SUCURSAL,
           I_S_TIPO_MOV,
           I_PDOC_NRO_DOC,
           'C',
           I_MON_LOC,
           I_FIN_CODIGO_PROV,
           SUBSTR(I_PER_NOMBRE, 1, 40),
           I_PDOC_FEC_OPER,
           I_PDOC_FEC,
           round(I_S_IMPORTE * NVL(S_TASA_OFIC, 1), l_redondeo),
           round(I_S_IMPORTE, l_redondeo),
           0,
           0,
           round(I_S_IMPORTE * NVL(S_TASA_OFIC, 1), l_redondeo),
           round(I_S_IMPORTE, l_redondeo),
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           I_DOC_OBS,---NULL, --'ANTIC. Personal-Modulo RRHH',
           0,
           0,
           GEN_DEVUELVE_USER,
           SYSDATE,
           'PER',
           2);

        INSERT INTO FIN_DOC_CONCEPTO_COMI015_TEMP
          (DCON_ITEM,
           DCON_CLAVE_DOC,
           DCON_CLAVE_CONCEPTO,
           DCON_CLAVE_CTACO,
           DCON_TIPO_SALDO,
           DCON_EXEN_LOC,
           DCON_EXEN_MON,
           DCON_GRAV_LOC,
           DCON_GRAV_MON,
           DCON_IVA_LOC,
           DCON_IVA_MON,
           DCON_IND_TIPO_IVA_COMPRA,
           DCON_OBS,
           DCON_EMPR)

        VALUES

          (1,
           V_CLAVE,
           I_PCON_CLAVE_CONCEPTO_FIN,
           I_PCON_CLAVE_CTACO,
           I_FCON_TIPO_SALDO,
           round(I_S_IMPORTE * NVL(S_TASA_OFIC, 1), l_redondeo),
           round(I_S_IMPORTE, l_redondeo),
           0,
           0,
           0,
           0,
           1,
           SUBSTR(I_DOC_OBS || '-' || SUBSTR(I_PER_NOMBRE, 1, 20), 1, 50),
           I_EMPRESA);

        /*   IF I_FEC_VTO_ADELANTO IS NULL THEN
                  RAISE_APPLICATION_ERROR(-20001,
                                          'La fecha de la cuota no puede quedar vacia');
                ELSIF I_FEC_VTO_ADELANTO < I_PDOC_FEC THEN
                  RAISE_APPLICATION_ERROR(-20001,
                                          'La fecha de vencimiento no puede ser menor que la fecha de documento');
                ELSIF I_FEC_VTO_ADELANTO > LAST_DAY(I_PDOC_FEC) THEN
                  RAISE_APPLICATION_ERROR(-20001,
                                          'La fecha de vencimiento mayor que la ultima fecha del mes del documento');
                END IF;
        */
        INSERT INTO FIN_CUOTA_COMI015_TEMP
          (CUO_CLAVE_DOC, CUO_FEC_VTO, CUO_IMP_LOC, CUO_IMP_MON, CUO_EMPR)
        VALUES
          (V_CLAVE,
           LAST_DAY(I_PDOC_FEC), -- I_FEC_VTO_ADELANTO,
           round(I_S_IMPORTE * NVL(S_TASA_OFIC, 1), l_redondeo),
           round(I_S_IMPORTE, l_redondeo),
           I_EMPRESA);

        V_CLAVE_IMG := COM_SEQ_FAC_REC_NEXTVAL;

        INSERT INTO COM_FACTURA_REC
          (FAC_CLAVE,
           FAC_EMPR,
           FAC_SUC,
           FAC_NRO_TIMBRADO,
           FAC_NRO_DOC,
           FAC_PROV,
           FAC_FECHA,
           FAC_TIPO_MOV,
           FAC_MON,
           FAC_TOT_GR10_II,
           FAC_TOT_GR05_II,
           FAC_TOT_EXEN,
           FAC_TOT_MON,
           FAC_CLAVE_DOC_FIN,
           FAC_LOGIN,
           FAC_CTA_BCO_P,
           FAC_TIPO_IMAGEN,
           FAC_LOGIN_CONF_IM,
           FAC_VER_CONT)
        VALUES
          (V_CLAVE_IMG,
           I_EMPRESA,
           I_SUCURSAL,
           NULL,
           I_PDOC_NRO_DOC,
           I_FIN_CODIGO_PROV,
           I_PDOC_FEC,
           I_S_TIPO_MOV,
           I_MON_LOC,
           0,
           0,
           round(I_S_IMPORTE,l_redondeo),
           round(I_S_IMPORTE,l_redondeo),
           V_CLAVE,
           GEN_DEVUELVE_USER,
           I_DOC_CTA_BCO,
           'C',
           GEN_DEVUELVE_USER,
           'N');

        INSERT INTO COM_FACTURA_IMAGEN
          (FAC_CLAVE, FAC_ITEM, FAC_LOR, FAC_EMPR)
        VALUES
          (V_CLAVE_IMG, 1, 10101, I_EMPRESA);

        RETURN V_CLAVE;

      ELSE

        INSERT INTO FIN_DOCUMENTO
          (DOC_CLAVE,
           DOC_EMPLEADO,
           DOC_EMPR,
           DOC_CTA_BCO,
           DOC_SUC,
           DOC_TIPO_MOV,
           DOC_NRO_DOC,
           DOC_TIPO_SALDO,
           DOC_MON,
           DOC_PROV,
           DOC_CLI_NOM,
           DOC_FEC_OPER,
           DOC_FEC_DOC,
           DOC_BRUTO_EXEN_LOC,
           DOC_BRUTO_EXEN_MON,
           DOC_BRUTO_GRAV_LOC,
           DOC_BRUTO_GRAV_MON,
           DOC_NETO_EXEN_LOC,
           DOC_NETO_EXEN_MON,
           DOC_NETO_GRAV_LOC,
           DOC_NETO_GRAV_MON,
           DOC_IVA_LOC,
           DOC_IVA_MON,
           DOC_SALDO_INI_MON,
           DOC_SALDO_LOC,
           DOC_SALDO_MON,
           DOC_SALDO_PER_ACT_LOC,
           DOC_SALDO_PER_ACT_MON,
           DOC_OBS,
           DOC_BASE_IMPON_LOC,
           DOC_BASE_IMPON_MON,
           DOC_LOGIN,
           DOC_FEC_GRAB,
           DOC_SIST,
           DOC_OPERADOR)
        VALUES
          (V_CLAVE,
           I_PDOC_EMPLEADO,
           I_EMPRESA,
           I_DOC_CTA_BCO,
           I_SUCURSAL,
           I_S_TIPO_MOV,
           I_PDOC_NRO_DOC,
           'C',
           I_MON_LOC,
           I_FIN_CODIGO_PROV,
           SUBSTR(I_PER_NOMBRE, 1, 40),
           I_PDOC_FEC_OPER,
           I_PDOC_FEC,
           round(I_S_IMPORTE * NVL(S_TASA_OFIC, 1),l_redondeo),
           round(I_S_IMPORTE, l_redondeo),
           0,
           0,
           I_S_IMPORTE * NVL(S_TASA_OFIC, 1),
           I_S_IMPORTE,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           I_DOC_OBS,--NULL, --'ANTIC. Personal-Modulo RRHH',
           0,
           0,
           GEN_DEVUELVE_USER,
           SYSDATE,
           'PER',
           2);

        INSERT INTO FIN_DOC_CONCEPTO
          (DCON_ITEM,
           DCON_CLAVE_DOC,
           DCON_CLAVE_CONCEPTO,
           DCON_CLAVE_CTACO,
           DCON_TIPO_SALDO,
           DCON_EXEN_LOC,
           DCON_EXEN_MON,
           DCON_GRAV_LOC,
           DCON_GRAV_MON,
           DCON_IVA_LOC,
           DCON_IVA_MON,
           DCON_IND_TIPO_IVA_COMPRA,
           DCON_OBS,
           DCON_EMPR)

        VALUES
          (1,
           V_CLAVE,
           I_PCON_CLAVE_CONCEPTO_FIN,
           I_PCON_CLAVE_CTACO,
           I_FCON_TIPO_SALDO,
           I_S_IMPORTE * NVL(S_TASA_OFIC, 1),
           I_S_IMPORTE,
           0,
           0,
           0,
           0,
           1,
           SUBSTR(I_DOC_OBS || '-' || SUBSTR(I_PER_NOMBRE, 1, 20), 1, 50),
           I_EMPRESA);

        INSERT INTO FIN_CUOTA
          (CUO_CLAVE_DOC, CUO_FEC_VTO, CUO_IMP_LOC, CUO_IMP_MON, CUO_EMPR)
        VALUES
          (V_CLAVE,
           LAST_DAY(I_PDOC_FEC), --I_PDOC_FEC,
           I_S_IMPORTE * NVL(S_TASA_OFIC, 1),
           I_S_IMPORTE,
           I_EMPRESA);

        RETURN V_CLAVE;
      END IF;

      -------=========== DESDE ACA PARA TRANSAGRO ===================----
    ELSE

      INSERT INTO FIN_DOCUMENTO
        (DOC_CLAVE,
         DOC_EMPLEADO,
         DOC_EMPR,
         DOC_CTA_BCO,
         DOC_SUC,
         DOC_TIPO_MOV,
         DOC_NRO_DOC,
         DOC_TIPO_SALDO,
         DOC_MON,
         DOC_PROV,
         DOC_CLI_NOM,
         DOC_FEC_OPER,
         DOC_FEC_DOC,
         DOC_BRUTO_EXEN_LOC,
         DOC_BRUTO_EXEN_MON,
         DOC_BRUTO_GRAV_LOC,
         DOC_BRUTO_GRAV_MON,
         DOC_NETO_EXEN_LOC,
         DOC_NETO_EXEN_MON,
         DOC_NETO_GRAV_LOC,
         DOC_NETO_GRAV_MON,
         DOC_IVA_LOC,
         DOC_IVA_MON,
         DOC_SALDO_INI_MON,
         DOC_SALDO_LOC,
         DOC_SALDO_MON,
         DOC_SALDO_PER_ACT_LOC,
         DOC_SALDO_PER_ACT_MON,
         DOC_OBS,
         DOC_BASE_IMPON_LOC,
         DOC_BASE_IMPON_MON,
         DOC_LOGIN,
         DOC_FEC_GRAB,
         DOC_SIST,
         DOC_OPERADOR)
      VALUES
        (V_CLAVE,
         I_PDOC_EMPLEADO,
         I_EMPRESA,
         I_DOC_CTA_BCO,
         1,
         I_S_TIPO_MOV,
         I_PDOC_NRO_DOC,
         'C',
         --  :PARAMETER.P_MON_LOC,
         S_MON,
         I_FIN_CODIGO_PROV,
         SUBSTR(I_PER_NOMBRE, 1, 40),
         I_PDOC_FEC_OPER,
         I_PDOC_FEC,
         I_S_IMPORTE * NVL(S_TASA_OFIC, 1),
         I_S_IMPORTE,
         0,
         0,
         I_S_IMPORTE * NVL(S_TASA_OFIC, 1),
         I_S_IMPORTE,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         I_DOC_OBS,---NULL, --'ANTIC. Personal-Modulo RRHH',
         0,
         0,
         GEN_DEVUELVE_USER,
         SYSDATE,
         'PER',
         2);

      INSERT INTO FIN_DOC_CONCEPTO
        (DCON_ITEM,
         DCON_CLAVE_DOC,
         DCON_CLAVE_CONCEPTO,
         DCON_CLAVE_CTACO,
         DCON_TIPO_SALDO,
         DCON_EXEN_LOC,
         DCON_EXEN_MON,
         DCON_GRAV_LOC,
         DCON_GRAV_MON,
         DCON_IVA_LOC,
         DCON_IVA_MON,
         DCON_IND_TIPO_IVA_COMPRA,
         DCON_OBS,
         DCON_EMPR)

      VALUES
        (1,
         V_CLAVE,
         I_PCON_CLAVE_CONCEPTO_FIN,
         I_PCON_CLAVE_CTACO,
         I_FCON_TIPO_SALDO,
         I_S_IMPORTE * NVL(S_TASA_OFIC, 1),
         I_S_IMPORTE,
         0,
         0,
         0,
         0,
         1,
         I_DOC_OBS || '-' || SUBSTR(I_PER_NOMBRE, 1, 20),
         I_EMPRESA);
 -- RAISE_APPLICATION_ERROR(-20001,I_S_TIPO_MOV||'-'||I_CUOTA);
      IF I_CUOTA = 'S' OR I_S_TIPO_MOV = 31 THEN

        SELECT NVL(SUM(C001),0) IMPORTE
          INTO X_IMPORTE_CUO
          FROM APEX_COLLECTIONS
         WHERE COLLECTION_NAME = 'CUOTA_PERI005'
           AND C003 = I_PDOC_EMPLEADO;

        -- RAISE_APPLICATION_ERROR (-20001, I_S_IMPORTE||'   J  '||X_IMPORTE_CUO);

        IF X_IMPORTE_CUO <> I_S_IMPORTE THEN
          RAISE_APPLICATION_ERROR(-20001,
                                  I_S_IMPORTE || ' El importe de la cuota debe ser igual a lo generado ' || X_IMPORTE_CUO||'- '||I_PDOC_EMPLEADO||'- '||I_S_IMPORTE);
          -- RAISE_APPLICATION_ERROR (-20001, 'Los montos de las cuotas deben ser igual al monto total')       ;
        END IF;

        FOR F IN (SELECT SEQ_ID, TO_NUMBER(C001) IMPORTE, C002 VENCIMIENTO
                    FROM APEX_COLLECTIONS
                   WHERE COLLECTION_NAME = 'CUOTA_PERI005'
                     AND C003 = I_PDOC_EMPLEADO) LOOP

          IF F.IMPORTE = 0 THEN
            RAISE_APPLICATION_ERROR(-20001,
                                    'El monto de la cuota debe ser mayor a cero');
          END IF;

          --
          INSERT INTO FIN_CUOTA
            (CUO_CLAVE_DOC,
             CUO_FEC_VTO,
             CUO_IMP_LOC,
             CUO_IMP_MON,
             CUO_EMPR)
          VALUES
            (V_CLAVE,
             F.VENCIMIENTO,
             F.IMPORTE * NVL(S_TASA_OFIC, 1),
             F.IMPORTE,
             I_EMPRESA);
          --  V_CLAVE     := FIN_SEQ_DOC_NEXTVAL;
        END LOOP;
      ELSE
        INSERT INTO FIN_CUOTA
          (CUO_CLAVE_DOC, CUO_FEC_VTO, CUO_IMP_LOC, CUO_IMP_MON, CUO_EMPR)
        VALUES
          (V_CLAVE,
           I_PDOC_FEC,
           I_S_IMPORTE * NVL(S_TASA_OFIC, 1),
           I_S_IMPORTE,
           I_EMPRESA);
      END IF;

      RETURN V_CLAVE;
    END IF;
    -- END LOOP;
    -- END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20020, SQLERRM); -- PL_EXHIBIR_ERROR_PLSQL;   */

  END;

  PROCEDURE PP_INS_PER_DOC(IPDOC_CLAVE          IN NUMBER,
                           IPDOC_QUINCENA       IN NUMBER,
                           IPDOC_EMPLEADO       IN NUMBER,
                           IPDOC_FEC            IN DATE,
                           IPDOC_NRO_DOC        IN NUMBER,
                           IPDOC_FEC_GRAB       IN DATE,
                           IPDOC_LOGIN          IN VARCHAR2,
                           IPDOC_FORM           IN VARCHAR2,
                           IPDOC_PERIODO        IN NUMBER,
                           IPDOC_CLAVE_FIN      IN NUMBER,
                           IPDOC_NRO_ITEM       IN NUMBER,
                           IPDOC_CONCEPTO       IN NUMBER,
                           IPDOC_IMPORTE        IN NUMBER,
                           IPDOC_IMPORTE_LOC    IN NUMBER,
                           IPDOC_MON            IN NUMBER,
                           IPDOC_DEPARTAMENTO   IN NUMBER,
                           IPDOC_EMPRESA        IN NUMBER,
                           I_S_TIPO_MOV         IN NUMBER,
                           I_S_TIP_MOV_ADEL_PRO IN NUMBER,
                           I_DESC_EMPRESA       IN VARCHAR2,
                           I_OBS                IN VARCHAR2,
                           I_CANT_DIAS_TRA      IN NUMBER)IS  ---LV09/04/2021) IS
  BEGIN

    IF I_S_TIPO_MOV = I_S_TIP_MOV_ADEL_PRO AND IPDOC_EMPRESA = 1 THEN

      -- IF IPDOC_DEPARTAMENTO IS NULL THEN
      -- RAISE_APPLICATION_ERROR(-20002,IPDOC_EMPLEADO);
      --END IF;
      INSERT INTO PER_DOCUMENTO_ADEL_TEMP
        (PDOC_CLAVE,
         PDOC_QUINCENA,
         PDOC_EMPLEADO,
         PDOC_FEC,
         PDOC_NRO_DOC,
         PDOC_FEC_GRAB,
         PDOC_LOGIN,
         PDOC_FORM,
         PDOC_PERIODO,
         PDOC_CLAVE_FIN,
         PDOC_MON,
         PDOC_DEPARTAMENTO,
         PDOC_EMPR,
         PDOC_OBS)
      VALUES
        (IPDOC_CLAVE,
         IPDOC_QUINCENA,
         IPDOC_EMPLEADO,
         IPDOC_FEC,
         IPDOC_NRO_DOC,
         IPDOC_FEC_GRAB,
         IPDOC_LOGIN,
         IPDOC_FORM,
         IPDOC_PERIODO,
         IPDOC_CLAVE_FIN,
         IPDOC_MON,
         IPDOC_DEPARTAMENTO,
         IPDOC_EMPRESA,
         I_OBS);

      INSERT INTO PER_DOCUMENTO_DET_ADEL_TEMP
        (PDDET_CLAVE_DOC,
         PDDET_ITEM,
         PDDET_CLAVE_CONCEPTO,
         PDDET_IMP,
         PDDET_IMP_LOC,
         PDDET_EMPR)
      VALUES
        (IPDOC_CLAVE,
         IPDOC_NRO_ITEM,
         IPDOC_CONCEPTO,
         IPDOC_IMPORTE,
         IPDOC_IMPORTE_LOC,
         IPDOC_EMPRESA);
    ELSE
      
    

      INSERT INTO PER_DOCUMENTO
        (PDOC_CLAVE,
         PDOC_QUINCENA,
         PDOC_EMPLEADO,
         PDOC_FEC,
         PDOC_NRO_DOC,
         PDOC_FEC_GRAB,
         PDOC_LOGIN,
         PDOC_FORM,
         PDOC_PERIODO,
         PDOC_CLAVE_FIN,
         PDOC_MON,
         PDOC_DEPARTAMENTO,
         PDOC_EMPR,
         PDOC_OBS,
         PDOC_CANT_DIAS)
      VALUES
        (IPDOC_CLAVE,
         IPDOC_QUINCENA,
         IPDOC_EMPLEADO,
         IPDOC_FEC,
         IPDOC_NRO_DOC,
         IPDOC_FEC_GRAB,
         IPDOC_LOGIN,
         IPDOC_FORM,
         IPDOC_PERIODO,
         IPDOC_CLAVE_FIN,
         IPDOC_MON,
         IPDOC_DEPARTAMENTO,
         IPDOC_EMPRESA,
         I_OBS,
         I_CANT_DIAS_TRA);

      INSERT INTO PER_DOCUMENTO_DET
        (PDDET_CLAVE_DOC,
         PDDET_ITEM,
         PDDET_CLAVE_CONCEPTO,
         PDDET_IMP,
         PDDET_IMP_LOC,
         PDDET_EMPR)
      VALUES
        (IPDOC_CLAVE,
         IPDOC_NRO_ITEM,
         IPDOC_CONCEPTO,
         IPDOC_IMPORTE,
         IPDOC_IMPORTE_LOC,
         IPDOC_EMPRESA);

    END IF;
  END;

  PROCEDURE PP_FEC_PROX_VTO(I_DOCU_FEC_EMIS IN DATE,
                            IO_FEC_PRIM_VTO OUT DATE) IS
  BEGIN

    IF IO_FEC_PRIM_VTO IS NULL THEN
      IO_FEC_PRIM_VTO := I_DOCU_FEC_EMIS + 30;
    END IF;

  END PP_FEC_PROX_VTO;

  PROCEDURE PP_GENERAR_CUOTA(I_FEC_PRIM_VTO      IN DATE,
                             I_FEC_OPER          IN DATE,
                             I_TOTAL             IN NUMBER,
                             I_OP_CUOTA          IN VARCHAR2,
                             I_CANT_CUOTAS       IN NUMBER,
                             I_TIPO_VENCIMIENTO  IN VARCHAR2,
                             I_DIAS_ENTRE_CUOTAS IN NUMBER,
                             I_MON_DEC_IMP       IN NUMBER,
                             I_PLAZO_PAGO        IN VARCHAR2) IS
    L_VC_ARR2 APEX_APPLICATION_GLOBAL.VC_ARR2;
  BEGIN

    --  RAISE_APPLICATION_ERROR(-20002,I_CANT_CUOTAS);
    IF I_OP_CUOTA = 'CUOTA' THEN
      --VALIDAR CANTIDAD DE CUOTAS SELECCIONADA
      IF NVL(I_CANT_CUOTAS, 0) < 1 THEN
        RAISE_APPLICATION_ERROR(-20002,
                                'Debe ingresar la cantidad de cuotas que desea generar!');
      END IF;
      --VALIDAR FECHA DEL PRIMER VENCIMIENTO
      IF I_FEC_PRIM_VTO IS NULL THEN
        RAISE_APPLICATION_ERROR(-20002,
                                'Debe ingresar la fecha del primer vencimiento!');
        --VALIDAR FECHA DEL PRIMER VENCIMIENTO, PARA QUE EL VENCIMIENTO DE LA CUOTA NO SEA MENOR A LA FECHA ACTUAL. 06/09/2019
        IF I_FEC_PRIM_VTO < TO_DATE(SYSDATE, 'DD/MM/YYYY') THEN
          RAISE_APPLICATION_ERROR(-20002,
                                  'La fecha de vencimiento de la cuota, no puede ser menor a la fecha actual!');
        END IF;
      END IF;
      --VALIDAR TIPO DE VENCIMIENTO
      IF I_CANT_CUOTAS > 1 THEN
        IF NVL(I_TIPO_VENCIMIENTO, ' ') NOT IN ('F', 'V') THEN
          RAISE_APPLICATION_ERROR(-20002,
                                  'Tipo de vencimiento debe ser F=Fijo, V=Variable!');
        END IF;
      END IF;
      --VALIDAR LOS DIAS ENTRE UNA CUOTA Y LA SIGUIENTE
      --SOLO SI S_TIPO_VENCIMIENTO = 'V' (VARIABLE)
      --Y SI LA CANTIDAD DE CUOTAS ES MAYOR QUE 1
      IF I_CANT_CUOTAS > 1 AND I_TIPO_VENCIMIENTO = 'V' THEN
        IF I_DIAS_ENTRE_CUOTAS IS NULL THEN
          RAISE_APPLICATION_ERROR(-20002,
                                  'Debe ingresar los dias entre una cuota y la otra!');
        END IF;
        IF I_DIAS_ENTRE_CUOTAS <= 0 THEN
          RAISE_APPLICATION_ERROR(-20002,
                                  'Los dias entre cuotas debe ser un numero > que cero!');
        END IF;
      END IF;

      DECLARE
        I             NUMBER;
        CONT          NUMBER := 0;
        V_FECHA       DATE := I_FEC_PRIM_VTO;
        V_CUO_IMP     NUMBER; /*:= ROUND(I_TOTAL / I_CANT_CUOTAS,
                                            I_MON_DEC_IMP);*/
        V_TOT_CUO_IMP NUMBER; -- := 0;
      BEGIN
        --    RAISE_APPLICATION_ERROR(-20002,I_MON_DEC_IMP);

        FOR X IN (SELECT SEQ_ID, N001 PDOC_EMPLEADO, N003 S_IMPORTE
                    FROM APEX_COLLECTIONS A
                   WHERE COLLECTION_NAME = 'PERI005') LOOP

          V_TOT_CUO_IMP := 0;
          V_CUO_IMP     := ROUND(X.S_IMPORTE / I_CANT_CUOTAS, I_MON_DEC_IMP);

           CONT := 0;
           V_FECHA  := I_FEC_PRIM_VTO;
          FOR I IN 1 .. I_CANT_CUOTAS LOOP

            V_TOT_CUO_IMP := V_TOT_CUO_IMP + V_CUO_IMP;
            IF I >= I_CANT_CUOTAS THEN
              --AJUSTAR LA DIFERENCIA POR REDONDEO A LA ULTIMA CUOTA;
              V_CUO_IMP := V_CUO_IMP + (X.S_IMPORTE - V_TOT_CUO_IMP);
            END IF;

            PP_ADICIONAR_CUOTA(I_CUO_FEC_VTO => V_FECHA,
                               I_CUO_IMP_MON => V_CUO_IMP,
                               I_CUO_EMPL    => X.PDOC_EMPLEADO);

            IF I_TIPO_VENCIMIENTO = 'V' THEN
              V_FECHA := V_FECHA + I_DIAS_ENTRE_CUOTAS;

            ELSE
              CONT := CONT + 1;
              --  RAISE_APPLICATION_ERROR(-20001,'holi');
              V_FECHA := ADD_MONTHS(I_FEC_PRIM_VTO, CONT);
            END IF;
          END LOOP;

        END LOOP;
      END;
    ELSE
      --DESGLOSA EL PLAZO
      L_VC_ARR2 := APEX_UTIL.STRING_TO_TABLE(P_STRING    => I_PLAZO_PAGO,
                                             P_SEPARATOR => '/');
      --GENERA LOS VENCIMIENTOS A PARTIR DE LA FECHA DE OPERACION
      --AL IR AL ULTIMO REGISTRO SE VA UNA MAS ABAJO
      --DE LO YA CARGADO, POR ESO LO BORRAMOS
      DECLARE
        V_FECHA       DATE := I_FEC_OPER;
        V_CUO_IMP     NUMBER; /* := ROUND(I_TOTAL / L_VC_ARR2.COUNT,
                                            I_MON_DEC_IMP);*/
        V_TOT_CUO_IMP NUMBER := 0;

      BEGIN

        FOR X IN (SELECT SEQ_ID, N001 PDOC_EMPLEADO, N003 S_IMPORTE
                    FROM APEX_COLLECTIONS A
                   WHERE COLLECTION_NAME = 'PERI005') LOOP
          V_TOT_CUO_IMP := 0;
          V_CUO_IMP     := ROUND(X.S_IMPORTE / L_VC_ARR2.COUNT,
                                 I_MON_DEC_IMP);

          FOR Z IN 1 .. L_VC_ARR2.COUNT LOOP
            V_TOT_CUO_IMP := V_TOT_CUO_IMP + V_CUO_IMP;
            IF Z >= L_VC_ARR2.COUNT THEN
              V_CUO_IMP := V_CUO_IMP + (X.S_IMPORTE - V_TOT_CUO_IMP);
            END IF;
            PP_ADICIONAR_CUOTA(I_CUO_FEC_VTO => V_FECHA + L_VC_ARR2(Z),
                               I_CUO_IMP_MON => V_CUO_IMP,
                               I_CUO_EMPL    => X.PDOC_EMPLEADO);
          END LOOP;

        END LOOP;
      END;
    END IF;
  END PP_GENERAR_CUOTA;

  PROCEDURE PP_ADICIONAR_CUOTA(I_CUO_FEC_VTO IN DATE,
                               I_CUO_IMP_MON IN NUMBER,
                               I_CUO_EMPL    IN NUMBER) IS
  BEGIN
    IF NOT
        APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => 'CUOTA_PERI005') THEN
      APEX_COLLECTION.CREATE_COLLECTION(P_COLLECTION_NAME => 'CUOTA_PERI005');
    END IF;

    ---RAISE_APPLICATION_ERROR(-20002,I_CUO_IMP_MON);
    APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'CUOTA_PERI005',
                               P_C001            => I_CUO_IMP_MON,
                               P_C002            => I_CUO_FEC_VTO,
                               P_C003            => I_CUO_EMPL);

  END PP_ADICIONAR_CUOTA;

  PROCEDURE PP_BLOQ_PROV_CLI(P_EMPRESA  IN NUMBER,
                             P_LEGAJO   IN NUMBER,
                             P_CONCEPTO IN NUMBER) IS

    V_CANT_REG_PROV NUMBER;
    V_CANT_REG_CLI  NUMBER;

    V_PROVEEDOR NUMBER;
    V_CLIENTE   NUMBER;
  BEGIN

    --------PEDIDO DEL SR UGO, NO SE PUEDE REALIZAR NINGUNA TRANSACCION SI EL CLIENTE CUENTA CON
    --------ADELANTOS VENCIDOS.. EXECPTO EL PAGO DE LOS MISMO--06/08/2020 LV
    IF P_EMPRESA = 1 THEN
      SELECT E.EMPL_CODIGO_PROV, E.EMPL_COD_CLIENTE
        INTO V_PROVEEDOR, V_CLIENTE
        FROM PER_EMPLEADO E
       WHERE EMPL_EMPRESA = P_EMPRESA
         AND EMPL_LEGAJO = P_LEGAJO;

      IF P_CONCEPTO NOT IN (16, 17, 18, 21, 24, 28, 36, 37) THEN
        SELECT COUNT(*)
          INTO V_CANT_REG_PROV
          FROM FIN_DOCUMENTO   A,
               FIN_UNION_CUOTA B,
               FIN_UNION_PAGO  C,
               FIN_PROVEEDOR   D
         WHERE A.DOC_TIPO_MOV IN (31)
           AND A.DOC_EMPR = 1
           AND A.DOC_CLAVE = B.CUO_CLAVE_DOC
           AND A.DOC_EMPR = B.CUO_EMPR
           AND A.DOC_PROV = D.PROV_CODIGO(+)
           AND A.DOC_EMPR = D.PROV_EMPR(+)
           AND (B.CUO_CLAVE_DOC = C.PAG_CLAVE_DOC(+) AND
               B.CUO_FEC_VTO = C.PAG_FEC_VTO(+) AND
               B.CUO_EMPR = C.PAG_EMPR(+))
           AND B.CUO_FEC_VTO < SYSDATE
           AND TRUNC(CUO_SALDO_MON) > 0
           AND TRUNC(SYSDATE) - CUO_FEC_VTO > 0
           AND DOC_PROV = V_PROVEEDOR;

        IF V_CANT_REG_PROV > 0 THEN

          RAISE_APPLICATION_ERROR(-20001,
                                  'El empleado ' || P_LEGAJO ||
                                  ' cuenta con ' || V_CANT_REG_PROV ||
                                  ' adelanto/s proveedor vencido/s');

        END IF;

        SELECT COUNT(1)
          INTO V_CANT_REG_CLI
          FROM FIN_DOCUMENTO   A,
               FIN_UNION_CUOTA B,
               FIN_UNION_PAGO  C,
               FIN_CLIENTE     D
         WHERE A.DOC_TIPO_MOV IN (18)
           AND A.DOC_EMPR = 1
           AND A.DOC_CLAVE = B.CUO_CLAVE_DOC
           AND A.DOC_EMPR = B.CUO_EMPR
           AND A.DOC_CLI = D.CLI_CODIGO(+)
           AND A.DOC_EMPR = D.CLI_EMPR(+)
           AND (B.CUO_CLAVE_DOC = C.PAG_CLAVE_DOC(+) AND
               B.CUO_FEC_VTO = C.PAG_FEC_VTO(+) AND
               B.CUO_EMPR = C.PAG_EMPR(+))
           AND B.CUO_FEC_VTO < SYSDATE
           AND TRUNC(CUO_SALDO_MON) > 0
           AND TRUNC(SYSDATE) - CUO_FEC_VTO > 0
           AND CLI_CODIGO = V_CLIENTE;

        IF V_CANT_REG_CLI > 0 THEN

          RAISE_APPLICATION_ERROR(-20001,
                                  'El empleado ' || P_LEGAJO ||
                                  ' cuenta con ' || V_CANT_REG_CLI ||
                                  ' adelanto/s cliente vencido/s');
        END IF;
      END IF;
    END IF;

  END PP_BLOQ_PROV_CLI;

  PROCEDURE PP_BLOQUEAR_SISTEMAS (P_EMPRESA IN NUMBER,
                                  P_FEC_VTO IN DATE DEFAULT NULL,
                                  P_PROGRAMA IN VARCHAR2 DEFAULT NULL) IS
     P_FECHA date;
     X_HABILITADO NUMBER;
  BEGIN
   --RAISE_APPLICATION_ERROR(-20001, P_FEC_VTO);
 IF P_EMPRESA = 1 and gen_devuelve_user not in  ('JBENITEZ', 'DAGUAYO')then
      SELECT B.PERI_FEC_CORTE_INI
        INTO P_FECHA
        FROM PER_CONFIGURACION A, PER_PERIODO B
       WHERE TO_CHAR(PERI_FEC_INI,'MM/YYYY')= TO_CHAR(SYSDATE, 'MM/YYYY')
         AND A.CONF_EMPR=B.PERI_EMPR
         AND A.CONF_EMPR=P_EMPRESA;           


        SELECT COUNT(A.PROG_NOMBRE_FORMULARIO)
          INTO X_HABILITADO
          FROM PER_HAB_PROG T, GEN_PROGRAMA A
         WHERE T.HAB_SISTEMA = A.PROG_CLAVE
           AND A.PROG_BLOQ_RRHH = 'S'
           AND T.HAB_EMPR = 1
           AND  case when PROG_NOMBRE_FORMULARIO = 'FINC001' then
                     'FINI001'
                     else
                       PROG_NOMBRE_FORMULARIO
                   end = P_PROGRAMA
           AND T.HAB_USUARIO = gen_devuelve_user
           AND NVL(P_FEC_VTO, trunc(SYSDATE)) BETWEEN T.HAB_FEC_DESDE AND T.HAB_FEC_HASTA;






           IF   TO_CHAR (SYSDATE,'DD/MM/YYYY') >= TO_CHAR(P_FECHA,'DD/MM/YYYY') AND TO_CHAR(P_FECHA,'MM/YYYY') = TO_CHAR (SYSDATE, 'MM/YYYY') THEN

             IF X_HABILITADO >0 THEN
               NULL;
             ELSIF X_HABILITADO = 0 AND P_PROGRAMA IN ('PERI097','PERI096') AND  TO_CHAR (SYSDATE,'DD/MM/YYYY') = TO_CHAR(P_FECHA,'DD/MM/YYYY') THEN
                NULL;
             ELSE

             IF P_FEC_VTO IS NULL then
                RAISE_APPLICATION_ERROR(-20001,
              'Ya no puede realizar movimientos que afecte salario para el periodo actual- Favor Comunicarse con Capital Humano');
             ELSIF TO_CHAR(P_FEC_VTO,'MM/YYYY') = TO_CHAR(P_FECHA,'MM/YYYY') THEN
               RAISE_APPLICATION_ERROR(-20001,
              'Ya no puede realizar movimientos que afecte salario para el periodo actual- Favor Comunicarse con Capital Humano');
             ELSE
                NULL;
             END IF;
             END IF;
           END IF;

    END IF;

  END PP_BLOQUEAR_SISTEMAS;

  PROCEDURE PP_LLAMAR_REPORTE  (P_EMPRESA NUMBER,
                                P_PERIODO NUMBER,
                                P_NRO_DOC NUMBER) IS



     V_AMPER               VARCHAR2(2) := '&';
     V_PARAMETROS          VARCHAR2(30000);


 BEGIN




   V_PARAMETROS := 'P_FORMATO=' || APEX_UTIL.URL_ENCODE('pdf');
    V_PARAMETROS := V_PARAMETROS || V_AMPER || 'P_EMPRESA=' ||
                    URL_ENCODE(P_EMPRESA); ---
    V_PARAMETROS := V_PARAMETROS || V_AMPER || 'P_NRO_DOC=' ||
                    URL_ENCODE(P_NRO_DOC); ---

    V_PARAMETROS := V_PARAMETROS || V_AMPER || 'P_PERIODO=' ||
                    URL_ENCODE(P_PERIODO); ---
    V_PARAMETROS := V_PARAMETROS || V_AMPER || 'P_PROGRAMA=' ||
                    URL_ENCODE(1); ---
    DELETE FROM GEN_PARAMETROS_REPORT WHERE USUARIO = GEN_DEVUELVE_USER;
      INSERT INTO GEN_PARAMETROS_REPORT
          (PARAMETROS, USUARIO, NOMBRE_REPORTE, FORMATO_SALIDA)
        VALUES
            (V_PARAMETROS,  GEN_DEVUELVE_USER,'PERI005' , 'pdf');
        COMMIT;





  END PP_LLAMAR_REPORTE ;


FUNCTION PP_DIAS_TRABA_MONTA (P_LEGAJO  NUMBER,
                              P_EMPRESA NUMBER,
                              P_FEC_INGRESO DATE,
                              P_FEC_SALIDA DATE,
                              P_FORMA_PAGO NUMBER,
                              P_DESDE      DATE,
                              P_HASTA      DATE)RETURN NUMBER IS
  X_DIA_LABORAL VARCHAR2(1);
  X_CANT_DIAS NUMBER :=0;
BEGIN
  
 FOR V IN (
         SELECT I_DIA FECHA
           FROM TABLE(RETURN_TABLE_DIA_PROF(TO_NUMBER(TO_CHAR(SYSDATE,'YYYY'))))
          WHERE I_DIA BETWEEN P_DESDE AND P_HASTA) LOOP

         
               X_DIA_LABORAL:=     PER_VERIFICAR_DIA_LABORAL(V_EMPL            => P_LEGAJO,
                                                             V_EMPRESA         => P_EMPRESA,
                                                             V_FECHA           => V.FECHA,
                                                             V_FEC_INGRESO_FUN => P_FEC_INGRESO,
                                                             V_FEC_SALIDA_FUN  => P_FEC_SALIDA,
                                                             V_TIP_COB         => P_FORMA_PAGO,
                                                             V_PAGADOS         => NULL);          
                                                             
        IF    X_DIA_LABORAL IN ('A','P','R','U','X','C') THEN
          
        X_CANT_DIAS := X_CANT_DIAS+1;
        
        END IF ;
        
        
                                                                             
  END LOOP;
RETURN X_CANT_DIAS;

END PP_DIAS_TRABA_MONTA;



END PERI005;
/
