CREATE OR REPLACE PACKAGE FACP009 IS

  -- AUTHOR  : DIEGO
  -- CREATED : 03/02/2018 7:06:37
  -- PURPOSE :

  PROCEDURE PP_VERIFICAR_PAGO(I_DOC_CLAVE_FIN IN NUMBER, I_EMPR IN NUMBER);
  function PP_VERIFICAR_PAGO(I_DOC_CLAVE_FIN IN NUMBER, I_EMPR IN NUMBER)
    return number;
  PROCEDURE PP_REVERTIR_DOC(I_DOC_CLAVE_FIN     IN NUMBER,
                            I_EMPR              IN NUMBER,
                            I_DOC_FEC           IN DATE,
                            I_MOTIVO            IN NUMBER,
                            I_OBS               IN VARCHAR2,
                            I_SUCURSAL          IN NUMBER,
                            I_DEPOSITO          IN NUMBER,
                            I_DOCU_CLAVE_STOPES IN NUMBER,
                            I_USER              IN VARCHAR2,
                            I_PROGRAMA          IN VARCHAR2 DEFAULT NULL,
                            I_AFECT_COMISION    IN VARCHAR2 DEFAULT NULL);

END FACP009;
/
CREATE OR REPLACE PACKAGE BODY FACP009 IS

  -- AUTHOR  : DIEGO
  -- CREATED : 03/02/2018 7:06:37
  -- PURPOSE :

  FUNCTION PP_VERIFICAR_PAGO(I_DOC_CLAVE_FIN IN NUMBER, I_EMPR IN NUMBER)
    RETURN NUMBER IS
    V_PAGO NUMBER;
  BEGIN
    SELECT COUNT(1)
      INTO V_PAGO
      FROM FIN_DOCUMENTO F, FIN_CUOTA C, FIN_PAGO FP
     WHERE F.DOC_CLAVE = C.CUO_CLAVE_DOC
       AND F.DOC_EMPR = C.CUO_EMPR
       AND C.CUO_CLAVE_DOC = FP.PAG_CLAVE_DOC
       AND C.CUO_FEC_VTO = FP.PAG_FEC_VTO
       AND FP.PAG_EMPR = C.CUO_EMPR
       AND F.DOC_EMPR = I_EMPR
       AND F.DOC_CLAVE = I_DOC_CLAVE_FIN;
  
    RETURN V_PAGO;
  
  END;

  PROCEDURE PP_VERIFICAR_PAGO(I_DOC_CLAVE_FIN IN NUMBER, I_EMPR IN NUMBER) IS
    V_PAGO       NUMBER;
    V_ORDEN_PAGO NUMBER;
  BEGIN
    SELECT COUNT(1)
      INTO V_PAGO
      FROM FIN_DOCUMENTO F, FIN_CUOTA C, FIN_PAGO FP
     WHERE F.DOC_CLAVE = C.CUO_CLAVE_DOC
       AND F.DOC_EMPR = C.CUO_EMPR
       AND C.CUO_CLAVE_DOC = FP.PAG_CLAVE_DOC
       AND C.CUO_FEC_VTO = FP.PAG_FEC_VTO
       AND FP.PAG_EMPR = C.CUO_EMPR
       AND F.DOC_EMPR = I_EMPR
       AND F.DOC_CLAVE = I_DOC_CLAVE_FIN;
  
    IF NVL(V_PAGO, 0) > 0 THEN
      RAISE_APPLICATION_ERROR(-20010, 'Primero debe anular el pago.');
    END IF;
    BEGIN
      SELECT (O.FOP_NRO)
        INTO V_ORDEN_PAGO
        FROM FIN_ORDEN_PAGO_DET O
       WHERE O.FOP_CLAVE_DOC = I_DOC_CLAVE_FIN
         AND ROWNUM = 1
         AND O.FOP_EMPR = I_EMPR;
      IF V_ORDEN_PAGO IS NOT NULL THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'Primero debe sacar del orden de pago : ' ||
                                V_ORDEN_PAGO);
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
  END;

  PROCEDURE PP_REVERTIR_DOC(I_DOC_CLAVE_FIN     IN NUMBER,
                            I_EMPR              IN NUMBER,
                            I_DOC_FEC           IN DATE,
                            I_MOTIVO            IN NUMBER,
                            I_OBS               IN VARCHAR2,
                            I_SUCURSAL          IN NUMBER,
                            I_DEPOSITO          IN NUMBER,
                            I_DOCU_CLAVE_STOPES IN NUMBER,
                            I_USER              IN VARCHAR2,
                            I_PROGRAMA          IN VARCHAR2 DEFAULT NULL,
                            I_AFECT_COMISION    IN VARCHAR2 DEFAULT NULL) IS
  
    V_CLAVE_FIN             NUMBER := FIN_SEQ_DOC_NEXTVAL;
    V_CLAVE_STK             NUMBER := STK_SEQ_DOCU_NEXTVAL; --CLAVE STOCK DE LA REVERSION
    V_CLAVE_STK_FACT        NUMBER; -- CLAVE STOCK DE LA FACTURA
    V_DOC_TIPO_MOV          NUMBER;
    V_DOC_NRO_DOC           NUMBER;
    V_DOC_SUC               NUMBER;
    V_DOC_CLI_PROV          NUMBER;
    V_DOC_FEC_DOC           DATE;
    V_DOC_CLAVE_PEDIDO_CAST NUMBER;
    V_DOC_NRO_TIMBRADO      NUMBER;
    V_DEP_ORIG              NUMBER;
    V_DEP_DEST              NUMBER;
    V_SUC_ORIG              NUMBER;
  
  BEGIN
    -- raise_application_error(-20001,'An error was encountered - '||I_DOC_CLAVE_FIN);
    SELECT DOC_TIPO_MOV,
           DOC_NRO_DOC,
           DOC_SUC,
           DOC_CLI,
           DOC_FEC_DOC,
           DOC_CLAVE_PEDIDO_CAST,
           DOC_TIMBRADO,
           DOC_CLAVE_STK
      INTO V_DOC_TIPO_MOV,
           V_DOC_NRO_DOC,
           V_DOC_SUC,
           V_DOC_CLI_PROV,
           V_DOC_FEC_DOC,
           V_DOC_CLAVE_PEDIDO_CAST,
           V_DOC_NRO_TIMBRADO,
           V_CLAVE_STK_FACT
      FROM FIN_DOCUMENTO
     WHERE DOC_CLAVE = I_DOC_CLAVE_FIN
       AND DOC_EMPR = I_EMPR;
  
    IF V_DOC_CLAVE_PEDIDO_CAST IS NOT NULL AND I_PROGRAMA = 'FACP009' THEN
      UPDATE CAST_PEDIDO A
         SET A.CAST_ESTADO = 'P'
       WHERE A.CAST_EMPR = I_EMPR
         AND A.CAST_DOC_CLAVE = V_DOC_CLAVE_PEDIDO_CAST;
    END IF;
    -- 29/07/2022 14:34:34 @PabloACespedes \(^-^)/
    -- a la fecha no se_utiliza este apartado de anular documento, directamente pasa a un apartado de abajo
    if I_EMPR = 1 and 1 = 2 then
    
      if I_PROGRAMA = 'FACP009' then
      
        BEGIN
        
          INSERT INTO FIN_DOCUMENTO --TABLA EN LA CUAL SE INSERTA
          --------DECLARACION DE CAMPOS DE LA TABLA QUE SE VAN A INSERTAR--------------------
            (DOC_CLAVE,
             DOC_EMPR,
             DOC_CTA_BCO_FCON,
             DOC_COND_VTA,
             DOC_SUC,
             DOC_DPTO,
             DOC_TIPO_MOV,
             DOC_NRO_DOC,
             DOC_TIPO_SALDO,
             DOC_MON,
             DOC_PROV,
             DOC_CLI,
             DOC_CLI_NOM,
             DOC_CLI_DIR,
             DOC_CLI_RUC,
             DOC_CLI_TEL,
             DOC_LEGAJO,
             DOC_BCO_CHEQUE,
             DOC_NRO_CHEQUE,
             DOC_FEC_CHEQUE,
             DOC_FEC_DEP_CHEQUE,
             DOC_EST_CHEQUE,
             DOC_NRO_DOC_DEP,
             DOC_CHEQUE_CERTIF,
             DOC_FEC_OPER,
             DOC_FEC_DOC,
             DOC_BRUTO_EXEN_LOC,
             DOC_BRUTO_EXEN_MON,
             DOC_BRUTO_GRAV_LOC,
             DOC_BRUTO_GRAV_MON,
             DOC_NETO_EXEN_LOC,
             DOC_NETO_EXEN_MON,
             DOC_NETO_GRAV_LOC,
             DOC_NETO_GRAV_MON,
             DOC_IVA_LOC,
             DOC_IVA_MON,
             DOC_SALDO_INI_MON,
             DOC_SALDO_LOC,
             DOC_SALDO_MON,
             DOC_SALDO_PER_ACT_LOC,
             DOC_SALDO_PER_ACT_MON,
             DOC_BASE_IMPON_LOC,
             DOC_BASE_IMPON_MON,
             DOC_DIF_CAMBIO,
             DOC_IND_STK,
             DOC_CLAVE_STK,
             DOC_IND_CUOTA,
             DOC_IND_PEDIDO,
             DOC_IND_FINANRESA,
             DOC_IND_CONSIGNACION,
             DOC_PLAN_FINAN,
             DOC_NRO_LISTADO_LIBRO_IVA,
             DOC_CLAVE_PADRE,
             DOC_LOGIN,
             DOC_SIST,
             DOC_CLI_CODEUDOR,
             DOC_OPERADOR,
             DOC_COBRADOR,
             DOC_CANT_PAGARE,
             DOC_SERIE,
             DOC_CLAVE_SCLI,
             DOC_ORDEN_COMPRA_CLAVE,
             DOC_PRES_CLAVE,
             DOC_TASA,
             DOC_NRO_ORDEN_COMPRA,
             DOC_DIF_CAMBIO_ACUM,
             DOC_DIVISION,
             DOC_FORMA_ENTREGA,
             DOC_OBS,
             DOC_CODIGO_TEFE,
             DOC_CAMION,
             DOC_IND_ORD_CARGA,
             DOC_CLAVE_PEDIDO_CAST,
             DOC_IND_IMPR_VENDEDOR,
             DOC_IND_IMPR_CONYUGUE,
             DOC_CANAL,
             DOC_TIMBRADO,
             DOC_CTACO)
          ---------DECLARACION DE LOS VALORES QUE TOMA EL PROCEDIMIENTO PARA INSERTAR-------------
            SELECT V_CLAVE_FIN,
                   DOC_EMPR,
                   DOC_CTA_BCO_FCON,
                   DOC_COND_VTA,
                   I_SUCURSAL,
                   DOC_DPTO,
                   47,
                   DOC_NRO_DOC,
                   'C',
                   DOC_MON,
                   DOC_PROV,
                   DOC_CLI,
                   DOC_CLI_NOM,
                   DOC_CLI_DIR,
                   DOC_CLI_RUC,
                   DOC_CLI_TEL,
                   DOC_LEGAJO,
                   DOC_BCO_CHEQUE,
                   DOC_NRO_CHEQUE,
                   DOC_FEC_CHEQUE,
                   DOC_FEC_DEP_CHEQUE,
                   DOC_EST_CHEQUE,
                   DOC_NRO_DOC_DEP,
                   DOC_CHEQUE_CERTIF,
                   I_DOC_FEC,
                   V_DOC_FEC_DOC,
                   DOC_BRUTO_EXEN_LOC,
                   DOC_BRUTO_EXEN_MON,
                   DOC_BRUTO_GRAV_LOC,
                   DOC_BRUTO_GRAV_MON,
                   DOC_NETO_EXEN_LOC,
                   DOC_NETO_EXEN_MON,
                   DOC_NETO_GRAV_LOC,
                   DOC_NETO_GRAV_MON,
                   DOC_IVA_LOC,
                   DOC_IVA_MON,
                   DOC_SALDO_INI_MON,
                   DOC_SALDO_LOC,
                   DOC_SALDO_MON,
                   DOC_SALDO_PER_ACT_LOC,
                   DOC_SALDO_PER_ACT_MON,
                   DOC_BASE_IMPON_LOC,
                   DOC_BASE_IMPON_MON,
                   DOC_DIF_CAMBIO,
                   DOC_IND_STK,
                   V_CLAVE_STK,
                   DOC_IND_CUOTA,
                   DOC_IND_PEDIDO,
                   DOC_IND_FINANRESA,
                   DOC_IND_CONSIGNACION,
                   DOC_PLAN_FINAN,
                   DOC_NRO_LISTADO_LIBRO_IVA,
                   I_DOC_CLAVE_FIN,
                   NVL(I_USER, USER),
                   DOC_SIST,
                   DOC_CLI_CODEUDOR,
                   DOC_OPERADOR,
                   DOC_COBRADOR,
                   DOC_CANT_PAGARE,
                   DOC_SERIE,
                   DOC_CLAVE_SCLI,
                   DOC_ORDEN_COMPRA_CLAVE,
                   DOC_PRES_CLAVE,
                   DOC_TASA,
                   DOC_NRO_ORDEN_COMPRA,
                   DOC_DIF_CAMBIO_ACUM,
                   DOC_DIVISION,
                   DOC_FORMA_ENTREGA,
                   I_OBS,
                   DOC_CODIGO_TEFE,
                   DOC_CAMION,
                   DOC_IND_ORD_CARGA,
                   DOC_CLAVE_PEDIDO_CAST,
                   DOC_IND_IMPR_VENDEDOR,
                   DOC_IND_IMPR_CONYUGUE,
                   DOC_CANAL,
                   DOC_TIMBRADO,
                   DOC_CTACO
              FROM FIN_DOCUMENTO
             WHERE DOC_CLAVE = I_DOC_CLAVE_FIN
               AND DOC_EMPR = I_EMPR;
          -- raise_application_error(-20001,'An error was encountered - '||I_DOC_CLAVE_FIN);
          --POST;
        EXCEPTION
          WHEN OTHERS THEN
            --DD ROLLBACK;
            RAISE_APPLICATION_ERROR(-20000,
                                    SQLERRM ||
                                    ' ERROR AL INSERTAR FIN_DOCUMENTO N? ' ||
                                    I_DOC_CLAVE_FIN);
        END;
      
        BEGIN
          INSERT INTO FIN_DOC_CONCEPTO
            (DCON_CLAVE_DOC,
             DCON_ITEM,
             DCON_CLAVE_CONCEPTO,
             DCON_CLAVE_CTACO,
             DCON_TIPO_SALDO,
             DCON_EXEN_LOC,
             DCON_EXEN_MON,
             DCON_GRAV_LOC,
             DCON_GRAV_MON,
             DCON_IVA_LOC,
             DCON_IVA_MON,
             DCON_CLAVE_IMP,
             DCON_IND_TIPO_IVA_COMPRA,
             DCON_OBS,
             DCON_PORC_IVA,
             DCON_IND_IMAGRO,
             DCON_CANAL,
             DCON_DIV_CANAL,
             DCON_CANT_COMB,
             DCON_KM_ACTUAL,
             DCON_OCOMD_CLAVE,
             DCON_OCOMD_NRO_ITEM,
             DCON_EMPR,
             DCON_OCOMD_PORC,
             DCON_IMP_AFECTA_COSTO)
            SELECT V_CLAVE_FIN,
                   D.DCON_ITEM,
                   D.DCON_CLAVE_CONCEPTO,
                   D.DCON_CLAVE_CTACO,
                   'C',
                   D.DCON_EXEN_LOC,
                   D.DCON_EXEN_MON,
                   D.DCON_GRAV_LOC,
                   D.DCON_GRAV_MON,
                   D.DCON_IVA_LOC,
                   D.DCON_IVA_MON,
                   D.DCON_CLAVE_IMP,
                   D.DCON_IND_TIPO_IVA_COMPRA,
                   D.DCON_OBS,
                   D.DCON_PORC_IVA,
                   D.DCON_IND_IMAGRO,
                   D.DCON_CANAL,
                   D.DCON_DIV_CANAL,
                   D.DCON_CANT_COMB,
                   D.DCON_KM_ACTUAL,
                   D.DCON_OCOMD_CLAVE,
                   D.DCON_OCOMD_NRO_ITEM,
                   D.DCON_EMPR,
                   D.DCON_OCOMD_PORC,
                   D.DCON_IMP_AFECTA_COSTO
              FROM FIN_DOC_CONCEPTO D
             WHERE DCON_EMPR = I_EMPR
               AND DCON_CLAVE_DOC = I_DOC_CLAVE_FIN
            --AND DCON_IVA_LOC > 0
            ;
        EXCEPTION
          WHEN OTHERS THEN
            --DD ROLLBACK;
            RAISE_APPLICATION_ERROR(-20000,
                                    SQLERRM ||
                                    ' ERROR AL INSERTAR FIN_DOC_CONCEPTO N? ' ||
                                    I_DOC_CLAVE_FIN);
        END;
      
        --------------------------------------------------------------------------------------
        --*--
        --RAISE_APPLICATION_ERROR(-20000,('V_CLAVE_STK= '||V_CLAVE_STK);
        BEGIN
          INSERT INTO STK_DOCUMENTO
            (DOCU_CLAVE,
             DOCU_EMPR,
             DOCU_CODIGO_OPER,
             DOCU_NRO_DOC,
             DOCU_SUC_ORIG,
             DOCU_DEP_ORIG,
             DOCU_SUC_DEST,
             DOCU_DEP_DEST,
             DOCU_MON,
             DOCU_PROV,
             DOCU_CLI,
             DOCU_CLI_NOM,
             DOCU_CLI_RUC,
             DOCU_LEGAJO,
             DOCU_DPTO,
             DOCU_SECCION,
             DOCU_FEC_EMIS,
             DOCU_FEC_OPER,
             DOCU_TIPO_MOV,
             DOCU_GRAV_NETO_LOC,
             DOCU_GRAV_NETO_MON,
             DOCU_EXEN_NETO_LOC,
             DOCU_EXEN_NETO_MON,
             DOCU_GRAV_BRUTO_LOC,
             DOCU_GRAV_BRUTO_MON,
             DOCU_EXEN_BRUTO_LOC,
             DOCU_EXEN_BRUTO_MON,
             DOCU_IVA_LOC,
             DOCU_IVA_MON,
             DOCU_TASA_US,
             DOCU_IND_CUOTA,
             DOCU_IND_CONSIGNACION,
             DOCU_OBS,
             DOCU_CLAVE_PADRE,
             DOCU_NRO_LISTADO_AUD,
             DOCU_LOGIN,
             DOCU_SIST,
             DOCU_OPERADOR,
             DOCU_NRO_PED,
             DOCU_OCARGA_LONDON,
             DOCU_MOT_NCR,
             DOCU_CLAVE_STOPES,
             DOCU_AFECT_COMISION) -------------------------------------lv I_EMPR/12/2020 para saber si afecta o no para el calculo de comision de descargadores
            SELECT V_CLAVE_STK,
                   DOCU_EMPR,
                   I_EMPR,
                   DOCU_NRO_DOC,
                   I_SUCURSAL,
                   I_DEPOSITO,
                   DOCU_SUC_DEST,
                   DOCU_DEP_DEST,
                   DOCU_MON,
                   DOCU_PROV,
                   DOCU_CLI,
                   DOCU_CLI_NOM,
                   DOCU_CLI_RUC,
                   DOCU_LEGAJO,
                   DOCU_DPTO,
                   DOCU_SECCION,
                   V_DOC_FEC_DOC,
                   I_DOC_FEC,
                   47,
                   DOCU_GRAV_NETO_LOC,
                   DOCU_GRAV_NETO_MON,
                   DOCU_EXEN_NETO_LOC,
                   DOCU_EXEN_NETO_MON,
                   DOCU_GRAV_BRUTO_LOC,
                   DOCU_GRAV_BRUTO_MON,
                   DOCU_EXEN_BRUTO_LOC,
                   DOCU_EXEN_BRUTO_MON,
                   DOCU_IVA_LOC,
                   DOCU_IVA_MON,
                   DOCU_TASA_US,
                   DOCU_IND_CUOTA,
                   DOCU_IND_CONSIGNACION,
                   I_OBS,
                   V_CLAVE_STK_FACT,
                   DOCU_NRO_LISTADO_AUD,
                   NVL(I_USER, USER),
                   DOCU_SIST,
                   DOCU_OPERADOR,
                   DOCU_NRO_PED,
                   DOCU_OCARGA_LONDON,
                   I_MOTIVO,
                   I_DOCU_CLAVE_STOPES,
                   I_AFECT_COMISION
              FROM STK_DOCUMENTO
             WHERE DOCU_CLAVE = V_CLAVE_STK_FACT
               AND DOCU_EMPR = I_EMPR;
        
          --POST;
        EXCEPTION
          WHEN OTHERS THEN
            --DD ROLLBACK;
            RAISE_APPLICATION_ERROR(-20001,
                                    SQLERRM ||
                                    ' ERROR AL INSERTAR STK_DOCUMENTO N? ' ||
                                    V_CLAVE_STK_FACT);
        END;
      
        INSERT INTO FIN_PAGO
          (PAG_CLAVE_DOC,
           PAG_FEC_VTO,
           PAG_CLAVE_PAGO,
           PAG_FEC_PAGO,
           PAG_IMP_LOC,
           PAG_IMP_MON,
           PAG_LOGIN,
           PAG_FEC_GRAB,
           PAG_SIST,
           PAG_EMPR)
          SELECT CUO_CLAVE_DOC,
                 CUO_FEC_VTO,
                 V_CLAVE_FIN,
                 I_DOC_FEC,
                 CUO_IMP_LOC,
                 CUO_IMP_MON,
                 NVL(I_USER, USER),
                 SYSDATE,
                 'FIN',
                 I_EMPR
            FROM FIN_CUOTA
           WHERE CUO_CLAVE_DOC = I_DOC_CLAVE_FIN
             AND CUO_EMPR = I_EMPR;
      
        INSERT INTO FAC_DOCUMENTO_DET
          (DET_CLAVE_DOC,
           DET_NRO_ITEM,
           DET_ART,
           DET_NRO_REMIS,
           DET_CANT,
           DET_BRUTO_LOC,
           DET_BRUTO_MON,
           DET_COD_IVA,
           DET_IVA_LOC,
           DET_IVA_MON,
           DET_PORC_DTO,
           DET_NETO_LOC,
           DET_NETO_MON,
           DET_TIPO_COMISION,
           DET_DESC_LARGA,
           DET_CLAVE_OT,
           DET_CLAVE_PED,
           DET_DET_MARC,
           DET_EMPR,
           DET_PLAN_CLAVE,
           DET_PLAN_ITEM)
          SELECT V_CLAVE_FIN,
                 DET_NRO_ITEM,
                 DET_ART,
                 DET_NRO_REMIS,
                 DET_CANT,
                 DET_BRUTO_LOC,
                 DET_BRUTO_MON,
                 DET_COD_IVA,
                 DET_IVA_LOC,
                 DET_IVA_MON,
                 DET_PORC_DTO,
                 DET_NETO_LOC,
                 DET_NETO_MON,
                 DET_TIPO_COMISION,
                 DET_DESC_LARGA,
                 DET_CLAVE_OT,
                 DET_CLAVE_PED,
                 DET_DET_MARC,
                 DET_EMPR,
                 DET_PLAN_CLAVE,
                 DET_PLAN_ITEM
            FROM FAC_DOCUMENTO_DET
           WHERE DET_CLAVE_DOC = I_DOC_CLAVE_FIN
             AND DET_EMPR = I_EMPR;
      
        INSERT INTO STK_DOCUMENTO_DET
          (DETA_CLAVE_DOC,
           DETA_NRO_ITEM,
           DETA_ART,
           DETA_EMPR,
           DETA_NRO_REM,
           DETA_CANT,
           DETA_IMP_NETO_LOC,
           DETA_IMP_NETO_MON,
           DETA_IMPU,
           DETA_IVA_LOC,
           DETA_IVA_MON,
           DETA_PORC_DTO,
           DETA_IMP_BRUTO_LOC,
           DETA_IMP_BRUTO_MON,
           DETA_CANT_REM,
           DETA_PERIODO)
        
          SELECT V_CLAVE_STK,
                 DETA_NRO_ITEM,
                 DETA_ART,
                 DETA_EMPR,
                 DETA_NRO_REM,
                 DETA_CANT,
                 DETA_IMP_NETO_LOC,
                 DETA_IMP_NETO_MON,
                 DETA_IMPU,
                 DETA_IVA_LOC,
                 DETA_IVA_MON,
                 DETA_PORC_DTO,
                 DETA_IMP_BRUTO_LOC,
                 DETA_IMP_BRUTO_MON,
                 DETA_CANT_REM,
                 DETA_PERIODO
            FROM STK_DOCUMENTO_DET
           WHERE DETA_CLAVE_DOC = V_CLAVE_STK_FACT
             AND DETA_EMPR = I_EMPR;
        begin
          INSERT INTO FIN_DOC_ANULADO
            (ANUL_TIPO_MOV,
             ANUL_NRO_DOC,
             ANUL_EMPR,
             ANUL_SUC,
             ANUL_CLI_PROV,
             ANUL_FEC_DOC,
             ANUL_MOTIVO,
             ANUL_OBS,
             ANUL_NRO_TIMBRADO)
          VALUES
            (V_DOC_TIPO_MOV,
             V_DOC_NRO_DOC,
             I_EMPR,
             V_DOC_SUC,
             V_DOC_CLI_PROV,
             V_DOC_FEC_DOC,
             I_MOTIVO,
             NULL,
             V_DOC_NRO_TIMBRADO);
        
          COMMIT;
        EXCEPTION
          WHEN OTHERS THEN
          
            RAISE_APPLICATION_ERROR(-20001,
                                    SQLCODE || ' ' || SQLERRM ||
                                    'Error al eliminar el documento: ' ||
                                    I_DOC_CLAVE_FIN);
        end;
      
      ELSE
        BEGIN
          SELECT GS.SUC_DEP_DEVOL_VIRTUAL
            INTO V_DEP_ORIG
            FROM GEN_SUCURSAL GS
          
           WHERE GS.SUC_EMPR = I_EMPR
             AND GS.SUC_CODIGO = I_SUCURSAL
             AND GS.SUC_DEP_DEVOL = I_DEPOSITO;
        EXCEPTION
          WHEN OTHERS THEN
            V_DEP_ORIG := I_DEPOSITO;
          
          /*SELECT SD.DOCU_DEP_ORIG, SD.DOCU_SUC_ORIG
           INTO V_DEP_DEST, V_SUC_ORIG
           FROM (SELECT FD.DOC_CLAVE_STK FROM FIN_DOCUMENTO FD WHERE FD.DOC_CLAVE = I_DOC_CLAVE_FIN AND FD.DOC_EMPR = I_EMPR) CLAVE_STK, STK_DOCUMENTO SD     
            WHERE CLAVE_STK.DOC_CLAVE_STK = SD.DOCU_CLAVE
          AND SD.DOCU_EMPR = I_EMPR;*/
        END;
      
        /* BEGIN
        SELECT DEP.DEP_TRASLADO 
        INTO V_DEP_ORIG
        FROM STK_DEPOSITO DEP  
        WHERE DEP.DEP_EMPR = I_EMPR
        AND DEP.DEP_SUC = V_SUC_ORIG 
        AND DEP.DEP_CODIGO = V_DEP_DEST;
             EXCEPTION
           WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR(-200010, 'EL DEPOSITO DE ORIGEN NO TIENE UN DEPOSITO VIRTUAL ASOCIADO');
        END; */
      
        INSERT INTO APEX_ANUL_FACT
          (ANUL_FAC_CLAVE_DOC,
           ANUL_FAC_EMPR,
           ANUL_FAC_MOTIVO,
           ANUL_FAC_FEC_GRAB,
           ANUL_FAC_TELEFONO,
           ANUL_FAC_COD_USUARIO_CAST,
           ANUL_FAC_USUARIO_NOMBRE,
           ANUL_FAC_ESTADO,
           ANUL_FAC_DOCU_CLAVE_TRASLADO,
           ANUL_FAC_APROBADO,
           ANUL_FAC_DEP_ORIG,
           ANUL_FAC_DEP_DEST)
        values
          (I_DOC_CLAVE_FIN,
           I_EMPR,
           I_MOTIVO,
           I_DOC_FEC,
           '0',
           '0',
           I_USER,
           'P',
           STK_SEQ_DOCU_NEXTVAL --v_anul_fac_docu_clave_traslado
          ,
           NULL,
           V_DEP_ORIG,
           I_DEPOSITO);
      
      end if;
    
    elsif I_EMPR not in (1, 2, 3, 20) then
      begin
        insert into fin_aud_documento
          (adoc_clave,
           adoc_empr,
           adoc_cta_bco,
           adoc_tipo_mov,
           adoc_tipo_saldo,
           adoc_nro_doc,
           adoc_suc,
           adoc_mon,
           adoc_prov,
           adoc_cli,
           adoc_legajo,
           adoc_cli_nom,
           adoc_cli_dir,
           adoc_cli_tel,
           adoc_cli_ruc,
           adoc_nro_cheq,
           adoc_bco_cheque,
           adoc_est_cheque,
           adoc_fec_dep_cheque,
           adoc_nro_doc_dep,
           adoc_cheque_certif,
           adoc_fec_oper,
           adoc_fec_doc,
           adoc_fec_cheque,
           adoc_bruto_grav_loc,
           adoc_bruto_grav_mon,
           adoc_bruto_exen_loc,
           adoc_bruto_exen_mon,
           adoc_neto_grav_loc,
           adoc_neto_grav_mon,
           adoc_neto_exen_loc,
           adoc_neto_exen_mon,
           adoc_iva_loc,
           adoc_iva_mon,
           adoc_base_impon_loc,
           adoc_base_impon_mon,
           adoc_saldo_loc,
           adoc_saldo_mon,
           adoc_obs,
           adoc_fec_anul,
           adoc_login_anul,
           adoc_login,
           adoc_fec_grab,
           adoc_sist,
           adoc_operador,
           adoc_motivo_anulacion,
           adoc_timbrado,
           adoc_tipo_operacion,
           adoc_canal)
          select doc_clave,
                 doc_empr,
                 doc_cta_bco,
                 doc_tipo_mov,
                 doc_tipo_saldo,
                 doc_nro_doc,
                 doc_suc,
                 doc_mon,
                 doc_prov,
                 doc_cli,
                 doc_legajo,
                 doc_cli_nom,
                 doc_cli_dir,
                 doc_cli_tel,
                 doc_cli_ruc,
                 doc_nro_cheque,
                 doc_bco_cheque,
                 doc_est_cheque,
                 doc_fec_dep_cheque,
                 doc_nro_doc_dep,
                 doc_cheque_certif,
                 doc_fec_oper,
                 doc_fec_doc,
                 doc_fec_cheque,
                 doc_bruto_grav_loc,
                 doc_bruto_grav_mon,
                 doc_bruto_exen_loc,
                 doc_bruto_exen_mon,
                 doc_neto_grav_loc,
                 doc_neto_grav_mon,
                 doc_neto_exen_loc,
                 doc_neto_exen_mon,
                 doc_iva_loc,
                 doc_iva_mon,
                 doc_base_impon_loc,
                 doc_base_impon_mon,
                 doc_saldo_loc,
                 doc_saldo_mon,
                 I_OBS,
                 sysdate,
                 gen_devuelve_user,
                 doc_login,
                 doc_fec_grab,
                 doc_sist,
                 doc_operador,
                 I_MOTIVO,
                 doc_timbrado,
                 'ELIMINADOS',
                 doc_canal
            from fin_documento
           where doc_empr = I_EMPR
             and doc_clave = I_DOC_CLAVE_FIN;
      
        delete fac_documento_det
         where det_empr = I_EMPR
           and det_clave_doc = I_DOC_CLAVE_FIN;
        delete stk_documento_det
         where deta_empr = I_EMPR
           and deta_clave_doc = V_CLAVE_STK_FACT;
        delete stk_documento
         where docu_empr = I_EMPR
           and docu_clave = V_CLAVE_STK_FACT;
        delete fin_doc_concepto
         where dcon_empr = I_EMPR
           and dcon_clave_doc = I_DOC_CLAVE_FIN;
        delete fin_cuota
         where cuo_empr = I_EMPR
           and cuo_clave_doc = I_DOC_CLAVE_FIN;
        delete fin_documento
         where doc_empr = I_EMPR
           and doc_clave = I_DOC_CLAVE_FIN;
        COMMIT;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          RAISE_APPLICATION_ERROR(-20002,
                                  'No se ha encontrado datos de la factura');
        WHEN TOO_MANY_ROWS THEN
          RAISE_APPLICATION_ERROR(-20002,
                                  'Demasiados datos para una sola factura, comunicarse con dep Informatica');
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20002,
                                  'Error al anular la factura ' || sqlerrm);
      end;
    else ---> AQUI DEBE INGRESAR CONSULTAGRO
    
      BEGIN
        -- RAISE_APPLICATION_ERROR(-20000, 'erroR ' || I_DOC_CLAVE_FIN);
        INSERT INTO FIN_DOCUMENTO --TABLA EN LA CUAL SE INSERTA
        --------DECLARACION DE CAMPOS DE LA TABLA QUE SE VAN A INSERTAR--------------------
          (DOC_CLAVE,
           DOC_EMPR,
           DOC_CTA_BCO_FCON,
           DOC_COND_VTA,
           DOC_SUC,
           DOC_DPTO,
           DOC_TIPO_MOV,
           DOC_NRO_DOC,
           DOC_TIPO_SALDO,
           DOC_MON,
           DOC_PROV,
           DOC_CLI,
           DOC_CLI_NOM,
           DOC_CLI_DIR,
           DOC_CLI_RUC,
           DOC_CLI_TEL,
           DOC_LEGAJO,
           DOC_BCO_CHEQUE,
           DOC_NRO_CHEQUE,
           DOC_FEC_CHEQUE,
           DOC_FEC_DEP_CHEQUE,
           DOC_EST_CHEQUE,
           DOC_NRO_DOC_DEP,
           DOC_CHEQUE_CERTIF,
           DOC_FEC_OPER,
           DOC_FEC_DOC,
           DOC_BRUTO_EXEN_LOC,
           DOC_BRUTO_EXEN_MON,
           DOC_BRUTO_GRAV_LOC,
           DOC_BRUTO_GRAV_MON,
           DOC_NETO_EXEN_LOC,
           DOC_NETO_EXEN_MON,
           DOC_NETO_GRAV_LOC,
           DOC_NETO_GRAV_MON,
           DOC_IVA_LOC,
           DOC_IVA_MON,
           DOC_SALDO_INI_MON,
           DOC_SALDO_LOC,
           DOC_SALDO_MON,
           DOC_SALDO_PER_ACT_LOC,
           DOC_SALDO_PER_ACT_MON,
           DOC_BASE_IMPON_LOC,
           DOC_BASE_IMPON_MON,
           DOC_DIF_CAMBIO,
           DOC_IND_STK,
           DOC_CLAVE_STK,
           DOC_IND_CUOTA,
           DOC_IND_PEDIDO,
           DOC_IND_FINANRESA,
           DOC_IND_CONSIGNACION,
           DOC_PLAN_FINAN,
           DOC_NRO_LISTADO_LIBRO_IVA,
           DOC_CLAVE_PADRE,
           DOC_LOGIN,
           DOC_SIST,
           DOC_CLI_CODEUDOR,
           DOC_OPERADOR,
           DOC_COBRADOR,
           DOC_CANT_PAGARE,
           DOC_SERIE,
           DOC_CLAVE_SCLI,
           DOC_ORDEN_COMPRA_CLAVE,
           DOC_PRES_CLAVE,
           DOC_TASA,
           DOC_NRO_ORDEN_COMPRA,
           DOC_DIF_CAMBIO_ACUM,
           DOC_DIVISION,
           DOC_FORMA_ENTREGA,
           DOC_OBS,
           DOC_CODIGO_TEFE,
           DOC_CAMION,
           DOC_IND_ORD_CARGA,
           DOC_CLAVE_PEDIDO_CAST,
           DOC_IND_IMPR_VENDEDOR,
           DOC_IND_IMPR_CONYUGUE,
           DOC_CANAL,
           DOC_TIMBRADO,
           DOC_CTACO)
        
        ---------DECLARACION DE LOS VALORES QUE TOMA EL PROCEDIMIENTO PARA INSERTAR-------------
          SELECT V_CLAVE_FIN,
                 DOC_EMPR,
                 DOC_CTA_BCO_FCON,
                 DOC_COND_VTA,
                 I_SUCURSAL,
                 DOC_DPTO,
                 47,
                 DOC_NRO_DOC,
                 'C',
                 DOC_MON,
                 DOC_PROV,
                 DOC_CLI,
                 DOC_CLI_NOM,
                 DOC_CLI_DIR,
                 DOC_CLI_RUC,
                 DOC_CLI_TEL,
                 DOC_LEGAJO,
                 DOC_BCO_CHEQUE,
                 DOC_NRO_CHEQUE,
                 DOC_FEC_CHEQUE,
                 DOC_FEC_DEP_CHEQUE,
                 DOC_EST_CHEQUE,
                 DOC_NRO_DOC_DEP,
                 DOC_CHEQUE_CERTIF,
                 I_DOC_FEC,
                 V_DOC_FEC_DOC,
                 DOC_BRUTO_EXEN_LOC,
                 DOC_BRUTO_EXEN_MON,
                 DOC_BRUTO_GRAV_LOC,
                 DOC_BRUTO_GRAV_MON,
                 DOC_NETO_EXEN_LOC,
                 DOC_NETO_EXEN_MON,
                 DOC_NETO_GRAV_LOC,
                 DOC_NETO_GRAV_MON,
                 DOC_IVA_LOC,
                 DOC_IVA_MON,
                 DOC_SALDO_INI_MON,
                 DOC_SALDO_LOC,
                 DOC_SALDO_MON,
                 DOC_SALDO_PER_ACT_LOC,
                 DOC_SALDO_PER_ACT_MON,
                 DOC_BASE_IMPON_LOC,
                 DOC_BASE_IMPON_MON,
                 DOC_DIF_CAMBIO,
                 DOC_IND_STK,
                 V_CLAVE_STK,
                 DOC_IND_CUOTA,
                 DOC_IND_PEDIDO,
                 DOC_IND_FINANRESA,
                 DOC_IND_CONSIGNACION,
                 DOC_PLAN_FINAN,
                 DOC_NRO_LISTADO_LIBRO_IVA,
                 I_DOC_CLAVE_FIN,
                 NVL(I_USER, USER),
                 DOC_SIST,
                 DOC_CLI_CODEUDOR,
                 DOC_OPERADOR,
                 DOC_COBRADOR,
                 DOC_CANT_PAGARE,
                 DOC_SERIE,
                 DOC_CLAVE_SCLI,
                 DOC_ORDEN_COMPRA_CLAVE,
                 DOC_PRES_CLAVE,
                 DOC_TASA,
                 DOC_NRO_ORDEN_COMPRA,
                 DOC_DIF_CAMBIO_ACUM,
                 DOC_DIVISION,
                 DOC_FORMA_ENTREGA,
                 I_OBS,
                 DOC_CODIGO_TEFE,
                 DOC_CAMION,
                 DOC_IND_ORD_CARGA,
                 DOC_CLAVE_PEDIDO_CAST,
                 DOC_IND_IMPR_VENDEDOR,
                 DOC_IND_IMPR_CONYUGUE,
                 DOC_CANAL,
                 DOC_TIMBRADO,
                 DOC_CTACO
            FROM FIN_DOCUMENTO
           WHERE DOC_CLAVE = I_DOC_CLAVE_FIN
             AND DOC_EMPR = I_EMPR;
      
        -- raise_application_error(-20001,'An error was encountered - '||I_DOC_CLAVE_FIN);
        --POST;
      EXCEPTION
        WHEN OTHERS THEN
          --DD ROLLBACK;
          RAISE_APPLICATION_ERROR(-20000,
                                  SQLERRM ||
                                  ' ERROR AL INSERTAR FIN_DOCUMENTO N? ' ||
                                  I_DOC_CLAVE_FIN);
      END;
    
      BEGIN
        INSERT INTO FIN_DOC_CONCEPTO
          (DCON_CLAVE_DOC,
           DCON_ITEM,
           DCON_CLAVE_CONCEPTO,
           DCON_CLAVE_CTACO,
           DCON_TIPO_SALDO,
           DCON_EXEN_LOC,
           DCON_EXEN_MON,
           DCON_GRAV_LOC,
           DCON_GRAV_MON,
           DCON_IVA_LOC,
           DCON_IVA_MON,
           DCON_CLAVE_IMP,
           DCON_IND_TIPO_IVA_COMPRA,
           DCON_OBS,
           DCON_PORC_IVA,
           DCON_IND_IMAGRO,
           DCON_CANAL,
           DCON_DIV_CANAL,
           DCON_CANT_COMB,
           DCON_KM_ACTUAL,
           DCON_OCOMD_CLAVE,
           DCON_OCOMD_NRO_ITEM,
           DCON_EMPR,
           DCON_OCOMD_PORC,
           DCON_IMP_AFECTA_COSTO)
          SELECT V_CLAVE_FIN,
                 D.DCON_ITEM,
                 D.DCON_CLAVE_CONCEPTO,
                 D.DCON_CLAVE_CTACO,
                 'C',
                 D.DCON_EXEN_LOC,
                 D.DCON_EXEN_MON,
                 D.DCON_GRAV_LOC,
                 D.DCON_GRAV_MON,
                 D.DCON_IVA_LOC,
                 D.DCON_IVA_MON,
                 D.DCON_CLAVE_IMP,
                 D.DCON_IND_TIPO_IVA_COMPRA,
                 D.DCON_OBS,
                 D.DCON_PORC_IVA,
                 D.DCON_IND_IMAGRO,
                 D.DCON_CANAL,
                 D.DCON_DIV_CANAL,
                 D.DCON_CANT_COMB,
                 D.DCON_KM_ACTUAL,
                 D.DCON_OCOMD_CLAVE,
                 D.DCON_OCOMD_NRO_ITEM,
                 D.DCON_EMPR,
                 D.DCON_OCOMD_PORC,
                 D.DCON_IMP_AFECTA_COSTO
            FROM FIN_DOC_CONCEPTO D
           WHERE DCON_EMPR = I_EMPR
             AND DCON_CLAVE_DOC = I_DOC_CLAVE_FIN
          --AND DCON_IVA_LOC > 0
          ;
      EXCEPTION
        WHEN OTHERS THEN
          --DD ROLLBACK;
          RAISE_APPLICATION_ERROR(-20000,
                                  SQLERRM ||
                                  ' ERROR AL INSERTAR FIN_DOC_CONCEPTO N? ' ||
                                  I_DOC_CLAVE_FIN);
      END;
    
      --------------------------------------------------------------------------------------
      --*--
      --RAISE_APPLICATION_ERROR(-20000,('V_CLAVE_STK= '||V_CLAVE_STK);
      BEGIN
        INSERT INTO STK_DOCUMENTO
          (DOCU_CLAVE,
           DOCU_EMPR,
           DOCU_CODIGO_OPER,
           DOCU_NRO_DOC,
           DOCU_SUC_ORIG,
           DOCU_DEP_ORIG,
           DOCU_SUC_DEST,
           DOCU_DEP_DEST,
           DOCU_MON,
           DOCU_PROV,
           DOCU_CLI,
           DOCU_CLI_NOM,
           DOCU_CLI_RUC,
           DOCU_LEGAJO,
           DOCU_DPTO,
           DOCU_SECCION,
           DOCU_FEC_EMIS,
           DOCU_FEC_OPER,
           DOCU_TIPO_MOV,
           DOCU_GRAV_NETO_LOC,
           DOCU_GRAV_NETO_MON,
           DOCU_EXEN_NETO_LOC,
           DOCU_EXEN_NETO_MON,
           DOCU_GRAV_BRUTO_LOC,
           DOCU_GRAV_BRUTO_MON,
           DOCU_EXEN_BRUTO_LOC,
           DOCU_EXEN_BRUTO_MON,
           DOCU_IVA_LOC,
           DOCU_IVA_MON,
           DOCU_TASA_US,
           DOCU_IND_CUOTA,
           DOCU_IND_CONSIGNACION,
           DOCU_OBS,
           DOCU_CLAVE_PADRE,
           DOCU_NRO_LISTADO_AUD,
           DOCU_LOGIN,
           DOCU_SIST,
           DOCU_OPERADOR,
           DOCU_NRO_PED,
           DOCU_OCARGA_LONDON,
           DOCU_MOT_NCR,
           DOCU_CLAVE_STOPES,
           DOCU_AFECT_COMISION) -------------------------------------lv I_EMPR/12/2020 para saber si afecta o no para el calculo de comision de descargadores
          SELECT V_CLAVE_STK,
                 DOCU_EMPR,
                 I_EMPR,
                 DOCU_NRO_DOC,
                 I_SUCURSAL,
                 I_DEPOSITO,
                 DOCU_SUC_DEST,
                 DOCU_DEP_DEST,
                 DOCU_MON,
                 DOCU_PROV,
                 DOCU_CLI,
                 DOCU_CLI_NOM,
                 DOCU_CLI_RUC,
                 DOCU_LEGAJO,
                 DOCU_DPTO,
                 DOCU_SECCION,
                 V_DOC_FEC_DOC,
                 I_DOC_FEC,
                 47,
                 DOCU_GRAV_NETO_LOC,
                 DOCU_GRAV_NETO_MON,
                 DOCU_EXEN_NETO_LOC,
                 DOCU_EXEN_NETO_MON,
                 DOCU_GRAV_BRUTO_LOC,
                 DOCU_GRAV_BRUTO_MON,
                 DOCU_EXEN_BRUTO_LOC,
                 DOCU_EXEN_BRUTO_MON,
                 DOCU_IVA_LOC,
                 DOCU_IVA_MON,
                 DOCU_TASA_US,
                 DOCU_IND_CUOTA,
                 DOCU_IND_CONSIGNACION,
                 I_OBS,
                 V_CLAVE_STK_FACT,
                 DOCU_NRO_LISTADO_AUD,
                 NVL(I_USER, USER),
                 DOCU_SIST,
                 DOCU_OPERADOR,
                 DOCU_NRO_PED,
                 DOCU_OCARGA_LONDON,
                 I_MOTIVO,
                 I_DOCU_CLAVE_STOPES,
                 I_AFECT_COMISION
            FROM STK_DOCUMENTO
           WHERE DOCU_CLAVE = V_CLAVE_STK_FACT
             AND DOCU_EMPR = I_EMPR;
      
        --POST;
      EXCEPTION
        WHEN OTHERS THEN
          --DD ROLLBACK;
          RAISE_APPLICATION_ERROR(-20001,
                                  SQLERRM ||
                                  ' ERROR AL INSERTAR STK_DOCUMENTO N? ' ||
                                  V_CLAVE_STK_FACT);
      END;
    
      INSERT INTO FIN_PAGO
        (PAG_CLAVE_DOC,
         PAG_FEC_VTO,
         PAG_CLAVE_PAGO,
         PAG_FEC_PAGO,
         PAG_IMP_LOC,
         PAG_IMP_MON,
         PAG_LOGIN,
         PAG_FEC_GRAB,
         PAG_SIST,
         PAG_EMPR)
        SELECT CUO_CLAVE_DOC,
               CUO_FEC_VTO,
               V_CLAVE_FIN,
               I_DOC_FEC,
               CUO_IMP_LOC,
               CUO_IMP_MON,
               NVL(I_USER, USER),
               SYSDATE,
               'FIN',
               I_EMPR
          FROM FIN_CUOTA
         WHERE CUO_CLAVE_DOC = I_DOC_CLAVE_FIN
           AND CUO_EMPR = I_EMPR;
    
      INSERT INTO FAC_DOCUMENTO_DET
        (DET_CLAVE_DOC,
         DET_NRO_ITEM,
         DET_ART,
         DET_NRO_REMIS,
         DET_CANT,
         DET_BRUTO_LOC,
         DET_BRUTO_MON,
         DET_COD_IVA,
         DET_IVA_LOC,
         DET_IVA_MON,
         DET_PORC_DTO,
         DET_NETO_LOC,
         DET_NETO_MON,
         DET_TIPO_COMISION,
         DET_DESC_LARGA,
         DET_CLAVE_OT,
         DET_CLAVE_PED,
         DET_DET_MARC,
         DET_EMPR,
         DET_PLAN_CLAVE,
         DET_PLAN_ITEM)
        SELECT V_CLAVE_FIN,
               DET_NRO_ITEM,
               DET_ART,
               DET_NRO_REMIS,
               DET_CANT,
               DET_BRUTO_LOC,
               DET_BRUTO_MON,
               DET_COD_IVA,
               DET_IVA_LOC,
               DET_IVA_MON,
               DET_PORC_DTO,
               DET_NETO_LOC,
               DET_NETO_MON,
               DET_TIPO_COMISION,
               DET_DESC_LARGA,
               DET_CLAVE_OT,
               DET_CLAVE_PED,
               DET_DET_MARC,
               DET_EMPR,
               DET_PLAN_CLAVE,
               DET_PLAN_ITEM
          FROM FAC_DOCUMENTO_DET
         WHERE DET_CLAVE_DOC = I_DOC_CLAVE_FIN
           AND DET_EMPR = I_EMPR;
    
      INSERT INTO STK_DOCUMENTO_DET
        (DETA_CLAVE_DOC,
         DETA_NRO_ITEM,
         DETA_ART,
         DETA_EMPR,
         DETA_NRO_REM,
         DETA_CANT,
         DETA_IMP_NETO_LOC,
         DETA_IMP_NETO_MON,
         DETA_IMPU,
         DETA_IVA_LOC,
         DETA_IVA_MON,
         DETA_PORC_DTO,
         DETA_IMP_BRUTO_LOC,
         DETA_IMP_BRUTO_MON,
         DETA_CANT_REM,
         DETA_PERIODO)
      
        SELECT V_CLAVE_STK,
               DETA_NRO_ITEM,
               DETA_ART,
               DETA_EMPR,
               DETA_NRO_REM,
               DETA_CANT,
               DETA_IMP_NETO_LOC,
               DETA_IMP_NETO_MON,
               DETA_IMPU,
               DETA_IVA_LOC,
               DETA_IVA_MON,
               DETA_PORC_DTO,
               DETA_IMP_BRUTO_LOC,
               DETA_IMP_BRUTO_MON,
               DETA_CANT_REM,
               DETA_PERIODO
          FROM STK_DOCUMENTO_DET
         WHERE DETA_CLAVE_DOC = V_CLAVE_STK_FACT
           AND DETA_EMPR = I_EMPR;
      begin
        INSERT INTO FIN_DOC_ANULADO
          (ANUL_TIPO_MOV,
           ANUL_NRO_DOC,
           ANUL_EMPR,
           ANUL_SUC,
           ANUL_CLI_PROV,
           ANUL_FEC_DOC,
           ANUL_MOTIVO,
           ANUL_OBS,
           ANUL_NRO_TIMBRADO)
        VALUES
          (V_DOC_TIPO_MOV,
           V_DOC_NRO_DOC,
           I_EMPR,
           V_DOC_SUC,
           V_DOC_CLI_PROV,
           V_DOC_FEC_DOC,
           I_MOTIVO,
           NULL,
           V_DOC_NRO_TIMBRADO);
      
        COMMIT;
      EXCEPTION
        WHEN OTHERS THEN
        
          RAISE_APPLICATION_ERROR(-20001,
                                  SQLCODE || ' ' || SQLERRM ||
                                  'Error al eliminar el documento: ' ||
                                  I_DOC_CLAVE_FIN);
      end;
    
    end if;
  
  END;
END FACP009;
/
