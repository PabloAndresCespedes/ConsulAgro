CREATE OR REPLACE VIEW PER_PERI047_V AS
SELECT DISTINCT EMPL_LEGAJO,
                A.EMPL_NOMBRE,
                A.EMPL_CODIGO_PROV,
                A.DPTO_DESC,
                A.PCON_CLAVE,
                A.PCON_DESC,
                A.FIN_OBS,
                A.CUO_CLAVE_DOC,
                A.CUO_FEC_VTO,
                A.CUO_IMP_MON,
                A.CUO_SALDO_MON,
                A.PCON_CANCELADO_POR_CONC,
                A.EMPL_SUCURSAL,
                A.EMPL_EMPRESA,
                A.EMPL_MON_PAGO,
                DEP.DPTO_GRUPO_PAGO,
                EMPL_DEPARTAMENTO,
                empl_forma_pago
  FROM (SELECT DISTINCT EM.EMPL_LEGAJO,
                        SUBSTR(EM.EMPL_NOMBRE || ' ' || EM.EMPL_APE, 1, 40) EMPL_NOMBRE,
                        EM.EMPL_CODIGO_PROV,
                        EM.EMPL_DEPARTAMENTO,
                        DE.DPTO_DESC,
                        PCO.PCON_CLAVE,
                        PCO.PCON_DESC,
                        NVL(DO.DOC_OBS, FCO.DCON_OBS) FIN_OBS,
                        CU.CUO_CLAVE_DOC,
                        CU.CUO_FEC_VTO,
                        CU.CUO_IMP_MON,
                        CU.CUO_SALDO_MON,
                        PCO.PCON_CANCELADO_POR_CONC,
                        EM.EMPL_SUCURSAL,
                        EM.EMPL_EMPRESA,
                        EMPL_MON_PAGO,
                        empl_forma_pago
          FROM FIN_DOCUMENTO    DO,
               FIN_DOC_CONCEPTO FCO,
               FIN_CUOTA        CU,
               FIN_PROVEEDOR    PRO,
               PER_EMPLEADO     EM,
               GEN_DEPARTAMENTO DE,
               PER_DPTO_CONC    DCO,
               PER_CONCEPTO     PCO
         WHERE DO.DOC_CLAVE = FCO.DCON_CLAVE_DOC
           AND DO.DOC_EMPR = FCO.DCON_EMPR

           AND DO.DOC_CLAVE = CU.CUO_CLAVE_DOC
           AND DO.DOC_EMPR = CU.CUO_EMPR

           AND DO.DOC_PROV = PRO.PROV_CODIGO
           AND DO.DOC_EMPR = PRO.PROV_EMPR

           AND PRO.PROV_CODIGO = EM.EMPL_CODIGO_PROV
           AND PRO.PROV_EMPR = EM.EMPL_EMPRESA

           AND EM.EMPL_DEPARTAMENTO = DE.DPTO_CODIGO
           AND EM.EMPL_EMPRESA = DE.DPTO_EMPR

           AND DCO.DPTOC_DPTO = DE.DPTO_CODIGO
           AND DCO.DPTOC_EMPR = DE.DPTO_EMPR

           AND FCO.DCON_CLAVE_CONCEPTO = DCO.DPTOC_FIN_CONC
           AND FCO.DCON_EMPR = DCO.DPTOC_EMPR

           AND DCO.DPTOC_PER_CONC = PCO.PCON_CLAVE
           AND DCO.DPTOC_EMPR = PCO.PCON_EMPR

           AND DO.DOC_TIPO_MOV IN (31, 81)
           AND CU.CUO_SALDO_MON <> 0
           AND DO.DOC_MON = 1
           AND DO.DOC_EMPR = 1
           AND EM.EMPL_SITUACION = 'A'

        UNION ALL

        SELECT DISTINCT EM.EMPL_LEGAJO,
                        SUBSTR(EM.EMPL_NOMBRE || ' ' || EM.EMPL_APE, 1, 40) EMPL_NOMBRE,
                        EM.EMPL_CODIGO_PROV,
                        EM.EMPL_DEPARTAMENTO,
                        DE.DPTO_DESC,
                        1 PCON_CLAVE,
                        'ADELA' PCON_DESC,
                        NVL(DO.DOC_OBS, FCO.DCON_OBS) FIN_OBS,
                        CU.CUO_CLAVE_DOC,
                        CU.CUO_FEC_VTO,
                        CU.CUO_IMP_MON,
                        CU.CUO_SALDO_MON,
                        18 PCON_CANCELADO_POR_CONC,
                        EM.EMPL_SUCURSAL,
                        EM.EMPL_EMPRESA,
                        EMPL_MON_PAGO,
                        empl_forma_pago
          FROM FIN_DOCUMENTO    DO,
               FIN_DOC_CONCEPTO FCO,
               FIN_CUOTA        CU,
               FIN_PROVEEDOR    PRO,
               PER_EMPLEADO     EM,
               GEN_DEPARTAMENTO DE,
               GEN_EMPRESA      GE
         WHERE DO.DOC_CLAVE = FCO.DCON_CLAVE_DOC
           AND DO.DOC_CLAVE = CU.CUO_CLAVE_DOC
           AND DO.DOC_PROV = PRO.PROV_CODIGO
           AND PRO.PROV_CODIGO = EM.EMPL_CODIGO_PROV
           AND EM.EMPL_DEPARTAMENTO = DE.DPTO_CODIGO
           AND FCO.DCON_CLAVE_CONCEPTO IN
               (1367, 1366, 987, 1301, 884, 6593, 1480, 883, 6592,  80,752,67)
           AND DO.DOC_TIPO_MOV IN (31, 81)
           AND CU.CUO_SALDO_MON <> 0
           AND DO.DOC_MON = 1
           AND EM.EMPL_SITUACION = 'A'
           AND DO.DOC_EMPR = GE.EMPR_CODIGO
           AND FCO.DCON_EMPR = GE.EMPR_CODIGO
           AND CU.CUO_EMPR = GE.EMPR_CODIGO
           AND PRO.PROV_EMPR = GE.EMPR_CODIGO
           AND DE.DPTO_EMPR = GE.EMPR_CODIGO
           AND DO.DOC_EMPR = 1
           AND CUO_CLAVE_DOC NOT IN
               (SELECT DISTINCT CUO_CLAVE_DOC
                  FROM FIN_DOCUMENTO    DO,
                       FIN_DOC_CONCEPTO FCO,
                       FIN_CUOTA        CU,
                       FIN_PROVEEDOR    PRO,
                       PER_EMPLEADO     EM,
                       GEN_DEPARTAMENTO DE,
                       PER_DPTO_CONC    DCO,
                       PER_CONCEPTO     PCO,
                       GEN_EMPRESA      GE
                 WHERE DO.DOC_CLAVE = FCO.DCON_CLAVE_DOC
                   AND DO.DOC_CLAVE = CU.CUO_CLAVE_DOC
                   AND DO.DOC_PROV = PRO.PROV_CODIGO
                   AND PRO.PROV_CODIGO = EM.EMPL_CODIGO_PROV
                   AND EM.EMPL_DEPARTAMENTO = DE.DPTO_CODIGO
                   AND DCO.DPTOC_DPTO = DE.DPTO_CODIGO
                   AND FCO.DCON_CLAVE_CONCEPTO = DCO.DPTOC_FIN_CONC
                   AND DCO.DPTOC_PER_CONC = PCO.PCON_CLAVE
                   AND DO.DOC_TIPO_MOV IN (31, 81)
                   AND CU.CUO_SALDO_MON <> 0
                   AND CU.CUO_SALDO_MON <> 0
                   AND DO.DOC_MON = 1
                   AND EM.EMPL_SITUACION = 'A'
                   AND DO.DOC_EMPR = GE.EMPR_CODIGO
                   AND FCO.DCON_EMPR = GE.EMPR_CODIGO
                   AND CU.CUO_EMPR = GE.EMPR_CODIGO
                   AND PRO.PROV_EMPR = GE.EMPR_CODIGO
                   AND EM.EMPL_EMPRESA = GE.EMPR_CODIGO
                   AND DE.DPTO_EMPR = GE.EMPR_CODIGO
                   AND DCO.DPTOC_EMPR = GE.EMPR_CODIGO
                   AND PCO.PCON_EMPR = GE.EMPR_CODIGO
                   AND DO.DOC_EMPR = 1)

        ) A,
       GEN_DEPARTAMENTO DEP,
       PER_RECIB_VAC_ADEL VAC
 WHERE DPTO_CODIGO = EMPL_DEPARTAMENTO
   AND DPTO_EMPR = 1
   AND A.EMPL_EMPRESA = 1
   AND A.CUO_CLAVE_DOC = VAC.VACADEL_CLAVE_DOC_ADEL(+)
   AND A.EMPL_EMPRESA = VAC.VACADEL_EMPR(+)
   AND VAC.VACADEL_CLAVE_DOC_ADEL IS NULL

 ORDER BY EMPL_NOMBRE;
