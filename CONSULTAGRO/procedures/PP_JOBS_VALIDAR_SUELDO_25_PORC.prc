CREATE OR REPLACE PROCEDURE PP_JOBS_VALIDAR_SUELDO_25_PORC IS
  co_cagro       constant gen_empresa.empr_codigo%type := empresas_pck.co_consultagro;
  co_empl_activo constant per_empleado.empl_situacion%type := 'A';
  co_format_date constant varchar2(8 char) := 'dd/mm/yy';
  co_tmv_fac_cont_rec constant number := 1;
  co_tmv_fac_cred_rec constant number := 2;
  
  CURSOR C is  
    SELECT EMPL_LEGAJO        LEGAJO,
           EMPL_EMPRESA       EMPRESA,
           EMPL_SITUACION,
           F.EMPL_CHEK_LIM_CD,
           F.EMPL_PORCENTAJE,
           F.EMPL_FORMA_PAGO,
           EMPL_COD_CLIENTE,
           F.EMPL_CODIGO_PROV,
           (lc.porcentaje / 100) porc_calculo
      FROM PER_EMPLEADO F
      inner join gen_porc_lim_cre_func lc on (lc.empresa_id = f.empl_empresa)
      full outer join per_empleado_credito_fijo cf on (cf.empleado_id = f.empl_legajo and f.empl_empresa = cf.empresa_id)
     WHERE F.EMPL_EMPRESA = co_cagro
       AND EMPL_SITUACION = co_empl_activo
       and cf.empleado_id is null
       and cf.empresa_id is null
       ;

  V_IMPORTE    NUMBER := 0;
  V_SUEDO_25   NUMBER := 0;
  V_FEC_INI    DATE := TRUNC(SYSDATE, 'MM');
  V_FEC_FIN    DATE := LAST_DAY(TRUNC(SYSDATE));
  DESTINATARIO VARCHAR2(600);
  COPIA        VARCHAR2(600);
  OCULTA       VARCHAR2(150);
  
begin
  -- SETEA A 0 EL LIMITE EL EMPLEADO
  UPDATE PER_EMPLEADO a
     SET EMPL_LIMITE_CRED = 0
   WHERE EMPL_EMPRESA = co_cagro;
   
  <<f_calculo_individual>>
  FOR V IN C loop
    IF V.EMPL_SITUACION = co_empl_activo THEN
      BEGIN
        SELECT SUM(PER_DOCUMENTO_DET.PDDET_IMP) IMPORTE
          INTO V_IMPORTE
          FROM PER_DOCUMENTO,
               PER_DOCUMENTO_DET,
               PER_CONCEPTO,
               PER_EMPLEADO E
         WHERE PER_DOCUMENTO_DET.PDDET_CLAVE_CONCEPTO =
               PER_CONCEPTO.PCON_CLAVE
           AND PER_DOCUMENTO.PDOC_CLAVE = PER_DOCUMENTO_DET.PDDET_CLAVE_DOC
           AND PER_DOCUMENTO.PDOC_EMPLEADO = E.EMPL_LEGAJO
              
           AND PER_DOCUMENTO_DET.PDDET_EMPR = PER_CONCEPTO.PCON_EMPR
           AND PER_DOCUMENTO.PDOC_EMPR = PER_DOCUMENTO_DET.PDDET_EMPR
           AND PER_DOCUMENTO.PDOC_EMPR = E.EMPL_EMPRESA
           AND E.EMPL_EMPRESA = co_cagro
           AND PCON_IND_AGUINALDO = 'S'
           AND (TO_DATE(PDOC_FEC, co_format_date) between TO_DATE(V_FEC_INI, co_format_date) AND TO_DATE(V_FEC_FIN, co_format_date))
           AND EMPL_SITUACION = co_empl_activo
           AND PER_CONCEPTO.PCON_IND_DBCR = 'C'
           AND PDOC_EMPLEADO = V.LEGAJO
           AND E.EMPL_EMPRESA = V.EMPRESA;
      EXCEPTION
        WHEN OTHERS THEN
          V_IMPORTE := 0;
      END;
    ELSE
      V_IMPORTE := 0;
    END if;
    
    IF V.EMPL_FORMA_PAGO = 3 then
      
      SELECT ROUND(SUM(A.DCON_GRAV_MON + A.DCON_EXEN_MON) * v.porc_calculo)
        INTO V_SUEDO_25
        FROM FIN_DOCUMENTO S, fin_doc_concepto a, FIN_CONCEPTO C
       WHERE DOC_EMPR = co_cagro
         AND DOC_TIPO_MOV IN (co_tmv_fac_cont_rec, co_tmv_fac_cred_rec)
         AND A.DCON_CLAVE_CONCEPTO = C.FCON_CLAVE
         AND A.DCON_EMPR = C.FCON_EMPR
         and s.doc_clave = a.dcon_clave_doc
         and s.doc_empr = a.dcon_empr
         AND A.DCON_CLAVE_CONCEPTO <> 603
         AND C.FCON_DESC not LIKE '%AGUINAL%'
         AND DOC_FEC_DOC BETWEEN TO_DATE(V_FEC_INI, 'dd/mm/yy') AND
             TO_DATE(V_FEC_FIN, 'dd/mm/yy')
         AND DOC_PROV = V.EMPL_CODIGO_PROV;
      
    ELSE
      V_SUEDO_25 := round(V_IMPORTE * v.porc_calculo);
    END if;
  
    UPDATE PER_EMPLEADO
       SET EMPL_LIMITE_CRED = V_SUEDO_25
     WHERE EMPL_LEGAJO = V.LEGAJO
       AND EMPL_EMPRESA = V.EMPRESA;
  
  END loop f_calculo_individual;
  
  <<f_creditos_fijos>>
  for cfe in (select cf.empleado_id   legajo,
                   cf.monto_credito monto,
                   cf.empresa_id    empresa
            from per_empleado_credito_fijo cf
            where cf.empresa_id = co_cagro
            )
  loop
    update per_empleado
    set empl_limite_cred = cfe.monto
    where empl_legajo    = cfe.legajo
    and empl_empresa     = cfe.empresa;
  end loop f_creditos_fijos; 
  
  COMMIT;
  
  SELECT T.CO_PARA, T.CO_CC, T.CO_CCO
    INTO DESTINATARIO, COPIA, OCULTA
    FROM GEN_CORREOS T
   WHERE T.CO_ACCION = 'Validar_Sueldo';

  IF DESTINATARIO IS NOT NULL THEN
  
    SEND_EMAIL(P_SENDER     => 'NOREPLY',
               P_RECIPIENTS => DESTINATARIO,
               P_CC         => COPIA,
               P_BCC        => OCULTA,
               P_SUBJECT    => 'SE EJECUTO PERFECTAMENTE EL LIMITE DE CREDITO DEL 25%',
               P_MESSAGE    => 'EL PROCEDIMIENTO SE EJECUTO CON EXITO',
               P_MIME_TYPE  => 'text/html');
  END IF;

  -- END IF;
END PP_JOBS_VALIDAR_SUELDO_25_PORC;
/
