create or replace procedure fin_aprobacion_automatica(in_doc_clave fin_documento_comi015_temp.doc_clave%type,
                                                      in_empresa   fin_documento_comi015_temp.doc_empr%type,
                                                      in_tmv       number := null
                                                      ) is
/*
  Author  : @PabloACespedes \(^-^)/
  Created : 31/08/2022 9:43:57
  es una manera de automatizar el proceso que se_utiliza en el 8-2-65
  para aprobar de forma automatica el presupuesto
  
  En APEX se_utiliza en la APP: 126, PAGE: 126, Al aprobar documento desde contabilidad
*/
 co_aprobado  constant cnt_doc_estado_aprob.docesta_pres_estado%type := 'A';
 
 l_user       gen_operador.oper_login%type;
 l_estado_act cnt_doc_estado_aprob.docesta_cont_estado%type;
 l_dummy      number;
begin
   -- hasta ahora solo factura contado y credito recibida entran en la automatizacion de presupuesto
   <<val_tmv>>
   begin
     select count(1) data
     into l_dummy
     from fin_configuracion f
     where f.conf_empr = in_empresa
     and  ( f.conf_fact_cr_rec = in_tmv 
            or 
            f.conf_fact_co_rec = in_tmv
            or
            f.conf_tmov_var_cr = in_tmv
          );
   exception
     when others then
       l_dummy := 0;
   end val_tmv;
   
   if nvl(l_dummy, 0) > 0 then
     l_user := gen_devuelve_user;
     
     <<obt_estado_actual>>
     begin
       select p.docesta_cont_estado
       into l_estado_act
       from cnt_doc_estado_aprob p
       where p.docesta_clave_doc = in_doc_clave
       and p.docesta_empr = in_empresa;
       
       if l_estado_act is not null and l_estado_act <> co_aprobado then
          update fin_documento_comi015_temp
          set    comi005_estado = 'T'
          where  doc_clave = in_doc_clave
          and    doc_empr  = in_empresa;
       end if;
       
       update cnt_doc_estado_aprob
       set docesta_pres_estado = 'S',
           docesta_pres_usuar  = l_user,
           docesta_pres_fecha  = sysdate
       where docesta_clave_doc = in_doc_clave
       and   docesta_empr      = in_empresa;
       
     exception
       when no_data_found then
         insert into cnt_doc_estado_aprob
          (docesta_clave_doc,
           docesta_pres_estado,
           docesta_pres_usuar,
           docesta_pres_fecha,
           docesta_empr)
        values
          (in_doc_clave,
           'S',
           l_user,
           sysdate,
           in_empresa
           );
     end obt_estado_actual;
   end if;
exception
  when others then
    raise_application_error(-20000, 'Prc: FIN_APROBACION_AUTOMATICA - '||sqlcode||': '||sqlerrm);
end fin_aprobacion_automatica;
/
