create or replace trigger gen_mensaje_bi
  before insert
  on GEN_MENSAJE 
  for each row
-- 29/07/2022 10:31:13 @PabloACespedes \(^-^)/
-- asigna PK
begin
  if :new.men_codigo is null then
    :new.men_codigo := SEQ_GEN_MENSAJE.nextval;
  end if;
end gen_mensaje_bi;
/
