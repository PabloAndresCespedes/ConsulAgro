CREATE OR REPLACE TRIGGER "FIN_DOCUMENTO_AUD_MON_LOC"
  BEFORE INSERT ON FIN_DOCUMENTO_COMI015_TEMP
  REFERENCING OLD AS O NEW AS N
/*
  Detecte diferencias entre el mon y loc con los TM 25 y 26 pero casos muy raros. Pero genera mucha diferencia en las cajas, por eso puse este trigger para poder detectar esas diferencias cuando ocurran
  04/09/2019
  */
  FOR EACH ROW
   WHEN (N.DOC_EMPR = 1 AND N.DOC_TIPO_MOV IN (25, 26, 31, 18)) DECLARE
BEGIN

  IF INSERTING THEN

    IF :N.DOC_NETO_EXEN_LOC <> :N.DOC_NETO_EXEN_MON AND :N.DOC_MON = 1 THEN
       --En Gs.
       RAISE_APPLICATION_ERROR(-20001,
                                'Existe una diferencia en entre el DOC_NETO_EXEN_LOC: '||:N.DOC_NETO_EXEN_LOC||' y DOC_NETO_EXEN_MON: '||:N.DOC_NETO_EXEN_MON||'. Avise al Dpto. de Informatica. ');
    END IF;
  END IF;
exception
  
        when others then 
                      RAISE_APPLICATION_ERROR(-20001,
                              'Error en el trigger FIN_DOCUMENTO_AUD_MON_LOC'||SQLCODE||'-'||SQLERRM);
END;
/
