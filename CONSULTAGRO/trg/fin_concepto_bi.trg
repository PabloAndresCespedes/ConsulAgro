create or replace trigger fin_concepto_bi
  before insert
  on FIN_CONCEPTO 
  for each row
-- 26/07/2022 14:23:07 @PabloACespedes \(^-^)/
-- asigna PK  y empresa default
declare
co_consultagro constant number := paq_empresas.co_consultagro;

begin
  if :new.fcon_clave is null then
    :new.fcon_clave := seq_fin_concepto.nextval;
  end if;
  
  if :new.fcon_empr is null then
    :new.fcon_empr := co_consultagro;
  end if;
end fin_concepto_bi;
/
