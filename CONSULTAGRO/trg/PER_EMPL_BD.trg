CREATE OR REPLACE TRIGGER "PER_EMPL_BD"
  BEFORE DELETE OR UPDATE ON PER_EMPLEADO
  REFERENCING OLD AS O NEW AS N
  FOR EACH ROW
     WHEN (N.EMPL_SITUACION != O.EMPL_SITUACION) DECLARE
  V_CLI_TAGRO NUMBER;
  V_CLI_HILAGRO NUMBER;
  V_HOLDING NUMBER;
BEGIN

  IF UPDATING AND NVL(:N.EMPL_SITUACION, 'I') = 'I' AND :O.EMPL_EMPRESA = 1 THEN
    BEGIN
      SELECT CLI_CODIGO
        INTO V_CLI_TAGRO
        FROM FIN_CLIENTE
       WHERE CLI_COD_EMPL_EMPR_ORIG = :O.EMPL_LEGAJO
         AND CLI_CATEG IN (4, 5)
         AND CLI_EMPR = 2;
         --and cli_est_cli = 'A'; ---@TAGRO
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_CLI_TAGRO := NULL;
      when too_many_rows then
           raise_application_error(-20001,'El empleado de Hilagro: '||:O.EMPL_LEGAJO||' Esta relacionado con dos o mas clientes de transagro. !Favor verificar con RRHH de transagro!');
    END;


            UPDATE GEN_OPERADOR T
            SET  T.OPER_MAX_SESSIONS =  0
            WHERE T.OPER_CODIGO =  :O.EMPL_COD_OPERADOR
            AND T.OPER_EMPR =:O.EMPL_EMPRESA;
    -----------------tambien eliminaremos las lineas de credito en la empresa Hilagro

 /* BEGIN
    SELECT S.EMPL_COD_CLIENTE
      INTO V_CLI_HILAGRO
      FROM PER_EMPLEADO S
     WHERE EMPL_LEGAJO = :O.EMPL_LEGAJO
       AND EMPL_EMPRESA = 1;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      NULL;
    END;
    */
   ------------------------------------------------------PARA ELIMINAR LINEA DE CREDITO HILAGRO
   :N.EMPL_LIMITE_CRED := 0;

  IF  :O.EMPL_COD_CLIENTE IS NOT NULL THEN

       UPDATE FIN_CLIENTE
          SET CLI_IMP_LIM_CR = 0
        WHERE CLI_CODIGO = :O.EMPL_COD_CLIENTE
          AND CLI_EMPR = 1; ---@HIL

      UPDATE FIN_CLI_EMPRESA
         SET CLEM_IMP_LIM_CR = 0
       WHERE CLEM_CLI = :O.EMPL_COD_CLIENTE
         AND CLEM_EMPR = 1;---@HIL

       BEGIN
          SELECT FF.CLI_COD_FICHA_HOLDING
              INTO V_HOLDING
              FROM FIN_CLIENTE FF
             WHERE CLI_CODIGO =  :O.EMPL_COD_CLIENTE
               AND CLI_EMPR = 1;---@HIL

             UPDATE FIN_FICHA_HOLDING ee
              SET HOL_LIMITE_CRED = 0,
                  HOL_PREST_IMP_LIM_CR = 0
            WHERE HOL_EMPR = 1
              AND HOL_CODIGO = V_HOLDING;---@HIL

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            V_HOLDING := NULL;
        END;

   END IF;




   ------------------------------------------------------PARA ELIMINAR LINEA DE CREDITO TRANSAGRO
    --ES PARA QUITAR LIMITE DE CREDITO AL CLIENTE
    IF V_CLI_TAGRO IS NOT NULL THEN
      UPDATE FIN_CLIENTE
         SET CLI_IMP_LIM_CR = 0
       WHERE CLI_CODIGO = V_CLI_TAGRO
         AND CLI_EMPR = 2; ---@TAGRO

      -- ES PARA QUITAR LIMITE DE CREDITO PARA COMBUSTIBLE A LOS QUE SE LIQUIDA
      UPDATE FIN_CLI_EMPRESA
         SET CLEM_IMP_LIM_CR = 0
       WHERE CLEM_CLI = V_CLI_TAGRO
         AND CLEM_EMPR = 2; --@TAGRO

      UPDATE FIN_LINEA_CREDITO
         SET LINC_IMP_CRED_TO = 0,
             LINC_IMP_CRED_EF = 0,
             LINC_IMP_CRED_CO = 0,
             LINC_ESTADO      = 'I'
       WHERE LINC_PERSONA = V_CLI_TAGRO
         AND LINC_EMPR = 2; ---@TAGRO


           UPDATE FIN_PERSONA T
               SET T.PNA_CLI_CATEG = 2
             WHERE T.PNA_EMPR = 2
               AND T.PNA_CODIGO = V_CLI_TAGRO;

            UPDATE FIN_CLIENTE A
               SET A.CLI_CATEG = 2
             WHERE A.CLI_EMPR = 2
               AND A.CLI_CODIGO = V_CLI_TAGRO;

            UPDATE FIN_PROVEEDOR T
               SET T.PROV_TIPO = 6
             WHERE T.PROV_EMPR = 2
               AND T.PROV_CODIGO = V_CLI_TAGRO;




    END IF;
  ELSIF UPDATING AND NVL(:N.EMPL_SITUACION, 'I') = 'I' AND :O.EMPL_EMPRESA = 2 THEN
   UPDATE GEN_OPERADOR T
    SET  T.OPER_MAX_SESSIONS =  0
    WHERE T.OPER_CODIGO =  :O.EMPL_COD_OPERADOR
    AND T.OPER_EMPR =:O.EMPL_EMPRESA;


            UPDATE FIN_PERSONA T
               SET T.PNA_CLI_CATEG = 2
             WHERE T.PNA_EMPR = 2
               AND T.PNA_CODIGO = :O.EMPL_CODIGO_CLI;

            UPDATE FIN_CLIENTE A
               SET A.CLI_CATEG = 2
             WHERE A.CLI_EMPR = 2
               AND A.CLI_CODIGO = :O.EMPL_CODIGO_CLI;

            UPDATE FIN_PROVEEDOR T
               SET T.PROV_TIPO = 6
             WHERE T.PROV_EMPR = 2
               AND T.PROV_CODIGO = :O.EMPL_CODIGO_PROV;
    ELSIF UPDATING AND NVL(:N.EMPL_SITUACION, 'I') = 'A' AND :O.EMPL_EMPRESA = 2 THEN

            UPDATE FIN_PERSONA T
               SET T.PNA_CLI_CATEG = 3
             WHERE T.PNA_EMPR = 2
               AND T.PNA_CODIGO = :O.EMPL_CODIGO_CLI;

            UPDATE FIN_CLIENTE A
               SET A.CLI_CATEG = 3
             WHERE A.CLI_EMPR = 2
               AND A.CLI_CODIGO = :O.EMPL_CODIGO_CLI;

            UPDATE FIN_PROVEEDOR T
               SET T.PROV_TIPO = 10
             WHERE T.PROV_EMPR = 2
               AND T.PROV_CODIGO = :O.EMPL_CODIGO_PROV;

  ELSIF DELETING THEN
    DELETE FROM PER_EMPLEADO_DEPTO
     WHERE PEREMPDEP_EMPL = :O.EMPL_LEGAJO
       AND PEREMPDEP_EMPR = :O.EMPL_EMPRESA;
       
    -- 09/08/2022 14:57:25 @PabloACespedes \(^-^)/
    -- elimina registro de tabla remota
    per_empl_cagro_pck.eliminar_empleado(in_legajo => :o.empl_legajo); 
     
  elsif DELETING AND :O.EMPL_EMPRESA = 2 THEN

      Delete PER_EMPL_SOL_CAMBIO_ESTADO SOL WHERE SOL.EMPSOL_EMPR = 2
      AND SOL.EMPSOL_EMPL_LEG =   :O.EMPL_LEGAJO;

            UPDATE FIN_PERSONA T
               SET T.PNA_CLI_CATEG = 2
             WHERE T.PNA_EMPR = 2
               AND T.PNA_CODIGO = :O.EMPL_CODIGO_CLI;


            UPDATE FIN_CLIENTE A
               SET A.CLI_CATEG = 2
             WHERE A.CLI_EMPR = 2
               AND A.CLI_CODIGO = :O.EMPL_CODIGO_CLI;

            UPDATE FIN_PROVEEDOR T
               SET T.PROV_TIPO = 6
             WHERE T.PROV_EMPR = 2
               AND T.PROV_CODIGO = :O.EMPL_CODIGO_PROV;


  END IF;
END PER_EMPL_BI;
/
