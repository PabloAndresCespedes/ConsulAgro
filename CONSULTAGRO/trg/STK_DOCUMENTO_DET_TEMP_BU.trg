CREATE OR REPLACE TRIGGER "STK_DOCUMENTO_DET_TEMP_BU"
  BEFORE UPDATE ON STK_DOCUMENTO_DET_TEMP
  REFERENCING OLD AS O NEW AS N
  FOR EACH ROW
   WHEN (N.DETAT_ESTADO = 'A' AND NVL (O.DETAT_ESTADO, 'X') <> 'A') DECLARE
  -- LOCAL VARIABLES HERE
  V_NRO_DOC     NUMBER;
  V_SUC_ORIG    NUMBER;
  V_DEP_ORIG    NUMBER;
  V_SUC_DEST    NUMBER;
  V_DEP_DEST    NUMBER;
  V_FEC_EMIS    DATE;
  V_CLAVE_AUX   NUMBER;
  V_CLAVE_PADRE NUMBER;
  V_CLAVE_ANT   NUMBER;
  V_CODIGO_OPER NUMBER;
  V_OBSERVACION VARCHAR2(200);
  V_LOGIN_APROB VARCHAR2(200);

BEGIN

  V_CLAVE_AUX := NULL;

  SELECT NVL(COUNT(DOCU_CLAVE), 0)
    INTO V_CLAVE_ANT
    FROM STK_DOCUMENTO
   WHERE DOCU_CLAVE = :N.DETAT_DOCU_CLAVE
     AND DOCU_EMPR = :N.DETAT_EMPR;
  
  SELECT DOCUT_CODIGO_OPER,
         DOCUT_NRO_DOC,
         DOCUT_SUC_ORIG,
         DOCUT_DEP_ORIG,
         DOCUT_SUC_DEST,
         DOCUT_DEP_DEST,
         DOCUT_FEC_EMIS,
         DOCUT_OBS,
         DOCUT_LOGIN_APROB
    INTO V_CODIGO_OPER,
         V_NRO_DOC,
         V_SUC_ORIG,
         V_DEP_ORIG,
         V_SUC_DEST,
         V_DEP_DEST,
         V_FEC_EMIS,
         V_OBSERVACION,
         V_LOGIN_APROB
    FROM STK_DOCUMENTO_TEMP
   WHERE DOCUT_CLAVE = :N.DETAT_DOCU_CLAVE
     AND DOCUT_EMPR = :N.DETAT_EMPR;

  IF V_CODIGO_OPER = 12 THEN
  
    IF V_CLAVE_ANT = 0 THEN
      ----SI NO EXISTE
      ----------------INSERTAR CABECERA SALIDA
    
      INSERT INTO STK_DOCUMENTO
        (DOCU_CLAVE,
         DOCU_EMPR,
         DOCU_MON,
         DOCU_CODIGO_OPER,
         DOCU_NRO_DOC,
         DOCU_SUC_ORIG,
         DOCU_DEP_ORIG,
         DOCU_SUC_DEST,
         DOCU_DEP_DEST,
         DOCU_FEC_EMIS,
         DOCU_CLAVE_PADRE,
         DOCU_LOGIN,
         DOCU_FEC_GRAB,
         DOCU_SIST)
      VALUES
        (:N.DETAT_DOCU_CLAVE,
         :N.DETAT_EMPR,
         1,
         12,
         V_NRO_DOC,
         V_SUC_ORIG,
         V_DEP_ORIG,
         V_SUC_DEST,
         V_DEP_DEST,
         TRUNC(SYSDATE),
         NULL,
         --NVL(V_LOGIN_APROB, GEN_DEVUELVE_USER()),
         GEN_DEVUELVE_USER,
         SYSDATE,
         'STK');
      --------------------------------------
    
      ---DETALLE DE SALIDA
      INSERT INTO STK_DOCUMENTO_DET
        (DETA_CLAVE_DOC,
         DETA_NRO_ITEM,
         DETA_ART,
         DETA_EMPR,
         DETA_CANT,
         DETA_NRO_LOTE)
      VALUES
        (:N.DETAT_DOCU_CLAVE,
         :N.DETAT_NRO_ITEM,
         :N.DETAT_ART,
         :N.DETAT_EMPR,
         :N.DETAT_ART_CANT,
         :N.DETAT_NRO_LOTE);
    
      ----------------INSERTAR CABECERA ENTRADA
      V_CLAVE_AUX := STK_SEQ_DOCU_NEXTVAL;
    
      INSERT INTO STK_DOCUMENTO
        (DOCU_CLAVE,
         DOCU_EMPR,
         DOCU_MON,
         DOCU_CODIGO_OPER,
         DOCU_NRO_DOC,
         DOCU_SUC_ORIG,
         DOCU_DEP_ORIG,
         DOCU_SUC_DEST,
         DOCU_DEP_DEST,
         DOCU_FEC_EMIS,
         DOCU_CLAVE_PADRE,
         DOCU_LOGIN,
         DOCU_FEC_GRAB,
         DOCU_SIST)
      VALUES
        (V_CLAVE_AUX,
         :N.DETAT_EMPR,
         1,
         13,
         V_NRO_DOC,
         V_SUC_DEST,
         V_DEP_DEST,
         V_SUC_ORIG,
         V_DEP_ORIG,
         TRUNC(SYSDATE),
         :N.DETAT_DOCU_CLAVE,
         -- NVL(V_LOGIN_APROB, USER),
         GEN_DEVUELVE_USER,
         SYSDATE,
         'STK');
    
      ----DETALLE DE ENTRADA
      INSERT INTO STK_DOCUMENTO_DET
        (DETA_CLAVE_DOC,
         DETA_NRO_ITEM,
         DETA_ART,
         DETA_EMPR,
         DETA_CANT,
         DETA_NRO_LOTE)
      VALUES
        (V_CLAVE_AUX,
         :N.DETAT_NRO_ITEM,
         :N.DETAT_ART,
         :N.DETAT_EMPR,
         :N.DETAT_ART_CANT,
         :N.DETAT_NRO_LOTE);
    
    ELSE
      ---SI YA EXISTE EL DOCUMENTO
    
      V_CLAVE_AUX := STK_SEQ_DOCU_NEXTVAL;
    
      ----------------INSERTAR CABECERA SALIDA
      V_CLAVE_PADRE := V_CLAVE_AUX;
      INSERT INTO STK_DOCUMENTO
        (DOCU_CLAVE,
         DOCU_EMPR,
         DOCU_MON,
         DOCU_CODIGO_OPER,
         DOCU_NRO_DOC,
         DOCU_SUC_ORIG,
         DOCU_DEP_ORIG,
         DOCU_SUC_DEST,
         DOCU_DEP_DEST,
         DOCU_FEC_EMIS,
         DOCU_CLAVE_PADRE,
         DOCU_LOGIN,
         DOCU_FEC_GRAB,
         DOCU_SIST)
      VALUES
        (V_CLAVE_AUX,
         :N.DETAT_EMPR,
         1,
         12,
         V_NRO_DOC,
         V_SUC_ORIG,
         V_DEP_ORIG,
         V_SUC_DEST,
         V_DEP_DEST,
         TRUNC(SYSDATE),
         NULL,
         -- NVL(V_LOGIN_APROB, USER),
         GEN_DEVUELVE_USER,
         SYSDATE,
         'STK');
      --------------------------------------
    
      ---DETALLE DE SALIDA
      INSERT INTO STK_DOCUMENTO_DET
        (DETA_CLAVE_DOC,
         DETA_NRO_ITEM,
         DETA_ART,
         DETA_EMPR,
         DETA_CANT,
         DETA_NRO_LOTE)
      VALUES
        (V_CLAVE_AUX,
         :N.DETAT_NRO_ITEM,
         :N.DETAT_ART,
         :N.DETAT_EMPR,
         :N.DETAT_ART_CANT,
         :N.DETAT_NRO_LOTE);
    
      ----------------INSERTAR CABECERA ENTRADA
      V_CLAVE_AUX := STK_SEQ_DOCU_NEXTVAL;
    
      INSERT INTO STK_DOCUMENTO
        (DOCU_CLAVE,
         DOCU_EMPR,
         DOCU_MON,
         DOCU_CODIGO_OPER,
         DOCU_NRO_DOC,
         DOCU_SUC_ORIG,
         DOCU_DEP_ORIG,
         DOCU_SUC_DEST,
         DOCU_DEP_DEST,
         DOCU_FEC_EMIS,
         DOCU_CLAVE_PADRE,
         DOCU_LOGIN,
         DOCU_FEC_GRAB,
         DOCU_SIST)
      VALUES
        (V_CLAVE_AUX,
         :N.DETAT_EMPR,
         1,
         13,
         V_NRO_DOC,
         V_SUC_DEST,
         V_DEP_DEST,
         V_SUC_ORIG,
         V_DEP_ORIG,
         TRUNC(SYSDATE),
         V_CLAVE_PADRE,
         -- NVL(V_LOGIN_APROB, USER),
         GEN_DEVUELVE_USER,
         SYSDATE,
         'STK');
    
      ----DETALLE DE ENTRADA
      INSERT INTO STK_DOCUMENTO_DET
        (DETA_CLAVE_DOC,
         DETA_NRO_ITEM,
         DETA_ART,
         DETA_EMPR,
         DETA_CANT,
         DETA_NRO_LOTE)
      VALUES
        (V_CLAVE_AUX,
         :N.DETAT_NRO_ITEM,
         :N.DETAT_ART,
         :N.DETAT_EMPR,
         :N.DETAT_ART_CANT,
         :N.DETAT_NRO_LOTE);
    
    END IF; -------
  
  ELSIF V_CODIGO_OPER = 10 THEN
    NULL;
  ELSE
    -------------CUANDO DIFERENCIA DE INVENTARIO
    IF V_CLAVE_ANT = 0 THEN
      ----SI NO EXISTE
      ----------------INSERTAR CABECERA
    
      GENERAL.PL_VALIDAR_HAB_MES_STK(FECHA   => V_FEC_EMIS,
                                     EMPRESA => :N.DETAT_EMPR,
                                     USUARIO => GEN_DEVUELVE_USER);
    
      INSERT INTO STK_DOCUMENTO
        (DOCU_CLAVE,
         DOCU_EMPR,
         DOCU_MON,
         DOCU_CODIGO_OPER,
         DOCU_NRO_DOC,
         DOCU_SUC_ORIG,
         DOCU_DEP_ORIG,
         DOCU_FEC_EMIS,
         DOCU_CLAVE_PADRE,
         DOCU_LOGIN,
         DOCU_FEC_GRAB,
         DOCU_SIST,
         DOCU_OBS)
      VALUES
        (:N.DETAT_DOCU_CLAVE,
         :N.DETAT_EMPR,
         1,
         V_CODIGO_OPER,
         V_NRO_DOC,
         V_SUC_ORIG,
         V_DEP_ORIG,
         TRUNC(V_FEC_EMIS),
         NULL,
         -- NVL(V_LOGIN_APROB, USER),
         GEN_DEVUELVE_USER,
         SYSDATE,
         'STK',
         V_OBSERVACION);
      --------------------------------------
    
      ---DETALLE
      INSERT INTO STK_DOCUMENTO_DET
        (DETA_CLAVE_DOC,
         DETA_NRO_ITEM,
         DETA_ART,
         DETA_EMPR,
         DETA_CANT,
         DETA_DESTINO_USO,
         DETA_NRO_LOTE)
      VALUES
        (:N.DETAT_DOCU_CLAVE,
         :N.DETAT_NRO_ITEM,
         :N.DETAT_ART,
         :N.DETAT_EMPR,
         :N.DETAT_ART_CANT,
         :N.DETAT_DESTINO_USO,
         :N.DETAT_NRO_LOTE);
    ELSE
      ---SI YA EXISTE EL DOCUMENTO
    
      V_CLAVE_AUX := STK_SEQ_DOCU_NEXTVAL;
    
      INSERT INTO STK_DOCUMENTO
        (DOCU_CLAVE,
         DOCU_EMPR,
         DOCU_MON,
         DOCU_CODIGO_OPER,
         DOCU_NRO_DOC,
         DOCU_SUC_ORIG,
         DOCU_DEP_ORIG,
         DOCU_FEC_EMIS,
         DOCU_CLAVE_PADRE,
         DOCU_LOGIN,
         DOCU_FEC_GRAB,
         DOCU_SIST,
         DOCU_OBS)
      VALUES
        (V_CLAVE_AUX,
         :N.DETAT_EMPR,
         1,
         V_CODIGO_OPER,
         V_NRO_DOC,
         V_SUC_ORIG,
         V_DEP_ORIG,
         TRUNC(V_FEC_EMIS),
         NULL,
         -- NVL(V_LOGIN_APROB, USER),
         GEN_DEVUELVE_USER,
         SYSDATE,
         'STK',
         V_OBSERVACION);
      --------------------------------------
    
      ---DETALLE
      INSERT INTO STK_DOCUMENTO_DET
        (DETA_CLAVE_DOC,
         DETA_NRO_ITEM,
         DETA_ART,
         DETA_EMPR,
         DETA_CANT,
         DETA_DESTINO_USO,
         DETA_NRO_LOTE)
      VALUES
        (V_CLAVE_AUX,
         :N.DETAT_NRO_ITEM,
         :N.DETAT_ART,
         :N.DETAT_EMPR,
         :N.DETAT_ART_CANT,
         :N.DETAT_DESTINO_USO,
         :N.DETAT_NRO_LOTE);
    END IF;
  END IF;

EXCEPTION
  WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20000, 'ERROR AL INSERTAR TRIGGER STK_DOCUMENTO_DET_TEMP_BU' || SQLERRM);
END STK_DOCUMENTO_DET_TEMP_BU;
/
