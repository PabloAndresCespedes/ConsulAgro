CREATE OR REPLACE TRIGGER "FIN_DOCUMENTO_COMI015_BU"
  BEFORE UPDATE ON FIN_DOCUMENTO_COMI015_TEMP
  FOR EACH ROW

    WHEN (OLD.COMI005_ESTADO IS NULL) DECLARE

  SALIR EXCEPTION;
  V_IND_AUT_ESPECIAL VARCHAR2(5);
  V_PERI_FEC_HAB_INI DATE;
  V_PERI_FEC_HAB_FIN DATE;

  --P=PENDIENTE NO EJECUTAR NINGUNA ACCION HASTA QUE SE APRUEBE EL DOCUMENTO
  --T=EJECUTAR GUARDADO DEL DOCUMENTO, EL DOCUMENTO YA FUE AUTORIZADO
  --NO DATA FOUND = CREAR AUTORIZACION
BEGIN

  IF (:NEW.COMI005_ESTADO = 'T') THEN

    --TRAER EL PERIODO HABILITADO
    SELECT DECODE(CONF_IND_HAB_MES_ACT,
                  'S',
                  CONF_PER_ACT_INI,
                  CONF_PER_SGTE_INI) PERI_FEC_HAB_INI,
           DECODE(CONF_IND_HAB_MES_ACT,
                  'S',
                  CONF_PER_ACT_INI,
                  CONF_PER_SGTE_INI) PERI_FEC_HAB_FIN
      INTO V_PERI_FEC_HAB_INI, V_PERI_FEC_HAB_FIN
      FROM FIN_CONFIGURACION C
     WHERE C.CONF_EMPR = :NEW.DOC_EMPR;
  END IF;

  --=====================================
  /*si el periodo esta cerrado y se quiere aprobar el documento se busca el estado de la autorizacion
  si esta autorizado se guarda si esta pendiente no hace nada y vuelve al estado anterior, si la autorizacion esta anulada
  o no existe entonces se crea uno nuevo y se espera para insertar
  */
  IF (:NEW.COMI005_ESTADO = 'T') AND :NEW.DOC_FEC_DOC < V_PERI_FEC_HAB_INI AND
     :NEW.DOC_EST_AUT_ESPECIAL = 'T' THEN
    V_IND_AUT_ESPECIAL := 'T';
  ELSIF (:NEW.COMI005_ESTADO = 'T') AND
        :NEW.DOC_FEC_DOC < V_PERI_FEC_HAB_INI AND
        :NEW.DOC_EST_AUT_ESPECIAL = 'P' THEN
    V_IND_AUT_ESPECIAL := 'P'; --SOLICITUD PENDIENTE, HAY QUE CREAR
  ELSE
    V_IND_AUT_ESPECIAL := 'I'; --SOLICITUD INEXISTENTE, HAY QUE CREAR

  END IF;

  --SI NECESITA UNA AUTORIZACION ESPECIA Y EL PEDIDO ESTA PENDIENTE NO PERMITIR GUARDAR
  IF (:NEW.COMI005_ESTADO = 'T') AND :NEW.DOC_OBS_AUT_ESPECIAL IS NOT NULL AND
     V_IND_AUT_ESPECIAL = 'P' THEN
    :NEW.COMI005_ESTADO := :OLD.COMI005_ESTADO;
    RAISE SALIR;
  END IF;

  IF (:NEW.COMI005_ESTADO = 'T') THEN
    --  raise_application_error(-20010,:NEW.COMI005_LOGIN_EST);
    IF (:NEW.COMI005_ESTADO = 'T') AND
       :NEW.DOC_OBS_AUT_ESPECIAL IS NOT NULL AND V_IND_AUT_ESPECIAL = 'I' THEN
      :NEW.COMI005_ESTADO := :OLD.COMI005_ESTADO;
      FIN_AUT_ESP_PER_ANT.PED_INSER_DOCUMENTO(P_CLAVE_DOC    => :NEW.DOC_CLAVE,
                                              P_EMPRESA      => :NEW.DOC_EMPR,
                                              P_LOGIN        => GEN_DEVUELVE_USER,
                                              P_OBSERVACION  => :NEW.DOC_OBS_AUT_ESPECIAL,
                                              P_SISTEMA      => 'COM',
                                              P_FECHA_PEDIDO => TRUNC(SYSDATE));

      :NEW.DOC_EST_AUT_ESPECIAL := 'P'; --DESPUES DE CREAR LA SOLICITUD DEJAR EN PENDIENTE
    ELSE

      -------------------------------------------------------------------------------------------------------------------------------------------------------------
      -------------------------------------------------------------------------------------------------------------------------------------------------------------
      -------------------------------------------- PASA LOS REGISTROS A LAS TABLAS ORIGINALES DEL REGISTRO MODIFICADO ---------------------------------------------
      -------------------------------------------------------------------------------------------------------------------------------------------------------------
      -------------------------------------------------------------------------------------------------------------------------------------------------------------
      BEGIN

        ---PASA LOS DATOS FINANZAS DEL DOCUMENTO EN LA TABLA ORIGINAL

        DECLARE
          ERR_NUM NUMBER;
          ERR_MSG VARCHAR2(500);
        BEGIN
          COM_DOC_RECIBIDOS_TABLAS.INSERTAR_DOCUMENTO_FIN(V_TABLA                     => 'D',
                                                          V_DOC_CLAVE                 => :NEW.DOC_CLAVE,
                                                          V_DOC_EMPR                  => :NEW.DOC_EMPR,
                                                          V_DOC_CTA_BCO               => :NEW.DOC_CTA_BCO,
                                                          V_DOC_COND_VTA              => :NEW.DOC_COND_VTA,
                                                          V_DOC_SUC                   => :NEW.DOC_SUC,
                                                          V_DOC_DPTO                  => :NEW.DOC_DPTO,
                                                          V_DOC_TIPO_MOV              => :NEW.DOC_TIPO_MOV,
                                                          V_DOC_NRO_DOC               => :NEW.DOC_NRO_DOC,
                                                          V_DOC_TIPO_SALDO            => :NEW.DOC_TIPO_SALDO,
                                                          V_DOC_MON                   => :NEW.DOC_MON,
                                                          V_DOC_PROV                  => :NEW.DOC_PROV,
                                                          V_DOC_CLI                   => :NEW.DOC_CLI,
                                                          V_DOC_CLI_NOM               => :NEW.DOC_CLI_NOM,
                                                          V_DOC_CLI_DIR               => :NEW.DOC_CLI_DIR,
                                                          V_DOC_CLI_RUC               => :NEW.DOC_CLI_RUC,
                                                          V_DOC_CLI_TEL               => :NEW.DOC_CLI_TEL,
                                                          V_DOC_LEGAJO                => :NEW.DOC_LEGAJO,
                                                          V_DOC_BCO_CHEQUE            => :NEW.DOC_BCO_CHEQUE,
                                                          V_DOC_NRO_CHEQUE            => :NEW.DOC_NRO_CHEQUE,
                                                          V_DOC_FEC_CHEQUE            => :NEW.DOC_FEC_CHEQUE,
                                                          V_DOC_FEC_DEP_CHEQUE        => :NEW.DOC_FEC_DEP_CHEQUE,
                                                          V_DOC_EST_CHEQUE            => :NEW.DOC_EST_CHEQUE,
                                                          V_DOC_NRO_DOC_DEP           => :NEW.DOC_NRO_DOC_DEP,
                                                          V_DOC_CHEQUE_CERTIF         => :NEW.DOC_CHEQUE_CERTIF,
                                                          V_DOC_FEC_OPER              => :NEW.DOC_FEC_OPER,
                                                          V_DOC_FEC_DOC               => :NEW.DOC_FEC_DOC,
                                                          V_DOC_BRUTO_EXEN_LOC        => :NEW.DOC_BRUTO_EXEN_LOC,
                                                          V_DOC_BRUTO_EXEN_MON        => :NEW.DOC_BRUTO_EXEN_MON,
                                                          V_DOC_BRUTO_GRAV_LOC        => :NEW.DOC_BRUTO_GRAV_LOC,
                                                          V_DOC_BRUTO_GRAV_MON        => :NEW.DOC_BRUTO_GRAV_MON,
                                                          V_DOC_NETO_EXEN_LOC         => :NEW.DOC_NETO_EXEN_LOC,
                                                          V_DOC_NETO_EXEN_MON         => :NEW.DOC_NETO_EXEN_MON,
                                                          V_DOC_NETO_GRAV_LOC         => :NEW.DOC_NETO_GRAV_LOC,
                                                          V_DOC_NETO_GRAV_MON         => :NEW.DOC_NETO_GRAV_MON,
                                                          V_DOC_IVA_LOC               => :NEW.DOC_IVA_LOC,
                                                          V_DOC_IVA_MON               => :NEW.DOC_IVA_MON,
                                                          V_DOC_SALDO_INI_MON         => :NEW.DOC_SALDO_INI_MON,
                                                          V_DOC_SALDO_LOC             => :NEW.DOC_SALDO_LOC,
                                                          V_DOC_SALDO_MON             => :NEW.DOC_SALDO_MON,
                                                          V_DOC_SALDO_PER_ACT_LOC     => :NEW.DOC_SALDO_PER_ACT_LOC,
                                                          V_DOC_SALDO_PER_ACT_MON     => :NEW.DOC_SALDO_PER_ACT_MON,
                                                          V_DOC_OBS                   => :NEW.DOC_OBS,
                                                          V_DOC_BASE_IMPON_LOC        => :NEW.DOC_BASE_IMPON_LOC,
                                                          V_DOC_BASE_IMPON_MON        => :NEW.DOC_BASE_IMPON_MON,
                                                          V_DOC_DIF_CAMBIO            => :NEW.DOC_DIF_CAMBIO,
                                                          V_DOC_IND_STK               => :NEW.DOC_IND_STK,
                                                          V_DOC_CLAVE_STK             => :NEW.DOC_CLAVE_STK,
                                                          V_DOC_IND_CUOTA             => :NEW.DOC_IND_CUOTA,
                                                          V_DOC_IND_PEDIDO            => :NEW.DOC_IND_PEDIDO,
                                                          V_DOC_IND_FINANRESA         => :NEW.DOC_IND_FINANRESA,
                                                          V_DOC_IND_CONSIGNACION      => :NEW.DOC_IND_CONSIGNACION,
                                                          V_DOC_PLAN_FINAN            => :NEW.DOC_PLAN_FINAN,
                                                          V_DOC_NRO_LISTADO_LIBRO_IVA => :NEW.DOC_NRO_LISTADO_LIBRO_IVA,
                                                          V_DOC_CLAVE_PADRE           => :NEW.DOC_CLAVE_PADRE,
                                                          V_DOC_LOGIN                 => :NEW.DOC_LOGIN,
                                                          V_DOC_FEC_GRAB              => SYSDATE,
                                                          V_DOC_SIST                  => :NEW.DOC_SIST,
                                                          V_DOC_CLI_CODEUDOR          => :NEW.DOC_CLI_CODEUDOR,
                                                          V_DOC_OPERADOR              => :NEW.DOC_OPERADOR,
                                                          V_DOC_COBRADOR              => :NEW.DOC_COBRADOR,
                                                          V_DOC_CANT_PAGARE           => :NEW.DOC_CANT_PAGARE,
                                                          V_DOC_SERIE                 => :NEW.DOC_SERIE,
                                                          V_DOC_CLAVE_SCLI            => :NEW.DOC_CLAVE_SCLI,
                                                          V_DOC_NRO_PED               => :NEW.DOC_NRO_PED,
                                                          V_DOC_ORDEN_COMPRA_CLAVE    => :NEW.DOC_ORDEN_COMPRA_CLAVE,
                                                          V_DOC_PRES_CLAVE            => :NEW.DOC_PRES_CLAVE,
                                                          V_DOC_TASA                  => :NEW.DOC_TASA,
                                                          V_DOC_NRO_ORDEN_COMPRA      => :NEW.DOC_NRO_ORDEN_COMPRA,
                                                          V_DOC_DIF_CAMBIO_ACUM       => :NEW.DOC_DIF_CAMBIO_ACUM,
                                                          V_DOC_DIVISION              => :NEW.DOC_DIVISION,
                                                          V_DOC_FORMA_ENTREGA         => :NEW.DOC_FORMA_ENTREGA,
                                                          V_DOC_CODIGO_TEFE           => :NEW.DOC_CODIGO_TEFE,
                                                          V_DOC_EMPLEADO              => :NEW.DOC_EMPLEADO,
                                                          V_DOC_IVA_10_LOC            => :NEW.DOC_IVA_10_LOC,
                                                          V_DOC_IVA_10_MON            => :NEW.DOC_IVA_10_MON,
                                                          V_DOC_IVA_5_LOC             => :NEW.DOC_IVA_5_LOC,
                                                          V_DOC_IVA_5_MON             => :NEW.DOC_IVA_5_MON,
                                                          V_DOC_GRAV_10_LOC           => :NEW.DOC_GRAV_10_LOC,
                                                          V_DOC_GRAV_10_MON           => :NEW.DOC_GRAV_10_MON,
                                                          V_DOC_GRAV_5_MON            => :NEW.DOC_GRAV_5_MON,
                                                          V_DOC_GRAV_5_LOC            => :NEW.DOC_GRAV_5_LOC,
                                                          V_DOC_CLI_NRO_PED           => :NEW.DOC_CLI_NRO_PED,
                                                          V_DOC_CAMION                => :NEW.DOC_CAMION,
                                                          V_DOC_IND_ORD_CARGA         => :NEW.DOC_IND_ORD_CARGA,
                                                          V_DOC_CLAVE_RETENCION       => :NEW.DOC_CLAVE_RETENCION,
                                                          V_DOC_IND_IMAGRO            => :NEW.DOC_IND_IMAGRO,
                                                          V_DOC_TIMBRADO              => :NEW.DOC_TIMBRADO,
                                                          V_DOC_RUC_DV                => :NEW.DOC_RUC_DV,
                                                          V_DOC_DV                    => :NEW.DOC_DV,
                                                          V_DOC_IND_EXPORT            => :NEW.DOC_IND_EXPORT,
                                                          V_DOC_TIMBRADO2             => :NEW.DOC_TIMBRADO2,
                                                          V_DOC_PROV_ACOPIO           => :NEW.DOC_PROV_ACOPIO,
                                                          V_DOC_MES_VTO_TIMBRADO      => :NEW.DOC_MES_VTO_TIMBRADO,
                                                          V_DOC_ANO_VTO_TIMBRADO      => :NEW.DOC_ANO_VTO_TIMBRADO,
                                                          V_DOC_CAPITAL_MON           => :NEW.DOC_CAPITAL_MON,
                                                          V_DOC_CAPITAL_LOC           => :NEW.DOC_CAPITAL_LOC,
                                                          V_DOC_SAL_CAP_MON           => :NEW.DOC_SAL_CAP_MON,
                                                          V_DOC_SAL_CAP_LOC           => :NEW.DOC_SAL_CAP_LOC,
                                                          V_DOC_SAL_CAP_PER_ACT_MON   => :NEW.DOC_SAL_CAP_PER_ACT_MON,
                                                          V_DOC_SAL_CAP_PER_ACT_LOC   => :NEW.DOC_SAL_CAP_PER_ACT_LOC,
                                                          V_DOC_DIF_CAM_CAP           => :NEW.DOC_DIF_CAM_CAP,
                                                          V_DOC_DIF_CAM_CAP_ACUM      => :NEW.DOC_DIF_CAM_CAP_ACUM,
                                                          V_DOC_INTERES_MON           => :NEW.DOC_INTERES_MON,
                                                          V_DOC_INTERES_LOC           => :NEW.DOC_INTERES_LOC,
                                                          V_DOC_SAL_INT_MON           => :NEW.DOC_SAL_INT_MON,
                                                          V_DOC_SAL_INT_LOC           => :NEW.DOC_SAL_INT_LOC,
                                                          V_DOC_SAL_INT_PER_ACT_MON   => :NEW.DOC_SAL_INT_PER_ACT_MON,
                                                          V_DOC_SAL_INT_PER_ACT_LOC   => :NEW.DOC_SAL_INT_PER_ACT_LOC,
                                                          V_DOC_DIF_CAM_INT           => :NEW.DOC_DIF_CAM_INT,
                                                          V_DOC_DIF_CAM_INT_ACUM      => :NEW.DOC_DIF_CAM_INT_ACUM,
                                                          V_DOC_CTACO                 => :NEW.DOC_CTACO,
                                                          V_DOC_IND_EXC_CNT           => :NEW.DOC_IND_EXC_CNT,
                                                          V_DOC_IND_FACTURAR_IVA      => :NEW.DOC_IND_FACTURAR_IVA,
                                                          V_DOC_CLAVE_PEDIDO_CAST     => :NEW.DOC_CLAVE_PEDIDO_CAST,
                                                          V_DOC_PORC_VENDEDOR         => :NEW.DOC_PORC_VENDEDOR,
                                                          V_DOC_PORC_COMPRADOR        => :NEW.DOC_PORC_COMPRADOR,
                                                          V_DOC_OBS_ST                => :NEW.DOC_OBS_ST,
                                                          V_DOC_ORDEN_COMPRA          => :NEW.DOC_ORDEN_COMPRA,
                                                          V_DOC_GTOS_FIN_MON          => :NEW.DOC_GTOS_FIN_MON,
                                                          V_DOC_GTOS_FIN_LOC          => :NEW.DOC_GTOS_FIN_LOC,
                                                          V_DOC_INDIC_1PAGARE         => :NEW.DOC_INDIC_1PAGARE,
                                                          V_DOC_TIPO_VENTA            => :NEW.DOC_TIPO_VENTA,
                                                          V_DOC_NRO_NOTA              => :NEW.DOC_NRO_NOTA,
                                                          V_DOC_NRO_CONTRATO          => :NEW.DOC_NRO_CONTRATO,
                                                          V_DOC_PORC_IVA_PMO          => :NEW.DOC_PORC_IVA_PMO,
                                                          V_DOC_INTM_MON              => :NEW.DOC_INTM_MON,
                                                          V_DOC_IND_COBRO             => :NEW.DOC_IND_COBRO,
                                                          V_DOC_ACT_PRESTAMO          => :NEW.DOC_ACT_PRESTAMO,
                                                          V_DOC_NRO_CREDITO_PREST     => :NEW.DOC_NRO_CREDITO_PREST,
                                                          V_DOC_IND_CANC_PMO          => :NEW.DOC_IND_CANC_PMO,
                                                          V_DOC_INTM_LOC              => :NEW.DOC_INTM_LOC,
                                                          V_DOC_CLAVE_SOL             => :NEW.DOC_CLAVE_SOL,
                                                          V_DOC_IND_PREST_REESTRUCT   => :NEW.DOC_IND_PREST_REESTRUCT,
                                                          V_DOC_IND_IMPR_VENDEDOR     => :NEW.DOC_IND_IMPR_VENDEDOR,
                                                          V_DOC_IND_IMPR_CONYUGUE     => :NEW.DOC_IND_IMPR_CONYUGUE,
                                                          V_DOC_REC_COB               => :NEW.DOC_REC_COB,
                                                          V_DOC_SUC_VTA               => :NEW.DOC_SUC_VTA,
                                                          V_DOC_CANAL                 => :NEW.DOC_CANAL,
                                                          V_DOC_CTRL_RET_REC          => :NEW.DOC_CTRL_RET_REC,
                                                          V_DOC_SISTEMA_AUX           => :NEW.DOC_SISTEMA_AUX,
                                                          V_COMI005_ESTADO            => NULL,
                                                          V_COMI005_FEC_ACT_EST       => NULL,
                                                          V_COMI005_LOGIN_EST         => NULL,
                                                          V_DOC_CLAVE_FIN_TA          => :NEW.DOC_CLAVE_FIN_TA,
                                                          V_DOC_CLAVE_LIQ_TA          => :NEW.DOC_CLAVE_LIQ_TA,
                                                          V_DOC_ACUSE_ENVIO_C         => :NEW.DOC_ACUSE_ENVIO_C,
                                                          V_DOC_EXP_PROFORMA          => :NEW.DOC_EXP_PROFORMA,
                                                          V_DOC_CTA_BCO_FCON          => :NEW.DOC_CTA_BCO_FCON,
                                                          V_DOC_CLAVE_FAC_NCR         => :NEW.DOC_CLAVE_FAC_NCR,
                                                          V_DOC_CTA_BCO_NCR           => :NEW.DOC_CTA_BCO_NCR,
                                                          V_DOC_SNC_CLAVE             => :NEW.DOC_SNC_CLAVE,
                                                          V_DOC_SUBV                  => :NEW.DOC_SUBV,
                                                          V_DOC_CLAVE_STK_FLETE       => :NEW.DOC_CLAVE_STK_FLETE,
                                                          V_DOC_NRO_TRANS_COMBUS      => :NEW.DOC_NRO_TRANS_COMBUS,
                                                          V_DOC_NRO_DESPACHO          => :NEW.DOC_NRO_DESPACHO,
                                                          V_DOC_IND_ADE_CHQ           => :NEW.DOC_IND_ADE_CHQ,
                                                          V_TIPO_DOC_PRO_CLI          => :NEW.DOC_TIPO_DOC_CLI_PROV,
                                                          V_TIPO_FACTURA              => :NEW.DOC_TIPO_FACTURA
                                                          ------
                                                          ,I_DOC_NRO_SNC_CLI          => :NEW.DOC_NRO_SNC_CLI
                                                          ------
                                                          );
        EXCEPTION
          WHEN OTHERS THEN
            ERR_NUM := SQLCODE;
            ERR_MSG := SQLERRM;
            RAISE_APPLICATION_ERROR(-20000,
                                    'Error al INSERTAR_DOCUMENTO_FIN' ||
                                    ERR_NUM || ERR_MSG);

        END;

        ---PASA LOS DATOS DEL CONCEPTO DEL DOCUMENTO EN LA TABLA ORIGINAL

        DECLARE
          ERR_NUM NUMBER;
          ERR_MSG VARCHAR2(500);

          CURSOR DOC_CONC IS
            SELECT DCON_CLAVE_DOC,
                   DCON_ITEM,
                   DCON_CLAVE_CONCEPTO,
                   DCON_CLAVE_CTACO,
                   DCON_TIPO_SALDO,
                   DCON_EXEN_LOC,
                   DCON_EXEN_MON,
                   DCON_GRAV_LOC,
                   DCON_GRAV_MON,
                   DCON_IVA_LOC,
                   DCON_IVA_MON,
                   DCON_CLAVE_IMP,
                   DCON_IND_TIPO_IVA_COMPRA,
                   DCON_OBS,
                   DCON_PORC_IVA,
                   DCON_IND_IMAGRO,
                   DCON_CANAL,
                   DCON_DIV_CANAL,
                   DCON_CANAL_BETA,
                   DCON_CANT_COMB,
                   DCON_KM_ACTUAL,
                   DCON_OCOMD_CLAVE,
                   DCON_OCOMD_NRO_ITEM,
                   DCON_EMPR,
                   DCON_OCOMD_PORC,
                   DCON_IMP_AFECTA_COSTO,
                   DCON_CLAVE_PRESTAMO,
                   DCON_IND_VIATICO
              FROM FIN_DOC_CONCEPTO_COMI015_TEMP D
             WHERE DCON_CLAVE_DOC = :NEW.DOC_CLAVE
               AND DCON_EMPR = :NEW.DOC_EMPR;
          --esto es para revertir los adelantos que se generaron por los prestamos
          CURSOR REVERSION_PRESTAMO IS
            SELECT DISTINCT DCON_CLAVE_PRESTAMO, DCON_EMPR
              FROM FIN_DOC_CONCEPTO_COMI015_TEMP
             WHERE DCON_CLAVE_DOC = :NEW.DOC_CLAVE
               AND DCON_EMPR = :NEW.DOC_EMPR
               AND DCON_clave_prestamo is not null;

        BEGIN

          FOR DC IN DOC_CONC LOOP

            COM_DOC_RECIBIDOS_TABLAS.INSERTAR_DOC_CONCEPTO(V_TABLA                    => 'D',
                                                           V_DCON_CLAVE_DOC           => DC.DCON_CLAVE_DOC,
                                                           V_DCON_ITEM                => DC.DCON_ITEM,
                                                           V_DCON_CLAVE_CONCEPTO      => DC.DCON_CLAVE_CONCEPTO,
                                                           V_DCON_CLAVE_CTACO         => DC.DCON_CLAVE_CTACO,
                                                           V_DCON_TIPO_SALDO          => DC.DCON_TIPO_SALDO,
                                                           V_DCON_EXEN_LOC            => DC.DCON_EXEN_LOC,
                                                           V_DCON_EXEN_MON            => DC.DCON_EXEN_MON,
                                                           V_DCON_GRAV_LOC            => DC.DCON_GRAV_LOC,
                                                           V_DCON_GRAV_MON            => DC.DCON_GRAV_MON,
                                                           V_DCON_IVA_LOC             => DC.DCON_IVA_LOC,
                                                           V_DCON_IVA_MON             => DC.DCON_IVA_MON,
                                                           V_DCON_CLAVE_IMP           => DC.DCON_CLAVE_IMP,
                                                           V_DCON_IND_TIPO_IVA_COMPRA => DC.DCON_IND_TIPO_IVA_COMPRA,
                                                           V_DCON_OBS                 => DC.DCON_OBS,
                                                           V_DCON_PORC_IVA            => DC.DCON_PORC_IVA,
                                                           V_DCON_IND_IMAGRO          => DC.DCON_IND_IMAGRO,
                                                           V_DCON_CANAL               => DC.DCON_CANAL,
                                                           V_DCON_DIV_CANAL           => DC.DCON_DIV_CANAL,
                                                           V_DCON_CANT_COMB           => DC.DCON_CANT_COMB,
                                                           V_DCON_KM_ACTUAL           => DC.DCON_KM_ACTUAL,
                                                           V_DCON_OCOMD_CLAVE         => DC.DCON_OCOMD_CLAVE,
                                                           V_DCON_OCOMD_NRO_ITEM      => DC.DCON_OCOMD_NRO_ITEM,
                                                           V_DCON_EMPR                => DC.DCON_EMPR,
                                                           V_DCON_OCOMD_PORC          => DC.DCON_OCOMD_PORC,
                                                           V_DCON_IMP_AFECTA_COSTO    => DC.DCON_IMP_AFECTA_COSTO,
                                                           V_DCON_CLAVE_PRESTAMO      => DC.DCON_CLAVE_PRESTAMO,
                                                           V_DCON_CANAL_BETA          => DC.DCON_CANAL_BETA,
                                                           V_DCON_IND_VIATICO         => DC.DCON_IND_VIATICO);
          END LOOP;

          FOR R IN REVERSION_PRESTAMO LOOP
            FINI257.PP_REVERTIR_FACT_PRESTAMO(P_EMPRESA        => R.DCON_EMPR,
                                              P_CLAVE_PRESTAMO => R.DCON_CLAVE_PRESTAMO,
                                              P_FECHA_DOC      => :NEW.DOC_FEC_DOC,
                                              P_FECHA_OPER     => :NEW.DOC_FEC_OPER);
          END LOOP;

        EXCEPTION
          WHEN OTHERS THEN
            ERR_NUM := SQLCODE;
            ERR_MSG := SQLERRM;
            RAISE_APPLICATION_ERROR(-20000,
                                    'Error al INSERTAR_DOC_CONCEPTO' ||
                                    ERR_NUM || ERR_MSG);
        END;

        ---PASA LOS DATOS DE LAS CUOTAS A LA TABLA ORIGINAL

        DECLARE
          ERR_NUM NUMBER;
          ERR_MSG VARCHAR2(500);
          CURSOR DOC_CUO IS
            SELECT CUO_CLAVE_DOC,
                   CUO_FEC_VTO,
                   CUO_IMP_LOC,
                   CUO_IMP_MON,
                   CUO_SALDO_LOC,
                   CUO_SALDO_MON,
                   CUO_SALDO_PER_ACT_LOC,
                   CUO_SALDO_PER_ACT_MON,
                   CUO_DIF_CAMBIO,
                   CUO_NRO_PAGARE,
                   CUO_SALDO_INT_MON,
                   CUO_SALDO_INT_LOC,
                   CUO_SALDO_INT_PER_ACT_MON,
                   CUO_SALDO_INT_PER_ACT_LOC,
                   CUO_CLAVE_RETENCION,
                   CUO_CAP_MON,
                   CUO_CAP_LOC,
                   CUO_SAL_CAP_MON,
                   CUO_SAL_CAP_LOC,
                   CUO_SAL_CAP_PER_ACT_MON,
                   CUO_SAL_CAP_PER_ACT_LOC,
                   CUO_DIF_CAM_CAP,
                   CUO_INT_MON,
                   CUO_INT_LOC,
                   CUO_SAL_INT_MON,
                   CUO_SAL_INT_LOC,
                   CUO_SAL_INT_PER_ACT_MON,
                   CUO_SAL_INT_PER_ACT_LOC,
                   CUO_DIF_CAM_INT,
                   CUO_LUGAR_ORIGEN_REPLICA,
                   CUO_ULT_TASA_DIF_CAM,
                   CUO_PEN_TASA_DIF_CAM,
                   CUO_EMPR
              FROM FIN_CUOTA_COMI015_TEMP
             WHERE CUO_CLAVE_DOC = :NEW.DOC_CLAVE
               AND CUO_EMPR = :NEW.DOC_EMPR;

        BEGIN
          FOR CU IN DOC_CUO LOOP
            COM_DOC_RECIBIDOS_TABLAS.INSERTAR_CUOTAS(V_TABLA                     => 'D',
                                                     V_CUO_CLAVE_DOC             => CU.CUO_CLAVE_DOC,
                                                     V_CUO_FEC_VTO               => CU.CUO_FEC_VTO,
                                                     V_CUO_IMP_LOC               => CU.CUO_IMP_LOC,
                                                     V_CUO_IMP_MON               => CU.CUO_IMP_MON,
                                                     V_CUO_SALDO_LOC             => CU.CUO_SALDO_LOC,
                                                     V_CUO_SALDO_MON             => CU.CUO_SALDO_MON,
                                                     V_CUO_SALDO_PER_ACT_LOC     => CU.CUO_SALDO_PER_ACT_LOC,
                                                     V_CUO_SALDO_PER_ACT_MON     => CU.CUO_SALDO_PER_ACT_MON,
                                                     V_CUO_DIF_CAMBIO            => CU.CUO_DIF_CAMBIO,
                                                     V_CUO_NRO_PAGARE            => CU.CUO_NRO_PAGARE,
                                                     V_CUO_SALDO_INT_MON         => CU.CUO_SALDO_INT_MON,
                                                     V_CUO_SALDO_INT_LOC         => CU.CUO_SALDO_INT_LOC,
                                                     V_CUO_SALDO_INT_PER_ACT_MON => CU.CUO_SALDO_INT_PER_ACT_MON,
                                                     V_CUO_SALDO_INT_PER_ACT_LOC => CU.CUO_SALDO_INT_PER_ACT_LOC,
                                                     V_CUO_CLAVE_RETENCION       => CU.CUO_CLAVE_RETENCION,
                                                     V_CUO_CAP_MON               => CU.CUO_CAP_MON,
                                                     V_CUO_CAP_LOC               => CU.CUO_CAP_LOC,
                                                     V_CUO_SAL_CAP_MON           => CU.CUO_SAL_CAP_MON,
                                                     V_CUO_SAL_CAP_LOC           => CU.CUO_SAL_CAP_LOC,
                                                     V_CUO_SAL_CAP_PER_ACT_MON   => CU.CUO_SAL_CAP_PER_ACT_MON,
                                                     V_CUO_SAL_CAP_PER_ACT_LOC   => CU.CUO_SAL_CAP_PER_ACT_LOC,
                                                     V_CUO_DIF_CAM_CAP           => CU.CUO_DIF_CAM_CAP,
                                                     V_CUO_INT_MON               => CU.CUO_INT_MON,
                                                     V_CUO_INT_LOC               => CU.CUO_INT_LOC,
                                                     V_CUO_SAL_INT_MON           => CU.CUO_SAL_INT_MON,
                                                     V_CUO_SAL_INT_LOC           => CU.CUO_SAL_INT_LOC,
                                                     V_CUO_SAL_INT_PER_ACT_MON   => CU.CUO_SAL_INT_PER_ACT_MON,
                                                     V_CUO_SAL_INT_PER_ACT_LOC   => CU.CUO_SAL_INT_PER_ACT_LOC,
                                                     V_CUO_DIF_CAM_INT           => CU.CUO_DIF_CAM_INT,
                                                     V_CUO_LUGAR_ORIGEN_REPLICA  => CU.CUO_LUGAR_ORIGEN_REPLICA,
                                                     V_CUO_ULT_TASA_DIF_CAM      => CU.CUO_ULT_TASA_DIF_CAM,
                                                     V_CUO_PEN_TASA_DIF_CAM      => CU.CUO_PEN_TASA_DIF_CAM,
                                                     V_CUO_EMPR                  => CU.CUO_EMPR);

          END LOOP;
        EXCEPTION
          WHEN OTHERS THEN
            ERR_NUM := SQLCODE;
            ERR_MSG := SQLERRM;
            RAISE_APPLICATION_ERROR(-20000,
                                    'Error al INSERTAR_CUOTAS' || ERR_NUM ||
                                    ERR_MSG);
        END;

        ---PASA LOS DATOS DE LOS DOCUMEMTOS DE STOCK
        DECLARE
          ERR_NUM NUMBER;
          ERR_MSG VARCHAR2(500);
          CURSOR DOC_STK IS
            SELECT DOCU_CLAVE,
                   DOCU_EMPR,
                   DOCU_CODIGO_OPER,
                   DOCU_NRO_DOC,
                   DOCU_SUC_ORIG,
                   DOCU_DEP_ORIG,
                   DOCU_SUC_DEST,
                   DOCU_DEP_DEST,
                   DOCU_MON,
                   DOCU_PROV,
                   DOCU_CLI,
                   DOCU_CLI_NOM,
                   DOCU_CLI_RUC,
                   DOCU_LEGAJO,
                   DOCU_DPTO,
                   DOCU_SECCION,
                   DOCU_FEC_EMIS,
                   DOCU_TIPO_MOV,
                   DOCU_GRAV_NETO_LOC,
                   DOCU_GRAV_NETO_MON,
                   DOCU_EXEN_NETO_LOC,
                   DOCU_EXEN_NETO_MON,
                   DOCU_GRAV_BRUTO_LOC,
                   DOCU_GRAV_BRUTO_MON,
                   DOCU_EXEN_BRUTO_LOC,
                   DOCU_EXEN_BRUTO_MON,
                   DOCU_IVA_LOC,
                   DOCU_IVA_MON,
                   DOCU_TASA_US,
                   DOCU_IND_CUOTA,
                   DOCU_IND_CONSIGNACION,
                   DOCU_OBS,
                   DOCU_CLAVE_PADRE,
                   DOCU_NRO_LISTADO_AUD,
                   DOCU_LOGIN,
                   DOCU_FEC_GRAB,
                   DOCU_SIST,
                   DOCU_OPERADOR,
                   DOCU_NRO_PED,
                   DOCU_PORC_UTIL,
                   DOCU_SERIE,
                   DOCU_FORM,
                   DOCU_RCONDUCT_COD,
                   DOCU_RTRANSP_COD,
                   DOCU_RMAR_VEH_COD,
                   DOCU_RCONDUCT_NOMB,
                   DOCU_RCONDUCT_CI,
                   DOCU_RCONDUCT_DIR,
                   DOCU_PRE_COMPRA,
                   DOCU_CLAVE_COMPRA,
                   DOCU_DESTINO_USO,
                   DOCU_CAMION,
                   DOCU_IND_ORD_CARGA,
                   DOCU_AUTORIZADO,
                   DOCU_DEST_REPAR,
                   DOCU_EST_TRASLADO,
                   DOCU_OCARGA_LONDON,
                   DOCU_MOT_NCR,
                   DOCU_TIMBRADO,
                   DOCU_TIPO_TRASLADO,
                   DOCU_CLAVE_FIN,
                   DOCU_FEC_FIN_TRANSLADO,
                   DOCU_KM_RECORRIDO,
                   DOCU_VEH_MARCA,
                   DOCU_VEH_CHAPA,
                   DOCU_CLAVE_STOPES,
                   DOCU_FEC_OPER,
                   DOCU_NRO_DESPACHO,
                   DOCU_CANAL,
                   DOCU_CLI_HOLDING,
                   DOCU_DEVOL_REPROC,
                   DOCU_CARRETA_CHAPA,
                   DOCU_PROF_PROV,
                   DOCU_FINCLAVE_FLETE,
                   DOCU_CLAVE_FAC_REM,
                   DOCU_TIPO_DOC_CLI_PROV,
                   DOCU_TIPO_FACTURA
              FROM STK_DOCUMENTO_COMI015_TEMP
             WHERE DOCU_CLAVE = :NEW.DOC_CLAVE_STK
               AND DOCU_EMPR = :NEW.DOC_EMPR;
        BEGIN

          FOR ST IN DOC_STK LOOP
            COM_DOC_RECIBIDOS_TABLAS.INSERTAR_DOCUMENTO_STK(V_TABLA                  => 'D',
                                                            V_DOCU_CLAVE             => ST.DOCU_CLAVE,
                                                            V_DOCU_EMPR              => ST.DOCU_EMPR,
                                                            V_DOCU_CODIGO_OPER       => ST.DOCU_CODIGO_OPER,
                                                            V_DOCU_NRO_DOC           => ST.DOCU_NRO_DOC,
                                                            V_DOCU_SUC_ORIG          => ST.DOCU_SUC_ORIG,
                                                            V_DOCU_DEP_ORIG          => ST.DOCU_DEP_ORIG,
                                                            V_DOCU_SUC_DEST          => ST.DOCU_SUC_DEST,
                                                            V_DOCU_DEP_DEST          => ST.DOCU_DEP_DEST,
                                                            V_DOCU_MON               => ST.DOCU_MON,
                                                            V_DOCU_PROV              => ST.DOCU_PROV,
                                                            V_DOCU_CLI               => ST.DOCU_CLI,
                                                            V_DOCU_CLI_NOM           => ST.DOCU_CLI_NOM,
                                                            V_DOCU_CLI_RUC           => ST.DOCU_CLI_RUC,
                                                            V_DOCU_LEGAJO            => ST.DOCU_LEGAJO,
                                                            V_DOCU_DPTO              => ST.DOCU_DPTO,
                                                            V_DOCU_SECCION           => ST.DOCU_SECCION,
                                                            V_DOCU_FEC_EMIS          => ST.DOCU_FEC_EMIS,
                                                            V_DOCU_TIPO_MOV          => ST.DOCU_TIPO_MOV,
                                                            V_DOCU_GRAV_NETO_LOC     => ST.DOCU_GRAV_NETO_LOC,
                                                            V_DOCU_GRAV_NETO_MON     => ST.DOCU_GRAV_NETO_MON,
                                                            V_DOCU_EXEN_NETO_LOC     => ST.DOCU_EXEN_NETO_LOC,
                                                            V_DOCU_EXEN_NETO_MON     => ST.DOCU_EXEN_NETO_MON,
                                                            V_DOCU_GRAV_BRUTO_LOC    => ST.DOCU_GRAV_BRUTO_LOC,
                                                            V_DOCU_GRAV_BRUTO_MON    => ST.DOCU_GRAV_BRUTO_MON,
                                                            V_DOCU_EXEN_BRUTO_LOC    => ST.DOCU_EXEN_BRUTO_LOC,
                                                            V_DOCU_EXEN_BRUTO_MON    => ST.DOCU_EXEN_BRUTO_MON,
                                                            V_DOCU_IVA_LOC           => ST.DOCU_IVA_LOC,
                                                            V_DOCU_IVA_MON           => ST.DOCU_IVA_MON,
                                                            V_DOCU_TASA_US           => ST.DOCU_TASA_US,
                                                            V_DOCU_IND_CUOTA         => ST.DOCU_IND_CUOTA,
                                                            V_DOCU_IND_CONSIGNACION  => ST.DOCU_IND_CONSIGNACION,
                                                            V_DOCU_OBS               => ST.DOCU_OBS,
                                                            V_DOCU_CLAVE_PADRE       => ST.DOCU_CLAVE_PADRE,
                                                            V_DOCU_NRO_LISTADO_AUD   => ST.DOCU_NRO_LISTADO_AUD,
                                                            V_DOCU_LOGIN             => ST.DOCU_LOGIN,
                                                            V_DOCU_FEC_GRAB          => SYSDATE,
                                                            V_DOCU_SIST              => ST.DOCU_SIST,
                                                            V_DOCU_OPERADOR          => ST.DOCU_OPERADOR,
                                                            V_DOCU_NRO_PED           => ST.DOCU_NRO_PED,
                                                            V_DOCU_PORC_UTIL         => ST.DOCU_PORC_UTIL,
                                                            V_DOCU_SERIE             => ST.DOCU_SERIE,
                                                            V_DOCU_FORM              => ST.DOCU_FORM,
                                                            V_DOCU_RCONDUCT_COD      => ST.DOCU_RCONDUCT_COD,
                                                            V_DOCU_RTRANSP_COD       => ST.DOCU_RTRANSP_COD,
                                                            V_DOCU_RMAR_VEH_COD      => ST.DOCU_RMAR_VEH_COD,
                                                            V_DOCU_RCONDUCT_NOMB     => ST.DOCU_RCONDUCT_NOMB,
                                                            V_DOCU_RCONDUCT_CI       => ST.DOCU_RCONDUCT_CI,
                                                            V_DOCU_RCONDUCT_DIR      => ST.DOCU_RCONDUCT_DIR,
                                                            V_DOCU_PRE_COMPRA        => ST.DOCU_PRE_COMPRA,
                                                            V_DOCU_CLAVE_COMPRA      => ST.DOCU_CLAVE_COMPRA,
                                                            V_DOCU_DESTINO_USO       => ST.DOCU_DESTINO_USO,
                                                            V_DOCU_CAMION            => ST.DOCU_CAMION,
                                                            V_DOCU_IND_ORD_CARGA     => ST.DOCU_IND_ORD_CARGA,
                                                            V_DOCU_AUTORIZADO        => ST.DOCU_AUTORIZADO,
                                                            V_DOCU_DEST_REPAR        => ST.DOCU_DEST_REPAR,
                                                            V_DOCU_EST_TRASLADO      => ST.DOCU_EST_TRASLADO,
                                                            V_DOCU_OCARGA_LONDON     => ST.DOCU_OCARGA_LONDON,
                                                            V_DOCU_MOT_NCR           => ST.DOCU_MOT_NCR,
                                                            V_DOCU_TIMBRADO          => ST.DOCU_TIMBRADO,
                                                            V_DOCU_TIPO_TRASLADO     => ST.DOCU_TIPO_TRASLADO,
                                                            V_DOCU_CLAVE_FIN         => ST.DOCU_CLAVE_FIN,
                                                            V_DOCU_FEC_FIN_TRANSLADO => ST.DOCU_FEC_FIN_TRANSLADO,
                                                            V_DOCU_KM_RECORRIDO      => ST.DOCU_KM_RECORRIDO,
                                                            V_DOCU_VEH_MARCA         => ST.DOCU_VEH_MARCA,
                                                            V_DOCU_VEH_CHAPA         => ST.DOCU_VEH_CHAPA,
                                                            V_DOCU_CLAVE_STOPES      => ST.DOCU_CLAVE_STOPES,
                                                            V_DOCU_FEC_OPER          => ST.DOCU_FEC_OPER,
                                                            V_DOCU_NRO_DESPACHO      => ST.DOCU_NRO_DESPACHO,
                                                            V_DOCU_CANAL             => ST.DOCU_CANAL,
                                                            V_DOCU_CLI_HOLDING       => ST.DOCU_CLI_HOLDING,
                                                            V_DOCU_DEVOL_REPROC      => ST.DOCU_DEVOL_REPROC,
                                                            V_DOCU_CARRETA_CHAPA     => ST.DOCU_CARRETA_CHAPA,
                                                            V_DOCU_PROF_PROV         => ST.DOCU_PROF_PROV,
                                                            V_DOCU_FINCLAVE_FLETE    => ST.DOCU_FINCLAVE_FLETE,
                                                            V_DOCU_CLAVE_FAC_REM     => ST.DOCU_CLAVE_FAC_REM,
                                                            V_TIPO_DOC_PRO_CLI       => ST.DOCU_TIPO_DOC_CLI_PROV,
                                                            V_TIPO_FACTURA           => ST.DOCU_TIPO_FACTURA);

          END LOOP;
        EXCEPTION
          WHEN OTHERS THEN
            ERR_NUM := SQLCODE;
            ERR_MSG := SQLERRM;
            RAISE_APPLICATION_ERROR(-20000,
                                    'Error al INSERTAR_DOCUMENTO_STK' ||
                                    ERR_NUM || ERR_MSG);
        END;

        ---PASA LOS DATOS DE LOS DETALLES DE DOC_STK
        DECLARE
          ERR_NUM NUMBER;
          ERR_MSG VARCHAR2(500);
          CURSOR DOC_STK_DET IS
            SELECT DETA_CLAVE_DOC,
                   DETA_NRO_ITEM,
                   DETA_ART,
                   DETA_EMPR,
                   DETA_NRO_REM,
                   DETA_CANT,
                   DETA_IMP_NETO_LOC,
                   DETA_IMP_NETO_MON,
                   DETA_IMPU,
                   DETA_IVA_LOC,
                   DETA_IVA_MON,
                   DETA_PORC_DTO,
                   DETA_IMP_BRUTO_LOC,
                   DETA_IMP_BRUTO_MON,
                   DETA_CANT_REM,
                   DETA_CANT_FACT,
                   DETA_PERIODO,
                   DETA_DESGLOZAR,
                   DETA_ART_ENT,
                   DETA_CLAVE_ENT_PROD,
                   DETA_NRO_FORMULA,
                   DETA_FEC_PROC_COSTO,
                   DETA_PORC_NO_DESG,
                   DETA_DESTINO_USO,
                   DETA_OCOMD_CLAVE,
                   DETA_OCOMD_NRO_ITEM,
                   DETA_NRO_LOTE,
                   DETA_CANT_SALKG,
                   DETA_CANT_ENTKG,
                   DETA_MOT_NCR,
                   DETA_CLAVE_CONCEPTO,
                   DETA_CLAVE_CANAL,
                   DETA_PERIODO_OPER
              FROM STK_DOCUMENTO_DET_COMI015_TEMP
             WHERE DETA_CLAVE_DOC = :NEW.DOC_CLAVE_STK
               AND DETA_EMPR = :NEW.DOC_EMPR;

        BEGIN
          FOR ST IN DOC_STK_DET LOOP

            COM_DOC_RECIBIDOS_TABLAS.INSERTAR_DOC_DET_STK(V_TABLA               => 'D',
                                                          V_DETA_CLAVE_DOC      => ST.DETA_CLAVE_DOC,
                                                          V_DETA_NRO_ITEM       => ST.DETA_NRO_ITEM,
                                                          V_DETA_ART            => ST.DETA_ART,
                                                          V_DETA_EMPR           => ST.DETA_EMPR,
                                                          V_DETA_NRO_REM        => ST.DETA_NRO_REM,
                                                          V_DETA_CANT           => ST.DETA_CANT,
                                                          V_DETA_IMP_NETO_LOC   => ST.DETA_IMP_NETO_LOC,
                                                          V_DETA_IMP_NETO_MON   => ST.DETA_IMP_NETO_MON,
                                                          V_DETA_IMPU           => ST.DETA_IMPU,
                                                          V_DETA_IVA_LOC        => ST.DETA_IVA_LOC,
                                                          V_DETA_IVA_MON        => ST.DETA_IVA_MON,
                                                          V_DETA_PORC_DTO       => ST.DETA_PORC_DTO,
                                                          V_DETA_IMP_BRUTO_LOC  => ST.DETA_IMP_BRUTO_LOC,
                                                          V_DETA_IMP_BRUTO_MON  => ST.DETA_IMP_BRUTO_MON,
                                                          V_DETA_CANT_REM       => ST.DETA_CANT_REM,
                                                          V_DETA_CANT_FACT      => ST.DETA_CANT_FACT,
                                                          V_DETA_PERIODO        => ST.DETA_PERIODO,
                                                          V_DETA_DESGLOZAR      => ST.DETA_DESGLOZAR,
                                                          V_DETA_ART_ENT        => ST.DETA_ART_ENT,
                                                          V_DETA_CLAVE_ENT_PROD => ST.DETA_CLAVE_ENT_PROD,
                                                          V_DETA_NRO_FORMULA    => ST.DETA_NRO_FORMULA,
                                                          V_DETA_FEC_PROC_COSTO => ST.DETA_FEC_PROC_COSTO,
                                                          V_DETA_PORC_NO_DESG   => ST.DETA_PORC_NO_DESG,
                                                          V_DETA_DESTINO_USO    => ST.DETA_DESTINO_USO,
                                                          V_DETA_OCOMD_CLAVE    => ST.DETA_OCOMD_CLAVE,
                                                          V_DETA_OCOMD_NRO_ITEM => ST.DETA_OCOMD_NRO_ITEM,
                                                          V_DETA_NRO_LOTE       => ST.DETA_NRO_LOTE,
                                                          V_DETA_CANT_SALKG     => ST.DETA_CANT_SALKG,
                                                          V_DETA_CANT_ENTKG     => ST.DETA_CANT_ENTKG,
                                                          V_DETA_MOT_NCR        => ST.DETA_MOT_NCR,
                                                          V_DETA_CLAVE_CONCEPTO => ST.DETA_CLAVE_CONCEPTO,
                                                          V_DETA_CLAVE_CANAL    => ST.DETA_CLAVE_CANAL,
                                                          V_DETA_PERIODO_OPER   => ST.DETA_PERIODO_OPER);

          END LOOP;
        EXCEPTION
          WHEN OTHERS THEN
            ERR_NUM := SQLCODE;
            ERR_MSG := SQLERRM;
            RAISE_APPLICATION_ERROR(-20000,
                                    '1-Error al INSERTAR_DOC_DET_STK' ||
                                    ERR_NUM || ERR_MSG);
        END;

        ---PASA LOS DATOS DE LOS DETALLES DE FAC_DOCUMENTO_DET

        DECLARE
          ERR_NUM NUMBER;
          ERR_MSG VARCHAR2(500);
          CURSOR FAC_DOC_DET IS
            SELECT DET_CLAVE_DOC,
                   DET_NRO_ITEM,
                   DET_ART,
                   DET_NRO_REMIS,
                   DET_CANT,
                   DET_BRUTO_LOC,
                   DET_BRUTO_MON,
                   DET_COD_IVA,
                   DET_IVA_LOC,
                   DET_IVA_MON,
                   DET_PORC_DTO,
                   DET_NETO_LOC,
                   DET_NETO_MON,
                   DET_TIPO_COMISION,
                   DET_DESC_LARGA,
                   DET_CLAVE_OT,
                   DET_CLAVE_PED,
                   DET_DET_MARC,
                   DET_EMPR,
                   DET_PLAN_CLAVE,
                   DET_PLAN_ITEM
              FROM FAC_DOCUMENTO_DET_COMI015_TEMP
             WHERE DET_CLAVE_DOC = :NEW.DOC_CLAVE
               AND DET_EMPR = :NEW.DOC_EMPR;

        BEGIN
          FOR FDET IN FAC_DOC_DET LOOP
            COM_DOC_RECIBIDOS_TABLAS.INSERTAR_FAC_DOC_DET(V_TABLA             => 'D',
                                                          V_DET_CLAVE_DOC     => FDET.DET_CLAVE_DOC,
                                                          V_DET_NRO_ITEM      => FDET.DET_NRO_ITEM,
                                                          V_DET_ART           => FDET.DET_ART,
                                                          V_DET_NRO_REMIS     => FDET.DET_NRO_REMIS,
                                                          V_DET_CANT          => FDET.DET_CANT,
                                                          V_DET_BRUTO_LOC     => FDET.DET_BRUTO_LOC,
                                                          V_DET_BRUTO_MON     => FDET.DET_BRUTO_MON,
                                                          V_DET_COD_IVA       => FDET.DET_COD_IVA,
                                                          V_DET_IVA_LOC       => FDET.DET_IVA_LOC,
                                                          V_DET_IVA_MON       => FDET.DET_IVA_MON,
                                                          V_DET_PORC_DTO      => FDET.DET_PORC_DTO,
                                                          V_DET_NETO_LOC      => FDET.DET_NETO_LOC,
                                                          V_DET_NETO_MON      => FDET.DET_NETO_MON,
                                                          V_DET_TIPO_COMISION => FDET.DET_TIPO_COMISION,
                                                          V_DET_DESC_LARGA    => FDET.DET_DESC_LARGA,
                                                          V_DET_CLAVE_OT      => FDET.DET_CLAVE_OT,
                                                          V_DET_CLAVE_PED     => FDET.DET_CLAVE_PED,
                                                          V_DET_DET_MARC      => FDET.DET_DET_MARC,
                                                          V_DET_EMPR          => FDET.DET_EMPR,
                                                          V_DET_PLAN_CLAVE    => FDET.DET_PLAN_CLAVE,
                                                          V_DET_PLAN_ITEM     => FDET.DET_PLAN_ITEM);

          END LOOP;
        EXCEPTION
          WHEN OTHERS THEN
            ERR_NUM := SQLCODE;
            ERR_MSG := SQLERRM;
            RAISE_APPLICATION_ERROR(-20000,
                                    'Error al INSERTAR_FAC_DOC_DET' ||
                                    ERR_NUM || ERR_MSG);
        END;

        --INSERTA EN CASO QUE HAYA DOCUMENTOS EN PER_DOCUMENTOS

        DECLARE
          V_PER_DOC              NUMBER;
          V_PDOC_CLAVE           NUMBER;
          V_PDOC_QUINCENA        NUMBER;
          V_PDOC_EMPLEADO        NUMBER;
          V_PDOC_FEC             DATE;
          V_PDOC_NRO_DOC         NUMBER;
          V_PDOC_FEC_GRAB        DATE;
          V_PDOC_LOGIN           VARCHAR2(50);
          V_PDOC_FORM            VARCHAR2(50);
          V_PDOC_PERIODO         NUMBER;
          V_PDOC_CLAVE_FIN       NUMBER;
          V_PDOC_MON             NUMBER;
          V_PDOC_DEPARTAMENTO    NUMBER;
          V_PDOC_EMPR            NUMBER;
          V_PDDET_CLAVE_CONCEPTO NUMBER;
          V_PDDET_IMP            NUMBER;
          V_PDDET_IMP_LOC        NUMBER;
          V_PDDET_EMPR           NUMBER;
          V_MARC                 CHAR(50);
          V_ULT_COD              NUMBER;
        BEGIN

          BEGIN
            SELECT T.PDOC_CLAVE
              INTO V_PER_DOC
              FROM PER_DOCUMENTO_ADEL_TEMP T
             WHERE T.PDOC_CLAVE_FIN = :NEW.DOC_CLAVE
               AND T.PDOC_EMPR = :NEW.DOC_EMPR;
            V_MARC := 'S';
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              V_MARC := 'N';
          END;

          IF V_MARC = 'S' THEN

            SELECT NVL(MAX(PDOC_CLAVE), 0) + 1 DOC_MAX
              INTO V_ULT_COD
              FROM PER_DOCUMENTO
             WHERE PDOC_EMPR = :NEW.DOC_EMPR;

            SELECT T.PDOC_CLAVE,
                   T.PDOC_QUINCENA,
                   T.PDOC_EMPLEADO,
                   T.PDOC_FEC,
                   T.PDOC_NRO_DOC,
                   T.PDOC_FEC_GRAB,
                   T.PDOC_LOGIN,
                   T.PDOC_FORM,
                   T.PDOC_PERIODO,
                   T.PDOC_CLAVE_FIN,
                   T.PDOC_MON,
                   T.PDOC_DEPARTAMENTO,
                   T.PDOC_EMPR
              INTO V_PDOC_CLAVE,
                   V_PDOC_QUINCENA,
                   V_PDOC_EMPLEADO,
                   V_PDOC_FEC,
                   V_PDOC_NRO_DOC,
                   V_PDOC_FEC_GRAB,
                   V_PDOC_LOGIN,
                   V_PDOC_FORM,
                   V_PDOC_PERIODO,
                   V_PDOC_CLAVE_FIN,
                   V_PDOC_MON,
                   V_PDOC_DEPARTAMENTO,
                   V_PDOC_EMPR
              FROM PER_DOCUMENTO_ADEL_TEMP T
             WHERE T.PDOC_CLAVE = V_PER_DOC
               AND T.PDOC_EMPR = :NEW.DOC_EMPR;

            INSERT INTO PER_DOCUMENTO
              (PDOC_CLAVE,
               PDOC_QUINCENA,
               PDOC_EMPLEADO,
               PDOC_FEC,
               PDOC_NRO_DOC,
               PDOC_FEC_GRAB,
               PDOC_LOGIN,
               PDOC_FORM,
               PDOC_PERIODO,
               PDOC_CLAVE_FIN,
               PDOC_MON,
               PDOC_DEPARTAMENTO,
               PDOC_EMPR)
            VALUES
              (V_ULT_COD,
               V_PDOC_QUINCENA,
               V_PDOC_EMPLEADO,
               V_PDOC_FEC,
               V_PDOC_NRO_DOC,
               V_PDOC_FEC_GRAB,
               V_PDOC_LOGIN,
               V_PDOC_FORM,
               V_PDOC_PERIODO,
               V_PDOC_CLAVE_FIN,
               V_PDOC_MON,
               V_PDOC_DEPARTAMENTO,
               V_PDOC_EMPR);

            SELECT PED.PDDET_CLAVE_CONCEPTO,
                   PED.PDDET_IMP,
                   PED.PDDET_IMP_LOC,
                   PED.PDDET_EMPR
              INTO V_PDDET_CLAVE_CONCEPTO,
                   V_PDDET_IMP,
                   V_PDDET_IMP_LOC,
                   V_PDDET_EMPR
              FROM PER_DOCUMENTO_DET_ADEL_TEMP PED
             WHERE PED.PDDET_CLAVE_DOC = V_PER_DOC
               AND PED.PDDET_ITEM = 1
               AND PED.PDDET_EMPR = :NEW.DOC_EMPR;

            INSERT INTO PER_DOCUMENTO_DET
              (PDDET_CLAVE_DOC,
               PDDET_ITEM,
               PDDET_CLAVE_CONCEPTO,
               PDDET_IMP,
               PDDET_IMP_LOC,
               PDDET_EMPR)
            VALUES
              (V_ULT_COD,
               1,
               V_PDDET_CLAVE_CONCEPTO,
               V_PDDET_IMP,
               V_PDDET_IMP_LOC,
               V_PDDET_EMPR);
          END IF;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            NULL;
        END;

        ---PASA LOS DATOS DE ACO_FIJACION

        DECLARE
          ERR_NUM NUMBER;
          ERR_MSG VARCHAR2(500);
          CURSOR ACO_FIJ IS
            SELECT FIJ_CLAVE,
                   FIJ_NRO,
                   FIJ_FEC_EMIS,
                   FIJ_FEC_LIMITE,
                   FIJ_CLAVE_CONTRATO,
                   FIJ_PROVEEDOR,
                   FIJ_CLIENTE,
                   FIJ_MONEDA,
                   FIJ_PRECIO,
                   FIJ_TASA,
                   FIJ_PRODUCTO,
                   FIJ_ARTICULO,
                   FIJ_CANTIDAD,
                   FIJ_SALDO,
                   FIJ_ZAFRA,
                   FIJ_LOR,
                   FIJ_EMPR
              FROM ACO_FIJACION_COMI015_TEMP
             WHERE FIJ_CLAVE =
                   (SELECT AD.DOC_CLAVE_FIJACION
                      FROM ACO_DOCUMENTO_COMI015_TEMP AD
                     WHERE AD.DOC_CLAVE_FIN = :NEW.DOC_CLAVE
                       AND AD.DOC_EMPR = :NEW.DOC_EMPR)
               AND FIJ_EMPR = :NEW.DOC_EMPR;
          V_FIJ_NRO NUMBER;

        BEGIN
          --Linea original, antes no ten?a la clave de empresa y devolv?a n?meros muy grandes. Se cambi? por una secuencia, para que sea m?s practico. 10/10/2020.
          ---SELECT NVL(MAX(FIJ_NRO), 0) + 1 INTO V_FIJ_NRO FROM ACO_FIJACION AC WHERE AC.FIJ_EMPR = :NEW.DOC_EMPR;
          V_FIJ_NRO := SEQ_ACO_FIJACION_H.NextVal;

          FOR AF IN ACO_FIJ LOOP

            INSERT INTO ACO_FIJACION
              (FIJ_CLAVE,
               FIJ_NRO,
               FIJ_FEC_EMIS,
               FIJ_FEC_LIMITE,
               FIJ_CLAVE_CONTRATO,
               FIJ_PROVEEDOR,
               FIJ_CLIENTE,
               FIJ_MONEDA,
               FIJ_PRECIO,
               FIJ_TASA,
               FIJ_PRODUCTO,
               FIJ_ARTICULO,
               FIJ_CANTIDAD,
               FIJ_SALDO,
               FIJ_ZAFRA,
               FIJ_LOR,
               FIJ_EMPR)
            VALUES
              (AF.FIJ_CLAVE,
               V_FIJ_NRO,
               AF.FIJ_FEC_EMIS,
               AF.FIJ_FEC_LIMITE,
               AF.FIJ_CLAVE_CONTRATO,
               AF.FIJ_PROVEEDOR,
               AF.FIJ_CLIENTE,
               AF.FIJ_MONEDA,
               AF.FIJ_PRECIO,
               AF.FIJ_TASA,
               AF.FIJ_PRODUCTO,
               AF.FIJ_ARTICULO,
               AF.FIJ_CANTIDAD,
               AF.FIJ_SALDO,
               AF.FIJ_ZAFRA,
               AF.FIJ_LOR,
               AF.FIJ_EMPR);

          END LOOP;
        EXCEPTION
          WHEN OTHERS THEN
            ERR_NUM := SQLCODE;
            ERR_MSG := SQLERRM;
            RAISE_APPLICATION_ERROR(-20000,
                                    'Error al INSERTAR ACO_FIJACION' ||
                                    ERR_NUM || ERR_MSG);
        END;

        ---PASA LOS DATOS DE DOCUMENTOS DE ACOPIO
        DECLARE
          ERR_NUM NUMBER;
          ERR_MSG VARCHAR2(500);
          CURSOR ACO_DOC IS
            SELECT DOC_CLAVE_FIN,
                   DOC_NRO,
                   DOC_OPERACION,
                   DOC_FEC,
                   DOC_PRODUCTO,
                   DOC_ZAFRA,
                   DOC_PROV,
                   DOC_MON,
                   DOC_UNID_NETO,
                   DOC_UNID_BRUTO,
                   DOC_NRO_PESAJE,
                   DOC_UNID_TARA,
                   DOC_UNID_DESCUENTO,
                   DOC_UNID_BONIFICACION,
                   DOC_PRECIO_UNITARIO_MON,
                   DOC_PRECIO_UNITARIO,
                   DOC_IMPORTE_MON,
                   DOC_TASA,
                   DOC_IMPORTE,
                   DOC_CLAVE_FIJACION,
                   DOC_LOR,
                   DOC_CLAVE,
                   DOC_EMPR
              FROM ACO_DOCUMENTO_COMI015_TEMP
             WHERE (DOC_NRO, DOC_CLAVE_FIN) =
                   (SELECT FR.DOC_NRO, FR.DOC_CLAVE_FIN
                      FROM ACO_DOCUMENTO_COMI015_TEMP FR
                     WHERE FR.DOC_CLAVE_FIN = :NEW.DOC_CLAVE
                       AND FR.DOC_EMPR = :NEW.DOC_EMPR)
               AND DOC_EMPR = :NEW.DOC_EMPR;

        BEGIN

          FOR AD IN ACO_DOC LOOP

            COM_DOC_RECIBIDOS_TABLAS.INSERTAR_ACO_DOCUMENTO('D',
                                                            AD.DOC_CLAVE_FIN,
                                                            AD.DOC_NRO,
                                                            AD.DOC_OPERACION,
                                                            AD.DOC_FEC,
                                                            AD.DOC_PRODUCTO,
                                                            AD.DOC_ZAFRA,
                                                            AD.DOC_PROV,
                                                            AD.DOC_MON,
                                                            AD.DOC_UNID_NETO,
                                                            AD.DOC_UNID_BRUTO,
                                                            AD.DOC_NRO_PESAJE,
                                                            AD.DOC_UNID_TARA,
                                                            AD.DOC_UNID_DESCUENTO,
                                                            AD.DOC_UNID_BONIFICACION,
                                                            AD.DOC_PRECIO_UNITARIO_MON,
                                                            AD.DOC_PRECIO_UNITARIO,
                                                            AD.DOC_IMPORTE_MON,
                                                            AD.DOC_TASA,
                                                            AD.DOC_IMPORTE,
                                                            AD.DOC_CLAVE_FIJACION,
                                                            AD.DOC_LOR,
                                                            AD.DOC_CLAVE,
                                                            AD.DOC_EMPR);
          END LOOP;
        EXCEPTION
          WHEN OTHERS THEN
            ERR_NUM := SQLCODE;
            ERR_MSG := SQLERRM;
            RAISE_APPLICATION_ERROR(-20000,
                                    'Error al INSERTAR_ACO_DOCUMENTO' ||
                                    ERR_NUM || ERR_MSG);
        END;

        ---PASA LOS DATOS DE LOS DETALLES DE CH_EMIT

        DECLARE
          ERR_NUM NUMBER;
          ERR_MSG VARCHAR2(255);
          CURSOR CH_EMIT IS
            SELECT CH_EMIT_CLAVE,
                   CH_EMIT_SERIE,
                   CH_EMIT_NRO,
                   CH_EMIT_FEC_VTO,
                   CH_EMIT_IMPORTE,
                   CH_EMIT_CLAVE_FIN,
                   CH_EMIT_CLAVE_FIN_CAN,
                   CH_EMIT_BENEFICIARIO,
                   CH_EMIT_EMPR
              FROM FIN_CHEQUE_EMIT_COMI015_TEMP
             WHERE CH_EMIT_CLAVE_FIN = :NEW.DOC_CLAVE
               AND CH_EMIT_EMPR = :NEW.DOC_EMPR;

        BEGIN

          FOR CE IN CH_EMIT LOOP

            COM_DOC_RECIBIDOS_TABLAS.INSERTAR_FIN_CHEQUE_EMIT('D',
                                                              CE.CH_EMIT_CLAVE,
                                                              CE.CH_EMIT_SERIE,
                                                              CE.CH_EMIT_NRO,
                                                              CE.CH_EMIT_FEC_VTO,
                                                              CE.CH_EMIT_IMPORTE,
                                                              CE.CH_EMIT_CLAVE_FIN,
                                                              CE.CH_EMIT_CLAVE_FIN_CAN,
                                                              CE.CH_EMIT_BENEFICIARIO,
                                                              CE.CH_EMIT_EMPR);

          END LOOP;
        EXCEPTION
          WHEN OTHERS THEN
            ERR_NUM := SQLCODE;
            ERR_MSG := SQLERRM;
            RAISE_APPLICATION_ERROR(-20000,
                                    'Error al INSERTAR_FIN_CHEQUE_EMIT' ||
                                    ERR_NUM || ERR_MSG);
        END;

        ---PROCEDIMIENTO DE QUE SE HACE CORRER DESDE EL COMI001 PERO SE RETRASA HASTA QUE SE CONFIRME
        DECLARE
          CURSOR DET_STK IS
            SELECT *
              FROM STK_DOCUMENTO_DET_COMI015_TEMP,
                   STK_DOCUMENTO_COMI015_TEMP
             WHERE DOCU_CLAVE = DETA_CLAVE_DOC
               AND DETA_CLAVE_DOC = :NEW.DOC_CLAVE_STK
               AND DETA_EMPR = :NEW.DOC_EMPR;
        BEGIN
          FOR I IN DET_STK LOOP
            STK_ACT_CANC_PED(I.DOCU_CLAVE,
                             1,
                             I.DOCU_SUC_ORIG,
                             I.DOCU_DEP_ORIG,
                             'I',
                             I.DETA_NRO_ITEM,
                             TO_NUMBER(I.DETA_ART),
                             I.DETA_CANT);
            NULL;
          END LOOP;

        EXCEPTION
          WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR(-20000,
                                    'Error al INSERTAR STK_ACT_CANC_PED');
        END;

        --PASA LOS REGISTROS A LAS TABLAS ORIGINALES DE LOS QUE FIGUREN COMO  RET O AUXILIAR------------------

        DECLARE
          CURSOR DOC_RET IS
            SELECT *
              FROM FIN_DOCUMENTO_COMI015_TEMP_RET G
             WHERE G.DOC_CLAVE_PADRE = :NEW.DOC_CLAVE
               AND G.DOC_EMPR = :NEW.DOC_EMPR;
        BEGIN

          FOR C IN DOC_RET LOOP

            COM_DOC_RECIBIDOS_TABLAS.INSERTAR_DOCUMENTO_FIN(V_TABLA                     => 'D',
                                                            V_DOC_CLAVE                 => C.DOC_CLAVE,
                                                            V_DOC_EMPR                  => C.DOC_EMPR,
                                                            V_DOC_CTA_BCO               => C.DOC_CTA_BCO,
                                                            V_DOC_COND_VTA              => C.DOC_COND_VTA,
                                                            V_DOC_SUC                   => C.DOC_SUC,
                                                            V_DOC_DPTO                  => C.DOC_DPTO,
                                                            V_DOC_TIPO_MOV              => C.DOC_TIPO_MOV,
                                                            V_DOC_NRO_DOC               => C.DOC_NRO_DOC,
                                                            V_DOC_TIPO_SALDO            => C.DOC_TIPO_SALDO,
                                                            V_DOC_MON                   => C.DOC_MON,
                                                            V_DOC_PROV                  => C.DOC_PROV,
                                                            V_DOC_CLI                   => C.DOC_CLI,
                                                            V_DOC_CLI_NOM               => C.DOC_CLI_NOM,
                                                            V_DOC_CLI_DIR               => C.DOC_CLI_DIR,
                                                            V_DOC_CLI_RUC               => C.DOC_CLI_RUC,
                                                            V_DOC_CLI_TEL               => C.DOC_CLI_TEL,
                                                            V_DOC_LEGAJO                => C.DOC_LEGAJO,
                                                            V_DOC_BCO_CHEQUE            => C.DOC_BCO_CHEQUE,
                                                            V_DOC_NRO_CHEQUE            => C.DOC_NRO_CHEQUE,
                                                            V_DOC_FEC_CHEQUE            => C.DOC_FEC_CHEQUE,
                                                            V_DOC_FEC_DEP_CHEQUE        => C.DOC_FEC_DEP_CHEQUE,
                                                            V_DOC_EST_CHEQUE            => C.DOC_EST_CHEQUE,
                                                            V_DOC_NRO_DOC_DEP           => C.DOC_NRO_DOC_DEP,
                                                            V_DOC_CHEQUE_CERTIF         => C.DOC_CHEQUE_CERTIF,
                                                            V_DOC_FEC_OPER              => C.DOC_FEC_OPER,
                                                            V_DOC_FEC_DOC               => C.DOC_FEC_DOC,
                                                            V_DOC_BRUTO_EXEN_LOC        => C.DOC_BRUTO_EXEN_LOC,
                                                            V_DOC_BRUTO_EXEN_MON        => C.DOC_BRUTO_EXEN_MON,
                                                            V_DOC_BRUTO_GRAV_LOC        => C.DOC_BRUTO_GRAV_LOC,
                                                            V_DOC_BRUTO_GRAV_MON        => C.DOC_BRUTO_GRAV_MON,
                                                            V_DOC_NETO_EXEN_LOC         => C.DOC_NETO_EXEN_LOC,
                                                            V_DOC_NETO_EXEN_MON         => C.DOC_NETO_EXEN_MON,
                                                            V_DOC_NETO_GRAV_LOC         => C.DOC_NETO_GRAV_LOC,
                                                            V_DOC_NETO_GRAV_MON         => C.DOC_NETO_GRAV_MON,
                                                            V_DOC_IVA_LOC               => C.DOC_IVA_LOC,
                                                            V_DOC_IVA_MON               => C.DOC_IVA_MON,
                                                            V_DOC_SALDO_INI_MON         => C.DOC_SALDO_INI_MON,
                                                            V_DOC_SALDO_LOC             => C.DOC_SALDO_LOC,
                                                            V_DOC_SALDO_MON             => C.DOC_SALDO_MON,
                                                            V_DOC_SALDO_PER_ACT_LOC     => C.DOC_SALDO_PER_ACT_LOC,
                                                            V_DOC_SALDO_PER_ACT_MON     => C.DOC_SALDO_PER_ACT_MON,
                                                            V_DOC_OBS                   => C.DOC_OBS,
                                                            V_DOC_BASE_IMPON_LOC        => C.DOC_BASE_IMPON_LOC,
                                                            V_DOC_BASE_IMPON_MON        => C.DOC_BASE_IMPON_MON,
                                                            V_DOC_DIF_CAMBIO            => C.DOC_DIF_CAMBIO,
                                                            V_DOC_IND_STK               => C.DOC_IND_STK,
                                                            V_DOC_CLAVE_STK             => C.DOC_CLAVE_STK,
                                                            V_DOC_IND_CUOTA             => C.DOC_IND_CUOTA,
                                                            V_DOC_IND_PEDIDO            => C.DOC_IND_PEDIDO,
                                                            V_DOC_IND_FINANRESA         => C.DOC_IND_FINANRESA,
                                                            V_DOC_IND_CONSIGNACION      => C.DOC_IND_CONSIGNACION,
                                                            V_DOC_PLAN_FINAN            => C.DOC_PLAN_FINAN,
                                                            V_DOC_NRO_LISTADO_LIBRO_IVA => C.DOC_NRO_LISTADO_LIBRO_IVA,
                                                            V_DOC_CLAVE_PADRE           => C.DOC_CLAVE_PADRE,
                                                            V_DOC_LOGIN                 => C.DOC_LOGIN,
                                                            V_DOC_FEC_GRAB              => C.DOC_FEC_GRAB,
                                                            V_DOC_SIST                  => C.DOC_SIST,
                                                            V_DOC_CLI_CODEUDOR          => C.DOC_CLI_CODEUDOR,
                                                            V_DOC_OPERADOR              => C.DOC_OPERADOR,
                                                            V_DOC_COBRADOR              => C.DOC_COBRADOR,
                                                            V_DOC_CANT_PAGARE           => C.DOC_CANT_PAGARE,
                                                            V_DOC_SERIE                 => C.DOC_SERIE,
                                                            V_DOC_CLAVE_SCLI            => C.DOC_CLAVE_SCLI,
                                                            V_DOC_NRO_PED               => C.DOC_NRO_PED,
                                                            V_DOC_ORDEN_COMPRA_CLAVE    => C.DOC_ORDEN_COMPRA_CLAVE,
                                                            V_DOC_PRES_CLAVE            => C.DOC_PRES_CLAVE,
                                                            V_DOC_TASA                  => C.DOC_TASA,
                                                            V_DOC_NRO_ORDEN_COMPRA      => C.DOC_NRO_ORDEN_COMPRA,
                                                            V_DOC_DIF_CAMBIO_ACUM       => C.DOC_DIF_CAMBIO_ACUM,
                                                            V_DOC_DIVISION              => C.DOC_DIVISION,
                                                            V_DOC_FORMA_ENTREGA         => C.DOC_FORMA_ENTREGA,
                                                            V_DOC_CODIGO_TEFE           => C.DOC_CODIGO_TEFE,
                                                            V_DOC_EMPLEADO              => C.DOC_EMPLEADO,
                                                            V_DOC_IVA_10_LOC            => C.DOC_IVA_10_LOC,
                                                            V_DOC_IVA_10_MON            => C.DOC_IVA_10_MON,
                                                            V_DOC_IVA_5_LOC             => C.DOC_IVA_5_LOC,
                                                            V_DOC_IVA_5_MON             => C.DOC_IVA_5_MON,
                                                            V_DOC_GRAV_10_LOC           => C.DOC_GRAV_10_LOC,
                                                            V_DOC_GRAV_10_MON           => C.DOC_GRAV_10_MON,
                                                            V_DOC_GRAV_5_MON            => C.DOC_GRAV_5_MON,
                                                            V_DOC_GRAV_5_LOC            => C.DOC_GRAV_5_LOC,
                                                            V_DOC_CLI_NRO_PED           => C.DOC_CLI_NRO_PED,
                                                            V_DOC_CAMION                => C.DOC_CAMION,
                                                            V_DOC_IND_ORD_CARGA         => C.DOC_IND_ORD_CARGA,
                                                            V_DOC_CLAVE_RETENCION       => C.DOC_CLAVE_RETENCION,
                                                            V_DOC_IND_IMAGRO            => C.DOC_IND_IMAGRO,
                                                            V_DOC_TIMBRADO              => C.DOC_TIMBRADO,
                                                            V_DOC_RUC_DV                => C.DOC_RUC_DV,
                                                            V_DOC_DV                    => C.DOC_DV,
                                                            V_DOC_IND_EXPORT            => C.DOC_IND_EXPORT,
                                                            V_DOC_TIMBRADO2             => C.DOC_TIMBRADO2,
                                                            V_DOC_PROV_ACOPIO           => C.DOC_PROV_ACOPIO,
                                                            V_DOC_MES_VTO_TIMBRADO      => C.DOC_MES_VTO_TIMBRADO,
                                                            V_DOC_ANO_VTO_TIMBRADO      => C.DOC_ANO_VTO_TIMBRADO,
                                                            V_DOC_CAPITAL_MON           => C.DOC_CAPITAL_MON,
                                                            V_DOC_CAPITAL_LOC           => C.DOC_CAPITAL_LOC,
                                                            V_DOC_SAL_CAP_MON           => C.DOC_SAL_CAP_MON,
                                                            V_DOC_SAL_CAP_LOC           => C.DOC_SAL_CAP_LOC,
                                                            V_DOC_SAL_CAP_PER_ACT_MON   => C.DOC_SAL_CAP_PER_ACT_MON,
                                                            V_DOC_SAL_CAP_PER_ACT_LOC   => C.DOC_SAL_CAP_PER_ACT_LOC,
                                                            V_DOC_DIF_CAM_CAP           => C.DOC_DIF_CAM_CAP,
                                                            V_DOC_DIF_CAM_CAP_ACUM      => C.DOC_DIF_CAM_CAP_ACUM,
                                                            V_DOC_INTERES_MON           => C.DOC_INTERES_MON,
                                                            V_DOC_INTERES_LOC           => C.DOC_INTERES_LOC,
                                                            V_DOC_SAL_INT_MON           => C.DOC_SAL_INT_MON,
                                                            V_DOC_SAL_INT_LOC           => C.DOC_SAL_INT_LOC,
                                                            V_DOC_SAL_INT_PER_ACT_MON   => C.DOC_SAL_INT_PER_ACT_MON,
                                                            V_DOC_SAL_INT_PER_ACT_LOC   => C.DOC_SAL_INT_PER_ACT_LOC,
                                                            V_DOC_DIF_CAM_INT           => C.DOC_DIF_CAM_INT,
                                                            V_DOC_DIF_CAM_INT_ACUM      => C.DOC_DIF_CAM_INT_ACUM,
                                                            V_DOC_CTACO                 => C.DOC_CTACO,
                                                            V_DOC_IND_EXC_CNT           => C.DOC_IND_EXC_CNT,
                                                            V_DOC_IND_FACTURAR_IVA      => C.DOC_IND_FACTURAR_IVA,
                                                            V_DOC_CLAVE_PEDIDO_CAST     => C.DOC_CLAVE_PEDIDO_CAST,
                                                            V_DOC_PORC_VENDEDOR         => C.DOC_PORC_VENDEDOR,
                                                            V_DOC_PORC_COMPRADOR        => C.DOC_PORC_COMPRADOR,
                                                            V_DOC_OBS_ST                => C.DOC_OBS_ST,
                                                            V_DOC_ORDEN_COMPRA          => C.DOC_ORDEN_COMPRA,
                                                            V_DOC_GTOS_FIN_MON          => C.DOC_GTOS_FIN_MON,
                                                            V_DOC_GTOS_FIN_LOC          => C.DOC_GTOS_FIN_LOC,
                                                            V_DOC_INDIC_1PAGARE         => C.DOC_INDIC_1PAGARE,
                                                            V_DOC_TIPO_VENTA            => C.DOC_TIPO_VENTA,
                                                            V_DOC_NRO_NOTA              => C.DOC_NRO_NOTA,
                                                            V_DOC_NRO_CONTRATO          => C.DOC_NRO_CONTRATO,
                                                            V_DOC_PORC_IVA_PMO          => C.DOC_PORC_IVA_PMO,
                                                            V_DOC_INTM_MON              => C.DOC_INTM_MON,
                                                            V_DOC_IND_COBRO             => C.DOC_IND_COBRO,
                                                            V_DOC_ACT_PRESTAMO          => C.DOC_ACT_PRESTAMO,
                                                            V_DOC_NRO_CREDITO_PREST     => C.DOC_NRO_CREDITO_PREST,
                                                            V_DOC_IND_CANC_PMO          => C.DOC_IND_CANC_PMO,
                                                            V_DOC_INTM_LOC              => C.DOC_INTM_LOC,
                                                            V_DOC_CLAVE_SOL             => C.DOC_CLAVE_SOL,
                                                            V_DOC_IND_PREST_REESTRUCT   => C.DOC_IND_PREST_REESTRUCT,
                                                            V_DOC_IND_IMPR_VENDEDOR     => C.DOC_IND_IMPR_VENDEDOR,
                                                            V_DOC_IND_IMPR_CONYUGUE     => C.DOC_IND_IMPR_CONYUGUE,
                                                            V_DOC_REC_COB               => C.DOC_REC_COB,
                                                            V_DOC_SUC_VTA               => C.DOC_SUC_VTA,
                                                            V_DOC_CANAL                 => C.DOC_CANAL,
                                                            V_DOC_CTRL_RET_REC          => C.DOC_CTRL_RET_REC,
                                                            V_DOC_SISTEMA_AUX           => C.DOC_SISTEMA_AUX,
                                                            V_COMI005_ESTADO            => NULL,
                                                            V_COMI005_FEC_ACT_EST       => NULL,
                                                            V_COMI005_LOGIN_EST         => NULL,
                                                            V_DOC_CLAVE_FIN_TA          => NULL,
                                                            V_DOC_CLAVE_LIQ_TA          => NULL,
                                                            V_DOC_ACUSE_ENVIO_C         => NULL,
                                                            V_DOC_EXP_PROFORMA          => NULL,
                                                            V_DOC_CTA_BCO_FCON          => NULL,
                                                            V_DOC_CLAVE_FAC_NCR         => NULL,
                                                            V_DOC_CTA_BCO_NCR           => NULL,
                                                            V_DOC_SNC_CLAVE             => NULL,
                                                            V_DOC_SUBV                  => NULL,
                                                            V_DOC_CLAVE_STK_FLETE       => NULL,
                                                            V_DOC_NRO_TRANS_COMBUS      => NULL,
                                                            V_TIPO_DOC_PRO_CLI          => C.DOC_TIPO_DOC_CLI_PROV,
                                                            V_TIPO_FACTURA              => C.DOC_TIPO_FACTURA);

            ---PASA LOS DATOS DEL CONCEPTO DEL DOCUMENTO EN LA TABLA ORIGINAL
            DECLARE
              CURSOR CON_RET IS
                SELECT *
                  FROM FIN_DOC_CONCEPTO_COMI015_RET
                 WHERE DCON_CLAVE_DOC = C.DOC_CLAVE
                   AND DCON_EMPR = C.DOC_EMPR;

            BEGIN

              FOR CO IN CON_RET LOOP
                COM_DOC_RECIBIDOS_TABLAS.INSERTAR_DOC_CONCEPTO(V_TABLA                    => 'D',
                                                               V_DCON_CLAVE_DOC           => CO.DCON_CLAVE_DOC,
                                                               V_DCON_ITEM                => CO.DCON_ITEM,
                                                               V_DCON_CLAVE_CONCEPTO      => CO.DCON_CLAVE_CONCEPTO,
                                                               V_DCON_CLAVE_CTACO         => CO.DCON_CLAVE_CTACO,
                                                               V_DCON_TIPO_SALDO          => CO.DCON_TIPO_SALDO,
                                                               V_DCON_EXEN_LOC            => CO.DCON_EXEN_LOC,
                                                               V_DCON_EXEN_MON            => CO.DCON_EXEN_MON,
                                                               V_DCON_GRAV_LOC            => CO.DCON_GRAV_LOC,
                                                               V_DCON_GRAV_MON            => CO.DCON_GRAV_MON,
                                                               V_DCON_IVA_LOC             => CO.DCON_IVA_LOC,
                                                               V_DCON_IVA_MON             => CO.DCON_IVA_MON,
                                                               V_DCON_CLAVE_IMP           => CO.DCON_CLAVE_IMP,
                                                               V_DCON_IND_TIPO_IVA_COMPRA => CO.DCON_IND_TIPO_IVA_COMPRA,
                                                               V_DCON_OBS                 => CO.DCON_OBS,
                                                               V_DCON_PORC_IVA            => CO.DCON_PORC_IVA,
                                                               V_DCON_IND_IMAGRO          => CO.DCON_IND_IMAGRO,
                                                               V_DCON_CANAL               => CO.DCON_CANAL,
                                                               V_DCON_DIV_CANAL           => CO.DCON_DIV_CANAL,
                                                               V_DCON_CANT_COMB           => CO.DCON_CANT_COMB,
                                                               V_DCON_KM_ACTUAL           => CO.DCON_KM_ACTUAL,
                                                               V_DCON_OCOMD_CLAVE         => CO.DCON_OCOMD_CLAVE,
                                                               V_DCON_OCOMD_NRO_ITEM      => CO.DCON_OCOMD_NRO_ITEM,
                                                               V_DCON_EMPR                => CO.DCON_EMPR,
                                                               V_DCON_OCOMD_PORC          => CO.DCON_OCOMD_PORC,
                                                               V_DCON_IMP_AFECTA_COSTO    => CO.DCON_IMP_AFECTA_COSTO,
                                                               V_DCON_CANAL_BETA          => CO.DCON_CANAL_BETA,
                                                               V_DCON_IND_VIATICO         => NULL);

              END LOOP;
            EXCEPTION
              WHEN OTHERS THEN
                RAISE_APPLICATION_ERROR(-20000,
                                        'Error al INSERTAR INSERTAR_DOC_CONCEPTO_2' ||
                                        SQLCODE || SQLERRM);
            END;

            ---PASA LOS DATOS DE LA RETENCION DEL DOCUMENTO EN LA TABLA ORIGINAL
            DECLARE
              CURSOR F_RET IS
                SELECT RET_DOC_NRO,
                       RET_DOC_FEC_DOC,
                       RET_CONTRIBUYENTE,
                       RET_TIPO_ID_CONTRIBUYENTE,
                       RET_ID_CONTRIBUYENTE,
                       RET_TOTAL,
                       RET_ESTADO,
                       RET_USER_EMIT,
                       RET_FEC_CONF,
                       RET_USER_CONF,
                       RET_DOC_CLAVE,
                       RET_RET_CLAVE,
                       RET_FECHA,
                       RET_JSON,
                       RET_FEC_GRAB,
                       RET_TIPO,
                       RET_PORCENTAJE,
                       RET_EMPR
                  FROM FIN_RETENCION_COMI015_TEMP
                 WHERE RET_RET_CLAVE = C.DOC_CLAVE
                   AND RET_EMPR = :NEW.DOC_EMPR;

            BEGIN

              FOR FR IN F_RET LOOP

                INSERT INTO FIN_RETENCION
                  (RET_DOC_NRO,
                   RET_DOC_FEC_DOC,
                   RET_CONTRIBUYENTE,
                   RET_TIPO_ID_CONTRIBUYENTE,
                   RET_ID_CONTRIBUYENTE,
                   RET_TOTAL,
                   RET_ESTADO,
                   RET_USER_EMIT,
                   RET_FEC_CONF,
                   RET_USER_CONF,
                   RET_DOC_CLAVE,
                   RET_RET_CLAVE,
                   RET_FECHA,
                   RET_JSON,
                   RET_FEC_GRAB,
                   RET_TIPO,
                   RET_PORCENTAJE,
                   RET_EMPR)
                VALUES
                  (FR.RET_DOC_NRO,
                   FR.RET_DOC_FEC_DOC,
                   FR.RET_CONTRIBUYENTE,
                   FR.RET_TIPO_ID_CONTRIBUYENTE,
                   FR.RET_ID_CONTRIBUYENTE,
                   FR.RET_TOTAL,
                   FR.RET_ESTADO,
                   FR.RET_USER_EMIT,
                   FR.RET_FEC_CONF,
                   FR.RET_USER_CONF,
                   FR.RET_DOC_CLAVE,
                   FR.RET_RET_CLAVE,
                   FR.RET_FECHA,
                   FR.RET_JSON,
                   FR.RET_FEC_GRAB,
                   FR.RET_TIPO,
                   FR.RET_PORCENTAJE,
                   FR.RET_EMPR);
              END LOOP;
            EXCEPTION
              WHEN OTHERS THEN
                RAISE_APPLICATION_ERROR(-20000,
                                        'Error al INSERTAR FIN_RETENCION');
            END;

            ---EN CASO QUE EN EL 2-2-5 SE CARGO ALGUN CHEQUE, ESTE SE PASA AL ORIGINAL

            DECLARE
              CLAVE_CHEQUE NUMBER;
            BEGIN
              SELECT CHDO_CLAVE_CHEQ
                INTO CLAVE_CHEQUE
                FROM FIN_CHEQUE_DOCUMENTO_COMI015
               WHERE CHDO_CLAVE_DOC = C.DOC_CLAVE
                 AND CHDO_EMPR = C.DOC_EMPR;

              DECLARE
                CURSOR F_CHEQ IS
                  SELECT CHEQ_CLAVE,
                         CHEQ_EMPR,
                         CHEQ_SERIE,
                         CHEQ_NRO,
                         CHEQ_SUC,
                         CHEQ_BCO,
                         CHEQ_MON,
                         CHEQ_CLI,
                         CHEQ_ORDEN,
                         CHEQ_CLI_NOM,
                         CHEQ_TITULAR,
                         CHEQ_FEC_EMIS,
                         CHEQ_FEC_DEPOSITAR,
                         CHEQ_IMPORTE,
                         CHEQ_SITUACION,
                         CHEQ_OBS,
                         CHEQ_FEC_RECHAZO,
                         CHEQ_LEGAJO,
                         CHEQ_ABOGADO,
                         CHEQ_FEC_GRAB,
                         CHEQ_LOGIN,
                         CHEQ_NRO_CTA_CHEQ,
                         CHEQ_IMPORTE_LOC,
                         CHEQ_MOTI_RECH
                    FROM FIN_CHEQUE_COMI015_TEMP
                   WHERE CHEQ_CLAVE = CLAVE_CHEQUE
                     AND CHEQ_EMPR = :NEW.DOC_EMPR;

                V_CHDO_CLAVE_DOC    NUMBER;
                V_CHDO_CLAVE_CHEQ   NUMBER;
                V_CHDO_EST_ANT_CHEQ NUMBER;
                V_CHDO_EMPR         NUMBER;
              BEGIN

                FOR FCH IN F_CHEQ LOOP
                  INSERT INTO FIN_CHEQUE
                    (CHEQ_CLAVE,
                     CHEQ_EMPR,
                     CHEQ_SERIE,
                     CHEQ_NRO,
                     CHEQ_SUC,
                     CHEQ_BCO,
                     CHEQ_MON,
                     CHEQ_CLI,
                     CHEQ_ORDEN,
                     CHEQ_CLI_NOM,
                     CHEQ_TITULAR,
                     CHEQ_FEC_EMIS,
                     CHEQ_FEC_DEPOSITAR,
                     CHEQ_IMPORTE,
                     CHEQ_SITUACION,
                     CHEQ_OBS,
                     CHEQ_FEC_RECHAZO,
                     CHEQ_LEGAJO,
                     CHEQ_ABOGADO,
                     CHEQ_FEC_GRAB,
                     CHEQ_LOGIN,
                     CHEQ_NRO_CTA_CHEQ,
                     CHEQ_IMPORTE_LOC,
                     CHEQ_MOTI_RECH)
                  VALUES
                    (FCH.CHEQ_CLAVE,
                     FCH.CHEQ_EMPR,
                     FCH.CHEQ_SERIE,
                     FCH.CHEQ_NRO,
                     FCH.CHEQ_SUC,
                     FCH.CHEQ_BCO,
                     FCH.CHEQ_MON,
                     FCH.CHEQ_CLI,
                     FCH.CHEQ_ORDEN,
                     FCH.CHEQ_CLI_NOM,
                     FCH.CHEQ_TITULAR,
                     FCH.CHEQ_FEC_EMIS,
                     FCH.CHEQ_FEC_DEPOSITAR,
                     FCH.CHEQ_IMPORTE,
                     FCH.CHEQ_SITUACION,
                     FCH.CHEQ_OBS,
                     FCH.CHEQ_FEC_RECHAZO,
                     FCH.CHEQ_LEGAJO,
                     FCH.CHEQ_ABOGADO,
                     FCH.CHEQ_FEC_GRAB,
                     FCH.CHEQ_LOGIN,
                     FCH.CHEQ_NRO_CTA_CHEQ,
                     FCH.CHEQ_IMPORTE_LOC,
                     FCH.CHEQ_MOTI_RECH);

                  SELECT CF.CHDO_CLAVE_DOC,
                         CF.CHDO_CLAVE_CHEQ,
                         CF.CHDO_EST_ANT_CHEQ,
                         CF.CHDO_EMPR
                    INTO V_CHDO_CLAVE_DOC,
                         V_CHDO_CLAVE_CHEQ,
                         V_CHDO_EST_ANT_CHEQ,
                         V_CHDO_EMPR
                    FROM FIN_CHEQUE_DOCUMENTO_COMI015 CF
                   WHERE CF.CHDO_CLAVE_CHEQ = CLAVE_CHEQUE
                     AND CF.CHDO_CLAVE_DOC = C.DOC_CLAVE
                     AND CF.CHDO_EMPR = C.DOC_EMPR
                     AND CF.CHDO_EMPR = :NEW.DOC_EMPR;

                  INSERT INTO FIN_CHEQUE_DOCUMENTO
                    (CHDO_CLAVE_DOC,
                     CHDO_CLAVE_CHEQ,
                     CHDO_EST_ANT_CHEQ,
                     CHDO_EMPR)
                  VALUES
                    (V_CHDO_CLAVE_DOC,
                     V_CHDO_CLAVE_CHEQ,
                     V_CHDO_EST_ANT_CHEQ,
                     V_CHDO_EMPR);

                END LOOP;
              EXCEPTION
                WHEN NO_DATA_FOUND THEN
                  NULL;
                WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR(-20000,
                                          'Error al FIN_CHEQUE_DOCUMENTO');
              END;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                NULL;
              WHEN OTHERS THEN
                RAISE_APPLICATION_ERROR(-20000,
                                        'Error al FIN_CHEQUE_DOCUMENTO_2');
            END;

          END LOOP;
        END;

      END;

      :NEW.COMI005_FEC_ACT_EST := NVL(:NEW.COMI005_FEC_ACT_EST, SYSDATE);
      :NEW.COMI005_LOGIN_EST   := GEN_DEVUELVE_USER;

      ------------------------------------------------------------------------------------------------------------------------------------------------------------
      --Verificar si la orden ya esta para ser cerrada. Si llega al 95% ya debe cerrarse. Si es una orden de conceptos, debe finiquitarse directo. 29/09/2020-----
      ------------------------------------------------------------------------------------------------------------------------------------------------------------
      IF :NEW.DOC_CLAVE_STK IS NULL THEN
        -- Es una factura por conceptos, si tiene orden, finiquitar directo.
        UPDATE STK_ORDEN_COMPRA S
           SET S.OCOM_ESTADO         = 'F',
               S.OCOM_FEC_CONT_PRES  = SYSDATE,
               S.OCOM_OPER_CONT_PRES = GEN_DEVUELVE_USER
         WHERE S.OCOM_CLAVE = :NEW.DOC_ORDEN_COMPRA
           AND S.OCOM_EMPR = :NEW.DOC_EMPR;
      elsif :NEW.DOC_CLAVE_STK IS NOT NULL AND
            STK_ESTADO_OC(:NEW.DOC_ORDEN_COMPRA, :NEW.DOC_EMPR) = 'S' THEN
        --La orden ya se debe cerrar.
        UPDATE STK_ORDEN_COMPRA S
           SET S.OCOM_ESTADO         = 'C',
               S.OCOM_FEC_CONT_PRES  = SYSDATE,
               S.OCOM_OPER_CONT_PRES = GEN_DEVUELVE_USER
         WHERE S.OCOM_CLAVE = :NEW.DOC_ORDEN_COMPRA
           AND S.OCOM_EMPR = :NEW.DOC_EMPR;
      end if;

    END IF;

  ELSIF (:NEW.COMI005_ESTADO = 'A') and :NEW.DOC_TIPO_MOV <> 16 THEN

    UPDATE STK_ENTRADA_MERC S
       SET S.ENTMER_CLAVE_DOC_FIN = NULL
     WHERE S.ENTMER_CLAVE_DOC_FIN = :NEW.DOC_CLAVE
       AND S.ENTMER_EMPR = :NEW.DOC_EMPR;

    :NEW.COMI005_FEC_ACT_EST := NVL(:NEW.COMI005_FEC_ACT_EST, SYSDATE);
    :NEW.COMI005_LOGIN_EST   := GEN_DEVUELVE_USER;

    DELETE FROM STK_FLETES_REMIS_FINAN T
     WHERE T.FLEREMFIN_FINAN = :NEW.DOC_CLAVE
       AND T.FLEREMFIN_EMPR = :NEW.DOC_EMPR; --EN CASO QUE TENGA UN FLETE DE EXPORTACION ANCLADO A ESTE DOCUMENTO.

  END IF;

  --EXCEPTION
  --  WHEN SALIR THEN

exception
  when others then
    RAISE_APPLICATION_ERROR(-20001,
                            'Error en el trigger FIN_DOCUMENTO_COMI015_BU' ||
                            SQLCODE || '-' || SQLERRM);

END FIN_DOCUMENTO_COMI015_BU;
/
