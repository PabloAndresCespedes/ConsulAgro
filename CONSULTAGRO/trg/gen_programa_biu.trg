create or replace trigger gen_programa_biu
  before insert or update
  on GEN_PROGRAMA 
  for each row
begin
  -- 20/08/2022 11:27:29 @PabloACespedes \(^-^)/
  -- asigna el icono segun su tipo si es que viene nulo
  select tp.tipr_icono
  into :new.prog_icono
  from gen_tipo_programa tp
  where tp.tipr_codigo = :new.prog_tipo_programa
  and   :new.prog_icono is null;
exception
  when no_data_found then
    null;
end gen_programa_biu;
/
