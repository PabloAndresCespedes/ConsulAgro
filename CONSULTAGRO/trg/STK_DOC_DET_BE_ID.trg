CREATE OR REPLACE TRIGGER "STK_DOC_DET_BE_ID"
BEFORE INSERT OR DELETE ON STK_DOCUMENTO_DET
REFERENCING OLD AS O
NEW AS N
FOR EACH ROW

declare
  v_tipo_art number;
BEGIN
IF :N.DETA_EMPR NOT IN (2,7) OR :O.DETA_EMPR NOT IN (2,7) THEN
  IF INSERTING THEN

     if :N.DETA_ART is null then
         RAISE_APPLICATION_ERROR(-20001, 'Codigo de articulo no puede ser nulo!');
     end if;

    begin
    select art_tipo
      into v_tipo_art
      from stk_articulo
     where art_codigo = :N.DETA_ART
       and art_empr = :N.DETA_EMPR;
    exception
    when others then
    Raise_application_error(-20000, 'Tipo de articulo duplicado');
    end;



  --

  --
    if v_tipo_art <> 4   then
      /* actualizar la existencia en la tabla STK_ARTICULO_DEPOSITO */
      STK_ACT_ART_DEP('I',
                      :N.DETA_ART,
                      :N.DETA_CLAVE_DOC,
                      :N.DETA_CANT,
                      :N.DETA_EMPR);


      /* actualizar datos en la tabla STK_ART_EMPR_PERI */
   STK_ACT_ART_EMP_OPER('I',
                      :N.DETA_ART,
                      :N.DETA_CLAVE_DOC,
                      :N.DETA_CANT,
                      :N.DETA_IMP_NETO_LOC,
                      :N.DETA_EMPR);
    --24331666,0000
   --IF :N.DETA_CLAVE_DOC = 52422630101 THEN
 /*
     STK_ACT_ART_EMP('I',
                      :N.DETA_ART,
                      :N.DETA_CLAVE_DOC,
                      :N.DETA_CANT,
                      NVL(:N.DETA_IMP_NETO_LOC,24331666),
                      :N.DETA_EMPR);
                      */
  --  ELSE

      STK_ACT_ART_EMP('I',
                      :N.DETA_ART,
                      :N.DETA_CLAVE_DOC,
                      :N.DETA_CANT,
                      :N.DETA_IMP_NETO_LOC,
                      :N.DETA_EMPR,
                      :N.DETA_IMP_NETO_MON);
   -- END IF;




      -- 05-08-2002 - Enrique Pereira - asignar un valor a la columna deta_periodo
      begin
      SELECT PERI_CODIGO
        INTO :N.DETA_PERIODO
        FROM STK_PERIODO, STK_DOCUMENTO
       WHERE DOCU_CLAVE = :N.DETA_CLAVE_DOC
         AND DOCU_FEC_EMIS BETWEEN PERI_FEC_INI AND PERI_FEC_FIN
         AND DOCU_EMPR = PERI_EMPR
         AND DOCU_EMPR = :N.DETA_EMPR;
      exception
        when too_many_rows then
          Raise_application_error(-20000, 'Muchos periodos de stock con misma fecha'); 
      end;  
      
       SELECT PERI_CODIGO
        INTO :N.DETA_PERIODO_OPER
        FROM STK_PERIODO, STK_DOCUMENTO
       WHERE DOCU_CLAVE = :N.DETA_CLAVE_DOC
         AND DOCU_FEC_OPER BETWEEN PERI_FEC_INI AND PERI_FEC_FIN
         AND DOCU_EMPR = PERI_EMPR
         AND DOCU_EMPR = :N.DETA_EMPR;
    end if;

  END IF;

  IF DELETING THEN

    select art_tipo
      into v_tipo_art
      from stk_articulo
     where art_codigo = :O.DETA_ART
       and art_empr = :O.DETA_EMPR;

    if v_tipo_art <> 4 then
      /* Grabar la tabla STK_AUD_DOCUMENTO_DET */
      INSERT INTO STK_AUD_DOCUMENTO_DET
        (ADETA_CLAVE_DOC,
         ADETA_NRO_ITEM,
         ADETA_ART,
         ADETA_EMPR,
         ADETA_NRO_REM,
         ADETA_CANT,
         ADETA_IMP_NETO_LOC,
         ADETA_IMP_NETO_MON,
         ADETA_IMPU,
         ADETA_IVA_LOC,
         ADETA_IVA_MON,
         ADETA_PORC_DTO,
         ADETA_IMP_BRUTO_LOC,
         ADETA_IMP_BRUTO_MON,
         ADETA_LOGIN_ANUL,
         ADETA_FEC_ANUL)
      VALUES
        (:O.DETA_CLAVE_DOC,
         :O.DETA_NRO_ITEM,
         :O.DETA_ART,
         :O.DETA_EMPR,
         :O.DETA_NRO_REM,
         :O.DETA_CANT,
         :O.DETA_IMP_NETO_LOC,
         :O.DETA_IMP_NETO_MON,
         :O.DETA_IMPU,
         :O.DETA_IVA_LOC,
         :O.DETA_IVA_MON,
         :O.DETA_PORC_DTO,
         :O.DETA_IMP_BRUTO_LOC,
         :O.DETA_IMP_BRUTO_MON,
         SUBSTR(USER, 1, 8),
         SYSDATE);

      /* actualizar la existencia en la tabla STK_ARTICULO_DEPOSITO */
      STK_ACT_ART_DEP('D',
                      :O.DETA_ART,
                      :O.DETA_CLAVE_DOC,
                      :O.DETA_CANT,
                      :O.DETA_EMPR);
 STK_ACT_ART_EMP_OPER('D',
                      :O.DETA_ART,
                      :O.DETA_CLAVE_DOC,
                      :O.DETA_CANT,
                      :O.DETA_IMP_NETO_LOC,
                      :O.DETA_EMPR);


     STK_ACT_ART_EMP('D',
                      :O.DETA_ART,
                      :O.DETA_CLAVE_DOC,
                      :O.DETA_CANT,
                      :O.DETA_IMP_NETO_LOC,
                      :O.DETA_EMPR,
                      :O.DETA_IMP_NETO_MON);


    end if;

  END IF;
ELSE
IF INSERTING THEN
   STK_ACT_ART_DEP('I',
                      :N.DETA_ART,
                      :N.DETA_CLAVE_DOC,
                      :N.DETA_CANT,
                      :N.DETA_EMPR);



     STK_ACT_ART_EMP('I',
                      :N.DETA_ART,
                      :N.DETA_CLAVE_DOC,
                      :N.DETA_CANT,
                      :N.DETA_IMP_NETO_LOC,
                      :N.DETA_EMPR,
                      :N.DETA_IMP_NETO_MON);


  IF :N.DETA_NRO_REM IS NOT NULL THEN
        --Actualizar remision
        DECLARE
                v_cli number;
        BEGIN
          select docu_cli
          into v_cli
          from stk_documento
          where docu_clave=:N.deta_clave_doc
          and docu_empr = :N.DETA_EMPR;

          STK_ACT_REM_DET('I', :N.DETA_EMPR, :N.DETA_NRO_REM, :N.DETA_ART, :N.DETA_CANT,v_cli,:N.DETA_EMPR);
       end;
     END IF;
  STK_ACT_STK_EST('I',
  :N.DETA_ART,
  :N.DETA_CLAVE_DOC,
  :N.DETA_CANT,:N.DETA_EMPR);
END IF;
IF DELETING THEN

   IF :O.DETA_CLAVE_OT IS NOT NULL THEN
     DELETE TAL_OT_ARTICULO A WHERE A.TAR_CLAVE_OT = :O.DETA_CLAVE_OT AND A.TAR_EMPR = :O.DETA_EMPR;
   END IF;
    /* Grabar la tabla STK_AUD_DOCUMENTO_DET */
    INSERT INTO STK_AUD_DOCUMENTO_DET
        (ADETA_CLAVE_DOC,ADETA_NRO_ITEM,ADETA_ART,ADETA_EMPR,ADETA_NRO_REM,
         ADETA_CANT,ADETA_IMP_NETO_LOC,ADETA_IMP_NETO_MON,ADETA_IMPU,
   ADETA_IVA_LOC,ADETA_IVA_MON,ADETA_PORC_DTO,ADETA_IMP_BRUTO_LOC,
         ADETA_IMP_BRUTO_MON,ADETA_LOGIN_ANUL,ADETA_FEC_ANUL) VALUES
        (:O.DETA_CLAVE_DOC,:O.DETA_NRO_ITEM,:O.DETA_ART,:O.DETA_EMPR,:O.DETA_NRO_REM,
         :O.DETA_CANT,:O.DETA_IMP_NETO_LOC,:O.DETA_IMP_NETO_MON,:O.DETA_IMPU,
   :O.DETA_IVA_LOC,:O.DETA_IVA_MON,:O.DETA_PORC_DTO,:O.DETA_IMP_BRUTO_LOC,
         :O.DETA_IMP_BRUTO_MON,SUBSTR(USER,1,8),SYSDATE);

     /* actualizar la existencia en la tabla STK_ARTICULO_DEPOSITO */
     STK_ACT_ART_DEP('D', :O.DETA_ART, :O.DETA_CLAVE_DOC, :O.DETA_CANT, :O.DETA_EMPR);

     /* actualizar datos en la tabla STK_ARTICULO_EMPRESA */
     STK_ACT_ART_EMP('D', :O.DETA_ART, :O.DETA_CLAVE_DOC, :O.DETA_CANT,
                      :O.DETA_IMP_NETO_LOC,  :O.DETA_EMPR, :O.DETA_IMP_NETO_MON);

     BEGIN
       UPDATE STK_ART_SERIE SET SER_ESTADO = 'A', SER_SALIDA = NULL
       WHERE SER_SALIDA = :O.DETA_CLAVE_DOC AND SER_ART = :O.DETA_ART AND SER_EMPR = :O.DETA_EMPR;
     END;



     IF :O.DETA_NRO_REM IS NOT NULL THEN
       /* actualizar remision */
        DECLARE
                v_cli number;
        BEGIN
          select docu_cli
          into v_cli
          from stk_documento
          where docu_clave=:O.deta_clave_doc
          and docu_empr = :O.DETA_EMPR;

          STK_ACT_REM_DET_DEL('D', :O.DETA_EMPR, :O.DETA_NRO_REM, :O.DETA_ART, :O.DETA_CANT, V_CLI,:O.DETA_EMPR);
       end;

     END IF;

     /* actualizar las cantidades de venta , Produccion, perdida en la
        tabla STK_ESTADISTICA . Realizado por Luis Fedriani .13-03-1998 */
     STK_ACT_STK_EST('D',:O.DETA_ART, :O.DETA_CLAVE_DOC, :O.DETA_CANT, :O.DETA_EMPR);

  --   STK_ACT_ASI_CNT('D',:O.DETA_ART, :O.DETA_CLAVE_DOC, :O.DETA_NRO_ITEM, :N.DETA_CANT);

  END IF;
END IF;
END;
/
