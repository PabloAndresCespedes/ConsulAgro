create or replace package FINC224 is

-- Author  : PROGRAMACION9
-- Created : 03/06/2021 15:50:05
-- Purpose :
PROCEDURE PP_BUSCAR_CATEGORIA       (O_CATEG_DESC OUT VARCHAR2,
                                     IN_EMPRESA   IN NUMBER);
PROCEDURE PP_BUSCAR_CLIENTE         (O_CLIENTE     OUT VARCHAR2,
                                     IN_CLI_CODIGO IN NUMBER,
                                     IN_EMPRESA    IN NUMBER);
PROCEDURE PP_BUSCAR_PRODUCTOS       (O_PROD_CLAVE OUT NUMBER,
                                     O_PROD_DESC  OUT VARCHAR,
                                     O_PROD_ART   OUT NUMBER,
                                     IN_PROD      IN NUMBER,
                                     IN_EMPRESA   IN NUMBER);
PROCEDURE PP_CARGAR_DOCUMENTOS      (IN_EMPRESA    IN NUMBER,
                                     IN_FEC_INI    IN DATE,
                                     IN_FEC_FIN    IN DATE,
                                     IN_CATEGORIA  IN NUMBER,
                                     IN_SESSION_ID IN NUMBER);
PROCEDURE PP_CARGAR_DOCUMENTOS_PEND (IN_EMPRESA    IN NUMBER,
                                     IN_FEC_INI    IN DATE,
                                     IN_FEC_FIN    IN DATE,
                                     IN_CATEGORIA  IN NUMBER,
                                     IN_SESSION_ID IN NUMBER);
PROCEDURE PP_CARGAR_REMISIONES      (IN_EMPRESA    IN NUMBER,
                                     IN_CLI_CODIGO IN NUMBER,
                                     IN_PROD_ART   IN NUMBER,
                                     IN_SESSION_ID IN NUMBER);
PROCEDURE PP_LLAMAR_REPORTE         (IN_EMPRESA    IN NUMBER,
                                     IN_SESSION_ID IN NUMBER,
                                     IN_FEC_INI    IN DATE,
                                     IN_FEC_FIN    IN DATE,
                                     IN_CATEGORIA  IN NUMBER,
                                     IN_FACTURADO  IN VARCHAR2,
                                     IN_CATE_DESC  IN VARCHAR2,
                                     IN_SUCURSAL   IN NUMBER);
end FINC224;
/
CREATE OR REPLACE PACKAGE BODY FINC224 IS

  PROCEDURE PP_BUSCAR_CATEGORIA(O_CATEG_DESC OUT VARCHAR2,
                                IN_EMPRESA   IN NUMBER) IS
  BEGIN
    SELECT FCAT_DESC
      INTO O_CATEG_DESC
      FROM FAC_CATEGORIA
     WHERE FCAT_CODIGO = nvl(v('P6720_S_CATEGORIA'), 4)
       AND FCAT_EMPR = IN_EMPRESA;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001, 'No esta habilitado');
  END;

  PROCEDURE PP_BUSCAR_CLIENTE(O_CLIENTE     OUT VARCHAR2,
                              IN_CLI_CODIGO IN NUMBER,
                              IN_EMPRESA    IN NUMBER) IS
  BEGIN

    SELECT PNA_NOMBRE
      INTO O_CLIENTE
      FROM FIN_PERSONA
     WHERE PNA_CODIGO = IN_CLI_CODIGO
       AND PNA_EMPR = IN_EMPRESA;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001, 'C?digo de Cliente Inexistente');

  END;

  PROCEDURE PP_BUSCAR_PRODUCTOS(O_PROD_CLAVE OUT NUMBER,
                                O_PROD_DESC  OUT VARCHAR,
                                O_PROD_ART   OUT NUMBER,
                                IN_PROD      IN NUMBER,
                                IN_EMPRESA   IN NUMBER) IS
  BEGIN
    SELECT PROD_CLAVE, PROD_DESC, PROD_STK_ART
      INTO O_PROD_CLAVE, O_PROD_DESC, O_PROD_ART
      FROM ACO_PRODUCTO
     WHERE PROD_CODIGO = IN_PROD
       AND PROD_EMPR = IN_EMPRESA;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001, 'Producto Inexistente');
  END;

  PROCEDURE PP_CARGAR_DOCUMENTOS(IN_EMPRESA    IN NUMBER,
                                 IN_FEC_INI    IN DATE,
                                 IN_FEC_FIN    IN DATE,
                                 IN_CATEGORIA  IN NUMBER,
                                 IN_SESSION_ID IN NUMBER) IS

    CURSOR V_REGISTRO IS
      SELECT DOC_NRO_TRANS_COMBUS,
             DOC_FEC_DOC,
             SUM(DOC_BRUTO_EXEN_LOC + DOC_BRUTO_GRAV_LOC + DOC_IVA_LOC) TOTAL
        FROM FIN_DOCUMENTO
       WHERE DOC_NRO_TRANS_COMBUS IS NOT NULL
         AND DOC_CATEG_COMB = IN_CATEGORIA
         AND DOC_CLAVE_COBRO_COMB IS NOT NULL
         AND DOC_EMPR = IN_EMPRESA
         AND (DOC_FEC_DOC >= IN_FEC_INI OR IN_FEC_INI IS NULL)
         AND (DOC_FEC_DOC <= IN_FEC_FIN OR IN_FEC_FIN IS NULL)
       GROUP BY DOC_NRO_TRANS_COMBUS, DOC_FEC_DOC
       ORDER BY 1 DESC;

  BEGIN
    DELETE FIN_FINC224_TEMP
     WHERE SESSION_ID = IN_SESSION_ID
       AND EMPR = IN_EMPRESA;

    FOR REG IN V_REGISTRO LOOP

      INSERT INTO FIN_FINC224_TEMP
        (NRO, FECHA, CLIENTE, MONTO, CHK_SINO, SESSION_ID, EMPR)
      VALUES
        (REG.DOC_NRO_TRANS_COMBUS,
         REG.DOC_FEC_DOC,
         case when v('P6720_S_CATEGORIA') = 4 then 'HILAGRO' else 'GRUPO EMPRESA' end,
         REG.TOTAL,
         'N',
         IN_SESSION_ID,
         IN_EMPRESA);

    END LOOP;
  END;

  PROCEDURE PP_CARGAR_DOCUMENTOS_PEND(IN_EMPRESA    IN NUMBER,
                                      IN_FEC_INI    IN DATE,
                                      IN_FEC_FIN    IN DATE,
                                      IN_CATEGORIA  IN NUMBER,
                                      IN_SESSION_ID IN NUMBER) IS

    CURSOR V_REGISTRO IS
      SELECT DOC_NRO_TRANS_COMBUS,
             DOC_FEC_DOC,
             SUM(DOC_BRUTO_EXEN_LOC + DOC_BRUTO_GRAV_LOC + DOC_IVA_LOC) TOTAL
        FROM FIN_DOCUMENTO
       WHERE DOC_NRO_TRANS_COMBUS IS NOT NULL
         AND DOC_EMPR = IN_EMPRESA
         AND DOC_CATEG_COMB = IN_CATEGORIA
         AND DOC_CLAVE_COBRO_COMB IS NULL
         AND (DOC_FEC_DOC >= IN_FEC_INI OR IN_FEC_INI IS NULL)
         AND (DOC_FEC_DOC <= IN_FEC_FIN OR IN_FEC_FIN IS NULL)
       GROUP BY DOC_NRO_TRANS_COMBUS, DOC_FEC_DOC
       ORDER BY 1 DESC;

  BEGIN
    DELETE ADCS.FIN_FINC224_TEMP
     WHERE SESSION_ID = IN_SESSION_ID
       AND EMPR = IN_EMPRESA;

    FOR REG IN V_REGISTRO LOOP
      INSERT INTO FIN_FINC224_TEMP
        (NRO, FECHA, CLIENTE, MONTO, CHK_SINO, SESSION_ID, EMPR)
      VALUES
        (REG.DOC_NRO_TRANS_COMBUS,
         REG.DOC_FEC_DOC,
         'HILAGRO',
         REG.TOTAL,
         'N',
         IN_SESSION_ID,
         IN_EMPRESA);
    END LOOP;
  END;

  PROCEDURE PP_CARGAR_REMISIONES(IN_EMPRESA    IN NUMBER,
                                 IN_CLI_CODIGO IN NUMBER,
                                 IN_PROD_ART   IN NUMBER,
                                 IN_SESSION_ID IN NUMBER) IS
    CURSOR V_REGISTRO IS
      SELECT DETR_REM,
             DOC_NRO_DOC,
             ROUND(DETR_CANT_REM, 0) DETR_CANT_REM,
             (DETR_CANT_REM - DETR_CANT_FACT) PEND_FACT,
             DOCR_CANT
        FROM STK_REMISION_DET,
             FIN_DOC_REMISION,
             STK_REMISION,
             FIN_DOCUMENTO
       WHERE DETR_CLAVE_REM = DOCR_CLAVE_REM(+)
         AND DETR_EMPR = DOCR_CEMPR(+)
         AND DETR_CLAVE_REM = REM_CLAVE
         AND DETR_EMPR = REM_EMPR
         AND DOCR_CLAVE_DOC = DOC_CLAVE(+)
         AND DOCR_CEMPR = DOC_EMPR(+)
         AND DETR_EMPR = IN_EMPRESA
         AND (REM_CLI = IN_CLI_CODIGO OR IN_CLI_CODIGO IS NULL)
         AND (DETR_ART = IN_PROD_ART OR IN_PROD_ART IS NULL)
       ORDER BY DOC_NRO_DOC;
  BEGIN

    FOR REG IN V_REGISTRO LOOP

      INSERT INTO FIN_FINC224_TEMP_1
        (REM_DOC_NRO,
         REM_NRO_REM,
         REM_CANT,
         REM_FACT,
         REM_DOC_CANT,
         SESSION_ID,
         EMPR)
      VALUES
        (REG.DOC_NRO_DOC,
         REG.DETR_REM,
         REG.DETR_CANT_REM,
         REG.PEND_FACT,
         REG.DOCR_CANT,
         IN_SESSION_ID,
         IN_EMPRESA);

    END LOOP;
  END;

  PROCEDURE PP_LLAMAR_REPORTE(IN_EMPRESA    IN NUMBER,
                              IN_SESSION_ID IN NUMBER,
                              IN_FEC_INI    IN DATE,
                              IN_FEC_FIN    IN DATE,
                              IN_CATEGORIA  IN NUMBER,
                              IN_FACTURADO  IN VARCHAR2,
                              IN_CATE_DESC  IN VARCHAR2,
                              IN_SUCURSAL   IN NUMBER) IS
    V_PARAMETROS    VARCHAR2(32767);
    V_IDENTIFICADOR VARCHAR2(2) := '&'; --SE UTILIZA PARA QUE AL COMPILAR NO LO TOME COMO PARAMETROS
    V_EMPR_DESC     VARCHAR2(50);
    V_DESC_SUCURSAL VARCHAR2(100);
  BEGIN

    BEGIN
      SELECT A.EMPR_RAZON_SOCIAL
        INTO V_EMPR_DESC
        FROM GEN_EMPRESA A
       WHERE A.EMPR_CODIGO = IN_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN

        RAISE_APPLICATION_ERROR(-20016, 'Empresa no valida!');
    END;

    BEGIN
      SELECT SUC_DESC
        INTO V_DESC_SUCURSAL
        FROM GEN_SUCURSAL
       WHERE SUC_CODIGO = IN_SUCURSAL
         AND SUC_EMPR = IN_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;

    V_PARAMETROS := 'P_FORMATO=' || URL_ENCODE('pdf');
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_SESSION_ID=' ||
                    URL_ENCODE(IN_SESSION_ID);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_EMPRESA=' ||
                    URL_ENCODE(IN_EMPRESA);

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_DESCRIP_EMPR=' ||
                    URL_ENCODE(V_EMPR_DESC);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_PROGRAMA=' ||
                    URL_ENCODE('FINC224');
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_SUCURSAL=' ||
                    URL_ENCODE(V_DESC_SUCURSAL);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_USUARIO=' ||
                    URL_ENCODE(GEN_DEVUELVE_USER);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_PROG_DESC=' ||
                    URL_ENCODE(V_EMPR_DESC||' - ESTADO DE COBRO DE COMBUSTIBLE HILAGRO');

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_FEC_INI=' ||
                    URL_ENCODE(IN_FEC_INI);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_FEC_FIN=' ||
                    URL_ENCODE(IN_FEC_FIN);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_CATEGORIA=' ||
                    URL_ENCODE(IN_CATEGORIA);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_FACTURADO=' ||
                    URL_ENCODE(IN_FACTURADO);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_CATE_DESC=' ||
                    URL_ENCODE(IN_CATE_DESC);

    DELETE FROM GEN_PARAMETROS_REPORT WHERE USUARIO = GEN_DEVUELVE_USER;
    INSERT INTO GEN_PARAMETROS_REPORT
      (PARAMETROS, USUARIO, NOMBRE_REPORTE, FORMATO_SALIDA)
    VALUES
      (V_PARAMETROS, GEN_DEVUELVE_USER, 'FINC224', 'pdf');
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001, SQLERRM);

  END;
END FINC224;
/
