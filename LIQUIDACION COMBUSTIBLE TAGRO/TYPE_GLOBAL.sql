CREATE OR REPLACE TYPE "T_PER_EMPLEADO_GRUPO" as object(
  cod_empl  number,
  cod_prov  number,
  cod_empr  number
);

CREATE OR REPLACE TYPE "T_PER_EMPLEADO_GRUPO_T" IS TABLE OF T_PER_EMPLEADO_GRUPO;
