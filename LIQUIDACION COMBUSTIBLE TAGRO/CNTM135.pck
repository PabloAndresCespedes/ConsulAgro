create or replace package CNTM135 is

  -- Author  : @PabloACespedes
  -- Created : 01/09/2022 8:38:15
  -- Purpose : Configuracion de cuentas contables para uso en servicios de transagro
  
  -- Public function and procedure declarations
  function existe_registro(
    in_empresa       in gen_empresa.empr_codigo%type,
    in_empresa_grupo in gen_empresa.empr_codigo%type,
    in_cuenta        in cnt_cuenta.ctac_clave%type
  )return boolean;
  
  -- 01/09/2022 8:46:07 @PabloACespedes \(^-^)/
  -- retorna la cuenta contable de la empresa del grupo,
  -- que utilizara en la empresa parada
  function obt_cuenta_de_empresa(
    in_empresa       in gen_empresa.empr_codigo%type,
    in_empresa_grupo in gen_empresa.empr_codigo%type
  )return cnt_conf_cuenta_combustible.cuenta_id%type;

end CNTM135;
/
create or replace package body CNTM135 is

  function existe_registro(
    in_empresa       in gen_empresa.empr_codigo%type,
    in_empresa_grupo in gen_empresa.empr_codigo%type,
    in_cuenta        in cnt_cuenta.ctac_clave%type
  )return boolean is
   l_dummy number;
  begin
    select distinct 1 into l_dummy
    from cnt_conf_cuenta_combustible c
    where c.empresa_id = in_empresa
    and   c.empresa_grupo_id = in_empresa_grupo
    and   c.cuenta_id        = in_cuenta;
    
    return true;
    
  exception
    when no_data_found then
      return false;
  end existe_registro;
  
  function obt_cuenta_de_empresa(
    in_empresa       in gen_empresa.empr_codigo%type,
    in_empresa_grupo in gen_empresa.empr_codigo%type
  )return cnt_conf_cuenta_combustible.cuenta_id%type is
   l_cnt cnt_conf_cuenta_combustible.cuenta_id%type;
  begin
    select c.cuenta_id
    into l_cnt
    from cnt_conf_cuenta_combustible c
    where c.empresa_id = in_empresa
    and   c.empresa_grupo_id = in_empresa_grupo
    and   rownum = 1;
    
    return l_cnt;
  exception
    when no_data_found then
      Raise_application_error(-20000, 'Necesita configurar la cuenta contable de la empresa. Comuniquese con CONTABILIDAD (Programa 7-1-135)');
  end obt_cuenta_de_empresa;
  
end CNTM135;
/
