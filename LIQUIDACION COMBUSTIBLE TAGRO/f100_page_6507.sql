prompt --application/set_environment
set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_200100 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2020.03.31'
,p_release=>'20.1.0.00.13'
,p_default_workspace_id=>2933390818164287
,p_default_application_id=>100
,p_default_id_offset=>102801135605236656
,p_default_owner=>'ADCS'
);
end;
/
 
prompt APPLICATION 100 - FINANZAS
--
-- Application Export:
--   Application:     100
--   Name:            FINANZAS
--   Date and Time:   17:09 Monday August 22, 2022
--   Exported By:     PABLOC
--   Flashback:       0
--   Export Type:     Page Export
--   Manifest
--     PAGE: 6507
--   Manifest End
--   Version:         20.1.0.00.13
--   Instance ID:     211687171918188
--

begin
null;
end;
/
prompt --application/pages/delete_06507
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>6507);
end;
/
prompt --application/pages/page_06507
begin
wwv_flow_api.create_page(
 p_id=>6507
,p_user_interface_id=>wwv_flow_api.id(116263539932678213)
,p_name=>'FINM300 - PERSONA'
,p_alias=>'FINM300-PERSONA'
,p_step_title=>'FINM300 - PERSONA'
,p_autocomplete_on_off=>'OFF'
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#APP_IMAGES#jquery-1.0.magic1.js',
'#WORKSPACE_IMAGES#sweetalert2.min.js'))
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*$( ".no_null" ).focusout(function() {',
'   // alert("El elemento no puede ser nulo!");',
'    if($(this).val()==''''){',
'            $.event.trigger(''alerta'');',
'    //alert("Los elementos son obligatorio!");',
'         //this.focus();',
'}',
'  })',
'',
'',
'$( ".obl" ).focusout(function() {',
'    if($(this).val()==''''){',
'    //alert("Los elementos de color rojo no pueden ser nulos, favor rellenar los campos!");',
'       ',
'        $.event.trigger(''alerta'');',
'         //this.focus();',
'}',
'  })',
'',
'',
'$(document).ready(function( event ) {    ',
'    if ($(''#P6507_LOGIN'').val() === "MAGDAQ" || $(''#P6507_LOGIN'').val() === "ADCS" || $(''#P6507_LOGIN'').val() === "JUANG"){',
'      window.parent.document.body.style.zoom="60%"',
'    }',
'});*/',
'   ',
'$(document).keyup(function( event ) {',
'  if (event.keyCode == 119) {',
'    openModal(''CLI_HIL'');  ',
'  }',
' });'))
,p_inline_css=>wwv_flow_string.join(wwv_flow_t_varchar2(
'.color {color:#ED134E} ',
'',
'.t-Form-inputContainer span.display_only {',
'    background-color: #ffffff;',
'}',
'',
'.a-GV-table th.a-GV-header, .a-GV-table th.a-GV-headerGroup {',
'    background-color: #cdd7e2; /*#a0c9f3;*/',
'    border-color: #000000;',
'}',
'',
'.a-IG-controlsContainer {',
'    display:none;',
'}',
'',
'/*.t-Region-body {',
'    background-color: #f5f5f5;',
'}',
'*/',
'/*.a-Button--hot, .t-Button--hot:not(.t-Button--simple), body .ui-button.ui-button--hot, body .ui-state-default.ui-priority-primary {',
'    background-color: #717171;',
'    color: #ffffff;',
'}*/',
'',
'',
'',
'',
'',
'.t-Form--large .apex-item-select, .t-Form--large .apex-item-text, .t-Form--large .apex-item-textarea, .t-Form-fieldContainer--large .apex-item-select, .t-Form-fieldContainer--large .apex-item-text, .t-Form-fieldContainer--large .apex-item-textarea {',
'    font-size: 8 px;',
'    padding: .7rem;',
'}'))
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'PABLOC'
,p_last_upd_yyyymmddhh24miss=>'20220822170622'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(51442890062132110)
,p_plug_name=>'CONTENEDOR'
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--accent7:t-Region--noBorder:t-Region--scrollBody:t-Form--noPadding:t-Form--large'
,p_plug_template=>wwv_flow_api.id(119469003339420663)
,p_plug_display_sequence=>20
,p_plug_display_column=>1
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(51544476917822746)
,p_plug_name=>'FIN_PERSONA'
,p_parent_plug_id=>wwv_flow_api.id(51442890062132110)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(119469003339420663)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_header=>'<B>Persona</B>'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(51442715326132109)
,p_plug_name=>'cuerpo'
,p_parent_plug_id=>wwv_flow_api.id(51544476917822746)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--noBorder:t-Region--scrollBody:t-Form--noPadding'
,p_plug_template=>wwv_flow_api.id(119469003339420663)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(51544175685822743)
,p_plug_name=>'cuerpo1'
,p_parent_plug_id=>wwv_flow_api.id(51544476917822746)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--noBorder:t-Region--scrollBody:t-Form--noPadding'
,p_plug_template=>wwv_flow_api.id(119469003339420663)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(52793524327525812)
,p_plug_name=>'BPIE'
,p_parent_plug_id=>wwv_flow_api.id(51442890062132110)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--noBorder:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(119469003339420663)
,p_plug_display_sequence=>40
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(53085895487906733)
,p_plug_name=>'PRES'
,p_parent_plug_id=>wwv_flow_api.id(51442890062132110)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(119469003339420663)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_plug_source_type=>'NATIVE_DISPLAY_SELECTOR'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'STANDARD'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(53085917162906734)
,p_plug_name=>'General'
,p_parent_plug_id=>wwv_flow_api.id(53085895487906733)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--noBorder:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(119469003339420663)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(51544578805822747)
,p_plug_name=>'BPROV'
,p_parent_plug_id=>wwv_flow_api.id(53085917162906734)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--scrollBody:t-Form--noPadding'
,p_plug_template=>wwv_flow_api.id(119469003339420663)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_header=>'<B>Proveedor</B>'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(52793250963525809)
,p_plug_name=>'BCLIE'
,p_parent_plug_id=>wwv_flow_api.id(53085917162906734)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(119469003339420663)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_header=>'<B>Cliente</B>'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(51903516165561243)
,p_plug_name=>'BCLIE_CUERPO'
,p_parent_plug_id=>wwv_flow_api.id(52793250963525809)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--noBorder:t-Region--scrollBody:t-Form--noPadding'
,p_plug_template=>wwv_flow_api.id(119469003339420663)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(51975914225655949)
,p_plug_name=>'FIN_CLI_CTA_BANC'
,p_region_name=>'cabe'
,p_parent_plug_id=>wwv_flow_api.id(52793250963525809)
,p_region_template_options=>'#DEFAULT#:t-IRR-region--noBorders'
,p_plug_template=>wwv_flow_api.id(119467875788420662)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT N001 CTA_CLI,',
'       N002 CTA_BCO,',
'       C001 BCO_DESC,',
'       C007 CTA_NRO,',
'       C002 CTA_TITULAR,',
'       C003 CTA_TITULAR_CI,',
'       N004 CTA_EMPR,',
'       C004 CTA_MON,',
'       C005 MON_SIMBOLO,',
'       C006 CTA_PREDET,',
'       SEQ_ID,',
'       ''<span class="fa fa-trash" aria-hidden="true"></span>'' borrar',
'  FROM APEX_COLLECTIONS',
' WHERE COLLECTION_NAME = ''FIN_CLI_CTA_BCO''',
''))
,p_plug_source_type=>'NATIVE_IG'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_prn_units=>'MILLIMETERS'
,p_prn_paper_size=>'A4'
,p_prn_width=>297
,p_prn_height=>210
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header=>'FIN_CLI_CTA_BANC'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#EEEEEE'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'bold'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#FFFFFF'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_prn_border_color=>'#666666'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(52793926231525816)
,p_name=>'CTA_CLI'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'CTA_CLI'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Cta Cli'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>30
,p_value_alignment=>'RIGHT'
,p_attribute_03=>'right'
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(52794088954525817)
,p_name=>'CTA_BCO'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'CTA_BCO'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_POPUP_LOV'
,p_heading=>'Bco'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>40
,p_value_alignment=>'LEFT'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_07=>'Cuenta Banco'
,p_attribute_08=>'500'
,p_attribute_09=>'500'
,p_is_required=>false
,p_lov_type=>'SQL_QUERY'
,p_lov_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT BCO_CODIGO||'' - ''||BCO_DESC, BCO_CODIGO',
'  FROM FIN_BANCO',
' WHERE BCO_EMPR = :P_EMPRESA',
' ORDER BY BCO_CODIGO',
''))
,p_lov_display_extra=>true
,p_lov_display_null=>true
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'LOV'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(52794171193525818)
,p_name=>'CTA_NRO'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'CTA_NRO'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Nro'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>50
,p_value_alignment=>'RIGHT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(52794273264525819)
,p_name=>'CTA_TITULAR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'CTA_TITULAR'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Titular'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>60
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(52794345853525820)
,p_name=>'CTA_TITULAR_CI'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'CTA_TITULAR_CI'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Nro Doc.Titular'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>70
,p_value_alignment=>'RIGHT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(52794425188525821)
,p_name=>'CTA_EMPR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'CTA_EMPR'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Cta Empr'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>80
,p_value_alignment=>'RIGHT'
,p_attribute_03=>'right'
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(52794537610525822)
,p_name=>'CTA_MON'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'CTA_MON'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_POPUP_LOV'
,p_heading=>'Moneda'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>90
,p_value_alignment=>'LEFT'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_07=>'MONEDAS'
,p_attribute_08=>'500'
,p_attribute_09=>'500'
,p_is_required=>false
,p_max_length=>4000
,p_lov_type=>'SQL_QUERY'
,p_lov_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT T.MON_CODIGO || '' - '' || MON_DESC, MON_CODIGO',
'  FROM GEN_MONEDA T',
' WHERE T.MON_EMPR = :P_EMPRESA',
'ORDER BY MON_CODIGO'))
,p_lov_display_extra=>true
,p_lov_display_null=>true
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'LOV'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(52794638119525823)
,p_name=>'CTA_PREDET'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'CTA_PREDET'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_YES_NO'
,p_heading=>'Predet.'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>100
,p_value_alignment=>'CENTER'
,p_attribute_01=>'CUSTOM'
,p_attribute_02=>'S'
,p_attribute_03=>'Si'
,p_attribute_04=>'N'
,p_attribute_05=>'No'
,p_is_required=>false
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(52794768525525824)
,p_name=>'APEX$ROW_ACTION'
,p_item_type=>'NATIVE_ROW_ACTION'
,p_display_sequence=>20
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(52794897375525825)
,p_name=>'APEX$ROW_SELECTOR'
,p_item_type=>'NATIVE_ROW_SELECTOR'
,p_display_sequence=>10
,p_attribute_01=>'Y'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(52795453307525831)
,p_name=>'BCO_DESC'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'BCO_DESC'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>120
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_item_attributes=>'readonly style=''background:#DEE3E5;'''
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(52795559610525832)
,p_name=>'MON_SIMBOLO'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'MON_SIMBOLO'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>130
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_item_attributes=>'readonly style=''background:#DEE3E5;'''
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53085782057906732)
,p_name=>'SEQ_ID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'SEQ_ID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Seq Id'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>140
,p_value_alignment=>'RIGHT'
,p_attribute_03=>'right'
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>true
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(54072652687084806)
,p_name=>'BORRAR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'BORRAR'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_LINK'
,p_heading=>'Borrar'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>150
,p_value_alignment=>'LEFT'
,p_link_target=>'javascript:$s(''P6507_SEQ_ID_BCO'',''&SEQ_ID.'');'
,p_link_text=>'&BORRAR.'
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
,p_escape_on_http_output=>false
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(52793862431525815)
,p_internal_uid=>52793862431525815
,p_is_editable=>true
,p_edit_operations=>'i:u:d'
,p_lost_update_check_type=>'VALUES'
,p_add_row_if_empty=>true
,p_submit_checked_rows=>false
,p_lazy_loading=>false
,p_requires_filter=>false
,p_select_first_row=>true
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>true
,p_toolbar_buttons=>'ACTIONS_MENU'
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_flashback=>true
,p_define_chart_view=>true
,p_enable_download=>true
,p_enable_mail_download=>true
,p_fixed_header=>'REGION'
,p_fixed_header_max_height=>300
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(52944197999042941)
,p_interactive_grid_id=>wwv_flow_api.id(52793862431525815)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(52944209202042941)
,p_report_id=>wwv_flow_api.id(52944197999042941)
,p_view_type=>'GRID'
,p_stretch_columns=>true
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(6505413058730)
,p_view_id=>wwv_flow_api.id(52944209202042941)
,p_display_seq=>0
,p_column_id=>wwv_flow_api.id(52794768525525824)
,p_is_visible=>true
,p_is_frozen=>true
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(31289253194430)
,p_view_id=>wwv_flow_api.id(52944209202042941)
,p_display_seq=>3
,p_column_id=>wwv_flow_api.id(52795453307525831)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>190
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(36701244226241)
,p_view_id=>wwv_flow_api.id(52944209202042941)
,p_display_seq=>5
,p_column_id=>wwv_flow_api.id(52795559610525832)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>82
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(52944706167042945)
,p_view_id=>wwv_flow_api.id(52944209202042941)
,p_display_seq=>1
,p_column_id=>wwv_flow_api.id(52793926231525816)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(52945203868042948)
,p_view_id=>wwv_flow_api.id(52944209202042941)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(52794088954525817)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>71
,p_sort_order=>1
,p_sort_direction=>'ASC'
,p_sort_nulls=>'LAST'
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(52945701804042950)
,p_view_id=>wwv_flow_api.id(52944209202042941)
,p_display_seq=>6
,p_column_id=>wwv_flow_api.id(52794171193525818)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>133
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(52946275385042952)
,p_view_id=>wwv_flow_api.id(52944209202042941)
,p_display_seq=>7
,p_column_id=>wwv_flow_api.id(52794273264525819)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>79
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(52946713385042954)
,p_view_id=>wwv_flow_api.id(52944209202042941)
,p_display_seq=>8
,p_column_id=>wwv_flow_api.id(52794345853525820)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>122
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(52947274048042956)
,p_view_id=>wwv_flow_api.id(52944209202042941)
,p_display_seq=>9
,p_column_id=>wwv_flow_api.id(52794425188525821)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(52947732822042958)
,p_view_id=>wwv_flow_api.id(52944209202042941)
,p_display_seq=>4
,p_column_id=>wwv_flow_api.id(52794537610525822)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>79
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(52948224337042959)
,p_view_id=>wwv_flow_api.id(52944209202042941)
,p_display_seq=>10
,p_column_id=>wwv_flow_api.id(52794638119525823)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>99
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53238116499097902)
,p_view_id=>wwv_flow_api.id(52944209202042941)
,p_display_seq=>11
,p_column_id=>wwv_flow_api.id(53085782057906732)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(54204208543754133)
,p_view_id=>wwv_flow_api.id(52944209202042941)
,p_display_seq=>12
,p_column_id=>wwv_flow_api.id(54072652687084806)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>63
);
wwv_flow_api.create_ig_report_highlight(
 p_id=>wwv_flow_api.id(95087000003)
,p_view_id=>wwv_flow_api.id(52944209202042941)
,p_execution_seq=>5
,p_name=>'moneda'
,p_column_id=>wwv_flow_api.id(52795559610525832)
,p_background_color=>'#E0E0E0'
,p_condition_type=>'COLUMN'
,p_condition_column_id=>wwv_flow_api.id(52795559610525832)
,p_condition_operator=>'NN'
,p_condition_is_case_sensitive=>false
,p_is_enabled=>true
);
wwv_flow_api.create_ig_report_highlight(
 p_id=>wwv_flow_api.id(243514000003)
,p_view_id=>wwv_flow_api.id(52944209202042941)
,p_execution_seq=>10
,p_name=>'bco'
,p_column_id=>wwv_flow_api.id(52795453307525831)
,p_background_color=>'#E0E0E0'
,p_condition_type=>'COLUMN'
,p_condition_column_id=>wwv_flow_api.id(52795453307525831)
,p_condition_operator=>'NN'
,p_condition_is_case_sensitive=>false
,p_is_enabled=>true
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(53086100872906736)
,p_plug_name=>'Lotes'
,p_parent_plug_id=>wwv_flow_api.id(53085895487906733)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--noBorder:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(119469003339420663)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(53086205160906737)
,p_plug_name=>'BAREA'
,p_parent_plug_id=>wwv_flow_api.id(53086100872906736)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_region_attributes=>'readonly style=''background:#DEE3E5;'''
,p_plug_template=>wwv_flow_api.id(119467875788420662)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT N001 LOTE_PERSONA,',
'       N002 LOTE_NUMERO,',
'       C001 LOTE_LOCALIDAD,',
'       C002 LOC_DESC,',
'       C003 LOTE_OBS_LOC,',
'       N003 LOTE_TAM_HAS,',
'       C004 LOTE_STATUS,',
'       C005 LOTE_STATUS_DESC,',
'       C006 LOTE_IND_HIPOTECA,',
'       C007 LOTE_NOM_HIPOTECA,',
'       C008 LOTE_TIPO_ALQUILER,',
'       C017 TIPO_DESC,',
'       C009 LOTE_CALIDAD_SUELO,',
'       C010 CAL_DESC,',
'       TO_NUMBER(C011) LOTE_HAS_SOJA,',
'       TO_NUMBER(C012) LOTE_HAS_SOJA_Z,',
'       TO_NUMBER(C013) LOTE_HAS_MAIZ,',
'       TO_NUMBER(C014) LOTE_HAS_TRIGO,',
'       C015 LOTE_LUGAR_ORIGEN,',
'       C016 LOTE_EMPR,',
'       SEQ_ID,',
'       C018 DIST_DESC,',
'       C019 BORRAR,',
'       C020 VALOR',
'  FROM APEX_COLLECTIONS',
' WHERE COLLECTION_NAME = ''AREA''',
'UNION ALL',
'',
'SELECT NULL LOTE_PERSONA,',
'       MAX(N002) LOTE_NUMERO,',
'       ''Distrito:'' LOTE_LOCALIDAD,',
'       C018 LOC_DESC,',
'       ''Hectareas:'' LOTE_OBS_LOC,',
'       SUM(N003) LOTE_TAM_HAS,',
'       NULL LOTE_STATUS,',
'       NULL LOTE_STATUS_DESC,',
'       NULL LOTE_IND_HIPOTECA,',
'       NULL LOTE_NOM_HIPOTECA,',
'       NULL LOTE_TIPO_ALQUILER,',
'       NULL TIPO_DESC,',
'       NULL LOTE_CALIDAD_SUELO,',
'       ''Totales'' CAL_DESC,',
'       SUM(TO_NUMBER(C011)) LOTE_HAS_SOJA,',
'       SUM(TO_NUMBER(C012)) LOTE_HAS_SOJA_Z,',
'       SUM(TO_NUMBER(C013)) LOTE_HAS_MAIZ,',
'       SUM(TO_NUMBER(C014)) LOTE_HAS_TRIGO,',
'       NULL LOTE_LUGAR_ORIGEN,',
'       NULL,',
'       NULL,',
'       NULL DIST_DESC,',
'       NULL BORRAR,',
'       NULL',
'  FROM APEX_COLLECTIONS',
' WHERE COLLECTION_NAME = ''AREA''',
'   AND C018 IS NOT NULL',
'GROUP BY C018'))
,p_plug_source_type=>'NATIVE_IG'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_prn_units=>'MILLIMETERS'
,p_prn_paper_size=>'A4'
,p_prn_width=>297
,p_prn_height=>210
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header=>'BAREA'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#EEEEEE'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'bold'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#FFFFFF'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_prn_border_color=>'#666666'
);
wwv_flow_api.create_region_column_group(
 p_id=>wwv_flow_api.id(53508256752059111)
,p_heading=>'POTENCIAL DE PRODUCCION(EN TON)'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53086485654906739)
,p_name=>'LOTE_PERSONA'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'LOTE_PERSONA'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Lote Persona'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>30
,p_value_alignment=>'RIGHT'
,p_attribute_03=>'right'
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53086550282906740)
,p_name=>'LOTE_NUMERO'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'LOTE_NUMERO'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'N.Lote'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>40
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_item_attributes=>'readonly style=''background:#DEE3E5;'''
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53086626137906741)
,p_name=>'LOTE_LOCALIDAD'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'LOTE_LOCALIDAD'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_POPUP_LOV'
,p_heading=>'Localidad'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>50
,p_value_alignment=>'LEFT'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_07=>'LOCALIDADES'
,p_attribute_08=>'500'
,p_attribute_09=>'500'
,p_is_required=>false
,p_max_length=>4000
,p_lov_type=>'SQL_QUERY'
,p_lov_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT LO.LOC_DESC||'' - ''||UPPER(DI.DIST_DESC) DIST_DESC, LO.LOC_CODIGO',
'  FROM GEN_DISTRITO  DI, ',
'       GEN_LOCALIDAD LO',
' WHERE DI.DIST_CODIGO = LO.LOC_DISTRITO',
'AND DI.DIST_EMPR = LO.LOC_EMPR',
'AND LOC_EMPR = :P_EMPRESA',
' ORDER BY 1  '))
,p_lov_display_extra=>true
,p_lov_display_null=>true
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'LOV'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53086737572906742)
,p_name=>'LOC_DESC'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'LOC_DESC'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>60
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_item_attributes=>'readonly style=''background:#DEE3E5;'''
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
end;
/
begin
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53086866048906743)
,p_name=>'LOTE_OBS_LOC'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'LOTE_OBS_LOC'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Obs.Localidad'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>70
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53086943116906744)
,p_name=>'LOTE_TAM_HAS'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'LOTE_TAM_HAS'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Hectareas'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>80
,p_value_alignment=>'RIGHT'
,p_attribute_03=>'right'
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53087019381906745)
,p_name=>'LOTE_STATUS'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'LOTE_STATUS'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_POPUP_LOV'
,p_heading=>'Status'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>90
,p_value_alignment=>'LEFT'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_07=>'STATUS'
,p_attribute_08=>'500'
,p_attribute_09=>'500'
,p_is_required=>false
,p_max_length=>4000
,p_lov_type=>'STATIC'
,p_lov_source=>'STATIC:ALQUILADO;A,PROPIO;P'
,p_lov_display_extra=>true
,p_lov_display_null=>true
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'LOV'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_default_type=>'STATIC'
,p_default_expression=>'P'
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53087168606906746)
,p_name=>'LOTE_STATUS_DESC'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'LOTE_STATUS_DESC'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>100
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_item_attributes=>'readonly style=''background:#DEE3E5;'''
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53087296733906747)
,p_name=>'LOTE_IND_HIPOTECA'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'LOTE_IND_HIPOTECA'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Hipoteca'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>110
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_default_type=>'STATIC'
,p_default_expression=>'N'
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53087371002906748)
,p_name=>'LOTE_NOM_HIPOTECA'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'LOTE_NOM_HIPOTECA'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Nombre Hipoteca'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>120
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_item_attributes=>'readonly style=''background:#DEE3E5;'''
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53087444015906749)
,p_name=>'LOTE_TIPO_ALQUILER'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'LOTE_TIPO_ALQUILER'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_POPUP_LOV'
,p_heading=>'Tipo Alquiler'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>130
,p_value_alignment=>'LEFT'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_07=>'TIPO DE ALQUILER'
,p_attribute_08=>'500'
,p_attribute_09=>'500'
,p_is_required=>false
,p_max_length=>4000
,p_lov_type=>'SQL_QUERY'
,p_lov_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT TIPO_DESC, TIPO_CODIGO',
'  FROM FIN_TIPO_ALQ_PROD',
' WHERE TIPO_EMPR = :P_EMPRESA',
' ORDER BY 1',
''))
,p_lov_display_extra=>true
,p_lov_display_null=>true
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'LOV'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53087553701906750)
,p_name=>'LOTE_CALIDAD_SUELO'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'LOTE_CALIDAD_SUELO'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_POPUP_LOV'
,p_heading=>'Calidad Suelo'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>140
,p_value_alignment=>'LEFT'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_07=>'CALIDADES DEL SUELO '
,p_attribute_08=>'500'
,p_attribute_09=>'500'
,p_is_required=>false
,p_max_length=>4000
,p_lov_type=>'SQL_QUERY'
,p_lov_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CAL_DESC, CAL_CODIGO',
'  FROM ACO_CALIDAD_SUELO',
' WHERE CAL_EMPR = :P_EMPRESA',
' ORDER BY 1',
''))
,p_lov_display_extra=>true
,p_lov_display_null=>true
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'LOV'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53507290649059101)
,p_name=>'CAL_DESC'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'CAL_DESC'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>150
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_item_attributes=>'readonly style=''background:#DEE3E5;'''
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53507314809059102)
,p_name=>'LOTE_HAS_SOJA'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'LOTE_HAS_SOJA'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'SOJA'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>160
,p_value_alignment=>'RIGHT'
,p_group_id=>wwv_flow_api.id(53508256752059111)
,p_use_group_for=>'BOTH'
,p_attribute_05=>'BOTH'
,p_format_mask=>'999G999G999G999G990D00'
,p_item_attributes=>'readonly style=''background:#DEE3E5;'''
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53507490591059103)
,p_name=>'LOTE_HAS_SOJA_Z'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'LOTE_HAS_SOJA_Z'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'SOJA Z.'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>170
,p_value_alignment=>'RIGHT'
,p_group_id=>wwv_flow_api.id(53508256752059111)
,p_use_group_for=>'BOTH'
,p_attribute_05=>'BOTH'
,p_format_mask=>'999G999G999G999G990D00'
,p_item_attributes=>'readonly style=''background:#DEE3E5;'''
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53507579042059104)
,p_name=>'LOTE_HAS_MAIZ'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'LOTE_HAS_MAIZ'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'MAIZ'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>180
,p_value_alignment=>'RIGHT'
,p_group_id=>wwv_flow_api.id(53508256752059111)
,p_use_group_for=>'BOTH'
,p_attribute_05=>'BOTH'
,p_format_mask=>'999G999G999G999G990D00'
,p_item_attributes=>'readonly style=''background:#DEE3E5;'''
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53507697042059105)
,p_name=>'LOTE_HAS_TRIGO'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'LOTE_HAS_TRIGO'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'TRIGO'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>190
,p_value_alignment=>'RIGHT'
,p_group_id=>wwv_flow_api.id(53508256752059111)
,p_use_group_for=>'BOTH'
,p_attribute_05=>'BOTH'
,p_format_mask=>'999G999G999G999G990D00'
,p_item_attributes=>'readonly style=''background:#DEE3E5;'''
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53507703721059106)
,p_name=>'LOTE_LUGAR_ORIGEN'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'LOTE_LUGAR_ORIGEN'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Lote Lugar Origen'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>200
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53507873020059107)
,p_name=>'LOTE_EMPR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'LOTE_EMPR'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Lote Empr'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>210
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53508387720059112)
,p_name=>'TIPO_DESC'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'TIPO_DESC'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>220
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_item_attributes=>'readonly style=''background:#DEE3E5;'''
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53508440916059113)
,p_name=>'APEX$ROW_ACTION'
,p_item_type=>'NATIVE_ROW_ACTION'
,p_display_sequence=>20
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53508520174059114)
,p_name=>'APEX$ROW_SELECTOR'
,p_item_type=>'NATIVE_ROW_SELECTOR'
,p_display_sequence=>10
,p_attribute_01=>'Y'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53508751272059116)
,p_name=>'SEQ_ID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'SEQ_ID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>230
,p_attribute_01=>'N'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_is_primary_key=>true
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(53509669791059125)
,p_name=>'DIST_DESC'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DIST_DESC'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Dist Desc'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>240
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_item_attributes=>'readonly'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(54076128631084841)
,p_name=>'BORRAR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'BORRAR'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_LINK'
,p_heading=>'Borrar'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>250
,p_value_alignment=>'LEFT'
,p_link_target=>'javascript:$s(''P6507_SEQ_AREA'',''&SEQ_ID.'');'
,p_link_text=>'&BORRAR.'
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
,p_escape_on_http_output=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(450826839287401106)
,p_name=>'VALOR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'VALOR'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Valor'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>260
,p_value_alignment=>'RIGHT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_control_break=>true
,p_enable_hide=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(53086329504906738)
,p_internal_uid=>53086329504906738
,p_is_editable=>true
,p_edit_operations=>'i:u:d'
,p_lost_update_check_type=>'VALUES'
,p_add_row_if_empty=>true
,p_submit_checked_rows=>false
,p_lazy_loading=>false
,p_requires_filter=>false
,p_select_first_row=>true
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>true
,p_toolbar_buttons=>'ACTIONS_MENU'
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_flashback=>true
,p_define_chart_view=>true
,p_enable_download=>true
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(53513710031059324)
,p_interactive_grid_id=>wwv_flow_api.id(53086329504906738)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>false
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(53513892146059325)
,p_report_id=>wwv_flow_api.id(53513710031059324)
,p_view_type=>'GRID'
,p_stretch_columns=>true
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(29451692093697)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>14
,p_column_id=>wwv_flow_api.id(53508387720059112)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53514321063059327)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>0
,p_column_id=>wwv_flow_api.id(53086485654906739)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53514814296059329)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>3
,p_column_id=>wwv_flow_api.id(53086550282906740)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>75
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53515359831059330)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>4
,p_column_id=>wwv_flow_api.id(53086626137906741)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53515817071059332)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>5
,p_column_id=>wwv_flow_api.id(53086737572906742)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53516389878059334)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>6
,p_column_id=>wwv_flow_api.id(53086866048906743)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53516872214059336)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>7
,p_column_id=>wwv_flow_api.id(53086943116906744)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53517329601059337)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>8
,p_column_id=>wwv_flow_api.id(53087019381906745)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53517814302059339)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>9
,p_column_id=>wwv_flow_api.id(53087168606906746)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53518316787059341)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>10
,p_column_id=>wwv_flow_api.id(53087296733906747)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53518898948059343)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>11
,p_column_id=>wwv_flow_api.id(53087371002906748)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53519363458059344)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>12
,p_column_id=>wwv_flow_api.id(53087444015906749)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53519830397059346)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>16
,p_column_id=>wwv_flow_api.id(53087553701906750)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53520359760059348)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>17
,p_column_id=>wwv_flow_api.id(53507290649059101)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53520899639059350)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>18
,p_column_id=>wwv_flow_api.id(53507314809059102)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53521390943059352)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>19
,p_column_id=>wwv_flow_api.id(53507490591059103)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53521859593059353)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>20
,p_column_id=>wwv_flow_api.id(53507579042059104)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53522374208059355)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>21
,p_column_id=>wwv_flow_api.id(53507697042059105)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53522878493059357)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>23
,p_column_id=>wwv_flow_api.id(53507703721059106)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53523373922059358)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>24
,p_column_id=>wwv_flow_api.id(53507873020059107)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53614560016424162)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>1
,p_column_id=>wwv_flow_api.id(53508440916059113)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53624522783426304)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>20
,p_column_id=>wwv_flow_api.id(53508751272059116)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(53644195578498702)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>13
,p_column_id=>wwv_flow_api.id(53509669791059125)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(54500283568841324)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(54076128631084841)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>54
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(451796736918673673)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_display_seq=>15
,p_column_id=>wwv_flow_api.id(450826839287401106)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_highlight(
 p_id=>wwv_flow_api.id(47173000001)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_execution_seq=>7.5
,p_name=>'localidad'
,p_column_id=>wwv_flow_api.id(53086737572906742)
,p_background_color=>'#CDCECD'
,p_condition_type=>'COLUMN'
,p_condition_column_id=>wwv_flow_api.id(53086737572906742)
,p_condition_operator=>'NN'
,p_condition_is_case_sensitive=>false
,p_is_enabled=>true
);
wwv_flow_api.create_ig_report_highlight(
 p_id=>wwv_flow_api.id(89006000002)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_execution_seq=>6.25
,p_name=>'soja'
,p_column_id=>wwv_flow_api.id(53507314809059102)
,p_text_color=>'#8A295B'
,p_condition_type=>'COLUMN'
,p_condition_column_id=>wwv_flow_api.id(53507314809059102)
,p_condition_operator=>'NN'
,p_condition_is_case_sensitive=>false
,p_is_enabled=>true
);
wwv_flow_api.create_ig_report_highlight(
 p_id=>wwv_flow_api.id(113823000003)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_execution_seq=>5
,p_name=>'DISTRITO'
,p_background_color=>'#CDCECD'
,p_condition_type=>'COLUMN'
,p_condition_column_id=>wwv_flow_api.id(53086626137906741)
,p_condition_operator=>'EQ'
,p_condition_is_case_sensitive=>false
,p_condition_expression=>'Distrito:'
,p_is_enabled=>true
);
wwv_flow_api.create_ig_report_highlight(
 p_id=>wwv_flow_api.id(179561000002)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_execution_seq=>6.875
,p_name=>'sojaz'
,p_column_id=>wwv_flow_api.id(53507490591059103)
,p_text_color=>'#8A295B'
,p_condition_type=>'COLUMN'
,p_condition_column_id=>wwv_flow_api.id(53507490591059103)
,p_condition_operator=>'NN'
,p_condition_is_case_sensitive=>false
,p_is_enabled=>true
);
wwv_flow_api.create_ig_report_highlight(
 p_id=>wwv_flow_api.id(227243000001)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_execution_seq=>8.75
,p_name=>'lote'
,p_column_id=>wwv_flow_api.id(53086550282906740)
,p_background_color=>'#CDCECD'
,p_condition_type=>'COLUMN'
,p_condition_column_id=>wwv_flow_api.id(53086550282906740)
,p_condition_operator=>'NN'
,p_condition_is_case_sensitive=>false
,p_is_enabled=>true
);
wwv_flow_api.create_ig_report_highlight(
 p_id=>wwv_flow_api.id(292597000001)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_execution_seq=>8.125
,p_name=>'status_desc'
,p_column_id=>wwv_flow_api.id(53087168606906746)
,p_background_color=>'#CDCECD'
,p_condition_type=>'COLUMN'
,p_condition_column_id=>wwv_flow_api.id(53087168606906746)
,p_condition_operator=>'NN'
,p_condition_is_case_sensitive=>false
,p_is_enabled=>true
);
wwv_flow_api.create_ig_report_highlight(
 p_id=>wwv_flow_api.id(347492000002)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_execution_seq=>7.1875
,p_name=>'maiz'
,p_column_id=>wwv_flow_api.id(53507579042059104)
,p_text_color=>'#8A295B'
,p_condition_type=>'COLUMN'
,p_condition_column_id=>wwv_flow_api.id(53507579042059104)
,p_condition_operator=>'NN'
,p_condition_is_case_sensitive=>false
,p_is_enabled=>true
);
wwv_flow_api.create_ig_report_highlight(
 p_id=>wwv_flow_api.id(391368000001)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_execution_seq=>8.4375
,p_name=>'tipo_alqui'
,p_column_id=>wwv_flow_api.id(53508387720059112)
,p_background_color=>'#CDCECD'
,p_condition_type=>'COLUMN'
,p_condition_column_id=>wwv_flow_api.id(53508387720059112)
,p_condition_operator=>'NN'
,p_condition_is_case_sensitive=>false
,p_is_enabled=>true
);
wwv_flow_api.create_ig_report_highlight(
 p_id=>wwv_flow_api.id(466411000002)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_execution_seq=>7.34375
,p_name=>'trigo'
,p_column_id=>wwv_flow_api.id(53507697042059105)
,p_text_color=>'#8A295B'
,p_condition_type=>'COLUMN'
,p_condition_column_id=>wwv_flow_api.id(53507697042059105)
,p_condition_operator=>'NN'
,p_condition_is_case_sensitive=>false
,p_is_enabled=>true
);
wwv_flow_api.create_ig_report_highlight(
 p_id=>wwv_flow_api.id(500995000001)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_execution_seq=>8.59375
,p_name=>'suelo'
,p_column_id=>wwv_flow_api.id(53507290649059101)
,p_background_color=>'#CDCECD'
,p_condition_type=>'COLUMN'
,p_condition_column_id=>wwv_flow_api.id(53507290649059101)
,p_condition_operator=>'NN'
,p_condition_is_case_sensitive=>false
,p_is_enabled=>true
);
wwv_flow_api.create_ig_report_highlight(
 p_id=>wwv_flow_api.id(24652214057840)
,p_view_id=>wwv_flow_api.id(53513892146059325)
,p_execution_seq=>10
,p_name=>'Hectareas'
,p_column_id=>wwv_flow_api.id(53086866048906743)
,p_background_color=>'#CDCECD'
,p_condition_type=>'COLUMN'
,p_condition_column_id=>wwv_flow_api.id(53086866048906743)
,p_condition_operator=>'EQ'
,p_condition_is_case_sensitive=>false
,p_condition_expression=>'Hectareas:'
,p_is_enabled=>true
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(53509805057059127)
,p_plug_name=>'Otros'
,p_parent_plug_id=>wwv_flow_api.id(53085895487906733)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--noBorder:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(119469003339420663)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(53509946729059128)
,p_plug_name=>'Co-Deudores'
,p_parent_plug_id=>wwv_flow_api.id(53509805057059127)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--scrollBody:t-Form--noPadding'
,p_plug_template=>wwv_flow_api.id(119469003339420663)
,p_plug_display_sequence=>10
,p_plug_grid_column_span=>8
,p_plug_display_column=>2
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_header=>'<B>Co-Deudores</B>'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(53511783232059146)
,p_plug_name=>'BCONFIG'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(119469003339420663)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(337848882092930806)
,p_plug_name=>'LLENADO DE FUNCIONARIO A CLIENTE'
,p_region_name=>'CLI_HIL'
,p_region_css_classes=>'js-dialog-size2000x700'
,p_region_template_options=>'#DEFAULT#:js-dialog-size600x400'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(119465752866420661)
,p_plug_display_sequence=>10
,p_plug_display_point=>'REGION_POSITION_04'
,p_query_type=>'SQL'
,p_plug_source=>'SELECT CODIGO, NOMBRE, RUC, DIRECCION, TELEFONO, null sele FROM FIN_EMPL_HIL_LLENADO;'
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'MILLIMETERS'
,p_prn_paper_size=>'A4'
,p_prn_width=>297
,p_prn_height=>210
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header=>'LLENADO DE FUNCIONARIO A CLIENTE'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#EEEEEE'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'bold'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#FFFFFF'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_prn_border_color=>'#666666'
);
end;
/
begin
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(337848924189930807)
,p_max_row_count=>'1000000'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:EMAIL:XLS:PDF:RTF'
,p_owner=>'LRAMIREZ'
,p_internal_uid=>337848924189930807
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(337849022866930808)
,p_db_column_name=>'CODIGO'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Codigo'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(337849115678930809)
,p_db_column_name=>'NOMBRE'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Nombre'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(337849296437930810)
,p_db_column_name=>'RUC'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Ruc'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(337849332500930811)
,p_db_column_name=>'DIRECCION'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Direccion'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(337849402824930812)
,p_db_column_name=>'TELEFONO'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Telefono'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(337849792470930815)
,p_db_column_name=>'SELE'
,p_display_order=>60
,p_column_identifier=>'F'
,p_column_label=>'Sele'
,p_column_link=>'javascript:$s(''P6507_PNA_NOMBRE'',''#NOMBRE#'');javascript:$s(''P6507_PNA_HILA_RUC'',''#RUC#'');javascript:$s(''P6507_PNA_HILA_COD'',''#CODIGO#'');javascript:closeModal(''CLI_HIL'');'
,p_column_linktext=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-view.png" class="apex-edit-view" alt="">'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(340661004070746289)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'3406611'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'CODIGO:NOMBRE:RUC:DIRECCION:TELEFONO:SELE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(53510462280059133)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(52793524327525812)
,p_button_name=>'ACEPTAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--iconRight'
,p_button_template_id=>wwv_flow_api.id(119514564106420685)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Aceptar'
,p_button_position=>'BODY'
,p_icon_css_classes=>'fa-check-circle'
,p_grid_new_row=>'N'
,p_grid_column=>10
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(53510549310059134)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(52793524327525812)
,p_button_name=>'CANCELAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--iconRight'
,p_button_template_id=>wwv_flow_api.id(119514564106420685)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Cancelar'
,p_button_position=>'BODY'
,p_icon_css_classes=>'fa-ban'
,p_grid_new_row=>'N'
,p_grid_new_column=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(53510600575059135)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(52793524327525812)
,p_button_name=>'BORRAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--iconRight'
,p_button_template_id=>wwv_flow_api.id(119514564106420685)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Borrar'
,p_button_position=>'BODY'
,p_icon_css_classes=>'fa-trash-o'
,p_grid_new_row=>'N'
,p_grid_new_column=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(53510785985059136)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_api.id(52793524327525812)
,p_button_name=>'SALIR'
,p_button_action=>'REDIRECT_APP'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--iconRight'
,p_button_template_id=>wwv_flow_api.id(119514564106420685)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Salir'
,p_button_position=>'BODY'
,p_button_redirect_url=>'f?p=105:13:&SESSION.::&DEBUG.:::'
,p_icon_css_classes=>'fa-arrow-circle-right'
,p_grid_new_row=>'N'
,p_grid_new_column=>'N'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(54075196966084831)
,p_branch_action=>'f?p=&APP_ID.:6506:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51444374570132125)
,p_name=>'P6507_PNA_CODIGO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Codigo :'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT PNA_CODIGO||'' - ''||PNA_NOMBRE, PNA_CODIGO',
'  FROM FIN_PERSONA',
' WHERE PNA_EMPR = :P_EMPRESA',
' ORDER BY PNA_CODIGO',
''))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_tag_css_classes=>'obl'
,p_tag_attributes=>'tabindex=1 maxlength=6'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_07=>'LISTA DE PERSONAS'
,p_attribute_08=>'500'
,p_attribute_09=>'500'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51444408131132126)
,p_name=>'P6507_PNA_TIPO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>'<spa style=color:red;>Tipo Persona:</spa>'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT TIPP_CODIGO||'' - ''||TIPP_DESC, TIPP_CODIGO',
'  FROM FIN_TIPO_PERSONA T',
' WHERE TIPP_EMPR = :P_EMPRESA',
' ORDER BY TIPP_CODIGO',
''))
,p_lov_display_null=>'YES'
,p_cSize=>100
,p_tag_css_classes=>'no_null'
,p_tag_attributes=>'tabindex=5'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_07=>'Tipo de persona'
,p_attribute_08=>'500'
,p_attribute_09=>'500'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51444505084132127)
,p_name=>'P6507_PNA_NOMBRE'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>'<spa style=color:red;>Nombre :</spa>'
,p_placeholder=>'F8 para levantar lista de Funcionarios Hilagro'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>100
,p_cMaxlength=>100
,p_tag_css_classes=>'no_null'
,p_tag_attributes=>'tabindex=3 onKeyUp="this.value=this.value.toUpperCase();"'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51444663306132128)
,p_name=>'P6507_PNA_APE'
,p_item_sequence=>320
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51444726903132129)
,p_name=>'P6507_PNA_RAZON_SOCIAL'
,p_item_sequence=>330
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51444856538132130)
,p_name=>'P6507_PNA_SEXO'
,p_item_sequence=>340
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51444981783132131)
,p_name=>'P6507_PNA_FEC_NAC'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>'<spa style=color:red;>Fec. Nacimiento :</spa>'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_tag_css_classes=>'no_null'
,p_tag_attributes=>'tabindex=14'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51445062823132132)
,p_name=>'P6507_PNA_ESTADO_CIVIL'
,p_item_sequence=>350
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51445160295132133)
,p_name=>'P6507_PNA_SECTOR'
,p_item_sequence=>360
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51445232523132134)
,p_name=>'P6507_PNA_OBS_ACTIV'
,p_item_sequence=>370
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51445322573132135)
,p_name=>'P6507_PNA_ACTIV_EDAD_INI'
,p_item_sequence=>380
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51445413977132136)
,p_name=>'P6507_PNA_CONYUGE'
,p_item_sequence=>390
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51445543519132137)
,p_name=>'P6507_PNA_OBS'
,p_item_sequence=>400
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51445615268132138)
,p_name=>'P6507_PNA_FEC_INGRESO'
,p_item_sequence=>410
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51445734905132139)
,p_name=>'P6507_PNA_FEC_ACTUALIZACION'
,p_item_sequence=>420
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51445836858132140)
,p_name=>'P6507_PNA_FOTO'
,p_item_sequence=>430
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51445978144132141)
,p_name=>'P6507_PNA_LUGAR_ORIGEN_REPLICA'
,p_item_sequence=>440
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51446070331132142)
,p_name=>'P6507_PNA_CLI_CATEG'
,p_item_sequence=>450
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51446117664132143)
,p_name=>'P6507_PNA_DV'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2
,p_tag_attributes=>'tabindex=10'
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51446260231132144)
,p_name=>'P6507_PNA_RUC_DV'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>'RUC - DV'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>20
,p_tag_attributes=>'tabindex=9'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51446327937132145)
,p_name=>'P6507_PNA_EMPR'
,p_item_sequence=>460
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51446473069132146)
,p_name=>'P6507_PNA_VENDEDOR'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(51544175685822743)
,p_use_cache_before_default=>'NO'
,p_prompt=>'<spa style=color:red;>Responsable :</spa>'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT VEND_LEGAJO||'' - ''||EMPL_NOMBRE, ',
'       VEND_LEGAJO',
'FROM   PER_EMPLEADO, FAC_VENDEDOR',
'WHERE  EMPL_LEGAJO = VEND_LEGAJO ',
'AND EMPL_EMPRESA = VEND_EMPR',
'AND VEND_EMPR = :P_EMPRESA',
'ORDER BY 1 ASC'))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_tag_attributes=>'tabindex=23'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_07=>'Vendedores'
,p_attribute_08=>'500'
,p_attribute_09=>'500'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51446578733132147)
,p_name=>'P6507_PNA_TELEFONO'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>'<spa style=color:red;>Telefono :</spa>'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>100
,p_tag_css_classes=>'no_null'
,p_tag_attributes=>'tabindex=2'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51446651909132148)
,p_name=>'P6507_PNA_SUC'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(51544175685822743)
,p_use_cache_before_default=>'NO'
,p_prompt=>'<spa style=color:red;>Sucursal :</spa>'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUC_CODIGO||'' - ''||SU.SUC_DESC, SU.SUC_CODIGO',
'  FROM GEN_SUCURSAL SU',
' WHERE SUC_EMPR = :P_EMPRESA',
' ORDER BY SUC_CODIGO',
''))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_tag_attributes=>'tabindex=22'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_07=>'Sucursales'
,p_attribute_08=>'500'
,p_attribute_09=>'500'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51446738312132149)
,p_name=>'P6507_PNA_SEGMENTO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(51544175685822743)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Categoria de Cliente :'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SEG_CODIGO||'' - ''||SEG_DESC, SEG_CODIGO',
'  FROM FIN_SEGMENTO',
' WHERE SEG_EMPR = :P_EMPRESA',
' ORDER BY SEG_CODIGO'))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_tag_attributes=>'tabindex=21'
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_07=>'SEGMENTOS'
,p_attribute_08=>'500'
,p_attribute_09=>'500'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51446827472132150)
,p_name=>'P6507_PNA_PROG_PAGO'
,p_item_sequence=>470
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51539997595822701)
,p_name=>'P6507_PNA_PAIS'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('<spa style=color:red;>Pa\00EDs :</spa>')
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT PAIS_CODIGO || '' - '' || PAIS_DESC, PAIS_CODIGO',
'  FROM GEN_PAIS',
' WHERE PAIS_EMPR = :P_EMPRESA',
' ORDER BY PAIS_CODIGO',
''))
,p_lov_display_null=>'YES'
,p_cSize=>100
,p_tag_css_classes=>'no_null'
,p_tag_attributes=>'tabindex=7'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_07=>'PAIS'
,p_attribute_08=>'500'
,p_attribute_09=>'500'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51540056822822702)
,p_name=>'P6507_PNA_NRODOC_CODEUDOR2'
,p_item_sequence=>460
,p_item_plug_id=>wwv_flow_api.id(53509946729059128)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Nro Documento Co-Deudor N\00BA 2:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51540135847822703)
,p_name=>'P6507_PNA_NRODOC_CODEUDOR1'
,p_item_sequence=>430
,p_item_plug_id=>wwv_flow_api.id(53509946729059128)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Nro Documento Co-Deudor N\00BA 1:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51540280012822704)
,p_name=>'P6507_PNA_NOTA_LIQUIDACION'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nota Liq. :'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>200
,p_cMaxlength=>200
,p_cHeight=>5
,p_tag_attributes=>'tabindex=15 onKeyUp="this.value=this.value.toUpperCase();"'
,p_begin_on_new_line=>'N'
,p_grid_column=>7
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51540387264822705)
,p_name=>'P6507_PNA_NOM_CONYUGE'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(51544175685822743)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nombre Conyuge.'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>40
,p_tag_attributes=>'tabindex=24 onKeyUp="this.value=this.value.toUpperCase();"'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51540485975822706)
,p_name=>'P6507_PNA_NOM_CODEUDOR2'
,p_item_sequence=>450
,p_item_plug_id=>wwv_flow_api.id(53509946729059128)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Co-Deudor N\00BA 2:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'onKeyUp="this.value=this.value.toUpperCase();"'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51540573180822707)
,p_name=>'P6507_PNA_NOM_CODEUDOR1'
,p_item_sequence=>410
,p_item_plug_id=>wwv_flow_api.id(53509946729059128)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Co-Deudor N\00BA 1:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'onKeyUp="this.value=this.value.toUpperCase();"'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51540663278822708)
,p_name=>'P6507_PNA_LOGIN'
,p_item_sequence=>480
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51540704208822709)
,p_name=>'P6507_PNA_CELULAR'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Celular :'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>100
,p_tag_attributes=>'tabindex=6'
,p_begin_on_new_line=>'N'
,p_grid_column=>7
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51540863592822710)
,p_name=>'P6507_PNA_CONTADOR_CEL'
,p_item_sequence=>490
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51540937582822711)
,p_name=>'P6507_PNA_CONTADOR_NOMB'
,p_item_sequence=>500
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51541080451822712)
,p_name=>'P6507_PNA_CTRL_RANGO_PRECIO'
,p_item_sequence=>510
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51541192497822713)
,p_name=>'P6507_PNA_DIRECCION'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('<spa style=color:red;>Direcci\00F3n :</spa>')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>150
,p_tag_css_classes=>'no_null'
,p_tag_attributes=>'tabindex=12 onKeyUp="this.value=this.value.toUpperCase();"'
,p_begin_on_new_line=>'N'
,p_grid_column=>7
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51541249575822714)
,p_name=>'P6507_PNA_DOC_IDENT_CONYUGE'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(51544175685822743)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Doc.Ident. Conyuge :'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>10
,p_tag_attributes=>'tabindex=25'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_inline_help_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<br>',
'<br>',
'<br>',
'<br>'))
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51541384117822715)
,p_name=>'P6507_PNA_DOM_LATITUD_GRA'
,p_item_sequence=>270
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51541405929822716)
,p_name=>'P6507_PNA_DOM_LATITUD_MIN'
,p_item_sequence=>280
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51541547193822717)
,p_name=>'P6507_PNA_DOM_LONGITUD_GRA'
,p_item_sequence=>290
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51541605932822718)
,p_name=>'P6507_PNA_DOM_LONGITUD_MIN'
,p_item_sequence=>300
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51541745836822719)
,p_name=>'P6507_PNA_EMAIL'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>'E-Mail :'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>100
,p_tag_attributes=>'tabindex=11 onKeyUp="this.value=this.value.toUpperCase();"'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51541843461822720)
,p_name=>'P6507_PNA_FAX'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Fax'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>15
,p_tag_attributes=>'tabindex=4 onKeyUp="this.value=this.value.toUpperCase();"'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>2
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51541979351822721)
,p_name=>'P6507_PNA_FEC_GRAB'
,p_item_sequence=>520
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51542079707822722)
,p_name=>'P6507_PNA_FEC_NAC_CONYUGE'
,p_item_sequence=>530
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51542106206822723)
,p_name=>'P6507_PNA_FORM'
,p_item_sequence=>540
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51542254500822724)
,p_name=>'P6507_PNA_IND_CONTROL_LC'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_api.id(51544175685822743)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_CHECKBOX'
,p_lov=>unistr('STATIC:Controlar Limite de Cr\00E9dido;S')
,p_colspan=>4
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'1'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51542379261822725)
,p_name=>'P6507_PNA_IND_INTERESES'
,p_item_sequence=>550
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51542420354822726)
,p_name=>'P6507_PNA_IND_LIBRE_FAC_GA_ADM'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_api.id(51544175685822743)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_CHECKBOX'
,p_lov=>'STATIC:Libre de Ga.Admin;S'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'1'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51542539121822727)
,p_name=>'P6507_PNA_IND_SIN_CONTRATO_AP'
,p_item_sequence=>560
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51542678649822728)
,p_name=>'P6507_PNA_LIMCR_ANTICIPO_US'
,p_item_sequence=>570
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51542716299822729)
,p_name=>'P6507_PNA_LIMCR_COMBUS_US'
,p_item_sequence=>580
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51542871481822730)
,p_name=>'P6507_PNA_LIMCR_INSUMO_US'
,p_item_sequence=>590
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51542905646822731)
,p_name=>'P6507_PNA_LIMCR_OTROS_US'
,p_item_sequence=>600
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51543007916822732)
,p_name=>'P6507_PNA_LINEA_NEGOCIO'
,p_item_sequence=>610
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51543165501822733)
,p_name=>'P6507_PNA_EMAIL_TESO'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>'<spa style=color:red;>Email  para el banco:</spa>'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>50
,p_tag_css_classes=>'no_null'
,p_tag_attributes=>'tabindex=13 onKeyUp="this.value=this.value.toUpperCase();"'
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_help_text=>'<br><spa style=color:red;>En caso que no tenga usar el de tagro </spa>'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51543256458822734)
,p_name=>'P6507_PNA_DOC_MONSANTO'
,p_item_sequence=>620
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51543310913822735)
,p_name=>'P6507_PNA_HAB_FACT'
,p_item_sequence=>630
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51543494468822736)
,p_name=>'P6507_PNA_MULTINACIONAL'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Multinacional:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>1
,p_tag_attributes=>'tabindex=8 onKeyUp="this.value=this.value.toUpperCase();"'
,p_begin_on_new_line=>'N'
,p_grid_column=>7
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51543591598822737)
,p_name=>'P6507_PNA_IND_FECHA_PUERTO'
,p_item_sequence=>640
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51543613350822738)
,p_name=>'P6507_TIPP_DESC'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'readonly '
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
end;
/
begin
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51543763145822739)
,p_name=>'P6507_PAIS_DESC'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'readonly '
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51543866041822740)
,p_name=>'P6507_SEG_DESC'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(51544175685822743)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'readonly '
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51543990308822741)
,p_name=>'P6507_SUC_DESC'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(51544175685822743)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'readonly '
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51544095981822742)
,p_name=>'P6507_VEND_NOMBRE'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(51544175685822743)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'readonly '
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51544250951822744)
,p_name=>'P6507_TXT'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_api.id(51544175685822743)
,p_use_cache_before_default=>'NO'
,p_item_default=>'al desembolsar anticipo a productor'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51544345961822745)
,p_name=>'P6507_CHK_IND_LLEG_PUERTO'
,p_item_sequence=>310
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51544778678822749)
,p_name=>'P6507_PROV_CODIGO'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51544855529822750)
,p_name=>'P6507_PROV_PAIS'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51899347429561201)
,p_name=>'P6507_PROV_RAZON_SOCIAL'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51899447228561202)
,p_name=>'P6507_PROV_PROPIETARIO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Propietario :'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>40
,p_tag_attributes=>'tabindex=26 onKeyUp="this.value=this.value.toUpperCase();"'
,p_colspan=>4
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51899541791561203)
,p_name=>'P6507_PROV_DIR'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51899673048561204)
,p_name=>'P6507_PROV_TEL'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51899704697561205)
,p_name=>'P6507_PROV_FAX'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51899924382561207)
,p_name=>'P6507_PROV_RUC'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51900068567561208)
,p_name=>'P6507_PROV_EST_PROV'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_prompt=>'<spa style=color:red;>Estado :</spa>'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>1
,p_tag_css_classes=>'no_null'
,p_tag_attributes=>'tabindex=28 onKeyUp="this.value=this.value.toUpperCase();"'
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51900144724561209)
,p_name=>'P6507_PROV_PERS_CONTACTO'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Pers.contacto :'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>40
,p_tag_attributes=>'tabindex=31 onKeyUp="this.value=this.value.toUpperCase();"'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51900233938561210)
,p_name=>'P6507_PROV_OBS'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Observaci\00F3n :')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>40
,p_tag_attributes=>'tabindex=30 onKeyUp="this.value=this.value.toUpperCase();"'
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51900378959561211)
,p_name=>'P6507_PROV_TIPO'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_prompt=>'<spa style=color:red;>Tipo Proveedor :</spa>'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT TIPR_CODIGO||'' - ''||TIPR_DESC, TIPR_CODIGO',
'  FROM FIN_TIPO_PROVEEDOR',
' WHERE TIPR_EMPR = :P_EMPRESA',
' ORDER BY TIPR_CODIGO',
''))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_tag_css_classes=>'no_null'
,p_tag_attributes=>'tabindex=32'
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_07=>'TIPO DE PROVEEDOR'
,p_attribute_08=>'500'
,p_attribute_09=>'500'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51900413002561212)
,p_name=>'P6507_PROV_EMAIL'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51900688323561214)
,p_name=>'P6507_PROV_CELULAR'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51900763799561215)
,p_name=>'P6507_PROV_PERS_CONTACTO2'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('2\00B0 Pers.contacto:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>40
,p_tag_attributes=>'tabindex=33 onKeyUp="this.value=this.value.toUpperCase();"'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51900868942561216)
,p_name=>'P6507_PROV_LUGAR_ORIGEN_REPLIC'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51900940724561217)
,p_name=>'P6507_PROV_TIMBRADO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51901045567561218)
,p_name=>'P6507_PROV_DV'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51901197422561219)
,p_name=>'P6507_PROV_RUC_DV'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51901250560561220)
,p_name=>'P6507_PROV_IND_RETENCION'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_item_default=>'S'
,p_prompt=>'Generar Retencion?'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>1
,p_tag_attributes=>'tabindex=29 onKeyUp="this.value=this.value.toUpperCase();"'
,p_colspan=>4
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51902366079561231)
,p_name=>'P6507_PROV_EMPR'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51902693017561234)
,p_name=>'P6507_PROV_CONTADOR_CEL'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cel/Tel :'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>50
,p_tag_attributes=>'tabindex=35'
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51902752432561235)
,p_name=>'P6507_PROV_CONTADOR_NOMB'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nombre Contador :'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>100
,p_tag_attributes=>'tabindex=34 onKeyUp="this.value=this.value.toUpperCase();"'
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51902895094561236)
,p_name=>'P6507_PROV_RETENCION'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51903330198561241)
,p_name=>'P6507_S_EST_DESC'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51903495350561242)
,p_name=>'P6507_TIPR_DESC'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(51544578805822747)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51903704376561245)
,p_name=>'P6507_CLI_CODIGO'
,p_item_sequence=>330
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51903883084561246)
,p_name=>'P6507_CLI_NOM'
,p_item_sequence=>340
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51903962473561247)
,p_name=>'P6507_CLI_PROPIETARIO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Propietario :'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>40
,p_tag_attributes=>'tabindex=36 onKeyUp="this.value=this.value.toUpperCase();"'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51904001089561248)
,p_name=>'P6507_CLI_NOM_FANTASIA'
,p_item_sequence=>350
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51904116712561249)
,p_name=>'P6507_CLI_DIR'
,p_item_sequence=>360
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51904254505561250)
,p_name=>'P6507_CLI_DOC_IDENT_PROPIETARI'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Doc. ident. :'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>20
,p_tag_attributes=>'tabindex=37 onKeyUp="this.value=this.value.toUpperCase();"'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51961102526655801)
,p_name=>'P6507_CLI_LOCALIDAD'
,p_item_sequence=>370
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51961240722655802)
,p_name=>'P6507_CLI_FEC_NAC_PROPIETARIO'
,p_item_sequence=>380
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51961330972655803)
,p_name=>'P6507_CLI_TEL'
,p_item_sequence=>390
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51961448947655804)
,p_name=>'P6507_CLI_FAX'
,p_item_sequence=>400
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51961527625655805)
,p_name=>'P6507_CLI_RUC'
,p_item_sequence=>410
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51961689455655806)
,p_name=>'P6507_CLI_RAMO'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_prompt=>'<spa style=color:red;>Actividad  :</spa>'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT RAMO_CODIGO||'' - ''||RAMO_DESC, RAMO_CODIGO',
'  FROM FIN_RAMO',
' WHERE RAMO_EMPR = :P_EMPRESA',
' ORDER BY RAMO_CODIGO',
''))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_tag_css_classes=>'no_null'
,p_tag_attributes=>'tabindex=43'
,p_colspan=>4
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_07=>'ACTIVIDADES'
,p_attribute_08=>'500'
,p_attribute_09=>'500'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51961837773655808)
,p_name=>'P6507_CLI_CATEG'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('<spa style=color:red;>Categor\00EDa :</spa>')
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT FCAT_CODIGO||'' - ''||FCAT_DESC, FCAT_CODIGO',
'  FROM FAC_CATEGORIA',
' WHERE FCAT_EMPR = :P_EMPRESA',
'   AND 1 = (CASE',
'         WHEN (SELECT COUNT(1)',
'                 FROM FIN_CLIENTE C, PER_EMPLEADO E',
'                WHERE C.CLI_RUC_DV(+) = TO_CHAR(E.EMPL_DOC_IDENT)',
'                  AND E.EMPL_SITUACION = ''A''',
'                  AND TO_CHAR(E.EMPL_DOC_IDENT) = :P6507_PNA_RUC_DV) > 0 AND',
'              FCAT_CODIGO NOT IN (1, 2, 6, 7) THEN',
'          1',
'         WHEN (SELECT COUNT(1)',
'                 FROM FIN_CLIENTE C, PER_EMPLEADO E',
'                WHERE C.CLI_RUC_DV(+) = TO_CHAR(E.EMPL_DOC_IDENT)',
'                  AND E.EMPL_SITUACION = ''A''',
'                  AND TO_CHAR(E.EMPL_DOC_IDENT) = :P6507_PNA_RUC_DV) = 0 AND',
'              FCAT_CODIGO NOT IN (3, 4, 5) THEN',
'          1',
'         ELSE',
'          0',
'       END)',
' ORDER BY 2',
''))
,p_lov_display_null=>'YES'
,p_lov_cascade_parent_items=>'P6507_PNA_RUC_DV'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_tag_css_classes=>'no_null'
,p_tag_attributes=>'tabindex=42'
,p_colspan=>4
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_07=>'CATEGORIAS'
,p_attribute_08=>'500'
,p_attribute_09=>'500'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51961946542655809)
,p_name=>'P6507_CLI_ZONA'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_prompt=>'<spa style=color:red;>Zona :</spa>'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ZONA_CODIGO||'' - ''||ZONA_DESC, ZONA_CODIGO',
'  FROM FAC_ZONA',
' WHERE ZONA_EMPR = :P_EMPRESA',
' ORDER BY ZONA_CODIGO',
''))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_tag_css_classes=>'no_null'
,p_tag_attributes=>'tabindex=41'
,p_colspan=>4
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_07=>'ZONA'
,p_attribute_08=>'500'
,p_attribute_09=>'500'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51962250294655812)
,p_name=>'P6507_CLI_PERS_CONTACTO'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Pers. contac. :'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>60
,p_tag_attributes=>'tabindex=38 onKeyUp="this.value=this.value.toUpperCase();"'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51962445898655814)
,p_name=>'P6507_CLI_OBS'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Observaci\00F3n :')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>500
,p_tag_attributes=>'tabindex=40 onKeyUp="this.value=this.value.toUpperCase();"'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51962638970655816)
,p_name=>'P6507_CLI_DOC_IDENT_CONTACTO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Doc. ident. :'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>20
,p_tag_attributes=>'tabindex=39 onKeyUp="this.value=this.value.toUpperCase();"'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51962708732655817)
,p_name=>'P6507_CLI_EST_CLI'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(51975914225655949)
,p_use_cache_before_default=>'NO'
,p_item_default=>'A'
,p_prompt=>'<spa style=color:red;>Estado :</spa>'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>1
,p_tag_css_classes=>'no_null'
,p_tag_attributes=>'tabindex=53 onKeyUp="this.value=this.value.toUpperCase();"'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51962989568655819)
,p_name=>'P6507_CLI_MON'
,p_item_sequence=>420
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51963038663655820)
,p_name=>'P6507_CLI_IMP_LIM_CR'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(51975914225655949)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51963194512655821)
,p_name=>'P6507_CLI_BLOQ_LIM_CR'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(51975914225655949)
,p_use_cache_before_default=>'NO'
,p_item_default=>'N'
,p_prompt=>'Lim Bloqueado :'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>1
,p_tag_attributes=>'tabindex=52 onKeyUp="this.value=this.value.toUpperCase();"'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51963247446655822)
,p_name=>'P6507_CLI_MAX_DIAS_ATRASO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(51975914225655949)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('M\00E1x.atr.:')
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>30
,p_tag_attributes=>'tabindex=51'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_03=>'right'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51964391922655833)
,p_name=>'P6507_CLI_DIR_PARTICULAR'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Dir. Part. :'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>40
,p_tag_attributes=>'tabindex=44 onKeyUp="this.value=this.value.toUpperCase();"'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51964565289655835)
,p_name=>'P6507_CLI_TEL_PARTICULAR'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(51975914225655949)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Tel.Par. :'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>15
,p_tag_attributes=>'tabindex=54'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51964859446655838)
,p_name=>'P6507_CLI_EMAIL'
,p_item_sequence=>430
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51964900575655839)
,p_name=>'P6507_CLI_NRO_FICHA'
,p_item_sequence=>440
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51965059414655840)
,p_name=>'P6507_CLI_PROYECTO'
,p_item_sequence=>450
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51965166113655841)
,p_name=>'P6507_CLI_SEXO'
,p_item_sequence=>460
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51966551759655905)
,p_name=>'P6507_CLI_DV'
,p_item_sequence=>470
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51966622766655906)
,p_name=>'P6507_CLI_RUC_DV'
,p_item_sequence=>480
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51968721829655927)
,p_name=>'P6507_CLI_APELLIDO'
,p_item_sequence=>490
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51968895268655928)
,p_name=>'P6507_CLI_NOMBRE'
,p_item_sequence=>500
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51969768091655937)
,p_name=>'P6507_CLI_IND_EXHIBIDOR'
,p_item_sequence=>510
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51969822103655938)
,p_name=>'P6507_CLI_IND_MOD_CANAL'
,p_item_sequence=>520
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51969951836655939)
,p_name=>'P6507_CLI_OBS_FACT'
,p_item_sequence=>530
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51970072865655940)
,p_name=>'P6507_CLI_SUCURSAL'
,p_item_sequence=>540
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51971772399655907)
,p_name=>'P6507_CLI_EMPR'
,p_item_sequence=>550
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51973889389655928)
,p_name=>'P6507_CLI_COD_EMPL_EMPR_ORIG'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cod. Empleado:'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>30
,p_tag_attributes=>'tabindex=46'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_03=>'right'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51974169422655931)
,p_name=>'P6507_CLI_FACT_COMB_GS'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Fact Combust en Gs?'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>1
,p_tag_attributes=>'tabindex=47 onKeyUp="this.value=this.value.toUpperCase();"'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51974292907655932)
,p_name=>'P6507_CLI_IND_EXIGIR_RESP_FCRE'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_CHECKBOX'
,p_lov=>'STATIC:Firma  Fcred TMU;S'
,p_tag_attributes=>'tabindex=45'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'1'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51974386295655933)
,p_name=>'P6507_CLI_PORC_FLETE'
,p_item_sequence=>270
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_prompt=>'% Rec. Desc Flete:'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>30
,p_tag_attributes=>'tabindex=48 onKeyUp="this.value=this.value.toUpperCase();"'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_03=>'right'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51974591592655935)
,p_name=>'P6507_CLI_VALOR_AGREGADO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(51975914225655949)
,p_use_cache_before_default=>'NO'
,p_prompt=>'V.Agre:'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>30
,p_tag_attributes=>'tabindex=55'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_03=>'right'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51975561185655945)
,p_name=>'P6507_MON_SIMBOLO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(51975914225655949)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
end;
/
begin
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51975655545655946)
,p_name=>'P6507_ZONA_DESC'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51975719210655947)
,p_name=>'P6507_FCAT_DESC'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(51975890647655948)
,p_name=>'P6507_RAMO_DESC'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(52793423033525811)
,p_name=>'P6507_TEX'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(51975914225655949)
,p_use_cache_before_default=>'NO'
,p_item_default=>'Cuentas Bancarias'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_grid_column=>6
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(52793621931525813)
,p_name=>'P6507_OBS'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(52793524327525812)
,p_item_default=>'Obs: Los campos en rojo son obligatorios.'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_css_classes=>'color'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(52793755152525814)
,p_name=>'P6507_S_EST_CLI'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(51975914225655949)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(53083463375906709)
,p_name=>'P6507_PNA_HILA_COD'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(53083523786906710)
,p_name=>'P6507_S_CLI_COD_EMPL_EMPR_ORIG'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(53084485421906719)
,p_name=>'P6507_CLI_PAIS'
,p_item_sequence=>280
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(53084543435906720)
,p_name=>'P6507_CLI_LUGAR_ORIGEN_REPLICA'
,p_item_sequence=>290
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(53084631217906721)
,p_name=>'P6507_CLI_FEC_INGRESO'
,p_item_sequence=>300
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(53084712886906722)
,p_name=>'P6507_CLI_FEC_ACTUALIZACION'
,p_item_sequence=>310
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(53084877771906723)
,p_name=>'P6507_CLI_IND_POTENCIAL'
,p_item_sequence=>320
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(53511829877059147)
,p_name=>'P6507_MON_LOC'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(53511783232059146)
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(53511974264059148)
,p_name=>'P6507_CANT_DECIMALES_LOC'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(53511783232059146)
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(53512066893059149)
,p_name=>'P6507_HAB_FACT_COMB_GS'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(53511783232059146)
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(53512168432059150)
,p_name=>'P6507_SIMBOLO_MON_LOC'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(53511783232059146)
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(54072136043084801)
,p_name=>'P6507_OPER_RET'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(53511783232059146)
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(54072231367084802)
,p_name=>'P6507_BLOQ'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(53511783232059146)
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(54072797706084807)
,p_name=>'P6507_SEQ_ID_BCO'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(51975914225655949)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(54075908670084839)
,p_name=>'P6507_IND_CREAR'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(51442890062132110)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(54076297049084842)
,p_name=>'P6507_SEQ_AREA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(53086100872906736)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(82606841918054238)
,p_name=>'P6507_PNA_EXIST'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(329446246241267138)
,p_name=>'P6507_LOGIN'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(429720706486390702)
,p_name=>'P6507_PNA_RUC_DV_AUX'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(429720833330390703)
,p_name=>'P6507_PNA_DV_AUX'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(499521584143646647)
,p_name=>'P6507_PNA_DOC_TIPO'
,p_is_required=>true
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_prompt=>'Tipo Documento'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPO_DOCUMENTO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT T.TDOC_DESC, T.TDOC_CODIGO ',
'FROM GEN_TIPO_DOC T',
'WHERE T.TDOC_EMPR = :P_EMPRESA',
''))
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_tag_css_classes=>'no_null'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(591557036854659348)
,p_name=>'P6507_PNA_HILA_RUC'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(633252638954559624)
,p_name=>'P6507_COD_CIUDAD'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Ciudad :'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select t.ciu_cod || ''-'' || t.ciu_desc nombre, t.ciu_cod codigo',
'  from fin_ciudad t',
' where t.ciu_empr = :p_empresa',
' order by 2'))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_07=>'CIUDADES'
,p_attribute_08=>'500'
,p_attribute_09=>'500'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(633252772299559625)
,p_name=>'P6507_CIU_DESC'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_api.id(51442715326132109)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'readonly '
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_help_text=>unistr('Requerido solo para cuando se le va hacer el pago a trav\00E9s de Regional')
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(642422453257969532)
,p_name=>'P6507_PNA_NOM_CODEUDOR3'
,p_item_sequence=>420
,p_item_plug_id=>wwv_flow_api.id(53509946729059128)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Co-Deudor N\00BA 3:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(642422538756969533)
,p_name=>'P6507_PNA_NRODOC_CODEUDOR3'
,p_item_sequence=>440
,p_item_plug_id=>wwv_flow_api.id(53509946729059128)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Nro Documento Co-Deudor N\00BA 3:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(709374230848554836)
,p_name=>'P6507_CLI_EMPRESA_FUNCIONARIO'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_prompt=>'<spa style=color:red;>Empresa Funcionario :</spa>'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select e.empr_codigo||''-''||e.empr_razon_social d,',
'       e.empr_codigo r',
'from gen_empresa e'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'%'
,p_cSize=>30
,p_tag_css_classes=>'no_null'
,p_tag_attributes=>'tabindex=42'
,p_colspan=>4
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_07=>'CATEGORIAS'
,p_attribute_08=>'500'
,p_attribute_09=>'500'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(709374327595554837)
,p_name=>'P6507_CLI_EMPRESA_FUNC_VIEW'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_api.id(51903516165561243)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(119514024289420685)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(709374425502554838)
,p_name=>'P6507_CATEG_FUNC_GRUPO_EMPR'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(53511783232059146)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select f.conf_cat_cli_func_grupo',
'from fin_configuracion f',
'where f.conf_empr=:p_empresa'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_is_persistent=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(82607437619054244)
,p_validation_name=>'codigo en blanco'
,p_validation_sequence=>10
,p_validation=>'P6507_PNA_CODIGO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>unistr('Ingrese el c\00F3digo de persona para seguir!')
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(52795272050525829)
,p_name=>'PP_TRAER_DESC_BANC'
,p_event_sequence=>10
,p_triggering_element_type=>'COLUMN'
,p_triggering_region_id=>wwv_flow_api.id(51975914225655949)
,p_triggering_element=>'CTA_BCO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(52795342920525830)
,p_event_id=>wwv_flow_api.id(52795272050525829)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_TRAER_DESC_BANC(I_CTA_BCO  => :CTA_BCO,',
'                                 I_EMPRESA  => :P_EMPRESA,',
'                                 I_BCO_DESC => :BCO_DESC);',
'END;',
''))
,p_attribute_02=>'CTA_BCO'
,p_attribute_03=>'BCO_DESC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(82606686592054236)
,p_name=>'VERIFICAR SI EMPREADO EXISTE '
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_PNA_CODIGO'
,p_condition_element=>'P6507_PNA_CODIGO'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(82606706471054237)
,p_event_id=>wwv_flow_api.id(82606686592054236)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'SELECT ''X''',
'INTO :P6507_PNA_EXIST',
'FROM FIN_PERSONA T',
'WHERE T.PNA_CODIGO = :P6507_PNA_CODIGO',
'AND T.PNA_EMPR = :P_EMPRESA;',
'EXCEPTION',
' WHEN NO_DATA_FOUND THEN',
'  NULL;',
'END ;'))
,p_attribute_02=>'P6507_PNA_CODIGO'
,p_attribute_03=>'P6507_PNA_EXIST'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(52795630053525833)
,p_name=>'PP_TRAER_DESC_MON_CTABANCARIA'
,p_event_sequence=>20
,p_triggering_element_type=>'COLUMN'
,p_triggering_region_id=>wwv_flow_api.id(51975914225655949)
,p_triggering_element=>'CTA_MON'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(52795794152525834)
,p_event_id=>wwv_flow_api.id(52795630053525833)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_TRAER_DESC_MON_CTABANCARIA(I_EMPRESA     => :P_EMPRESA,',
'                                            I_MON_SIMBOLO => :MON_SIMBOLO,',
'                                            I_CTA_MON     => :CTA_MON);',
'END;',
''))
,p_attribute_02=>'CTA_MON'
,p_attribute_03=>'MON_SIMBOLO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(52795817586525835)
,p_name=>'CARGAR_BLOQUE_FIN_PERSONA'
,p_event_sequence=>30
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_IND_CREAR'
,p_condition_element=>'P6507_IND_CREAR'
,p_triggering_condition_type=>'NOT_EQUALS'
,p_triggering_expression=>'C'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(82607537570054245)
,p_event_id=>wwv_flow_api.id(52795817586525835)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ALERT'
,p_attribute_01=>'PARA MODIFICACION VUELVA A LA SECCION DE LISTA'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(52795996745525836)
,p_event_id=>wwv_flow_api.id(52795817586525835)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P6507_PNA_CODIGO IS NULL THEN',
'  RAISE_APPLICATION_ERROR(-20003,''No puede ser nulo!'');',
'END IF;',
' ',
'DECLARE',
'  V_PER NUMBER;',
'BEGIN',
'   ',
'  SELECT PNA_CODIGO INTO V_PER',
'    FROM FIN_PERSONA',
'   WHERE PNA_CODIGO = :P6507_PNA_CODIGO ',
'   AND PNA_EMPR = :P_EMPRESA;',
'',
' if FINM300_PKG.fin_persona_restringida(:P6507_PNA_CODIGO, :p_empresa)=''S'' then',
'  	RAISE_APPLICATION_ERROR(-20010,''La persona del documento se encuentra Restringida!'');',
'  end if; ',
'  ',
'   SELECT PNA_NOMBRE,',
'       PNA_TIPO,',
'       PNA_PAIS,',
'       PNA_RUC_DV,',
'       PNA_DV,',
'       PNA_DIRECCION,',
'       PNA_DOM_LATITUD_GRA,',
'       PNA_DOM_LATITUD_MIN,',
'       PNA_DOM_LONGITUD_GRA,',
'       PNA_DOM_LONGITUD_MIN,',
'       PNA_TELEFONO,',
'       PNA_FAX,',
'       PNA_CELULAR,',
'       PNA_EMAIL,',
'       PNA_EMAIL_TESO,',
'       PNA_NOTA_LIQUIDACION,',
'       PNA_MULTINACIONAL,',
'       PNA_IND_CONTROL_LC,',
'       PNA_IND_LIBRE_FAC_GA_ADM,',
'       PNA_SEGMENTO,',
'       PNA_EMPR,',
'       PNA_SUC,',
'       PNA_VENDEDOR,',
'       PNA_NOM_CONYUGE,',
'       PNA_DOC_IDENT_CONYUGE,',
'       PNA_LUGAR_ORIGEN_REPLICA,',
'       PNA_FEC_NAC,',
'       PNA_NOM_CODEUDOR1,',
'       PNA_NRODOC_CODEUDOR1,',
'       PNA_NOM_CODEUDOR2,',
'       PNA_NRODOC_CODEUDOR2,',
'       PNA_NOM_CODEUDOR3,       ',
'       PNA_NRODOC_CODEUDOR3,',
'       PNA_CONTADOR_NOMB,',
'       PNA_CONTADOR_CEL,',
'	   pna_doc_tipo',
'   INTO',
'       :P6507_PNA_NOMBRE,',
'       :P6507_PNA_TIPO,',
'       :P6507_PNA_PAIS,',
'       :P6507_PNA_RUC_DV,',
'       :P6507_PNA_DV,',
'       :P6507_PNA_DIRECCION,',
'       :P6507_PNA_DOM_LATITUD_GRA,',
'       :P6507_PNA_DOM_LATITUD_MIN,',
'       :P6507_PNA_DOM_LONGITUD_GRA,',
'       :P6507_PNA_DOM_LONGITUD_MIN,',
'       :P6507_PNA_TELEFONO,',
'       :P6507_PNA_FAX,',
'       :P6507_PNA_CELULAR,',
'       :P6507_PNA_EMAIL,',
'       :P6507_PNA_EMAIL_TESO,',
'       :P6507_PNA_NOTA_LIQUIDACION,',
'       :P6507_PNA_MULTINACIONAL,',
'       :P6507_PNA_IND_CONTROL_LC,',
'       :P6507_PNA_IND_LIBRE_FAC_GA_ADM,',
'       :P6507_PNA_SEGMENTO,',
'       :P6507_PNA_EMPR,',
'       :P6507_PNA_SUC,',
'       :P6507_PNA_VENDEDOR,',
'       :P6507_PNA_NOM_CONYUGE,',
'       :P6507_PNA_DOC_IDENT_CONYUGE,',
'       :P6507_PNA_LUGAR_ORIGEN_REPLICA,',
'       :P6507_PNA_FEC_NAC,',
'       :P6507_PNA_NOM_CODEUDOR1,',
'       :P6507_PNA_NRODOC_CODEUDOR1,',
'       :P6507_PNA_NOM_CODEUDOR2,',
'       :P6507_PNA_NRODOC_CODEUDOR2,',
'       :P6507_PNA_NOM_CODEUDOR3,',
'       :P6507_PNA_NRODOC_CODEUDOR3,',
'       :P6507_PNA_CONTADOR_NOMB,',
'       :P6507_PNA_CONTADOR_CEL,',
'	   :P6507_PNA_DOC_TIPO',
'  FROM FIN_PERSONA T',
' WHERE PNA_CODIGO = :P6507_PNA_CODIGO',
'   AND PNA_EMPR = :P_EMPRESA;',
'',
'  IF FIN_PERSONA_RESTRINGIDA(:P6507_PNA_CODIGO, :P_EMPRESA)=''S'' THEN',
'    RAISE_APPLICATION_ERROR(-20004,''La persona del documento se encuentra Restringida!'');',
'  END IF;',
'      ',
'EXCEPTION',
' WHEN NO_DATA_FOUND THEN',
'       null;',
'     /*     :P6507_PNA_NOMBRE:=null;',
'       :P6507_PNA_TIPO:=null;',
'       :P6507_PNA_PAIS:=null;',
'       :P6507_PNA_RUC_DV:=null;',
'       :P6507_PNA_DV:=null;',
'       :P6507_PNA_DIRECCION:=null;',
'       :P6507_PNA_DOM_LATITUD_GRA:=null;',
'       :P6507_PNA_DOM_LATITUD_MIN:=null;',
'       :P6507_PNA_DOM_LONGITUD_GRA:=null;',
'       :P6507_PNA_DOM_LONGITUD_MIN:=null;',
'       :P6507_PNA_TELEFONO:=null;',
'       :P6507_PNA_FAX:=null;',
'       :P6507_PNA_CELULAR:=null;',
'       :P6507_PNA_EMAIL:=null;',
'       :P6507_PNA_EMAIL_TESO:=null;',
'       :P6507_PNA_NOTA_LIQUIDACION:=null;',
'       :P6507_PNA_MULTINACIONAL:=null;',
'       :P6507_PNA_IND_CONTROL_LC:=null;',
'       :P6507_PNA_IND_LIBRE_FAC_GA_ADM:=null;',
'       :P6507_PNA_SEGMENTO:=null;',
'       :P6507_PNA_EMPR:=null;',
'       :P6507_PNA_SUC:=null;',
'       :P6507_PNA_VENDEDOR:=null;',
'       :P6507_PNA_NOM_CONYUGE:=null;',
'       :P6507_PNA_DOC_IDENT_CONYUGE:=null;',
'       :P6507_PNA_LUGAR_ORIGEN_REPLICA:=null;',
'       :P6507_PNA_FEC_NAC:=null;',
'       :P6507_PNA_NOM_CODEUDOR1:=null;',
'       :P6507_PNA_NRODOC_CODEUDOR1:=null;',
'       :P6507_PNA_NOM_CODEUDOR2:=null;',
'       :P6507_PNA_NRODOC_CODEUDOR2:=null;',
'       :P6507_PNA_CONTADOR_NOMB:=null;',
'       :P6507_PNA_CONTADOR_CEL:=null;*/',
'END;',
''))
,p_attribute_02=>'P6507_PNA_CODIGO'
,p_attribute_03=>'P6507_PNA_RUC_DV,P6507_PNA_NOMBRE,P6507_PNA_TIPO,P6507_PNA_PAIS,P6507_PNA_RUC_DV,P6507_PNA_DV,P6507_PNA_DIRECCION,P6507_PNA_DOM_LATITUD_GRA,P6507_PNA_DOM_LATITUD_MIN,P6507_PNA_DOM_LONGITUD_GRA,P6507_PNA_DOM_LONGITUD_MIN,P6507_PNA_TELEFONO,P6507_PNA_F'
||'AX,P6507_PNA_CELULAR,P6507_PNA_EMAIL,P6507_PNA_EMAIL_TESO,P6507_PNA_NOTA_LIQUIDACION,P6507_PNA_MULTINACIONAL,P6507_PNA_IND_CONTROL_LC,P6507_PNA_IND_LIBRE_FAC_GA_ADM,P6507_PNA_SEGMENTO,P6507_PNA_EMPR,P6507_PNA_SUC,P6507_PNA_VENDEDOR,P6507_PNA_NOM_CONY'
||'UGE,P6507_PNA_DOC_IDENT_CONYUGE,P6507_PNA_LUGAR_ORIGEN_REPLICA,P6507_PNA_FEC_NAC,P6507_PNA_NOM_CODEUDOR1,P6507_PNA_NRODOC_CODEUDOR1,P6507_PNA_NOM_CODEUDOR2,P6507_PNA_NRODOC_CODEUDOR2,,P6507_PNA_NRODOC_CODEUDOR3,P6507_PNA_NOM_CODEUDOR3,P6507_PNA_CONTA'
||'DOR_NOMB,P6507_PNA_CONTADOR_CEL,P6507_PNA_DOC_TIPO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(53084296061906717)
,p_event_id=>wwv_flow_api.id(52795817586525835)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'SELECT PROV_CODIGO,',
'       PROV_RUC_DV,',
'       PROV_DV,',
'       PROV_TIMBRADO,',
'       PROV_RAZON_SOCIAL,',
'       PROV_DIR,',
'       PROV_TEL,',
'       PROV_CELULAR,',
'       PROV_FAX,',
'       PROV_RUC,',
'       PROV_IND_RETENCION,',
'       PROV_PERS_CONTACTO,',
'       PROV_PERS_CONTACTO2,',
'       PROV_PROPIETARIO,',
'       PROV_EST_PROV,',
'       PROV_OBS,',
'       PROV_TIPO,',
'       PROV_LUGAR_ORIGEN_REPLICA,',
'       PROV_RETENCION,',
'       PROV_PAIS,',
'       PROV_EMAIL,',
'       PROV_CONTADOR_NOMB,',
'       PROV_CONTADOR_CEL,',
'       PROV_EMPR,',
'       PROV_CIUDAD',
'  INTO :P6507_PROV_CODIGO,',
'       :P6507_PROV_RUC_DV,',
'       :P6507_PROV_DV,',
'       :P6507_PROV_TIMBRADO,',
'       :P6507_PROV_RAZON_SOCIAL,',
'       :P6507_PROV_DIR,',
'       :P6507_PROV_TEL,',
'       :P6507_PROV_CELULAR,',
'       :P6507_PROV_FAX,',
'       :P6507_PROV_RUC,',
'       :P6507_PROV_IND_RETENCION,',
'       :P6507_PROV_PERS_CONTACTO,',
'       :P6507_PROV_PERS_CONTACTO2,',
'       :P6507_PROV_PROPIETARIO,',
'       :P6507_PROV_EST_PROV,',
'       :P6507_PROV_OBS,',
'       :P6507_PROV_TIPO,',
'       :P6507_PROV_LUGAR_ORIGEN_REPLIC,',
'       :P6507_PROV_RETENCION,',
'       :P6507_PROV_PAIS,',
'       :P6507_PROV_EMAIL,',
'       :P6507_PROV_CONTADOR_NOMB,',
'       :P6507_PROV_CONTADOR_CEL,',
'       :P6507_PROV_EMPR,',
'       :P6507_COD_CIUDAD',
'  FROM FIN_PROVEEDOR',
' WHERE PROV_CODIGO = :P6507_PNA_CODIGO',
'   AND PROV_EMPR = :P_EMPRESA;',
' EXCEPTION',
'  WHEN NO_DATA_FOUND THEN',
'       NULL;',
'   END;',
''))
,p_attribute_02=>'P6507_PNA_CODIGO'
,p_attribute_03=>'P6507_PROV_CODIGO,P6507_PROV_RUC_DV,P6507_PROV_DV,P6507_PROV_TIMBRADO,P6507_PROV_RAZON_SOCIAL,P6507_PROV_DIR,P6507_PROV_TEL,P6507_PROV_CELULAR,P6507_PROV_FAX,P6507_PROV_RUC,P6507_PROV_IND_RETENCION,P6507_PROV_PERS_CONTACTO,P6507_PROV_PERS_CONTACTO2,P'
||'6507_PROV_PROPIETARIO,P6507_PROV_EST_PROV,P6507_PROV_OBS,P6507_PROV_TIPO,P6507_PROV_LUGAR_ORIGEN_REPLIC,P6507_PROV_RETENCION,P6507_PROV_PAIS,P6507_PROV_EMAIL,P6507_PROV_CONTADOR_NOMB,P6507_PROV_CONTADOR_CEL,P6507_PROV_EMPR,P6507_COD_CIUDAD'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(53084362186906718)
,p_event_id=>wwv_flow_api.id(52795817586525835)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'SELECT CLI_CODIGO,',
'       CLI_RUC_DV,',
'       CLI_DV,',
'       CLI_NOM,',
'       CLI_EMAIL,',
'       CLI_PROPIETARIO,',
'       CLI_DOC_IDENT_PROPIETARIO,',
'       CLI_PERS_CONTACTO,',
'       CLI_DOC_IDENT_CONTACTO,',
'       CLI_DIR,',
'       CLI_OBS,',
'       CLI_ZONA,',
'       CLI_CATEG,',
'       CLI_RAMO,',
'       CLI_DIR_PARTICULAR,',
'       CLI_IND_EXIGIR_RESP_FCRED,',
'       CLI_COD_EMPL_EMPR_ORIG,',
'       CLI_FACT_COMB_GS,',
'       CLI_PORC_FLETE,',
'       CLI_IMP_LIM_CR,',
'       CLI_RUC,',
'       CLI_MAX_DIAS_ATRASO,',
'       CLI_IND_POTENCIAL,',
'       CLI_EST_CLI,',
'       CLI_TEL,',
'       CLI_TEL_PARTICULAR,',
'       CLI_VALOR_AGREGADO,',
'       CLI_FEC_ACTUALIZACION,',
'       CLI_FEC_INGRESO,',
'       CLI_MON,',
'       CLI_FAX,',
'       CLI_BLOQ_LIM_CR,',
'       CLI_LUGAR_ORIGEN_REPLICA,',
'       CLI_PAIS,',
'       CLI_EMPR,',
'       cli_empresa_funcionario',
'  INTO :P6507_CLI_CODIGO,',
'       :P6507_CLI_RUC_DV,',
'       :P6507_CLI_DV,',
'       :P6507_CLI_NOM,',
'       :P6507_CLI_EMAIL,',
'       :P6507_CLI_PROPIETARIO,',
'       :P6507_CLI_DOC_IDENT_PROPIETARI,',
'       :P6507_CLI_PERS_CONTACTO,',
'       :P6507_CLI_DOC_IDENT_CONTACTO,',
'       :P6507_CLI_DIR,',
'       :P6507_CLI_OBS,',
'       :P6507_CLI_ZONA,',
'       :P6507_CLI_CATEG,',
'       :P6507_CLI_RAMO,',
'       :P6507_CLI_DIR_PARTICULAR,',
'       :P6507_CLI_IND_EXIGIR_RESP_FCRE,',
'       :P6507_CLI_COD_EMPL_EMPR_ORIG,',
'       :P6507_CLI_FACT_COMB_GS,',
'       :P6507_CLI_PORC_FLETE,',
'       :P6507_CLI_IMP_LIM_CR,',
'       :P6507_CLI_RUC,',
'       :P6507_CLI_MAX_DIAS_ATRASO,',
'       :P6507_CLI_IND_POTENCIAL,',
'       :P6507_CLI_EST_CLI,',
'       :P6507_CLI_TEL,',
'       :P6507_CLI_TEL_PARTICULAR,',
'       :P6507_CLI_VALOR_AGREGADO,',
'       :P6507_CLI_FEC_ACTUALIZACION,',
'       :P6507_CLI_FEC_INGRESO,',
'       :P6507_CLI_MON,',
'       :P6507_CLI_FAX,',
'       :P6507_CLI_BLOQ_LIM_CR,',
'       :P6507_CLI_LUGAR_ORIGEN_REPLICA,',
'       :P6507_CLI_PAIS,',
'       :P6507_CLI_EMPR,',
'       :P6507_CLI_EMPRESA_FUNCIONARIO',
'  FROM FIN_CLIENTE',
' WHERE CLI_CODIGO = :P6507_PNA_CODIGO',
'   AND CLI_EMPR = :P_EMPRESA;',
'EXCEPTION',
' WHEN NO_DATA_FOUND THEN',
'  ',
'       NULL;',
' END;',
''))
,p_attribute_02=>'P6507_PNA_CODIGO'
,p_attribute_03=>'P6507_CLI_CODIGO,P6507_CLI_RUC_DV,P6507_CLI_DV,P6507_CLI_NOM,P6507_CLI_EMAIL,P6507_CLI_PROPIETARIO,P6507_CLI_DOC_IDENT_PROPIETARI,P6507_CLI_PERS_CONTACTO,P6507_CLI_DOC_IDENT_CONTACTO,P6507_CLI_DIR,P6507_CLI_OBS,P6507_CLI_ZONA,P6507_CLI_CATEG,P6507_CL'
||'I_RAMO,P6507_CLI_DIR_PARTICULAR,P6507_CLI_IND_EXIGIR_RESP_FCRE,P6507_CLI_COD_EMPL_EMPR_ORIG,P6507_CLI_FACT_COMB_GS,P6507_CLI_PORC_FLETE,P6507_CLI_IMP_LIM_CR,P6507_CLI_RUC,P6507_CLI_MAX_DIAS_ATRASO,P6507_CLI_IND_POTENCIAL,P6507_CLI_EST_CLI,P6507_CLI_T'
||'EL,P6507_CLI_TEL_PARTICULAR,P6507_CLI_VALOR_AGREGADO,P6507_CLI_FEC_ACTUALIZACION,P6507_CLI_FEC_INGRESO,P6507_CLI_MON,P6507_CLI_FAX,P6507_CLI_BLOQ_LIM_CR,P6507_CLI_LUGAR_ORIGEN_REPLICA,P6507_CLI_PAIS,P6507_CLI_EMPR,P6507_CLI_EMPRESA_FUNCIONARIO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(52796027613525837)
,p_name=>'PP_MOSTRAR_TIPO_PERSONA'
,p_event_sequence=>40
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_PNA_TIPO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(52796180429525838)
,p_event_id=>wwv_flow_api.id(52796027613525837)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P6507_PNA_TIPO IS NULL THEN ',
'	 RAISE_APPLICATION_ERROR(-20004,''Ingrese el tipo de persona!'');	',
'END IF;',
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_MOSTRAR_TIPO_PERSONA(I_TIPP_DESC => :P6507_TIPP_DESC,',
'                                      I_PNA_TIPO  => :P6507_PNA_TIPO,',
'                                      I_EMPRESA   => :P_EMPRESA);',
'END;',
''))
,p_attribute_02=>'P6507_PNA_TIPO'
,p_attribute_03=>'P6507_TIPP_DESC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(52796255387525839)
,p_name=>'PP_TRAER_DESC_PAIS'
,p_event_sequence=>50
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_PNA_PAIS'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(52796349989525840)
,p_event_id=>wwv_flow_api.id(52796255387525839)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P6507_PNA_PAIS <> 1  THEN',
'   :P6507_PNA_RUC_DV := 99999901;',
'   if :P6507_PNA_DOC_TIPO = 2 then',
'   :P6507_PNA_DV:=FINM300_PKG.PA_CALCULAR_DV_11_A(:P6507_PNA_RUC_DV);',
'   else',
'   :P6507_PNA_DV:= null;',
'   end if;',
'   :P6507_PNA_RUC_DV_AUX := 99999901;',
'    if :P6507_PNA_DOC_TIPO = 2 then',
'   :P6507_PNA_DV_AUX := FINM300_PKG.PA_CALCULAR_DV_11_A(:P6507_PNA_RUC_DV);',
'   else',
'    :P6507_PNA_DV_AUX := null;',
'   end if;',
'END IF;',
'',
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_TRAER_DESC_PAIS(I_PNA_PAIS  => :P6507_PNA_PAIS,',
'                                 I_PAIS_DESC => :P6507_PAIS_DESC,',
'                                 I_EMPRESA   => :P_EMPRESA);',
'END;',
'',
'',
''))
,p_attribute_02=>'P6507_PNA_PAIS,P6507_PNA_RUC_DV,P6507_PNA_DOC_TIPO'
,p_attribute_03=>'P6507_PNA_DV_AUX,P6507_PNA_RUC_DV_AUX,P6507_PNA_RUC_DV ,P6507_PNA_DV,P6507_PAIS_DESC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
end;
/
begin
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(52796493815525841)
,p_name=>'P6507_PNA_SEGMENTO '
,p_event_sequence=>60
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_PNA_SEGMENTO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(52796587686525842)
,p_event_id=>wwv_flow_api.id(52796493815525841)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P6507_PNA_SEGMENTO IS NOT NULL THEN ',
'   BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_TRAER_DESC_SEGMENTO(I_SEG_DESC     => :P6507_SEG_DESC,',
'                                     I_PNA_SEGMENTO => :P6507_PNA_SEGMENTO,',
'                                     I_EMPRESA      => :P_EMPRESA);',
'END;',
'',
'ELSE',
'   :P6507_SEG_DESC   :=     NULL;',
'END IF;',
''))
,p_attribute_02=>'P6507_PNA_SEGMENTO'
,p_attribute_03=>'P6507_SEG_DESC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(52796617255525843)
,p_name=>'PP_TRAER_DESC_SUCURSAL'
,p_event_sequence=>70
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_PNA_SUC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(52796787275525844)
,p_event_id=>wwv_flow_api.id(52796617255525843)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P6507_PNA_SUC IS NOT NULL THEN ',
'   BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_TRAER_DESC_SUCURSAL(I_SUC_DESC => :P6507_SUC_DESC,',
'                                     I_EMPRESA  => :P_EMPRESA,',
'                                     I_PNA_SUC  => :P6507_PNA_SUC);',
'END;',
'',
'ELSE',
'--   :PNA_EMPR  :=    NULL;',
'   :P6507_SUC_DESC   :=     NULL;',
'END IF;',
''))
,p_attribute_02=>'P6507_PNA_SUC'
,p_attribute_03=>'P6507_SUC_DESC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(52796849368525845)
,p_name=>'PP_MOSTRAR_VENDEDOR'
,p_event_sequence=>80
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_PNA_VENDEDOR'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(52796960706525846)
,p_event_id=>wwv_flow_api.id(52796849368525845)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P6507_PNA_VENDEDOR IS NOT NULL THEN ',
'   BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_MOSTRAR_VENDEDOR(I_VEND_NOMBRE  => :P6507_VEND_NOMBRE,',
'                                  I_PNA_VENDEDOR => :P6507_PNA_VENDEDOR,',
'                                  I_EMPRESA      => :P_EMPRESA);',
'END;',
'',
'ELSE',
'   :P6507_VEND_NOMBRE  :=     NULL;',
'END IF;',
''))
,p_attribute_02=>'P6507_PNA_VENDEDOR'
,p_attribute_03=>'P6507_VEND_NOMBRE'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(52797065251525847)
,p_name=>'MOSTRAR_ESTADO'
,p_event_sequence=>90
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_PROV_EST_PROV'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(52797171868525848)
,p_event_id=>wwv_flow_api.id(52797065251525847)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  IF :P6507_PROV_EST_PROV IS NULL THEN',
'    RAISE_APPLICATION_ERROR(-20009,''Estado no puede ser nulo!'');',
'  END IF;',
'  --',
'  IF :P6507_PROV_EST_PROV NOT IN (''A'', ''I'') THEN',
unistr('     RAISE_APPLICATION_ERROR(-20010,''Opci\00F3n no v\00E1lida - Ingrese A o I!'');'),
'  ELSE',
'     IF    :P6507_PROV_EST_PROV = ''A'' THEN',
'           :P6507_S_EST_DESC := ''ACTIVO'';',
'     ELSIF :P6507_PROV_EST_PROV = ''I'' THEN',
'           :P6507_S_EST_DESC := ''INACTIVO'';',
'     END IF;',
'  END IF;',
'END;'))
,p_attribute_02=>'P6507_PROV_EST_PROV'
,p_attribute_03=>'P6507_S_EST_DESC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(52797250020525849)
,p_name=>'PP_TRAER_TIPO_PROVEEDOR'
,p_event_sequence=>100
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_PROV_TIPO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(52797365664525850)
,p_event_id=>wwv_flow_api.id(52797250020525849)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_TRAER_TIPO_PROVEEDOR(I_PROV_TIPO => :P6507_PROV_TIPO,',
'                                      I_EMPRESA   => :P_EMPRESA,',
'                                      I_TIPR_DESC => :P6507_TIPR_DESC);',
'END;',
''))
,p_attribute_02=>'P6507_PROV_TIPO'
,p_attribute_03=>'P6507_TIPR_DESC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(53082673656906701)
,p_name=>'PP_TRAER_DESC_ZONA'
,p_event_sequence=>110
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_CLI_ZONA'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(53082714410906702)
,p_event_id=>wwv_flow_api.id(53082673656906701)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P6507_CLI_ZONA IS NULL THEN',
'	RAISE_aPPLICATION_ERROR(-20012,''No puede ser nulo!.'');',
'ELSE	',
' BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_TRAER_DESC_ZONA(I_ZONA_DESC => :P6507_ZONA_DESC,',
'                                 I_CLI_ZONA  => :P6507_CLI_ZONA,',
'                                 I_EMPRESA   => :P_EMPRESA);',
'END;',
'',
'END IF;'))
,p_attribute_02=>'P6507_CLI_ZONA'
,p_attribute_03=>'P6507_ZONA_DESC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(53082864833906703)
,p_name=>'PP_TRAER_DESC_CATE'
,p_event_sequence=>120
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_CLI_CATEG'
,p_condition_element=>'P6507_CLI_CATEG'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(53082974890906704)
,p_event_id=>wwv_flow_api.id(53082864833906703)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  V_CONTADOR NUMBER := 0;',
'  l_cat_funcionario_grupo number;',
'BEGIN',
'   l_cat_funcionario_grupo := :P6507_CATEG_FUNC_GRUPO_EMPR;',
'   ',
'   SELECT COUNT(1)',
'    INTO V_CONTADOR',
'    FROM FIN_CLIENTE C, PER_EMPLEADO E',
'  WHERE C.CLI_RUC_DV(+) = TO_CHAR(E.EMPL_DOC_IDENT)',
'    AND E.EMPL_SITUACION = ''A''',
'    AND TO_CHAR(E.EMPL_DOC_IDENT) = :P6507_PNA_RUC_DV;',
'    ',
'  IF V_CONTADOR > 0 THEN',
'    IF :P6507_CLI_CATEG NOT IN (3, 4, 5, l_cat_funcionario_grupo) THEN',
unistr('        RAISE_APPLICATION_ERROR(-20013,''Este cliente no puede pertenecer a esta categor\00EDa'');'),
'    ELSE',
'        BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_TRAER_DESC_CATE(I_FCAT_DESC => :P6507_FCAT_DESC,',
'                                 I_CLI_CATEG => :P6507_CLI_CATEG,',
'                                 I_EMPRESA   => :P_EMPRESA);',
'END;',
'',
'    END IF;',
'  ELSIF V_CONTADOR < 1 THEN',
'      IF :P6507_CLI_CATEG NOT IN (1, 2, 6, 7, 8, 9, l_cat_funcionario_grupo) THEN',
unistr('           RAISE_APPLICATION_ERROR(-20014,''Este cliente no puede pertenecer a esta categor\00EDa'');'),
'      ELSE',
'          BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_TRAER_DESC_CATE(I_FCAT_DESC => :P6507_FCAT_DESC,',
'                                 I_CLI_CATEG => :P6507_CLI_CATEG,',
'                                 I_EMPRESA   => :P_EMPRESA);',
'END;',
'',
'      END IF;  ',
'  END IF;',
'',
'END;',
''))
,p_attribute_02=>'P6507_PNA_RUC_DV,P6507_CLI_CATEG,P6507_CATEG_FUNC_GRUPO_EMPR'
,p_attribute_03=>'P6507_FCAT_DESC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(53083040289906705)
,p_name=>'PP_TRAER_DESC_RAMO'
,p_event_sequence=>130
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_CLI_RAMO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(53083122551906706)
,p_event_id=>wwv_flow_api.id(53083040289906705)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF  :P6507_CLI_RAMO IS NULL THEN',
'  RAISE_APPLICATION_ERROR(-20015,''Debe ingresarse un ramo!.'');',
'ELSE',
'  BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_TRAER_DESC_RAMO(I_CLI_RAMO  => :P6507_CLI_RAMO,',
'                                 I_EMPRESA   => :P_EMPRESA,',
'                                 I_RAMO_DESC => :P6507_RAMO_DESC);',
'END;',
'',
'END IF;',
''))
,p_attribute_02=>'P6507_CLI_RAMO'
,p_attribute_03=>'P6507_RAMO_DESC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(53083206986906707)
,p_name=>'IF P6507_PNA_HILA_COD IS NOT NULL THEN'
,p_event_sequence=>140
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_PNA_HILA_COD'
,p_condition_element=>'P6507_PNA_HILA_COD'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(53084046609906715)
,p_event_id=>wwv_flow_api.id(53083206986906707)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P6507_PNA_NOMBRE IS NULL THEN',
'  RAISE_APPLICATION_ERROR(-20001,''El nombre de la persona no puede ser nula!'');',
'END IF;',
'IF :P6507_PNA_HILA_COD IS NOT NULL THEN',
'   :P6507_PNA_TIPO := 1;',
'   :P6507_PNA_PAIS := 1;',
'   :P6507_PNA_SUC := 7;',
'   :P6507_PNA_VENDEDOR := 323;',
'   :P6507_COD_CIUDAD:=61;',
'   :P6507_PNA_EMAIL:=''MAIL@SINCORREO.COM.PY'';',
'   :P6507_PNA_RUC_DV:=:P6507_PNA_HILA_RUC;',
'   :P6507_PNA_DOC_IDENT_CONYUGE := :P6507_PNA_RUC_DV;',
'   :P6507_PNA_DOC_TIPO:=1;',
'   if :P6507_PNA_DOC_TIPO = 2 then',
'    ',
'   :P6507_PNA_DV:=FINM300_PKG.PA_CALCULAR_DV_11_A(:P6507_PNA_RUC_DV);',
'   ',
'   else',
'   :P6507_PNA_DV:= 0;',
'   end if;',
'  ',
'--RAISE_APPLICATION_ERROR(-20001,''aaaaaaaaaaa'');',
'  :P6507_PROV_TIMBRADO := 0;',
'  :P6507_PROV_TIPO := 10;',
' ',
'  :P6507_PROV_EST_PROV := ''A'';',
'  :P6507_S_EST_DESC := ''ACTIVO'';',
'  ',
'  ',
'  :P6507_CLI_ZONA := 20;',
'  :P6507_CLI_CATEG := 4;',
'  :P6507_CLI_RAMO := 3;',
'  ',
'  :P6507_CLI_COD_EMPL_EMPR_ORIG := :P6507_PNA_HILA_COD;',
'  :P6507_CLI_IMP_LIM_CR := 0;',
'  :P6507_CLI_EST_CLI := ''A'';',
'  :P6507_CLI_FACT_COMB_GS := ''N'';',
'   :P6507_PNA_SEGMENTO := 3;',
'  ',
'END IF;'))
,p_attribute_02=>'P6507_PNA_HILA_RUC,P6507_PNA_NOMBRE,P6507_PNA_HILA_RUC,P6507_PNA_RUC_DV,P6507_PNA_PAIS'
,p_attribute_03=>'P6507_PNA_EMAIL,P6507_PNA_PAIS,P6507_PNA_RUC_DV,P6507_PNA_DOC_TIPO,P6507_COD_CIUDAD,P6507_PNA_SEGMENTO,P6507_PNA_TIPO,P6507_PNA_PAIS,P6507_PNA_SEGMENTO,P6507_PNA_SUC,P6507_PNA_VENDEDOR,P6507_PNA_DOC_IDENT_CONYUGE,P6507_PROV_TIMBRADO,P6507_PROV_TIPO,P'
||'6507_PROV_EST_PROV,P6507_S_EST_DESC,P6507_CLI_ZONA,P6507_CLI_CATEG,P6507_CLI_RAMO,P6507_CLI_COD_EMPL_EMPR_ORIG,P6507_CLI_IMP_LIM_CR,P6507_CLI_EST_CLI,P6507_CLI_FACT_COMB_GS'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(53083628018906711)
,p_name=>'PP_TRAER_EMPLEADO'
,p_event_sequence=>150
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_CLI_COD_EMPL_EMPR_ORIG'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(53083721980906712)
,p_event_id=>wwv_flow_api.id(53083628018906711)
,p_event_result=>'TRUE'
,p_action_sequence=>350
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_TRAER_EMPLEADO(I_CLI_CATEG                => :P6507_CLI_CATEG,',
'                                I_CLI_COD_EMPL_EMPR_ORIG   => :P6507_CLI_COD_EMPL_EMPR_ORIG,',
'                                in_empresa                 => :P_EMPRESA,',
'                                in_empresa_func_grupo      => :P6507_CLI_EMPRESA_FUNCIONARIO,',
'                                I_S_CLI_COD_EMPL_EMPR_ORIG => :P6507_S_CLI_COD_EMPL_EMPR_ORIG',
'                               );',
'END;',
''))
,p_attribute_02=>'P6507_CLI_CATEG,P6507_CLI_COD_EMPL_EMPR_ORIG,P_EMPRESA,P6507_CLI_EMPRESA_FUNCIONARIO'
,p_attribute_03=>'P6507_S_CLI_COD_EMPL_EMPR_ORIG'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(53085056109906725)
,p_name=>'PP_TRAER_DESC_MON'
,p_event_sequence=>160
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_CLI_MON'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(53085135676906726)
,p_event_id=>wwv_flow_api.id(53085056109906725)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_TRAER_DESC_MON(I_EMPRESA     => :P_EMPRESA,',
'                                I_CLI_MON     => :P6507_CLI_MON,',
'                                I_MON_SIMBOLO => :P6507_MON_SIMBOLO);',
'END;',
''))
,p_attribute_02=>'P6507_CLI_MON'
,p_attribute_03=>'P6507_MON_SIMBOLO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(53085210181906727)
,p_name=>'REFRESCAR_DETALLE'
,p_event_sequence=>170
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_IND_CREAR'
,p_condition_element=>'P6507_IND_CREAR'
,p_triggering_condition_type=>'NOT_EQUALS'
,p_triggering_expression=>'C'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(53085408503906729)
,p_event_id=>wwv_flow_api.id(53085210181906727)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_CARGRA_FIN_CLI_CTA_BCO(I_EMPRESA    => :P_EMPRESA,',
'                                        I_PNA_CODIGO => :P6507_PNA_CODIGO);',
'END;',
''))
,p_attribute_02=>'P6507_PNA_CODIGO'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(82607628112054246)
,p_event_id=>wwv_flow_api.id(53085210181906727)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ALERT'
,p_attribute_01=>'PARA MODIFICACION VUELVA A LA SECCION DE LISTA'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(53085321992906728)
,p_event_id=>wwv_flow_api.id(53085210181906727)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(51975914225655949)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(53508871578059117)
,p_name=>'PP_MOSTRAR_CALIDAD_SUELO'
,p_event_sequence=>180
,p_triggering_element_type=>'COLUMN'
,p_triggering_region_id=>wwv_flow_api.id(53086205160906737)
,p_triggering_element=>'LOTE_CALIDAD_SUELO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(53508940598059118)
,p_event_id=>wwv_flow_api.id(53508871578059117)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_MOSTRAR_CALIDAD_SUELO(LOTE_CALIDAD_SUELO => :LOTE_CALIDAD_SUELO,',
'                                       I_EMPRESA          => :P_EMPRESA,',
'                                       CAL_DESC           => :CAL_DESC);',
'END;',
''))
,p_attribute_02=>'LOTE_CALIDAD_SUELO'
,p_attribute_03=>'CAL_DESC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(53509062956059119)
,p_name=>'PP_MOSTRAR_LOCALIDAD'
,p_event_sequence=>190
,p_triggering_element_type=>'COLUMN'
,p_triggering_region_id=>wwv_flow_api.id(53086205160906737)
,p_triggering_element=>'LOTE_LOCALIDAD'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(53509152996059120)
,p_event_id=>wwv_flow_api.id(53509062956059119)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_MOSTRAR_LOCALIDAD(LOC_DESC       => :LOC_DESC,',
'                                   DIST_DESC      => :DIST_DESC,',
'                                   LOTE_LOCALIDAD => :LOTE_LOCALIDAD,',
'                                   I_EMPRESA      => :P_EMPRESA);',
'END;',
''))
,p_attribute_02=>'LOTE_LOCALIDAD'
,p_attribute_03=>'LOC_DESC,DIST_DESC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(53509286468059121)
,p_name=>'PP_MOSTRAR_STATUS'
,p_event_sequence=>200
,p_triggering_element_type=>'COLUMN'
,p_triggering_region_id=>wwv_flow_api.id(53086205160906737)
,p_triggering_element=>'LOTE_STATUS'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(53509373085059122)
,p_event_id=>wwv_flow_api.id(53509286468059121)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_MOSTRAR_STATUS(LOTE_STATUS      => :LOTE_STATUS,',
'                                LOTE_STATUS_DESC => :LOTE_STATUS_DESC);',
'END;',
''))
,p_attribute_02=>'LOTE_STATUS'
,p_attribute_03=>'LOTE_STATUS_DESC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(53509425161059123)
,p_name=>'PP_MOSTRAR_TIPO_ALQUILER'
,p_event_sequence=>210
,p_triggering_element_type=>'COLUMN'
,p_triggering_region_id=>wwv_flow_api.id(53086205160906737)
,p_triggering_element=>'LOTE_TIPO_ALQUILER'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(53509546563059124)
,p_event_id=>wwv_flow_api.id(53509425161059123)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_MOSTRAR_TIPO_ALQUILER(TIPO_DESC          => :TIPO_DESC,',
'                                       I_EMPRESA          => :P_EMPRESA,',
'                                       LOTE_TIPO_ALQUILER => :LOTE_TIPO_ALQUILER);',
'END;',
''))
,p_attribute_02=>'LOTE_TIPO_ALQUILER'
,p_attribute_03=>'TIPO_DESC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(53511202068059141)
,p_name=>'PP_VALIDAR_NRO_DOC_CONYUGE'
,p_event_sequence=>220
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_PNA_DOC_IDENT_CONYUGE'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(53511305864059142)
,p_event_id=>wwv_flow_api.id(53511202068059141)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P6507_PNA_DOC_IDENT_CONYUGE IS NOT NULL THEN ',
'	 BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_VALIDAR_NRO_DOC_CONYUGE(I_PNA_CODIGO            => :P6507_PNA_CODIGO,',
'                                         I_PNA_DOC_IDENT_CONYUGE => :P6507_PNA_DOC_IDENT_CONYUGE ,',
'                                         I_EMPRESA               => :P_EMPRESA,',
'                                         I_PNA_NOMBRE            => :P6507_PNA_NOMBRE);',
'END;',
'END IF;',
''))
,p_attribute_02=>'P6507_PNA_DOC_IDENT_CONYUGE,P6507_PNA_CODIGO,P6507_PNA_NOMBRE'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(53511501149059144)
,p_name=>'TRAER_ESTADO_DE_CLIENTE'
,p_event_sequence=>230
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_CLI_EST_CLI'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(53511629544059145)
,p_event_id=>wwv_flow_api.id(53511501149059144)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  IF :P6507_CLI_EST_CLI IS NULL THEN',
'  RAISE_APPLICATION_ERROR(-20006,''No puede ser nulo!'');',
'END IF;',
'--',
'BEGIN',
'  IF    :P6507_CLI_EST_CLI = ''A'' THEN',
'        :P6507_S_EST_CLI := ''ACTIVO'';',
'  ELSIF :P6507_CLI_EST_CLI = ''M'' THEN',
'        :P6507_S_EST_CLI := ''MOROSO'';',
'  ELSIF :P6507_CLI_EST_CLI = ''J'' THEN',
'        :P6507_S_EST_CLI := ''JUDICIAL'';',
'  ELSIF :P6507_CLI_EST_CLI = ''I'' THEN',
'        :P6507_S_EST_CLI := ''INACTIVO'';',
'  ELSIF :P6507_CLI_EST_CLI = ''N'' THEN',
'        :P6507_S_EST_CLI := ''INCOBRABLE'';',
'  ELSE',
unistr('        RAISE_APPLICATION_ERROR(-20005,''Opci\00F3n no v\00E1lida - Ingrese A, I, M, J o N!'');'),
'  END IF;',
'END;'))
,p_attribute_02=>'P6507_CLI_EST_CLI'
,p_attribute_03=>'P6507_S_EST_CLI'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(54072404815084804)
,p_name=>'OCULTAR_CONFIG'
,p_event_sequence=>240
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(54072541636084805)
,p_event_id=>wwv_flow_api.id(54072404815084804)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(53511783232059146)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(85016573825060143)
,p_event_id=>wwv_flow_api.id(54072404815084804)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_FOCUS'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P6507_PNA_CODIGO'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(54073246397084812)
,p_name=>'PP_BORRAR_FIN_CLI_CTA_BCO'
,p_event_sequence=>260
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_SEQ_ID_BCO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(54073344600084813)
,p_event_id=>wwv_flow_api.id(54073246397084812)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_BORRAR_FIN_CLI_CTA_BCO(SEQ_ID_BCO => :P6507_SEQ_ID_BCO);',
'END;*/',
'APEX_COLLECTION.DELETE_MEMBER(P_COLLECTION_NAME => ''FIN_CLI_CTA_BCO'',',
'                              P_SEQ             => :P6507_SEQ_ID_BCO);'))
,p_attribute_02=>'P6507_SEQ_ID_BCO'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(54073440216084814)
,p_event_id=>wwv_flow_api.id(54073246397084812)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(51975914225655949)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(54076325204084843)
,p_name=>'PP_BORRAR_LOTE'
,p_event_sequence=>270
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_SEQ_AREA'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(54076418010084844)
,p_event_id=>wwv_flow_api.id(54076325204084843)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_BORRAR_LOTE(SEQ_AREA => :P6507_SEQ_AREA);',
'END;',
''))
,p_attribute_02=>'P6507_SEQ_AREA'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(54076591626084845)
,p_event_id=>wwv_flow_api.id(54076325204084843)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(53086205160906737)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(54076734670084847)
,p_name=>'validacion'
,p_event_sequence=>280
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_PNA_PAIS'
,p_condition_element=>'P6507_PNA_PAIS'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'1'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(349969388694802311)
,p_event_id=>wwv_flow_api.id(54076734670084847)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'$("#P6507_PNA_RUC_DV").attr("disabled", true);',
'$("#P6507_PNA_DV").attr("disabled", true);'))
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(349969224666802310)
,p_event_id=>wwv_flow_api.id(54076734670084847)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'$("#P6507_PNA_RUC_DV").attr("disabled", false);',
'$("#P6507_PNA_DV").attr("disabled", false);'))
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(54561768494898301)
,p_name=>'calcular ruc persona'
,p_event_sequence=>290
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_PNA_RUC_DV'
,p_bind_type=>'bind'
,p_bind_event_type=>'focusout'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(337851067830930828)
,p_event_id=>wwv_flow_api.id(54561768494898301)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P6507_PNA_RUC_DV IS NOT NULL THEN',
'DECLARE',
'BEGIN',
' -- SELECT TO_NUMBER(:P6507_PNA_RUC_DV) INTO :P6507_PNA_RUC_DV FROM DUAL;',
' SELECT :P6507_PNA_RUC_DV INTO :P6507_PNA_RUC_DV FROM DUAL;',
'EXCEPTION',
'  WHEN INVALID_NUMBER THEN',
'    SELECT (REPLACE(REPLACE(REPLACE(:P6507_PNA_RUC_DV, ''-'',NULL),'' '',NULL),',
'                             (SELECT SUBSTR(:P6507_PNA_RUC_DV,-1) FROM DUAL)))',
'      INTO :P6507_PNA_RUC_DV',
'      FROM DUAL;',
'END;',
'END IF;',
''))
,p_attribute_02=>'P6507_PNA_RUC_DV'
,p_attribute_03=>'P6507_PNA_RUC_DV'
,p_attribute_04=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(54561838497898302)
,p_event_id=>wwv_flow_api.id(54561768494898301)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P6507_PNA_RUC_DV IS NULL THEN',
'  RAISE_APPLICATION_ERROR(-20001,''Ruc puede ser nulo!'');',
'END IF;',
'',
'DECLARE',
'	CURSOR C IS',
'	SELECT PNA_CODIGO||''-''||PNA_NOMBRE NOMBRE',
'	  FROM FIN_PERSONA',
'	 WHERE PNA_CODIGO<>:P6507_PNA_CODIGO',
'	   AND PNA_RUC_DV=:P6507_PNA_RUC_DV',
'	   AND PNA_EMPR = :P_EMPRESA;',
'',
'BEGIN',
'',
'	  IF :P6507_PNA_RUC_DV  NOT in (''999999101'', ''99999901'')  THEN --Cuando sea ruc de proveedores internacionales, saltar esa parte, porque tienen el mismo ruc todos.',
'	  ',
'	  ',
'',
'	  FOR V IN C LOOP',
'	      RAISE_APPLICATION_ERROR(-20003,''El proveedor ''||V.NOMBRE||'', ya posee este ruc'');	   ',
'	    null;',
'	  END LOOP; ',
'	  end if;',
'	  ',
'	  IF :P6507_PNA_PAIS <> 1 THEN',
'			:P6507_PNA_RUC_DV := 99999901;',
'			IF :P6507_PNA_DOC_TIPO = 2 THEN',
'				:P6507_PNA_DV:=Pa_Calcular_Dv_11_A(:P6507_PNA_RUC_DV);',
'			ELSE',
'			 :P6507_PNA_DV:= 0;',
'			END IF;',
'		END IF;',
'END;	  ',
'',
'',
'',
'',
'',
'',
''))
,p_attribute_02=>'P6507_PNA_RUC_DV,P6507_PNA_PAIS,P6507_PNA_CODIGO,P6507_PNA_DOC_TIPO'
,p_attribute_03=>'P6507_PNA_RUC_DV,P6507_PNA_DV'
,p_attribute_04=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(54562349515898307)
,p_event_id=>wwv_flow_api.id(54561768494898301)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--PP_VERIFICA_RUCC;',
'DECLARE ',
'V_PNA_DV NUMBER;',
'BEGIN',
'IF :P6507_PNA_RUC_DV IS NOT NULL AND :P6507_PNA_DOC_TIPO = 2 THEN',
'  :P6507_PNA_DV :=PA_CALCULAR_DV_11_A(:P6507_PNA_RUC_DV);',
'  ELSE ',
'   :P6507_PNA_DV := NULL;',
'END IF;   ',
'',
'IF :P6507_CLI_DV IS NOT NULL AND :P6507_PNA_DOC_TIPO = 2  THEN',
'    :P6507_CLI_DV := :P6507_CLI_RUC_DV||''-''||:P6507_CLI_DV;',
'ELSE',
' 	:P6507_CLI_DV :=  :P6507_CLI_RUC_DV;',
'',
'END IF;',
'      ',
'',
'',
'END;'))
,p_attribute_02=>'P6507_PNA_RUC_DV,P6507_CLI_RUC_DV,P6507_CLI_DV'
,p_attribute_03=>'P6507_PNA_DV,P6507_CLI_DV'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(54561995622898303)
,p_name=>'definir valor de max dias atraso'
,p_event_sequence=>300
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_CLI_MAX_DIAS_ATRASO'
,p_bind_type=>'bind'
,p_bind_event_type=>'focusout'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(54562047026898304)
,p_event_id=>wwv_flow_api.id(54561995622898303)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P6507_CLI_MAX_DIAS_ATRASO IS NULL THEN',
'  :P6507_CLI_MAX_DIAS_ATRASO := 30;',
'END IF;'))
,p_attribute_02=>'P6507_CLI_MAX_DIAS_ATRASO'
,p_attribute_03=>'P6507_CLI_MAX_DIAS_ATRASO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(54562167263898305)
,p_name=>'validar nro de verificacion'
,p_event_sequence=>310
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_PNA_DV'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(54562212031898306)
,p_event_id=>wwv_flow_api.id(54562167263898305)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P6507_PNA_DV IS NULL AND :P6507_PNA_DOC_TIPO  = 2 THEN',
'  raise_application_error(-20010,''Digito Verificador no puede ser nulo!'');',
'END IF;',
''))
,p_attribute_02=>'P6507_PNA_DV,P6507_PNA_DOC_TIPO'
,p_wait_for_result=>'Y'
);
end;
/
begin
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(54562416260898308)
,p_name=>'FP_REND_PROD'
,p_event_sequence=>320
,p_triggering_element_type=>'COLUMN'
,p_triggering_region_id=>wwv_flow_api.id(53086205160906737)
,p_triggering_element=>'CAL_DESC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(54562507749898309)
,p_event_id=>wwv_flow_api.id(54562416260898308)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT FINM300_PKG.FP_REND_PROD(1, :LOTE_CALIDAD_SUELO, :LOTE_TAM_HAS),',
'       FINM300_PKG.FP_REND_PROD(2, :LOTE_CALIDAD_SUELO, :LOTE_TAM_HAS),',
'       FINM300_PKG.FP_REND_PROD(3, :LOTE_CALIDAD_SUELO, :LOTE_TAM_HAS),',
'       FINM300_PKG.FP_REND_PROD(4, :LOTE_CALIDAD_SUELO, :LOTE_TAM_HAS)',
'  INTO :LOTE_HAS_SOJA, :LOTE_HAS_SOJA_Z, :LOTE_HAS_MAIZ, :LOTE_HAS_TRIGO',
'  FROM DUAL;',
'',
''))
,p_attribute_02=>'LOTE_CALIDAD_SUELO,LOTE_TAM_HAS'
,p_attribute_03=>'LOTE_HAS_SOJA, LOTE_HAS_SOJA_Z, LOTE_HAS_MAIZ, LOTE_HAS_TRIGO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(54562619846898310)
,p_name=>'PP_CARGAR_LOTES'
,p_event_sequence=>330
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_IND_CREAR'
,p_condition_element=>'P6507_IND_CREAR'
,p_triggering_condition_type=>'NOT_EQUALS'
,p_triggering_expression=>'C'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(53508062450059109)
,p_event_id=>wwv_flow_api.id(54562619846898310)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_CARGAR_LOTES(I_EMPRESA    => :P_EMPRESA,',
'                              I_PNA_CODIGO => :P6507_PNA_CODIGO);',
'END;',
''))
,p_attribute_02=>'P6507_PNA_CODIGO'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(82607775095054247)
,p_event_id=>wwv_flow_api.id(54562619846898310)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ALERT'
,p_attribute_01=>'PARA MODIFICACION VUELVA A LA SECCION DE LISTA'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(53508110430059110)
,p_event_id=>wwv_flow_api.id(54562619846898310)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(53086205160906737)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(82607084304054240)
,p_name=>'alerta'
,p_event_sequence=>340
,p_triggering_element_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_element=>'document'
,p_bind_type=>'bind'
,p_bind_event_type=>'custom'
,p_bind_event_type_custom=>'alerta'
,p_display_when_type=>'NEVER'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(82607220164054242)
,p_event_id=>wwv_flow_api.id(82607084304054240)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'raise_Application_error(-20001,''Los elementos de color rojo no pueden ser nulos, favor rellenar los campos!'');'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(82607869798054248)
,p_name=>'alerta de modificacion'
,p_event_sequence=>350
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_PNA_EXIST'
,p_condition_element=>'P6507_PNA_EXIST'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'X'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(82607928686054249)
,p_event_id=>wwv_flow_api.id(82607869798054248)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P6507_IND_CREAR = ''C'' THEN',
unistr(' RAISE_APPLICATION_ERROR(-20010,''El c\00F3digo ya fue utilizado para otra persona, pruebe con otro!'');'),
'END IF;'))
,p_attribute_02=>'P6507_IND_CREAR'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(85016341664060141)
,p_name=>'CALCULAR NRO DE PERSONA'
,p_event_sequence=>360
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_PNA_CODIGO'
,p_condition_element=>'P6507_PNA_CODIGO'
,p_triggering_condition_type=>'NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'focusout'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(85016486495060142)
,p_event_id=>wwv_flow_api.id(85016341664060141)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  CURSOR PNACUR IS',
'     SELECT PNA_CODIGO',
'       FROM FIN_PERSONA',
'       WHERE PNA_EMPR= :P_EMPRESA',
'      ORDER BY PNA_CODIGO;',
'      ',
'  V_CONT NUMBER := 1;',
'BEGIN',
'  IF :P6507_PNA_CODIGO IS NOT NULL THEN',
'    raise_application_error(-20001,''Primero debe aceptar o cancelar los cambios hechos!'');',
'  END IF;',
'',
'  FOR RPNA IN PNACUR LOOP',
'    IF RPNA.PNA_CODIGO > V_CONT THEN',
'      EXIT;',
'    ELSIF RPNA.PNA_CODIGO < V_CONT THEN',
'      NULL; --leer sig.cliente hasta empatar con V_CONT',
'    ELSE',
'      V_CONT := V_CONT + 1; --si cliente = V_CONT entonces aumentar 1 y leer sig.cliente',
'    END IF;',
'  END LOOP;',
'',
'  ',
unistr('  :P6507_PNA_CODIGO := V_CONT; --el codigo V_CONT est\00E1 libre y se puede usar'),
unistr('  --Pedido de Ariel Balmori, 13/02/2020, para que sea autom\00E1tico el check de generar retenci\00F3n'),
'  :P6507_PROV_IND_RETENCION := ''S'';',
'  ',
'EXCEPTION',
'  WHEN NO_DATA_FOUND THEN',
'    :P6507_PNA_CODIGO := 0;',
'END;',
''))
,p_attribute_02=>'P6507_PNA_CODIGO'
,p_attribute_03=>'P6507_PNA_CODIGO,P6507_PROV_IND_RETENCION'
,p_attribute_04=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(499521667093646648)
,p_name=>'AL_CAMB_TIPO_DOC'
,p_event_sequence=>370
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_PNA_DOC_TIPO'
,p_condition_element=>'P6507_PNA_DOC_TIPO'
,p_triggering_condition_type=>'NOT_EQUALS'
,p_triggering_expression=>'2'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(499521719436646649)
,p_event_id=>wwv_flow_api.id(499521667093646648)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_CLEAR'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P6507_PNA_DV,P6507_PNA_DV_AUX,P6507_PROV_DV,P6507_CLI_DV'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(633252836782559626)
,p_name=>'P6507_COD_CIUDAD'
,p_event_sequence=>380
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_COD_CIUDAD'
,p_condition_element=>'P6507_COD_CIUDAD'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(633252902144559627)
,p_event_id=>wwv_flow_api.id(633252836782559626)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  -- Call the procedure',
'  finm301_pkg.pp_traer_desc_ciudad(i_ciu_desc => :P6507_CIU_DESC,',
'                                   i_ciu_cod  => :P6507_COD_CIUDAD,',
'                                   i_empresa  => :P_EMPRESA);',
'end;',
''))
,p_attribute_02=>'P6507_COD_CIUDAD'
,p_attribute_03=>'P6507_CIU_DESC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(709374553281554839)
,p_name=>'onChange Cli Cat view CLI EMPR FUNCIONARIO'
,p_event_sequence=>390
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_CLI_CATEG'
,p_condition_element=>'P6507_CLI_CATEG'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'&P6507_CATEG_FUNC_GRUPO_EMPR.'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(709374604691554840)
,p_event_id=>wwv_flow_api.id(709374553281554839)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P6507_CLI_EMPRESA_FUNCIONARIO,P6507_CLI_EMPRESA_FUNC_VIEW'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(709374703957554841)
,p_event_id=>wwv_flow_api.id(709374553281554839)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P6507_CLI_EMPRESA_FUNCIONARIO,P6507_CLI_EMPRESA_FUNC_VIEW'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(709374861984554842)
,p_name=>'obt descripcion empresa'
,p_event_sequence=>400
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6507_CLI_EMPRESA_FUNCIONARIO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(709374920751554843)
,p_event_id=>wwv_flow_api.id(709374861984554842)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select e.empr_razon_social',
'into :P6507_CLI_EMPRESA_FUNC_VIEW',
'from gen_empresa e',
'where e.empr_codigo = :P6507_CLI_EMPRESA_FUNCIONARIO;'))
,p_attribute_02=>'P6507_CLI_EMPRESA_FUNCIONARIO'
,p_attribute_03=>'P6507_CLI_EMPRESA_FUNC_VIEW'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(53508621537059115)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(51975914225655949)
,p_process_type=>'NATIVE_IG_DML'
,p_process_name=>'GUAREDAR_FIN_CLI_CTA_BCO '
,p_attribute_01=>'PLSQL_CODE'
,p_attribute_04=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :APEX$ROW_STATUS  IN (''C'') THEN ',
'APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => ''FIN_CLI_CTA_BCO'',',
'                                 P_N001            => :P6507_PNA_CODIGO,',
'                                 P_N002            => :CTA_BCO,',
'                                 P_C001            => :BCO_DESC,',
'                                 P_C007            => :CTA_NRO,',
'                                 P_C002            => :CTA_TITULAR,',
'                                 P_C003            => :CTA_TITULAR_CI,',
'                                 P_N004            => :P_EMPRESA,',
'                                 P_C004            => :CTA_MON,',
'                                 P_C005            => :MON_SIMBOLO,',
'                                 P_C006            => :CTA_PREDET);',
' ELSIF :APEX$ROW_STATUS  IN (''U'') THEN ',
' --RAISE_APPLICATION_ERROR(-20010,''AAAAAAAAAAA'');',
'APEX_COLLECTION.UPDATE_MEMBER(P_COLLECTION_NAME => ''FIN_CLI_CTA_BCO'',',
'                           P_SEQ             => :SEQ_ID,',
'                           P_N001            => :P6507_PNA_CODIGO,',
'                           P_N002            => :CTA_BCO,',
'                           P_C001            => :BCO_DESC,',
'                           P_C007            => :CTA_NRO,',
'                           P_C002            => :CTA_TITULAR,',
'                           P_C003            => :CTA_TITULAR_CI,',
'                           P_N004            => :P_EMPRESA,',
'                           P_C004            => :CTA_MON,',
'                           P_C005            => :MON_SIMBOLO,',
'                           P_C006            => :CTA_PREDET);',
'                                 ',
'END IF;   '))
,p_attribute_05=>'N'
,p_attribute_06=>'Y'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(53510462280059133)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(53509705284059126)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(53086205160906737)
,p_process_type=>'NATIVE_IG_DML'
,p_process_name=>'GUARDAR_AREA'
,p_attribute_01=>'PLSQL_CODE'
,p_attribute_04=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :LOTE_LOCALIDAD IS NULL THEN',
' RAISE_APPLICATION_ERROR(-20010,''La localidad no puede ser nula!'');',
'END IF;',
'',
'IF :LOTE_OBS_LOC IS NULL THEN',
' RAISE_APPLICATION_ERROR(-20010,''La obs de localidad no puede ser nula!'');',
'END IF;',
'',
'IF :LOTE_TAM_HAS IS NULL THEN',
' RAISE_APPLICATION_ERROR(-20010,''La hectarea no puede ser nula!'');',
'END IF;',
'',
'IF :LOTE_CALIDAD_SUELO IS NULL THEN',
' RAISE_APPLICATION_ERROR(-20010,''La calidad del suelo no puede ser nula!'');',
'END IF;',
'',
'',
' ',
' ',
'IF :APEX$ROW_STATUS  IN (''C'') THEN ',
'',
'',
'APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => ''AREA'',',
'                                 P_N001            => :P6507_PNA_CODIGO,',
'                                 P_N002            => :LOTE_NUMERO,',
'                                 P_C001            => :LOTE_LOCALIDAD,',
'                                 P_C002            => :LOC_DESC,',
'                                 P_C003            => :LOTE_OBS_LOC,',
'                                 P_N003            => :LOTE_TAM_HAS,',
'                                 P_C004            => :LOTE_STATUS,',
'                                 P_C005            => :LOTE_STATUS_DESC,',
'                                 P_C006            => :LOTE_IND_HIPOTECA,',
'                                 P_C007            => :LOTE_NOM_HIPOTECA,',
'                                 P_C008            => :LOTE_TIPO_ALQUILER,',
'                                 P_C009            => :LOTE_CALIDAD_SUELO,',
'                                 P_C010            => :CAL_DESC,',
'                                 P_C011            => :LOTE_HAS_SOJA,',
'                                 P_C012            => :LOTE_HAS_SOJA_Z,',
'                                 P_C013            => :LOTE_HAS_MAIZ,',
'                                 P_C014            => :LOTE_HAS_TRIGO,',
'                                 P_C016            => :P_EMPRESA,',
'                                 P_C017            => :TIPO_DESC,',
'                                 P_C018            => :DIST_DESC);',
'                                 ',
'  ',
'ELSIF :APEX$ROW_STATUS  IN (''U'') THEN      ',
'--APEX_COLLECTION.CREATE_OR_TRUNCATE_COLLECTION(P_COLLECTION_NAME => ''AREA'');',
'    APEX_COLLECTION.UPDATE_MEMBER(P_COLLECTION_NAME => ''AREA'',',
'                                 P_SEQ    => :SEQ_ID,',
'                                 P_N001            => :P6507_PNA_CODIGO,',
'                                 P_N002            => :LOTE_NUMERO,',
'                                 P_C001            => :LOTE_LOCALIDAD,',
'                                 P_C002            => :LOC_DESC,',
'                                 P_C003            => :LOTE_OBS_LOC,',
'                                 P_N003            => :LOTE_TAM_HAS,',
'                                 P_C004            => :LOTE_STATUS,',
'                                 P_C005            => :LOTE_STATUS_DESC,',
'                                 P_C006            => :LOTE_IND_HIPOTECA,',
'                                 P_C007            => :LOTE_NOM_HIPOTECA,',
'                                 P_C008            => :LOTE_TIPO_ALQUILER,',
'                                 P_C009            => :LOTE_CALIDAD_SUELO,',
'                                 P_C010            => :CAL_DESC,',
'                                 P_C011            => :LOTE_HAS_SOJA,',
'                                 P_C012            => :LOTE_HAS_SOJA_Z,',
'                                 P_C013            => :LOTE_HAS_MAIZ,',
'                                 P_C014            => :LOTE_HAS_TRIGO,',
'                                 P_C016            => :P_EMPRESA,',
'                                 P_C017            => :TIPO_DESC,',
'                                 P_C018            => :DIST_DESC);',
'                                                            ',
'END IF;',
''))
,p_attribute_05=>'N'
,p_attribute_06=>'N'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(53510462280059133)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(53511029527059139)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'CANCELAR'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(53510549310059134)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(54076613727084846)
,p_process_sequence=>60
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PP_ACTUALIZAR_REGISTRO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_ACTUALIZAR_REGISTRO(I_EMPRESA                   => :P_EMPRESA,',
'                                     I_PNA_CODIGO                => :P6507_PNA_CODIGO,',
'                                     I_CLI_FEC_INGRESO           => :P6507_PNA_FEC_INGRESO,',
'                                     I_PNA_LUGAR_ORIGEN_REPLICA  => :P6507_PNA_LUGAR_ORIGEN_REPLICA,',
'                                     I_PROV_LUGAR_ORIGEN_REPLICA => :P6507_PROV_LUGAR_ORIGEN_REPLIC,',
'                                     I_CLI_LUGAR_ORIGEN_REPLICA  => :P6507_CLI_LUGAR_ORIGEN_REPLICA,',
'                                     I_CLI_FEC_ACTUALIZACION     => :P6507_CLI_FEC_ACTUALIZACION,',
'                                     I_CLI_MON                   => :P6507_CLI_MON,',
'                                     I_MON_LOC                   => :P6507_MON_LOC);',
'END;',
''))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(53510462280059133)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(54562894447898312)
,p_process_sequence=>70
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PP_BORRAR_REGISTRO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_BORRAR_REGISTRO;',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(53510600575059135)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(53085613734906731)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'DELETE_COLLECTION '
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF APEX_COLLECTION.COLLECTION_EXISTS ( p_collection_name => ''FIN_CLI_CTA_BCO'') THEN',
'APEX_COLLECTION.DELETE_COLLECTION (',
'    p_collection_name => ''FIN_CLI_CTA_BCO'');',
'END IF;',
'',
'IF APEX_COLLECTION.COLLECTION_EXISTS ( p_collection_name => ''AREA'') THEN',
'APEX_COLLECTION.DELETE_COLLECTION (',
'    p_collection_name => ''AREA'');',
'END IF;',
'',
'',
'',
'APEX_COLLECTION.CREATE_OR_TRUNCATE_COLLECTION(P_COLLECTION_NAME => ''BOR_CLI_CTA_BCO'');',
'',
'APEX_COLLECTION.CREATE_OR_TRUNCATE_COLLECTION(P_COLLECTION_NAME => ''AREA_BORRAR'');',
''))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(54072375106084803)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PP_CARGAR_DATOS'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINM300_PKG.PP_CARGAR_DATOS(I_EMPRESA => :P_EMPRESA, BLOQ => :P6507_BLOQ);',
'END;',
':P6507_LOGIN := :APP_USER;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
prompt --application/end_environment
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
