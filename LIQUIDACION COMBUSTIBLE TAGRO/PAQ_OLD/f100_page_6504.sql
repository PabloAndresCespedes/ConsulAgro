prompt --application/set_environment
set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_200100 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2020.03.31'
,p_release=>'20.1.0.00.13'
,p_default_workspace_id=>2933390818164287
,p_default_application_id=>100
,p_default_id_offset=>10209955456998265921
,p_default_owner=>'ADCS'
);
end;
/
 
prompt APPLICATION 100 - FINANZAS
--
-- Application Export:
--   Application:     100
--   Name:            FINANZAS
--   Date and Time:   18:03 Tuesday August 23, 2022
--   Exported By:     PABLOC
--   Flashback:       0
--   Export Type:     Page Export
--   Manifest
--     PAGE: 6504
--   Manifest End
--   Version:         20.1.0.00.13
--   Instance ID:     211687171918188
--

begin
null;
end;
/
prompt --application/pages/delete_06504
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>6504);
end;
/
prompt --application/pages/page_06504
begin
wwv_flow_api.create_page(
 p_id=>6504
,p_user_interface_id=>wwv_flow_api.id(10120616725720470822)
,p_name=>'FINI237 - COBRO COMBUSTIBLE A FUNC HILAGRO'
,p_alias=>'FINI237-COBRO-COMBUSTIBLE-A-FUNC-HILAGRO'
,p_step_title=>'FINI237 - COBRO COMBUSTIBLE A FUNC HILAGRO'
,p_autocomplete_on_off=>'OFF'
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'AMENDEZ'
,p_last_upd_yyyymmddhh24miss=>'20210623155802'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(10178332462199670140)
,p_plug_name=>'Detalle'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(10123821061576213271)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT A.SEQ_ID,',
'       A.C001   CLIENTE,',
'       A.C002   NOMBRE,',
'       A.C003   PROV_DEST,',
'       A.C004   FACTURA,',
'       A.C005   FECHA,',
'       A.C006   FEC_CUOTA,',
'       TO_NUMBER(A.C007)   IMPORTE,',
'       A.C008   TIPO_MOV,',
'       A.C009   LINEA_NEGOCIO,',
'       A.C010   EMPLEADO,',
'       A.C011   TMOV_ABREV,',
'       A.C012   LNEGOCIO_DESC,',
'       A.C013   DOC_CLAVE,',
'       A.C014   FCAT_CODIGO,',
'       A.C015   PROCESAR',
'  FROM APEX_COLLECTIONS A',
' WHERE A.COLLECTION_NAME = ''FINI257_DETALLE''',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P6504_CONSULTAR'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'MILLIMETERS'
,p_prn_paper_size=>'A4'
,p_prn_width=>297
,p_prn_height=>210
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header=>'Detalle'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#EEEEEE'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'bold'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#FFFFFF'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_prn_border_color=>'#666666'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(10178332518601670141)
,p_max_row_count=>'1000000'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_show_display_row_count=>'Y'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:EMAIL:XLS:PDF:RTF'
,p_owner=>'AMENDEZ'
,p_internal_uid=>173979332813877532
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10178332780941670143)
,p_db_column_name=>'SEQ_ID'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Seq'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10178332802089670144)
,p_db_column_name=>'CLIENTE'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Cliente'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10178332985711670145)
,p_db_column_name=>'NOMBRE'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Nombre'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10178333046423670146)
,p_db_column_name=>'PROV_DEST'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Prov Dest'
,p_column_type=>'STRING'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10178333149066670147)
,p_db_column_name=>'FACTURA'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Factura'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10178333201866670148)
,p_db_column_name=>'FECHA'
,p_display_order=>60
,p_column_identifier=>'F'
,p_column_label=>'Fecha'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10178333360072670149)
,p_db_column_name=>'FEC_CUOTA'
,p_display_order=>70
,p_column_identifier=>'G'
,p_column_label=>'Vto. Cuota'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10178333520794670151)
,p_db_column_name=>'TIPO_MOV'
,p_display_order=>90
,p_column_identifier=>'I'
,p_column_label=>'Tipo Mov'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10178333650766670152)
,p_db_column_name=>'LINEA_NEGOCIO'
,p_display_order=>100
,p_column_identifier=>'J'
,p_column_label=>'Linea Negocio'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10192362035416056728)
,p_db_column_name=>'EMPLEADO'
,p_display_order=>170
,p_column_identifier=>'Q'
,p_column_label=>'Empleado'
,p_column_type=>'STRING'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10192362149004056729)
,p_db_column_name=>'TMOV_ABREV'
,p_display_order=>180
,p_column_identifier=>'R'
,p_column_label=>'Tipo mov.'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10192362273490056730)
,p_db_column_name=>'LNEGOCIO_DESC'
,p_display_order=>190
,p_column_identifier=>'S'
,p_column_label=>'Linea negocio'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10192362333877056731)
,p_db_column_name=>'DOC_CLAVE'
,p_display_order=>200
,p_column_identifier=>'T'
,p_column_label=>'Doc Clave'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10192362441803056732)
,p_db_column_name=>'FCAT_CODIGO'
,p_display_order=>210
,p_column_identifier=>'U'
,p_column_label=>'Fcat Codigo'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10192362541809056733)
,p_db_column_name=>'PROCESAR'
,p_display_order=>220
,p_column_identifier=>'V'
,p_column_label=>'Procesar'
,p_column_link=>'javascript:$s(''P6504_CAMBIAR_ESTADO'',''#SEQ_ID#'');'
,p_column_linktext=>'#PROCESAR#'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10195795535478326914)
,p_db_column_name=>'IMPORTE'
,p_display_order=>230
,p_column_identifier=>'W'
,p_column_label=>'Importe'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_format_mask=>'999G999G999G999G990D00'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(10192359092040053890)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1880060'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>100
,p_report_columns=>'SEQ_ID:PROCESAR:CLIENTE:NOMBRE:EMPLEADO:PROV_DEST:FACTURA:FECHA:FEC_CUOTA:IMPORTE:TMOV_ABREV:LNEGOCIO_DESC:'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(10196761367493119835)
,p_report_id=>wwv_flow_api.id(10192359092040053890)
,p_name=>'No procesar'
,p_condition_type=>'HIGHLIGHT'
,p_allow_delete=>'Y'
,p_column_name=>'PROCESAR'
,p_operator=>'='
,p_expr=>'No'
,p_condition_sql=>' (case when ("PROCESAR" = #APXWS_EXPR#) then #APXWS_HL_ID# end) '
,p_condition_display=>'#APXWS_COL_NAME# = ''No''  '
,p_enabled=>'Y'
,p_highlight_sequence=>10
,p_column_bg_color=>'#FFD6D2'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(10192361165301056719)
,p_plug_name=>'COBRO COMBUSTIBLE'
,p_region_template_options=>'#DEFAULT#:t-Region--noPadding:t-Region--accent14:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(10123822189127213272)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(10178331278173670128)
,p_plug_name=>'COBRO COMBUSTIBLE CAB'
,p_parent_plug_id=>wwv_flow_api.id(10192361165301056719)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--accent14:t-Region--noBorder:t-Region--scrollBody:t-Form--large:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(10123822189127213272)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(10192361251567056720)
,p_plug_name=>'Total'
,p_parent_plug_id=>wwv_flow_api.id(10192361165301056719)
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody:t-Form--labelsAbove'
,p_plug_template=>wwv_flow_api.id(10123822189127213272)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_grid_column_span=>3
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(10192360321129056711)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(10192361165301056719)
,p_button_name=>'CANCELAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#:t-Button--iconRight'
,p_button_template_id=>wwv_flow_api.id(10123867749894213294)
,p_button_image_alt=>'Cancelar'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(10178332346172670139)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(10192361165301056719)
,p_button_name=>'Consultar'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#:t-Button--iconRight'
,p_button_template_id=>wwv_flow_api.id(10123867749894213294)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Consultar'
,p_button_position=>'REGION_TEMPLATE_CREATE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(10192360233308056710)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(10178332462199670140)
,p_button_name=>'ELIMINAR'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--warning:t-Button--iconRight'
,p_button_template_id=>wwv_flow_api.id(10123867749894213294)
,p_button_image_alt=>'Eliminar'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_warn_on_unsaved_changes=>null
,p_button_condition=>'P6504_DOC_NRO_TRANS_COMBUS'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(10178334340818670159)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(10178332462199670140)
,p_button_name=>'IMPRIMIR'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--success:t-Button--iconRight'
,p_button_template_id=>wwv_flow_api.id(10123867749894213294)
,p_button_image_alt=>'Imprimir'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_warn_on_unsaved_changes=>null
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(10178332612531670142)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(10178332462199670140)
,p_button_name=>'GUARDAR'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--iconRight'
,p_button_template_id=>wwv_flow_api.id(10123867749894213294)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Guardar'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_warn_on_unsaved_changes=>null
,p_button_condition=>'P6504_DOC_NRO_TRANS_COMBUS'
,p_button_condition_type=>'ITEM_IS_NULL'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10178331316913670129)
,p_name=>'P6504_DOC_NRO_TRANS_COMBUS'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(10178331278173670128)
,p_prompt=>unistr('N\00FAmero')
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ''Nro.: '' || T.DOC_NRO_TRANS_COMBUS || '' -. Fecha: '' ||',
'       MAX(T.DOC_FEC_DOC) || ''-. Cant.: '' || COUNT(*) CANT,',
'       T.DOC_NRO_TRANS_COMBUS',
'  FROM FIN_DOCUMENTO T',
' WHERE T.DOC_EMPR = :P_EMPRESA',
'   AND T.DOC_NRO_TRANS_COMBUS IS NOT NULL',
'   AND T.DOC_FEC_DOC > SYSDATE - 300',
' GROUP BY T.DOC_NRO_TRANS_COMBUS',
' ORDER BY 1 DESC;',
''))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'%'
,p_cSize=>60
,p_field_template=>wwv_flow_api.id(10123867210077213294)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_inline_help_text=>'Se calcula, al guardar'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'N'
,p_attribute_05=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10178331460608670130)
,p_name=>'P6504_CATEG_CLI'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(10178331278173670128)
,p_prompt=>'Categoria cliente'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
' SELECT C.FCAT_CODIGO || '' - '' || FCAT_DESC, C.FCAT_CODIGO',
'   FROM FAC_CATEGORIA C',
'  WHERE FCAT_EMPR = :P_EMPRESA',
''))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(10123867210077213294)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10178331584373670131)
,p_name=>'P6504_DOC_NRO_RECIBO_INICIAL'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(10178331278173670128)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NVL(IMP_ULT_REC_PAGO_EMIT, 0) + 1 A',
'  FROM GEN_IMPRESORA',
' WHERE IMPR_IP = FP_IP_USER',
'   AND IMP_EMPR = :P_EMPRESA',
''))
,p_item_default_type=>'SQL_QUERY'
,p_prompt=>unistr('N\00FAmero recibo inicial')
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(10123867210077213294)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_03=>'right'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10178331631741670132)
,p_name=>'P6504_LINEA_DE_NEGOCIO'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(10178331278173670128)
,p_prompt=>'Linea de negocio'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC:Combustible;4,Taller y otros gastos;5,No Operativo;9,Uniforme;0'
,p_lov_display_null=>'YES'
,p_lov_null_text=>'%'
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(10123867210077213294)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10178331774776670133)
,p_name=>'P6504_DOC_FEC_OPER'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(10178331278173670128)
,p_item_default=>'SELECT TRUNC(SYSDATE) FROM DUAL'
,p_item_default_type=>'SQL_QUERY'
,p_prompt=>unistr('Fecha operaci\00F3n')
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(10123867210077213294)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10178331793582670134)
,p_name=>'P6504_DOC_FEC_DOC'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(10178331278173670128)
,p_item_default=>'SELECT TRUNC(SYSDATE) FROM DUAL'
,p_item_default_type=>'SQL_QUERY'
,p_prompt=>'Fecha comprobante'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(10123867210077213294)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10178331962958670135)
,p_name=>'P6504_FEC_VTO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(10178331278173670128)
,p_prompt=>'Fecha vencimiento'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(10123867210077213294)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10178332061884670136)
,p_name=>'P6504_CHK_CUOTA'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(10178331278173670128)
,p_prompt=>'Cuota'
,p_display_as=>'NATIVE_YES_NO'
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(10123867210077213294)
,p_item_template_options=>'#DEFAULT#'
,p_inline_help_text=>'Si marca cuota traera las cuotas que vencen en la fecha especificada'
,p_attribute_01=>'CUSTOM'
,p_attribute_02=>'S'
,p_attribute_03=>'Si'
,p_attribute_04=>'N'
,p_attribute_05=>'No'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10178332122168670137)
,p_name=>'P6504_FECHA_CORTE'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(10178331278173670128)
,p_prompt=>'Fecha de corte'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(10123867210077213294)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10178332228654670138)
,p_name=>'P6504_CLIENTE'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(10178331278173670128)
,p_prompt=>'Cliente'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTE_COD'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c.cli_codigo ||''. ''||c.cli_nom , c.cli_codigo',
'  from fin_cliente c',
' where  c.cli_empr = :P_EMPRESA',
' order by 1'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'%'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(10123867210077213294)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10192360462440056712)
,p_name=>'P6504_CLIENTE_DESC'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(10178331278173670128)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(10123867210077213294)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10192360846753056716)
,p_name=>'P6504_CATEG_CLI_DESC'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(10178331278173670128)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(10123867210077213294)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10192361326131056721)
,p_name=>'P6504_CANTIDAD'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(10192361251567056720)
,p_prompt=>'Cantidad'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_field_template=>wwv_flow_api.id(10123867210077213294)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10192361405372056722)
,p_name=>'P6504_TOTAL_IMPORTE'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(10192361251567056720)
,p_prompt=>'Total importe'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_field_template=>wwv_flow_api.id(10123867210077213294)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10192362598758056734)
,p_name=>'P6504_CAMBIAR_ESTADO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(10178332462199670140)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10195795882654326917)
,p_name=>'P6504_CANTIDAD_PROC'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(10192361251567056720)
,p_prompt=>'Cantidad a procesar'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_field_template=>wwv_flow_api.id(10123867210077213294)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10195795982461326918)
,p_name=>'P6504_TOT_IMP_PROCESAR'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(10192361251567056720)
,p_prompt=>'Total importe a procesar'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_field_template=>wwv_flow_api.id(10123867210077213294)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10195796090399326920)
,p_name=>'P6504_CONSULTAR'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(10192361165301056719)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10195797365326326932)
,p_name=>'P6504_IMPRIMIR'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(10192361165301056719)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(10192360525455056713)
,p_name=>'cliente'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6504_CLIENTE'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(10192360594763056714)
,p_event_id=>wwv_flow_api.id(10192360525455056713)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P6504_CLIENTE_DESC'
,p_attribute_01=>'SQL_STATEMENT'
,p_attribute_03=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT T.CLI_NOM',
'  FROM FIN_CLIENTE T',
' WHERE T.CLI_EMPR = :P_EMPRESA',
'   AND T.CLI_CODIGO = :P6504_CLIENTE',
''))
,p_attribute_07=>'P6504_CLIENTE'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(10192360917997056717)
,p_name=>'categoria_cliente'
,p_event_sequence=>20
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6504_CATEG_CLI'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(10192361048489056718)
,p_event_id=>wwv_flow_api.id(10192360917997056717)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P6504_CATEG_CLI_DESC'
,p_attribute_01=>'SQL_STATEMENT'
,p_attribute_03=>wwv_flow_string.join(wwv_flow_t_varchar2(
' SELECT FCAT_DESC',
'   FROM FAC_CATEGORIA C',
'  WHERE FCAT_EMPR = :P_EMPRESA',
'  AND C.FCAT_CODIGO = :P6504_CATEG_CLI',
''))
,p_attribute_07=>'P6504_CATEG_CLI'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(10192362706404056735)
,p_name=>'cambia estado'
,p_event_sequence=>30
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6504_CAMBIAR_ESTADO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(10192362832230056736)
,p_event_id=>wwv_flow_api.id(10192362706404056735)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINI237.PP_CAMBIAR_ESTADO(I_EMPRESA => :P_EMPRESA, I_SEQ => :P6504_CAMBIAR_ESTADO);',
'END;',
''))
,p_attribute_02=>'P6504_CAMBIAR_ESTADO'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(10192362980273056737)
,p_event_id=>wwv_flow_api.id(10192362706404056735)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(10178332462199670140)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(10195795670576326915)
,p_name=>'despues de refrescar'
,p_event_sequence=>40
,p_triggering_element_type=>'REGION'
,p_triggering_region_id=>wwv_flow_api.id(10178332462199670140)
,p_bind_type=>'bind'
,p_bind_event_type=>'apexafterrefresh'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(10195795698140326916)
,p_event_id=>wwv_flow_api.id(10195795670576326915)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT COUNT(*) CANTIDAD, fini237.fp_nformat(i_number => NVL(SUM(A.C007), 0)) IMPORTE',
'  INTO :P6504_CANTIDAD, :P6504_TOTAL_IMPORTE',
'  FROM APEX_COLLECTIONS A',
' WHERE A.COLLECTION_NAME = ''FINI257_DETALLE'';'))
,p_attribute_03=>'P6504_CANTIDAD,P6504_TOTAL_IMPORTE'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(10195796018394326919)
,p_event_id=>wwv_flow_api.id(10195795670576326915)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT COUNT(*) CANTIDAD, fini237.fp_nformat(i_number => NVL(SUM(A.C007), 0)) IMPORTE',
'  INTO :P6504_CANTIDAD_PROC, :P6504_TOT_IMP_PROCESAR',
'  FROM APEX_COLLECTIONS A',
' WHERE A.COLLECTION_NAME = ''FINI257_DETALLE''',
' AND NVL(A.C015,''A'') = ''Si'';'))
,p_attribute_03=>'P6504_CANTIDAD_PROC,P6504_TOT_IMP_PROCESAR'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(10195796368478326922)
,p_name=>'guardar'
,p_event_sequence=>50
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(10178332612531670142)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(10195796413804326923)
,p_event_id=>wwv_flow_api.id(10195796368478326922)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'GUARDAR'
,p_attribute_02=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(10195796506031326924)
,p_name=>'CONSULTAR_EXIST'
,p_event_sequence=>60
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6504_DOC_NRO_TRANS_COMBUS'
,p_condition_element=>'P6504_DOC_NRO_TRANS_COMBUS'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(10195796675870326925)
,p_event_id=>wwv_flow_api.id(10195796506031326924)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'EXISTE'
,p_attribute_02=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(10195797985711326938)
,p_event_id=>wwv_flow_api.id(10195796506031326924)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'CANCELAR'
,p_attribute_02=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(10195796953295326928)
,p_name=>'BORRAR'
,p_event_sequence=>70
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(10192360233308056710)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(10195797021109326929)
,p_event_id=>wwv_flow_api.id(10195796953295326928)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_CONFIRM'
,p_attribute_01=>unistr('\00BFDesea borrar?')
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(10195797120906326930)
,p_event_id=>wwv_flow_api.id(10195796953295326928)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'BORRAR'
,p_attribute_02=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(10195797444292326933)
,p_name=>'IMPRIMIR'
,p_event_sequence=>80
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6504_IMPRIMIR'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(10195797537977326934)
,p_event_id=>wwv_flow_api.id(10195797444292326933)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINI237.PP_LLAMAR_REPORT(I_NRO_COMBUS => :P6504_IMPRIMIR,',
'                           I_EMPRESA    => :P_EMPRESA);',
'END;',
''))
,p_attribute_02=>'P6504_IMPRIMIR'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(10195797819931326937)
,p_event_id=>wwv_flow_api.id(10195797444292326933)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'javascript:var myWindow = window.open(''f?p=&APP_ID.:3010:&SESSION.::::::'');'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(10195797674446326935)
,p_name=>'CLICK_IMPRIMIR'
,p_event_sequence=>90
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(10178334340818670159)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(10195797719161326936)
,p_event_id=>wwv_flow_api.id(10195797674446326935)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P6504_IMPRIMIR'
,p_attribute_01=>'FUNCTION_BODY'
,p_attribute_06=>'RETURN :P6504_DOC_NRO_TRANS_COMBUS;'
,p_attribute_07=>'P6504_DOC_NRO_TRANS_COMBUS'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(10195796211562326921)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'ACTUALIZAR_REGISTRO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  V_NRO_COMBUS NUMBER;',
'  V_MENSAJE    VARCHAR2(500);',
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINI237.PP_ACTUALIZAR_REGISTRO(I_EMPRESA                => :P_EMPRESA,',
'                                 I_CATEG_CLI              => :P6504_CATEG_CLI,',
'                                 I_DOC_FEC_OPER           => :P6504_DOC_FEC_OPER,',
'                                 I_DOC_FEC_DOC            => :P6504_DOC_FEC_DOC,',
'                                 I_LINEA_NEGOCIO          => :P6504_LINEA_DE_NEGOCIO,',
'                                 I_DOC_NRO_RECIBO_INICIAL => :P6504_DOC_NRO_RECIBO_INICIAL,',
'                                 O_NRO_TRANS_COMBUS       => V_NRO_COMBUS);',
'',
'  V_MENSAJE := ''Registro actualizado. <br><a href="javascript:$s(''''P6504_IMPRIMIR'''','' ||',
'               V_NRO_COMBUS || '');" > Nro. :'' || V_NRO_COMBUS ||',
'               '' Imprimir</a>'';',
'',
'  APEX_APPLICATION.G_PRINT_SUCCESS_MESSAGE := V_MENSAJE;',
'END;',
''))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'GUARDAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(10195796712738326926)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'EXISTENTE'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINI237.PP_CONSULTAR_EXISTENTE(I_EMPRESA => :P_EMPRESA,',
'                                 I_DOC_NRO_TRANS_COMBUS => :P6504_DOC_NRO_TRANS_COMBUS,',
'                                 O_DOC_FEC_OPER => :P6504_DOC_FEC_OPER,',
'                                 O_DOC_FEC_DOC => :P6504_DOC_FEC_DOC,',
'                                 O_DOC_NRO_INICIAL => :P6504_DOC_NRO_RECIBO_INICIAL,',
'                                 O_CATEG_CLI => :P6504_CATEG_CLI);',
'                                 ',
'                                 ',
' :P6504_CONSULTAR:= ''S'';',
'',
'END;',
'',
' '))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'EXISTE'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
);
end;
/
begin
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(10192361919807056727)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'CONSULTAR'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'  IF :P6504_DOC_NRO_TRANS_COMBUS IS NOT NULL THEN',
'    -- CALL THE PROCEDURE',
'    FINI237.PP_CONSULTAR_EXISTENTE(I_EMPRESA              => :P_EMPRESA,',
'                                   I_DOC_NRO_TRANS_COMBUS => :P6504_DOC_NRO_TRANS_COMBUS,',
'                                   O_DOC_FEC_OPER         => :P6504_DOC_FEC_OPER,',
'                                   O_DOC_FEC_DOC          => :P6504_DOC_FEC_DOC,',
'                                   O_DOC_NRO_INICIAL      => :P6504_DOC_NRO_RECIBO_INICIAL,',
'                                   O_CATEG_CLI            => :P6504_CATEG_CLI);',
'  ELSE',
'  ',
'    -- CALL THE PROCEDURE',
'    FINI237.PP_CONSULTAR(I_EMPRESA       => :P_EMPRESA,',
'                         I_CATEG_CLI     => :P6504_CATEG_CLI,',
'                         I_CHK_CUOTA     => :P6504_CHK_CUOTA,',
'                         I_FEC_VTO       => :P6504_FEC_VTO,',
'                         I_LINEA_NEGOCIO => :P6504_LINEA_DE_NEGOCIO,',
'                         I_CLIENTE       => :P6504_CLIENTE,',
'                         I_FEC_CORTE     => :P6504_FECHA_CORTE);',
'  END IF;',
'  :P6504_CONSULTAR := ''S'';',
'',
'END;',
''))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(10178332346172670139)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(10195796818257326927)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PP_BORRAR_REGISTROS'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FINI237.PP_BORRAR_REGISTRO(I_EMPRESA              => :P_EMPRESA,',
'                             I_CATEG_CLI            => :P6504_CATEG_CLI,',
'                             I_DOC_NRO_TRANS_COMBUS => :P6504_DOC_NRO_TRANS_COMBUS);',
'END;',
''))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'BORRAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(10192360692914056715)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'cancelar'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(10192360321129056711)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(10195797250672326931)
,p_process_sequence=>60
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'cancelar2'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'BORRAR,GUARDAR'
,p_process_when_type=>'REQUEST_IN_CONDITION'
);
end;
/
prompt --application/end_environment
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
