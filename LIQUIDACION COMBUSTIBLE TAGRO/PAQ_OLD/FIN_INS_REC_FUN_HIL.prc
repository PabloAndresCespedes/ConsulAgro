CREATE OR REPLACE PROCEDURE FIN_INS_REC_FUN_HIL(I_OPERACION   VARCHAR2,
                                                I_CATEGORIA   IN NUMBER,
                                                I_NRO_PROCESO IN NUMBER,
                                                IN_EMPRESA    IN NUMBER) AS
  V_FECHA_GRAB              DATE := SYSDATE;
  V_DOC_CLAVE_FIN_DESTINO   NUMBER;
  V_DOC_CLAVE_FIN_DEST_ADEL NUMBER;
  V_PROV_DEST               NUMBER;
  V_CLAVE_ANTICIPO          NUMBER;
  V_CLAVE_HIJO              NUMBER;
  VCTACO                    NUMBER;
  V_NRO_RECIBO              NUMBER;
  V_EMPRESA                 NUMBER := 0;
  V_DOC_TIPO_SALDO          VARCHAR(1);
  V_FCON_TIPO_SALDO         VARCHAR(1);
  V_OBSERVACION             VARCHAR(500);
  V_CLI_ANT                 NUMBER;
  V_CLIENTE                 NUMBER;
  V_CLAVE_ADELANTO          NUMBER;
  V_CTA_HIL                 NUMBER;
  FUNCTION TCH(I_CHAR VARCHAR2) RETURN VARCHAR2 IS
  BEGIN
    RETURN CHR(39) || I_CHAR || CHR(39);
  END;

BEGIN

  DBMS_OUTPUT.ENABLE;

  IF I_CATEGORIA = 4 THEN
    V_EMPRESA := 1;
  ELSIF I_CATEGORIA = 5 THEN
    V_EMPRESA := 8;
  ELSE
    RAISE_APPLICATION_ERROR(-20010,
                            'Falta cargar empresa correspondiente a categoria en FIN_INS_REC_FUN');
  END IF;

  IF I_OPERACION = 'I' THEN
  
    FOR V IN (SELECT FCAT_CODIGO,
                     T.CLAVE_FIN,
                     T.NRO_RECIBO,
                     T.CLI_CODIGO,
                     T.CLI_NOM,
                     T.CLI_COD_EMPL_EMPR_ORIG,
                     T.FECHA_DOC,
                     T.FECHA_OPER,
                     SUM(T.CUO_IMP_MON) TOTAL,
                     T.TIPO_MOV,
                     T.LINEA_NEGOCIO
                FROM FIN_FINI037_TEMP T
               WHERE T.EMPR = IN_EMPRESA
               GROUP BY FCAT_CODIGO,
                        T.CLAVE_FIN,
                        T.NRO_RECIBO,
                        T.CLI_CODIGO,
                        T.CLI_NOM,
                        T.CLI_COD_EMPL_EMPR_ORIG,
                        T.FECHA_DOC,
                        T.FECHA_OPER,
                        T.TIPO_MOV,
                        T.LINEA_NEGOCIO
               ORDER BY 3, 10) LOOP
    
      IF I_CATEGORIA = 4 THEN
        VCTACO := 4883;
      END IF;
    
      IF V.TIPO_MOV = 10 THEN
        V_NRO_RECIBO := V.NRO_RECIBO;
        INSERT INTO FIN_DOCUMENTO
          (DOC_CLAVE,
           DOC_EMPR,
           DOC_CTA_BCO,
           DOC_COND_VTA,
           DOC_SUC,
           DOC_DPTO,
           DOC_TIPO_MOV,
           DOC_NRO_DOC,
           DOC_TIPO_SALDO,
           DOC_MON,
           DOC_PROV,
           DOC_CLI,
           DOC_CLI_NOM,
           DOC_CLI_DIR,
           DOC_CLI_RUC,
           DOC_CLI_TEL,
           DOC_LEGAJO,
           DOC_BCO_CHEQUE,
           DOC_NRO_CHEQUE,
           DOC_FEC_CHEQUE,
           DOC_FEC_DEP_CHEQUE,
           DOC_EST_CHEQUE,
           DOC_NRO_DOC_DEP,
           DOC_CHEQUE_CERTIF,
           DOC_FEC_OPER,
           DOC_FEC_DOC,
           DOC_BRUTO_EXEN_LOC,
           DOC_BRUTO_EXEN_MON,
           DOC_BRUTO_GRAV_LOC,
           DOC_BRUTO_GRAV_MON,
           DOC_NETO_EXEN_LOC,
           DOC_NETO_EXEN_MON,
           DOC_NETO_GRAV_LOC,
           DOC_NETO_GRAV_MON,
           DOC_IVA_LOC,
           DOC_IVA_MON,
           DOC_SALDO_INI_MON,
           DOC_SALDO_LOC,
           DOC_SALDO_MON,
           DOC_SALDO_PER_ACT_LOC,
           DOC_SALDO_PER_ACT_MON,
           DOC_OBS,
           DOC_BASE_IMPON_LOC,
           DOC_BASE_IMPON_MON,
           DOC_DIF_CAMBIO,
           DOC_IND_STK,
           DOC_CLAVE_STK,
           DOC_IND_CUOTA,
           DOC_IND_PEDIDO,
           DOC_IND_FINANRESA,
           DOC_IND_CONSIGNACION,
           DOC_PLAN_FINAN,
           DOC_NRO_LISTADO_LIBRO_IVA,
           DOC_CLAVE_PADRE,
           DOC_LOGIN,
           DOC_FEC_GRAB,
           DOC_SIST,
           DOC_CLI_CODEUDOR,
           DOC_OPERADOR,
           DOC_COBRADOR,
           DOC_CANT_PAGARE,
           DOC_SERIE,
           DOC_CLAVE_SCLI,
           DOC_NRO_PED,
           DOC_ORDEN_COMPRA_CLAVE,
           DOC_PRES_CLAVE,
           DOC_TASA,
           DOC_NRO_ORDEN_COMPRA,
           DOC_DIF_CAMBIO_ACUM,
           DOC_DIVISION,
           DOC_FORMA_ENTREGA,
           DOC_GRAV_10_LOC,
           DOC_GRAV_10_MON,
           DOC_GRAV_5_LOC,
           DOC_GRAV_5_MON,
           DOC_IVA_10_LOC,
           DOC_IVA_10_MON,
           DOC_IVA_5_LOC,
           DOC_IVA_5_MON,
           DOC_COSTO_FLETE,
           DOC_CHOFER,
           DOC_VEHICULO,
           DOC_CANT_KILOS,
           DOC_ZONA,
           DOC_CLAVE_PRE_FACT,
           DOC_NRO_TIMBRADO,
           DOC_AGRUPAR,
           DOC_RETENCION_MON,
           DOC_RETENCION_LOC,
           DOC_DV,
           DOC_RUC_DV,
           DOC_CLAVE_RETENCION,
           DOC_PROV_ACOPIO,
           DOC_IND_EXPORT,
           DOC_ART,
           DOC_BCO_PAGO,
           DOC_NRO_CUENTA,
           DOC_OBS_FPAGO,
           DOC_CLI_PORC_EX,
           DOC_CLAVE_PED_COM,
           DOC_IND_COM_INSUMO,
           DOC_HIST_VENC,
           DOC_CONTRATO,
           DOC_RESP_NOM,
           DOC_RESP_CI,
           DOC_RESP_TEL,
           DOC_IND_ACO_TIC,
           DOC_IND_INTERNO,
           DOC_CATEG_COM,
           DOC_CTACO,
           DOC_EMPLEADO,
           DOC_NRO_TRANS_COMBUS,
           DOC_CLAVE_FIN_DESTINO,
           DOC_CATEG_COMB)
        VALUES
          (V.CLAVE_FIN,
           IN_EMPRESA,
           NULL,
           NULL,
           1,
           NULL,
           6,
           V.NRO_RECIBO,
           'D',
           1,
           NULL,
           V.CLI_CODIGO,
           V.CLI_NOM,
           '',
           '',
           '',
           NULL,
           NULL,
           '',
           NULL,
           NULL,
           '',
           NULL,
           '',
           V.FECHA_OPER,
           V.FECHA_DOC,
           V.TOTAL,
           V.TOTAL,
           0.0000,
           0.0000,
           V.TOTAL,
           V.TOTAL,
           0.0000,
           0.0000,
           0.0000,
           0.0000,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           V.LINEA_NEGOCIO, ---LIZVILLASANTI 10/02/2020---'',
           0.0000,
           0.0000,
           NULL,
           '',
           NULL,
           '',
           '',
           '',
           '',
           NULL,
           NULL,
           NULL,
           GEN_DEVUELVE_USER,
           V_FECHA_GRAB,
           'FIN',
           NULL,
           '2',
           NULL,
           NULL,
           'A',
           NULL,
           NULL,
           NULL,
           NULL,
           1.000000,
           '',
           NULL,
           '',
           '',
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           '',
           '',
           NULL,
           NULL,
           '',
           '',
           NULL,
           NULL,
           '',
           NULL,
           NULL,
           '',
           '',
           NULL,
           NULL,
           '',
           '',
           '',
           '',
           '',
           '',
           '',
           'S',
           NULL,
           VCTACO,
           NULL,
           I_NRO_PROCESO,
           NULL,
           I_CATEGORIA);
      
        INSERT INTO FIN_DOC_CONCEPTO
          (DCON_CLAVE_DOC,
           DCON_ITEM,
           DCON_CLAVE_CONCEPTO,
           DCON_CLAVE_CTACO,
           DCON_TIPO_SALDO,
           DCON_EXEN_LOC,
           DCON_EXEN_MON,
           DCON_GRAV_LOC,
           DCON_GRAV_MON,
           DCON_IVA_LOC,
           DCON_IVA_MON,
           DCON_CLAVE_IMP,
           DCON_IND_TIPO_IVA_COMPRA,
           DCON_PORC_IVA,
           DCON_VEHICULO,
           DCON_IND_GASTO,
           DCON_IND_IMAGRO,
           DCON_OBS,
           DCON_TRA_CAMION,
           DCON_CANT_COMB,
           DCON_TRA_KM_ACTUAL,
           DCON_TRA_HORA_ACTUAL,
           DCON_EMPR)
        VALUES
          (V.CLAVE_FIN,
           1,
           721,
           2266,
           'D',
           V.TOTAL,
           V.TOTAL,
           0.0000,
           0.0000,
           0.0000,
           0.0000,
           NULL,
           NULL,
           NULL,
           NULL,
           '',
           '',
           '',
           NULL,
           NULL,
           NULL,
           NULL,
           IN_EMPRESA);
      
      ELSIF V.TIPO_MOV = 31 THEN
        SELECT FIN_SEQ_DOC_NEXTVAL INTO V_CLAVE_ADELANTO FROM DUAL;
        SELECT TMOV_TIPO
          INTO V_DOC_TIPO_SALDO
          FROM GEN_TIPO_MOV
         WHERE TMOV_CODIGO = 33
           AND TMOV_EMPR = IN_EMPRESA;
      
        SELECT FCON_TIPO_SALDO
          INTO V_FCON_TIPO_SALDO
          FROM FIN_CONCEPTO
         WHERE FCON_CLAVE = 895
           AND FCON_EMPR = IN_EMPRESA;
        BEGIN
          INSERT INTO FIN_DOCUMENTO
            (DOC_CLAVE,
             DOC_EMPR,
             DOC_CTA_BCO,
             DOC_SUC,
             DOC_TIPO_MOV,
             DOC_NRO_DOC,
             DOC_TIPO_SALDO,
             DOC_MON,
             DOC_PROV,
             DOC_CLI_NOM,
             DOC_FEC_OPER,
             DOC_FEC_DOC,
             DOC_BRUTO_EXEN_LOC,
             DOC_BRUTO_EXEN_MON,
             DOC_BRUTO_GRAV_LOC,
             DOC_BRUTO_GRAV_MON,
             DOC_NETO_EXEN_LOC,
             DOC_NETO_EXEN_MON,
             DOC_NETO_GRAV_LOC,
             DOC_NETO_GRAV_MON,
             DOC_IVA_LOC,
             DOC_IVA_MON,
             DOC_SALDO_INI_MON,
             DOC_SALDO_LOC,
             DOC_SALDO_MON,
             DOC_SALDO_PER_ACT_LOC,
             DOC_SALDO_PER_ACT_MON,
             DOC_OBS,
             DOC_BASE_IMPON_LOC,
             DOC_BASE_IMPON_MON,
             DOC_LOGIN,
             DOC_FEC_GRAB,
             DOC_SIST,
             DOC_OPERADOR,
             --DOC_CLAVE_FIN_DESTINO  ,
             DOC_CLAVE_PADRE,
             DOC_NRO_TRANS_COMBUS,
             DOC_CATEG_COMB)
          VALUES
            (V.CLAVE_FIN,
             IN_EMPRESA,
             NULL,
             1,
             33,
             V.NRO_RECIBO,
             V_DOC_TIPO_SALDO,
             1,
             V.CLI_CODIGO,
             NULL,
             V.FECHA_OPER,--TRUNC(SYSDATE),
             V.FECHA_DOC,  --TRUNC(SYSDATE),
             V.TOTAL,
             V.TOTAL,
             0,
             0,
             V.TOTAL,
             V.TOTAL,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             'COBRO ANTICIPO FUNC. HILAGRO', --'ANTIC. Personal-Modulo RRHH',
             0,
             0,
             GEN_DEVUELVE_USER,
             SYSDATE,
             'FIN',
             2,
             NULL, --V.CLAVE_FIN,
             I_NRO_PROCESO,
             I_CATEGORIA);
        
          INSERT INTO FIN_DOC_CONCEPTO
            (DCON_ITEM,
             DCON_CLAVE_DOC,
             DCON_CLAVE_CONCEPTO,
             DCON_CLAVE_CTACO,
             DCON_TIPO_SALDO,
             DCON_EXEN_LOC,
             DCON_EXEN_MON,
             DCON_GRAV_LOC,
             DCON_GRAV_MON,
             DCON_IVA_LOC,
             DCON_IVA_MON,
             DCON_IND_TIPO_IVA_COMPRA,
             DCON_OBS,
             DCON_EMPR)
          
          VALUES
            (1,
             V.CLAVE_FIN,
             895,
             2520,
             V_FCON_TIPO_SALDO,
             V.TOTAL,
             V.TOTAL,
             0,
             0,
             0,
             0,
             1,
             'COBRO ANTICIPO FUNC. HILAGRO',
             IN_EMPRESA);
        EXCEPTION
          WHEN OTHERS THEN
            IF SQLCODE = -2291 THEN
              INSERT INTO FIN_DOCUMENTO
                (DOC_CLAVE,
                 DOC_EMPR,
                 DOC_CTA_BCO,
                 DOC_SUC,
                 DOC_TIPO_MOV,
                 DOC_NRO_DOC,
                 DOC_TIPO_SALDO,
                 DOC_MON,
                 DOC_PROV,
                 DOC_CLI_NOM,
                 DOC_FEC_OPER,
                 DOC_FEC_DOC,
                 DOC_BRUTO_EXEN_LOC,
                 DOC_BRUTO_EXEN_MON,
                 DOC_BRUTO_GRAV_LOC,
                 DOC_BRUTO_GRAV_MON,
                 DOC_NETO_EXEN_LOC,
                 DOC_NETO_EXEN_MON,
                 DOC_NETO_GRAV_LOC,
                 DOC_NETO_GRAV_MON,
                 DOC_IVA_LOC,
                 DOC_IVA_MON,
                 DOC_SALDO_INI_MON,
                 DOC_SALDO_LOC,
                 DOC_SALDO_MON,
                 DOC_SALDO_PER_ACT_LOC,
                 DOC_SALDO_PER_ACT_MON,
                 DOC_OBS,
                 DOC_BASE_IMPON_LOC,
                 DOC_BASE_IMPON_MON,
                 DOC_LOGIN,
                 DOC_FEC_GRAB,
                 DOC_SIST,
                 DOC_OPERADOR,
                 --DOC_CLAVE_FIN_DESTINO  ,
                 DOC_CLAVE_PADRE,
                 DOC_NRO_TRANS_COMBUS)
              VALUES
                (V.CLAVE_FIN,
                 IN_EMPRESA,
                 NULL,
                 1,
                 33,
                 V.NRO_RECIBO,
                 V_DOC_TIPO_SALDO,
                 1,
                 V.CLI_CODIGO,
                 NULL,
                 V.FECHA_OPER,--TRUNC(SYSDATE),
                 V.FECHA_DOC,  --TRUNC(SYSDATE),
                 V.TOTAL,
                 V.TOTAL,
                 0,
                 0,
                 V.TOTAL,
                 V.TOTAL,
                 0,
                 0,
                 0,
                 0,
                 0,
                 0,
                 0,
                 0,
                 0,
                 'COBRO ANTICIPO FUNC. HILAGRO', --'ANTIC. Personal-Modulo RRHH',
                 0,
                 0,
                 GEN_DEVUELVE_USER,
                 SYSDATE,
                 'FIN',
                 2,
                 NULL,
                 I_NRO_PROCESO);
            
              INSERT INTO FIN_DOC_CONCEPTO
                (DCON_ITEM,
                 DCON_CLAVE_DOC,
                 DCON_CLAVE_CONCEPTO,
                 DCON_CLAVE_CTACO,
                 DCON_TIPO_SALDO,
                 DCON_EXEN_LOC,
                 DCON_EXEN_MON,
                 DCON_GRAV_LOC,
                 DCON_GRAV_MON,
                 DCON_IVA_LOC,
                 DCON_IVA_MON,
                 DCON_IND_TIPO_IVA_COMPRA,
                 DCON_OBS,
                 DCON_EMPR)
              
              VALUES
                (1,
                 V.CLAVE_FIN,
                 895,
                 2520,
                 V_FCON_TIPO_SALDO,
                 V.TOTAL,
                 V.TOTAL,
                 0,
                 0,
                 0,
                 0,
                 1,
                 'COBRO ANTICIPO FUNC. HILAGRO',
                 IN_EMPRESA);
            END IF;
        END;
      
      END IF;
    
      FOR X IN (SELECT *
                  FROM FIN_FINI037_TEMP T
                 WHERE T.CLAVE_FIN = V.CLAVE_FIN
                   AND T.EMPR = IN_EMPRESA
                   AND T.TIPO_MOV = V.TIPO_MOV
                 ORDER BY ROWID) LOOP
        INSERT INTO FIN_PAGO
          (PAG_CLAVE_DOC,
           PAG_FEC_VTO,
           PAG_CLAVE_PAGO,
           PAG_FEC_PAGO,
           PAG_IMP_LOC,
           PAG_IMP_MON,
           PAG_LOGIN,
           PAG_FEC_GRAB,
           PAG_SIST,
           PAG_IMP_INT_LOC,
           PAG_IMP_INT_MON,
           PAG_EMPR)
        VALUES
          (X.CLAVE_FAC,
           X.CUO_FEC_VTO,
           V.CLAVE_FIN,
           V.FECHA_DOC,
           X.CUO_IMP_MON,
           X.CUO_IMP_MON,
           GEN_DEVUELVE_USER,
           TRUNC(V_FECHA_GRAB),
           'FIN',
           NULL,
           NULL,
           IN_EMPRESA);
      
      END LOOP;
    
      --INSERTA ANTICIPO EN LA BD DE HILAGRO USA V_EMPRESA CON LOS DATOS DE EMPRESA CORRESPONDIENTE A HILAGRO
    
      --  IF V.TIPO_MOV = 10 THEN
    
      FOR REG IN (SELECT FCAT_CODIGO,
                         T.CLAVE_FIN,
                         T.NRO_RECIBO,
                         T.CLI_CODIGO,
                         T.CLI_NOM,
                         T.CLI_COD_EMPL_EMPR_ORIG,
                         T.FECHA_DOC,
                         T.FECHA_OPER,
                         SUM(T.CUO_IMP_MON) TOTAL,
                         T.TIPO_MOV,
                         T.LINEA_NEGOCIO,
                         T.LIN_CON_HIL
                    FROM FIN_FINI037_TEMP T
                   WHERE T.EMPR = IN_EMPRESA
                     AND T.CLI_CODIGO = V.CLI_CODIGO
                     AND T.TIPO_MOV = V.TIPO_MOV
                     AND T.CLAVE_FIN = V.CLAVE_FIN
                   GROUP BY FCAT_CODIGO,
                            T.CLAVE_FIN,
                            T.NRO_RECIBO,
                            T.CLI_CODIGO,
                            T.CLI_NOM,
                            T.CLI_COD_EMPL_EMPR_ORIG,
                            T.FECHA_DOC,
                            T.FECHA_OPER,
                            T.TIPO_MOV,
                            T.LINEA_NEGOCIO,
                            T.LIN_CON_HIL
                   ORDER BY 1) LOOP
      
        IF V.TIPO_MOV = 10 THEN
          V_OBSERVACION := 'MIGRADO DE TRANSAGRO ' || REG.LINEA_NEGOCIO ||
                           ' RECIBO =';
        
        ELSE
          V_OBSERVACION := 'COBRO ADELANTO MIGRADO DE TRANSAGRO RECIBO=';
        END IF;
      
        SELECT EMPL_CODIGO_PROV
          INTO V_PROV_DEST
          FROM PER_EMPLEADO
         WHERE EMPL_EMPRESA = 1
           AND EMPL_LEGAJO = REG.CLI_COD_EMPL_EMPR_ORIG;
      
        SELECT FIN_SEQ_DOC_NEXTVAL INTO V_DOC_CLAVE_FIN_DESTINO FROM DUAL;
      
        BEGIN
          SELECT FCON_CLAVE_CTACO
            INTO V_CTA_HIL
            FROM FIN_CONCEPTO A
           WHERE FCON_CLAVE = REG.LIN_CON_HIL
             AND FCON_EMPR = 1;
        EXCEPTION
          WHEN OTHERS THEN
            V_CTA_HIL := NULL;
        END;
      
        INSERT INTO FIN_DOCUMENTO
          (DOC_CLAVE,
           DOC_EMPR,
           DOC_CTA_BCO,
           DOC_SUC,
           DOC_TIPO_MOV,
           DOC_NRO_DOC,
           DOC_TIPO_SALDO,
           DOC_MON,
           DOC_PROV,
           DOC_CLI_NOM,
           DOC_FEC_OPER,
           DOC_FEC_DOC,
           DOC_BRUTO_EXEN_LOC,
           DOC_BRUTO_EXEN_MON,
           DOC_BRUTO_GRAV_LOC,
           DOC_BRUTO_GRAV_MON,
           DOC_NETO_EXEN_LOC,
           DOC_NETO_EXEN_MON,
           DOC_NETO_GRAV_LOC,
           DOC_NETO_GRAV_MON,
           DOC_IVA_LOC,
           DOC_IVA_MON,
           DOC_OBS,
           DOC_BASE_IMPON_LOC,
           DOC_BASE_IMPON_MON,
           DOC_DIF_CAMBIO,
           DOC_LOGIN,
           DOC_FEC_GRAB,
           DOC_SIST,
           DOC_OPERADOR,
           DOC_TASA,
           DOC_DIF_CAMBIO_ACUM,
           DOC_EMPLEADO,
           DOC_IND_CANC_PMO,
           DOC_CTACO,
           DOC_NRO_TRANS_COMBUS,
           DOC_CLAVE_PADRE_ORIG)
        VALUES
          (V_DOC_CLAVE_FIN_DESTINO,
           V_EMPRESA,
           NULL,
           1,
           81,
           REG.NRO_RECIBO,
           'C',
           1,
           V_PROV_DEST,
           NULL,
           REG.FECHA_OPER,
           REG.FECHA_DOC,
           REG.TOTAL,
           REG.TOTAL,
           0,
           0,
           REG.TOTAL,
           REG.TOTAL,
           0,
           0,
           0,
           0,
           V_OBSERVACION || REG.NRO_RECIBO,
           0,
           0,
           0,
           GEN_DEVUELVE_USER,
           SYSDATE,
           'FIN',
           2,
           1,
           0,
           REG.CLI_COD_EMPL_EMPR_ORIG,
           'N',
           V_CTA_HIL,
           I_NRO_PROCESO,
           V.CLAVE_FIN);
      
        INSERT INTO FIN_DOC_CONCEPTO
          (DCON_EMPR,
           DCON_CLAVE_DOC,
           DCON_ITEM,
           DCON_CLAVE_CONCEPTO,
           DCON_CLAVE_CTACO,
           DCON_TIPO_SALDO,
           DCON_EXEN_LOC,
           DCON_EXEN_MON,
           DCON_GRAV_LOC,
           DCON_GRAV_MON,
           DCON_IVA_LOC,
           DCON_IVA_MON,
           DCON_IND_TIPO_IVA_COMPRA,
           DCON_OBS)
        VALUES
          (V_EMPRESA,
           V_DOC_CLAVE_FIN_DESTINO,
           1,
           REG.LIN_CON_HIL, --1480
           18,
           'C',
           REG.TOTAL,
           REG.TOTAL,
           0,
           0,
           0,
           0,
           1,
           V_OBSERVACION || V.NRO_RECIBO);
      
        INSERT INTO FIN_CUOTA
          (CUO_CLAVE_DOC,
           CUO_FEC_VTO,
           CUO_IMP_LOC,
           CUO_IMP_MON,
           CUO_DIF_CAMBIO,
           CUO_EMPR)
        VALUES
          (V_DOC_CLAVE_FIN_DESTINO,
           LAST_DAY(REG.FECHA_DOC),
           REG.TOTAL,
           REG.TOTAL,
           0,
           V_EMPRESA);
      END LOOP;
    
    END LOOP;
    --
  
  ELSIF I_OPERACION = 'D' THEN
  
    FOR V IN (SELECT C.DOC_CLAVE, DOC_CLAVE_PADRE
                FROM FIN_DOCUMENTO C
               WHERE C.DOC_NRO_TRANS_COMBUS = I_NRO_PROCESO
                 AND DOC_EMPR = IN_EMPRESA
               ORDER BY 2) LOOP
    
      FOR RR IN (SELECT DOC_CLAVE, DOC_EMPR
                   FROM FIN_DOCUMENTO C
                  WHERE C.DOC_NRO_TRANS_COMBUS = I_NRO_PROCESO
                       ---     AND C.DOC_CLAVE_PADRE_ORIG = V.DOC_CLAVE
                    AND DOC_EMPR = 1) LOOP
        --CONSULTABA POR DBLINK
      
        DELETE FROM FIN_CUOTA P
         WHERE P.CUO_CLAVE_DOC = RR.DOC_CLAVE
           AND P.CUO_EMPR = 1;
      
        DELETE FROM FIN_DOC_CONCEPTO Q
         WHERE Q.DCON_CLAVE_DOC = RR.DOC_CLAVE
           AND Q.DCON_EMPR = 1;
      
        DELETE FROM FIN_DOCUMENTO R
         WHERE R.DOC_CLAVE = RR.DOC_CLAVE
           AND R.DOC_EMPR = 1;
      END LOOP;
    
      /* UPDATE FIN_DOCUMENTO SET DOC_CLAVE_PADRE = NULL  WHERE
      DOC_CLAVE_PADRE = V.DOC_CLAVE_PADRE AND DOC_EMPR = IN_EMPRESA;*/
    
      DELETE FROM FIN_PAGO P
       WHERE P.PAG_CLAVE_PAGO = V.DOC_CLAVE
         AND P.PAG_EMPR = IN_EMPRESA;
    
      DELETE FROM FIN_DOC_CONCEPTO Q
       WHERE Q.DCON_CLAVE_DOC = V.DOC_CLAVE
         AND Q.DCON_EMPR = IN_EMPRESA;
    
      DELETE FROM FIN_DOCUMENTO R
       WHERE R.DOC_CLAVE = V.DOC_CLAVE
         AND R.DOC_EMPR = IN_EMPRESA;
    
    END LOOP;
  
  END IF;

END;
/
