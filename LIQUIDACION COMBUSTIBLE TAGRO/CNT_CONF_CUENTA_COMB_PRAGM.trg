create or replace trigger CNT_CONF_CUENTA_COMB_PRAGM
  before insert or update
  on CNT_CONF_CUENTA_COMBUSTIBLE 
  for each row
-- 01/09/2022 9:27:30 @PabloACespedes \(^-^)/
-- disparador para validacion sobre misma tabla   
declare
  pragma autonomous_transaction;
begin
  if cntm135.existe_registro(in_empresa       => :new.empresa_id,
                             in_empresa_grupo => :new.empresa_grupo_id,
                             in_cuenta        => :new.cuenta_id) then
                                   
    Raise_application_error(-20000, 'La empresa ya tiene registrada la Cuenta Contable');                    
  end if;
  
end CNT_CONF_CUENTA_COMB_PRAGM;
/
