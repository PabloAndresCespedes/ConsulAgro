CREATE OR REPLACE PROCEDURE FIN_INS_REC_FUN_HIL(I_OPERACION   VARCHAR2,
                                                I_CATEGORIA   IN NUMBER,
                                                I_NRO_PROCESO IN NUMBER,
                                                IN_EMPRESA    IN number,
                                                in_empresa_grupo in number := null
                                                ) as

  co_empr_hilagro  constant number := 1;
  co_empr_halten   constant number := 8;

  co_func_hilagro  constant number := 4;
  co_func_halten   constant number := 5;

  co_mnd_gs        constant number := 1;
  co_tmv_srv_tagro constant number := 81;

  l_cat_cli_func_grupo  fin_configuracion.conf_cat_cli_func_grupo%type;
  l_tmv_fact_cre_emit   fin_configuracion.conf_fact_cr_emit%type;
  l_cta_adelanto_prov   fin_configuracion.conf_cta_adel_prov%type;

  l_empr_funcionario_desc gen_empresa.empr_razon_social%type;
  l_sufijo                gen_empresa.empr_sufijo%type;

  c_del_dynamic sys_refcursor;
  l_del_clave   number;
  l_del_query   varchar2(32747);

  V_FECHA_GRAB              DATE := SYSDATE;
  V_DOC_CLAVE_FIN_DESTINO   number;
  V_PROV_DEST               number;
  VCTACO                    NUMBER;
  V_NRO_RECIBO              NUMBER;
  V_EMPRESA                 NUMBER := 0;
  V_DOC_TIPO_SALDO          VARCHAR(1);
  V_FCON_TIPO_SALDO         VARCHAR(1);
  V_OBSERVACION             VARCHAR(500);
  V_CLAVE_ADELANTO          NUMBER;
  V_CTA_HIL                 NUMBER;

  FUNCTION TCH(I_CHAR VARCHAR2) RETURN VARCHAR2 IS
  BEGIN
    RETURN CHR(39) || I_CHAR || CHR(39);
  end;

  function obt_empresa_desc_cli( in_cliente number, in_empr_cli number ) return varchar2 is
    l_desc_empr gen_empresa.empr_razon_social%type;
  begin
    select g.empr_razon_social
    into l_desc_empr
    from fin_cliente c
    inner join gen_empresa g on ( g.empr_codigo = c.cli_empresa_funcionario )
    where c.cli_empr = in_empr_cli
    and   c.cli_codigo = in_cliente;

    return l_desc_empr;
  exception
    when no_data_found then
      return '--';
  end obt_empresa_desc_cli;

begin
  DBMS_OUTPUT.ENABLE;

  l_cat_cli_func_grupo := fini237.obt_cat_cli_func_grupo( in_empresa => IN_EMPRESA );
  l_tmv_fact_cre_emit  := fini237.obt_tmv_fact_cred_emit( in_empresa => IN_EMPRESA );

  IF I_CATEGORIA = co_func_hilagro THEN
    V_EMPRESA := co_empr_hilagro;
  ELSIF I_CATEGORIA = co_func_halten THEN
    V_EMPRESA := co_empr_halten;
  elsif i_categoria = l_cat_cli_func_grupo then
    v_empresa := in_empresa_grupo;
  ELSE
    RAISE_APPLICATION_ERROR(-20010,
                            'Falta cargar empresa correspondiente a categoria en FIN_INS_REC_FUN');
  END IF;

  IF I_OPERACION = 'I' THEN

    FOR V IN (SELECT FCAT_CODIGO,
                     T.CLAVE_FIN,
                     T.NRO_RECIBO,
                     T.CLI_CODIGO,
                     T.CLI_NOM,
                     T.CLI_COD_EMPL_EMPR_ORIG,
                     T.FECHA_DOC,
                     T.FECHA_OPER,
                     SUM(T.CUO_IMP_MON) TOTAL,
                     T.TIPO_MOV,
                     T.LINEA_NEGOCIO
                FROM FIN_FINI037_TEMP T
               WHERE T.EMPR = IN_EMPRESA
               GROUP BY FCAT_CODIGO,
                        T.CLAVE_FIN,
                        T.NRO_RECIBO,
                        T.CLI_CODIGO,
                        T.CLI_NOM,
                        T.CLI_COD_EMPL_EMPR_ORIG,
                        T.FECHA_DOC,
                        T.FECHA_OPER,
                        T.TIPO_MOV,
                        T.LINEA_NEGOCIO
               ORDER BY 3, 10) LOOP

      IF I_CATEGORIA in (co_func_hilagro, l_cat_cli_func_grupo) THEN
        -- VCTACO := 4883; --> anticipos de combustible a funcionarios de Hilagro
        -- Configurar en 7-1-135 (Area de Contabilidad)
        vctaco := cntm135.obt_cuenta_de_empresa(in_empresa => in_empresa, in_empresa_grupo => in_empresa_grupo);
        
        if i_categoria = l_cat_cli_func_grupo then
          l_empr_funcionario_desc := obt_empresa_desc_cli(v.cli_cod_empl_empr_orig, in_empresa);
        else
          l_empr_funcionario_desc := 'HILAGRO';
        end if;

      END IF;

      IF V.TIPO_MOV = l_tmv_fact_cre_emit THEN
        V_NRO_RECIBO := V.NRO_RECIBO;
        begin
        INSERT INTO FIN_DOCUMENTO
          (DOC_CLAVE,
           DOC_EMPR,
           DOC_CTA_BCO,
           DOC_COND_VTA,
           DOC_SUC,
           DOC_DPTO,
           DOC_TIPO_MOV,
           DOC_NRO_DOC,
           DOC_TIPO_SALDO,
           DOC_MON,
           DOC_PROV,
           DOC_CLI,
           DOC_CLI_NOM,
           DOC_CLI_DIR,
           DOC_CLI_RUC,
           DOC_CLI_TEL,
           DOC_LEGAJO,
           DOC_BCO_CHEQUE,
           DOC_NRO_CHEQUE,
           DOC_FEC_CHEQUE,
           DOC_FEC_DEP_CHEQUE,
           DOC_EST_CHEQUE,
           DOC_NRO_DOC_DEP,
           DOC_CHEQUE_CERTIF,
           DOC_FEC_OPER,
           DOC_FEC_DOC,
           DOC_BRUTO_EXEN_LOC,
           DOC_BRUTO_EXEN_MON,
           DOC_BRUTO_GRAV_LOC,
           DOC_BRUTO_GRAV_MON,
           DOC_NETO_EXEN_LOC,
           DOC_NETO_EXEN_MON,
           DOC_NETO_GRAV_LOC,
           DOC_NETO_GRAV_MON,
           DOC_IVA_LOC,
           DOC_IVA_MON,
           DOC_SALDO_INI_MON,
           DOC_SALDO_LOC,
           DOC_SALDO_MON,
           DOC_SALDO_PER_ACT_LOC,
           DOC_SALDO_PER_ACT_MON,
           DOC_OBS,
           DOC_BASE_IMPON_LOC,
           DOC_BASE_IMPON_MON,
           DOC_DIF_CAMBIO,
           DOC_IND_STK,
           DOC_CLAVE_STK,
           DOC_IND_CUOTA,
           DOC_IND_PEDIDO,
           DOC_IND_FINANRESA,
           DOC_IND_CONSIGNACION,
           DOC_PLAN_FINAN,
           DOC_NRO_LISTADO_LIBRO_IVA,
           DOC_CLAVE_PADRE,
           DOC_LOGIN,
           DOC_FEC_GRAB,
           DOC_SIST,
           DOC_CLI_CODEUDOR,
           DOC_OPERADOR,
           DOC_COBRADOR,
           DOC_CANT_PAGARE,
           DOC_SERIE,
           DOC_CLAVE_SCLI,
           DOC_NRO_PED,
           DOC_ORDEN_COMPRA_CLAVE,
           DOC_PRES_CLAVE,
           DOC_TASA,
           DOC_NRO_ORDEN_COMPRA,
           DOC_DIF_CAMBIO_ACUM,
           DOC_DIVISION,
           DOC_FORMA_ENTREGA,
           DOC_GRAV_10_LOC,
           DOC_GRAV_10_MON,
           DOC_GRAV_5_LOC,
           DOC_GRAV_5_MON,
           DOC_IVA_10_LOC,
           DOC_IVA_10_MON,
           DOC_IVA_5_LOC,
           DOC_IVA_5_MON,
           DOC_COSTO_FLETE,
           DOC_CHOFER,
           DOC_VEHICULO,
           DOC_CANT_KILOS,
           DOC_ZONA,
           DOC_CLAVE_PRE_FACT,
           DOC_NRO_TIMBRADO,
           DOC_AGRUPAR,
           DOC_RETENCION_MON,
           DOC_RETENCION_LOC,
           DOC_DV,
           DOC_RUC_DV,
           DOC_CLAVE_RETENCION,
           DOC_PROV_ACOPIO,
           DOC_IND_EXPORT,
           DOC_ART,
           DOC_BCO_PAGO,
           DOC_NRO_CUENTA,
           DOC_OBS_FPAGO,
           DOC_CLI_PORC_EX,
           DOC_CLAVE_PED_COM,
           DOC_IND_COM_INSUMO,
           DOC_HIST_VENC,
           DOC_CONTRATO,
           DOC_RESP_NOM,
           DOC_RESP_CI,
           DOC_RESP_TEL,
           DOC_IND_ACO_TIC,
           DOC_IND_INTERNO,
           DOC_CATEG_COM,
           DOC_CTACO,
           DOC_EMPLEADO,
           DOC_NRO_TRANS_COMBUS,
           DOC_CLAVE_FIN_DESTINO,
           DOC_CATEG_COMB)
        VALUES
          (V.CLAVE_FIN,
           IN_EMPRESA,
           NULL,
           NULL,
           1,
           NULL,
           6, --> TM RECIBO
           V.NRO_RECIBO,
           'D',
           1, --> MND GS
           NULL,
           V.CLI_CODIGO,
           V.CLI_NOM,
           '',
           '',
           '',
           NULL,
           NULL,
           '',
           NULL,
           NULL,
           '',
           NULL,
           '',
           V.FECHA_OPER,
           V.FECHA_DOC,
           V.TOTAL,
           V.TOTAL,
           0.0000,
           0.0000,
           V.TOTAL,
           V.TOTAL,
           0.0000,
           0.0000,
           0.0000,
           0.0000,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           V.LINEA_NEGOCIO, ---LIZVILLASANTI 10/02/2020---'',
           0.0000,
           0.0000,
           NULL,
           '',
           NULL,
           '',
           '',
           '',
           '',
           NULL,
           NULL,
           NULL,
           GEN_DEVUELVE_USER,
           V_FECHA_GRAB,
           'FIN',
           NULL,
           '2',
           NULL,
           NULL,
           'A',
           NULL,
           NULL,
           NULL,
           NULL,
           1.000000,
           '',
           NULL,
           '',
           '',
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           '',
           '',
           NULL,
           NULL,
           '',
           '',
           NULL,
           NULL,
           '',
           NULL,
           NULL,
           '',
           '',
           NULL,
           NULL,
           '',
           '',
           '',
           '',
           '',
           '',
           '',
           'S',
           NULL,
           VCTACO,
           NULL,
           I_NRO_PROCESO,
           NULL,
           I_CATEGORIA);
        exception
           when others then
             Raise_application_error(-20000, 'Clave '||V.CLAVE_FIN||' '||sqlerrm );
        end;

        INSERT INTO FIN_DOC_CONCEPTO
          (DCON_CLAVE_DOC,
           DCON_ITEM,
           DCON_CLAVE_CONCEPTO,
           DCON_CLAVE_CTACO,
           DCON_TIPO_SALDO,
           DCON_EXEN_LOC,
           DCON_EXEN_MON,
           DCON_GRAV_LOC,
           DCON_GRAV_MON,
           DCON_IVA_LOC,
           DCON_IVA_MON,
           DCON_CLAVE_IMP,
           DCON_IND_TIPO_IVA_COMPRA,
           DCON_PORC_IVA,
           DCON_VEHICULO,
           DCON_IND_GASTO,
           DCON_IND_IMAGRO,
           DCON_OBS,
           DCON_TRA_CAMION,
           DCON_CANT_COMB,
           DCON_TRA_KM_ACTUAL,
           DCON_TRA_HORA_ACTUAL,
           DCON_EMPR)
        VALUES
          (V.CLAVE_FIN,
           1,
           721,
           2266,
           'D',
           V.TOTAL,
           V.TOTAL,
           0.0000,
           0.0000,
           0.0000,
           0.0000,
           NULL,
           NULL,
           NULL,
           NULL,
           '',
           '',
           '',
           NULL,
           NULL,
           NULL,
           NULL,
           IN_EMPRESA);

      ELSIF V.TIPO_MOV = 31 THEN
        SELECT FIN_SEQ_DOC_NEXTVAL INTO V_CLAVE_ADELANTO FROM DUAL;
        
        SELECT TMOV_TIPO
          INTO V_DOC_TIPO_SALDO
          FROM GEN_TIPO_MOV
         WHERE TMOV_CODIGO = 33
           AND TMOV_EMPR = IN_EMPRESA;

        SELECT FCON_TIPO_SALDO
          INTO V_FCON_TIPO_SALDO
          FROM FIN_CONCEPTO
         WHERE FCON_CLAVE = 895
           AND FCON_EMPR = IN_EMPRESA;
           
        BEGIN
          INSERT INTO FIN_DOCUMENTO
            (DOC_CLAVE,
             DOC_EMPR,
             DOC_CTA_BCO,
             DOC_SUC,
             DOC_TIPO_MOV,
             DOC_NRO_DOC,
             DOC_TIPO_SALDO,
             DOC_MON,
             DOC_PROV,
             DOC_CLI_NOM,
             DOC_FEC_OPER,
             DOC_FEC_DOC,
             DOC_BRUTO_EXEN_LOC,
             DOC_BRUTO_EXEN_MON,
             DOC_BRUTO_GRAV_LOC,
             DOC_BRUTO_GRAV_MON,
             DOC_NETO_EXEN_LOC,
             DOC_NETO_EXEN_MON,
             DOC_NETO_GRAV_LOC,
             DOC_NETO_GRAV_MON,
             DOC_IVA_LOC,
             DOC_IVA_MON,
             DOC_SALDO_INI_MON,
             DOC_SALDO_LOC,
             DOC_SALDO_MON,
             DOC_SALDO_PER_ACT_LOC,
             DOC_SALDO_PER_ACT_MON,
             DOC_OBS,
             DOC_BASE_IMPON_LOC,
             DOC_BASE_IMPON_MON,
             DOC_LOGIN,
             DOC_FEC_GRAB,
             DOC_SIST,
             DOC_OPERADOR,
             --DOC_CLAVE_FIN_DESTINO  ,
             DOC_CLAVE_PADRE,
             DOC_NRO_TRANS_COMBUS,
             DOC_CATEG_COMB)
          VALUES
            (V.CLAVE_FIN,
             IN_EMPRESA,
             NULL,
             1,
             33,
             V.NRO_RECIBO,
             V_DOC_TIPO_SALDO,
             1,
             V.CLI_CODIGO,
             NULL,
             V.FECHA_OPER,--TRUNC(SYSDATE),
             V.FECHA_DOC,  --TRUNC(SYSDATE),
             V.TOTAL,
             V.TOTAL,
             0,
             0,
             V.TOTAL,
             V.TOTAL,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             'COBRO ANTICIPO FUNC. '||l_empr_funcionario_desc,
             0,
             0,
             GEN_DEVUELVE_USER,
             SYSDATE,
             'FIN',
             2,
             NULL, --V.CLAVE_FIN,
             I_NRO_PROCESO,
             I_CATEGORIA);

          INSERT INTO FIN_DOC_CONCEPTO
            (DCON_ITEM,
             DCON_CLAVE_DOC,
             DCON_CLAVE_CONCEPTO,
             DCON_CLAVE_CTACO,
             DCON_TIPO_SALDO,
             DCON_EXEN_LOC,
             DCON_EXEN_MON,
             DCON_GRAV_LOC,
             DCON_GRAV_MON,
             DCON_IVA_LOC,
             DCON_IVA_MON,
             DCON_IND_TIPO_IVA_COMPRA,
             DCON_OBS,
             DCON_EMPR)

          VALUES
            (1,
             V.CLAVE_FIN,
             895,
             2520,
             V_FCON_TIPO_SALDO,
             V.TOTAL,
             V.TOTAL,
             0,
             0,
             0,
             0,
             1,
             'COBRO ANTICIPO FUNC. '||l_empr_funcionario_desc,
             IN_EMPRESA);
        EXCEPTION
          WHEN OTHERS THEN
            IF SQLCODE = -2291 then
              begin
              INSERT INTO FIN_DOCUMENTO
                (DOC_CLAVE,
                 DOC_EMPR,
                 DOC_CTA_BCO,
                 DOC_SUC,
                 DOC_TIPO_MOV,
                 DOC_NRO_DOC,
                 DOC_TIPO_SALDO,
                 DOC_MON,
                 DOC_PROV,
                 DOC_CLI_NOM,
                 DOC_FEC_OPER,
                 DOC_FEC_DOC,
                 DOC_BRUTO_EXEN_LOC,
                 DOC_BRUTO_EXEN_MON,
                 DOC_BRUTO_GRAV_LOC,
                 DOC_BRUTO_GRAV_MON,
                 DOC_NETO_EXEN_LOC,
                 DOC_NETO_EXEN_MON,
                 DOC_NETO_GRAV_LOC,
                 DOC_NETO_GRAV_MON,
                 DOC_IVA_LOC,
                 DOC_IVA_MON,
                 DOC_SALDO_INI_MON,
                 DOC_SALDO_LOC,
                 DOC_SALDO_MON,
                 DOC_SALDO_PER_ACT_LOC,
                 DOC_SALDO_PER_ACT_MON,
                 DOC_OBS,
                 DOC_BASE_IMPON_LOC,
                 DOC_BASE_IMPON_MON,
                 DOC_LOGIN,
                 DOC_FEC_GRAB,
                 DOC_SIST,
                 DOC_OPERADOR,
                 --DOC_CLAVE_FIN_DESTINO  ,
                 DOC_CLAVE_PADRE,
                 DOC_NRO_TRANS_COMBUS)
              VALUES
                (V.CLAVE_FIN,
                 IN_EMPRESA,
                 NULL,
                 1,
                 33,
                 V.NRO_RECIBO,
                 V_DOC_TIPO_SALDO,
                 1,
                 V.CLI_CODIGO,
                 NULL,
                 V.FECHA_OPER,--TRUNC(SYSDATE),
                 V.FECHA_DOC,  --TRUNC(SYSDATE),
                 V.TOTAL,
                 V.TOTAL,
                 0,
                 0,
                 V.TOTAL,
                 V.TOTAL,
                 0,
                 0,
                 0,
                 0,
                 0,
                 0,
                 0,
                 0,
                 0,
                 'COBRO ANTICIPO FUNC. '||l_empr_funcionario_desc, --'ANTIC. Personal-Modulo RRHH',
                 0,
                 0,
                 GEN_DEVUELVE_USER,
                 SYSDATE,
                 'FIN',
                 2,
                 NULL,
                 I_NRO_PROCESO);
              exception
                when others then
                  Raise_application_error(-20000, 'Clave '||V.CLAVE_FIN||' '||sqlerrm);
              end;

              INSERT INTO FIN_DOC_CONCEPTO
                (DCON_ITEM,
                 DCON_CLAVE_DOC,
                 DCON_CLAVE_CONCEPTO,
                 DCON_CLAVE_CTACO,
                 DCON_TIPO_SALDO,
                 DCON_EXEN_LOC,
                 DCON_EXEN_MON,
                 DCON_GRAV_LOC,
                 DCON_GRAV_MON,
                 DCON_IVA_LOC,
                 DCON_IVA_MON,
                 DCON_IND_TIPO_IVA_COMPRA,
                 DCON_OBS,
                 DCON_EMPR)

              VALUES
                (1,
                 V.CLAVE_FIN,
                 895,
                 2520,
                 V_FCON_TIPO_SALDO,
                 V.TOTAL,
                 V.TOTAL,
                 0,
                 0,
                 0,
                 0,
                 1,
                 'COBRO ANTICIPO FUNC. '||l_empr_funcionario_desc,
                 IN_EMPRESA);
            else
              Raise_application_error(-20000, 'Dupl clave '||V.CLAVE_FIN||' '||sqlerrm);
            END IF;
        END;

      END IF;

      FOR X IN (SELECT *
                  FROM FIN_FINI037_TEMP T
                 WHERE T.CLAVE_FIN = V.CLAVE_FIN
                   AND T.EMPR = IN_EMPRESA
                   AND T.TIPO_MOV = V.TIPO_MOV
                 ORDER BY rowid
                ) LOOP
        INSERT INTO FIN_PAGO
          (PAG_CLAVE_DOC,
           PAG_FEC_VTO,
           PAG_CLAVE_PAGO,
           PAG_FEC_PAGO,
           PAG_IMP_LOC,
           PAG_IMP_MON,
           PAG_LOGIN,
           PAG_FEC_GRAB,
           PAG_SIST,
           PAG_IMP_INT_LOC,
           PAG_IMP_INT_MON,
           PAG_EMPR)
        VALUES
          (X.CLAVE_FAC,
           X.CUO_FEC_VTO,
           V.CLAVE_FIN,
           V.FECHA_DOC,
           X.CUO_IMP_MON,
           X.CUO_IMP_MON,
           GEN_DEVUELVE_USER,
           TRUNC(V_FECHA_GRAB),
           'FIN',
           NULL,
           NULL,
           IN_EMPRESA);

      END LOOP;

      --INSERTA ANTICIPO EN LA BD DE HILAGRO USA V_EMPRESA CON LOS DATOS DE EMPRESA CORRESPONDIENTE A HILAGRO

      FOR REG IN (SELECT FCAT_CODIGO,
                         T.CLAVE_FIN,
                         T.NRO_RECIBO,
                         T.CLI_CODIGO,
                         T.CLI_NOM,
                         T.CLI_COD_EMPL_EMPR_ORIG,
                         T.FECHA_DOC,
                         T.FECHA_OPER,
                         SUM(T.CUO_IMP_MON) TOTAL,
                         T.TIPO_MOV,
                         T.LINEA_NEGOCIO,
                         T.LIN_CON_HIL,
                         cl.cli_cod_empl_empr_orig cod_empleado
                    FROM FIN_FINI037_TEMP T
                    inner join fin_cliente cl on (cl.cli_codigo = t.cli_codigo and cl.cli_empr = t.empr)
                   WHERE T.EMPR = IN_EMPRESA
                     AND T.CLI_CODIGO = V.CLI_CODIGO
                     AND T.TIPO_MOV = V.TIPO_MOV
                     AND T.CLAVE_FIN = V.CLAVE_FIN
                   GROUP BY FCAT_CODIGO,
                            T.CLAVE_FIN,
                            T.NRO_RECIBO,
                            T.CLI_CODIGO,
                            T.CLI_NOM,
                            T.CLI_COD_EMPL_EMPR_ORIG,
                            T.FECHA_DOC,
                            T.FECHA_OPER,
                            T.TIPO_MOV,
                            T.LINEA_NEGOCIO,
                            T.LIN_CON_HIL,
                            cl.cli_cod_empl_empr_orig
                   ORDER BY 1) LOOP

        IF V.TIPO_MOV = l_tmv_fact_cre_emit THEN
          V_OBSERVACION := 'MIGRADO DE TRANSAGRO ' || REG.LINEA_NEGOCIO ||
                           ' RECIBO =';

        ELSE
          V_OBSERVACION := 'COBRO ADELANTO MIGRADO DE TRANSAGRO RECIBO=';
        END IF;

        if I_CATEGORIA <> l_cat_cli_func_grupo then

          SELECT EMPL_CODIGO_PROV
            INTO V_PROV_DEST
            FROM PER_EMPLEADO
           WHERE EMPL_EMPRESA = co_empr_hilagro
             AND EMPL_LEGAJO = REG.CLI_COD_EMPL_EMPR_ORIG;

          BEGIN
            SELECT FCON_CLAVE_CTACO
              INTO V_CTA_HIL
              FROM FIN_CONCEPTO A
             WHERE FCON_CLAVE = REG.LIN_CON_HIL
               AND FCON_EMPR = co_empr_hilagro;
          EXCEPTION
            WHEN OTHERS THEN
              V_CTA_HIL := NULL;
          END;
        else
          
          if v_empresa is null then
             v_empresa := fini237.obt_empresa_func_grupo(in_cliente => reg.cli_codigo, in_empresa => in_empresa);
          end if;
          
          -- en la base_datos remota es necesario que esto se_calcule
          V_CTA_HIL := REG.LIN_CON_HIL;

          if v_cta_hil is null then
            Raise_application_error(-20000, 'Es necesario que la empresa externa configure el detalle de su linea de negocio. Programa 3-1-90');
          end if;

          select to_char(empl.cod_prov)
          into V_PROV_DEST
          from table( fini237.pipe_per_empleado_grupo(in_empr_grupo => v_empresa) ) empl
          where empl.cod_empl = reg.cod_empleado;

          <<obt_sufijo_empr>>
          begin
            select ex.empr_sufijo
            into l_sufijo
            from gen_empresa ex
            where ex.empr_codigo = v_empresa;
          exception
            when no_data_found then
              l_sufijo := null;
          end obt_sufijo_empr;

        end if;
        
        -- get PK
        SELECT FIN_SEQ_DOC_NEXTVAL INTO V_DOC_CLAVE_FIN_DESTINO FROM DUAL;

        if l_sufijo is null then
          <<obt_cuenta_contable_concepto>>
          BEGIN
            SELECT FCON_CLAVE_CTACO
            INTO V_CTA_HIL
            FROM FIN_CONCEPTO A
            WHERE FCON_CLAVE = REG.LIN_CON_HIL
            AND   FCON_EMPR  = in_empresa_grupo;
          EXCEPTION
              WHEN no_data_found THEN
                Raise_application_error(-20000, 'Configure en el 3-1-90 en la linea de negocio '
                                        || REG.LIN_CON_HIL ||' de la empresa '|| in_empresa_grupo||
                                        ' el concepto correspondiente'
                                        );
          end obt_cuenta_contable_concepto;
        
          <<obt_cta_adel_prov>>
          begin
            select f.conf_cta_adel_prov
            into l_cta_adelanto_prov
            from fin_configuracion f
            where f.conf_empr = v_empresa;

            if l_cta_adelanto_prov is null then
              raise no_data_found;
            end if;

          exception
            when no_data_found then
              Raise_application_error(-20000, 'Es necesario configurar en la empresa la cuenta de adelanto proveedor');
          end obt_cta_adel_prov;

          begin
          INSERT INTO FIN_DOCUMENTO
            (DOC_CLAVE,
             DOC_EMPR,
             DOC_CTA_BCO,
             DOC_SUC,
             DOC_TIPO_MOV,
             DOC_NRO_DOC,
             DOC_TIPO_SALDO,
             DOC_MON,
             DOC_PROV,
             DOC_CLI_NOM,
             DOC_FEC_OPER,
             DOC_FEC_DOC,
             DOC_BRUTO_EXEN_LOC,
             DOC_BRUTO_EXEN_MON,
             DOC_BRUTO_GRAV_LOC,
             DOC_BRUTO_GRAV_MON,
             DOC_NETO_EXEN_LOC,
             DOC_NETO_EXEN_MON,
             DOC_NETO_GRAV_LOC,
             DOC_NETO_GRAV_MON,
             DOC_IVA_LOC,
             DOC_IVA_MON,
             DOC_OBS,
             DOC_BASE_IMPON_LOC,
             DOC_BASE_IMPON_MON,
             DOC_DIF_CAMBIO,
             DOC_LOGIN,
             DOC_FEC_GRAB,
             DOC_SIST,
             DOC_OPERADOR,
             DOC_TASA,
             DOC_DIF_CAMBIO_ACUM,
             DOC_EMPLEADO,
             DOC_IND_CANC_PMO,
             DOC_CTACO,
             DOC_NRO_TRANS_COMBUS,
             DOC_CLAVE_PADRE_ORIG)
          VALUES
            (V_DOC_CLAVE_FIN_DESTINO,
             V_EMPRESA,
             NULL,
             1,  --> suc Central
             co_tmv_srv_tagro,
             REG.NRO_RECIBO,
             'C',
             co_mnd_gs,
             V_PROV_DEST,
             NULL,
             REG.FECHA_OPER,
             REG.FECHA_DOC,
             REG.TOTAL,
             REG.TOTAL,
             0,
             0,
             REG.TOTAL,
             REG.TOTAL,
             0,
             0,
             0,
             0,
             V_OBSERVACION || REG.NRO_RECIBO,
             0,
             0,
             0,
             GEN_DEVUELVE_USER,
             SYSDATE,
             'FIN',
             2,
             1,
             0,
             REG.CLI_COD_EMPL_EMPR_ORIG,
             'N',
             V_CTA_HIL,
             I_NRO_PROCESO,
             V.CLAVE_FIN
             );
          exception
            when others then
              Raise_application_error(-20000, 'Agregando detalle Adelanto Clave: '||V_DOC_CLAVE_FIN_DESTINO||' '||sqlerrm);
          end;

          INSERT INTO FIN_DOC_CONCEPTO
            (DCON_EMPR,
             DCON_CLAVE_DOC,
             DCON_ITEM,
             DCON_CLAVE_CONCEPTO,
             DCON_CLAVE_CTACO,
             DCON_TIPO_SALDO,
             DCON_EXEN_LOC,
             DCON_EXEN_MON,
             DCON_GRAV_LOC,
             DCON_GRAV_MON,
             DCON_IVA_LOC,
             DCON_IVA_MON,
             DCON_IND_TIPO_IVA_COMPRA,
             DCON_OBS)
          VALUES
            (V_EMPRESA,
             V_DOC_CLAVE_FIN_DESTINO,
             1,
             REG.LIN_CON_HIL, --1480
             l_cta_adelanto_prov, --> anticipo proveedor
             'C',
             REG.TOTAL,
             REG.TOTAL,
             0,
             0,
             0,
             0,
             1,
             V_OBSERVACION || V.NRO_RECIBO);

          INSERT INTO FIN_CUOTA
            (CUO_CLAVE_DOC,
             CUO_FEC_VTO,
             CUO_IMP_LOC,
             CUO_IMP_MON,
             CUO_DIF_CAMBIO,
             CUO_EMPR)
          VALUES
            (V_DOC_CLAVE_FIN_DESTINO,
             LAST_DAY(REG.FECHA_DOC),
             REG.TOTAL,
             REG.TOTAL,
             0,
             V_EMPRESA
             );
        else --> with sufix
          <<add_dynamic_fin_documento>>
          declare
            l_query_fin_doc varchar2(32474);
          begin
            l_query_fin_doc := 'INSERT INTO FIN_DOCUMENTO_'||TRIM(UPPER(l_sufijo));
            l_query_fin_doc := l_query_fin_doc||
            '(DOC_CLAVE,DOC_EMPR,DOC_SUC,DOC_TIPO_MOV,DOC_NRO_DOC,DOC_TIPO_SALDO,DOC_MON,DOC_PROV,DOC_BRUTO_EXEN_LOC,DOC_BRUTO_EXEN_MON,DOC_BRUTO_GRAV_LOC,DOC_BRUTO_GRAV_MON,DOC_NETO_EXEN_LOC,DOC_NETO_EXEN_MON,DOC_NETO_GRAV_LOC,DOC_NETO_GRAV_MON,DOC_IVA_LOC,DOC_IVA_MON,DOC_OBS,DOC_BASE_IMPON_LOC,DOC_BASE_IMPON_MON,DOC_DIF_CAMBIO,DOC_LOGIN,DOC_SIST,DOC_OPERADOR,DOC_TASA,DOC_DIF_CAMBIO_ACUM,DOC_EMPLEADO,DOC_IND_CANC_PMO,DOC_CTACO,DOC_NRO_TRANS_COMBUS,DOC_CLAVE_PADRE_ORIG,DOC_FEC_OPER,DOC_FEC_DOC)VALUES(';

            l_query_fin_doc := l_query_fin_doc||V_DOC_CLAVE_FIN_DESTINO||',';
            l_query_fin_doc := l_query_fin_doc||V_EMPRESA||',';
            l_query_fin_doc := l_query_fin_doc||1||',';
            l_query_fin_doc := l_query_fin_doc||co_tmv_srv_tagro||',';
            l_query_fin_doc := l_query_fin_doc||REG.NRO_RECIBO||',';
            l_query_fin_doc := l_query_fin_doc||'''C'''||',';
            l_query_fin_doc := l_query_fin_doc||co_mnd_gs||',';
            l_query_fin_doc := l_query_fin_doc||V_PROV_DEST||',';
            l_query_fin_doc := l_query_fin_doc||REG.TOTAL||',';
            l_query_fin_doc := l_query_fin_doc||REG.TOTAL||',';
            l_query_fin_doc := l_query_fin_doc||0||',';
            l_query_fin_doc := l_query_fin_doc||0||',';
            l_query_fin_doc := l_query_fin_doc||REG.TOTAL||',';
            l_query_fin_doc := l_query_fin_doc||REG.TOTAL||',';
            l_query_fin_doc := l_query_fin_doc||0||',';
            l_query_fin_doc := l_query_fin_doc||0||',';
            l_query_fin_doc := l_query_fin_doc||0||',';
            l_query_fin_doc := l_query_fin_doc||0||',';

            l_query_fin_doc := l_query_fin_doc||'''';
            l_query_fin_doc := l_query_fin_doc||V_OBSERVACION||REG.nRO_RECIBO;
            l_query_fin_doc := l_query_fin_doc||''''||',';

            l_query_fin_doc := l_query_fin_doc||0||',';
            l_query_fin_doc := l_query_fin_doc||0||',';
            l_query_fin_doc := l_query_fin_doc||0||',';

            l_query_fin_doc := l_query_fin_doc||'''';
            l_query_fin_doc := l_query_fin_doc||gen_devuelve_user;
            l_query_fin_doc := l_query_fin_doc||''''||',';

            l_query_fin_doc := l_query_fin_doc||'''FIN'''||',';
            l_query_fin_doc := l_query_fin_doc||2||',';
            l_query_fin_doc := l_query_fin_doc||1||',';
            l_query_fin_doc := l_query_fin_doc||0||',';
            l_query_fin_doc := l_query_fin_doc||REG.CLI_COD_EMPL_EMPR_ORIG||',';
            l_query_fin_doc := l_query_fin_doc||'''N'''||',';
            l_query_fin_doc := l_query_fin_doc||NVL(V_CTA_HIL, 9999)||',';
            l_query_fin_doc := l_query_fin_doc||I_NRO_PROCESO||',';
            l_query_fin_doc := l_query_fin_doc||V.CLAVE_FIN||',';

            l_query_fin_doc := l_query_fin_doc||'to_date('''||REG.FECHA_OPER||''''||',';
            l_query_fin_doc := l_query_fin_doc||'''dd/mm/yyyy''),';

            l_query_fin_doc := l_query_fin_doc||'to_date('''||REG.FECHA_DOC||''''||',';
            l_query_fin_doc := l_query_fin_doc||'''dd/mm/yyyy'')) ';

            execute immediate l_query_fin_doc;

          end add_dynamic_fin_documento;

          -- calcular en base_datos remota este dato
          -- Tabla FIN_CONFIGURACION >> col: CONF_CTA_ADEL_PROV
          l_cta_adelanto_prov := null;

          <<add_dynamic_fin_doc_concepto>>
          declare
            l_query_fin_doc_conc varchar2(32747);
          begin
            l_query_fin_doc_conc := 'INSERT INTO FIN_DOC_CONCEPTO_'||trim(upper(l_sufijo));

            l_query_fin_doc_conc := l_query_fin_doc_conc ||
            '(DCON_EMPR,DCON_CLAVE_DOC,DCON_ITEM,DCON_CLAVE_CONCEPTO,DCON_CLAVE_CTACO,DCON_TIPO_SALDO,DCON_EXEN_LOC,DCON_EXEN_MON,DCON_GRAV_LOC,DCON_GRAV_MON,DCON_IVA_LOC,DCON_IVA_MON,DCON_IND_TIPO_IVA_COMPRA,DCON_OBS)
              VALUES (';
            l_query_fin_doc_conc := l_query_fin_doc_conc ||V_EMPRESA||',';
            l_query_fin_doc_conc := l_query_fin_doc_conc ||V_DOC_CLAVE_FIN_DESTINO||',';
            l_query_fin_doc_conc := l_query_fin_doc_conc ||1||',';
            l_query_fin_doc_conc := l_query_fin_doc_conc ||REG.LIN_CON_HIL||',';
            l_query_fin_doc_conc := l_query_fin_doc_conc ||nvl(l_cta_adelanto_prov, 999)||',';
            l_query_fin_doc_conc := l_query_fin_doc_conc ||'''C'''||',';
            l_query_fin_doc_conc := l_query_fin_doc_conc ||REG.TOTAL||',';
            l_query_fin_doc_conc := l_query_fin_doc_conc ||0||',';
            l_query_fin_doc_conc := l_query_fin_doc_conc ||0||',';
            l_query_fin_doc_conc := l_query_fin_doc_conc ||0||',';
            l_query_fin_doc_conc := l_query_fin_doc_conc ||0||',';
            l_query_fin_doc_conc := l_query_fin_doc_conc ||0||',';
            l_query_fin_doc_conc := l_query_fin_doc_conc ||1||',';

            l_query_fin_doc_conc := l_query_fin_doc_conc||'''';
            l_query_fin_doc_conc := l_query_fin_doc_conc ||V_OBSERVACION ||V_NRO_RECIBO;
            l_query_fin_doc_conc := l_query_fin_doc_conc||''''||')';

            execute immediate l_query_fin_doc_conc;
          end add_dynamic_fin_doc_concepto;

          <<add_dynamic_fin_cuota>>
          declare
            l_query_fin_cuota varchar2(32747);
          begin
            l_query_fin_cuota := 'INSERT INTO FIN_CUOTA_'||trim(upper(l_sufijo));

            l_query_fin_cuota := l_query_fin_cuota||
                        '(CUO_CLAVE_DOC,CUO_FEC_VTO,CUO_IMP_LOC,CUO_IMP_MON,CUO_DIF_CAMBIO,CUO_EMPR)
                      VALUES(';
            l_query_fin_cuota := l_query_fin_cuota||V_DOC_CLAVE_FIN_DESTINO||',';

            l_query_fin_cuota := l_query_fin_cuota||'to_date(''';
            l_query_fin_cuota := l_query_fin_cuota||LAST_DAY(REG.FECHA_DOC);
            l_query_fin_cuota := l_query_fin_cuota||''', ''dd/mm/yyyy'')'||',';

            l_query_fin_cuota := l_query_fin_cuota||REG.TOTAL||',';
            l_query_fin_cuota := l_query_fin_cuota||REG.TOTAL||',';
            l_query_fin_cuota := l_query_fin_cuota||0||',';
            l_query_fin_cuota := l_query_fin_cuota||V_EMPRESA||')';

            execute immediate l_query_fin_cuota;
          end add_dynamic_fin_cuota;

        end if; --> condicion de SUFIJO por empresa
      END LOOP;
    END LOOP;
    --

  ELSIF I_OPERACION = 'D' THEN
    <<f_doc_tagro>>
    FOR V IN (SELECT C.DOC_CLAVE, DOC_CLAVE_PADRE, nvl(c.doc_cli, c.doc_prov) doc_pro_cli, c.doc_empr
                FROM FIN_DOCUMENTO C
               WHERE C.DOC_NRO_TRANS_COMBUS = I_NRO_PROCESO
                 AND DOC_EMPR = IN_EMPRESA
               ORDER BY 2
               )
    loop
      if I_CATEGORIA = co_func_hilagro then
        <<f_doc_hilagro>>
        FOR RR IN (SELECT DOC_CLAVE, DOC_EMPR
                     FROM FIN_DOCUMENTO C
                    WHERE C.DOC_NRO_TRANS_COMBUS = I_NRO_PROCESO
                      AND DOC_EMPR = co_empr_hilagro
                  )
        LOOP

          DELETE FROM FIN_CUOTA P
           WHERE P.CUO_CLAVE_DOC = RR.DOC_CLAVE
             AND P.CUO_EMPR = co_empr_hilagro;

          DELETE FROM FIN_DOC_CONCEPTO Q
           WHERE Q.DCON_CLAVE_DOC = RR.DOC_CLAVE
             AND Q.DCON_EMPR = co_empr_hilagro;

          DELETE FROM FIN_DOCUMENTO R
           WHERE R.DOC_CLAVE = RR.DOC_CLAVE
             AND R.DOC_EMPR = co_empr_hilagro;

        END loop f_doc_hilagro;
      else
        begin
          select c.cli_empresa_funcionario
          into v_empresa
          from fin_cliente c
          where c.cli_codigo = v.doc_pro_cli --> en TAGRO es igual el nro de cliente y proveedor
          and   c.cli_empr   = v.doc_empr;
        exception
          when no_datA_found then
            Raise_application_error(-20000, 'No se encontro la empresa del cliente (cat func grupo): '||v.doc_pro_cli);
        end;

        select e.empr_sufijo
        into l_sufijo
        from gen_empresa e
        where e.empr_codigo = v_empresa;

        if l_sufijo is null then
          FOR grup_empr IN (SELECT DOC_CLAVE
                            FROM FIN_DOCUMENTO C
                            WHERE C.DOC_NRO_TRANS_COMBUS = I_NRO_PROCESO
                            AND DOC_EMPR = v_empresa
          )
          loop
            DELETE FROM FIN_CUOTA P
            WHERE P.CUO_CLAVE_DOC = grup_empr.doc_clave
            AND   P.CUO_EMPR      = v_empresa;

            DELETE FROM FIN_DOC_CONCEPTO Q
            WHERE Q.DCON_CLAVE_DOC = grup_empr.DOC_CLAVE
            AND   Q.DCON_EMPR      = v_empresa;

            DELETE FROM FIN_DOCUMENTO R
            WHERE R.DOC_CLAVE = grup_empr.DOC_CLAVE
            AND   R.DOC_EMPR  = v_empresa;
          end loop;
        else
          l_del_query := 'select doc_clave from fin_documento_'||l_sufijo||
                         ' where doc_nro_trans_combus='||i_nro_proceso||
                         ' and doc_empr='||v_empresa;

          open c_del_dynamic for l_del_query;
            loop
              fetch c_del_dynamic into l_del_clave;
              exit when c_del_dynamic%notfound;

              <<del_fin_cuota>>
              declare
               l_query_fc varchar2(32747);
              begin
                l_query_fc := 'delete from FIN_CUOTA_'||trim(upper(l_sufijo));
                l_query_fc := l_query_fc||' where CUO_CLAVE_DOC='||l_del_clave;

                execute immediate l_query_fc;
              end del_fin_cuota;

              <<del_fin_doc_concepto>>
              declare
               l_query_fdc varchar2(32747);
              begin
                l_query_fdc := 'delete from FIN_DOC_CONCEPTO_'||trim(upper(l_sufijo));
                l_query_fdc := l_query_fdc||' where DCON_CLAVE_DOC='||l_del_clave;

                execute immediate l_query_fdc;
              end del_fin_doc_concepto;

              <<del_fin_documento>>
              declare
               l_query_fd varchar2(32747);
              begin
                l_query_fd := 'delete from FIN_DOCUMENTO_'||trim(upper(l_sufijo));
                l_query_fd := l_query_fd||' where DOC_CLAVE='||l_del_clave;

                execute immediate l_query_fd;
              end del_fin_documento;
            end loop;
          close c_del_dynamic;

        end if; --> fin sufix
      end if;

      DELETE FROM FIN_PAGO P
       WHERE P.PAG_CLAVE_PAGO = V.DOC_CLAVE
         AND P.PAG_EMPR = IN_EMPRESA;

      DELETE FROM FIN_DOC_CONCEPTO Q
       WHERE Q.DCON_CLAVE_DOC = V.DOC_CLAVE
         AND Q.DCON_EMPR = IN_EMPRESA;

      DELETE FROM FIN_DOCUMENTO R
       WHERE R.DOC_CLAVE = V.DOC_CLAVE
         AND R.DOC_EMPR = IN_EMPRESA;

    END loop f_doc_tagro;

  END IF;

END;
/
