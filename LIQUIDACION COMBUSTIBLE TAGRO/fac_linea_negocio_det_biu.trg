create or replace trigger fac_linea_negocio_det_biu
    before insert or update  
    on fac_linea_negocio_det 
    for each row 
begin 
    if :new.id is null then 
        :new.id := fac_linea_negocio_det_seq.nextval; 
    end if; 
    if inserting then 
        :new.created := sysdate; 
        :new.created_by := nvl(sys_context('APEX$SESSION','APP_USER'),user); 
    end if; 
    :new.updated := sysdate; 
    :new.updated_by := nvl(sys_context('APEX$SESSION','APP_USER'),user); 
end fac_linea_negocio_det_biu; 
/
