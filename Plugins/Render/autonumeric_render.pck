create or replace package autonumeric_render is

  -- Author  : @PabloACespedes
  -- Created : 28/07/2022 11:10:17
  -- Purpose : pasar renderizacion a Backend del plugin
  
  procedure render (
    p_item   in            apex_plugin.t_item,
    p_plugin in            apex_plugin.t_plugin,
    p_param  in            apex_plugin.t_item_render_param,
    p_result in out nocopy apex_plugin.t_item_render_result 
  );
  
  procedure metadata_autonumeric (
  p_item   in            apex_plugin.t_item,
  p_plugin in            apex_plugin.t_plugin,
  p_param  in            apex_plugin.t_item_meta_data_param,
  p_result in out nocopy apex_plugin.t_item_meta_data_result 
  );
  
end autonumeric_render;
/
create or replace package body autonumeric_render is

  -- GLOBAL
subtype gt_string is varchar2(32767);
subtype lt_string is varchar2(500);

--------------------------------------------------------------------------------
-- Render Procedure
--------------------------------------------------------------------------------
procedure render (
    p_item   in            apex_plugin.t_item,
    p_plugin in            apex_plugin.t_plugin,
    p_param  in            apex_plugin.t_item_render_param,
    p_result in out nocopy apex_plugin.t_item_render_result )
is

  c_display_only_id_postfix constant varchar2(30) := '_DISPLAY';
  c_is_display_only constant boolean := p_param.is_readonly or p_param.is_printer_friendly;

  -- Item componens
  l_options                    gt_string := p_item.attribute_01;
  l_nls_replace                boolean := false;
  l_alignment                  lt_string := nvl(p_item.attribute_02,p_plugin.attribute_01);
  l_alignment_class            lt_string;  
 
  -- Component type
  l_component_type lt_string         := p_item.component_type_id;
  l_comp_type_ig_column lt_string    := apex_component.c_comp_type_ig_column;
    
  l_onload_code gt_string := '';
begin

   apex_debug.info('plugin '||p_item.name||' started');
   apex_debug.info('l_component_type '||l_component_type);
   apex_debug.info('l_comp_type_ig_column '||l_comp_type_ig_column);
   apex_debug.info('c_is_display_only:  '|| case when c_is_display_only then 'TRUE' else 'FALSE' end);
   apex_plugin_util.debug_page_item (
    p_plugin    => p_plugin,
    p_page_item => p_item );
    
    
    if l_alignment = 'LEFT' then 
      l_alignment_class := null;
    elsif l_alignment = 'CENTER' then
      l_alignment_class := 'u-tC'; 
    elsif l_alignment = 'RIGHT' then
      l_alignment_class := 'u-tE'; 
    else
      l_alignment_class := null;
      apex_debug.error('Unhanlded alignment option: %s', l_alignment);
    end if;    
     
    if  wwv_flow.g_nls_decimal_separator=',' 
    then
        l_nls_replace := true;
    end if;
     
     apex_debug.info('l_nls_replace :%s',case when l_nls_replace then 'true' else 'false' end);
     
 
    
    if l_component_type = l_comp_type_ig_column then -- interactive grid
        apex_debug.info('item is in IG ');
        
        if p_param.is_readonly or p_param.is_printer_friendly then
            apex_debug.info('is_readonly  or is_printer_friendly');
            
        end if ;
        
        apex_debug.info('not READONLY or PRINTERFRENDLY');

        sys.htp.prn(
            apex_string.format(
                '<div class="ig-div-autonumeric"><input %s id="%s"  type="text" %s value="%s" %s /></div>'
                , apex_plugin_util.get_element_attributes(p_item, p_item.id, 'apex-item-text ig-auto-numeric apex-item-plugin')
                , p_item.name
                , case when p_item.placeholder is not null then 'placeholder="'||p_item.placeholder||'"' end
                , case when p_param.value is null then '' else ltrim( rtrim ( apex_escape.html_attribute(case when l_nls_replace then replace(p_param.value,',','.') else p_param.value end) ) ) end
                , case when p_param.is_readonly or p_param.is_printer_friendly then 'readonly' else '' end
             )
         );

        l_onload_code := l_onload_code||
            apex_string.format(
              'ANIGinit("%s", %s, %s);'
              , p_item.name 
              , nvl(l_options, '{}')
              , case when l_nls_replace then 'true' else 'false' end  
        );   
            
               
               
        p_result.is_navigable := (not p_param.is_readonly = false and not p_param.is_printer_friendly);
  
    
    else  --normal page item
        
      if c_is_display_only then
            apex_plugin_util.print_hidden_if_readonly( 
              p_item  => p_item
              ,p_param => p_param
            );

            apex_plugin_util.print_display_only( 
              p_item_name          => p_item.name
              , p_display_value    => p_param.value
              , p_show_line_breaks => true
              , p_escape           => false
              , p_attributes       => p_item.element_attributes
            );        
        
        else
            sys.htp.prn(
                apex_string.format(
                    '<input type="text" id="%s" name="%s" placeholder="%s" class="%s" value="%s" size="%s" maxlength="%s" %s %s />%s'
                    ,p_item.name
                    ,p_item.name
                    ,p_item.placeholder
                    ,'text_field apex-item-text apex-item-autonumeric '||p_item.element_css_classes|| case when p_item.icon_css_classes is not null then ' apex-item-has-icon' else null end || case when p_item.ignore_change then ' js-ignoreChange' end
                    ,case when p_item.escape_output then sys.htf.escape_sc(case when l_nls_replace then replace(p_param.value,',','.') else p_param.value end) else p_param.value end
                    ,nvl(p_item.element_width,30)
                    ,p_item.element_max_length
                    ,p_item.element_attributes
                    ,case when p_param.is_readonly then 'disabled="disabled" ' else '' end
                    ,case when p_item.icon_css_classes is not null then '<span class="apex-item-icon fa '|| p_item.icon_css_classes ||'" aria-hidden="true"></span>' else null end 
                )
            );

        end if;

        -- init AutoNumeric instance
        l_onload_code := l_onload_code|| 
                apex_string.format(
                        'ANForminit("%s", %s, %s, "%s");'
                        , p_item.name || case when c_is_display_only then c_display_only_id_postfix end
                        , nvl(l_options, '{}')
                        , case when l_nls_replace then 'true' else 'false' end
                        , l_alignment_class
                );      
    

    end if;
   
    apex_javascript.add_onload_code (p_code => l_onload_code);
   
    apex_debug.info('plugin '||p_item.name||' ended');
   

end render;

--------------------------------------------------------------------------------
-- Meta Data Procedure
--------------------------------------------------------------------------------
procedure metadata_autonumeric (
  p_item   in            apex_plugin.t_item,
  p_plugin in            apex_plugin.t_plugin,
  p_param  in            apex_plugin.t_item_meta_data_param,
  p_result in out nocopy apex_plugin.t_item_meta_data_result )
is
begin
  p_result.escape_output := false;
end metadata_autonumeric;

end autonumeric_render;
/
