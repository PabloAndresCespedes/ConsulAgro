CREATE OR REPLACE PROCEDURE STK_ACT_ART_EMP_OPER(V_TIPO_OPER    IN VARCHAR2,
                                                 V_ART          IN STK_DOCUMENTO_DET.DETA_ART%TYPE,
                                                 V_CLAVE_DOC    IN STK_DOCUMENTO_DET.DETA_CLAVE_DOC%TYPE,
                                                 V_CANT         IN STK_ART_EMPR_PERI.AEP_TOT_EXIST%TYPE,
                                                 V_IMP_NETO_LOC IN STK_DOCUMENTO_DET.DETA_IMP_NETO_LOC%TYPE,
                                                 V_EMPRESA      IN GEN_EMPRESA.EMPR_CODIGO%TYPE) IS
  --
  -- declaracion de registros, variables y cursores
  V_DOCU_CODIGO_OPER STK_DOCUMENTO.DOCU_CODIGO_OPER%TYPE;
  V_DOCU_TASA_US     STK_DOCUMENTO.DOCU_TASA_US%TYPE;
  V_DOCU_FEC_OPER    STK_DOCUMENTO.DOCU_FEC_OPER%TYPE;

  V_OPER_DESC    STK_OPERACION.OPER_DESC%TYPE;
  V_OPER_ENT_SAL STK_OPERACION.OPER_ENT_SAL%TYPE;

  V_CONF_PERIODO_ACT  STK_CONFIGURACION.CONF_PERIODO_ACT%TYPE;
  V_CONF_PERIODO_SGTE STK_CONFIGURACION.CONF_PERIODO_SGTE%TYPE;
  --
  SALIR EXCEPTION;

  --
  V_IMP_NETO_MON  NUMBER := 0;
  V_TOT_EXIST     NUMBER := 0;
  V_EC_COMPRA     NUMBER := 0;
  V_EL_COMPRA     NUMBER := 0;
  V_EM_COMPRA     NUMBER := 0;
  V_EC_COMPRA_GAN NUMBER := 0;

  V_SC_DEV_COMPRA NUMBER := 0;
  V_SL_DEV_COMPRA NUMBER := 0;
  V_SM_DEV_COMPRA NUMBER := 0;
  V_SC_VTA        NUMBER := 0;
  V_SL_VTA        NUMBER := 0;
  V_SM_VTA        NUMBER := 0;

  V_SC_VTA_GAN NUMBER := 0;

  V_EC_DEV_VTA NUMBER := 0;
  V_EL_DEV_VTA NUMBER := 0;
  V_EM_DEV_VTA NUMBER := 0;

  V_EC_PROD NUMBER := 0;
  V_EL_PROD NUMBER := 0;
  V_EM_PROD NUMBER := 0;

  V_EC_PROD_RP NUMBER := 0; --Cantidad de produccion en re-proceso
  V_EL_PROD_RP NUMBER := 0; --Costo loc de produccion en re-proceso
  V_EM_PROD_RP NUMBER := 0; --Costo mon de produccion en re-proceso

  V_EC_DEV_PROD NUMBER := 0;
  V_EL_DEV_PROD NUMBER := 0;
  V_EM_DEV_PROD NUMBER := 0;

  V_SC_PROD NUMBER := 0;

  V_SC_PROD_RP NUMBER := 0; --Cantidad de SALIDA DE produccion en re-proceso

  V_EC_KIT     NUMBER := 0;
  V_EL_KIT     NUMBER := 0;
  V_EM_KIT     NUMBER := 0;
  V_SC_KIT     NUMBER := 0;
  V_SL_KIT     NUMBER := 0;
  V_SM_KIT     NUMBER := 0;
  V_SC_PERDIDA NUMBER := 0;
  V_SC_CONS    NUMBER := 0;
  V_C_DIF_INV  NUMBER := 0;

  V_C_TRANSF     NUMBER := 0;
  V_C_TRANSF_DEV NUMBER := 0; --TRASLADO por devolucion

  V_C_REM      NUMBER := 0;
  V_EC_ARM_DES NUMBER := 0;
  V_EL_ARM_DES NUMBER := 0;
  V_EM_ARM_DES NUMBER := 0;
  V_SC_ARM_DES NUMBER := 0;
  V_SL_ARM_DES NUMBER := 0;
  V_SM_ARM_DES NUMBER := 0;

  V_EL_REVALUO NUMBER := 0;
  V_EM_REVALUO NUMBER := 0;
  V_SL_DEPREC  NUMBER := 0;
  V_SM_DEPREC  NUMBER := 0;

  V_TOT_CANT           NUMBER := 0;
  V_TOT_IMP_MON        NUMBER := 0;
  V_TOT_IMP_LOC        NUMBER := 0;
  V_COSTO_PROM_LOC     NUMBER := 0;
  V_COSTO_PROM_INI_LOC NUMBER := 0;
  V_COSTO_INI_LOC      NUMBER := 0;
  V_COSTO_PROM_MON     NUMBER := 0;
  V_COSTO_PROM_INI_MON NUMBER := 0;
  V_COSTO_INI_MON      NUMBER := 0;
  V_EXIST_INI          NUMBER := 0;

  V_PERIODO         NUMBER;
  V_IND_ACTUALIZADO VARCHAR2(1); -- S=Actualizo, N=NoActualizo
  
  -- 26/07/2022 8:02:42 @PabloACespedes \(^-^)/
  -- Utilizado para la cotizacion
  co_hilagro constant number := 1;
  
  --
  -- cursor usado para leer STK_DOCUMENTO
  V_TASA_AUX NUMBER := 0;
  CURSOR DOCU_CURSOR(CLAVE NUMBER) IS
    SELECT DOCU_CODIGO_OPER,
           NVL(DOCU_TASA_US, COT_VENTA),
           REPLACE(DOCU_FEC_OPER, '0019', '2019') DOCU_FEC_OPER
      FROM STK_DOCUMENTO,
           (SELECT C.COT_VENTA
              FROM STK_COTIZACION C
             WHERE C.COT_FEC = TRUNC(SYSDATE)
               AND C.COT_EMPR = co_hilagro -- @PabloACespedes
               AND C.COT_MON = 2)
     WHERE DOCU_CLAVE = CLAVE
       AND DOCU_EMPR = V_EMPRESA;
  --
  -- cursor usado para leer STK_OPERACION
  CURSOR OPER_CURSOR(CODIGO NUMBER) IS
    SELECT OPER_DESC, OPER_ENT_SAL
      FROM STK_OPERACION
     WHERE OPER_CODIGO = CODIGO
       AND OPER_EMPR = V_EMPRESA;
  --
  -- cursor usado para leer STK_ART_EMPR_PERI
  CURSOR AEP_CURSOR(PERIODO  IN NUMBER,
                    EMPRESA  IN NUMBER,
                    ARTICULO IN NUMBER) IS
    SELECT *
      FROM STK_ART_EMPR_PERI
     WHERE AEP_PERIODO >= PERIODO
       AND AEP_EMPR = EMPRESA
       AND AEP_ART = ARTICULO
     ORDER BY AEP_PERIODO
       FOR UPDATE;
  --
  -- cursor usado para leer STK_PERIODO
  -- (PERI: periodo del documento actual)
  CURSOR PERI_CURSOR(FECHA DATE) IS
    SELECT PERI_CODIGO
      FROM STK_PERIODO
     WHERE PERI_FEC_INI <=
           TO_DATE(TO_CHAR(FECHA, 'dd/mm/yyyy'), 'dd/mm/yyyy')
       AND PERI_FEC_FIN >=
           TO_DATE(TO_CHAR(FECHA, 'dd/mm/yyyy'), 'dd/mm/yyyy')
       AND PERI_EMPR = V_EMPRESA;
  /*
  WHERE PERI_FEC_INI <= FECHA -- to_char(FECHA, 'dd/mm/yyyy')
    AND PERI_FEC_FIN >= FECHA -- to_char(FECHA, 'dd/mm/yyyy')
    AND PERI_EMPR = V_EMPRESA;
    */

  --
  -- cursor usado para leer STK_CONFIGURACION
  CURSOR CONF_CURSOR(EMPRESA IN NUMBER) IS
    SELECT CONF_PERIODO_ACT, CONF_PERIODO_SGTE
      FROM STK_CONFIGURACION
     WHERE CONF_EMPR = EMPRESA;
  --
BEGIN

  OPEN DOCU_CURSOR(V_CLAVE_DOC);
  FETCH DOCU_CURSOR
    INTO V_DOCU_CODIGO_OPER, V_DOCU_TASA_US, V_DOCU_FEC_OPER;
  IF DOCU_CURSOR%NOTFOUND THEN
    CLOSE DOCU_CURSOR;
    RAISE_APPLICATION_ERROR(-20103,
                            'No existe stk_documento con clave: ' ||
                            TO_CHAR(V_CLAVE_DOC));
  END IF;
  /*
    IF V_DOCU_TASA_US IS NULL THEN
       RAISE_APPLICATION_ERROR(-20104,'Primero debe ingresar la cotizacion del dia.');
    END IF;
  */
  CLOSE DOCU_CURSOR;
  --
  OPEN OPER_CURSOR(V_DOCU_CODIGO_OPER);
  FETCH OPER_CURSOR
    INTO V_OPER_DESC, V_OPER_ENT_SAL;
  IF OPER_CURSOR%NOTFOUND THEN
    CLOSE OPER_CURSOR;
    RAISE_APPLICATION_ERROR(-20104,
                            'No existe codigo operacion: ' ||
                            TO_CHAR(V_DOCU_CODIGO_OPER));
  END IF;
  CLOSE OPER_CURSOR;
  --
  -- validar que DOCU_TASA_US sea la correcta
  IF (V_OPER_DESC = 'COMPRA' OR V_OPER_DESC = 'DEV_COM' OR
     V_OPER_DESC = 'ENT_PROD' OR V_OPER_DESC = 'ENT_PROD_RP' OR
     V_OPER_DESC = 'DEV_PROD' OR V_OPER_DESC = 'ANUL_VENTA' OR
     V_OPER_DESC = 'ANUL_NC' OR V_OPER_DESC = 'ENT_KIT' OR
     V_OPER_DESC = 'REVALUO' OR V_OPER_DESC = 'DEPRECIACION' OR
     V_OPER_DESC = 'ENT_ARM_DES') AND NVL(V_DOCU_TASA_US, 0) = 0 THEN
    RAISE_APPLICATION_ERROR(-20105,
                            'DOCU_TASA_US no puede ser nulo ni cero!');
    /* ELSE
    V_IMP_NETO_MON := ROUND(V_IMP_NETO_LOC / V_DOCU_TASA_US, 2);
    -- Actualizar la fecha de primer ingreso de la marca para que puede calcular
    -- el cuadro demostrativo promedio.
    UPDATE STK_MARCA
       SET MARC_FEC_PRIMER_INGRESO = V_DOCU_FEC_OPER
     WHERE MARC_CODIGO = (SELECT ART_MARCA
                            FROM STK_ARTICULO
                           WHERE ART_CODIGO = V_ART
                             AND ART_EMPR = V_EMPRESA)
       AND MARC_FEC_PRIMER_INGRESO IS NULL
       AND MARC_EMPR = V_EMPRESA;*/
  END IF;
  --
  OPEN PERI_CURSOR(V_DOCU_FEC_OPER);
  FETCH PERI_CURSOR
    INTO V_PERIODO;
  IF PERI_CURSOR%NOTFOUND THEN
  
    CLOSE PERI_CURSOR;
    RAISE_APPLICATION_ERROR(-20106,
                            '210-No existe registro en STK_PERIODO para fecha:' ||
                            TO_CHAR(V_DOCU_FEC_OPER, 'dd-mm-yyyy'));
  END IF;
  CLOSE PERI_CURSOR;
  --
  OPEN CONF_CURSOR(V_EMPRESA);
  FETCH CONF_CURSOR
    INTO V_CONF_PERIODO_ACT, V_CONF_PERIODO_SGTE;
  IF CONF_CURSOR%NOTFOUND THEN
    CLOSE CONF_CURSOR;
    RAISE_APPLICATION_ERROR(-20107, 'No existe registro de configuracion!');
  END IF;
  CLOSE CONF_CURSOR;

  IF V_PERIODO NOT IN (V_CONF_PERIODO_ACT, V_CONF_PERIODO_SGTE) THEN
    RAISE SALIR;
  END IF;

  IF V_OPER_DESC IN ('REVALUO', 'DEPRECIACION') THEN
    IF V_CANT <> 0 THEN
      RAISE_APPLICATION_ERROR(-20107,
                              'Cantidad debe ser CERO en operacii??n de REVALUO i?? DEPRECIACIi??N!');
    END IF;
  END IF;

  --
  -- para actualizar STK_ART_EMPR_PERI.AEP_TOT_EXIST
  IF V_TIPO_OPER = 'I' THEN
    -- si se ingresa el documento
    IF V_OPER_ENT_SAL = 'E' THEN
      -- si es una entrada de articulos
      V_TOT_EXIST := V_CANT;
    ELSIF V_OPER_ENT_SAL = 'N' THEN
      NULL;
    ELSE
      -- si es una salida de articulos
      V_TOT_EXIST := V_CANT * (-1);
    END IF;
  ELSE
    -- si se elimina el documento se invierte
    IF V_OPER_ENT_SAL = 'E' THEN
      -- si es una entrada de articulos
      V_TOT_EXIST := V_CANT * (-1);
    ELSIF V_OPER_ENT_SAL = 'N' THEN
      NULL;
    ELSE
      -- si es una salida de articulos
      V_TOT_EXIST := V_CANT;
    END IF;
  END IF;
  --
  -- establecer variables para usarlas mas adelante en la
  -- actualizacion de STK_ART_EMPR_PERI
  IF V_TIPO_OPER = 'I' THEN
    -- si se ingresa el documento
    IF V_OPER_DESC = 'COMPRA' THEN
      -- si es una compra
      V_EC_COMPRA := V_CANT;
      V_EL_COMPRA := V_IMP_NETO_LOC;
      V_EM_COMPRA := V_IMP_NETO_MON;
    ELSIF V_OPER_DESC = 'GAN_COMPRA' THEN
      -- si es una compra DE GANADO
      V_EC_COMPRA_GAN := V_CANT;
    ELSIF V_OPER_DESC = 'DEV_COM' THEN
      -- si es una devolucion de compra
      V_SC_DEV_COMPRA := V_CANT;
      V_SL_DEV_COMPRA := V_IMP_NETO_LOC;
      V_SM_DEV_COMPRA := V_IMP_NETO_MON;
    ELSIF (V_OPER_DESC = 'VENTA' OR V_OPER_DESC = 'VENTA FUTURA' OR
          V_OPER_DESC = 'ANUL_NC') THEN
      -- si es una VENTA o VENTA ANTICIPADA
      V_SC_VTA := V_CANT;
      V_SL_VTA := V_IMP_NETO_LOC;
      V_SM_VTA := V_IMP_NETO_MON;
    ELSIF V_OPER_DESC = 'GAN_VENTA' THEN
      -- si es una VENTA DE GANADO
      V_SC_VTA_GAN := V_CANT;
    ELSIF V_OPER_DESC IN ('DEV_VENTA', 'DEV_VENTA FUTURA', 'ANUL_VENTA') THEN
      -- si es una devol. de VENTA o de VENTA ANTICIPADA
      V_EC_DEV_VTA := V_CANT;
      V_EL_DEV_VTA := V_IMP_NETO_LOC;
      V_EM_DEV_VTA := V_IMP_NETO_MON;
    ELSIF V_OPER_DESC IN
          ('GASTOS_DE_SEGURIDAD', 'CONSUMO', 'SAL_AVER_VENCIDOS') THEN
      -- si es una perdida
      V_SC_PERDIDA := V_CANT;
    ELSIF V_OPER_DESC = 'DIF_MAS' THEN
      -- si es una diferencia mas
      V_C_DIF_INV := V_CANT;
    ELSIF V_OPER_DESC = 'DIF_MEN' THEN
      -- si es una diferencia menos
      V_C_DIF_INV := V_CANT * (-1);
    ELSIF V_OPER_DESC = 'SAL_PROD' THEN
      -- si es una salida por produccion
      V_SC_PROD := V_CANT;
    ELSIF V_OPER_DESC = 'ENT_PROD' THEN
      -- si es una entrada por produccion
      V_EC_PROD := V_CANT;
      V_EL_PROD := V_IMP_NETO_LOC;
      V_EM_PROD := V_IMP_NETO_MON;
    ELSIF V_OPER_DESC = 'SAL_PROD_RP' THEN
      -- si es una salida por produccion
      V_SC_PROD_RP := V_CANT;
    ELSIF V_OPER_DESC = 'ENT_PROD_RP' THEN
      -- si es una entrada por produccion
      V_EC_PROD_RP := V_CANT;
      V_EL_PROD_RP := V_IMP_NETO_LOC;
      V_EM_PROD_RP := V_IMP_NETO_MON;
    ELSIF V_OPER_DESC = 'DEV_PROD' THEN
      -- si es una devolucion por produccion
      V_EC_DEV_PROD := V_CANT;
      V_EL_DEV_PROD := V_IMP_NETO_LOC;
      V_EM_DEV_PROD := V_IMP_NETO_MON;
    ELSIF V_OPER_DESC = 'SAL_KIT' THEN
      -- si es una salida por armado de kit
      V_SC_KIT := V_CANT;
      V_SL_KIT := V_IMP_NETO_LOC;
      V_SM_KIT := V_IMP_NETO_MON;
    ELSIF V_OPER_DESC = 'ENT_KIT' THEN
      -- si es una entrada por armado de kit
      V_EC_KIT := V_CANT;
      V_EL_KIT := V_IMP_NETO_LOC;
      V_EM_KIT := V_IMP_NETO_MON;
    ELSIF V_OPER_DESC IN ('INSUMOS_DE_FABRICA', 'COMODATO', 'PERDIDA') THEN
      -- si es un consumo interno
      V_SC_CONS := V_CANT;
    ELSIF V_OPER_DESC = 'REMISION' THEN
      -- si es una REMISION
      V_C_REM := V_CANT;
    ELSIF V_OPER_DESC = 'TRAN_SAL' THEN
      -- si es una TRANSFencia salida
      V_C_TRANSF := V_CANT * (-1);
    ELSIF V_OPER_DESC = 'TRAN_ENT' THEN
      -- si es una TRANSFencia entrada
      V_C_TRANSF := V_CANT;
    ELSIF V_OPER_DESC = 'TRAN_SAL_DEV' THEN
      -- si es una TRANSFencia salida
      V_C_TRANSF_DEV := V_CANT * (-1);
    ELSIF V_OPER_DESC = 'TRAN_ENT_DEV' THEN
      -- si es una TRANSFencia entrada
      V_C_TRANSF_DEV := V_CANT;
    ELSIF V_OPER_DESC = 'SAL_ARM_DES' THEN
      -- si es una salida por armado o desarmado
      V_SC_ARM_DES := V_CANT;
      V_SL_ARM_DES := V_IMP_NETO_LOC;
      V_SM_ARM_DES := V_IMP_NETO_MON;
    ELSIF V_OPER_DESC = 'ENT_ARM_DES' THEN
      -- si es una entrada por armado o desarmado
      V_EC_ARM_DES := V_CANT;
      V_EL_ARM_DES := V_IMP_NETO_LOC;
      V_EM_ARM_DES := V_IMP_NETO_MON;
    ELSIF V_OPER_DESC = 'REC_PROD_COND' THEN
      NULL;
    ELSIF V_OPER_DESC = 'PRE_LIQ_PROD_COND' THEN
      NULL;
    ELSIF V_OPER_DESC = 'EMIS_PROD' THEN
      NULL;
    ELSIF V_OPER_DESC = 'LIQUID_MOT_VARIOS' THEN
      NULL;
    ELSIF V_OPER_DESC = 'LIQUID_VTA_VARIAS' THEN
      NULL;
    ELSIF V_OPER_DESC = 'REMISION PROVEEDOR' THEN
      NULL;
    ELSIF V_OPER_DESC = 'REMISION VENTA' THEN
      NULL;
    
    ELSIF V_OPER_DESC = 'REVALUO' THEN
      -- operacii??n de revali??o..
      V_EL_REVALUO := V_IMP_NETO_LOC;
      V_EM_REVALUO := V_IMP_NETO_MON;
    
    ELSIF V_OPER_DESC = 'DEPRECIACION' THEN
      -- operacii??n de depreciacii??n..
      V_SL_DEPREC := V_IMP_NETO_LOC;
      V_SM_DEPREC := V_IMP_NETO_MON;
    
    ELSE
      RAISE_APPLICATION_ERROR(-20107,
                              'Operacion desconocida: ' || V_OPER_DESC);
    END IF;
  ELSE
    -- si se elimina el documento se invierten los signos...
    IF V_OPER_DESC = 'COMPRA' THEN
      -- si es una compra
      V_EC_COMPRA := V_CANT * (-1);
      V_EL_COMPRA := V_IMP_NETO_LOC * (-1);
      V_EM_COMPRA := V_IMP_NETO_MON * (-1);
    ELSIF V_OPER_DESC = 'GAN_COMPRA' THEN
      -- si es una compra DE GANADO
      V_EC_COMPRA_GAN := V_CANT * (-1);
    ELSIF V_OPER_DESC = 'DEV_COM' THEN
      -- si es una devolucion de compra
      V_SC_DEV_COMPRA := V_CANT * (-1);
      V_SL_DEV_COMPRA := V_IMP_NETO_LOC * (-1);
      V_SM_DEV_COMPRA := V_IMP_NETO_MON * (-1);
    ELSIF (V_OPER_DESC = 'VENTA' OR V_OPER_DESC = 'VENTA FUTURA' OR
          V_OPER_DESC = 'ANUL_NC') THEN
      -- si es una VENTA o VENTA FUTURA
      V_SC_VTA := V_CANT * (-1);
      V_SL_VTA := V_IMP_NETO_LOC * (-1);
      V_SM_VTA := V_IMP_NETO_MON * (-1);
    ELSIF V_OPER_DESC = 'GAN_VENTA' THEN
      -- si es una VENTA DE GANADO
      V_SC_VTA_GAN := V_CANT * (-1);
    ELSIF V_OPER_DESC IN ('DEV_VENTA', 'DEV_VENTA FUTURA', 'ANUL_VENTA') THEN
      -- si es una devol. de VENTA o de VENTA ANTICIPADA
      V_EC_DEV_VTA := V_CANT * (-1);
      V_EL_DEV_VTA := V_IMP_NETO_LOC * (-1);
      V_EM_DEV_VTA := V_IMP_NETO_MON * (-1);
    ELSIF V_OPER_DESC IN
          ('GASTOS_DE_SEGURIDAD', 'CONSUMO', 'SAL_AVER_VENCIDOS') THEN
      -- si es una perdida
      V_SC_PERDIDA := V_CANT * (-1);
    ELSIF V_OPER_DESC = 'DIF_MAS' THEN
      -- si es una diferencia mas
      V_C_DIF_INV := V_CANT * (-1);
    ELSIF V_OPER_DESC = 'DIF_MEN' THEN
      -- si es una diferencia menos
      V_C_DIF_INV := V_CANT;
    ELSIF V_OPER_DESC = 'SAL_PROD' THEN
      -- si es una salida por produccion
      V_SC_PROD := V_CANT * (-1);
    ELSIF V_OPER_DESC = 'ENT_PROD' THEN
      -- si es una entrada por produccion
      V_EC_PROD := V_CANT * (-1);
      V_EL_PROD := V_IMP_NETO_LOC * (-1);
      V_EM_PROD := V_IMP_NETO_MON * (-1);
    ELSIF V_OPER_DESC = 'SAL_PROD_RP' THEN
      -- si es una salida por produccion
      V_SC_PROD_RP := V_CANT * (-1);
    ELSIF V_OPER_DESC = 'ENT_PROD_RP' THEN
      -- si es una entrada por produccion
      V_EC_PROD_RP := V_CANT * (-1);
      V_EL_PROD_RP := V_IMP_NETO_LOC * (-1);
      V_EM_PROD_RP := V_IMP_NETO_MON * (-1);
    ELSIF V_OPER_DESC = 'DEV_PROD' THEN
      -- si es una devolucion por produccion
      V_EC_DEV_PROD := V_CANT * (-1);
      V_EL_DEV_PROD := V_IMP_NETO_LOC * (-1);
      V_EM_DEV_PROD := V_IMP_NETO_MON * (-1);
    ELSIF V_OPER_DESC = 'SAL_KIT' THEN
      -- si es una salida por armado de kit
      V_SC_KIT := V_CANT * (-1);
      V_SL_KIT := V_IMP_NETO_LOC * (-1);
      V_SM_KIT := V_IMP_NETO_MON * (-1);
    ELSIF V_OPER_DESC = 'ENT_KIT' THEN
      -- si es una entrada por armado de kit
      V_EC_KIT := V_CANT * (-1);
      V_EL_KIT := V_IMP_NETO_LOC * (-1);
      V_EM_KIT := V_IMP_NETO_MON * (-1);
    ELSIF V_OPER_DESC IN ('INSUMOS_DE_FABRICA', 'COMODATO', 'PERDIDA') THEN
      -- si es un consumo interno
      V_SC_CONS := V_CANT * (-1);
    ELSIF V_OPER_DESC = 'REMISION' THEN
      -- si es una REMISION
      V_C_REM := V_CANT * (-1);
    ELSIF V_OPER_DESC = 'TRAN_SAL' THEN
      -- si es una TRANSFERENCIA salida
      V_C_TRANSF := V_CANT;
    ELSIF V_OPER_DESC = 'TRAN_ENT' THEN
      -- si es una TRANSFERENCIA entrada
      V_C_TRANSF := V_CANT * (-1);
    ELSIF V_OPER_DESC = 'TRAN_SAL_DEV' THEN
      -- si es una TRANSFERENCIA salida
      V_C_TRANSF_DEV := V_CANT;
    ELSIF V_OPER_DESC = 'TRAN_ENT_DEV' THEN
      -- si es una TRANSFERENCIA entrada
      V_C_TRANSF_DEV := V_CANT * (-1);
    ELSIF V_OPER_DESC = 'SAL_ARM_DES' THEN
      -- si es una salida por armado o desarmado
      V_SC_ARM_DES := V_CANT * (-1);
      V_SL_ARM_DES := V_IMP_NETO_LOC * (-1);
      V_SM_ARM_DES := V_IMP_NETO_MON * (-1);
    ELSIF V_OPER_DESC = 'ENT_ARM_DES' THEN
      -- si es una entrada por armado o desarmado
      V_EC_ARM_DES := V_CANT * (-1);
      V_EL_ARM_DES := V_IMP_NETO_LOC * (-1);
      V_EM_ARM_DES := V_IMP_NETO_MON * (-1);
    ELSIF V_OPER_DESC = 'REC_PROD_COND' THEN
      NULL;
    ELSIF V_OPER_DESC = 'PRE_LIQ_PROD_COND' THEN
      NULL;
    ELSIF V_OPER_DESC = 'EMIS_PROD' THEN
      NULL;
    ELSIF V_OPER_DESC = 'LIQUID_MOT_VARIOS' THEN
      NULL;
    ELSIF V_OPER_DESC = 'LIQUID_VTA_VARIAS' THEN
      NULL;
    ELSIF V_OPER_DESC = 'REMISION VENTA' THEN
      NULL;
    
    ELSIF V_OPER_DESC = 'REVALUO' THEN
      -- operacii??n de revali??o..
      V_EL_REVALUO := -1 * V_IMP_NETO_LOC;
      V_EM_REVALUO := -1 * V_IMP_NETO_MON;
    
    ELSIF V_OPER_DESC = 'DEPRECIACION' THEN
      -- operacii??n de depreciacii??n..
      V_SL_DEPREC := -1 * V_IMP_NETO_LOC;
      V_SM_DEPREC := -1 * V_IMP_NETO_MON;
    
    ELSE
      RAISE_APPLICATION_ERROR(-20108,
                              'Operacion desconocida: ' || V_OPER_DESC);
    END IF;
  END IF;

  -- actualizar STK_ART_EMPR_PERI
  V_IND_ACTUALIZADO := 'N';
  FOR RAEP IN AEP_CURSOR(V_PERIODO, V_EMPRESA, V_ART) LOOP
    V_IND_ACTUALIZADO := 'S';
    IF RAEP.AEP_PERIODO = V_PERIODO THEN
      /* calcular costo promedio del periodo */
      IF RAEP.AEP_EXIST_INI_OPER > 0 THEN
        V_TOT_CANT    := RAEP.AEP_EXIST_INI_OPER;
        V_TOT_IMP_MON := RAEP.AEP_COSTO_INI_MON_OPER;
        V_TOT_IMP_LOC := RAEP.AEP_COSTO_INI_LOC_OPER;
      ELSE
        V_TOT_CANT    := 0;
        V_TOT_IMP_MON := 0;
        V_TOT_IMP_LOC := 0;
      END IF;
      V_TOT_CANT := V_TOT_CANT + RAEP.AEP_EC_COMPRA_OPER + V_EC_COMPRA +
                    RAEP.AEP_EC_PROD_OPER + V_EC_PROD +
                    RAEP.AEP_EC_PROD_RP_OPER + V_EC_PROD_RP +
                    RAEP.AEP_EC_DEV_PROD_OPER + V_EC_DEV_PROD +
                    RAEP.AEP_EC_KIT_OPER + V_EC_KIT +
                    RAEP.AEP_EC_ARM_DES_OPER + V_EC_ARM_DES -
                    RAEP.AEP_SC_DEV_COMPRA_OPER - V_SC_DEV_COMPRA;
      --+ V_EC_DEV_VTA
      --+ RAEP.AEP_EC_DEV_VTA;
    
      V_TOT_IMP_MON := V_TOT_IMP_MON + RAEP.AEP_EM_COMPRA_OPER +
                       V_EM_COMPRA + RAEP.AEP_EM_PROD_OPER + V_EM_PROD +
                       RAEP.AEP_EM_PROD_RP_OPER + V_EM_PROD_RP +
                       RAEP.AEP_EM_DEV_PROD_OPER + V_EM_DEV_PROD +
                       RAEP.AEP_EM_KIT_OPER + V_EM_KIT +
                       RAEP.AEP_EM_ARM_DES_OPER + V_EM_ARM_DES +
                       RAEP.AEP_SM_DEV_COMPRA_OPER - V_SM_DEV_COMPRA
                      
                       + RAEP.AEP_EM_REVALUO + V_EM_REVALUO -
                       RAEP.AEP_SM_DEPREC - V_SM_DEPREC
      
       ;
      --+ RAEP.AEP_EM_DEV_VTA
      --+ V_EM_DEV_VTA;
    
      V_TOT_IMP_LOC := V_TOT_IMP_LOC + RAEP.AEP_EL_COMPRA_OPER +
                       V_EL_COMPRA + RAEP.AEP_EL_PROD_OPER + V_EL_PROD +
                       RAEP.AEP_EL_PROD_RP_OPER + V_EL_PROD_RP +
                       RAEP.AEP_EL_DEV_PROD_OPER + V_EL_DEV_PROD +
                       RAEP.AEP_EL_KIT_OPER + V_EL_KIT +
                       RAEP.AEP_EL_ARM_DES_OPER + V_EL_ARM_DES -
                       RAEP.AEP_SL_DEV_COMPRA_OPER - V_SL_DEV_COMPRA
                      
                       + RAEP.AEP_EL_REVALUO + V_EL_REVALUO -
                       RAEP.AEP_SL_DEPREC - V_SM_DEPREC
      
       ;
      --+ RAEP.AEP_EL_DEV_VTA
      --+ V_EL_DEV_VTA;
      IF V_TOT_CANT <= 0 OR V_TOT_IMP_MON <= 0 THEN
        -- para que calcule el costo de los valores de compra y dev.de venta
        -- que es igual al ultimo costo
        V_TOT_CANT    := RAEP.AEP_EC_COMPRA_OPER + V_EC_COMPRA; -- + V_EC_DEV_VTA;
        V_TOT_IMP_MON := RAEP.AEP_EM_COMPRA_OPER + V_EM_COMPRA; -- + V_EM_DEV_VTA;
        V_TOT_IMP_LOC := RAEP.AEP_EL_COMPRA_OPER + V_EL_COMPRA; -- + V_EL_DEV_VTA;
      END IF;
      IF V_TOT_CANT <= 0 OR V_TOT_IMP_MON <= 0 THEN
        -- para que no se pierda el costo MON (US$)
        V_COSTO_PROM_MON := RAEP.AEP_COSTO_PROM_MON_OPER;
      ELSE
        V_COSTO_PROM_MON := V_TOT_IMP_MON / V_TOT_CANT;
      END IF;
    
      IF V_TOT_CANT <= 0 OR V_TOT_IMP_LOC <= 0 THEN
        -- para que no se pierda el costo LOC
        V_COSTO_PROM_LOC := RAEP.AEP_COSTO_PROM_LOC_OPER;
      ELSE
        V_COSTO_PROM_LOC := V_TOT_IMP_LOC / V_TOT_CANT;
      END IF;
    
      /* inicializar variables para el siguiente periodo */
      V_EXIST_INI          := RAEP.AEP_TOT_EXIST_OPER + V_TOT_EXIST;
      V_COSTO_PROM_INI_MON := V_COSTO_PROM_MON;
      V_COSTO_PROM_INI_LOC := V_COSTO_PROM_LOC;
      V_COSTO_INI_MON      := 0;
      V_COSTO_INI_LOC      := 0;
      /* modificado por luis fedriani el 21/11/1997*/
      /* para que actualize el costo cuando la existencia es negativa*/
      /* anteriormente tenia mayor que cero*/
      IF V_EXIST_INI <> 0 THEN
        V_COSTO_INI_MON := V_COSTO_PROM_MON * V_EXIST_INI;
        V_COSTO_INI_LOC := V_COSTO_PROM_LOC * V_EXIST_INI;
      END IF;
    
      UPDATE STK_ART_EMPR_PERI
         SET AEP_TOT_EXIST_OPER      = NVL(AEP_TOT_EXIST + V_TOT_EXIST, 0),
             AEP_COSTO_PROM_LOC_OPER = NVL(V_COSTO_PROM_LOC, 0),
             AEP_COSTO_PROM_MON_OPER = NVL(V_COSTO_PROM_MON, 0),
             AEP_EC_COMPRA_OPER      = NVL(AEP_EC_COMPRA + V_EC_COMPRA, 0),
             AEP_EL_COMPRA_OPER      = NVL(AEP_EL_COMPRA + V_EL_COMPRA, 0),
             AEP_EM_COMPRA_OPER      = NVL(AEP_EM_COMPRA + V_EM_COMPRA, 0),
             AEP_SC_DEV_COMPRA_OPER  = NVL(AEP_SC_DEV_COMPRA +
                                           V_SC_DEV_COMPRA,
                                           0),
             AEP_SL_DEV_COMPRA_OPER  = NVL(AEP_SL_DEV_COMPRA +
                                           V_SL_DEV_COMPRA,
                                           0),
             AEP_SM_DEV_COMPRA_OPER  = NVL(AEP_SM_DEV_COMPRA +
                                           V_SM_DEV_COMPRA,
                                           0),
             AEP_SC_VTA_OPER         = NVL(AEP_SC_VTA + V_SC_VTA, 0),
             AEP_SL_VTA_OPER         = NVL(AEP_SL_VTA + V_SL_VTA, 0),
             AEP_SM_VTA_OPER         = NVL(AEP_SM_VTA + V_SM_VTA, 0),
             AEP_EC_DEV_VTA_OPER     = NVL(AEP_EC_DEV_VTA + V_EC_DEV_VTA, 0),
             AEP_EL_DEV_VTA_OPER     = NVL(AEP_EL_DEV_VTA + V_EL_DEV_VTA, 0),
             AEP_EM_DEV_VTA_OPER     = NVL(AEP_EM_DEV_VTA + V_EM_DEV_VTA, 0),
             AEP_EC_PROD_OPER        = NVL(AEP_EC_PROD + V_EC_PROD, 0),
             AEP_EL_PROD_OPER        = NVL(AEP_EL_PROD + V_EL_PROD, 0),
             AEP_EM_PROD_OPER        = NVL(AEP_EM_PROD + V_EM_PROD, 0),
             AEP_EC_PROD_RP_OPER     = NVL(AEP_EC_PROD_RP + V_EC_PROD_RP, 0),
             AEP_EL_PROD_RP_OPER     = NVL(AEP_EL_PROD_RP + V_EL_PROD_RP, 0),
             AEP_EM_PROD_RP_OPER     = NVL(AEP_EM_PROD_RP + V_EM_PROD_RP, 0),
             AEP_EC_DEV_PROD_OPER    = NVL(AEP_EC_DEV_PROD + V_EC_DEV_PROD,
                                           0),
             AEP_EL_DEV_PROD_OPER    = NVL(AEP_EL_DEV_PROD + V_EL_DEV_PROD,
                                           0),
             AEP_EM_DEV_PROD_OPER    = NVL(AEP_EM_DEV_PROD + V_EM_DEV_PROD,
                                           0),
             AEP_SC_PROD_OPER        = NVL(AEP_SC_PROD + V_SC_PROD, 0),
             AEP_SC_PROD_RP_OPER     = NVL(AEP_SC_PROD_RP + V_SC_PROD_RP, 0),
             AEP_EC_KIT_OPER         = NVL(AEP_EC_KIT + V_EC_KIT, 0),
             AEP_EL_KIT_OPER         = NVL(AEP_EL_KIT + V_EL_KIT, 0),
             AEP_EM_KIT_OPER         = NVL(AEP_EM_KIT + V_EM_KIT, 0),
             AEP_SC_KIT_OPER         = NVL(AEP_SC_KIT + V_SC_KIT, 0),
             AEP_SL_KIT_OPER         = NVL(AEP_SL_KIT + V_SL_KIT, 0),
             AEP_SM_KIT_OPER         = NVL(AEP_SM_KIT + V_SM_KIT, 0),
             AEP_SC_PERDIDA_OPER     = NVL(AEP_SC_PERDIDA + V_SC_PERDIDA, 0),
             AEP_SC_CONS_OPER        = NVL(AEP_SC_CONS + V_SC_CONS, 0),
             AEP_C_DIF_INV_OPER      = NVL(AEP_C_DIF_INV + V_C_DIF_INV, 0),
             AEP_C_TRANSF_OPER       = NVL(AEP_C_TRANSF + V_C_TRANSF, 0),
             AEP_C_TRANSF_DEV_OPER   = NVL(AEP_C_TRANSF_DEV + V_C_TRANSF_DEV,
                                           0),
             AEP_C_REM_OPER          = NVL(AEP_C_REM + V_C_REM, 0),
             ------------------------------------------------------------------------              
             AEP_EC_ARM_DES_OPER = NVL(AEP_EC_ARM_DES + V_EC_ARM_DES, 0),
             AEP_EL_ARM_DES_OPER = NVL(AEP_EL_ARM_DES + V_EL_ARM_DES, 0),
             AEP_EM_ARM_DES_OPER = NVL(AEP_EM_ARM_DES + V_EM_ARM_DES, 0),
             AEP_SC_ARM_DES_OPER = NVL(AEP_SC_ARM_DES + V_SC_ARM_DES, 0),
             AEP_SL_ARM_DES_OPER = NVL(AEP_SL_ARM_DES + V_SL_ARM_DES, 0),
             AEP_SM_ARM_DES_OPER = NVL(AEP_SM_ARM_DES + V_SM_ARM_DES, 0),
             ------------------------------------------------------------------------              
             AEP_EL_REVALUO_OPER = NVL(AEP_EL_REVALUO_OPER, 0) +
                                   NVL(V_EL_REVALUO, 0),
             AEP_EM_REVALUO_OPER = NVL(AEP_EM_REVALUO_OPER, 0) +
                                   NVL(V_EM_REVALUO, 0),
             
             AEP_SL_DEPREC_OPER = NVL(AEP_SL_DEPREC_OPER, 0) +
                                  NVL(V_SL_DEPREC, 0),
             AEP_SM_DEPREC_OPER = NVL(AEP_SM_DEPREC_OPER, 0) +
                                  NVL(V_SM_DEPREC, 0)
      ------------------------------------------------------------------------ 
      
       WHERE CURRENT OF AEP_CURSOR;
    ELSE
      /* calcular costo promedio del periodo */
      IF V_EXIST_INI > 0 THEN
        V_TOT_CANT := V_EXIST_INI;
      ELSE
        V_TOT_CANT := 0;
      END IF;
    
      V_TOT_CANT := V_TOT_CANT + RAEP.AEP_EC_COMPRA_OPER +
                    RAEP.AEP_EC_PROD_OPER + RAEP.AEP_EC_PROD_RP_OPER +
                    RAEP.AEP_EC_DEV_PROD_OPER + RAEP.AEP_EC_KIT_OPER +
                    RAEP.AEP_EC_ARM_DES_OPER - RAEP.AEP_SC_DEV_COMPRA_OPER;
    
      -- + RAEP.AEP_EC_DEV_VTA;       
      V_TOT_IMP_MON := V_COSTO_INI_MON + RAEP.AEP_EM_COMPRA_OPER +
                       RAEP.AEP_EM_PROD_OPER + RAEP.AEP_EM_PROD_RP_OPER +
                       RAEP.AEP_EM_DEV_PROD_OPER + RAEP.AEP_EM_KIT_OPER +
                       RAEP.AEP_EM_ARM_DES_OPER -
                       RAEP.AEP_SM_DEV_COMPRA_OPER
                      
                       + RAEP.AEP_EM_REVALUO_OPER - RAEP.AEP_SM_DEPREC_OPER;
    
      -- + RAEP.AEP_EM_DEV_VTA;
    
      V_TOT_IMP_LOC := V_COSTO_INI_LOC + RAEP.AEP_EL_COMPRA_OPER +
                       RAEP.AEP_EL_PROD_OPER + RAEP.AEP_EL_PROD_RP_OPER +
                       RAEP.AEP_EL_DEV_PROD_OPER + RAEP.AEP_EL_KIT_OPER +
                       RAEP.AEP_EL_ARM_DES_OPER -
                       RAEP.AEP_SL_DEV_COMPRA_OPER
                      
                       + RAEP.AEP_EL_REVALUO_OPER - RAEP.AEP_SL_DEPREC_OPER;
    
      -- + RAEP.AEP_EL_DEV_VTA;
    
      IF V_TOT_CANT <= 0 OR V_TOT_IMP_MON <= 0 THEN
      
        -- para que calcule el costo de los valores de compra y dev.vta.
        -- que es igual al ultimo costo
      
        V_TOT_CANT    := RAEP.AEP_EC_COMPRA_OPER; -- + RAEP.AEP_EC_DEV_VTA;
        V_TOT_IMP_MON := RAEP.AEP_EM_COMPRA_OPER; -- + RAEP.AEP_EM_DEV_VTA;
        V_TOT_IMP_LOC := RAEP.AEP_EL_COMPRA_OPER; -- + RAEP.AEP_EL_DEV_VTA;
      
      END IF;
    
      IF V_TOT_CANT <= 0 OR V_TOT_IMP_MON <= 0 THEN
        -- para que no se pierda el costo
        V_COSTO_PROM_MON := V_COSTO_PROM_INI_MON;
      ELSE
        V_COSTO_PROM_MON := V_TOT_IMP_MON / V_TOT_CANT;
      END IF;
    
      IF V_TOT_CANT <= 0 OR V_TOT_IMP_LOC <= 0 THEN
        -- para que no se pierda el costo
        V_COSTO_PROM_LOC := V_COSTO_PROM_INI_LOC;
      ELSE
        V_COSTO_PROM_LOC := V_TOT_IMP_LOC / V_TOT_CANT;
      END IF;
    
      --verificacion provisoria de errores de calculo
      --IF V_COSTO_PROM_INI_LOC <> V_COSTO_PROM_LOC THEN
      --  RAISE_APPLICATION_ERROR(-20110,'Costo promedio incorrecto, articulo: '||
      -- TO_CHAR(V_ART)||' para la empresa: '||TO_CHAR(V_EMPRESA)||
      -- ' en el periodo: '||TO_CHAR(V_PERIODO+1));
      --END IF;
      UPDATE STK_ART_EMPR_PERI
         SET AEP_TOT_EXIST_OPER          = NVL(AEP_TOT_EXIST + V_TOT_EXIST,
                                               0),
             AEP_EXIST_INI_OPER          = NVL(V_EXIST_INI, 0),
             AEP_COSTO_INI_LOC_OPER      = NVL(V_COSTO_INI_LOC, 0),
             AEP_COSTO_PROM_INI_LOC_OPER = NVL(V_COSTO_PROM_INI_LOC, 0),
             AEP_COSTO_PROM_LOC_OPER     = NVL(V_COSTO_PROM_LOC, 0),
             AEP_COSTO_INI_MON_OPER      = NVL(V_COSTO_INI_MON, 0),
             AEP_COSTO_PROM_INI_MON_OPER = NVL(V_COSTO_PROM_INI_MON, 0),
             AEP_COSTO_PROM_MON_OPER     = NVL(V_COSTO_PROM_MON, 0)
      
       WHERE CURRENT OF AEP_CURSOR;
    END IF;
  END LOOP;
  IF V_IND_ACTUALIZADO = 'N' THEN
    -- si no se actualizo ningun registro
    RAISE_APPLICATION_ERROR(-20109,
                            'No existe articulo: ' || TO_CHAR(V_ART) ||
                            ' para la empresa: ' || TO_CHAR(V_EMPRESA) ||
                            ' en el periodo: ' || TO_CHAR(V_PERIODO));
  END IF;

  /*
  EXCEPTION
    --WHEN SALIR THEN
    WHEN OTHERS THEN
  
      RAISE_APPLICATION_ERROR(-20108, SQLERRM || '. ' ||
                              'Error al actualizar la operacion: ' || V_OPER_DESC ||
                              '. Empresa '|| V_EMPRESA
                               );
                               */
  /*
  RAISE_APPLICATION_ERROR(-20108,
                          'error al actualizar la operacion: ' ||
                          V_OPER_DESC);
  */
  --  NULL;

EXCEPTION
  WHEN SALIR THEN
    NULL;
  
  WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20108,
                            'error en el triggers STK_ACT_ART_EMP_OPER: ' ||
                            SQLCODE || '-' || SQLERRM);
  
END;
/
