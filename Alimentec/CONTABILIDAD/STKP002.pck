CREATE OR REPLACE PACKAGE STKP002 IS

  PROCEDURE PP_ALTER_TRIGGERS(i_OPERACION in VARCHAR2);

  PROCEDURE PP_VERIF_ART_DEP_EX_INI(I_EMPRESA IN NUMBER,
                                    I_ART     IN NUMBER DEFAULT NULL);

  PROCEDURE PP_VERIF_ART_DEP_EX_INI_OPER(I_EMPRESA IN NUMBER,
                                         I_ART     IN NUMBER DEFAULT NULL);

  PROCEDURE PP_VERIF_ART_DEP_EX_ACT(I_EMPRESA IN NUMBER,
                                    I_ART     IN NUMBER DEFAULT NULL);

  PROCEDURE PP_VERIF_ART_DEP_EX_ACT_OPER(I_EMPRESA IN NUMBER,
                                         I_ART     IN NUMBER DEFAULT NULL);

  PROCEDURE PP_COMPARAR_CON_PERI_ANT(I_EMPRESA IN NUMBER,
                                     I_PERIODO IN NUMBER,
                                     I_ART     IN NUMBER DEFAULT NULL);

  PROCEDURE PP_COMPARAR_CON_PERI_ANT_OPER(I_EMPRESA IN NUMBER,
                                          I_PERIODO IN NUMBER,
                                          I_ART     IN NUMBER DEFAULT NULL);

  PROCEDURE PP_VERIF_AEP(I_EMPRESA IN NUMBER,
                         I_FECINI  IN DATE,
                         I_FECFIN  IN DATE,
                         I_PERIODO IN NUMBER,
                         I_ART     IN NUMBER DEFAULT NULL);

  PROCEDURE PP_VERIF_AEP_OPER(I_EMPRESA IN NUMBER,
                              I_FECINI  IN DATE,
                              I_FECFIN  IN DATE,
                              I_PERIODO IN NUMBER,
                              I_ART     IN NUMBER DEFAULT NULL);

  PROCEDURE PP_VERIF_EST(I_EMPRESA IN NUMBER, I_ART IN NUMBER DEFAULT NULL);

  PROCEDURE PP_ESTABLECER_MES(V_VTA_01           IN OUT NUMBER,
                              V_VTA_02           IN OUT NUMBER,
                              V_VTA_03           IN OUT NUMBER,
                              V_VTA_04           IN OUT NUMBER,
                              V_VTA_05           IN OUT NUMBER,
                              V_VTA_06           IN OUT NUMBER,
                              V_TAL_01           IN OUT NUMBER,
                              V_TAL_02           IN OUT NUMBER,
                              V_TAL_03           IN OUT NUMBER,
                              V_TAL_04           IN OUT NUMBER,
                              V_TAL_05           IN OUT NUMBER,
                              V_TAL_06           IN OUT NUMBER,
                              V_CANT_VTA_ACT     IN NUMBER,
                              V_CANT_TAL_ACT     IN NUMBER,
                              V_PERI_ACT_FEC_INI in date);

  PROCEDURE PP_ESTABLECER_CANT_EST(V_EST_MES_VTA_01   IN OUT NUMBER,
                                   V_EST_MES_VTA_02   IN OUT NUMBER,
                                   V_EST_MES_VTA_03   IN OUT NUMBER,
                                   V_EST_MES_VTA_04   IN OUT NUMBER,
                                   V_EST_MES_VTA_05   IN OUT NUMBER,
                                   V_EST_MES_VTA_06   IN OUT NUMBER,
                                   V_EST_MES_TAL_01   IN OUT NUMBER,
                                   V_EST_MES_TAL_02   IN OUT NUMBER,
                                   V_EST_MES_TAL_03   IN OUT NUMBER,
                                   V_EST_MES_TAL_04   IN OUT NUMBER,
                                   V_EST_MES_TAL_05   IN OUT NUMBER,
                                   V_EST_MES_TAL_06   IN OUT NUMBER,
                                   V_MES_VTA_ACT      OUT NUMBER,
                                   V_MES_TAL_ACT      OUT NUMBER,
                                   V_CANTIDAD_VTA     IN NUMBER,
                                   V_CANTIDAD_TAL     IN NUMBER,
                                   V_PERI_ACT_FEC_INI IN DATE);

  PROCEDURE PP_COMPARAR_ARDE_AEP(I_EMPRESA IN NUMBER,
                                 I_ART     IN NUMBER DEFAULT NULL);

  PROCEDURE PP_COMPARAR_ARDE_AEP_OPER(I_EMPRESA IN NUMBER,
                                      I_ART     IN NUMBER DEFAULT NULL);

  PROCEDURE PP_MESSAGE(I_MESSAGE IN VARCHAR2, I_EMPRESA IN NUMBER);

  PROCEDURE PP_RECALCULAR_ARDE(I_EMPRESA IN NUMBER,
                               I_PERIODO IN NUMBER,
                               I_ART     IN NUMBER DEFAULT NULL);

  PROCEDURE PP_VERIF_AEP_TAGRO(I_EMPRESA IN NUMBER,
                               I_FECINI  IN DATE,
                               I_FECFIN  IN DATE,
                               I_PERIODO IN NUMBER,
                               I_ART     IN NUMBER DEFAULT NULL);

  PROCEDURE PP_VALIDAR_PERIODO(I_PERIODO IN NUMBER, I_EMPRESA IN NUMBER);

  PROCEDURE PP_VERIFICAR_SISTEMA(I_EMPRESA     IN NUMBER,
                                 I_PERI_CODIGO IN NUMBER,
                                 I_ESTADISTICA IN VARCHAR2,
                                 I_ART         IN NUMBER DEFAULT NULL);

 
END;
/
CREATE OR REPLACE PACKAGE BODY STKP002 IS

  PROCEDURE PP_ALTER_TRIGGERS(I_OPERACION IN VARCHAR2) IS
    CURSOR TRIG_CUR IS
    /*SELECT OBJECT_NAME TRIGGER_NAME, OWNER
            FROM ALL_OBJECTS
           WHERE UPPER(OBJECT_TYPE) = 'TRIGGER'
                --  AND UPPER(SUBSTR(OBJECT_NAME, NVL(LENGTH(OBJECT_NAME), 0) - 5, 6)) =
                --      'AF_IDU'
                --  AND UPPER(SUBSTR(OBJECT_NAME, 1, 3)) = 'STK'
             AND OWNER = 'ADCS';*/
    
      SELECT TRIGGER_NAME FROM USER_TRIGGERS;
  
    V_RESULTADO INTEGER;
  
  BEGIN
    FOR RTRIG IN TRIG_CUR LOOP
      BEGIN
        V_RESULTADO := GEN_DDL('ALTER TRIGGER ' || RTRIG.TRIGGER_NAME || ' ' ||
                               I_OPERACION);
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
    END LOOP;
  END;

  --===================================================================================

  PROCEDURE PP_VERIF_ART_DEP_EX_INI(I_EMPRESA IN NUMBER,
                                    I_ART     IN NUMBER DEFAULT NULL) IS
  
    CURSOR MOV_SC_ACUM(I_PERI_ACT_FEC_INI IN DATE) IS
    
      SELECT CANT_CUR.*
        FROM (SELECT DOCU_EMPR EMPR,
                     DOCU_SUC_ORIG SUC,
                     DOCU_DEP_ORIG DEP,
                     DETA_ART ART,
                     SUM(DECODE(OPER_ENT_SAL, 'E', DETA_CANT, 0)) CANT_ENT,
                     SUM(DECODE(OPER_ENT_SAL, 'S', DETA_CANT, 0)) CANT_SAL
                FROM STK_OPERACION O,
                     (SELECT *
                        FROM STK_DOCUMENTO D, STK_DOCUMENTO_DET DE
                       WHERE DOCU_CLAVE = DETA_CLAVE_DOC
                         AND DOCU_EMPR = DETA_EMPR
                      --   AND (DETA_ART = I_ART OR I_ART IS NULL) --*
                      ) DT,
                     STK_ARTICULO A,
                     (SELECT *
                        FROM STK_ARTICULO_DEPOSITO B
                       WHERE B.ARDE_EMPR = I_EMPRESA
                      --   AND (ARDE_ART = I_ART OR I_ART IS NULL) --*
                      ) AD
               WHERE OPER_CODIGO = DOCU_CODIGO_OPER
                 AND OPER_EMPR = DOCU_EMPR
                    
                 AND DT.DOCU_EMPR = AD.ARDE_EMPR(+)
                 AND DT.DOCU_SUC_ORIG = AD.ARDE_SUC(+)
                 AND DT.DOCU_DEP_ORIG = AD.ARDE_DEP(+)
                 AND DT.DETA_ART = AD.ARDE_ART(+)
                 AND AD.ARDE_ART IS NULL
                    
                 AND ART_CODIGO = DETA_ART
                 AND ART_EMPR = DETA_EMPR
                    
                 AND ART_TIPO <> 4
                    
                 AND DOCU_EMPR = I_EMPRESA
                    
                 AND DOCU_FEC_EMIS < I_PERI_ACT_FEC_INI
                    
              --   AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
              
               GROUP BY DOCU_EMPR, DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART) CANT_CUR
       ORDER BY EMPR, SUC, DEP, ART;
  
    CURSOR ACUM_SC_MOV(I_PERI_ACT_FEC_INI IN DATE) IS
      SELECT ARDE_EMPR, ARDE_SUC, ARDE_DEP, ARDE_ART, ARDE_CANT_INI
        FROM STK_ARTICULO,
             STK_ARTICULO_DEPOSITO AD,
             (SELECT *
                FROM STK_DOCUMENTO D, STK_DOCUMENTO_DET DE
               WHERE DOCU_CLAVE = DETA_CLAVE_DOC
                 AND DOCU_EMPR = DETA_EMPR
                 AND DOCU_EMPR = I_EMPRESA
                 AND DOCU_FEC_EMIS < I_PERI_ACT_FEC_INI
              --   AND (DETA_ART = I_ART OR I_ART IS NULL) --*
              ) DT
       WHERE ART_CODIGO = ARDE_ART
         AND ART_EMPR = ARDE_EMPR
         AND ARDE_EMPR = I_EMPRESA
            
         AND AD.ARDE_EMPR = DT.DOCU_EMPR(+)
         AND AD.ARDE_SUC = DT.DOCU_SUC_ORIG(+)
         AND AD.ARDE_DEP = DT.DOCU_DEP_ORIG(+)
         AND AD.ARDE_ART = DT.DETA_ART(+)
            
         AND DT.DOCU_CLAVE IS NULL
         AND ART_TIPO <> 4
         AND ARDE_CANT_INI <> 0
        -- AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
      
       ORDER BY ARDE_EMPR, ARDE_SUC, ARDE_DEP, ARDE_ART;
  
    V_MESSAGE VARCHAR2(200);
  
    V_PERI_ACT_FEC_INI DATE;
  BEGIN
  
    SELECT PERI_FEC_INI
      INTO V_PERI_ACT_FEC_INI
      FROM STK_PERIODO, STK_CONFIGURACION
     WHERE PERI_CODIGO = CONF_PERIODO_ACT
       AND PERI_EMPR = CONF_EMPR
       AND CONF_EMPR = I_EMPRESA;
  
    --MOVIMIENTOS SIN COINCIDIR CON ACUMULADOS
    FOR V IN MOV_SC_ACUM(I_PERI_ACT_FEC_INI => V_PERI_ACT_FEC_INI) LOOP
    
      V_MESSAGE := ' MOV_SC_ACUM, Empr=' || TO_CHAR(V.EMPR) || ', Suc=' ||
                   TO_CHAR(V.SUC) || ', Dep=' || TO_CHAR(V.DEP) || ', Art=' ||
                   TO_CHAR(V.ART);
    
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
    
      V_MESSAGE := 'Error 201: Falta reg. en STK_ARTICULO_DEPOSITO' ||
                   ' para Empr=' || TO_CHAR(V.EMPR) || ', Suc=' ||
                   TO_CHAR(V.SUC) || ', Dep=' || TO_CHAR(V.DEP) || ', Art=' ||
                   TO_CHAR(V.ART) || '. Desea corregir?';
    
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
    
      INSERT INTO STK_ARTICULO_DEPOSITO
        (ARDE_EMPR,
         ARDE_SUC,
         ARDE_DEP,
         ARDE_ART,
         ARDE_CANT_INI,
         ARDE_CANT_ACT,
         ARDE_CANT_ENT,
         ARDE_CANT_SAL,
         ARDE_CANT_INV,
         ARDE_FEC_INV,
         ARDE_UBIC)
      VALUES
        (V.EMPR,
         V.SUC,
         V.DEP,
         V.ART,
         V.CANT_ENT - V.CANT_SAL,
         0,
         0,
         0,
         0,
         NULL,
         NULL);
    
    END LOOP;
  
    --ACUMULADOS SIN COINCIDIR CON MOVIMIENTOS
    FOR V IN ACUM_SC_MOV(I_PERI_ACT_FEC_INI => V_PERI_ACT_FEC_INI) LOOP
    
      V_MESSAGE := 'ACUM_SC_MOV, Empr=' || TO_CHAR(V.ARDE_EMPR) || ', Suc=' ||
                   TO_CHAR(V.ARDE_SUC) || ', Dep=' || TO_CHAR(V.ARDE_DEP) ||
                   ', Art=' || TO_CHAR(V.ARDE_ART);
    
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
    
      V_MESSAGE := 'Error 202: Cant.inicial incorrecto ' ||
                   ' en STK_ARTICULO_DEPOSITO para ' || 'Empr=' ||
                   TO_CHAR(V.ARDE_EMPR) || ', Suc=' || TO_CHAR(V.ARDE_SUC) ||
                   ', Dep=' || TO_CHAR(V.ARDE_DEP) || ', Art=' ||
                   TO_CHAR(V.ARDE_ART) || '. Desea corregir?';
    
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
    
      UPDATE STK_ARTICULO_DEPOSITO
         SET ARDE_CANT_INI = 0
       WHERE ARDE_EMPR = V.ARDE_EMPR
         AND ARDE_SUC = V.ARDE_SUC
         AND ARDE_DEP = V.ARDE_DEP
         AND ARDE_ART = V.ARDE_ART;
    
    END LOOP;
  
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001,
                              'Error en  PP_VERIF_ART_DEP_EX_INI - ' ||
                              SQLCODE || ' -ERROR- ' || SQLERRM);
  END PP_VERIF_ART_DEP_EX_INI;

  PROCEDURE PP_VERIF_ART_DEP_EX_INI_OPER(I_EMPRESA IN NUMBER,
                                         I_ART     IN NUMBER DEFAULT NULL) IS
  
    CURSOR MOV_SC_ACUM(I_PERI_ACT_FEC_INI IN DATE) IS
      SELECT CANT_CUR.*
        FROM (SELECT DOCU_EMPR EMPR,
                     DOCU_SUC_ORIG SUC,
                     DOCU_DEP_ORIG DEP,
                     DETA_ART ART,
                     SUM(DECODE(OPER_ENT_SAL, 'E', DETA_CANT, 0)) CANT_ENT,
                     SUM(DECODE(OPER_ENT_SAL, 'S', DETA_CANT, 0)) CANT_SAL
                FROM STK_OPERACION O,
                     (SELECT *
                        FROM STK_DOCUMENTO D, STK_DOCUMENTO_DET DE
                       WHERE DOCU_CLAVE = DETA_CLAVE_DOC
                         AND DOCU_EMPR = DETA_EMPR
                     --    AND (DETA_ART = I_ART OR I_ART IS NULL) --*
                      ) DT,
                     STK_ARTICULO A,
                     (SELECT *
                        FROM STK_ARTICULO_DEPOSITO B
                       WHERE B.ARDE_EMPR = I_EMPRESA
                    --     AND (ARDE_ART = I_ART OR I_ART IS NULL) --*
                      
                      ) AD
               WHERE OPER_EMPR = DOCU_EMPR
                 AND OPER_CODIGO = DOCU_CODIGO_OPER
                    
                 AND DT.DOCU_EMPR = AD.ARDE_EMPR(+)
                 AND DT.DOCU_SUC_ORIG = AD.ARDE_SUC(+)
                 AND DT.DOCU_DEP_ORIG = AD.ARDE_DEP(+)
                 AND DT.DETA_ART = AD.ARDE_ART(+)
                 AND AD.ARDE_ART IS NULL
                    
                 AND ART_CODIGO = DETA_ART
                 AND ART_EMPR = DETA_EMPR
             --    AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
                    
                 AND ART_TIPO <> 4
                 AND DOCU_EMPR = I_EMPRESA
                 AND DOCU_FEC_OPER < I_PERI_ACT_FEC_INI
              
               GROUP BY DOCU_EMPR, DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART) CANT_CUR
       ORDER BY EMPR, SUC, DEP, ART;
  
    CURSOR ACUM_SC_MOV(I_PERI_ACT_FEC_INI IN DATE) IS
    
      SELECT ARDE_EMPR, ARDE_SUC, ARDE_DEP, ARDE_ART, ARDE_CANT_INI
        FROM STK_ARTICULO,
             STK_ARTICULO_DEPOSITO AD,
             (SELECT *
                FROM STK_DOCUMENTO D, STK_DOCUMENTO_DET DE
               WHERE DOCU_CLAVE = DETA_CLAVE_DOC
                 AND DOCU_EMPR = DETA_EMPR
                 AND DOCU_EMPR = I_EMPRESA
                 AND DOCU_FEC_OPER < I_PERI_ACT_FEC_INI
              --   AND (DETA_ART = I_ART OR I_ART IS NULL) --*
              ) DT
       WHERE AD.ARDE_EMPR = DT.DOCU_EMPR(+)
         AND AD.ARDE_SUC = DT.DOCU_SUC_ORIG(+)
         AND AD.ARDE_DEP = DT.DOCU_DEP_ORIG(+)
         AND AD.ARDE_ART = DT.DETA_ART(+)
         AND DT.DOCU_CLAVE IS NULL
            
         AND ART_CODIGO = ARDE_ART
         AND ART_EMPR = ARDE_EMPR
        -- AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
            
         AND ARDE_EMPR = I_EMPRESA
         AND ART_TIPO <> 4
         AND ARDE_CANT_INI <> 0
      
       ORDER BY ARDE_EMPR, ARDE_SUC, ARDE_DEP, ARDE_ART;
  
    V_MESSAGE          VARCHAR2(200);
    V_PERI_ACT_FEC_INI DATE;
  BEGIN
  
    SELECT PERI_FEC_INI
      INTO V_PERI_ACT_FEC_INI
      FROM STK_PERIODO, STK_CONFIGURACION
     WHERE PERI_CODIGO = CONF_PERIODO_ACT
       AND PERI_EMPR = CONF_EMPR
       AND CONF_EMPR = I_EMPRESA;
  
    --MOVIMIENTOS SIN COINCIDIR CON ACUMULADOS
    FOR V IN MOV_SC_ACUM(I_PERI_ACT_FEC_INI => V_PERI_ACT_FEC_INI) LOOP
      V_MESSAGE := ' MOV_SC_ACUM, Empr=' || TO_CHAR(V.EMPR) || ', Suc=' ||
                   TO_CHAR(V.SUC) || ', Dep=' || TO_CHAR(V.DEP) || ', Art=' ||
                   TO_CHAR(V.ART);
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
      V_MESSAGE := 'Error 201: Falta reg. en STK_ARTICULO_DEPOSITO' ||
                   ' para Empr=' || TO_CHAR(V.EMPR) || ', Suc=' ||
                   TO_CHAR(V.SUC) || ', Dep=' || TO_CHAR(V.DEP) || ', Art=' ||
                   TO_CHAR(V.ART) || '. Desea corregir?';
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
      INSERT INTO STK_ARTICULO_DEPOSITO
        (ARDE_EMPR,
         ARDE_SUC,
         ARDE_DEP,
         ARDE_ART,
         ARDE_CANT_INI_OPER,
         ARDE_CANT_ACT_OPER,
         ARDE_CANT_ENT_OPER,
         ARDE_CANT_SAL_OPER)
      VALUES
        (V.EMPR, V.SUC, V.DEP, V.ART, V.CANT_ENT - V.CANT_SAL, 0, 0, 0);
    
    END LOOP;
  
    --ACUMULADOS SIN COINCIDIR CON MOVIMIENTOS
    FOR V IN ACUM_SC_MOV(I_PERI_ACT_FEC_INI => V_PERI_ACT_FEC_INI) LOOP
      V_MESSAGE := 'ACUM_SC_MOV, Empr=' || TO_CHAR(V.ARDE_EMPR) || ', Suc=' ||
                   TO_CHAR(V.ARDE_SUC) || ', Dep=' || TO_CHAR(V.ARDE_DEP) ||
                   ', Art=' || TO_CHAR(V.ARDE_ART);
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
      V_MESSAGE := 'Error 202: Cant.inicial incorrecto ' ||
                   ' en STK_ARTICULO_DEPOSITO para ' || 'Empr=' ||
                   TO_CHAR(V.ARDE_EMPR) || ', Suc=' || TO_CHAR(V.ARDE_SUC) ||
                   ', Dep=' || TO_CHAR(V.ARDE_DEP) || ', Art=' ||
                   TO_CHAR(V.ARDE_ART) || '. Desea corregir?';
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
      UPDATE STK_ARTICULO_DEPOSITO
         SET ARDE_CANT_INI_OPER = 0
       WHERE ARDE_EMPR = V.ARDE_EMPR
         AND ARDE_SUC = V.ARDE_SUC
         AND ARDE_DEP = V.ARDE_DEP
         AND ARDE_ART = V.ARDE_ART;
    
    END LOOP;
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001,
                              'Error en PP_VERIF_ART_DEP_EX_INI_OPER - ' ||
                              SQLCODE || ' -ERROR- ' || SQLERRM);
    
  END PP_VERIF_ART_DEP_EX_INI_OPER;

  PROCEDURE PP_VERIF_ART_DEP_EX_ACT(I_EMPRESA IN NUMBER,
                                    I_ART     IN NUMBER DEFAULT NULL) IS
  
    CURSOR MOV_SC_ACUM(I_PERI_ACT_FEC_INI  IN DATE,
                       I_PERI_SGTE_FEC_FIN IN DATE) IS
      SELECT CANT_CUR.*
        FROM (SELECT DOCU_EMPR EMPR,
                     DOCU_SUC_ORIG SUC,
                     DOCU_DEP_ORIG DEP,
                     DETA_ART ART,
                     SUM(DECODE(OPER_ENT_SAL, 'E', DETA_CANT, 0)) CANT_ENT,
                     SUM(DECODE(OPER_ENT_SAL, 'S', DETA_CANT, 0)) CANT_SAL
                FROM STK_OPERACION,
                     STK_DOCUMENTO,
                     STK_DOCUMENTO_DET,
                     STK_ARTICULO
              
               WHERE OPER_CODIGO = DOCU_CODIGO_OPER
                 AND OPER_EMPR = DOCU_EMPR
                    
                 AND DOCU_CLAVE = DETA_CLAVE_DOC
                 AND DOCU_EMPR = DETA_EMPR
                    
                 AND ART_CODIGO = DETA_ART
                 AND ART_EMPR = DETA_EMPR
            --     AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
                    
                 AND ART_TIPO <> 4
                 AND DOCU_EMPR = I_EMPRESA
                 AND DOCU_FEC_EMIS BETWEEN I_PERI_ACT_FEC_INI AND
                     I_PERI_SGTE_FEC_FIN
              
               GROUP BY DOCU_EMPR, DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART
              
              ) CANT_CUR,
             
             (SELECT ARDE_EMPR,
                     ARDE_SUC,
                     ARDE_DEP,
                     ARDE_ART,
                     ARDE_CANT_INI,
                     ARDE_CANT_ENT,
                     ARDE_CANT_SAL,
                     ARDE_CANT_ACT
                FROM STK_ARTICULO_DEPOSITO, STK_ARTICULO
               WHERE ARDE_EMPR = I_EMPRESA
                 AND ARDE_ART = ART_CODIGO
                 AND ARDE_EMPR = ART_EMPR
                    
             --    AND (ARDE_ART = I_ART OR I_ART IS NULL) --*
                 AND ART_TIPO <> 4
              
              ) ARDE_CUR
       WHERE (CANT_CUR.EMPR = ARDE_CUR.ARDE_EMPR(+) AND
             CANT_CUR.SUC = ARDE_CUR.ARDE_SUC(+) AND
             CANT_CUR.DEP = ARDE_CUR.ARDE_DEP(+) AND
             CANT_CUR.ART = ARDE_CUR.ARDE_ART(+))
         AND ARDE_CUR.ARDE_ART IS NULL
       ORDER BY EMPR, SUC, DEP, ART;
  
    --
    -- CURSOR PARA SELECCIONAR LOS REGISTROS DE TOTALES DE STK_ARTICULO_DEPOSITO
    CURSOR ACUM_SC_MOV(I_PERI_ACT_FEC_INI  IN DATE,
                       I_PERI_SGTE_FEC_FIN IN DATE) IS
      SELECT CANT_CUR.*, ARDE_CANT_INI
        FROM (SELECT DOCU_EMPR EMPR,
                     DOCU_SUC_ORIG SUC,
                     DOCU_DEP_ORIG DEP,
                     DETA_ART ART,
                     SUM(DECODE(OPER_ENT_SAL, 'E', DETA_CANT, 0)) CANT_ENT,
                     SUM(DECODE(OPER_ENT_SAL, 'S', DETA_CANT, 0)) CANT_SAL
                FROM STK_OPERACION,
                     STK_DOCUMENTO,
                     STK_DOCUMENTO_DET,
                     STK_ARTICULO
               WHERE OPER_CODIGO = DOCU_CODIGO_OPER
                 AND OPER_EMPR = DOCU_EMPR
                    
                 AND DOCU_CLAVE = DETA_CLAVE_DOC
                 AND DOCU_EMPR = DETA_EMPR
                    
                 AND ART_CODIGO = DETA_ART
                 AND ART_EMPR = DETA_EMPR
           --      AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
                    
                 AND ART_TIPO <> 4
                 AND DOCU_EMPR = I_EMPRESA
                 AND DOCU_FEC_EMIS BETWEEN I_PERI_ACT_FEC_INI AND
                     I_PERI_SGTE_FEC_FIN
              
               GROUP BY DOCU_EMPR, DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART
              
              ) CANT_CUR,
             
             (SELECT ARDE_EMPR,
                     ARDE_SUC,
                     ARDE_DEP,
                     ARDE_ART,
                     ARDE_CANT_INI,
                     ARDE_CANT_ENT,
                     ARDE_CANT_SAL,
                     ARDE_CANT_ACT
                FROM STK_ARTICULO_DEPOSITO, STK_ARTICULO
               WHERE ARDE_EMPR = I_EMPRESA
                 AND ARDE_ART = ART_CODIGO
                 AND ARDE_EMPR = ART_EMPR
              --   AND (ARDE_ART = I_ART OR I_ART IS NULL) --*
                    
                 AND ART_TIPO <> 4
                 AND (ARDE_CANT_SAL <> 0 OR ARDE_CANT_ENT <> 0 OR
                     ARDE_CANT_ACT <> ARDE_CANT_INI)
              
              ) ARDE_CUR
       WHERE (CANT_CUR.EMPR(+) = ARDE_CUR.ARDE_EMPR AND
             CANT_CUR.SUC(+) = ARDE_CUR.ARDE_SUC AND
             CANT_CUR.DEP(+) = ARDE_CUR.ARDE_DEP AND
             CANT_CUR.ART(+) = ARDE_CUR.ARDE_ART)
         AND CANT_CUR.ART IS NULL
      
       ORDER BY ARDE_EMPR, ARDE_SUC, ARDE_DEP, ARDE_ART;
  
    CURSOR DIFERENCIAS(I_PERI_ACT_FEC_INI  IN DATE,
                       I_PERI_SGTE_FEC_FIN IN DATE) IS
      SELECT CANT_CUR.*, ARDE_CANT_INI
        FROM (SELECT DOCU_EMPR EMPR,
                     DOCU_SUC_ORIG SUC,
                     DOCU_DEP_ORIG DEP,
                     DETA_ART ART,
                     SUM(DECODE(OPER_ENT_SAL, 'E', DETA_CANT, 0)) CANT_ENT,
                     SUM(DECODE(OPER_ENT_SAL, 'S', DETA_CANT, 0)) CANT_SAL
                FROM STK_OPERACION,
                     STK_DOCUMENTO,
                     STK_DOCUMENTO_DET,
                     STK_ARTICULO
              
               WHERE OPER_CODIGO = DOCU_CODIGO_OPER
                 AND OPER_EMPR = DOCU_EMPR
                    
                 AND DOCU_CLAVE = DETA_CLAVE_DOC
                 AND DOCU_EMPR = DETA_EMPR
                    
                 AND ART_CODIGO = DETA_ART
                 AND ART_EMPR = DETA_EMPR
         --        AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
                    
                 AND ART_TIPO <> 4
                 AND DOCU_EMPR = I_EMPRESA
                    
                 AND DOCU_FEC_EMIS BETWEEN I_PERI_ACT_FEC_INI AND
                     I_PERI_SGTE_FEC_FIN
              
               GROUP BY DOCU_EMPR, DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART) CANT_CUR,
             (SELECT ARDE_EMPR,
                     ARDE_SUC,
                     ARDE_DEP,
                     ARDE_ART,
                     ARDE_CANT_INI,
                     ARDE_CANT_ENT,
                     ARDE_CANT_SAL,
                     ARDE_CANT_ACT
                FROM STK_ARTICULO_DEPOSITO, STK_ARTICULO
               WHERE ARDE_EMPR = I_EMPRESA
                 AND ARDE_ART = ART_CODIGO
                 AND ARDE_EMPR = ART_EMPR
            --     AND (ARDE_ART = I_ART OR I_ART IS NULL) --*
                    
                 AND ART_TIPO <> 4
              
              ) ARDE_CUR
       WHERE (CANT_CUR.EMPR = ARDE_CUR.ARDE_EMPR AND
             CANT_CUR.SUC = ARDE_CUR.ARDE_SUC AND
             CANT_CUR.DEP = ARDE_CUR.ARDE_DEP AND
             CANT_CUR.ART = ARDE_CUR.ARDE_ART)
         AND (CANT_ENT <> ARDE_CANT_ENT OR CANT_SAL <> ARDE_CANT_SAL OR
             (ARDE_CANT_INI + CANT_ENT - CANT_SAL) <> ARDE_CANT_ACT)
       ORDER BY ARDE_EMPR, ARDE_SUC, ARDE_DEP, ARDE_ART;
  
    CURSOR ACUM_SC_MOV2(I_PERI_ACT_FEC_INI  IN DATE,
                        I_PERI_SGTE_FEC_FIN IN DATE) IS
      SELECT CANT_CUR.*, ARDE_CANT_INI
      
        FROM (SELECT DOCU_EMPR EMPR,
                     DOCU_SUC_ORIG SUC,
                     DOCU_DEP_ORIG DEP,
                     DETA_ART ART,
                     SUM(DECODE(OPER_ENT_SAL, 'E', DETA_CANT, 0)) CANT_ENT,
                     SUM(DECODE(OPER_ENT_SAL, 'S', DETA_CANT, 0)) CANT_SAL
                FROM STK_OPERACION,
                     STK_DOCUMENTO,
                     STK_DOCUMENTO_DET,
                     STK_ARTICULO
              
               WHERE OPER_CODIGO = DOCU_CODIGO_OPER
                 AND OPER_EMPR = DOCU_EMPR
                    
                 AND DOCU_CLAVE = DETA_CLAVE_DOC
                 AND DOCU_EMPR = DETA_EMPR
                    
                 AND ART_CODIGO = DETA_ART
                 AND ART_EMPR = DETA_EMPR
           --      AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
                    
                 AND ART_TIPO <> 4
                 AND DOCU_EMPR = I_EMPRESA
                    
                 AND DOCU_FEC_EMIS BETWEEN I_PERI_ACT_FEC_INI AND
                     I_PERI_SGTE_FEC_FIN
              
               GROUP BY DOCU_EMPR, DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART
              
              ) CANT_CUR,
             (SELECT ARDE_EMPR,
                     ARDE_SUC,
                     ARDE_DEP,
                     ARDE_ART,
                     ARDE_CANT_INI,
                     ARDE_CANT_ENT,
                     ARDE_CANT_SAL,
                     ARDE_CANT_ACT
                FROM STK_ARTICULO_DEPOSITO, STK_ARTICULO
               WHERE ARDE_EMPR = I_EMPRESA
                 AND ARDE_ART = ART_CODIGO
                 AND ARDE_EMPR = ART_EMPR
           --      AND (ARDE_ART = I_ART OR I_ART IS NULL) --*
                    
                 AND ART_TIPO <> 4
                 AND (ARDE_CANT_SAL <> 0 OR ARDE_CANT_ENT <> 0 OR
                     ARDE_CANT_ACT <> ARDE_CANT_INI)) ARDE_CUR
      
       WHERE (CANT_CUR.EMPR(+) = ARDE_CUR.ARDE_EMPR AND
             CANT_CUR.SUC(+) = ARDE_CUR.ARDE_SUC AND
             CANT_CUR.DEP(+) = ARDE_CUR.ARDE_DEP AND
             CANT_CUR.ART(+) = ARDE_CUR.ARDE_ART)
         AND CANT_CUR.ART IS NULL
         AND (ARDE_CANT_SAL <> 0 OR ARDE_CANT_ENT <> 0 OR
             (ARDE_CANT_INI + ARDE_CANT_ENT - ARDE_CANT_SAL) <>
             ARDE_CANT_ACT)
       ORDER BY ARDE_EMPR, ARDE_SUC, ARDE_DEP, ARDE_ART;
  
    V_MESSAGE VARCHAR2(200);
  
    V_PERI_ACT_FEC_INI  DATE;
    V_PERI_SGTE_FEC_FIN DATE;
  BEGIN
  
    SELECT ACT.PERI_FEC_INI, SGT.PERI_FEC_FIN
      INTO V_PERI_ACT_FEC_INI, V_PERI_SGTE_FEC_FIN
      FROM STK_PERIODO ACT, STK_CONFIGURACION C, STK_PERIODO SGT
     WHERE ACT.PERI_CODIGO = C.CONF_PERIODO_ACT
       AND ACT.PERI_EMPR = C.CONF_EMPR
       AND SGT.PERI_CODIGO = C.CONF_PERIODO_SGTE
       AND SGT.PERI_EMPR = C.CONF_EMPR
       AND C.CONF_EMPR = I_EMPRESA;
  
    -- V_PERI_ACT_FEC_INI
  
    FOR V IN MOV_SC_ACUM(I_PERI_ACT_FEC_INI  => V_PERI_ACT_FEC_INI,
                         I_PERI_SGTE_FEC_FIN => V_PERI_SGTE_FEC_FIN) LOOP
    
      V_MESSAGE := 'MOV_SC_ACUM, Empr=' || TO_CHAR(V.EMPR) || ', Suc=' ||
                   TO_CHAR(V.EMPR) || ', Dep=' || TO_CHAR(V.DEP) ||
                   ', Art=' || TO_CHAR(V.ART);
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
      -- ENTRA AQUI SI EXISTEN MOVIMIENTOS SIN MAESTRO
      V_MESSAGE := 'Error 501: Falta reg. en STK_ARTICULO_DEPOSITO' ||
                   ' para Empr=' || TO_CHAR(V.EMPR) || ', Suc=' ||
                   TO_CHAR(V.EMPR) || ', Dep=' || TO_CHAR(V.DEP) ||
                   ', Art=' || TO_CHAR(V.ART) || '. Desea corregir?';
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
      INSERT INTO STK_ARTICULO_DEPOSITO
        (ARDE_EMPR,
         ARDE_SUC,
         ARDE_DEP,
         ARDE_ART,
         ARDE_CANT_INI,
         ARDE_CANT_ACT,
         ARDE_CANT_ENT,
         ARDE_CANT_SAL,
         ARDE_CANT_INV,
         ARDE_FEC_INV,
         ARDE_UBIC)
      VALUES
        (V.EMPR,
         V.SUC,
         V.DEP,
         V.ART,
         0,
         V.CANT_ENT - V.CANT_SAL,
         V.CANT_ENT,
         V.CANT_SAL,
         0,
         NULL,
         NULL);
    
    END LOOP;
  
    FOR V IN ACUM_SC_MOV(I_PERI_ACT_FEC_INI  => V_PERI_ACT_FEC_INI,
                         I_PERI_SGTE_FEC_FIN => V_PERI_SGTE_FEC_FIN) LOOP
      V_MESSAGE := 'ACUM_SC_MOV, Empr=' || TO_CHAR(V.EMPR) || ', Suc=' ||
                   TO_CHAR(V.EMPR) || ', Dep=' || TO_CHAR(V.DEP) ||
                   ', Art=' || TO_CHAR(V.ART);
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
      V_MESSAGE := 'Error 502: Cant.entrada y salida incorrecto' ||
                   ' en STK_ARTICULO_DEPOSITO para ' || 'Empr=' ||
                   TO_CHAR(V.EMPR) || ', Suc=' || TO_CHAR(V.SUC) ||
                   ', Dep=' || TO_CHAR(V.DEP) || ', Art=' || TO_CHAR(V.ART) ||
                   '. Desea corregir?';
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
      UPDATE STK_ARTICULO_DEPOSITO
         SET ARDE_CANT_ENT = 0,
             ARDE_CANT_SAL = 0,
             ARDE_CANT_ACT = V.ARDE_CANT_INI
       WHERE ARDE_EMPR = V.EMPR
         AND ARDE_SUC = V.SUC
         AND ARDE_DEP = V.DEP
         AND ARDE_ART = V.ART;
    
    END LOOP;
  
    FOR V IN DIFERENCIAS(I_PERI_ACT_FEC_INI  => V_PERI_ACT_FEC_INI,
                         I_PERI_SGTE_FEC_FIN => V_PERI_SGTE_FEC_FIN) LOOP
      V_MESSAGE := 'DIF, Empr=' || TO_CHAR(V.EMPR) || ', Suc=' ||
                   TO_CHAR(V.EMPR) || ', Dep=' || TO_CHAR(V.DEP) ||
                   ', Art=' || TO_CHAR(V.ART);
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
      -- ACTUALIZAR SI HAY DIFERENCIAS ENTRE MOV.Y MAESTRO
      V_MESSAGE := 'Error 503: Cantidad incorrecta en STK_ARTICULO_DEPOSITO' ||
                   ' para Empr=' || TO_CHAR(V.EMPR) || ', Suc=' ||
                   TO_CHAR(V.SUC) || ', Dep=' || TO_CHAR(V.DEP) || ', Art=' ||
                   TO_CHAR(V.ART) || '. Desea corregir?';
    
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
    
      UPDATE STK_ARTICULO_DEPOSITO
         SET ARDE_CANT_ENT = V.CANT_ENT,
             ARDE_CANT_SAL = V.CANT_SAL,
             ARDE_CANT_ACT =
             (V.ARDE_CANT_INI + V.CANT_ENT - V.CANT_SAL)
       WHERE ARDE_EMPR = V.EMPR
         AND ARDE_SUC = V.SUC
         AND ARDE_DEP = V.DEP
         AND ARDE_ART = V.ART;
    
    END LOOP;
  
    FOR V IN ACUM_SC_MOV2(I_PERI_ACT_FEC_INI  => V_PERI_ACT_FEC_INI,
                          I_PERI_SGTE_FEC_FIN => V_PERI_SGTE_FEC_FIN) LOOP
      V_MESSAGE := 'ACUM_SC_MOV2, Empr=' || TO_CHAR(V.EMPR) || ', Suc=' ||
                   TO_CHAR(V.EMPR) || ', Dep=' || TO_CHAR(V.DEP) ||
                   ', Art=' || TO_CHAR(V.ART);
    
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
    
      V_MESSAGE := 'Error 505: Cantidad incorrecta en STK_ARTICULO_DEPOSITO para ' ||
                   'Empr=' || TO_CHAR(V.EMPR) || ', Suc=' || TO_CHAR(V.SUC) ||
                   ', Dep=' || TO_CHAR(V.DEP) || ', Art=' || TO_CHAR(V.ART) ||
                   '. Desea corregir?';
    
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
    
      UPDATE STK_ARTICULO_DEPOSITO
         SET ARDE_CANT_ENT = 0,
             ARDE_CANT_SAL = 0,
             ARDE_CANT_ACT = V.ARDE_CANT_INI
       WHERE ARDE_EMPR = V.EMPR
         AND ARDE_SUC = V.SUC
         AND ARDE_DEP = V.DEP
         AND ARDE_ART = V.ART;
    
    END LOOP;
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20015,
                              'Error en PP_VERIF_ART_DEP_EX_ACT' || SQLCODE || '-' ||
                              SQLERRM);
  END PP_VERIF_ART_DEP_EX_ACT;

  PROCEDURE PP_VERIF_ART_DEP_EX_ACT_OPER(I_EMPRESA IN NUMBER,
                                         I_ART     IN NUMBER DEFAULT NULL) IS
  
    CURSOR MOV_SC_ACUM(I_PERI_ACT_FEC_INI  IN DATE,
                       I_PERI_SGTE_FEC_FIN IN DATE) IS
      SELECT CANT_CUR.*
        FROM (SELECT DOCU_EMPR EMPR,
                     DOCU_SUC_ORIG SUC,
                     DOCU_DEP_ORIG DEP,
                     DETA_ART ART,
                     SUM(DECODE(OPER_ENT_SAL, 'E', DETA_CANT, 0)) CANT_ENT,
                     SUM(DECODE(OPER_ENT_SAL, 'S', DETA_CANT, 0)) CANT_SAL
                FROM STK_OPERACION,
                     STK_DOCUMENTO,
                     STK_DOCUMENTO_DET,
                     STK_ARTICULO
               WHERE OPER_CODIGO = DOCU_CODIGO_OPER
                 AND OPER_EMPR = DOCU_EMPR
                    
                 AND DOCU_CLAVE = DETA_CLAVE_DOC
                 AND DOCU_EMPR = DETA_EMPR
                    
                 AND ART_CODIGO = DETA_ART
                 AND ART_EMPR = DETA_EMPR
             --    AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
                    
                 AND ART_TIPO <> 4
                 AND DOCU_EMPR = I_EMPRESA
                 AND DOCU_FEC_OPER BETWEEN I_PERI_ACT_FEC_INI AND
                     I_PERI_SGTE_FEC_FIN
              
               GROUP BY DOCU_EMPR, DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART
              
              ) CANT_CUR,
             
             (SELECT ARDE_EMPR,
                     ARDE_SUC,
                     ARDE_DEP,
                     ARDE_ART,
                     ARDE_CANT_INI_OPER,
                     ARDE_CANT_ENT_OPER,
                     ARDE_CANT_SAL_OPER,
                     ARDE_CANT_ACT_OPER
                FROM STK_ARTICULO_DEPOSITO, STK_ARTICULO
               WHERE ARDE_EMPR = I_EMPRESA
                 AND ARDE_ART = ART_CODIGO
                 AND ARDE_EMPR = ART_EMPR
                 AND ART_TIPO <> 4
             --    AND (ARDE_ART = I_ART OR I_ART IS NULL) --*
              ) ARDE_CUR
       WHERE (CANT_CUR.EMPR = ARDE_CUR.ARDE_EMPR(+) AND
             CANT_CUR.SUC = ARDE_CUR.ARDE_SUC(+) AND
             CANT_CUR.DEP = ARDE_CUR.ARDE_DEP(+) AND
             CANT_CUR.ART = ARDE_CUR.ARDE_ART(+))
         AND ARDE_CUR.ARDE_ART IS NULL
       ORDER BY EMPR, SUC, DEP, ART;
  
    --
    -- CURSOR PARA SELECCIONAR LOS REGISTROS DE TOTALES DE STK_ARTICULO_DEPOSITO
    CURSOR ACUM_SC_MOV(I_PERI_ACT_FEC_INI  IN DATE,
                       I_PERI_SGTE_FEC_FIN IN DATE) IS
      SELECT CANT_CUR.*, ARDE_CANT_INI_OPER
        FROM (SELECT DOCU_EMPR EMPR,
                     DOCU_SUC_ORIG SUC,
                     DOCU_DEP_ORIG DEP,
                     DETA_ART ART,
                     SUM(DECODE(OPER_ENT_SAL, 'E', DETA_CANT, 0)) CANT_ENT,
                     SUM(DECODE(OPER_ENT_SAL, 'S', DETA_CANT, 0)) CANT_SAL
                FROM STK_OPERACION,
                     STK_DOCUMENTO,
                     STK_DOCUMENTO_DET,
                     STK_ARTICULO
               WHERE OPER_CODIGO = DOCU_CODIGO_OPER
                 AND OPER_EMPR = DOCU_EMPR
                    
                 AND DOCU_CLAVE = DETA_CLAVE_DOC
                 AND DOCU_EMPR = DETA_EMPR
                    
                 AND ART_CODIGO = DETA_ART
                 AND ART_EMPR = DETA_EMPR
         --        AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
                 AND ART_TIPO <> 4
                 AND DOCU_EMPR = I_EMPRESA
                 AND DOCU_FEC_OPER BETWEEN I_PERI_ACT_FEC_INI AND
                     I_PERI_SGTE_FEC_FIN
               GROUP BY DOCU_EMPR, DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART) CANT_CUR,
             (SELECT ARDE_EMPR,
                     ARDE_SUC,
                     ARDE_DEP,
                     ARDE_ART,
                     ARDE_CANT_INI_OPER,
                     ARDE_CANT_ENT_OPER,
                     ARDE_CANT_SAL_OPER,
                     ARDE_CANT_ACT_OPER
                FROM STK_ARTICULO_DEPOSITO, STK_ARTICULO
               WHERE ARDE_EMPR = I_EMPRESA
                 AND ARDE_ART = ART_CODIGO
                 AND ARDE_EMPR = ART_EMPR
            --     AND (ARDE_ART = I_ART OR I_ART IS NULL) --*
                 AND ART_TIPO <> 4
                 AND (ARDE_CANT_SAL_OPER <> 0 OR ARDE_CANT_ENT_OPER <> 0 OR
                     ARDE_CANT_ACT_OPER <> ARDE_CANT_INI_OPER)) ARDE_CUR
       WHERE (CANT_CUR.EMPR(+) = ARDE_CUR.ARDE_EMPR AND
             CANT_CUR.SUC(+) = ARDE_CUR.ARDE_SUC AND
             CANT_CUR.DEP(+) = ARDE_CUR.ARDE_DEP AND
             CANT_CUR.ART(+) = ARDE_CUR.ARDE_ART)
         AND CANT_CUR.ART IS NULL
      
       ORDER BY ARDE_EMPR, ARDE_SUC, ARDE_DEP, ARDE_ART;
  
    CURSOR DIFERENCIAS(I_PERI_ACT_FEC_INI  IN DATE,
                       I_PERI_SGTE_FEC_FIN IN DATE) IS
      SELECT CANT_CUR.*, ARDE_CANT_INI_OPER
        FROM (SELECT DOCU_EMPR EMPR,
                     DOCU_SUC_ORIG SUC,
                     DOCU_DEP_ORIG DEP,
                     DETA_ART ART,
                     SUM(DECODE(OPER_ENT_SAL, 'E', DETA_CANT, 0)) CANT_ENT,
                     SUM(DECODE(OPER_ENT_SAL, 'S', DETA_CANT, 0)) CANT_SAL
                FROM STK_OPERACION,
                     STK_DOCUMENTO,
                     STK_DOCUMENTO_DET,
                     STK_ARTICULO
               WHERE OPER_CODIGO = DOCU_CODIGO_OPER
                 AND OPER_EMPR = DOCU_EMPR
                    
                 AND DOCU_CLAVE = DETA_CLAVE_DOC
                 AND DOCU_EMPR = DETA_EMPR
                    
                 AND ART_CODIGO = DETA_ART
                 AND ART_EMPR = DETA_EMPR
            --     AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
                 AND ART_TIPO <> 4
                 AND DOCU_EMPR = I_EMPRESA
                 AND DOCU_FEC_OPER BETWEEN I_PERI_ACT_FEC_INI AND
                     I_PERI_SGTE_FEC_FIN
               GROUP BY DOCU_EMPR, DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART) CANT_CUR,
             (SELECT ARDE_EMPR,
                     ARDE_SUC,
                     ARDE_DEP,
                     ARDE_ART,
                     ARDE_CANT_INI_OPER,
                     ARDE_CANT_ENT_OPER,
                     ARDE_CANT_SAL_OPER,
                     ARDE_CANT_ACT_OPER
                FROM STK_ARTICULO_DEPOSITO, STK_ARTICULO
               WHERE ARDE_EMPR = I_EMPRESA
                 AND ARDE_ART = ART_CODIGO
                 AND ARDE_EMPR = ART_EMPR
                 AND ART_TIPO <> 4
          --       AND (ARDE_ART = I_ART OR I_ART IS NULL) --*
              ) ARDE_CUR
       WHERE (CANT_CUR.EMPR = ARDE_CUR.ARDE_EMPR AND
             CANT_CUR.SUC = ARDE_CUR.ARDE_SUC AND
             CANT_CUR.DEP = ARDE_CUR.ARDE_DEP AND
             CANT_CUR.ART = ARDE_CUR.ARDE_ART)
         AND (CANT_ENT <> ARDE_CANT_ENT_OPER OR
             CANT_SAL <> ARDE_CANT_SAL_OPER OR
             (ARDE_CANT_INI_OPER + CANT_ENT - CANT_SAL) <>
             ARDE_CANT_ACT_OPER)
       ORDER BY ARDE_EMPR, ARDE_SUC, ARDE_DEP, ARDE_ART;
  
    CURSOR ACUM_SC_MOV2(I_PERI_ACT_FEC_INI  IN DATE,
                        I_PERI_SGTE_FEC_FIN IN DATE) IS
      SELECT CANT_CUR.*, ARDE_CANT_INI_OPER
        FROM (SELECT DOCU_EMPR EMPR,
                     DOCU_SUC_ORIG SUC,
                     DOCU_DEP_ORIG DEP,
                     DETA_ART ART,
                     SUM(DECODE(OPER_ENT_SAL, 'E', DETA_CANT, 0)) CANT_ENT,
                     SUM(DECODE(OPER_ENT_SAL, 'S', DETA_CANT, 0)) CANT_SAL
                FROM STK_OPERACION,
                     STK_DOCUMENTO,
                     STK_DOCUMENTO_DET,
                     STK_ARTICULO
               WHERE OPER_CODIGO = DOCU_CODIGO_OPER
                 AND OPER_EMPR = DOCU_EMPR
                    
                 AND DOCU_CLAVE = DETA_CLAVE_DOC
                 AND DOCU_EMPR = DETA_EMPR
                    
                 AND ART_CODIGO = DETA_ART
                 AND ART_EMPR = DETA_EMPR
            --     AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
                    
                 AND ART_TIPO <> 4
                 AND DOCU_EMPR = I_EMPRESA
                 AND DOCU_FEC_OPER BETWEEN I_PERI_ACT_FEC_INI AND
                     I_PERI_SGTE_FEC_FIN
               GROUP BY DOCU_EMPR, DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART) CANT_CUR,
             (SELECT ARDE_EMPR,
                     ARDE_SUC,
                     ARDE_DEP,
                     ARDE_ART,
                     ARDE_CANT_INI_OPER,
                     ARDE_CANT_ENT_OPER,
                     ARDE_CANT_SAL_OPER,
                     ARDE_CANT_ACT_OPER
                FROM STK_ARTICULO_DEPOSITO, STK_ARTICULO
               WHERE ARDE_EMPR = I_EMPRESA
                 AND ARDE_ART = ART_CODIGO
                 AND ARDE_EMPR = ART_EMPR
                 AND ART_TIPO <> 4
            --    AND (ARDE_ART = I_ART OR I_ART IS NULL) --*
                    
                 AND (ARDE_CANT_SAL_OPER <> 0 OR ARDE_CANT_ENT_OPER <> 0 OR
                     ARDE_CANT_ACT_OPER <> ARDE_CANT_INI_OPER)) ARDE_CUR
       WHERE (CANT_CUR.EMPR(+) = ARDE_CUR.ARDE_EMPR AND
             CANT_CUR.SUC(+) = ARDE_CUR.ARDE_SUC AND
             CANT_CUR.DEP(+) = ARDE_CUR.ARDE_DEP AND
             CANT_CUR.ART(+) = ARDE_CUR.ARDE_ART)
         AND CANT_CUR.ART IS NULL
         AND (ARDE_CANT_SAL_OPER <> 0 OR ARDE_CANT_ENT_OPER <> 0 OR
             (ARDE_CANT_INI_OPER + ARDE_CANT_ENT_OPER - ARDE_CANT_SAL_OPER) <>
             ARDE_CANT_ACT_OPER)
       ORDER BY ARDE_EMPR, ARDE_SUC, ARDE_DEP, ARDE_ART;
  
    V_MESSAGE VARCHAR2(200);
  
    V_PERI_ACT_FEC_INI  DATE;
    V_PERI_SGTE_FEC_FIN DATE;
  BEGIN
  
    SELECT ACT.PERI_FEC_INI, SGT.PERI_FEC_FIN
      INTO V_PERI_ACT_FEC_INI, V_PERI_SGTE_FEC_FIN
      FROM STK_PERIODO ACT, STK_CONFIGURACION C, STK_PERIODO SGT
     WHERE ACT.PERI_CODIGO = C.CONF_PERIODO_ACT
       AND ACT.PERI_EMPR = C.CONF_EMPR
       AND SGT.PERI_CODIGO = C.CONF_PERIODO_SGTE
       AND SGT.PERI_EMPR = C.CONF_EMPR
       AND C.CONF_EMPR = I_EMPRESA;
  
    FOR V IN MOV_SC_ACUM(I_PERI_ACT_FEC_INI  => V_PERI_ACT_FEC_INI,
                         I_PERI_SGTE_FEC_FIN => V_PERI_SGTE_FEC_FIN) LOOP
    
      V_MESSAGE := 'MOV_SC_ACUM, Empr=' || TO_CHAR(V.EMPR) || ', Suc=' ||
                   TO_CHAR(V.EMPR) || ', Dep=' || TO_CHAR(V.DEP) ||
                   ', Art=' || TO_CHAR(V.ART);
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
      -- ENTRA AQUI SI EXISTEN MOVIMIENTOS SIN MAESTRO
      V_MESSAGE := 'Error 501: Falta reg. en STK_ARTICULO_DEPOSITO' ||
                   ' para Empr=' || TO_CHAR(V.EMPR) || ', Suc=' ||
                   TO_CHAR(V.EMPR) || ', Dep=' || TO_CHAR(V.DEP) ||
                   ', Art=' || TO_CHAR(V.ART) || '. Desea corregir?';
    
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
      INSERT INTO STK_ARTICULO_DEPOSITO
        (ARDE_EMPR,
         ARDE_SUC,
         ARDE_DEP,
         ARDE_ART,
         ARDE_CANT_INI_OPER,
         ARDE_CANT_ACT_OPER,
         ARDE_CANT_ENT_OPER,
         ARDE_CANT_SAL_OPER)
      VALUES
        (V.EMPR,
         V.SUC,
         V.DEP,
         V.ART,
         0,
         V.CANT_ENT - V.CANT_SAL,
         V.CANT_ENT,
         V.CANT_SAL);
    
    END LOOP;
  
    V_MESSAGE := 'Seleccionando acumulados sin coincidir con movimientos OPER';
  
    FOR V IN ACUM_SC_MOV(I_PERI_ACT_FEC_INI  => V_PERI_ACT_FEC_INI,
                         I_PERI_SGTE_FEC_FIN => V_PERI_SGTE_FEC_FIN) LOOP
      V_MESSAGE := 'ACUM_SC_MOV, Empr=' || TO_CHAR(V.EMPR) || ', Suc=' ||
                   TO_CHAR(V.EMPR) || ', Dep=' || TO_CHAR(V.DEP) ||
                   ', Art=' || TO_CHAR(V.ART);
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
      V_MESSAGE := 'Error 502: Cant.entrada y salida incorrecto' ||
                   ' en STK_ARTICULO_DEPOSITO para ' || 'Empr=' ||
                   TO_CHAR(V.EMPR) || ', Suc=' || TO_CHAR(V.SUC) ||
                   ', Dep=' || TO_CHAR(V.DEP) || ', Art=' || TO_CHAR(V.ART) ||
                   '. Desea corregir?';
    
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
    
      UPDATE STK_ARTICULO_DEPOSITO
         SET ARDE_CANT_ENT_OPER = 0,
             ARDE_CANT_SAL_OPER = 0,
             ARDE_CANT_ACT_OPER = V.ARDE_CANT_INI_OPER
       WHERE ARDE_EMPR = V.EMPR
         AND ARDE_SUC = V.SUC
         AND ARDE_DEP = V.DEP
         AND ARDE_ART = V.ART;
    END LOOP;
  
    FOR V IN DIFERENCIAS(I_PERI_ACT_FEC_INI  => V_PERI_ACT_FEC_INI,
                         I_PERI_SGTE_FEC_FIN => V_PERI_SGTE_FEC_FIN) LOOP
      V_MESSAGE := 'DIF, Empr=' || TO_CHAR(V.EMPR) || ', Suc=' ||
                   TO_CHAR(V.EMPR) || ', Dep=' || TO_CHAR(V.DEP) ||
                   ', Art=' || TO_CHAR(V.ART);
    
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
    
      -- ACTUALIZAR SI HAY DIFERENCIAS ENTRE MOV.Y MAESTRO
      V_MESSAGE := 'Error 503: Cantidad incorrecta en STK_ARTICULO_DEPOSITO' ||
                   ' para Empr=' || TO_CHAR(V.EMPR) || ', Suc=' ||
                   TO_CHAR(V.SUC) || ', Dep=' || TO_CHAR(V.DEP) || ', Art=' ||
                   TO_CHAR(V.ART) || '. Desea corregir?';
    
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
    
      UPDATE STK_ARTICULO_DEPOSITO
         SET ARDE_CANT_ENT_OPER = V.CANT_ENT,
             ARDE_CANT_SAL_OPER = V.CANT_SAL,
             ARDE_CANT_ACT_OPER =
             (V.ARDE_CANT_INI_OPER + V.CANT_ENT - V.CANT_SAL)
       WHERE ARDE_EMPR = V.EMPR
         AND ARDE_SUC = V.SUC
         AND ARDE_DEP = V.DEP
         AND ARDE_ART = V.ART;
    END LOOP;
  
    FOR V IN ACUM_SC_MOV2(I_PERI_ACT_FEC_INI  => V_PERI_ACT_FEC_INI,
                          I_PERI_SGTE_FEC_FIN => V_PERI_SGTE_FEC_FIN) LOOP
      V_MESSAGE := 'ACUM_SC_MOV2, Empr=' || TO_CHAR(V.EMPR) || ', Suc=' ||
                   TO_CHAR(V.EMPR) || ', Dep=' || TO_CHAR(V.DEP) ||
                   ', Art=' || TO_CHAR(V.ART);
    
      PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
    
      V_MESSAGE := 'Error 505: Cantidad incorrecta en STK_ARTICULO_DEPOSITO para ' ||
                   'Empr=' || TO_CHAR(V.EMPR) || ', Suc=' || TO_CHAR(V.SUC) ||
                   ', Dep=' || TO_CHAR(V.DEP) || ', Art=' || TO_CHAR(V.ART) ||
                   '. Desea corregir?';
    
      UPDATE STK_ARTICULO_DEPOSITO
         SET ARDE_CANT_ENT = 0,
             ARDE_CANT_SAL = 0,
             ARDE_CANT_ACT = V.ARDE_CANT_INI_OPER
       WHERE ARDE_EMPR = V.EMPR
         AND ARDE_SUC = V.SUC
         AND ARDE_DEP = V.DEP
         AND ARDE_ART = V.ART;
    
    END LOOP;
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20016,
                              'Error en PP_VERIF_ART_DEP_EX_ACT_OPER' ||
                              SQLCODE || '-' || SQLERRM);
    
  END PP_VERIF_ART_DEP_EX_ACT_OPER;

  PROCEDURE PP_COMPARAR_CON_PERI_ANT(I_EMPRESA IN NUMBER,
                                     I_PERIODO IN NUMBER,
                                     I_ART     IN NUMBER DEFAULT NULL) IS
  
    CURSOR AEP_CUR(PERIANT IN NUMBER, PERIACT IN NUMBER) IS
      SELECT *
        FROM STK_ART_EMPR_PERI, STK_ARTICULO
       WHERE AEP_EMPR = I_EMPRESA
         AND ART_CODIGO = AEP_ART
         AND ART_EMPR = AEP_EMPR
         AND (ART_TIPO <> 4 or art_ind_inv_valor_costo = 'S')
       --  AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
         AND AEP_PERIODO BETWEEN PERIANT AND PERIACT
       ORDER BY AEP_EMPR, AEP_ART, AEP_PERIODO
         FOR UPDATE OF AEP_EXIST_INI, AEP_COSTO_INI_LOC, AEP_COSTO_PROM_INI_LOC, AEP_COSTO_INI_MON, AEP_COSTO_PROM_INI_MON;
  
    SALIR EXCEPTION;
    PERIANT              NUMBER;
    V_EMPR_ANT           STK_ART_EMPR_PERI.AEP_EMPR%TYPE;
    V_ART_ANT            STK_ART_EMPR_PERI.AEP_ART%TYPE;
    V_TOT_EXIST_ANT      STK_ART_EMPR_PERI.AEP_TOT_EXIST%TYPE;
    V_COSTO_PROM_LOC_ANT STK_ART_EMPR_PERI.AEP_COSTO_PROM_LOC%TYPE;
    V_COSTO_INI_LOC      STK_ART_EMPR_PERI.AEP_COSTO_INI_LOC%TYPE;
    V_COSTO_PROM_MON_ANT STK_ART_EMPR_PERI.AEP_COSTO_PROM_MON%TYPE;
    V_COSTO_INI_MON      STK_ART_EMPR_PERI.AEP_COSTO_INI_MON%TYPE;
    V_CANT_DECIMALES_LOC NUMBER := 0;
    -- V_MON_DEC_IMP_ANT    GEN_MONEDA.MON_DEC_IMP%TYPE;
  
    V_MENSAJE           VARCHAR2(200);
    V_DIFERENCIA        NUMBER;
    V_RESPUESTA         VARCHAR2(1) := 'S';
    EXISTE_PERIODO_SGTE BOOLEAN := TRUE;
  
  BEGIN
  
    SELECT MAX(PERI_CODIGO)
      INTO PERIANT
      FROM STK_PERIODO
     WHERE PERI_CODIGO < I_PERIODO
       AND PERI_EMPR = I_EMPRESA;
  
    IF PERIANT IS NULL THEN
      RAISE SALIR;
    END IF;
  
    FOR RAEP IN AEP_CUR(PERIANT, I_PERIODO) LOOP
      V_MENSAJE := 'Emp:' || TO_CHAR(RAEP.AEP_EMPR) || 'Art:' ||
                   TO_CHAR(RAEP.AEP_ART) || 'Per:' ||
                   TO_CHAR(RAEP.AEP_PERIODO);
    
      IF RAEP.AEP_PERIODO = PERIANT THEN
        IF NOT EXISTE_PERIODO_SGTE THEN
          --INGRESAR UN REGISTRO DE PERIODO SIGUIENTE
          V_COSTO_INI_LOC := 0;
          IF V_TOT_EXIST_ANT <> 0 THEN
            V_COSTO_INI_LOC := ROUND(V_TOT_EXIST_ANT *
                                     NVL(V_COSTO_PROM_LOC_ANT, 0),
                                     V_CANT_DECIMALES_LOC);
          END IF;
          V_COSTO_INI_MON := 0;
          IF V_TOT_EXIST_ANT <> 0 THEN
            V_COSTO_INI_MON := ROUND(V_TOT_EXIST_ANT * V_COSTO_PROM_MON_ANT,
                                     4);
          END IF;
        
          IF V_RESPUESTA = 'S' THEN
            INSERT INTO STK_ART_EMPR_PERI
              (AEP_PERIODO,
               AEP_EMPR,
               AEP_ART,
               AEP_TOT_EXIST,
               AEP_EXIST_INI,
               AEP_COSTO_INI_LOC,
               AEP_COSTO_INI_MON,
               AEP_COSTO_PROM_INI_LOC,
               AEP_COSTO_PROM_INI_MON,
               AEP_COSTO_PROM_LOC,
               AEP_COSTO_PROM_MON,
               AEP_EC_COMPRA,
               AEP_EL_COMPRA,
               AEP_EM_COMPRA,
               AEP_SC_DEV_COMPRA,
               AEP_SL_DEV_COMPRA,
               AEP_SM_DEV_COMPRA,
               AEP_SC_VTA,
               AEP_SL_VTA,
               AEP_SM_VTA,
               AEP_EC_DEV_VTA,
               AEP_EL_DEV_VTA,
               AEP_EM_DEV_VTA,
               AEP_EC_PROD,
               AEP_EL_PROD,
               AEP_EM_PROD,
               AEP_SC_PROD,
               AEP_EC_KIT,
               AEP_EL_KIT,
               AEP_EM_KIT,
               AEP_SC_KIT,
               AEP_SL_KIT,
               AEP_SM_KIT,
               AEP_SC_PERDIDA,
               AEP_SC_CONS,
               AEP_C_DIF_INV,
               AEP_C_TRANSF,
               AEP_C_REM,
               AEP_EC_ARM_DES,
               AEP_EL_ARM_DES,
               AEP_EM_ARM_DES,
               AEP_SC_ARM_DES,
               AEP_SL_ARM_DES,
               AEP_SM_ARM_DES,
               AEP_EL_DEV_PROD,
               AEP_EM_DEV_PROD,
               AEP_EC_DEV_PROD)
            VALUES
              (I_PERIODO,
               V_EMPR_ANT,
               V_ART_ANT,
               V_TOT_EXIST_ANT,
               V_TOT_EXIST_ANT,
               V_COSTO_INI_LOC,
               V_COSTO_INI_MON,
               NVL(V_COSTO_PROM_LOC_ANT, 0),
               V_COSTO_PROM_MON_ANT,
               V_COSTO_PROM_LOC_ANT,
               V_COSTO_PROM_MON_ANT,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0);
          END IF;
        END IF;
      END IF;
    
      IF RAEP.AEP_PERIODO = PERIANT THEN
        /* SI ES EL PERIODO ANTERIOR */
        V_EMPR_ANT           := RAEP.AEP_EMPR;
        V_ART_ANT            := RAEP.AEP_ART;
        V_TOT_EXIST_ANT      := RAEP.AEP_TOT_EXIST;
        V_COSTO_PROM_LOC_ANT := NVL(RAEP.AEP_COSTO_PROM_LOC, 0);
        V_COSTO_PROM_MON_ANT := NVL(RAEP.AEP_COSTO_PROM_MON, 0);
        EXISTE_PERIODO_SGTE  := FALSE;
      ELSE
        /* SI ES EL PERIODO ACTUAL */
        IF V_EMPR_ANT IS NOT NULL THEN
          /* SI EXISTI? REGISTRO DEL PERIODO ANTERIOR */
          IF RAEP.AEP_EMPR = V_EMPR_ANT AND RAEP.AEP_ART = V_ART_ANT THEN
            /* SI EL REGISTRO DEL PERIODO ANTERIOR CORRESPONDE A LA
            MISMA EMPRESA Y ART?CULO */
            EXISTE_PERIODO_SGTE := TRUE;
            V_COSTO_INI_LOC     := 0;
          
            /* SI LA EXISTENCIA INICIAL ES MENOR O IGUAL A CERO, EL COSTO INICIAL
            DEBE SER CERO PARA QUE NO INFLUYA EN EL CALCULO DEL COSTO PROMEDIO
            ACTUAL. PARA LOS INFORMES DE BALANCES EL COSTO SE SACA DEL COSTO
            PROMEDIO INICIAL */
          
            IF V_TOT_EXIST_ANT <> 0 THEN
              V_COSTO_INI_LOC := ROUND(V_TOT_EXIST_ANT *
                                       V_COSTO_PROM_LOC_ANT,
                                       V_CANT_DECIMALES_LOC);
            END IF;
            V_COSTO_INI_MON := 0;
            IF V_TOT_EXIST_ANT <> 0 THEN
              V_COSTO_INI_MON := ROUND(V_TOT_EXIST_ANT *
                                       V_COSTO_PROM_MON_ANT,
                                       4);
            END IF;
          
            V_MENSAJE := NULL;
            IF RAEP.AEP_EXIST_INI <> V_TOT_EXIST_ANT THEN
              V_MENSAJE := 'Error 401: en existencia inicial del periodo ' ||
                           TO_CHAR(I_PERIODO) || ' empresa ' ||
                           TO_CHAR(RAEP.AEP_EMPR) || ' articulo ' ||
                           TO_CHAR(RAEP.AEP_ART) || '. Es:' ||
                           TO_CHAR(RAEP.AEP_EXIST_INI) || '. Debe ser:' ||
                           TO_CHAR(V_TOT_EXIST_ANT) || '. Desea corregir?';
            END IF;
            IF RAEP.AEP_COSTO_INI_LOC > V_COSTO_INI_LOC THEN
              V_DIFERENCIA := RAEP.AEP_COSTO_INI_LOC - V_COSTO_INI_LOC;
            ELSE
              V_DIFERENCIA := V_COSTO_INI_LOC - RAEP.AEP_COSTO_INI_LOC;
            END IF;
            IF V_DIFERENCIA > 10 THEN
              V_MENSAJE := 'Error 402: en costo inicial local del periodo ' ||
                           TO_CHAR(I_PERIODO) || ' empresa ' ||
                           TO_CHAR(RAEP.AEP_EMPR) || ' articulo ' ||
                           TO_CHAR(RAEP.AEP_ART) || '. Es:' ||
                           TO_CHAR(RAEP.AEP_COSTO_INI_LOC) || '. Debe ser:' ||
                           TO_CHAR(V_COSTO_INI_LOC) || '. Desea corregir?';
            END IF;
            IF RAEP.AEP_COSTO_INI_MON > V_COSTO_INI_MON THEN
              V_DIFERENCIA := RAEP.AEP_COSTO_INI_MON - V_COSTO_INI_MON;
            ELSE
              V_DIFERENCIA := V_COSTO_INI_MON - RAEP.AEP_COSTO_INI_MON;
            END IF;
            IF V_DIFERENCIA > 0.01 THEN
              V_MENSAJE := 'Error 403: en costo inicial U$ del periodo ' ||
                           TO_CHAR(I_PERIODO) || ' empresa ' ||
                           TO_CHAR(RAEP.AEP_EMPR) || ' articulo ' ||
                           TO_CHAR(RAEP.AEP_ART) || '. Es:' ||
                           TO_CHAR(RAEP.AEP_COSTO_INI_MON) || '. Debe ser:' ||
                           TO_CHAR(V_COSTO_INI_MON) || '. Desea corregir?';
            END IF;
            --
            IF RAEP.AEP_COSTO_PROM_INI_MON > V_COSTO_PROM_MON_ANT THEN
              V_DIFERENCIA := RAEP.AEP_COSTO_PROM_INI_MON -
                              V_COSTO_PROM_MON_ANT;
            ELSE
              V_DIFERENCIA := V_COSTO_PROM_MON_ANT -
                              RAEP.AEP_COSTO_PROM_INI_MON;
            END IF;
            IF V_DIFERENCIA > 0.02 THEN
              V_MENSAJE := 'Error 404: en costo promedio inicial U$ del periodo ' ||
                           TO_CHAR(I_PERIODO) || ' empresa ' ||
                           TO_CHAR(RAEP.AEP_EMPR) || ' articulo ' ||
                           TO_CHAR(RAEP.AEP_ART) || '. Es:' ||
                           TO_CHAR(RAEP.AEP_COSTO_PROM_INI_MON) ||
                           '. Debe ser:' || TO_CHAR(V_COSTO_PROM_MON_ANT) ||
                           '. Desea corregir?';
            END IF;
            --
            IF RAEP.AEP_COSTO_PROM_INI_LOC > V_COSTO_PROM_LOC_ANT THEN
              V_DIFERENCIA := RAEP.AEP_COSTO_PROM_INI_LOC -
                              V_COSTO_PROM_LOC_ANT;
            ELSE
              V_DIFERENCIA := V_COSTO_PROM_LOC_ANT -
                              RAEP.AEP_COSTO_PROM_INI_LOC;
            END IF;
            IF V_DIFERENCIA > 0.01 THEN
              V_MENSAJE := 'Error 405: en costo promedio inicial local del periodo ' ||
                           TO_CHAR(I_PERIODO) || ' empresa ' ||
                           TO_CHAR(RAEP.AEP_EMPR) || ' articulo ' ||
                           TO_CHAR(RAEP.AEP_ART) || '. Es:' ||
                           TO_CHAR(RAEP.AEP_COSTO_PROM_INI_LOC) ||
                           '. Debe ser:' || TO_CHAR(V_COSTO_PROM_LOC_ANT) ||
                           '. Desea corregir?';
            END IF;
            --
            IF V_MENSAJE IS NOT NULL THEN
              UPDATE STK_ART_EMPR_PERI
                 SET AEP_EXIST_INI          = V_TOT_EXIST_ANT,
                     AEP_COSTO_INI_LOC      = V_COSTO_INI_LOC,
                     AEP_COSTO_INI_MON      = V_COSTO_INI_MON,
                     AEP_COSTO_PROM_INI_LOC = V_COSTO_PROM_LOC_ANT,
                     AEP_COSTO_PROM_INI_MON = V_COSTO_PROM_MON_ANT
               WHERE CURRENT OF AEP_CUR;
            END IF;
          END IF;
        END IF;
        V_EMPR_ANT := NULL; --PARA VERIFICAR SI ES QUE HUBO UN PERIODO ANTERIOR
      END IF;
    END LOOP;
  
    --SI PARA EL ?LTIMO REGISTRO NO EXISTE PERIODO SIGUIENTE
    IF NOT EXISTE_PERIODO_SGTE THEN
      --INGRESAR UN REGISTRO DE PERIODO SIGUIENTE
      V_COSTO_INI_LOC := 0;
      IF V_TOT_EXIST_ANT <> 0 THEN
        V_COSTO_INI_LOC := ROUND(V_TOT_EXIST_ANT * V_COSTO_PROM_LOC_ANT,
                                 V_CANT_DECIMALES_LOC);
      END IF;
      V_COSTO_INI_MON := 0;
      IF V_TOT_EXIST_ANT <> 0 THEN
        V_COSTO_INI_MON := ROUND(V_TOT_EXIST_ANT * V_COSTO_PROM_MON_ANT, 4);
      END IF;
    
      IF V_RESPUESTA = 'S' THEN
        INSERT INTO STK_ART_EMPR_PERI
          (AEP_PERIODO,
           AEP_EMPR,
           AEP_ART,
           AEP_TOT_EXIST,
           AEP_EXIST_INI,
           AEP_COSTO_INI_LOC,
           AEP_COSTO_INI_MON,
           AEP_COSTO_PROM_INI_LOC,
           AEP_COSTO_PROM_INI_MON,
           AEP_COSTO_PROM_LOC,
           AEP_COSTO_PROM_MON,
           AEP_EC_COMPRA,
           AEP_EL_COMPRA,
           AEP_EM_COMPRA,
           AEP_SC_DEV_COMPRA,
           AEP_SL_DEV_COMPRA,
           AEP_SM_DEV_COMPRA,
           AEP_SC_VTA,
           AEP_SL_VTA,
           AEP_SM_VTA,
           AEP_EC_DEV_VTA,
           AEP_EL_DEV_VTA,
           AEP_EM_DEV_VTA,
           AEP_EC_PROD,
           AEP_EL_PROD,
           AEP_EM_PROD,
           AEP_SC_PROD,
           AEP_EC_KIT,
           AEP_EL_KIT,
           AEP_EM_KIT,
           AEP_SC_KIT,
           AEP_SL_KIT,
           AEP_SM_KIT,
           AEP_SC_PERDIDA,
           AEP_SC_CONS,
           AEP_C_DIF_INV,
           AEP_C_TRANSF,
           AEP_C_REM,
           AEP_EC_ARM_DES,
           AEP_EL_ARM_DES,
           AEP_EM_ARM_DES,
           AEP_SC_ARM_DES,
           AEP_SL_ARM_DES,
           AEP_SM_ARM_DES,
           AEP_EL_DEV_PROD,
           AEP_EM_DEV_PROD,
           AEP_EC_DEV_PROD)
        VALUES
          (I_PERIODO,
           V_EMPR_ANT,
           V_ART_ANT,
           V_TOT_EXIST_ANT,
           V_TOT_EXIST_ANT,
           V_COSTO_INI_LOC,
           V_COSTO_INI_MON,
           NVL(V_COSTO_PROM_LOC_ANT, 0),
           V_COSTO_PROM_MON_ANT,
           NVL(V_COSTO_PROM_LOC_ANT, 0),
           V_COSTO_PROM_MON_ANT,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0);
      END IF;
    END IF;
    COMMIT;
  EXCEPTION
    WHEN SALIR THEN
      NULL;
  END PP_COMPARAR_CON_PERI_ANT;

  PROCEDURE PP_COMPARAR_CON_PERI_ANT_OPER(I_EMPRESA IN NUMBER,
                                          I_PERIODO IN NUMBER,
                                          I_ART     IN NUMBER DEFAULT NULL) IS
  
    CURSOR AEP_CUR(PERIANT IN NUMBER, PERIACT IN NUMBER) IS
      SELECT *
        FROM STK_ART_EMPR_PERI, STK_ARTICULO
       WHERE AEP_EMPR = I_EMPRESA
         AND ART_CODIGO = AEP_ART
         AND ART_EMPR = AEP_EMPR
         AND (ART_TIPO <> 4 or art_ind_inv_valor_costo = 'S')
     --    AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
         AND AEP_PERIODO BETWEEN PERIANT AND PERIACT
       ORDER BY AEP_EMPR, AEP_ART, AEP_PERIODO
         FOR UPDATE OF AEP_EXIST_INI_OPER, AEP_COSTO_INI_LOC_OPER, AEP_COSTO_PROM_INI_LOC_OPER, AEP_COSTO_INI_MON_OPER, AEP_COSTO_PROM_INI_MON_OPER;
  
    SALIR EXCEPTION;
    PERIANT              NUMBER;
    V_EMPR_ANT           STK_ART_EMPR_PERI.AEP_EMPR%TYPE;
    V_ART_ANT            STK_ART_EMPR_PERI.AEP_ART%TYPE;
    V_TOT_EXIST_ANT      STK_ART_EMPR_PERI.AEP_TOT_EXIST_OPER%TYPE;
    V_COSTO_PROM_LOC_ANT STK_ART_EMPR_PERI.AEP_COSTO_PROM_LOC_OPER%TYPE;
    V_COSTO_INI_LOC      STK_ART_EMPR_PERI.AEP_COSTO_INI_LOC_OPER%TYPE;
    V_COSTO_PROM_MON_ANT STK_ART_EMPR_PERI.AEP_COSTO_PROM_MON_OPER%TYPE;
    V_COSTO_INI_MON      STK_ART_EMPR_PERI.AEP_COSTO_INI_MON_OPER%TYPE;
  
    -- V_MON_DEC_IMP_ANT    GEN_MONEDA.MON_DEC_IMP%TYPE;
  
    V_MENSAJE            VARCHAR2(200);
    V_DIFERENCIA         NUMBER;
    V_RESPUESTA          VARCHAR2(1) := 'S';
    V_CANT_DECIMALES_LOC NUMBER := 0;
    EXISTE_PERIODO_SGTE  BOOLEAN := TRUE;
  
  BEGIN
  
    SELECT MAX(PERI_CODIGO)
      INTO PERIANT
      FROM STK_PERIODO
     WHERE PERI_CODIGO < I_PERIODO
       AND PERI_EMPR = I_EMPRESA;
    IF PERIANT IS NULL THEN
      RAISE SALIR;
    END IF;
  
    FOR RAEP IN AEP_CUR(PERIANT, I_PERIODO) LOOP
      V_MENSAJE := 'Emp:' || TO_CHAR(RAEP.AEP_EMPR) || 'Art:' ||
                   TO_CHAR(RAEP.AEP_ART) || 'Per:' ||
                   TO_CHAR(RAEP.AEP_PERIODO);
    
      IF RAEP.AEP_PERIODO = PERIANT THEN
        IF NOT EXISTE_PERIODO_SGTE THEN
          --INGRESAR UN REGISTRO DE PERIODO SIGUIENTE
          V_COSTO_INI_LOC := 0;
          IF V_TOT_EXIST_ANT <> 0 THEN
            V_COSTO_INI_LOC := ROUND(V_TOT_EXIST_ANT *
                                     NVL(V_COSTO_PROM_LOC_ANT, 0),
                                     V_CANT_DECIMALES_LOC);
          END IF;
          V_COSTO_INI_MON := 0;
          IF V_TOT_EXIST_ANT <> 0 THEN
            V_COSTO_INI_MON := ROUND(V_TOT_EXIST_ANT * V_COSTO_PROM_MON_ANT,
                                     4);
          END IF;
        
          IF V_RESPUESTA = 'S' THEN
            INSERT INTO STK_ART_EMPR_PERI
              (AEP_PERIODO,
               AEP_EMPR,
               AEP_ART,
               AEP_TOT_EXIST,
               AEP_EXIST_INI,
               AEP_COSTO_INI_LOC,
               AEP_COSTO_INI_MON,
               AEP_COSTO_PROM_INI_LOC,
               AEP_COSTO_PROM_INI_MON,
               AEP_COSTO_PROM_LOC,
               AEP_COSTO_PROM_MON,
               AEP_EC_COMPRA,
               AEP_EL_COMPRA,
               AEP_EM_COMPRA,
               AEP_SC_DEV_COMPRA,
               AEP_SL_DEV_COMPRA,
               AEP_SM_DEV_COMPRA,
               AEP_SC_VTA,
               AEP_SL_VTA,
               AEP_SM_VTA,
               AEP_EC_DEV_VTA,
               AEP_EL_DEV_VTA,
               AEP_EM_DEV_VTA,
               AEP_EC_PROD,
               AEP_EL_PROD,
               AEP_EM_PROD,
               AEP_SC_PROD,
               AEP_EC_KIT,
               AEP_EL_KIT,
               AEP_EM_KIT,
               AEP_SC_KIT,
               AEP_SL_KIT,
               AEP_SM_KIT,
               AEP_SC_PERDIDA,
               AEP_SC_CONS,
               AEP_C_DIF_INV,
               AEP_C_TRANSF,
               AEP_C_REM,
               AEP_EC_ARM_DES,
               AEP_EL_ARM_DES,
               AEP_EM_ARM_DES,
               AEP_SC_ARM_DES,
               AEP_SL_ARM_DES,
               AEP_SM_ARM_DES,
               AEP_EL_DEV_PROD,
               AEP_EM_DEV_PROD,
               AEP_EC_DEV_PROD,
               AEP_TOT_EXIST_OPER,
               AEP_EXIST_INI_OPER,
               AEP_COSTO_INI_LOC_OPER,
               AEP_COSTO_INI_MON_OPER,
               AEP_COSTO_PROM_INI_LOC_OPER,
               AEP_COSTO_PROM_INI_MON_OPER,
               AEP_COSTO_PROM_LOC_OPER,
               AEP_COSTO_PROM_MON_OPER)
            VALUES
              (I_PERIODO,
               V_EMPR_ANT,
               V_ART_ANT,
               V_TOT_EXIST_ANT,
               V_TOT_EXIST_ANT,
               V_COSTO_INI_LOC,
               V_COSTO_INI_MON,
               NVL(V_COSTO_PROM_LOC_ANT, 0),
               V_COSTO_PROM_MON_ANT,
               NVL(V_COSTO_PROM_LOC_ANT, 0),
               V_COSTO_PROM_MON_ANT,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               V_TOT_EXIST_ANT,
               V_TOT_EXIST_ANT,
               V_COSTO_INI_LOC,
               V_COSTO_INI_MON,
               V_COSTO_PROM_LOC_ANT,
               V_COSTO_PROM_MON_ANT,
               V_COSTO_PROM_LOC_ANT,
               V_COSTO_PROM_MON_ANT);
          END IF;
        END IF;
      END IF;
    
      IF RAEP.AEP_PERIODO = PERIANT THEN
        V_EMPR_ANT           := RAEP.AEP_EMPR;
        V_ART_ANT            := RAEP.AEP_ART;
        V_TOT_EXIST_ANT      := RAEP.AEP_TOT_EXIST_OPER;
        V_COSTO_PROM_LOC_ANT := RAEP.AEP_COSTO_PROM_LOC_OPER;
        V_COSTO_PROM_MON_ANT := RAEP.AEP_COSTO_PROM_MON_OPER;
        EXISTE_PERIODO_SGTE  := FALSE;
      ELSE
        IF V_EMPR_ANT IS NOT NULL THEN
        
          IF RAEP.AEP_EMPR = V_EMPR_ANT AND RAEP.AEP_ART = V_ART_ANT THEN
            EXISTE_PERIODO_SGTE := TRUE;
            V_COSTO_INI_LOC     := 0;
          
            IF V_TOT_EXIST_ANT <> 0 THEN
              V_COSTO_INI_LOC := ROUND(V_TOT_EXIST_ANT *
                                       V_COSTO_PROM_LOC_ANT,
                                       V_CANT_DECIMALES_LOC);
            END IF;
          
            V_COSTO_INI_MON := 0;
          
            IF V_TOT_EXIST_ANT <> 0 THEN
              V_COSTO_INI_MON := ROUND(V_TOT_EXIST_ANT *
                                       V_COSTO_PROM_MON_ANT,
                                       4);
            END IF;
          
            V_MENSAJE := NULL;
            IF RAEP.AEP_EXIST_INI_OPER <> V_TOT_EXIST_ANT THEN
              V_MENSAJE := 'Error 401: en existencia inicial del periodo ' ||
                           TO_CHAR(I_PERIODO) || ' empresa ' ||
                           TO_CHAR(RAEP.AEP_EMPR) || ' articulo ' ||
                           TO_CHAR(RAEP.AEP_ART) || '. Es:' ||
                           TO_CHAR(RAEP.AEP_EXIST_INI_OPER) ||
                           '. Debe ser:' || TO_CHAR(V_TOT_EXIST_ANT) ||
                           '. Desea corregir?';
            END IF;
            IF RAEP.AEP_COSTO_INI_LOC_OPER > V_COSTO_INI_LOC THEN
              V_DIFERENCIA := RAEP.AEP_COSTO_INI_LOC_OPER - V_COSTO_INI_LOC;
            ELSE
              V_DIFERENCIA := V_COSTO_INI_LOC - RAEP.AEP_COSTO_INI_LOC_OPER;
            END IF;
            IF V_DIFERENCIA > 10 THEN
              V_MENSAJE := 'Error 402: en costo inicial local del periodo ' ||
                           TO_CHAR(I_PERIODO) || ' empresa ' ||
                           TO_CHAR(RAEP.AEP_EMPR) || ' articulo ' ||
                           TO_CHAR(RAEP.AEP_ART) || '. Es:' ||
                           TO_CHAR(RAEP.AEP_COSTO_INI_LOC_OPER) ||
                           '. Debe ser:' || TO_CHAR(V_COSTO_INI_LOC) ||
                           '. Desea corregir?';
            END IF;
            IF RAEP.AEP_COSTO_INI_MON_OPER > V_COSTO_INI_MON THEN
              V_DIFERENCIA := RAEP.AEP_COSTO_INI_MON_OPER - V_COSTO_INI_MON;
            ELSE
              V_DIFERENCIA := V_COSTO_INI_MON - RAEP.AEP_COSTO_INI_MON_OPER;
            END IF;
            IF V_DIFERENCIA > 0.01 THEN
              V_MENSAJE := 'Error 403: en costo inicial U$ del periodo ' ||
                           TO_CHAR(I_PERIODO) || ' empresa ' ||
                           TO_CHAR(RAEP.AEP_EMPR) || ' articulo ' ||
                           TO_CHAR(RAEP.AEP_ART) || '. Es:' ||
                           TO_CHAR(RAEP.AEP_COSTO_INI_MON_OPER) ||
                           '. Debe ser:' || TO_CHAR(V_COSTO_INI_MON) ||
                           '. Desea corregir?';
            END IF;
            --
            IF RAEP.AEP_COSTO_PROM_INI_MON_OPER > V_COSTO_PROM_MON_ANT THEN
              V_DIFERENCIA := RAEP.AEP_COSTO_PROM_INI_MON_OPER -
                              V_COSTO_PROM_MON_ANT;
            ELSE
              V_DIFERENCIA := V_COSTO_PROM_MON_ANT -
                              RAEP.AEP_COSTO_PROM_INI_MON_OPER;
            END IF;
            IF V_DIFERENCIA > 0.02 THEN
              V_MENSAJE := 'Error 404: en costo promedio inicial U$ del periodo ' ||
                           TO_CHAR(I_PERIODO) || ' empresa ' ||
                           TO_CHAR(RAEP.AEP_EMPR) || ' articulo ' ||
                           TO_CHAR(RAEP.AEP_ART) || '. Es:' ||
                           TO_CHAR(RAEP.AEP_COSTO_PROM_INI_MON_OPER) ||
                           '. Debe ser:' || TO_CHAR(V_COSTO_PROM_MON_ANT) ||
                           '. Desea corregir?';
            END IF;
            --
            IF RAEP.AEP_COSTO_PROM_INI_LOC > V_COSTO_PROM_LOC_ANT THEN
              V_DIFERENCIA := RAEP.AEP_COSTO_PROM_INI_LOC_OPER -
                              V_COSTO_PROM_LOC_ANT;
            ELSE
              V_DIFERENCIA := V_COSTO_PROM_LOC_ANT -
                              RAEP.AEP_COSTO_PROM_INI_LOC_OPER;
            END IF;
            IF V_DIFERENCIA > 1 THEN
              V_MENSAJE := 'Error 405: en costo promedio inicial local del periodo ' ||
                           TO_CHAR(I_PERIODO) || ' empresa ' ||
                           TO_CHAR(RAEP.AEP_EMPR) || ' articulo ' ||
                           TO_CHAR(RAEP.AEP_ART) || '. Es:' ||
                           TO_CHAR(RAEP.AEP_COSTO_PROM_INI_LOC_OPER) ||
                           '. Debe ser:' || TO_CHAR(V_COSTO_PROM_LOC_ANT) ||
                           '. Desea corregir?';
            END IF;
            --
            IF V_MENSAJE IS NOT NULL THEN
              IF V_RESPUESTA = 'S' THEN
                --  INSERT INTO STKP002_LOG (VFECHA,
                --    VMENSAJE,
                --   VEMPR) VALUES
                --(SYSDATE,'ART:'||RAEP.AEP_ART||' periodo:'||RAEP.AEP_PERIODO||' cant='||V_TOT_EXIST_ANT ,1);
              
                /*UPDATE STK_ART_EMPR_PERI SET
                 AEP_EXIST_INI_OPER = V_TOT_EXIST_ANT,
                 AEP_COSTO_INI_LOC_OPER = V_COSTO_INI_LOC,
                 AEP_COSTO_INI_MON_OPER = V_COSTO_INI_MON,
                 AEP_COSTO_PROM_INI_LOC_OPER = V_COSTO_PROM_LOC_ANT,
                 AEP_COSTO_PROM_INI_MON_OPER = V_COSTO_PROM_MON_ANT
                WHERE CURRENT OF AEP_CUR;*/
                NULL;
              END IF;
            END IF;
          END IF;
        END IF;
        V_EMPR_ANT := NULL; --PARA VERIFICAR SI ES QUE HUBO UN PERIODO ANTERIOR
      END IF;
    END LOOP;
  
    --SI PARA EL ?LTIMO REGISTRO NO EXISTE PERIODO SIGUIENTE
    IF NOT EXISTE_PERIODO_SGTE THEN
      --INGRESAR UN REGISTRO DE PERIODO SIGUIENTE
      V_COSTO_INI_LOC := 0;
      IF V_TOT_EXIST_ANT <> 0 THEN
        V_COSTO_INI_LOC := ROUND(V_TOT_EXIST_ANT * V_COSTO_PROM_LOC_ANT,
                                 V_CANT_DECIMALES_LOC);
      END IF;
      V_COSTO_INI_MON := 0;
      IF V_TOT_EXIST_ANT <> 0 THEN
        V_COSTO_INI_MON := ROUND(V_TOT_EXIST_ANT * V_COSTO_PROM_MON_ANT, 4);
      END IF;
    
      IF V_RESPUESTA = 'S' THEN
      
        INSERT INTO STK_ART_EMPR_PERI
          (AEP_PERIODO,
           AEP_EMPR,
           AEP_ART,
           AEP_TOT_EXIST,
           AEP_EXIST_INI,
           AEP_COSTO_INI_LOC,
           AEP_COSTO_INI_MON,
           AEP_COSTO_PROM_INI_LOC,
           AEP_COSTO_PROM_INI_MON,
           AEP_COSTO_PROM_LOC,
           AEP_COSTO_PROM_MON,
           AEP_EC_COMPRA,
           AEP_EL_COMPRA,
           AEP_EM_COMPRA,
           AEP_SC_DEV_COMPRA,
           AEP_SL_DEV_COMPRA,
           AEP_SM_DEV_COMPRA,
           AEP_SC_VTA,
           AEP_SL_VTA,
           AEP_SM_VTA,
           AEP_EC_DEV_VTA,
           AEP_EL_DEV_VTA,
           AEP_EM_DEV_VTA,
           AEP_EC_PROD,
           AEP_EL_PROD,
           AEP_EM_PROD,
           AEP_SC_PROD,
           AEP_EC_KIT,
           AEP_EL_KIT,
           AEP_EM_KIT,
           AEP_SC_KIT,
           AEP_SL_KIT,
           AEP_SM_KIT,
           AEP_SC_PERDIDA,
           AEP_SC_CONS,
           AEP_C_DIF_INV,
           AEP_C_TRANSF,
           AEP_C_REM,
           AEP_EC_ARM_DES,
           AEP_EL_ARM_DES,
           AEP_EM_ARM_DES,
           AEP_SC_ARM_DES,
           AEP_SL_ARM_DES,
           AEP_SM_ARM_DES,
           AEP_EL_DEV_PROD,
           AEP_EM_DEV_PROD,
           AEP_EC_DEV_PROD,
           AEP_TOT_EXIST_OPER,
           AEP_EXIST_INI_OPER,
           AEP_COSTO_INI_LOC_OPER,
           AEP_COSTO_INI_MON_OPER,
           AEP_COSTO_PROM_INI_LOC_OPER,
           AEP_COSTO_PROM_INI_MON_OPER,
           AEP_COSTO_PROM_LOC_OPER,
           AEP_COSTO_PROM_MON_OPER)
        VALUES
          (I_PERIODO,
           V_EMPR_ANT,
           V_ART_ANT,
           V_TOT_EXIST_ANT,
           V_TOT_EXIST_ANT,
           V_COSTO_INI_LOC,
           V_COSTO_INI_MON,
           V_COSTO_PROM_LOC_ANT,
           V_COSTO_PROM_MON_ANT,
           V_COSTO_PROM_LOC_ANT,
           V_COSTO_PROM_MON_ANT,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           V_TOT_EXIST_ANT,
           V_TOT_EXIST_ANT,
           V_COSTO_INI_LOC,
           V_COSTO_INI_MON,
           V_COSTO_PROM_LOC_ANT,
           V_COSTO_PROM_MON_ANT,
           V_COSTO_PROM_LOC_ANT,
           V_COSTO_PROM_MON_ANT);
      END IF;
    END IF;
    COMMIT;
  EXCEPTION
    WHEN SALIR THEN
      NULL;
  END PP_COMPARAR_CON_PERI_ANT_OPER;

  PROCEDURE PP_VERIF_AEP(I_EMPRESA IN NUMBER,
                         I_FECINI  IN DATE,
                         I_FECFIN  IN DATE,
                         I_PERIODO IN NUMBER,
                         I_ART     IN NUMBER DEFAULT NULL) IS
  
    CURSOR MOVI_CUR IS
      SELECT DOCU_EMPR,
             DETA_ART,
             SUM(DECODE(OPER_DESC, 'COMPRA', DETA_CANT, 0)) EC_COMPRA,
             SUM(DECODE(OPER_DESC, 'COMPRA', DETA_IMP_NETO_LOC, 0)) EL_COMPRA,
             SUM(DECODE(OPER_DESC,
                        'COMPRA',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_COMPRA,
             SUM(DECODE(OPER_DESC, 'DEV_COM', DETA_CANT, 0)) SC_DEV_COMPRA,
             SUM(DECODE(OPER_DESC, 'DEV_COM', DETA_IMP_NETO_LOC, 0)) SL_DEV_COMPRA,
             SUM(DECODE(OPER_DESC,
                        'DEV_COM',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) SM_DEV_COMPRA,
             SUM(DECODE(OPER_DESC, 'VENTA', DETA_CANT, 0)) SC_VTA,
             SUM(DECODE(OPER_DESC, 'VENTA', DETA_IMP_NETO_LOC, 0)) SL_VTA,
             SUM(DECODE(OPER_DESC,
                        'VENTA',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) SM_VTA,
             SUM(DECODE(OPER_DESC, 'DEV_VENTA', DETA_CANT, 0)) EC_DEV_VTA,
             SUM(DECODE(OPER_DESC, 'DEV_VENTA', DETA_IMP_NETO_LOC, 0)) EL_DEV_VTA,
             SUM(DECODE(OPER_DESC,
                        'DEV_VENTA',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_DEV_VTA,
             --OPERACI?N 14. ANULACION DE VENTAS
             
             SUM(DECODE(OPER_DESC, 'ANUL_VENTA', DETA_CANT, 0)) EC_ANUL_VTA,
             SUM(DECODE(OPER_DESC, 'ANUL_VENTA', DETA_IMP_NETO_LOC, 0)) EL_ANUL_VTA,
             SUM(DECODE(OPER_DESC,
                        'ANUL_VENTA',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_ANUL_VTA,
             --OPERACI?N 34. ANULACI?N DE NOTAS DE CREDITOS HILAGRO
             
             SUM(DECODE(OPER_DESC, 'ANUL_NC', DETA_CANT, 0)) SC_ANUL_NC,
             SUM(DECODE(OPER_DESC, 'ANUL_NC', DETA_IMP_NETO_LOC, 0)) SL_ANUL_NC,
             SUM(DECODE(OPER_DESC,
                        'ANUL_NC',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) SM_ANUL_NC,
             --
             SUM(DECODE(OPER_DESC, 'ENT_PROD', DETA_CANT, 0)) EC_PROD,
             SUM(DECODE(OPER_DESC, 'ENT_PROD', DETA_IMP_NETO_LOC, 0)) EL_PROD,
             
             SUM(DECODE(OPER_DESC,
                        'ENT_PROD',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_PROD,
             
             SUM(DECODE(OPER_DESC, 'DEV_PROD', DETA_CANT, 0)) EC_DEV_PROD,
             SUM(DECODE(OPER_DESC, 'DEV_PROD', DETA_IMP_NETO_LOC, 0)) EL_DEV_PROD,
             SUM(DECODE(OPER_DESC,
                        'DEV_PROD',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_DEV_PROD,
             SUM(DECODE(OPER_DESC, 'SAL_PROD', DETA_CANT, 0)) SC_PROD,
             SUM(DECODE(OPER_DESC, 'ENT_KIT', DETA_CANT, 0)) EC_KIT,
             SUM(DECODE(OPER_DESC, 'ENT_KIT', DETA_IMP_NETO_LOC, 0)) EL_KIT,
             SUM(DECODE(OPER_DESC,
                        'ENT_KIT',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_KIT,
             SUM(DECODE(OPER_DESC, 'SAL_KIT', DETA_CANT, 0)) SC_KIT,
             SUM(DECODE(OPER_DESC, 'SAL_KIT', DETA_IMP_NETO_LOC, 0)) SL_KIT,
             SUM(DECODE(OPER_DESC,
                        'SAL_KIT',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) SM_KIT,
             SUM(DECODE(OPER_DESC, 'PERDIDA', DETA_CANT, 0)) SC_PERDIDA,
             SUM(DECODE(OPER_DESC, 'CONSUMO', DETA_CANT, 0)) SC_CONS,
             SUM(DECODE(OPER_DESC, 'DIF_MAS', DETA_CANT, 0)) EC_DIF_MAS,
             SUM(DECODE(OPER_DESC, 'DIF_MEN', DETA_CANT, 0)) SC_DIF_MEN,
             SUM(DECODE(OPER_DESC, 'TRAN_SAL', DETA_CANT, 0)) SC_TRANSF,
             SUM(DECODE(OPER_DESC, 'TRAN_ENT', DETA_CANT, 0)) EC_TRANSF,
             SUM(DECODE(OPER_DESC, 'REMISION', DETA_CANT, 0)) C_REM,
             SUM(DECODE(OPER_DESC, 'ENT_ARM_DES', DETA_CANT, 0)) EC_ARM_DES,
             SUM(DECODE(OPER_DESC, 'ENT_ARM_DES', DETA_IMP_NETO_LOC, 0)) EL_ARM_DES,
             SUM(DECODE(OPER_DESC,
                        'ENT_ARM_DES',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_ARM_DES,
             SUM(DECODE(OPER_DESC, 'SAL_ARM_DES', DETA_CANT, 0)) SC_ARM_DES,
             SUM(DECODE(OPER_DESC, 'SAL_ARM_DES', DETA_IMP_NETO_LOC, 0)) SL_ARM_DES,
             SUM(DECODE(OPER_DESC,
                        'SAL_ARM_DES',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) SM_ARM_DES,
             SUM(DECODE(OPER_ENT_SAL, 'E', DETA_CANT, 0)) C_ENT,
             SUM(DECODE(OPER_ENT_SAL, 'S', DETA_CANT, 0)) C_SAL,
             
             SUM(DECODE(OPER_DESC, 'REVALUO', DETA_IMP_NETO_LOC, 0)) EL_REVALUO,
             SUM(DECODE(OPER_DESC, 'DEPRECIACION', DETA_IMP_NETO_LOC, 0)) SL_DEPREC,
             
             SUM(DECODE(OPER_DESC,
                        'REVALUO',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_REVALUO,
             SUM(DECODE(OPER_DESC,
                        'DEPRECIACION',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) SM_DEPREC
      
        FROM STK_OPERACION,
             STK_DOCUMENTO,
             STK_DOCUMENTO_DET,
             STK_ARTICULO AR
       WHERE OPER_CODIGO = DOCU_CODIGO_OPER
         AND OPER_EMPR = DOCU_EMPR
            
         AND DOCU_CLAVE = DETA_CLAVE_DOC
         AND DOCU_EMPR = DETA_EMPR
            
         AND ART_CODIGO = DETA_ART
         AND ART_EMPR = DETA_EMPR
            
         AND DOCU_EMPR = I_EMPRESA
            
         AND ART_TIPO <> 4
        -- AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
         AND DOCU_FEC_EMIS BETWEEN I_FECINI AND I_FECFIN
       GROUP BY DOCU_EMPR, DETA_ART
       ORDER BY DOCU_EMPR, DETA_ART;
  
    CURSOR AEP_CUR IS
      SELECT AEP_PERIODO,
             AEP_EMPR,
             AEP_ART,
             AEP_TOT_EXIST,
             AEP_EXIST_INI,
             AEP_COSTO_INI_LOC,
             AEP_COSTO_PROM_INI_LOC,
             AEP_COSTO_PROM_LOC,
             AEP_COSTO_INI_MON,
             AEP_COSTO_PROM_INI_MON,
             AEP_COSTO_PROM_MON,
             AEP_EC_COMPRA,
             AEP_EL_COMPRA,
             AEP_EM_COMPRA,
             AEP_SC_DEV_COMPRA,
             AEP_SL_DEV_COMPRA,
             AEP_SM_DEV_COMPRA,
             AEP_SC_VTA,
             AEP_SL_VTA,
             AEP_SM_VTA,
             AEP_EC_DEV_VTA,
             AEP_EL_DEV_VTA,
             AEP_EM_DEV_VTA,
             AEP_EC_PROD,
             AEP_EL_PROD,
             AEP_EM_PROD,
             AEP_SC_PROD,
             AEP_EC_KIT,
             AEP_EL_KIT,
             AEP_EM_KIT,
             AEP_SC_KIT,
             AEP_SC_PERDIDA,
             AEP_SC_CONS,
             AEP_C_DIF_INV,
             AEP_C_TRANSF,
             AEP_C_REM,
             AEP_EC_ARM_DES,
             AEP_EL_ARM_DES,
             AEP_EM_ARM_DES,
             AEP_SC_ARM_DES,
             AEP_SL_ARM_DES,
             AEP_SM_ARM_DES,
             AEP_SL_KIT,
             AEP_SM_KIT,
             AEP_EC_DEV_PROD,
             AEP_EL_DEV_PROD,
             AEP_EM_DEV_PROD
      
        FROM STK_ART_EMPR_PERI, STK_ARTICULO
       WHERE AEP_EMPR = I_EMPRESA
         AND AEP_ART = ART_CODIGO
         AND AEP_EMPR = ART_EMPR
         AND AEP_PERIODO = I_PERIODO
         AND ART_TIPO <> 4
     --    AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
      
       ORDER BY AEP_EMPR, AEP_ART;
    --
    RMOVI MOVI_CUR%ROWTYPE;
    RAEP  AEP_CUR%ROWTYPE;
    --
  
    V_MESSAGE VARCHAR2(200);
  
  BEGIN
  
    V_MESSAGE := 'Verificando: Acumulados de Articulo/Empresa/Periodo(' ||
                 TO_CHAR(I_PERIODO) || ')...';
  
    IF I_EMPRESA in (1,3, 20) THEN
      --CALCULAR COSTO PROMEDIO PONDERADO
      V_MESSAGE := 'Obteniendo Promedio Ponderado';
      BEGIN
        STK_RECALC_CCP(I_PERIODO, I_EMPRESA);
      EXCEPTION
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20020, 'STK_RECALC_CCP = ' || SQLERRM);
      END;
    END IF;
    V_MESSAGE := 'Seleccionando...';
  
    OPEN MOVI_CUR;
    FETCH MOVI_CUR
      INTO RMOVI;
  
    OPEN AEP_CUR;
    FETCH AEP_CUR
      INTO RAEP;
  
    LOOP
    
      V_MESSAGE := 'Empr=' || TO_CHAR(RMOVI.DOCU_EMPR) || ', Art=' ||
                   TO_CHAR(RMOVI.DETA_ART);
    
      EXIT WHEN MOVI_CUR%NOTFOUND OR AEP_CUR%NOTFOUND;
    
      LOOP
        EXIT WHEN MOVI_CUR%NOTFOUND OR(RMOVI.DOCU_EMPR > RAEP.AEP_EMPR OR
                                       (RMOVI.DOCU_EMPR = RAEP.AEP_EMPR AND
                                       RMOVI.DETA_ART >= RAEP.AEP_ART));
        -- ENTRA AQUI SI EXISTEN MOVIMIENTOS SIN MAESTRO
        V_MESSAGE := 'Error 601: Falta reg. en STK_ART_EMPR_PERI' ||
                     ' para Empr=' || TO_CHAR(RMOVI.DOCU_EMPR) || ', Art=' ||
                     TO_CHAR(RMOVI.DETA_ART) || '. Desea corregir?';
      
        BEGIN
        
          INSERT INTO STK_ART_EMPR_PERI
            (AEP_PERIODO,
             AEP_EMPR,
             AEP_ART,
             AEP_TOT_EXIST,
             AEP_EXIST_INI,
             AEP_COSTO_INI_LOC,
             AEP_COSTO_INI_MON,
             AEP_COSTO_PROM_INI_LOC,
             AEP_COSTO_PROM_INI_MON,
             AEP_COSTO_PROM_LOC,
             AEP_COSTO_PROM_MON,
             AEP_EC_COMPRA,
             AEP_EL_COMPRA,
             AEP_EM_COMPRA,
             AEP_SC_DEV_COMPRA,
             AEP_SL_DEV_COMPRA,
             AEP_SM_DEV_COMPRA,
             AEP_SC_VTA,
             AEP_SL_VTA,
             AEP_SM_VTA,
             AEP_EC_DEV_VTA,
             AEP_EL_DEV_VTA,
             AEP_EM_DEV_VTA,
             AEP_EC_PROD,
             AEP_EL_PROD,
             AEP_EM_PROD,
             AEP_SC_PROD,
             AEP_EC_KIT,
             AEP_EL_KIT,
             AEP_EM_KIT,
             AEP_SC_KIT,
             AEP_SL_KIT,
             AEP_SM_KIT,
             AEP_SC_PERDIDA,
             AEP_SC_CONS,
             AEP_C_DIF_INV,
             AEP_C_TRANSF,
             AEP_C_REM,
             AEP_EC_ARM_DES,
             AEP_EL_ARM_DES,
             AEP_EM_ARM_DES,
             AEP_SC_ARM_DES,
             AEP_SL_ARM_DES,
             AEP_SM_ARM_DES,
             AEP_EC_DEV_PROD,
             AEP_EL_DEV_PROD,
             AEP_EM_DEV_PROD)
          VALUES
            (I_PERIODO,
             RMOVI.DOCU_EMPR,
             RMOVI.DETA_ART,
             RMOVI.C_ENT - RMOVI.C_SAL,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             RMOVI.EC_COMPRA,
             RMOVI.EL_COMPRA,
             RMOVI.EM_COMPRA,
             RMOVI.SC_DEV_COMPRA,
             RMOVI.SL_DEV_COMPRA,
             RMOVI.SM_DEV_COMPRA,
             RMOVI.SC_VTA,
             RMOVI.SL_VTA,
             RMOVI.SM_VTA,
             RMOVI.EC_DEV_VTA,
             RMOVI.EL_DEV_VTA,
             RMOVI.EM_DEV_VTA,
             RMOVI.EC_PROD,
             RMOVI.EL_PROD,
             RMOVI.EM_PROD,
             RMOVI.SC_PROD,
             RMOVI.EC_KIT,
             RMOVI.EL_KIT,
             RMOVI.EM_KIT,
             RMOVI.SC_KIT,
             RMOVI.SL_KIT,
             RMOVI.SM_KIT,
             RMOVI.SC_PERDIDA,
             RMOVI.SC_CONS,
             RMOVI.EC_DIF_MAS - RMOVI.SC_DIF_MEN,
             RMOVI.EC_TRANSF - RMOVI.SC_TRANSF,
             RMOVI.C_REM,
             RMOVI.EC_ARM_DES,
             RMOVI.EL_ARM_DES,
             RMOVI.EM_ARM_DES,
             RMOVI.SC_ARM_DES,
             RMOVI.SL_ARM_DES,
             RMOVI.SM_ARM_DES,
             RMOVI.EC_DEV_PROD,
             RMOVI.EL_DEV_PROD,
             RMOVI.EM_DEV_PROD);
        
        EXCEPTION
          WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR(-20011,
                                    'STK_ART_EMPR_PERI = ' || SQLERRM);
        END STK_ART_EMPR_PERI;
      
        FETCH MOVI_CUR
          INTO RMOVI;
      END LOOP;
    
      LOOP
        EXIT WHEN AEP_CUR%NOTFOUND OR(RAEP.AEP_EMPR > RMOVI.DOCU_EMPR OR
                                      (RAEP.AEP_EMPR = RMOVI.DOCU_EMPR AND
                                      RAEP.AEP_ART >= RMOVI.DETA_ART));
        -- ENTRA AQUI SI EXISTEN REG.MAESTROS SIN MOVIMIENTOS
        IF RAEP.AEP_TOT_EXIST <> RAEP.AEP_EXIST_INI OR
           RAEP.AEP_EC_COMPRA <> 0 OR RAEP.AEP_EL_COMPRA <> 0 OR
           RAEP.AEP_EM_COMPRA <> 0 OR RAEP.AEP_SC_DEV_COMPRA <> 0 OR
           RAEP.AEP_SL_DEV_COMPRA <> 0 OR RAEP.AEP_SM_DEV_COMPRA <> 0 OR
           RAEP.AEP_SC_VTA <> 0 OR RAEP.AEP_SL_VTA <> 0 OR
           RAEP.AEP_SM_VTA <> 0 OR RAEP.AEP_EC_DEV_VTA <> 0 OR
           RAEP.AEP_EL_DEV_VTA <> 0 OR RAEP.AEP_EM_DEV_VTA <> 0 OR
           RAEP.AEP_EC_PROD <> 0 OR RAEP.AEP_EL_PROD <> 0 OR
           RAEP.AEP_EM_PROD <> 0 OR RAEP.AEP_EC_DEV_PROD <> 0 OR
           RAEP.AEP_EL_DEV_PROD <> 0 OR RAEP.AEP_EM_DEV_PROD <> 0 OR
           RAEP.AEP_SC_PROD <> 0 OR RAEP.AEP_EC_KIT <> 0 OR
           RAEP.AEP_EL_KIT <> 0 OR RAEP.AEP_EM_KIT <> 0 OR
           RAEP.AEP_SC_KIT <> 0 OR RAEP.AEP_SL_KIT <> 0 OR
           RAEP.AEP_SM_KIT <> 0 OR RAEP.AEP_SC_PERDIDA <> 0 OR
           RAEP.AEP_SC_CONS <> 0 OR RAEP.AEP_C_DIF_INV <> 0 OR
           RAEP.AEP_C_TRANSF <> 0 OR RAEP.AEP_C_REM <> 0 OR
           RAEP.AEP_EC_ARM_DES <> 0 OR RAEP.AEP_EL_ARM_DES <> 0 OR
           RAEP.AEP_EM_ARM_DES <> 0 OR RAEP.AEP_SC_ARM_DES <> 0 OR
           RAEP.AEP_SL_ARM_DES <> 0 OR RAEP.AEP_SM_ARM_DES <> 0 THEN
          V_MESSAGE := 'Error 602: Cantidades incorrectas' ||
                       ' en STK_ART_EMPR_PERI para ' || 'Empr=' ||
                       TO_CHAR(RAEP.AEP_EMPR) || ', Art=' ||
                       TO_CHAR(RAEP.AEP_ART) || '. Desea corregir?';
        
          BEGIN
          
            UPDATE STK_ART_EMPR_PERI
               SET AEP_EC_COMPRA     = 0,
                   AEP_EL_COMPRA     = 0,
                   AEP_EM_COMPRA     = 0,
                   AEP_SC_DEV_COMPRA = 0,
                   AEP_SL_DEV_COMPRA = 0,
                   AEP_SM_DEV_COMPRA = 0,
                   AEP_SC_VTA        = 0,
                   AEP_SL_VTA        = 0,
                   AEP_SM_VTA        = 0,
                   AEP_EC_DEV_VTA    = 0,
                   AEP_EL_DEV_VTA    = 0,
                   AEP_EM_DEV_VTA    = 0,
                   AEP_EC_PROD       = 0,
                   AEP_EL_PROD       = 0,
                   AEP_EM_PROD       = 0,
                   AEP_EC_DEV_PROD   = 0,
                   AEP_EL_DEV_PROD   = 0,
                   AEP_EM_DEV_PROD   = 0,
                   AEP_SC_PROD       = 0,
                   AEP_EC_KIT        = 0,
                   AEP_EL_KIT        = 0,
                   AEP_EM_KIT        = 0,
                   AEP_SC_KIT        = 0,
                   AEP_SL_KIT        = 0,
                   AEP_SM_KIT        = 0,
                   AEP_SC_PERDIDA    = 0,
                   AEP_SC_CONS       = 0,
                   AEP_C_DIF_INV     = 0,
                   AEP_C_TRANSF      = 0,
                   AEP_EC_ARM_DES    = 0,
                   AEP_EL_ARM_DES    = 0,
                   AEP_EM_ARM_DES    = 0,
                   AEP_SC_ARM_DES    = 0,
                   AEP_SL_ARM_DES    = 0,
                   AEP_SM_ARM_DES    = 0,
                   AEP_C_REM         = 0
             WHERE AEP_PERIODO = I_PERIODO
               AND AEP_EMPR = RAEP.AEP_EMPR
               AND AEP_ART = RAEP.AEP_ART;
          
          EXCEPTION
            WHEN OTHERS THEN
              RAISE_APPLICATION_ERROR(-20012,
                                      'UPDATE STK_ART_EMPR_PERI = ' ||
                                      SQLERRM);
          END;
        
        END IF;
        FETCH AEP_CUR
          INTO RAEP;
      END LOOP;
    
      EXIT WHEN MOVI_CUR%NOTFOUND OR AEP_CUR%NOTFOUND;
      IF RMOVI.DOCU_EMPR = RAEP.AEP_EMPR AND RMOVI.DETA_ART = RAEP.AEP_ART THEN
        -- ENTRA AQUI SI EXISTEN MOVIMIENTOS Y SU REG.MAESTRO
      
        IF RAEP.AEP_TOT_EXIST <>
           (RAEP.AEP_EXIST_INI + RMOVI.C_ENT - RMOVI.C_SAL) OR
           RAEP.AEP_EC_COMPRA <> RMOVI.EC_COMPRA OR
           RAEP.AEP_EL_COMPRA <> RMOVI.EL_COMPRA OR
           RAEP.AEP_EM_COMPRA <> RMOVI.EM_COMPRA OR
           RAEP.AEP_SC_DEV_COMPRA <> RMOVI.SC_DEV_COMPRA OR
           RAEP.AEP_SL_DEV_COMPRA <> RMOVI.SL_DEV_COMPRA OR
           RAEP.AEP_SM_DEV_COMPRA <> RMOVI.SM_DEV_COMPRA OR
           RAEP.AEP_SC_VTA <> RMOVI.SC_VTA OR
           RAEP.AEP_SL_VTA <> RMOVI.SL_VTA OR
           RAEP.AEP_SM_VTA <> RMOVI.SM_VTA OR
           RAEP.AEP_EC_DEV_VTA <> RMOVI.EC_DEV_VTA OR
           RAEP.AEP_EL_DEV_VTA <> RMOVI.EL_DEV_VTA OR
           RAEP.AEP_EM_DEV_VTA <> RMOVI.EM_DEV_VTA OR
           RAEP.AEP_EC_PROD <> RMOVI.EC_PROD OR
           RAEP.AEP_EL_PROD <> RMOVI.EL_PROD OR
           RAEP.AEP_EM_PROD <> RMOVI.EM_PROD OR
           RAEP.AEP_EC_DEV_PROD <> RMOVI.EC_DEV_PROD OR
           RAEP.AEP_EL_DEV_PROD <> RMOVI.EL_DEV_PROD OR
           RAEP.AEP_EM_DEV_PROD <> RMOVI.EM_DEV_PROD OR
           RAEP.AEP_SC_PROD <> RMOVI.SC_PROD OR
           RAEP.AEP_EC_KIT <> RMOVI.EC_KIT OR
           RAEP.AEP_EL_KIT <> RMOVI.EL_KIT OR
           RAEP.AEP_EM_KIT <> RMOVI.EM_KIT OR
           RAEP.AEP_SC_KIT <> RMOVI.SC_KIT OR
           RAEP.AEP_SL_KIT <> RMOVI.SL_KIT OR
           RAEP.AEP_SM_KIT <> RMOVI.SM_KIT OR
           RAEP.AEP_SC_PERDIDA <> RMOVI.SC_PERDIDA OR
           RAEP.AEP_SC_CONS <> RMOVI.SC_CONS OR
           RAEP.AEP_C_DIF_INV <> (RMOVI.EC_DIF_MAS - RMOVI.SC_DIF_MEN) OR
           RAEP.AEP_C_TRANSF <> (RMOVI.EC_TRANSF - RMOVI.SC_TRANSF) OR
           RAEP.AEP_C_REM <> RMOVI.C_REM OR
           RAEP.AEP_EC_ARM_DES <> RMOVI.EC_ARM_DES OR
           RAEP.AEP_EL_ARM_DES <> RMOVI.EL_ARM_DES OR
           RAEP.AEP_EM_ARM_DES <> RMOVI.EM_ARM_DES OR
           RAEP.AEP_SC_ARM_DES <> RMOVI.SC_ARM_DES OR
           RAEP.AEP_SL_ARM_DES <> RMOVI.SL_ARM_DES OR
           RAEP.AEP_SM_ARM_DES <> RMOVI.SM_ARM_DES THEN
          -- ACTUALIZAR SI HAY DIFERENCIAS ENTRE MOV.Y MAESTRO
          V_MESSAGE := 'Error 603: En STK_ART_EMPR_PERI' || ' para Empr=' ||
                       TO_CHAR(RAEP.AEP_EMPR) || ', Art=' ||
                       TO_CHAR(RAEP.AEP_ART) || '. Desea corregir?';
        
          BEGIN
          
            UPDATE STK_ART_EMPR_PERI
               SET AEP_TOT_EXIST    =
                   (NVL(RAEP.AEP_EXIST_INI, 0) + NVL(RMOVI.C_ENT, 0) -
                   NVL(RMOVI.C_SAL, 0)),
                   AEP_EC_COMPRA     = RMOVI.EC_COMPRA,
                   AEP_EL_COMPRA     = RMOVI.EL_COMPRA,
                   AEP_EM_COMPRA     = RMOVI.EM_COMPRA,
                   AEP_SC_DEV_COMPRA = RMOVI.SC_DEV_COMPRA,
                   AEP_SL_DEV_COMPRA = RMOVI.SL_DEV_COMPRA,
                   AEP_SM_DEV_COMPRA = RMOVI.SM_DEV_COMPRA,
                   AEP_SC_VTA        = RMOVI.SC_VTA,
                   AEP_SL_VTA        = RMOVI.SL_VTA,
                   AEP_SM_VTA        = RMOVI.SM_VTA,
                   AEP_EC_DEV_VTA    = RMOVI.EC_DEV_VTA,
                   AEP_EL_DEV_VTA    = RMOVI.EL_DEV_VTA,
                   AEP_EM_DEV_VTA    = RMOVI.EM_DEV_VTA,
                   AEP_EC_PROD       = RMOVI.EC_PROD,
                   AEP_EL_PROD       = NVL(RMOVI.EL_PROD, 0),
                   AEP_EM_PROD       = NVL(RMOVI.EM_PROD, 0),
                   AEP_EC_DEV_PROD   = RMOVI.EC_DEV_PROD,
                   AEP_EL_DEV_PROD   = RMOVI.EL_DEV_PROD,
                   AEP_EM_DEV_PROD   = RMOVI.EM_DEV_PROD,
                   AEP_SC_PROD       = RMOVI.SC_PROD,
                   AEP_EC_KIT        = RMOVI.EC_KIT,
                   AEP_EL_KIT        = RMOVI.EL_KIT,
                   AEP_EM_KIT        = RMOVI.EM_KIT,
                   AEP_SC_KIT        = RMOVI.SC_KIT,
                   AEP_SL_KIT        = RMOVI.SL_KIT,
                   AEP_SM_KIT        = RMOVI.SM_KIT,
                   AEP_SC_PERDIDA    = RMOVI.SC_PERDIDA,
                   AEP_SC_CONS       = RMOVI.SC_CONS,
                   AEP_C_DIF_INV    =
                   (RMOVI.EC_DIF_MAS - RMOVI.SC_DIF_MEN),
                   AEP_C_TRANSF     =
                   (RMOVI.EC_TRANSF - RMOVI.SC_TRANSF),
                   AEP_C_REM         = RMOVI.C_REM,
                   AEP_EC_ARM_DES    = RMOVI.EC_ARM_DES,
                   AEP_EL_ARM_DES    = RMOVI.EL_ARM_DES,
                   AEP_EM_ARM_DES    = RMOVI.EM_ARM_DES,
                   AEP_SC_ARM_DES    = RMOVI.SC_ARM_DES,
                   AEP_SL_ARM_DES    = RMOVI.SL_ARM_DES,
                   AEP_SM_ARM_DES    = RMOVI.SM_ARM_DES
             WHERE AEP_PERIODO = I_PERIODO
               AND AEP_EMPR = RAEP.AEP_EMPR
               AND AEP_ART = RAEP.AEP_ART;
          
          EXCEPTION
            WHEN OTHERS THEN
              RAISE_APPLICATION_ERROR(-20014, SQLERRM);
          END;
        END IF;
      
        FETCH MOVI_CUR
          INTO RMOVI;
        FETCH AEP_CUR
          INTO RAEP;
      END IF;
    
    END LOOP;
  
    IF MOVI_CUR%FOUND AND AEP_CUR%NOTFOUND THEN
      LOOP
        EXIT WHEN MOVI_CUR%NOTFOUND;
        -- ENTRA AQUI SI EXISTEN MOVIMIENTOS SIN MAESTRO
        V_MESSAGE := 'Error 604: Falta reg. en STK_ART_EMPR_PERI para ' ||
                     'Empr=' || TO_CHAR(RMOVI.DOCU_EMPR) || ', Art=' ||
                     TO_CHAR(RMOVI.DETA_ART) || '. Desea corregir?';
      
        BEGIN
        
          INSERT INTO STK_ART_EMPR_PERI
            (AEP_PERIODO,
             AEP_EMPR,
             AEP_ART,
             AEP_TOT_EXIST,
             AEP_EXIST_INI,
             AEP_COSTO_INI_LOC,
             AEP_COSTO_INI_MON,
             AEP_COSTO_PROM_INI_LOC,
             AEP_COSTO_PROM_INI_MON,
             AEP_COSTO_PROM_LOC,
             AEP_COSTO_PROM_MON,
             AEP_EC_COMPRA,
             AEP_EL_COMPRA,
             AEP_EM_COMPRA,
             AEP_SC_DEV_COMPRA,
             AEP_SL_DEV_COMPRA,
             AEP_SM_DEV_COMPRA,
             AEP_SC_VTA,
             AEP_SL_VTA,
             AEP_SM_VTA,
             AEP_EC_DEV_VTA,
             AEP_EL_DEV_VTA,
             AEP_EM_DEV_VTA,
             AEP_EC_PROD,
             AEP_EL_PROD,
             AEP_EM_PROD,
             AEP_SC_PROD,
             AEP_EC_KIT,
             AEP_EL_KIT,
             AEP_EM_KIT,
             AEP_SC_KIT,
             AEP_SL_KIT,
             AEP_SM_KIT,
             AEP_SC_PERDIDA,
             AEP_SC_CONS,
             AEP_C_DIF_INV,
             AEP_C_TRANSF,
             AEP_C_REM,
             AEP_EC_ARM_DES,
             AEP_EL_ARM_DES,
             AEP_EM_ARM_DES,
             AEP_SC_ARM_DES,
             AEP_SL_ARM_DES,
             AEP_SM_ARM_DES,
             AEP_EC_DEV_PROD,
             AEP_EL_DEV_PROD,
             AEP_EM_DEV_PROD)
          VALUES
            (I_PERIODO,
             RMOVI.DOCU_EMPR,
             RMOVI.DETA_ART,
             RMOVI.C_ENT - RMOVI.C_SAL,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             RMOVI.EC_COMPRA,
             RMOVI.EL_COMPRA,
             RMOVI.EM_COMPRA,
             RMOVI.SC_DEV_COMPRA,
             RMOVI.SL_DEV_COMPRA,
             RMOVI.SM_DEV_COMPRA,
             RMOVI.SC_VTA,
             RMOVI.SL_VTA,
             RMOVI.SM_VTA,
             RMOVI.EC_DEV_VTA,
             RMOVI.EL_DEV_VTA,
             RMOVI.EM_DEV_VTA,
             RMOVI.EC_PROD,
             RMOVI.EL_PROD,
             RMOVI.EM_PROD,
             RMOVI.SC_PROD,
             RMOVI.EC_KIT,
             RMOVI.EL_KIT,
             RMOVI.EM_KIT,
             RMOVI.SC_KIT,
             RMOVI.SL_KIT,
             RMOVI.SM_KIT,
             RMOVI.SC_PERDIDA,
             RMOVI.SC_CONS,
             RMOVI.EC_DIF_MAS - RMOVI.SC_DIF_MEN,
             RMOVI.EC_TRANSF - RMOVI.SC_TRANSF,
             RMOVI.C_REM,
             RMOVI.EC_ARM_DES,
             RMOVI.EL_ARM_DES,
             RMOVI.EM_ARM_DES,
             RMOVI.SC_ARM_DES,
             RMOVI.SL_ARM_DES,
             RMOVI.SM_ARM_DES,
             RMOVI.EC_DEV_PROD,
             RMOVI.EL_DEV_PROD,
             RMOVI.EM_DEV_PROD);
        
        EXCEPTION
          WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR(-20016,
                                    'INSERT STK_ART_EMPR_PERI = ' ||
                                    SQLERRM);
        END;
      
        FETCH MOVI_CUR
          INTO RMOVI;
      END LOOP;
    
    END IF;
  
    IF MOVI_CUR%NOTFOUND AND AEP_CUR%FOUND THEN
      LOOP
        EXIT WHEN AEP_CUR%NOTFOUND;
        -- ENTRA AQUI SI EXISTEN MAESTROS SIN MOVIMIENTOS
        IF RAEP.AEP_COSTO_PROM_LOC <> RAEP.AEP_COSTO_PROM_INI_LOC OR
           RAEP.AEP_COSTO_PROM_MON <> RAEP.AEP_COSTO_PROM_INI_MON OR
           RAEP.AEP_TOT_EXIST <> RAEP.AEP_EXIST_INI OR
           RAEP.AEP_EC_COMPRA <> 0 OR RAEP.AEP_EL_COMPRA <> 0 OR
           RAEP.AEP_EM_COMPRA <> 0 OR RAEP.AEP_SC_DEV_COMPRA <> 0 OR
           RAEP.AEP_SL_DEV_COMPRA <> 0 OR RAEP.AEP_SM_DEV_COMPRA <> 0 OR
           RAEP.AEP_SC_VTA <> 0 OR RAEP.AEP_SL_VTA <> 0 OR
           RAEP.AEP_SM_VTA <> 0 OR RAEP.AEP_EC_DEV_VTA <> 0 OR
           RAEP.AEP_EL_DEV_VTA <> 0 OR RAEP.AEP_EM_DEV_VTA <> 0 OR
           RAEP.AEP_EC_PROD <> 0 OR RAEP.AEP_EL_PROD <> 0 OR
           RAEP.AEP_EM_PROD <> 0 OR RAEP.AEP_EC_DEV_PROD <> 0 OR
           RAEP.AEP_EL_DEV_PROD <> 0 OR RAEP.AEP_EM_DEV_PROD <> 0 OR
           RAEP.AEP_SC_PROD <> 0 OR RAEP.AEP_EC_KIT <> 0 OR
           RAEP.AEP_EL_KIT <> 0 OR RAEP.AEP_EM_KIT <> 0 OR
           RAEP.AEP_SC_KIT <> 0 OR RAEP.AEP_SL_KIT <> 0 OR
           RAEP.AEP_SM_KIT <> 0 OR RAEP.AEP_SC_PERDIDA <> 0 OR
           RAEP.AEP_SC_CONS <> 0 OR RAEP.AEP_C_DIF_INV <> 0 OR
           RAEP.AEP_C_TRANSF <> 0 OR RAEP.AEP_C_REM <> 0 OR
           RAEP.AEP_EC_ARM_DES <> 0 OR RAEP.AEP_EL_ARM_DES <> 0 OR
           RAEP.AEP_EM_ARM_DES <> 0 OR RAEP.AEP_SC_ARM_DES <> 0 OR
           RAEP.AEP_SL_ARM_DES <> 0 OR RAEP.AEP_SM_ARM_DES <> 0 THEN
          V_MESSAGE := 'Error 605: Cantidades incorrectas' ||
                       ' en STK_ART_EMPR_PERI para ' || 'Empr=' ||
                       TO_CHAR(RAEP.AEP_EMPR) || ', Art=' ||
                       TO_CHAR(RAEP.AEP_ART) || '. Desea corregir?';
        
          BEGIN
          
            UPDATE STK_ART_EMPR_PERI
               SET AEP_TOT_EXIST      = AEP_EXIST_INI,
                   AEP_COSTO_PROM_LOC = AEP_COSTO_PROM_INI_LOC,
                   AEP_COSTO_PROM_MON = AEP_COSTO_PROM_INI_MON,
                   AEP_EC_COMPRA      = 0,
                   AEP_EL_COMPRA      = 0,
                   AEP_EM_COMPRA      = 0,
                   AEP_SC_DEV_COMPRA  = 0,
                   AEP_SL_DEV_COMPRA  = 0,
                   AEP_SM_DEV_COMPRA  = 0,
                   AEP_SC_VTA         = 0,
                   AEP_SL_VTA         = 0,
                   AEP_SM_VTA         = 0,
                   AEP_EC_DEV_VTA     = 0,
                   AEP_EL_DEV_VTA     = 0,
                   AEP_EM_DEV_VTA     = 0,
                   AEP_EC_PROD        = 0,
                   AEP_EL_PROD        = 0,
                   AEP_EM_PROD        = 0,
                   AEP_EC_DEV_PROD    = 0,
                   AEP_EL_DEV_PROD    = 0,
                   AEP_EM_DEV_PROD    = 0,
                   AEP_SC_PROD        = 0,
                   AEP_EC_KIT         = 0,
                   AEP_EL_KIT         = 0,
                   AEP_EM_KIT         = 0,
                   AEP_SC_KIT         = 0,
                   AEP_SL_KIT         = 0,
                   AEP_SM_KIT         = 0,
                   AEP_SC_PERDIDA     = 0,
                   AEP_SC_CONS        = 0,
                   AEP_C_DIF_INV      = 0,
                   AEP_C_TRANSF       = 0,
                   AEP_C_REM          = 0,
                   AEP_EC_ARM_DES     = 0,
                   AEP_EL_ARM_DES     = 0,
                   AEP_EM_ARM_DES     = 0,
                   AEP_SC_ARM_DES     = 0,
                   AEP_SL_ARM_DES     = 0,
                   AEP_SM_ARM_DES     = 0
             WHERE AEP_PERIODO = I_PERIODO
               AND AEP_EMPR = RAEP.AEP_EMPR
               AND AEP_ART = RAEP.AEP_ART;
          
          EXCEPTION
            WHEN OTHERS THEN
              RAISE_APPLICATION_ERROR(-20018,
                                      'UPDATE STK_ART_EMPR_PERI = ' ||
                                      SQLERRM);
          END;
        
        END IF;
        FETCH AEP_CUR
          INTO RAEP;
      
      END LOOP;
    END IF;
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20017, 'PP_VERIF_AEP = ' || SQLERRM);
  END PP_VERIF_AEP;

  PROCEDURE PP_VERIF_AEP_OPER(I_EMPRESA IN NUMBER,
                              I_FECINI  IN DATE,
                              I_FECFIN  IN DATE,
                              I_PERIODO IN NUMBER,
                              I_ART     IN NUMBER DEFAULT NULL) IS
  
    CURSOR MOVI_CUR IS
      SELECT DOCU_EMPR,
             DETA_ART,
             SUM(DECODE(OPER_DESC, 'COMPRA', DETA_CANT, 0)) EC_COMPRA,
             SUM(DECODE(OPER_DESC, 'COMPRA', DETA_IMP_NETO_LOC, 0)) EL_COMPRA,
             SUM(DECODE(OPER_DESC,
                        'COMPRA',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_COMPRA,
             SUM(DECODE(OPER_DESC, 'DEV_COM', DETA_CANT, 0)) SC_DEV_COMPRA,
             SUM(DECODE(OPER_DESC, 'DEV_COM', DETA_IMP_NETO_LOC, 0)) SL_DEV_COMPRA,
             SUM(DECODE(OPER_DESC,
                        'DEV_COM',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) SM_DEV_COMPRA,
             SUM(DECODE(OPER_DESC, 'VENTA', DETA_CANT, 0)) SC_VTA,
             SUM(DECODE(OPER_DESC, 'VENTA', DETA_IMP_NETO_LOC, 0)) SL_VTA,
             SUM(DECODE(OPER_DESC,
                        'VENTA',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) SM_VTA,
             SUM(DECODE(OPER_DESC, 'DEV_VENTA', DETA_CANT, 0)) EC_DEV_VTA,
             SUM(DECODE(OPER_DESC, 'DEV_VENTA', DETA_IMP_NETO_LOC, 0)) EL_DEV_VTA,
             SUM(DECODE(OPER_DESC,
                        'DEV_VENTA',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_DEV_VTA,
             --OPERACI?N 14. ANULACION DE VENTAS
             SUM(DECODE(OPER_DESC, 'ANUL_VENTA', DETA_CANT, 0)) EC_ANUL_VTA,
             SUM(DECODE(OPER_DESC, 'ANUL_VENTA', DETA_IMP_NETO_LOC, 0)) EL_ANUL_VTA,
             SUM(DECODE(OPER_DESC,
                        'ANUL_VENTA',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_ANUL_VTA,
             --OPERACI?N 34. ANULACI?N DE NOTAS DE CREDITOS HILAGRO
             SUM(DECODE(OPER_DESC, 'ANUL_NC', DETA_CANT, 0)) SC_ANUL_NC,
             SUM(DECODE(OPER_DESC, 'ANUL_NC', DETA_IMP_NETO_LOC, 0)) SL_ANUL_NC,
             SUM(DECODE(OPER_DESC,
                        'ANUL_NC',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) SM_ANUL_NC,
             --
             SUM(DECODE(OPER_DESC, 'ENT_PROD', DETA_CANT, 0)) EC_PROD,
             SUM(DECODE(OPER_DESC, 'ENT_PROD', DETA_IMP_NETO_LOC, 0)) EL_PROD,
             
             SUM(DECODE(OPER_DESC,
                        'ENT_PROD',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_PROD,
             
             SUM(DECODE(OPER_DESC, 'DEV_PROD', DETA_CANT, 0)) EC_DEV_PROD,
             SUM(DECODE(OPER_DESC, 'DEV_PROD', DETA_IMP_NETO_LOC, 0)) EL_DEV_PROD,
             SUM(DECODE(OPER_DESC,
                        'DEV_PROD',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_DEV_PROD,
             SUM(DECODE(OPER_DESC, 'SAL_PROD', DETA_CANT, 0)) SC_PROD,
             SUM(DECODE(OPER_DESC, 'ENT_KIT', DETA_CANT, 0)) EC_KIT,
             SUM(DECODE(OPER_DESC, 'ENT_KIT', DETA_IMP_NETO_LOC, 0)) EL_KIT,
             SUM(DECODE(OPER_DESC,
                        'ENT_KIT',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_KIT,
             SUM(DECODE(OPER_DESC, 'SAL_KIT', DETA_CANT, 0)) SC_KIT,
             SUM(DECODE(OPER_DESC, 'SAL_KIT', DETA_IMP_NETO_LOC, 0)) SL_KIT,
             SUM(DECODE(OPER_DESC,
                        'SAL_KIT',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) SM_KIT,
             SUM(DECODE(OPER_DESC, 'PERDIDA', DETA_CANT, 0)) SC_PERDIDA,
             SUM(DECODE(OPER_DESC, 'CONSUMO', DETA_CANT, 0)) SC_CONS,
             SUM(DECODE(OPER_DESC, 'DIF_MAS', DETA_CANT, 0)) EC_DIF_MAS,
             SUM(DECODE(OPER_DESC, 'DIF_MEN', DETA_CANT, 0)) SC_DIF_MEN,
             SUM(DECODE(OPER_DESC, 'TRAN_SAL', DETA_CANT, 0)) SC_TRANSF,
             SUM(DECODE(OPER_DESC, 'TRAN_ENT', DETA_CANT, 0)) EC_TRANSF,
             SUM(DECODE(OPER_DESC, 'REMISION', DETA_CANT, 0)) C_REM,
             SUM(DECODE(OPER_DESC, 'ENT_ARM_DES', DETA_CANT, 0)) EC_ARM_DES,
             SUM(DECODE(OPER_DESC, 'ENT_ARM_DES', DETA_IMP_NETO_LOC, 0)) EL_ARM_DES,
             SUM(DECODE(OPER_DESC,
                        'ENT_ARM_DES',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_ARM_DES,
             SUM(DECODE(OPER_DESC, 'SAL_ARM_DES', DETA_CANT, 0)) SC_ARM_DES,
             SUM(DECODE(OPER_DESC, 'SAL_ARM_DES', DETA_IMP_NETO_LOC, 0)) SL_ARM_DES,
             SUM(DECODE(OPER_DESC,
                        'SAL_ARM_DES',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) SM_ARM_DES,
             SUM(DECODE(OPER_ENT_SAL, 'E', DETA_CANT, 0)) C_ENT,
             SUM(DECODE(OPER_ENT_SAL, 'S', DETA_CANT, 0)) C_SAL,
             
             SUM(DECODE(OPER_DESC, 'REVALUO', DETA_IMP_NETO_LOC, 0)) EL_REVALUO,
             SUM(DECODE(OPER_DESC, 'DEPRECIACION', DETA_IMP_NETO_LOC, 0)) SL_DEPREC,
             
             SUM(DECODE(OPER_DESC,
                        'REVALUO',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_REVALUO,
             SUM(DECODE(OPER_DESC,
                        'DEPRECIACION',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) SM_DEPREC
      
        FROM STK_OPERACION     OP,
             STK_DOCUMENTO     DO,
             STK_DOCUMENTO_DET DE,
             STK_ARTICULO      AR
       WHERE OPER_CODIGO = DOCU_CODIGO_OPER
         AND OPER_EMPR = DOCU_EMPR
            
         AND DOCU_CLAVE = DETA_CLAVE_DOC
         AND DOCU_EMPR = DETA_EMPR
            
         AND ART_CODIGO = DETA_ART
         AND ART_EMPR = DETA_EMPR
            
         AND DOCU_EMPR = I_EMPRESA
            
            -------------------------------------------------------------------------------
         AND (ART_TIPO <> 4 OR AR.ART_IND_INV_VALOR_COSTO = 'S')
            -------------------------------------------------------------------------------
            
  --       AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
         AND DOCU_FEC_OPER BETWEEN I_FECINI AND I_FECFIN
       GROUP BY DOCU_EMPR, DETA_ART
       ORDER BY DOCU_EMPR, DETA_ART;
  
    CURSOR AEP_CUR IS
      SELECT AEP_PERIODO,
             AEP_EMPR,
             AEP_ART,
             AEP_TOT_EXIST_OPER,
             AEP_EXIST_INI_OPER,
             AEP_COSTO_INI_LOC_OPER,
             AEP_COSTO_PROM_INI_LOC_OPER,
             AEP_COSTO_PROM_LOC_OPER,
             AEP_COSTO_INI_MON_OPER,
             AEP_COSTO_PROM_INI_MON_OPER,
             AEP_COSTO_PROM_MON_OPER,
             AEP_EC_COMPRA_OPER,
             AEP_EL_COMPRA_OPER,
             AEP_EM_COMPRA_OPER,
             AEP_SC_DEV_COMPRA_OPER,
             AEP_SL_DEV_COMPRA_OPER,
             AEP_SM_DEV_COMPRA_OPER,
             AEP_SC_VTA_OPER,
             AEP_SL_VTA_OPER,
             AEP_SM_VTA_OPER,
             AEP_EC_DEV_VTA_OPER,
             AEP_EL_DEV_VTA_OPER,
             AEP_EM_DEV_VTA_OPER,
             AEP_EC_PROD_OPER,
             AEP_EL_PROD_OPER,
             AEP_EM_PROD_OPER,
             AEP_SC_PROD_OPER,
             AEP_EC_KIT_OPER,
             AEP_EL_KIT_OPER,
             AEP_EM_KIT_OPER,
             AEP_SC_KIT_OPER,
             AEP_SC_PERDIDA_OPER,
             AEP_SC_CONS_OPER,
             AEP_C_DIF_INV_OPER,
             AEP_C_TRANSF_OPER,
             AEP_C_REM_OPER,
             AEP_EC_ARM_DES_OPER,
             AEP_EL_ARM_DES_OPER,
             AEP_EM_ARM_DES_OPER,
             AEP_SC_ARM_DES_OPER,
             AEP_SL_ARM_DES_OPER,
             AEP_SM_ARM_DES_OPER,
             AEP_SL_KIT_OPER,
             AEP_SM_KIT_OPER,
             AEP_EC_DEV_PROD_OPER,
             AEP_EL_DEV_PROD_OPER,
             AEP_EM_DEV_PROD_OPER
        FROM STK_ART_EMPR_PERI AP, STK_ARTICULO AR
       WHERE AEP_EMPR = I_EMPRESA
         AND AEP_ART = ART_CODIGO
         AND AEP_EMPR = ART_EMPR
         AND AEP_PERIODO = I_PERIODO
            -------------------------------------------------------------------------------
         AND (ART_TIPO <> 4 OR AR.ART_IND_INV_VALOR_COSTO = 'S')
            -------------------------------------------------------------------------------
    --     AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
       ORDER BY AEP_EMPR, AEP_ART;
    --
    RMOVI     MOVI_CUR%ROWTYPE;
    RAEP      AEP_CUR%ROWTYPE;
    V_MESSAGE VARCHAR2(200);
  
  BEGIN
  
    V_MESSAGE := 'Verificando: Acumulados de Articulo/Empresa/Periodo(' ||
                 TO_CHAR(I_PERIODO) || ')...';
  
    --CALCULAR COSTO PROMEDIO PONDERADO
    V_MESSAGE := 'Obteniendo Promedio Ponderado';
  
    V_MESSAGE := 'Seleccionando...';
  
    OPEN MOVI_CUR;
    FETCH MOVI_CUR
      INTO RMOVI;
  
    OPEN AEP_CUR;
    FETCH AEP_CUR
      INTO RAEP;
  
    LOOP
    
      V_MESSAGE := 'Empr=' || TO_CHAR(RMOVI.DOCU_EMPR) || ', Art=' ||
                   TO_CHAR(RMOVI.DETA_ART);
    
      EXIT WHEN MOVI_CUR%NOTFOUND OR AEP_CUR%NOTFOUND;
    
      LOOP
        EXIT WHEN MOVI_CUR%NOTFOUND OR(RMOVI.DOCU_EMPR > RAEP.AEP_EMPR OR
                                       (RMOVI.DOCU_EMPR = RAEP.AEP_EMPR AND
                                       RMOVI.DETA_ART >= RAEP.AEP_ART));
        -- ENTRA AQUI SI EXISTEN MOVIMIENTOS SIN MAESTRO
        V_MESSAGE := 'Error 601: Falta reg. en STK_ART_EMPR_PERI' ||
                     ' para Empr=' || TO_CHAR(RMOVI.DOCU_EMPR) || ', Art=' ||
                     TO_CHAR(RMOVI.DETA_ART) || '. Desea corregir?';
      
        INSERT INTO STK_ART_EMPR_PERI
          (AEP_PERIODO,
           AEP_EMPR,
           AEP_ART,
           AEP_TOT_EXIST,
           AEP_EXIST_INI,
           AEP_COSTO_INI_LOC,
           AEP_COSTO_INI_MON,
           AEP_COSTO_PROM_INI_LOC,
           AEP_COSTO_PROM_INI_MON,
           AEP_COSTO_PROM_LOC,
           AEP_COSTO_PROM_MON,
           AEP_EC_COMPRA,
           AEP_EL_COMPRA,
           AEP_EM_COMPRA,
           AEP_SC_DEV_COMPRA,
           AEP_SL_DEV_COMPRA,
           AEP_SM_DEV_COMPRA,
           AEP_SC_VTA,
           AEP_SL_VTA,
           AEP_SM_VTA,
           AEP_EC_DEV_VTA,
           AEP_EL_DEV_VTA,
           AEP_EM_DEV_VTA,
           AEP_EC_PROD,
           AEP_EL_PROD,
           AEP_EM_PROD,
           AEP_SC_PROD,
           AEP_EC_KIT,
           AEP_EL_KIT,
           AEP_EM_KIT,
           AEP_SC_KIT,
           AEP_SL_KIT,
           AEP_SM_KIT,
           AEP_SC_PERDIDA,
           AEP_SC_CONS,
           AEP_C_DIF_INV,
           AEP_C_TRANSF,
           AEP_C_REM,
           AEP_EC_ARM_DES,
           AEP_EL_ARM_DES,
           AEP_EM_ARM_DES,
           AEP_SC_ARM_DES,
           AEP_SL_ARM_DES,
           AEP_SM_ARM_DES,
           AEP_EC_DEV_PROD,
           AEP_EL_DEV_PROD,
           AEP_EM_DEV_PROD,
           --DESDE AQUI COMIENZAN LO CAMPOS DE OPERACION
           AEP_TOT_EXIST_OPER,
           AEP_EXIST_INI_OPER,
           AEP_COSTO_INI_LOC_OPER,
           AEP_COSTO_PROM_INI_LOC_OPER,
           AEP_COSTO_PROM_LOC_OPER,
           AEP_COSTO_INI_MON_OPER,
           AEP_COSTO_PROM_INI_MON_OPER,
           AEP_COSTO_PROM_MON_OPER,
           AEP_EC_COMPRA_OPER,
           AEP_EL_COMPRA_OPER,
           AEP_EM_COMPRA_OPER,
           AEP_SC_DEV_COMPRA_OPER,
           AEP_SL_DEV_COMPRA_OPER,
           AEP_SM_DEV_COMPRA_OPER,
           AEP_SC_VTA_OPER,
           AEP_SL_VTA_OPER,
           AEP_SM_VTA_OPER,
           AEP_EC_DEV_VTA_OPER,
           AEP_EL_DEV_VTA_OPER,
           AEP_EM_DEV_VTA_OPER,
           AEP_EC_PROD_OPER,
           AEP_EL_PROD_OPER,
           AEP_EM_PROD_OPER,
           AEP_SC_PROD_OPER,
           AEP_EC_KIT_OPER,
           AEP_EL_KIT_OPER,
           AEP_EM_KIT_OPER,
           AEP_SC_KIT_OPER,
           AEP_SC_PERDIDA_OPER,
           AEP_SC_CONS_OPER,
           AEP_C_DIF_INV_OPER,
           AEP_C_TRANSF_OPER,
           AEP_C_REM_OPER,
           AEP_EC_ARM_DES_OPER,
           AEP_EL_ARM_DES_OPER,
           AEP_EM_ARM_DES_OPER,
           AEP_SC_ARM_DES_OPER,
           AEP_SL_ARM_DES_OPER,
           AEP_SM_ARM_DES_OPER,
           AEP_SL_KIT_OPER,
           AEP_SM_KIT_OPER,
           AEP_EC_DEV_PROD_OPER,
           AEP_EL_DEV_PROD_OPER,
           AEP_EM_DEV_PROD_OPER
           
           )
        VALUES
          (I_PERIODO,
           RMOVI.DOCU_EMPR,
           RMOVI.DETA_ART,
           RMOVI.C_ENT - RMOVI.C_SAL,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           RMOVI.EC_COMPRA,
           RMOVI.EL_COMPRA,
           RMOVI.EM_COMPRA,
           RMOVI.SC_DEV_COMPRA,
           RMOVI.SL_DEV_COMPRA,
           RMOVI.SM_DEV_COMPRA,
           RMOVI.SC_VTA,
           RMOVI.SL_VTA,
           RMOVI.SM_VTA,
           RMOVI.EC_DEV_VTA,
           RMOVI.EL_DEV_VTA,
           RMOVI.EM_DEV_VTA,
           RMOVI.EC_PROD,
           RMOVI.EL_PROD,
           RMOVI.EM_PROD,
           RMOVI.SC_PROD,
           RMOVI.EC_KIT,
           RMOVI.EL_KIT,
           RMOVI.EM_KIT,
           RMOVI.SC_KIT,
           RMOVI.SL_KIT,
           RMOVI.SM_KIT,
           RMOVI.SC_PERDIDA,
           RMOVI.SC_CONS,
           RMOVI.EC_DIF_MAS - RMOVI.SC_DIF_MEN,
           RMOVI.EC_TRANSF - RMOVI.SC_TRANSF,
           RMOVI.C_REM,
           RMOVI.EC_ARM_DES,
           RMOVI.EL_ARM_DES,
           RMOVI.EM_ARM_DES,
           RMOVI.SC_ARM_DES,
           RMOVI.SL_ARM_DES,
           RMOVI.SM_ARM_DES,
           RMOVI.EC_DEV_PROD,
           RMOVI.EL_DEV_PROD,
           RMOVI.EM_DEV_PROD,
           --DESDE AQUI COMIENZAN LO CAMPOS DE OPERACION
           RMOVI.C_ENT - RMOVI.C_SAL,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           RMOVI.EC_COMPRA,
           RMOVI.EL_COMPRA,
           RMOVI.EM_COMPRA,
           RMOVI.SC_DEV_COMPRA,
           RMOVI.SL_DEV_COMPRA,
           RMOVI.SM_DEV_COMPRA,
           RMOVI.SC_VTA,
           RMOVI.SL_VTA,
           RMOVI.SM_VTA,
           RMOVI.EC_DEV_VTA,
           RMOVI.EL_DEV_VTA,
           RMOVI.EM_DEV_VTA,
           RMOVI.EC_PROD,
           RMOVI.EL_PROD,
           RMOVI.EM_PROD,
           RMOVI.SC_PROD,
           RMOVI.EC_KIT,
           RMOVI.EL_KIT,
           RMOVI.EM_KIT,
           RMOVI.SC_KIT,
           RMOVI.SL_KIT,
           RMOVI.SM_KIT,
           RMOVI.SC_PERDIDA,
           RMOVI.SC_CONS,
           RMOVI.EC_DIF_MAS - RMOVI.SC_DIF_MEN,
           RMOVI.EC_TRANSF - RMOVI.SC_TRANSF,
           RMOVI.C_REM,
           RMOVI.EC_ARM_DES,
           RMOVI.EL_ARM_DES,
           RMOVI.EM_ARM_DES,
           RMOVI.SC_ARM_DES,
           RMOVI.SL_ARM_DES,
           RMOVI.SM_ARM_DES,
           RMOVI.EC_DEV_PROD,
           RMOVI.EL_DEV_PROD,
           RMOVI.EM_DEV_PROD);
        FETCH MOVI_CUR
          INTO RMOVI;
      END LOOP;
    
      LOOP
        EXIT WHEN AEP_CUR%NOTFOUND OR(RAEP.AEP_EMPR > RMOVI.DOCU_EMPR OR
                                      (RAEP.AEP_EMPR = RMOVI.DOCU_EMPR AND
                                      RAEP.AEP_ART >= RMOVI.DETA_ART));
        -- ENTRA AQUI SI EXISTEN REG.MAESTROS SIN MOVIMIENTOS
        IF RAEP.AEP_TOT_EXIST_OPER <> RAEP.AEP_EXIST_INI_OPER OR
           RAEP.AEP_EC_COMPRA_OPER <> 0 OR RAEP.AEP_EL_COMPRA_OPER <> 0 OR
           RAEP.AEP_EM_COMPRA_OPER <> 0 OR RAEP.AEP_SC_DEV_COMPRA_OPER <> 0 OR
           RAEP.AEP_SL_DEV_COMPRA_OPER <> 0 OR
           RAEP.AEP_SM_DEV_COMPRA_OPER <> 0 OR RAEP.AEP_SC_VTA_OPER <> 0 OR
           RAEP.AEP_SL_VTA_OPER <> 0 OR RAEP.AEP_SM_VTA_OPER <> 0 OR
           RAEP.AEP_EC_DEV_VTA_OPER <> 0 OR RAEP.AEP_EL_DEV_VTA_OPER <> 0 OR
           RAEP.AEP_EM_DEV_VTA_OPER <> 0 OR RAEP.AEP_EC_PROD_OPER <> 0 OR
           RAEP.AEP_EL_PROD_OPER <> 0 OR RAEP.AEP_EM_PROD_OPER <> 0 OR
           RAEP.AEP_EC_DEV_PROD_OPER <> 0 OR RAEP.AEP_EL_DEV_PROD_OPER <> 0 OR
           RAEP.AEP_EM_DEV_PROD_OPER <> 0 OR RAEP.AEP_SC_PROD_OPER <> 0 OR
           RAEP.AEP_EC_KIT_OPER <> 0 OR RAEP.AEP_EL_KIT_OPER <> 0 OR
           RAEP.AEP_EM_KIT_OPER <> 0 OR RAEP.AEP_SC_KIT_OPER <> 0 OR
           RAEP.AEP_SL_KIT_OPER <> 0 OR RAEP.AEP_SM_KIT_OPER <> 0 OR
           RAEP.AEP_SC_PERDIDA_OPER <> 0 OR RAEP.AEP_SC_CONS_OPER <> 0 OR
           RAEP.AEP_C_DIF_INV_OPER <> 0 OR RAEP.AEP_C_TRANSF_OPER <> 0 OR
           RAEP.AEP_C_REM_OPER <> 0 OR RAEP.AEP_EC_ARM_DES_OPER <> 0 OR
           RAEP.AEP_EL_ARM_DES_OPER <> 0 OR RAEP.AEP_EM_ARM_DES_OPER <> 0 OR
           RAEP.AEP_SC_ARM_DES_OPER <> 0 OR RAEP.AEP_SL_ARM_DES_OPER <> 0 OR
           RAEP.AEP_SM_ARM_DES_OPER <> 0 THEN
        
          V_MESSAGE := 'Error 602: Cantidades incorrectas' ||
                       ' en STK_ART_EMPR_PERI para ' || 'Empr=' ||
                       TO_CHAR(RAEP.AEP_EMPR) || ', Art=' ||
                       TO_CHAR(RAEP.AEP_ART) || '. Desea corregir?';
        
          UPDATE STK_ART_EMPR_PERI
             SET AEP_EC_COMPRA_OPER     = 0,
                 AEP_EL_COMPRA_OPER     = 0,
                 AEP_EM_COMPRA_OPER     = 0,
                 AEP_SC_DEV_COMPRA_OPER = 0,
                 AEP_SL_DEV_COMPRA_OPER = 0,
                 AEP_SM_DEV_COMPRA_OPER = 0,
                 AEP_SC_VTA_OPER        = 0,
                 AEP_SL_VTA_OPER        = 0,
                 AEP_SM_VTA_OPER        = 0,
                 AEP_EC_DEV_VTA_OPER    = 0,
                 AEP_EL_DEV_VTA_OPER    = 0,
                 AEP_EM_DEV_VTA_OPER    = 0,
                 AEP_EC_PROD_OPER       = 0,
                 AEP_EL_PROD_OPER       = 0,
                 AEP_EM_PROD_OPER       = 0,
                 AEP_EC_DEV_PROD_OPER   = 0,
                 AEP_EL_DEV_PROD_OPER   = 0,
                 AEP_EM_DEV_PROD_OPER   = 0,
                 AEP_SC_PROD_OPER       = 0,
                 AEP_EC_KIT_OPER        = 0,
                 AEP_EL_KIT_OPER        = 0,
                 AEP_EM_KIT_OPER        = 0,
                 AEP_SC_KIT_OPER        = 0,
                 AEP_SL_KIT_OPER        = 0,
                 AEP_SM_KIT_OPER        = 0,
                 AEP_SC_PERDIDA_OPER    = 0,
                 AEP_SC_CONS_OPER       = 0,
                 AEP_C_DIF_INV_OPER     = 0,
                 AEP_C_TRANSF_OPER      = 0,
                 AEP_EC_ARM_DES_OPER    = 0,
                 AEP_EL_ARM_DES_OPER    = 0,
                 AEP_EM_ARM_DES_OPER    = 0,
                 AEP_SC_ARM_DES_OPER    = 0,
                 AEP_SL_ARM_DES_OPER    = 0,
                 AEP_SM_ARM_DES_OPER    = 0,
                 AEP_C_REM_OPER         = 0
           WHERE AEP_PERIODO = I_PERIODO
             AND AEP_EMPR = RAEP.AEP_EMPR
             AND AEP_ART = RAEP.AEP_ART;
        END IF;
        FETCH AEP_CUR
          INTO RAEP;
      END LOOP;
    
      EXIT WHEN MOVI_CUR%NOTFOUND OR AEP_CUR%NOTFOUND;
      IF RMOVI.DOCU_EMPR = RAEP.AEP_EMPR AND RMOVI.DETA_ART = RAEP.AEP_ART THEN
        -- ENTRA AQUI SI EXISTEN MOVIMIENTOS Y SU REG.MAESTRO
      
        IF RAEP.AEP_TOT_EXIST_OPER <>
           (RAEP.AEP_EXIST_INI_OPER + RMOVI.C_ENT - RMOVI.C_SAL)
          
           OR RAEP.AEP_EC_COMPRA_OPER <> RMOVI.EC_COMPRA OR
           RAEP.AEP_EL_COMPRA_OPER <> RMOVI.EL_COMPRA OR
           RAEP.AEP_EM_COMPRA_OPER <> RMOVI.EM_COMPRA OR
           RAEP.AEP_SC_DEV_COMPRA_OPER <> RMOVI.SC_DEV_COMPRA OR
           RAEP.AEP_SL_DEV_COMPRA_OPER <> RMOVI.SL_DEV_COMPRA OR
           RAEP.AEP_SM_DEV_COMPRA_OPER <> RMOVI.SM_DEV_COMPRA OR
           RAEP.AEP_SC_VTA_OPER <> RMOVI.SC_VTA OR
           RAEP.AEP_SL_VTA_OPER <> RMOVI.SL_VTA OR
           RAEP.AEP_SM_VTA_OPER <> RMOVI.SM_VTA OR
           RAEP.AEP_EC_DEV_VTA_OPER <> RMOVI.EC_DEV_VTA OR
           RAEP.AEP_EL_DEV_VTA_OPER <> RMOVI.EL_DEV_VTA OR
           RAEP.AEP_EM_DEV_VTA_OPER <> RMOVI.EM_DEV_VTA OR
           RAEP.AEP_EC_PROD_OPER <> RMOVI.EC_PROD OR
           RAEP.AEP_EL_PROD_OPER <> RMOVI.EL_PROD OR
           RAEP.AEP_EM_PROD_OPER <> RMOVI.EM_PROD OR
           RAEP.AEP_EC_DEV_PROD_OPER <> RMOVI.EC_DEV_PROD OR
           RAEP.AEP_EL_DEV_PROD_OPER <> RMOVI.EL_DEV_PROD OR
           RAEP.AEP_EM_DEV_PROD_OPER <> RMOVI.EM_DEV_PROD OR
           RAEP.AEP_SC_PROD_OPER <> RMOVI.SC_PROD OR
           RAEP.AEP_EC_KIT_OPER <> RMOVI.EC_KIT OR
           RAEP.AEP_EL_KIT_OPER <> RMOVI.EL_KIT OR
           RAEP.AEP_EM_KIT_OPER <> RMOVI.EM_KIT OR
           RAEP.AEP_SC_KIT_OPER <> RMOVI.SC_KIT OR
           RAEP.AEP_SL_KIT_OPER <> RMOVI.SL_KIT OR
           RAEP.AEP_SM_KIT_OPER <> RMOVI.SM_KIT OR
           RAEP.AEP_SC_PERDIDA_OPER <> RMOVI.SC_PERDIDA OR
           RAEP.AEP_SC_CONS_OPER <> RMOVI.SC_CONS OR
           RAEP.AEP_C_DIF_INV_OPER <> (RMOVI.EC_DIF_MAS - RMOVI.SC_DIF_MEN) OR
           RAEP.AEP_C_TRANSF_OPER <> (RMOVI.EC_TRANSF - RMOVI.SC_TRANSF) OR
           RAEP.AEP_C_REM_OPER <> RMOVI.C_REM OR
           RAEP.AEP_EC_ARM_DES_OPER <> RMOVI.EC_ARM_DES OR
           RAEP.AEP_EL_ARM_DES_OPER <> RMOVI.EL_ARM_DES OR
           RAEP.AEP_EM_ARM_DES_OPER <> RMOVI.EM_ARM_DES OR
           RAEP.AEP_SC_ARM_DES_OPER <> RMOVI.SC_ARM_DES OR
           RAEP.AEP_SL_ARM_DES_OPER <> RMOVI.SL_ARM_DES OR
           RAEP.AEP_SM_ARM_DES_OPER <> RMOVI.SM_ARM_DES THEN
          -- ACTUALIZAR SI HAY DIFERENCIAS ENTRE MOV.Y MAESTRO
          V_MESSAGE := 'Error 603: En STK_ART_EMPR_PERI' || ' para Empr=' ||
                       TO_CHAR(RAEP.AEP_EMPR) || ', Art=' ||
                       TO_CHAR(RAEP.AEP_ART) || '. Desea corregir?';
        
          BEGIN
            UPDATE STK_ART_EMPR_PERI
               SET AEP_TOT_EXIST_OPER    =
                   (NVL(RAEP.AEP_EXIST_INI_OPER, 0) + NVL(RMOVI.C_ENT, 0) -
                   NVL(RMOVI.C_SAL, 0)),
                   AEP_EC_COMPRA_OPER     = RMOVI.EC_COMPRA,
                   AEP_EL_COMPRA_OPER     = RMOVI.EL_COMPRA,
                   AEP_EM_COMPRA_OPER     = RMOVI.EM_COMPRA,
                   AEP_SC_DEV_COMPRA_OPER = RMOVI.SC_DEV_COMPRA,
                   AEP_SL_DEV_COMPRA_OPER = RMOVI.SL_DEV_COMPRA,
                   AEP_SM_DEV_COMPRA_OPER = RMOVI.SM_DEV_COMPRA,
                   AEP_SC_VTA_OPER        = RMOVI.SC_VTA,
                   AEP_SL_VTA_OPER        = RMOVI.SL_VTA,
                   AEP_SM_VTA_OPER        = RMOVI.SM_VTA,
                   AEP_EC_DEV_VTA_OPER    = RMOVI.EC_DEV_VTA,
                   AEP_EL_DEV_VTA_OPER    = RMOVI.EL_DEV_VTA,
                   AEP_EM_DEV_VTA_OPER    = RMOVI.EM_DEV_VTA,
                   AEP_EC_PROD_OPER       = RMOVI.EC_PROD,
                   AEP_EL_PROD_OPER       = NVL(RMOVI.EL_PROD, 0),
                   AEP_EM_PROD_OPER       = NVL(RMOVI.EM_PROD, 0),
                   AEP_EC_DEV_PROD_OPER   = RMOVI.EC_DEV_PROD,
                   AEP_EL_DEV_PROD_OPER   = RMOVI.EL_DEV_PROD,
                   AEP_EM_DEV_PROD_OPER   = RMOVI.EM_DEV_PROD,
                   AEP_SC_PROD_OPER       = RMOVI.SC_PROD,
                   AEP_EC_KIT_OPER        = RMOVI.EC_KIT,
                   AEP_EL_KIT_OPER        = RMOVI.EL_KIT,
                   AEP_EM_KIT_OPER        = RMOVI.EM_KIT,
                   AEP_SC_KIT_OPER        = RMOVI.SC_KIT,
                   AEP_SL_KIT_OPER        = RMOVI.SL_KIT,
                   AEP_SM_KIT_OPER        = RMOVI.SM_KIT,
                   AEP_SC_PERDIDA_OPER    = RMOVI.SC_PERDIDA,
                   AEP_SC_CONS_OPER       = RMOVI.SC_CONS,
                   AEP_C_DIF_INV_OPER    =
                   (RMOVI.EC_DIF_MAS - RMOVI.SC_DIF_MEN),
                   AEP_C_TRANSF_OPER     =
                   (RMOVI.EC_TRANSF - RMOVI.SC_TRANSF),
                   AEP_C_REM_OPER         = RMOVI.C_REM,
                   AEP_EC_ARM_DES_OPER    = RMOVI.EC_ARM_DES,
                   AEP_EL_ARM_DES_OPER    = RMOVI.EL_ARM_DES,
                   AEP_EM_ARM_DES_OPER    = RMOVI.EM_ARM_DES,
                   AEP_SC_ARM_DES_OPER    = RMOVI.SC_ARM_DES,
                   AEP_SL_ARM_DES_OPER    = RMOVI.SL_ARM_DES,
                   AEP_SM_ARM_DES_OPER    = RMOVI.SM_ARM_DES
             WHERE AEP_PERIODO = I_PERIODO
               AND AEP_EMPR = RAEP.AEP_EMPR
               AND AEP_ART = RAEP.AEP_ART;
          EXCEPTION
            WHEN OTHERS THEN
              RAISE_APPLICATION_ERROR(-20018, SQLERRM);
          END;
        END IF;
      
        FETCH MOVI_CUR
          INTO RMOVI;
        FETCH AEP_CUR
          INTO RAEP;
      END IF;
    
    END LOOP;
  
    IF MOVI_CUR%FOUND AND AEP_CUR%NOTFOUND THEN
      LOOP
        EXIT WHEN MOVI_CUR%NOTFOUND;
        -- ENTRA AQUI SI EXISTEN MOVIMIENTOS SIN MAESTRO
        V_MESSAGE := 'Error 604: Falta reg. en STK_ART_EMPR_PERI para ' ||
                     'Empr=' || TO_CHAR(RMOVI.DOCU_EMPR) || ', Art=' ||
                     TO_CHAR(RMOVI.DETA_ART) || '. Desea corregir?';
      
        INSERT INTO STK_ART_EMPR_PERI
          (AEP_PERIODO,
           AEP_EMPR,
           AEP_ART,
           AEP_TOT_EXIST,
           AEP_EXIST_INI,
           AEP_COSTO_INI_LOC,
           AEP_COSTO_INI_MON,
           AEP_COSTO_PROM_INI_LOC,
           AEP_COSTO_PROM_INI_MON,
           AEP_COSTO_PROM_LOC,
           AEP_COSTO_PROM_MON,
           AEP_EC_COMPRA,
           AEP_EL_COMPRA,
           AEP_EM_COMPRA,
           AEP_SC_DEV_COMPRA,
           AEP_SL_DEV_COMPRA,
           AEP_SM_DEV_COMPRA,
           AEP_SC_VTA,
           AEP_SL_VTA,
           AEP_SM_VTA,
           AEP_EC_DEV_VTA,
           AEP_EL_DEV_VTA,
           AEP_EM_DEV_VTA,
           AEP_EC_PROD,
           AEP_EL_PROD,
           AEP_EM_PROD,
           AEP_SC_PROD,
           AEP_EC_KIT,
           AEP_EL_KIT,
           AEP_EM_KIT,
           AEP_SC_KIT,
           AEP_SL_KIT,
           AEP_SM_KIT,
           AEP_SC_PERDIDA,
           AEP_SC_CONS,
           AEP_C_DIF_INV,
           AEP_C_TRANSF,
           AEP_C_REM,
           AEP_EC_ARM_DES,
           AEP_EL_ARM_DES,
           AEP_EM_ARM_DES,
           AEP_SC_ARM_DES,
           AEP_SL_ARM_DES,
           AEP_SM_ARM_DES,
           AEP_EC_DEV_PROD,
           AEP_EL_DEV_PROD,
           AEP_EM_DEV_PROD,
           --DESDE AQUI COMIENZAN LO CAMPOS DE OPERACION
           AEP_TOT_EXIST_OPER,
           AEP_EXIST_INI_OPER,
           AEP_COSTO_INI_LOC_OPER,
           AEP_COSTO_PROM_INI_LOC_OPER,
           AEP_COSTO_PROM_LOC_OPER,
           AEP_COSTO_INI_MON_OPER,
           AEP_COSTO_PROM_INI_MON_OPER,
           AEP_COSTO_PROM_MON_OPER,
           AEP_EC_COMPRA_OPER,
           AEP_EL_COMPRA_OPER,
           AEP_EM_COMPRA_OPER,
           AEP_SC_DEV_COMPRA_OPER,
           AEP_SL_DEV_COMPRA_OPER,
           AEP_SM_DEV_COMPRA_OPER,
           AEP_SC_VTA_OPER,
           AEP_SL_VTA_OPER,
           AEP_SM_VTA_OPER,
           AEP_EC_DEV_VTA_OPER,
           AEP_EL_DEV_VTA_OPER,
           AEP_EM_DEV_VTA_OPER,
           AEP_EC_PROD_OPER,
           AEP_EL_PROD_OPER,
           AEP_EM_PROD_OPER,
           AEP_SC_PROD_OPER,
           AEP_EC_KIT_OPER,
           AEP_EL_KIT_OPER,
           AEP_EM_KIT_OPER,
           AEP_SC_KIT_OPER,
           AEP_SC_PERDIDA_OPER,
           AEP_SC_CONS_OPER,
           AEP_C_DIF_INV_OPER,
           AEP_C_TRANSF_OPER,
           AEP_C_REM_OPER,
           AEP_EC_ARM_DES_OPER,
           AEP_EL_ARM_DES_OPER,
           AEP_EM_ARM_DES_OPER,
           AEP_SC_ARM_DES_OPER,
           AEP_SL_ARM_DES_OPER,
           AEP_SM_ARM_DES_OPER,
           AEP_SL_KIT_OPER,
           AEP_SM_KIT_OPER,
           AEP_EC_DEV_PROD_OPER,
           AEP_EL_DEV_PROD_OPER,
           AEP_EM_DEV_PROD_OPER
           
           )
        VALUES
          (I_PERIODO,
           RMOVI.DOCU_EMPR,
           RMOVI.DETA_ART,
           RMOVI.C_ENT - RMOVI.C_SAL,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           RMOVI.EC_COMPRA,
           RMOVI.EL_COMPRA,
           RMOVI.EM_COMPRA,
           RMOVI.SC_DEV_COMPRA,
           RMOVI.SL_DEV_COMPRA,
           RMOVI.SM_DEV_COMPRA,
           RMOVI.SC_VTA,
           RMOVI.SL_VTA,
           RMOVI.SM_VTA,
           RMOVI.EC_DEV_VTA,
           RMOVI.EL_DEV_VTA,
           RMOVI.EM_DEV_VTA,
           RMOVI.EC_PROD,
           RMOVI.EL_PROD,
           RMOVI.EM_PROD,
           RMOVI.SC_PROD,
           RMOVI.EC_KIT,
           RMOVI.EL_KIT,
           RMOVI.EM_KIT,
           RMOVI.SC_KIT,
           RMOVI.SL_KIT,
           RMOVI.SM_KIT,
           RMOVI.SC_PERDIDA,
           RMOVI.SC_CONS,
           RMOVI.EC_DIF_MAS - RMOVI.SC_DIF_MEN,
           RMOVI.EC_TRANSF - RMOVI.SC_TRANSF,
           RMOVI.C_REM,
           RMOVI.EC_ARM_DES,
           RMOVI.EL_ARM_DES,
           RMOVI.EM_ARM_DES,
           RMOVI.SC_ARM_DES,
           RMOVI.SL_ARM_DES,
           RMOVI.SM_ARM_DES,
           RMOVI.EC_DEV_PROD,
           RMOVI.EL_DEV_PROD,
           RMOVI.EM_DEV_PROD,
           --DESDE AQUI COMIENZAN LO CAMPOS DE OPERACION
           RMOVI.C_ENT - RMOVI.C_SAL,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           RMOVI.EC_COMPRA,
           RMOVI.EL_COMPRA,
           RMOVI.EM_COMPRA,
           RMOVI.SC_DEV_COMPRA,
           RMOVI.SL_DEV_COMPRA,
           RMOVI.SM_DEV_COMPRA,
           RMOVI.SC_VTA,
           RMOVI.SL_VTA,
           RMOVI.SM_VTA,
           RMOVI.EC_DEV_VTA,
           RMOVI.EL_DEV_VTA,
           RMOVI.EM_DEV_VTA,
           RMOVI.EC_PROD,
           RMOVI.EL_PROD,
           RMOVI.EM_PROD,
           RMOVI.SC_PROD,
           RMOVI.EC_KIT,
           RMOVI.EL_KIT,
           RMOVI.EM_KIT,
           RMOVI.SC_KIT,
           RMOVI.SL_KIT,
           RMOVI.SM_KIT,
           RMOVI.SC_PERDIDA,
           RMOVI.SC_CONS,
           RMOVI.EC_DIF_MAS - RMOVI.SC_DIF_MEN,
           RMOVI.EC_TRANSF - RMOVI.SC_TRANSF,
           RMOVI.C_REM,
           RMOVI.EC_ARM_DES,
           RMOVI.EL_ARM_DES,
           RMOVI.EM_ARM_DES,
           RMOVI.SC_ARM_DES,
           RMOVI.SL_ARM_DES,
           RMOVI.SM_ARM_DES,
           RMOVI.EC_DEV_PROD,
           RMOVI.EL_DEV_PROD,
           RMOVI.EM_DEV_PROD);
      
        FETCH MOVI_CUR
          INTO RMOVI;
      END LOOP;
    
    END IF;
  
    IF MOVI_CUR%NOTFOUND AND AEP_CUR%FOUND THEN
      LOOP
        EXIT WHEN AEP_CUR%NOTFOUND;
        -- ENTRA AQUI SI EXISTEN MAESTROS SIN MOVIMIENTOS
        IF RAEP.AEP_COSTO_PROM_LOC_OPER <> RAEP.AEP_COSTO_PROM_INI_LOC_OPER OR
           RAEP.AEP_COSTO_PROM_MON_OPER <> RAEP.AEP_COSTO_PROM_INI_MON_OPER OR
           RAEP.AEP_TOT_EXIST_OPER <> RAEP.AEP_EXIST_INI_OPER OR
           RAEP.AEP_EC_COMPRA_OPER <> 0 OR RAEP.AEP_EL_COMPRA_OPER <> 0 OR
           RAEP.AEP_EM_COMPRA_OPER <> 0 OR RAEP.AEP_SC_DEV_COMPRA_OPER <> 0 OR
           RAEP.AEP_SL_DEV_COMPRA_OPER <> 0 OR
           RAEP.AEP_SM_DEV_COMPRA_OPER <> 0 OR RAEP.AEP_SC_VTA_OPER <> 0 OR
           RAEP.AEP_SL_VTA_OPER <> 0 OR RAEP.AEP_SM_VTA_OPER <> 0 OR
           RAEP.AEP_EC_DEV_VTA_OPER <> 0 OR RAEP.AEP_EL_DEV_VTA_OPER <> 0 OR
           RAEP.AEP_EM_DEV_VTA_OPER <> 0 OR RAEP.AEP_EC_PROD_OPER <> 0 OR
           RAEP.AEP_EL_PROD_OPER <> 0 OR RAEP.AEP_EM_PROD_OPER <> 0 OR
           RAEP.AEP_EC_DEV_PROD_OPER <> 0 OR RAEP.AEP_EL_DEV_PROD_OPER <> 0 OR
           RAEP.AEP_EM_DEV_PROD_OPER <> 0 OR RAEP.AEP_SC_PROD_OPER <> 0 OR
           RAEP.AEP_EC_KIT_OPER <> 0 OR RAEP.AEP_EL_KIT_OPER <> 0 OR
           RAEP.AEP_EM_KIT_OPER <> 0 OR RAEP.AEP_SC_KIT_OPER <> 0 OR
           RAEP.AEP_SL_KIT_OPER <> 0 OR RAEP.AEP_SM_KIT_OPER <> 0 OR
           RAEP.AEP_SC_PERDIDA_OPER <> 0 OR RAEP.AEP_SC_CONS_OPER <> 0 OR
           RAEP.AEP_C_DIF_INV_OPER <> 0 OR RAEP.AEP_C_TRANSF_OPER <> 0 OR
           RAEP.AEP_C_REM_OPER <> 0 OR RAEP.AEP_EC_ARM_DES_OPER <> 0 OR
           RAEP.AEP_EL_ARM_DES_OPER <> 0 OR RAEP.AEP_EM_ARM_DES_OPER <> 0 OR
           RAEP.AEP_SC_ARM_DES_OPER <> 0 OR RAEP.AEP_SL_ARM_DES_OPER <> 0 OR
           RAEP.AEP_SM_ARM_DES_OPER <> 0 THEN
        
          V_MESSAGE := 'Error 605: Cantidades incorrectas' ||
                       ' en STK_ART_EMPR_PERI para ' || 'Empr=' ||
                       TO_CHAR(RAEP.AEP_EMPR) || ', Art=' ||
                       TO_CHAR(RAEP.AEP_ART) || '. Desea corregir?';
        
          UPDATE STK_ART_EMPR_PERI
             SET AEP_TOT_EXIST_OPER      = AEP_EXIST_INI_OPER,
                 AEP_COSTO_PROM_LOC_OPER = AEP_COSTO_PROM_INI_LOC_OPER,
                 AEP_COSTO_PROM_MON_OPER = AEP_COSTO_PROM_INI_MON_OPER,
                 AEP_EC_COMPRA_OPER      = 0,
                 AEP_EL_COMPRA_OPER      = 0,
                 AEP_EM_COMPRA_OPER      = 0,
                 AEP_SC_DEV_COMPRA_OPER  = 0,
                 AEP_SL_DEV_COMPRA_OPER  = 0,
                 AEP_SM_DEV_COMPRA_OPER  = 0,
                 AEP_SC_VTA_OPER         = 0,
                 AEP_SL_VTA_OPER         = 0,
                 AEP_SM_VTA_OPER         = 0,
                 AEP_EC_DEV_VTA_OPER     = 0,
                 AEP_EL_DEV_VTA_OPER     = 0,
                 AEP_EM_DEV_VTA_OPER     = 0,
                 AEP_EC_PROD_OPER        = 0,
                 AEP_EL_PROD_OPER        = 0,
                 AEP_EM_PROD_OPER        = 0,
                 AEP_EC_DEV_PROD_OPER    = 0,
                 AEP_EL_DEV_PROD_OPER    = 0,
                 AEP_EM_DEV_PROD_OPER    = 0,
                 AEP_SC_PROD_OPER        = 0,
                 AEP_EC_KIT_OPER         = 0,
                 AEP_EL_KIT_OPER         = 0,
                 AEP_EM_KIT_OPER         = 0,
                 AEP_SC_KIT_OPER         = 0,
                 AEP_SL_KIT_OPER         = 0,
                 AEP_SM_KIT_OPER         = 0,
                 AEP_SC_PERDIDA_OPER     = 0,
                 AEP_SC_CONS_OPER        = 0,
                 AEP_C_DIF_INV_OPER      = 0,
                 AEP_C_TRANSF_OPER       = 0,
                 AEP_C_REM_OPER          = 0,
                 AEP_EC_ARM_DES_OPER     = 0,
                 AEP_EL_ARM_DES_OPER     = 0,
                 AEP_EM_ARM_DES_OPER     = 0,
                 AEP_SC_ARM_DES_OPER     = 0,
                 AEP_SL_ARM_DES_OPER     = 0,
                 AEP_SM_ARM_DES_OPER     = 0
           WHERE AEP_PERIODO = I_PERIODO
             AND AEP_EMPR = RAEP.AEP_EMPR
             AND AEP_ART = RAEP.AEP_ART;
        END IF;
        FETCH AEP_CUR
          INTO RAEP;
      
      END LOOP;
    END IF;
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20019, SQLERRM || ' ' || V_MESSAGE);
    
  END PP_VERIF_AEP_OPER;

  PROCEDURE PP_VERIF_EST(I_EMPRESA IN NUMBER, I_ART IN NUMBER DEFAULT NULL) IS
  
    CURSOR MOVI_CUR(I_PERI_ACT_FEC_INI  IN DATE,
                    I_PERI_SGTE_FEC_INI IN DATE) IS
    
      SELECT DOCU_EMPR,
             DETA_ART,
             ROUND(SUM(DECODE(TO_CHAR(DOCU_FEC_EMIS, 'MM'),
                              TO_CHAR(I_PERI_ACT_FEC_INI, 'MM'),
                              DECODE(OPER_DESC,
                                     'VENTA',
                                     DETA_CANT,
                                     'ANUL_NC',
                                     DETA_CANT,
                                     'DEV_VENTA',
                                     -DETA_CANT,
                                     'ANUL_VENTA',
                                     -DETA_CANT,
                                     0),
                              0)),
                   2) CANT_VTA_ACT,
             ROUND(SUM(DECODE(TO_CHAR(DOCU_FEC_EMIS, 'MM'),
                              TO_CHAR(I_PERI_ACT_FEC_INI, 'MM'),
                              DECODE(OPER_DESC,
                                     'PERDIDA',
                                     DETA_CANT,
                                     'SAL_PROD',
                                     DETA_CANT,
                                     'CONSUMO',
                                     DETA_CANT,
                                     'SAL_KIT',
                                     DETA_CANT,
                                     'DEV_PROD',
                                     -DETA_CANT,
                                     0),
                              0)),
                   2) CANT_TAL_ACT,
             ROUND(SUM(DECODE(TO_CHAR(DOCU_FEC_EMIS, 'MM'),
                              TO_CHAR(I_PERI_SGTE_FEC_INI, 'MM'),
                              DECODE(OPER_DESC,
                                     'VENTA',
                                     DETA_CANT,
                                     'ANUL_NC',
                                     DETA_CANT,
                                     'DEV_VENTA',
                                     -DETA_CANT,
                                     'ANUL_VENTA',
                                     -DETA_CANT,
                                     0),
                              0)),
                   2) CANT_VTA_SGTE,
             ROUND(SUM(DECODE(TO_CHAR(DOCU_FEC_EMIS, 'MM'),
                              TO_CHAR(I_PERI_SGTE_FEC_INI, 'MM'),
                              DECODE(OPER_DESC,
                                     'PERDIDA',
                                     DETA_CANT,
                                     'SAL_PROD',
                                     DETA_CANT,
                                     'CONSUMO',
                                     DETA_CANT,
                                     'SAL_KIT',
                                     DETA_CANT,
                                     'DEV_PROD',
                                     -DETA_CANT,
                                     0),
                              0)),
                   2) CANT_TAL_SGTE
        FROM STK_DOCUMENTO, STK_DOCUMENTO_DET, STK_OPERACION
      
       WHERE DOCU_EMPR = I_EMPRESA
         AND DOCU_FEC_EMIS BETWEEN I_PERI_ACT_FEC_INI AND
             I_PERI_SGTE_FEC_INI
         AND DOCU_CLAVE = DETA_CLAVE_DOC
         AND DOCU_EMPR = DETA_EMPR
         AND DOCU_CODIGO_OPER = OPER_CODIGO
         AND DOCU_EMPR = OPER_EMPR
      --   AND (DETA_ART = I_ART OR I_ART IS NULL) --*
         AND OPER_DESC IN ('VENTA',
                           'ANUL_NC',
                           'DEV_VENTA',
                           'PERDIDA',
                           'SAL_PROD',
                           'CONSUMO',
                           'SAL_KIT',
                           'DEV_PROD',
                           'ANUL_VENTA')
      
       GROUP BY DOCU_EMPR, DETA_ART
       ORDER BY DOCU_EMPR, DETA_ART;
  
    -- CURSOR PARA OBTENER LOS TOTALES DE STK_ESTADISTICA
    CURSOR EST_CUR IS
      SELECT EST_EMPR,
             EST_ART,
             EST_MES_VTA_01,
             EST_MES_VTA_02,
             EST_MES_VTA_03,
             EST_MES_VTA_04,
             EST_MES_VTA_05,
             EST_MES_VTA_06,
             EST_MES_VTA_SGTE,
             EST_MES_TAL_01,
             EST_MES_TAL_02,
             EST_MES_TAL_03,
             EST_MES_TAL_04,
             EST_MES_TAL_05,
             EST_MES_TAL_06,
             EST_MES_TAL_SGTE
        FROM STK_ESTADISTICA ET
       WHERE EST_EMPR = I_EMPRESA
  --       AND (ET.EST_ART = I_ART OR I_ART IS NULL) --*
       ORDER BY EST_EMPR, EST_ART
         FOR UPDATE OF EST_EMPR, EST_ART, EST_MES_VTA_01, EST_MES_VTA_02, EST_MES_VTA_03, EST_MES_VTA_04, EST_MES_VTA_05, EST_MES_VTA_06, EST_MES_VTA_SGTE, EST_MES_TAL_01, EST_MES_TAL_02, EST_MES_TAL_03, EST_MES_TAL_04, EST_MES_TAL_05, EST_MES_TAL_06, EST_MES_TAL_SGTE;
  
    V_MESSAGE VARCHAR2(200);
    -- VARIABLES PARA EL FETCH DE MOVIMIENTOS
    V_ART           NUMBER;
    V_EMPR          NUMBER;
    V_CANT_VTA_ACT  NUMBER;
    V_CANT_TAL_ACT  NUMBER;
    V_CANT_VTA_SGTE NUMBER;
    V_CANT_TAL_SGTE NUMBER;
    -- VARIABLES PARA EL FETCH DE ESTADISTICAS
    V_EST_ART          NUMBER;
    V_EST_EMPR         NUMBER;
    V_EST_MES_VTA_01   NUMBER;
    V_EST_MES_VTA_02   NUMBER;
    V_EST_MES_VTA_03   NUMBER;
    V_EST_MES_VTA_04   NUMBER;
    V_EST_MES_VTA_05   NUMBER;
    V_EST_MES_VTA_06   NUMBER;
    V_EST_MES_VTA_SGTE NUMBER;
    V_EST_MES_TAL_01   NUMBER;
    V_EST_MES_TAL_02   NUMBER;
    V_EST_MES_TAL_03   NUMBER;
    V_EST_MES_TAL_04   NUMBER;
    V_EST_MES_TAL_05   NUMBER;
    V_EST_MES_TAL_06   NUMBER;
    V_EST_MES_TAL_SGTE NUMBER;
    -- VARIABLES AUXILIARES PARA DETERMINAR LAS CANTIDADES DEL MES
    -- CORRESPONDIENTE
    V_MES_VTA_ACT NUMBER := NULL;
    V_MES_TAL_ACT NUMBER := NULL;
    V_VTA_01      NUMBER;
    V_VTA_02      NUMBER;
    V_VTA_03      NUMBER;
    V_VTA_04      NUMBER;
    V_VTA_05      NUMBER;
    V_VTA_06      NUMBER;
    V_VTA_SGTE    NUMBER;
    V_TAL_01      NUMBER;
    V_TAL_02      NUMBER;
    V_TAL_03      NUMBER;
    V_TAL_04      NUMBER;
    V_TAL_05      NUMBER;
    V_TAL_06      NUMBER;
    V_TAL_SGTE    NUMBER;
  
    V_PERI_ACT_FEC_INI  DATE;
    V_PERI_SGTE_FEC_INI DATE;
  
  BEGIN
  
    SELECT ACT.PERI_FEC_INI, SGT.PERI_FEC_INI
      INTO V_PERI_ACT_FEC_INI, V_PERI_SGTE_FEC_INI
      FROM STK_PERIODO ACT, STK_CONFIGURACION C, STK_PERIODO SGT
     WHERE ACT.PERI_CODIGO = C.CONF_PERIODO_ACT
       AND ACT.PERI_EMPR = C.CONF_EMPR
       AND SGT.PERI_CODIGO = C.CONF_PERIODO_SGTE
       AND SGT.PERI_EMPR = C.CONF_EMPR
       AND C.CONF_EMPR = I_EMPRESA;
  
    OPEN EST_CUR;
    FETCH EST_CUR
      INTO V_EST_EMPR,
           V_EST_ART,
           V_EST_MES_VTA_01,
           V_EST_MES_VTA_02,
           V_EST_MES_VTA_03,
           V_EST_MES_VTA_04,
           V_EST_MES_VTA_05,
           V_EST_MES_VTA_06,
           V_EST_MES_VTA_SGTE,
           V_EST_MES_TAL_01,
           V_EST_MES_TAL_02,
           V_EST_MES_TAL_03,
           V_EST_MES_TAL_04,
           V_EST_MES_TAL_05,
           V_EST_MES_TAL_06,
           V_EST_MES_TAL_SGTE;
  
    OPEN MOVI_CUR(I_PERI_ACT_FEC_INI  => V_PERI_ACT_FEC_INI,
                  I_PERI_SGTE_FEC_INI => V_PERI_SGTE_FEC_INI);
    FETCH MOVI_CUR
      INTO V_EMPR,
           V_ART,
           V_CANT_VTA_ACT,
           V_CANT_TAL_ACT,
           V_CANT_VTA_SGTE,
           V_CANT_TAL_SGTE;
  
    LOOP
      V_MESSAGE := 'Empr=' || TO_CHAR(V_EMPR) || ', Art=' || TO_CHAR(V_ART);
    
      EXIT WHEN MOVI_CUR%NOTFOUND OR EST_CUR%NOTFOUND;
    
      LOOP
        EXIT WHEN MOVI_CUR%NOTFOUND OR(V_EMPR > V_EST_EMPR OR
                                       (V_EMPR = V_EST_EMPR AND
                                       V_ART >= V_EST_ART));
        -- ENTRA AQUI SI EXISTEN MOVIMIENTOS SIN TOTALES EN
        -- ESTADISTICA
        V_MESSAGE := 'Error 751: Falta reg. en STK_ESTADISTICA' ||
                     ' para Empr=' || TO_CHAR(V_EMPR) || ', Art=' ||
                     TO_CHAR(V_ART) || '. Desea corregir?';
      
        PP_ESTABLECER_MES(V_VTA_01           => V_VTA_01,
                          V_VTA_02           => V_VTA_02,
                          V_VTA_03           => V_VTA_03,
                          V_VTA_04           => V_VTA_04,
                          V_VTA_05           => V_VTA_05,
                          V_VTA_06           => V_VTA_06,
                          V_TAL_01           => V_TAL_01,
                          V_TAL_02           => V_TAL_02,
                          V_TAL_03           => V_TAL_03,
                          V_TAL_04           => V_TAL_04,
                          V_TAL_05           => V_TAL_05,
                          V_TAL_06           => V_TAL_06,
                          V_CANT_VTA_ACT     => V_CANT_VTA_ACT,
                          V_CANT_TAL_ACT     => V_CANT_TAL_ACT,
                          V_PERI_ACT_FEC_INI => V_PERI_ACT_FEC_INI);
      
        INSERT INTO STK_ESTADISTICA
          (EST_EMPR,
           EST_ART,
           EST_SEM_01,
           EST_SEM_02,
           EST_SEM_03,
           EST_SEM_04,
           EST_SEM_05,
           EST_SEM_06,
           EST_SEM_07,
           EST_SEM_08,
           EST_SEM_09,
           EST_SEM_10,
           EST_MES_VTA_01,
           EST_MES_TAL_01,
           EST_MES_VTA_02,
           EST_MES_TAL_02,
           EST_MES_VTA_03,
           EST_MES_TAL_03,
           EST_MES_VTA_04,
           EST_MES_TAL_04,
           EST_MES_VTA_05,
           EST_MES_TAL_05,
           EST_MES_VTA_06,
           EST_MES_TAL_06,
           EST_MES_VTA_SGTE,
           EST_MES_TAL_SGTE)
        VALUES
          (V_EMPR,
           V_ART,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           V_VTA_01,
           V_TAL_01,
           V_VTA_02,
           V_TAL_02,
           V_VTA_03,
           V_TAL_03,
           V_VTA_04,
           V_TAL_04,
           V_VTA_05,
           V_TAL_05,
           V_VTA_06,
           V_TAL_06,
           V_CANT_VTA_SGTE,
           V_CANT_TAL_SGTE);
      
        FETCH MOVI_CUR
          INTO V_EMPR,
               V_ART,
               V_CANT_VTA_ACT,
               V_CANT_TAL_ACT,
               V_CANT_VTA_SGTE,
               V_CANT_TAL_SGTE;
      END LOOP;
    
      LOOP
        EXIT WHEN EST_CUR%NOTFOUND OR(V_EST_EMPR > V_EMPR OR
                                      (V_EST_EMPR = V_EMPR AND
                                      V_EST_ART >= V_ART));
        -- ENTRA AQUI SI EXISTEN REG.MAESTROS SIN MOVIMIENTOS
      
        STKP002.PP_ESTABLECER_CANT_EST(V_EST_MES_VTA_01   => V_EST_MES_VTA_01,
                                       V_EST_MES_VTA_02   => V_EST_MES_VTA_02,
                                       V_EST_MES_VTA_03   => V_EST_MES_VTA_03,
                                       V_EST_MES_VTA_04   => V_EST_MES_VTA_04,
                                       V_EST_MES_VTA_05   => V_EST_MES_VTA_05,
                                       V_EST_MES_VTA_06   => V_EST_MES_VTA_06,
                                       V_EST_MES_TAL_01   => V_EST_MES_TAL_01,
                                       V_EST_MES_TAL_02   => V_EST_MES_TAL_02,
                                       V_EST_MES_TAL_03   => V_EST_MES_TAL_03,
                                       V_EST_MES_TAL_04   => V_EST_MES_TAL_04,
                                       V_EST_MES_TAL_05   => V_EST_MES_TAL_05,
                                       V_EST_MES_TAL_06   => V_EST_MES_TAL_06,
                                       V_MES_VTA_ACT      => V_MES_VTA_ACT,
                                       V_MES_TAL_ACT      => V_MES_TAL_ACT,
                                       V_CANTIDAD_VTA     => NULL,
                                       V_CANTIDAD_TAL     => NULL,
                                       V_PERI_ACT_FEC_INI => V_PERI_ACT_FEC_INI);
      
        IF NVL(V_EST_MES_TAL_SGTE, 0) <> 0 OR
           NVL(V_EST_MES_VTA_SGTE, 0) <> 0 OR NVL(V_MES_VTA_ACT, 0) <> 0 OR
           NVL(V_MES_TAL_ACT, 0) <> 0 THEN
          V_MESSAGE := 'Error 752: estadistica incorrecta' ||
                       ' en STK_ESTADISTICA para ' || 'Empr=' ||
                       TO_CHAR(V_EST_EMPR) || ', Art=' ||
                       TO_CHAR(V_EST_ART) || '. Desea corregir?';
        
          UPDATE STK_ESTADISTICA
             SET EST_MES_VTA_01   = V_EST_MES_VTA_01,
                 EST_MES_TAL_01   = V_EST_MES_TAL_01,
                 EST_MES_VTA_02   = V_EST_MES_VTA_02,
                 EST_MES_TAL_02   = V_EST_MES_TAL_02,
                 EST_MES_VTA_03   = V_EST_MES_VTA_03,
                 EST_MES_TAL_03   = V_EST_MES_TAL_03,
                 EST_MES_VTA_04   = V_EST_MES_VTA_04,
                 EST_MES_TAL_04   = V_EST_MES_TAL_04,
                 EST_MES_VTA_05   = V_EST_MES_VTA_05,
                 EST_MES_TAL_05   = V_EST_MES_TAL_05,
                 EST_MES_VTA_06   = V_EST_MES_VTA_06,
                 EST_MES_TAL_06   = V_EST_MES_TAL_06,
                 EST_MES_VTA_SGTE = NULL,
                 EST_MES_TAL_SGTE = NULL
           WHERE CURRENT OF EST_CUR;
        END IF;
        FETCH EST_CUR
          INTO V_EST_EMPR,
               V_EST_ART,
               V_EST_MES_VTA_01,
               V_EST_MES_VTA_02,
               V_EST_MES_VTA_03,
               V_EST_MES_VTA_04,
               V_EST_MES_VTA_05,
               V_EST_MES_VTA_06,
               V_EST_MES_VTA_SGTE,
               V_EST_MES_TAL_01,
               V_EST_MES_TAL_02,
               V_EST_MES_TAL_03,
               V_EST_MES_TAL_04,
               V_EST_MES_TAL_05,
               V_EST_MES_TAL_06,
               V_EST_MES_TAL_SGTE;
      END LOOP;
    
      EXIT WHEN MOVI_CUR%NOTFOUND OR EST_CUR%NOTFOUND;
      IF V_EMPR = V_EST_EMPR AND V_ART = V_EST_ART THEN
        -- ENTRA AQUI SI EXISTEN MOVIMIENTOS Y SU REG.MAESTRO
      
        -- CALL THE PROCEDURE
        STKP002.PP_ESTABLECER_CANT_EST(V_EST_MES_VTA_01   => V_EST_MES_VTA_01,
                                       V_EST_MES_VTA_02   => V_EST_MES_VTA_02,
                                       V_EST_MES_VTA_03   => V_EST_MES_VTA_03,
                                       V_EST_MES_VTA_04   => V_EST_MES_VTA_04,
                                       V_EST_MES_VTA_05   => V_EST_MES_VTA_05,
                                       V_EST_MES_VTA_06   => V_EST_MES_VTA_06,
                                       V_EST_MES_TAL_01   => V_EST_MES_TAL_01,
                                       V_EST_MES_TAL_02   => V_EST_MES_TAL_02,
                                       V_EST_MES_TAL_03   => V_EST_MES_TAL_03,
                                       V_EST_MES_TAL_04   => V_EST_MES_TAL_04,
                                       V_EST_MES_TAL_05   => V_EST_MES_TAL_05,
                                       V_EST_MES_TAL_06   => V_EST_MES_TAL_06,
                                       V_MES_VTA_ACT      => V_MES_VTA_ACT,
                                       V_MES_TAL_ACT      => V_MES_TAL_ACT,
                                       V_CANTIDAD_VTA     => V_CANT_VTA_ACT,
                                       V_CANTIDAD_TAL     => V_CANT_TAL_ACT,
                                       V_PERI_ACT_FEC_INI => V_PERI_ACT_FEC_INI);
      
        IF NVL(V_EST_MES_TAL_SGTE, 0) <> V_CANT_TAL_SGTE OR
           NVL(V_EST_MES_VTA_SGTE, 0) <> V_CANT_VTA_SGTE OR
           NVL(V_MES_VTA_ACT, 0) <> V_CANT_VTA_ACT OR
           NVL(V_MES_TAL_ACT, 0) <> V_CANT_TAL_ACT
        
         THEN
          -- ACTUALIZAR SI HAY DIFERENCIAS ENTRE MOV.Y MAESTRO
          V_MESSAGE := 'Error 753: Cantidad incorrecta en STK_ESTADISTICA' ||
                       ' para Empr=' || TO_CHAR(V_EST_EMPR) || ', Art=' ||
                       TO_CHAR(V_EST_ART) || '. Desea corregir?';
        
          UPDATE STK_ESTADISTICA
             SET EST_MES_VTA_01   = V_EST_MES_VTA_01,
                 EST_MES_TAL_01   = V_EST_MES_TAL_01,
                 EST_MES_VTA_02   = V_EST_MES_VTA_02,
                 EST_MES_TAL_02   = V_EST_MES_TAL_02,
                 EST_MES_VTA_03   = V_EST_MES_VTA_03,
                 EST_MES_TAL_03   = V_EST_MES_TAL_03,
                 EST_MES_VTA_04   = V_EST_MES_VTA_04,
                 EST_MES_TAL_04   = V_EST_MES_TAL_04,
                 EST_MES_VTA_05   = V_EST_MES_VTA_05,
                 EST_MES_TAL_05   = V_EST_MES_TAL_05,
                 EST_MES_VTA_06   = V_EST_MES_VTA_06,
                 EST_MES_TAL_06   = V_EST_MES_TAL_06,
                 EST_MES_VTA_SGTE = V_CANT_VTA_SGTE,
                 EST_MES_TAL_SGTE = V_CANT_TAL_SGTE
           WHERE CURRENT OF EST_CUR;
        
        END IF;
      
        FETCH MOVI_CUR
          INTO V_EMPR,
               V_ART,
               V_CANT_VTA_ACT,
               V_CANT_TAL_ACT,
               V_CANT_VTA_SGTE,
               V_CANT_TAL_SGTE;
        FETCH EST_CUR
          INTO V_EST_EMPR,
               V_EST_ART,
               V_EST_MES_VTA_01,
               V_EST_MES_VTA_02,
               V_EST_MES_VTA_03,
               V_EST_MES_VTA_04,
               V_EST_MES_VTA_05,
               V_EST_MES_VTA_06,
               V_EST_MES_VTA_SGTE,
               V_EST_MES_TAL_01,
               V_EST_MES_TAL_02,
               V_EST_MES_TAL_03,
               V_EST_MES_TAL_04,
               V_EST_MES_TAL_05,
               V_EST_MES_TAL_06,
               V_EST_MES_TAL_SGTE;
      END IF;
    
    END LOOP;
  
    IF MOVI_CUR%FOUND AND EST_CUR%NOTFOUND THEN
      LOOP
        EXIT WHEN MOVI_CUR%NOTFOUND;
        -- ENTRA AQUI SI EXISTEN MOVIMIENTOS SIN MAESTRO
        V_MESSAGE := 'Error 754: Falta reg. en STK_ESTADISTICA para ' ||
                     ' para Empr=' || TO_CHAR(V_EMPR) || ', Art=' ||
                     TO_CHAR(V_ART) || '. Desea corregir?';
      
        STKP002.PP_ESTABLECER_MES(V_VTA_01           => V_VTA_01,
                                  V_VTA_02           => V_VTA_02,
                                  V_VTA_03           => V_VTA_03,
                                  V_VTA_04           => V_VTA_04,
                                  V_VTA_05           => V_VTA_05,
                                  V_VTA_06           => V_VTA_06,
                                  V_TAL_01           => V_TAL_01,
                                  V_TAL_02           => V_TAL_02,
                                  V_TAL_03           => V_TAL_03,
                                  V_TAL_04           => V_TAL_04,
                                  V_TAL_05           => V_TAL_05,
                                  V_TAL_06           => V_TAL_06,
                                  V_CANT_VTA_ACT     => V_CANT_VTA_ACT,
                                  V_CANT_TAL_ACT     => V_CANT_TAL_ACT,
                                  V_PERI_ACT_FEC_INI => V_PERI_ACT_FEC_INI);
      
        V_VTA_SGTE := V_CANT_VTA_SGTE;
        V_TAL_SGTE := V_CANT_TAL_SGTE;
        INSERT INTO STK_ESTADISTICA
          (EST_EMPR,
           EST_ART,
           EST_SEM_01,
           EST_SEM_02,
           EST_SEM_03,
           EST_SEM_04,
           EST_SEM_05,
           EST_SEM_06,
           EST_SEM_07,
           EST_SEM_08,
           EST_SEM_09,
           EST_SEM_10,
           EST_MES_VTA_01,
           EST_MES_TAL_01,
           EST_MES_VTA_02,
           EST_MES_TAL_02,
           EST_MES_VTA_03,
           EST_MES_TAL_03,
           EST_MES_VTA_04,
           EST_MES_TAL_04,
           EST_MES_VTA_05,
           EST_MES_TAL_05,
           EST_MES_VTA_06,
           EST_MES_TAL_06,
           EST_MES_VTA_SGTE,
           EST_MES_TAL_SGTE)
        VALUES
          (V_EMPR,
           V_ART,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           V_VTA_01,
           V_TAL_01,
           V_VTA_02,
           V_TAL_02,
           V_VTA_03,
           V_TAL_03,
           V_VTA_04,
           V_TAL_04,
           V_VTA_05,
           V_TAL_05,
           V_VTA_06,
           V_TAL_06,
           V_VTA_SGTE,
           V_TAL_SGTE);
      
        FETCH MOVI_CUR
          INTO V_EMPR,
               V_ART,
               V_CANT_VTA_ACT,
               V_CANT_TAL_ACT,
               V_CANT_VTA_SGTE,
               V_CANT_TAL_SGTE;
      END LOOP;
    END IF;
  
    IF MOVI_CUR%NOTFOUND AND EST_CUR%FOUND THEN
      LOOP
        EXIT WHEN EST_CUR%NOTFOUND;
        -- ENTRA AQUI SI EXISTEN MAESTROS SIN MOVIMIENTOS
      
        STKP002.PP_ESTABLECER_CANT_EST(V_EST_MES_VTA_01   => V_EST_MES_VTA_01,
                                       V_EST_MES_VTA_02   => V_EST_MES_VTA_02,
                                       V_EST_MES_VTA_03   => V_EST_MES_VTA_03,
                                       V_EST_MES_VTA_04   => V_EST_MES_VTA_04,
                                       V_EST_MES_VTA_05   => V_EST_MES_VTA_05,
                                       V_EST_MES_VTA_06   => V_EST_MES_VTA_06,
                                       V_EST_MES_TAL_01   => V_EST_MES_TAL_01,
                                       V_EST_MES_TAL_02   => V_EST_MES_TAL_02,
                                       V_EST_MES_TAL_03   => V_EST_MES_TAL_03,
                                       V_EST_MES_TAL_04   => V_EST_MES_TAL_04,
                                       V_EST_MES_TAL_05   => V_EST_MES_TAL_05,
                                       V_EST_MES_TAL_06   => V_EST_MES_TAL_06,
                                       V_MES_VTA_ACT      => V_MES_VTA_ACT,
                                       V_MES_TAL_ACT      => V_MES_TAL_ACT,
                                       V_CANTIDAD_VTA     => NULL,
                                       V_CANTIDAD_TAL     => NULL,
                                       V_PERI_ACT_FEC_INI => V_PERI_ACT_FEC_INI);
      
        IF NVL(V_EST_MES_TAL_SGTE, 0) <> 0 OR
           NVL(V_EST_MES_VTA_SGTE, 0) <> 0 OR NVL(V_MES_VTA_ACT, 0) <> 0 OR
           NVL(V_MES_TAL_ACT, 0) <> 0 THEN
          V_MESSAGE := 'Error 755: estadistica incorrecta' ||
                       ' en STK_ESTADISTICA para ' || 'Empr=' ||
                       TO_CHAR(V_EST_EMPR) || ', Art=' ||
                       TO_CHAR(V_EST_ART) || '. Desea corregir?';
        
          UPDATE STK_ESTADISTICA
             SET EST_MES_VTA_01   = V_EST_MES_VTA_01,
                 EST_MES_TAL_01   = V_EST_MES_TAL_01,
                 EST_MES_VTA_02   = V_EST_MES_VTA_02,
                 EST_MES_TAL_02   = V_EST_MES_TAL_02,
                 EST_MES_VTA_03   = V_EST_MES_VTA_03,
                 EST_MES_TAL_03   = V_EST_MES_TAL_03,
                 EST_MES_VTA_04   = V_EST_MES_VTA_04,
                 EST_MES_TAL_04   = V_EST_MES_TAL_04,
                 EST_MES_VTA_05   = V_EST_MES_VTA_05,
                 EST_MES_TAL_05   = V_EST_MES_TAL_05,
                 EST_MES_VTA_06   = V_EST_MES_VTA_06,
                 EST_MES_TAL_06   = V_EST_MES_TAL_06,
                 EST_MES_VTA_SGTE = NULL,
                 EST_MES_TAL_SGTE = NULL
           WHERE CURRENT OF EST_CUR;
        
        END IF;
        FETCH EST_CUR
          INTO V_EST_EMPR,
               V_EST_ART,
               V_EST_MES_VTA_01,
               V_EST_MES_VTA_02,
               V_EST_MES_VTA_03,
               V_EST_MES_VTA_04,
               V_EST_MES_VTA_05,
               V_EST_MES_VTA_06,
               V_EST_MES_VTA_SGTE,
               V_EST_MES_TAL_01,
               V_EST_MES_TAL_02,
               V_EST_MES_TAL_03,
               V_EST_MES_TAL_04,
               V_EST_MES_TAL_05,
               V_EST_MES_TAL_06,
               V_EST_MES_TAL_SGTE;
      END LOOP;
    END IF;
  
    CLOSE EST_CUR;
    CLOSE MOVI_CUR;
    COMMIT;
  END PP_VERIF_EST;

  PROCEDURE PP_ESTABLECER_MES(V_VTA_01           IN OUT NUMBER,
                              V_VTA_02           IN OUT NUMBER,
                              V_VTA_03           IN OUT NUMBER,
                              V_VTA_04           IN OUT NUMBER,
                              V_VTA_05           IN OUT NUMBER,
                              V_VTA_06           IN OUT NUMBER,
                              V_TAL_01           IN OUT NUMBER,
                              V_TAL_02           IN OUT NUMBER,
                              V_TAL_03           IN OUT NUMBER,
                              V_TAL_04           IN OUT NUMBER,
                              V_TAL_05           IN OUT NUMBER,
                              V_TAL_06           IN OUT NUMBER,
                              V_CANT_VTA_ACT     IN NUMBER,
                              V_CANT_TAL_ACT     IN NUMBER,
                              V_PERI_ACT_FEC_INI IN DATE) IS
  
    V_LIMITE NUMBER := NULL;
  
  BEGIN
  
    IF TO_NUMBER(TO_CHAR(V_PERI_ACT_FEC_INI, 'MM')) > 6 THEN
      V_LIMITE := 6;
    ELSE
      V_LIMITE := 0;
    END IF;
  
    IF TO_NUMBER(TO_CHAR(V_PERI_ACT_FEC_INI, 'MM')) - V_LIMITE = 1 THEN
      V_VTA_01 := V_CANT_VTA_ACT;
      V_VTA_02 := NULL;
      V_VTA_03 := NULL;
      V_VTA_04 := NULL;
      V_VTA_05 := NULL;
      V_VTA_06 := NULL;
      V_TAL_01 := V_CANT_TAL_ACT;
      V_TAL_02 := NULL;
      V_TAL_03 := NULL;
      V_TAL_04 := NULL;
      V_TAL_05 := NULL;
      V_TAL_06 := NULL;
    ELSIF TO_NUMBER(TO_CHAR(V_PERI_ACT_FEC_INI, 'MM')) - V_LIMITE = 2 THEN
      V_VTA_01 := NULL;
      V_VTA_02 := V_CANT_VTA_ACT;
      V_VTA_03 := NULL;
      V_VTA_04 := NULL;
      V_VTA_05 := NULL;
      V_VTA_06 := NULL;
      V_TAL_01 := NULL;
      V_TAL_02 := V_CANT_TAL_ACT;
      V_TAL_03 := NULL;
      V_TAL_04 := NULL;
      V_TAL_05 := NULL;
      V_TAL_06 := NULL;
    ELSIF TO_NUMBER(TO_CHAR(V_PERI_ACT_FEC_INI, 'MM')) - V_LIMITE = 3 THEN
      V_VTA_01 := NULL;
      V_VTA_02 := NULL;
      V_VTA_03 := V_CANT_VTA_ACT;
      V_VTA_04 := NULL;
      V_VTA_05 := NULL;
      V_VTA_06 := NULL;
      V_TAL_01 := NULL;
      V_TAL_02 := NULL;
      V_TAL_03 := V_CANT_TAL_ACT;
      V_TAL_04 := NULL;
      V_TAL_05 := NULL;
      V_TAL_06 := NULL;
    ELSIF TO_NUMBER(TO_CHAR(V_PERI_ACT_FEC_INI, 'MM')) - V_LIMITE = 4 THEN
      V_VTA_01 := NULL;
      V_VTA_02 := NULL;
      V_VTA_03 := NULL;
      V_VTA_04 := V_CANT_VTA_ACT;
      V_VTA_05 := NULL;
      V_VTA_06 := NULL;
      V_TAL_01 := NULL;
      V_TAL_02 := NULL;
      V_TAL_03 := NULL;
      V_TAL_04 := V_CANT_TAL_ACT;
      V_TAL_05 := NULL;
      V_TAL_06 := NULL;
    ELSIF TO_NUMBER(TO_CHAR(V_PERI_ACT_FEC_INI, 'MM')) - V_LIMITE = 5 THEN
      V_VTA_01 := NULL;
      V_VTA_02 := NULL;
      V_VTA_03 := NULL;
      V_VTA_04 := NULL;
      V_VTA_05 := V_CANT_VTA_ACT;
      V_VTA_06 := NULL;
      V_TAL_01 := NULL;
      V_TAL_02 := NULL;
      V_TAL_03 := NULL;
      V_TAL_04 := NULL;
      V_TAL_05 := V_CANT_TAL_ACT;
      V_TAL_06 := NULL;
    ELSIF TO_NUMBER(TO_CHAR(V_PERI_ACT_FEC_INI, 'MM')) - V_LIMITE = 6 THEN
      V_VTA_01 := NULL;
      V_VTA_02 := NULL;
      V_VTA_03 := NULL;
      V_VTA_04 := NULL;
      V_VTA_05 := NULL;
      V_VTA_06 := V_CANT_VTA_ACT;
      V_TAL_01 := NULL;
      V_TAL_02 := NULL;
      V_TAL_03 := NULL;
      V_TAL_04 := NULL;
      V_TAL_05 := NULL;
      V_TAL_06 := V_CANT_TAL_ACT;
    END IF;
  
  END PP_ESTABLECER_MES;

  PROCEDURE PP_ESTABLECER_CANT_EST(V_EST_MES_VTA_01   IN OUT NUMBER,
                                   V_EST_MES_VTA_02   IN OUT NUMBER,
                                   V_EST_MES_VTA_03   IN OUT NUMBER,
                                   V_EST_MES_VTA_04   IN OUT NUMBER,
                                   V_EST_MES_VTA_05   IN OUT NUMBER,
                                   V_EST_MES_VTA_06   IN OUT NUMBER,
                                   V_EST_MES_TAL_01   IN OUT NUMBER,
                                   V_EST_MES_TAL_02   IN OUT NUMBER,
                                   V_EST_MES_TAL_03   IN OUT NUMBER,
                                   V_EST_MES_TAL_04   IN OUT NUMBER,
                                   V_EST_MES_TAL_05   IN OUT NUMBER,
                                   V_EST_MES_TAL_06   IN OUT NUMBER,
                                   V_MES_VTA_ACT      OUT NUMBER,
                                   V_MES_TAL_ACT      OUT NUMBER,
                                   V_CANTIDAD_VTA     IN NUMBER,
                                   V_CANTIDAD_TAL     IN NUMBER,
                                   V_PERI_ACT_FEC_INI IN DATE) IS
  
    V_LIMITE NUMBER := NULL;
  BEGIN
    IF TO_NUMBER(TO_CHAR(V_PERI_ACT_FEC_INI, 'MM')) > 6 THEN
      V_LIMITE := 6;
    ELSE
      V_LIMITE := 0;
    END IF;
  
    IF TO_NUMBER(TO_CHAR(V_PERI_ACT_FEC_INI, 'MM')) - V_LIMITE = 1 THEN
      V_MES_VTA_ACT    := V_EST_MES_VTA_01;
      V_MES_TAL_ACT    := V_EST_MES_TAL_01;
      V_EST_MES_VTA_01 := V_CANTIDAD_VTA;
      V_EST_MES_TAL_01 := V_CANTIDAD_TAL;
    ELSIF TO_NUMBER(TO_CHAR(V_PERI_ACT_FEC_INI, 'MM')) - V_LIMITE = 2 THEN
      V_MES_VTA_ACT    := V_EST_MES_VTA_02;
      V_MES_TAL_ACT    := V_EST_MES_TAL_02;
      V_EST_MES_VTA_02 := V_CANTIDAD_VTA;
      V_EST_MES_TAL_02 := V_CANTIDAD_TAL;
    ELSIF TO_NUMBER(TO_CHAR(V_PERI_ACT_FEC_INI, 'MM')) - V_LIMITE = 3 THEN
      V_MES_VTA_ACT    := V_EST_MES_VTA_03;
      V_MES_TAL_ACT    := V_EST_MES_TAL_03;
      V_EST_MES_VTA_03 := V_CANTIDAD_VTA;
      V_EST_MES_TAL_03 := V_CANTIDAD_TAL;
    ELSIF TO_NUMBER(TO_CHAR(V_PERI_ACT_FEC_INI, 'MM')) - V_LIMITE = 4 THEN
      V_MES_VTA_ACT    := V_EST_MES_VTA_04;
      V_MES_TAL_ACT    := V_EST_MES_TAL_04;
      V_EST_MES_VTA_04 := V_CANTIDAD_VTA;
      V_EST_MES_TAL_04 := V_CANTIDAD_TAL;
    ELSIF TO_NUMBER(TO_CHAR(V_PERI_ACT_FEC_INI, 'MM')) - V_LIMITE = 5 THEN
      V_MES_VTA_ACT    := V_EST_MES_VTA_05;
      V_MES_TAL_ACT    := V_EST_MES_TAL_05;
      V_EST_MES_VTA_05 := V_CANTIDAD_VTA;
      V_EST_MES_TAL_05 := V_CANTIDAD_TAL;
    ELSIF TO_NUMBER(TO_CHAR(V_PERI_ACT_FEC_INI, 'MM')) - V_LIMITE = 6 THEN
      V_MES_VTA_ACT    := V_EST_MES_VTA_06;
      V_MES_TAL_ACT    := V_EST_MES_TAL_06;
      V_EST_MES_VTA_06 := V_CANTIDAD_VTA;
      V_EST_MES_TAL_06 := V_CANTIDAD_TAL;
    END IF;
  
  END PP_ESTABLECER_CANT_EST;

  PROCEDURE PP_COMPARAR_ARDE_AEP(I_EMPRESA IN NUMBER,
                                 I_ART     IN NUMBER DEFAULT NULL) IS
  
    CURSOR ARDE_CUR IS
      SELECT ARDE_EMPR, ARDE_ART, SUM(ARDE_CANT_INI) ARDE_CANT_INI
        FROM STK_ARTICULO_DEPOSITO, STK_ARTICULO
       WHERE ARDE_EMPR = I_EMPRESA
         AND ART_CODIGO = ARDE_ART
         AND ART_EMPR = ARDE_EMPR
         AND ART_TIPO <> 4
       --  AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
      
       GROUP BY ARDE_EMPR, ARDE_ART
       ORDER BY ARDE_EMPR, ARDE_ART;
  
    CURSOR AEP_CUR(I_PERI_ACT IN NUMBER) IS
      SELECT AEP_EMPR, AEP_ART, AEP_EXIST_INI
        FROM STK_ART_EMPR_PERI, STK_ARTICULO
       WHERE AEP_EMPR = I_EMPRESA
         AND AEP_PERIODO = I_PERI_ACT
         AND ART_CODIGO = AEP_ART
         AND ART_EMPR = AEP_EMPR
            
         AND ART_TIPO <> 4
      --   AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
       ORDER BY AEP_EMPR, AEP_ART;
  
    V_MESSAGE VARCHAR2(200);
    --
    W_ARDE_EMPR     NUMBER;
    W_ARDE_ART      NUMBER;
    W_ARDE_CANT_INI NUMBER;
    --
    W_AEP_EMPR      NUMBER;
    W_AEP_ART       NUMBER;
    W_AEP_EXIST_INI NUMBER;
  
    V_PERI_CODIGO NUMBER;
  BEGIN
  
    SELECT ACT.PERI_CODIGO
      INTO V_PERI_CODIGO
      FROM STK_PERIODO ACT, STK_CONFIGURACION C
     WHERE ACT.PERI_CODIGO = C.CONF_PERIODO_ACT
       AND ACT.PERI_EMPR = C.CONF_EMPR
       AND C.CONF_EMPR = I_EMPRESA;
  
    OPEN ARDE_CUR;
    FETCH ARDE_CUR
      INTO W_ARDE_EMPR, W_ARDE_ART, W_ARDE_CANT_INI;
  
    OPEN AEP_CUR(I_PERI_ACT => V_PERI_CODIGO);
    FETCH AEP_CUR
      INTO W_AEP_EMPR, W_AEP_ART, W_AEP_EXIST_INI;
  
    LOOP
      V_MESSAGE := ('Emp:' || TO_CHAR(W_AEP_EMPR) || 'Art:' ||
                   TO_CHAR(W_AEP_ART));
    
      EXIT WHEN ARDE_CUR%NOTFOUND OR AEP_CUR%NOTFOUND;
    
      LOOP
        EXIT WHEN ARDE_CUR%NOTFOUND OR(W_ARDE_EMPR > W_AEP_EMPR OR
                                       (W_ARDE_EMPR = W_AEP_EMPR AND
                                       W_ARDE_ART >= W_AEP_ART));
        -- ENTRA AQUI SI EXISTEN ARDE SIN AEP
        IF W_ARDE_CANT_INI <> 0 THEN
          V_MESSAGE := ('Error 550: No existe AEP para ARDE_EMPR=' ||
                       TO_CHAR(W_ARDE_EMPR) || ', ARDE_ART=' ||
                       TO_CHAR(W_ARDE_ART) || '. Existencia:' ||
                       TO_CHAR(W_ARDE_CANT_INI) || '. Anote el error!');
        END IF;
        FETCH ARDE_CUR
          INTO W_ARDE_EMPR, W_ARDE_ART, W_ARDE_CANT_INI;
      END LOOP;
    
      LOOP
        EXIT WHEN AEP_CUR%NOTFOUND OR(W_AEP_EMPR > W_ARDE_EMPR OR
                                      (W_AEP_EMPR = W_ARDE_EMPR AND
                                      W_AEP_ART >= W_ARDE_ART));
        -- ENTRA AQUI SI EXISTEN AEP SIN ARDE
        IF W_AEP_EXIST_INI <> 0 THEN
          V_MESSAGE := ('Error 551: No existe ARDE para AEP_EMPR=' ||
                       TO_CHAR(W_AEP_EMPR) || ', AEP_ART=' ||
                       TO_CHAR(W_AEP_ART) || '. Existencia:' ||
                       TO_CHAR(W_AEP_EXIST_INI) || '. Anote el error!');
        END IF;
        FETCH AEP_CUR
          INTO W_AEP_EMPR, W_AEP_ART, W_AEP_EXIST_INI;
      END LOOP;
    
      EXIT WHEN ARDE_CUR%NOTFOUND OR AEP_CUR%NOTFOUND;
      IF W_ARDE_EMPR = W_AEP_EMPR AND W_ARDE_ART = W_AEP_ART THEN
        -- ENTRA AQUI SI EXISTEN ARDE Y AEP
        IF W_ARDE_CANT_INI <> W_AEP_EXIST_INI THEN
          V_MESSAGE := ('Err 552: Diferencia entre cantidades ARDE y AEP para ' ||
                       'EMPR:' || TO_CHAR(W_AEP_EMPR) || ', ART:' ||
                       TO_CHAR(W_AEP_ART) || ', cant_dif:' ||
                       TO_CHAR(W_AEP_EXIST_INI - W_ARDE_CANT_INI) ||
                       '. Anote el error!');
        END IF;
      
        FETCH AEP_CUR
          INTO W_AEP_EMPR, W_AEP_ART, W_AEP_EXIST_INI;
        FETCH ARDE_CUR
          INTO W_ARDE_EMPR, W_ARDE_ART, W_ARDE_CANT_INI;
      END IF;
    
    END LOOP;
  
    IF ARDE_CUR%FOUND AND AEP_CUR%NOTFOUND THEN
      LOOP
        EXIT WHEN ARDE_CUR%NOTFOUND;
        -- ENTRA AQUI SI EXISTEN ARDE SIN AEP
        IF W_ARDE_CANT_INI <> 0 THEN
          V_MESSAGE := ('Error 553: No existe AEP para ARDE_EMPR=' ||
                       TO_CHAR(W_ARDE_EMPR) || ', ARDE_ART=' ||
                       TO_CHAR(W_ARDE_ART) || '. Existencia:' ||
                       TO_CHAR(W_ARDE_CANT_INI) || '. Anote el error!');
        END IF;
        FETCH ARDE_CUR
          INTO W_ARDE_EMPR, W_ARDE_ART, W_ARDE_CANT_INI;
      END LOOP;
    END IF;
  
    IF ARDE_CUR%NOTFOUND AND AEP_CUR%FOUND THEN
      LOOP
        EXIT WHEN AEP_CUR%NOTFOUND;
        -- ENTRA AQUI SI EXISTEN AEP SIN ARDE
        IF W_AEP_EXIST_INI <> 0 THEN
          V_MESSAGE := ('Error 554: No existe ARDE para AEP_EMPR=' ||
                       TO_CHAR(W_AEP_EMPR) || ', AEP_ART=' ||
                       TO_CHAR(W_AEP_ART) || '. Existencia:' ||
                       TO_CHAR(W_AEP_EXIST_INI) || '. Anote el error!');
        END IF;
        FETCH AEP_CUR
          INTO W_AEP_EMPR, W_AEP_ART, W_AEP_EXIST_INI;
      END LOOP;
    END IF;
    COMMIT;
  END PP_COMPARAR_ARDE_AEP;

  PROCEDURE PP_COMPARAR_ARDE_AEP_OPER(I_EMPRESA IN NUMBER,
                                      I_ART     IN NUMBER DEFAULT NULL) IS
  
    CURSOR ARDE_CUR IS
      SELECT ARDE_EMPR, ARDE_ART, SUM(ARDE_CANT_INI_OPER) ARDE_CANT_INI
        FROM STK_ARTICULO_DEPOSITO, STK_ARTICULO
       WHERE ARDE_EMPR = I_EMPRESA
         AND ART_CODIGO = ARDE_ART
         AND ART_EMPR = ARDE_EMPR
         AND ART_TIPO <> 4
    --    AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
      
       GROUP BY ARDE_EMPR, ARDE_ART
       ORDER BY ARDE_EMPR, ARDE_ART;
  
    CURSOR AEP_CUR(I_PERI_ACT IN NUMBER) IS
      SELECT AEP_EMPR, AEP_ART, AEP_EXIST_INI_OPER
        FROM STK_ART_EMPR_PERI, STK_ARTICULO
       WHERE AEP_EMPR = I_EMPRESA
         AND AEP_PERIODO = I_PERI_ACT
         AND ART_CODIGO = AEP_ART
         AND ART_EMPR = AEP_EMPR
            
         AND ART_TIPO <> 4
    --     AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
      
       ORDER BY AEP_EMPR, AEP_ART;
  
    V_MESSAGE VARCHAR2(200);
    --
    W_ARDE_EMPR     NUMBER;
    W_ARDE_ART      NUMBER;
    W_ARDE_CANT_INI NUMBER;
    --
    W_AEP_EMPR      NUMBER;
    W_AEP_ART       NUMBER;
    W_AEP_EXIST_INI NUMBER;
  
    V_PERI_CODIGO NUMBER;
  BEGIN
  
    SELECT ACT.PERI_CODIGO
      INTO V_PERI_CODIGO
      FROM STK_PERIODO ACT, STK_CONFIGURACION C
     WHERE ACT.PERI_CODIGO = C.CONF_PERIODO_ACT
       AND ACT.PERI_EMPR = C.CONF_EMPR
       AND C.CONF_EMPR = I_EMPRESA;
  
    OPEN ARDE_CUR;
    FETCH ARDE_CUR
      INTO W_ARDE_EMPR, W_ARDE_ART, W_ARDE_CANT_INI;
  
    OPEN AEP_CUR(I_PERI_ACT => V_PERI_CODIGO);
    FETCH AEP_CUR
      INTO W_AEP_EMPR, W_AEP_ART, W_AEP_EXIST_INI;
  
    LOOP
      V_MESSAGE := ('Emp:' || TO_CHAR(W_AEP_EMPR) || 'Art:' ||
                   TO_CHAR(W_AEP_ART));
    
      EXIT WHEN ARDE_CUR%NOTFOUND OR AEP_CUR%NOTFOUND;
    
      LOOP
        EXIT WHEN ARDE_CUR%NOTFOUND OR(W_ARDE_EMPR > W_AEP_EMPR OR
                                       (W_ARDE_EMPR = W_AEP_EMPR AND
                                       W_ARDE_ART >= W_AEP_ART));
        -- ENTRA AQUI SI EXISTEN ARDE SIN AEP
        IF W_ARDE_CANT_INI <> 0 THEN
          V_MESSAGE := ('Error 550: No existe AEP para ARDE_EMPR=' ||
                       TO_CHAR(W_ARDE_EMPR) || ', ARDE_ART=' ||
                       TO_CHAR(W_ARDE_ART) || '. Existencia:' ||
                       TO_CHAR(W_ARDE_CANT_INI) || '. Anote el error!');
        END IF;
        FETCH ARDE_CUR
          INTO W_ARDE_EMPR, W_ARDE_ART, W_ARDE_CANT_INI;
      END LOOP;
    
      LOOP
        EXIT WHEN AEP_CUR%NOTFOUND OR(W_AEP_EMPR > W_ARDE_EMPR OR
                                      (W_AEP_EMPR = W_ARDE_EMPR AND
                                      W_AEP_ART >= W_ARDE_ART));
        -- ENTRA AQUI SI EXISTEN AEP SIN ARDE
        IF W_AEP_EXIST_INI <> 0 THEN
          V_MESSAGE := ('Error 551: No existe ARDE para AEP_EMPR=' ||
                       TO_CHAR(W_AEP_EMPR) || ', AEP_ART=' ||
                       TO_CHAR(W_AEP_ART) || '. Existencia:' ||
                       TO_CHAR(W_AEP_EXIST_INI) || '. Anote el error!');
        END IF;
        FETCH AEP_CUR
          INTO W_AEP_EMPR, W_AEP_ART, W_AEP_EXIST_INI;
      END LOOP;
    
      EXIT WHEN ARDE_CUR%NOTFOUND OR AEP_CUR%NOTFOUND;
      IF W_ARDE_EMPR = W_AEP_EMPR AND W_ARDE_ART = W_AEP_ART THEN
        -- ENTRA AQUI SI EXISTEN ARDE Y AEP
        IF W_ARDE_CANT_INI <> W_AEP_EXIST_INI THEN
          V_MESSAGE := ('Err 552: Diferencia entre cant. ARDE_OPER y AEP_OPER para ' ||
                       'EMPR:' || TO_CHAR(W_AEP_EMPR) || ', ART:' ||
                       TO_CHAR(W_AEP_ART) || ', cant_dif:' ||
                       TO_CHAR(W_AEP_EXIST_INI - W_ARDE_CANT_INI) ||
                       '. Anote el error!');
        END IF;
      
        FETCH AEP_CUR
          INTO W_AEP_EMPR, W_AEP_ART, W_AEP_EXIST_INI;
        FETCH ARDE_CUR
          INTO W_ARDE_EMPR, W_ARDE_ART, W_ARDE_CANT_INI;
      END IF;
    
    END LOOP;
  
    IF ARDE_CUR%FOUND AND AEP_CUR%NOTFOUND THEN
      LOOP
        EXIT WHEN ARDE_CUR%NOTFOUND;
        -- ENTRA AQUI SI EXISTEN ARDE SIN AEP
        IF W_ARDE_CANT_INI <> 0 THEN
          V_MESSAGE := ('Error 553: No existe AEP para ARDE_EMPR=' ||
                       TO_CHAR(W_ARDE_EMPR) || ', ARDE_ART=' ||
                       TO_CHAR(W_ARDE_ART) || '. Existencia:' ||
                       TO_CHAR(W_ARDE_CANT_INI) || '. Anote el error!');
        END IF;
        FETCH ARDE_CUR
          INTO W_ARDE_EMPR, W_ARDE_ART, W_ARDE_CANT_INI;
      END LOOP;
    END IF;
  
    IF ARDE_CUR%NOTFOUND AND AEP_CUR%FOUND THEN
      LOOP
        EXIT WHEN AEP_CUR%NOTFOUND;
        -- ENTRA AQUI SI EXISTEN AEP SIN ARDE
        IF W_AEP_EXIST_INI <> 0 THEN
          V_MESSAGE := ('Error 554: No existe ARDE para AEP_EMPR=' ||
                       TO_CHAR(W_AEP_EMPR) || ', AEP_ART=' ||
                       TO_CHAR(W_AEP_ART) || '. Existencia:' ||
                       TO_CHAR(W_AEP_EXIST_INI) || '. Anote el error!');
        END IF;
        FETCH AEP_CUR
          INTO W_AEP_EMPR, W_AEP_ART, W_AEP_EXIST_INI;
      END LOOP;
    END IF;
    COMMIT;
  END PP_COMPARAR_ARDE_AEP_OPER;

  PROCEDURE PP_MESSAGE(I_MESSAGE IN VARCHAR2, I_EMPRESA IN NUMBER) AS
    
     V_DESTINATARIO VARCHAR2(600);
    V_CC           VARCHAR2(5000);
    V_CCO          VARCHAR2(200);
    V_ASUNTO_MAIL  VARCHAR2(500);
    V_HEAD_HTML    VARCHAR2(5000);
    V_FOOTER_HTML  VARCHAR2(5000);
    V_TABLE        CLOB;
    V_MENSAJE      CLOB;
    DESTINATARIO   VARCHAR2(600);
    COPIA          VARCHAR2(600);
    OCULTA         VARCHAR2(150);

  BEGIN
    /*      SELECT T.CO_PARA, T.CO_CC, T.CO_CCO
              INTO DESTINATARIO, COPIA, OCULTA
              FROM GEN_CORREOS T
             WHERE T.CO_ACCION = 'Verif_Sistema_stk';

      
   if V_MENSAJE is not null then
   SEND_EMAIL(P_SENDER     => 'NOREPLY',
             P_RECIPIENTS => DESTINATARIO,
             P_CC         => COPIA,
             P_BCC        => OCULTA,
             P_SUBJECT    => 'ERROR EN VERIFICION DE STK',
             P_MESSAGE    => SUBSTR(V_MENSAJE, 0, 30000),
             P_MIME_TYPE  => 'text/html');

  end if;
  */
  
  
    /* INSERT INTO STKP002_LOG
      (VFECHA, VMENSAJE, VEMPR)
    VALUES
      (SYSDATE, I_MESSAGE, I_EMPRESA);*/
    null;
  END PP_MESSAGE;

  PROCEDURE PP_RECALCULAR_ARDE(I_EMPRESA IN NUMBER,
                               I_PERIODO IN NUMBER,
                               I_ART     IN NUMBER DEFAULT NULL) IS
  
    V_PERI_FEC_INI     DATE;
    V_PERI_FEC_FIN     DATE;
    V_PERI_ACTUAL      NUMBER;
    V_PERI_FEC_SIG_INI DATE;
    V_PERI_FEC_SIG_FIN DATE;
    SALIR EXCEPTION;
    CURSOR AEP_OPER IS --RECALCULA LA CANTIDA INICIAL DE ARTICULO EMPRESA PERIODO POR FECHA DE OPERACION
      SELECT DOCU_EMPR,
             DETA_ART ARTICULO,
             SUM(CASE
                   WHEN DOCU_FEC_OPER < V_PERI_FEC_INI THEN
                    CASE
                      WHEN C.OPER_ENT_SAL = 'E' THEN
                       DETA_CANT
                      WHEN C.OPER_ENT_SAL = 'S' THEN
                       DETA_CANT * (-1)
                      ELSE
                       0
                    END
                   ELSE
                    0
                 END) CANT_INI,
             
             SUM(CASE
                   WHEN DOCU_FEC_OPER <= V_PERI_FEC_FIN THEN
                    CASE
                      WHEN C.OPER_ENT_SAL = 'E' THEN
                       DETA_CANT
                      WHEN C.OPER_ENT_SAL = 'S' THEN
                       DETA_CANT * (-1)
                      ELSE
                       0
                    END
                   ELSE
                    0
                 END) CANT_ACTUAL
      
        FROM STK_DOCUMENTO A, STK_DOCUMENTO_DET B, STK_OPERACION C
       WHERE A.DOCU_EMPR = DETA_EMPR
         AND A.DOCU_CLAVE = DETA_CLAVE_DOC
         AND DOCU_CODIGO_OPER = OPER_CODIGO
         AND DOCU_EMPR = OPER_EMPR
         AND DOCU_FEC_OPER <= V_PERI_FEC_FIN
            
         AND DOCU_EMPR = I_EMPRESA
   --      AND (DETA_ART = I_ART OR I_ART IS NULL) --*
      
       GROUP BY DETA_ART, DOCU_EMPR;
  
    CURSOR AEP_EMIS IS --RECALCULA LA CANTIDA INICIAL DE ARTICULO EMPRESA PERIODO  POR FECHA DE EMISION
      SELECT DOCU_EMPR,
             DETA_ART ARTICULO,
             SUM(CASE
                   WHEN DOCU_FEC_EMIS < V_PERI_FEC_INI THEN
                    CASE
                      WHEN C.OPER_ENT_SAL = 'E' THEN
                       DETA_CANT
                      WHEN C.OPER_ENT_SAL = 'S' THEN
                       DETA_CANT * (-1)
                      ELSE
                       0
                    END
                   ELSE
                    0
                 END) CANT_INI,
             
             SUM(CASE
                   WHEN DOCU_FEC_EMIS <= V_PERI_FEC_FIN THEN
                    CASE
                      WHEN C.OPER_ENT_SAL = 'E' THEN
                       DETA_CANT
                      WHEN C.OPER_ENT_SAL = 'S' THEN
                       DETA_CANT * (-1)
                      ELSE
                       0
                    END
                   ELSE
                    0
                 END) CANT_ACTUAL
      
        FROM STK_DOCUMENTO A, STK_DOCUMENTO_DET B, STK_OPERACION C
       WHERE A.DOCU_EMPR = DETA_EMPR
         AND A.DOCU_CLAVE = DETA_CLAVE_DOC
         AND DOCU_CODIGO_OPER = OPER_CODIGO
         AND DOCU_EMPR = OPER_EMPR
         AND DOCU_FEC_EMIS <= V_PERI_FEC_FIN
            
         AND DOCU_EMPR = I_EMPRESA
   --      AND (DETA_ART = I_ART OR I_ART IS NULL) --*
      
       GROUP BY DETA_ART, DOCU_EMPR;
  
    CURSOR ARDE_OPER IS ---ESTE CURSOR RECALCULA POR LA FECHA DE OPERACION
    
      SELECT DOCU_EMPR,
             DOCU_SUC_ORIG SUCURSAL,
             DOCU_DEP_ORIG DEPOSITO,
             DETA_ART ARTICULO,
             SUM(CASE
                   WHEN DOCU_FEC_OPER < V_PERI_FEC_INI THEN
                    CASE
                      WHEN C.OPER_ENT_SAL = 'E' THEN
                       DETA_CANT
                      WHEN C.OPER_ENT_SAL = 'S' THEN
                       DETA_CANT * (-1)
                      ELSE
                       0
                    END
                   ELSE
                    0
                 END) CANT_INI,
             
             SUM(CASE
                   WHEN DOCU_FEC_OPER <= V_PERI_FEC_FIN THEN
                    CASE
                      WHEN C.OPER_ENT_SAL = 'E' THEN
                       DETA_CANT
                      WHEN C.OPER_ENT_SAL = 'S' THEN
                       DETA_CANT * (-1)
                      ELSE
                       0
                    END
                   ELSE
                    0
                 END) CANT_ACTUAL,
             
             SUM(CASE
                   WHEN DOCU_FEC_OPER BETWEEN V_PERI_FEC_INI AND V_PERI_FEC_FIN THEN
                    CASE
                      WHEN C.OPER_ENT_SAL = 'E' THEN
                       DETA_CANT
                    
                      ELSE
                       0
                    END
                   ELSE
                    0
                 END) CANT_ENTRADA,
             
             SUM(CASE
                   WHEN DOCU_FEC_OPER BETWEEN V_PERI_FEC_INI AND V_PERI_FEC_FIN THEN
                    CASE
                      WHEN C.OPER_ENT_SAL = 'S' THEN
                       DETA_CANT
                      ELSE
                       0
                    END
                   ELSE
                    0
                 END) CANT_SALIDA
      
        FROM STK_DOCUMENTO A, STK_DOCUMENTO_DET B, STK_OPERACION C
       WHERE A.DOCU_EMPR = DETA_EMPR
         AND A.DOCU_CLAVE = DETA_CLAVE_DOC
         AND DOCU_CODIGO_OPER = OPER_CODIGO
         AND DOCU_EMPR = OPER_EMPR
         AND DOCU_FEC_OPER <= V_PERI_FEC_FIN
            
         AND DOCU_EMPR = I_EMPRESA
  --       AND (DETA_ART = I_ART OR I_ART IS NULL) --*
      
       GROUP BY DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART, DOCU_EMPR;
  
    CURSOR ARDE_EMIS IS ---ESTE CURSOR RECALCULA POR LA FECHA DE EMISION
    
      SELECT DOCU_EMPR,
             DOCU_SUC_ORIG SUCURSAL,
             DOCU_DEP_ORIG DEPOSITO,
             DETA_ART ARTICULO,
             SUM(CASE
                   WHEN DOCU_FEC_EMIS < V_PERI_FEC_INI THEN
                    CASE
                      WHEN C.OPER_ENT_SAL = 'E' THEN
                       DETA_CANT
                      WHEN C.OPER_ENT_SAL = 'S' THEN
                       DETA_CANT * (-1)
                      ELSE
                       0
                    END
                   ELSE
                    0
                 END) CANT_INI,
             
             SUM(CASE
                   WHEN DOCU_FEC_EMIS <= V_PERI_FEC_FIN THEN
                    CASE
                      WHEN C.OPER_ENT_SAL = 'E' THEN
                       DETA_CANT
                      WHEN C.OPER_ENT_SAL = 'S' THEN
                       DETA_CANT * (-1)
                      ELSE
                       0
                    END
                   ELSE
                    0
                 END) CANT_ACTUAL,
             
             SUM(CASE
                   WHEN DOCU_FEC_EMIS BETWEEN V_PERI_FEC_INI AND V_PERI_FEC_FIN THEN
                    CASE
                      WHEN C.OPER_ENT_SAL = 'E' THEN
                       DETA_CANT
                    
                      ELSE
                       0
                    END
                   ELSE
                    0
                 END) CANT_ENTRADA,
             
             SUM(CASE
                   WHEN DOCU_FEC_EMIS BETWEEN V_PERI_FEC_INI AND V_PERI_FEC_FIN THEN
                    CASE
                      WHEN C.OPER_ENT_SAL = 'S' THEN
                       DETA_CANT
                      ELSE
                       0
                    END
                   ELSE
                    0
                 END) CANT_SALIDA
      
        FROM STK_DOCUMENTO A, STK_DOCUMENTO_DET B, STK_OPERACION C
       WHERE A.DOCU_EMPR = DETA_EMPR
         AND A.DOCU_CLAVE = DETA_CLAVE_DOC
         AND DOCU_CODIGO_OPER = OPER_CODIGO
         AND DOCU_EMPR = OPER_EMPR
         AND DOCU_FEC_EMIS <= V_PERI_FEC_FIN
            
         AND DOCU_EMPR = I_EMPRESA
     --    AND (DETA_ART = I_ART OR I_ART IS NULL) --*
      
       GROUP BY DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART, DOCU_EMPR;
  
  BEGIN
    NULL;
  
    -- RAISE SALIR;--BLOQUEARE ESTO POR EL MOMENTO--APERALTA 08/08/2018
    -- RAISE_APPLICATION_ERROR(-20016,
    --                           'pruebas!');
  
    SELECT C.CONF_PERIODO_ACT,
           P.PERI_FEC_INI,
           P.PERI_FEC_FIN,
           PS.PERI_FEC_INI,
           PS.PERI_FEC_FIN
      INTO V_PERI_ACTUAL,
           V_PERI_FEC_INI,
           V_PERI_FEC_FIN,
           V_PERI_FEC_SIG_INI,
           V_PERI_FEC_SIG_FIN
      FROM STK_PERIODO P, STK_CONFIGURACION C, STK_PERIODO PS
     WHERE P.PERI_EMPR = I_EMPRESA
       AND P.PERI_CODIGO = I_PERIODO
       AND P.PERI_EMPR = CONF_EMPR
       AND C.CONF_PERIODO_SGTE = PS.PERI_CODIGO
       AND C.CONF_EMPR = PS.PERI_EMPR;
  
    IF V_PERI_ACTUAL = I_PERIODO THEN
      FOR R IN ARDE_OPER LOOP
        UPDATE STK_ARTICULO_DEPOSITO A
           SET A.ARDE_CANT_INI_OPER = R.CANT_INI,
               A.ARDE_CANT_ACT_OPER = R.CANT_ACTUAL,
               A.ARDE_CANT_ENT_OPER = R.CANT_ENTRADA,
               A.ARDE_CANT_SAL_OPER = R.CANT_SALIDA
         WHERE A.ARDE_EMPR = R.DOCU_EMPR
           AND ARDE_ART = R.ARTICULO
           AND A.ARDE_DEP = R.DEPOSITO
           AND ARDE_SUC = R.SUCURSAL;
      
      END LOOP;
    
      FOR R IN ARDE_EMIS LOOP
        UPDATE STK_ARTICULO_DEPOSITO A
           SET A.ARDE_CANT_INI = R.CANT_INI,
               A.ARDE_CANT_ACT = R.CANT_ACTUAL,
               A.ARDE_CANT_ENT = R.CANT_ENTRADA,
               A.ARDE_CANT_SAL = R.CANT_SALIDA
         WHERE A.ARDE_EMPR = R.DOCU_EMPR
           AND ARDE_ART = R.ARTICULO
           AND A.ARDE_DEP = R.DEPOSITO
           AND ARDE_SUC = R.SUCURSAL;
      
      END LOOP;
    
    ELSE
    
      FOR R IN ARDE_OPER LOOP
        UPDATE STK_ARTICULO_DEPOSITO_HIST A
           SET A.ARDE_CANT_INI_OPER = R.CANT_INI,
               A.ARDE_CANT_ACT_OPER = R.CANT_ACTUAL,
               A.ARDE_CANT_ENT_OPER = R.CANT_ENTRADA,
               A.ARDE_CANT_SAL_OPER = R.CANT_SALIDA
         WHERE A.ARDE_EMPR = R.DOCU_EMPR
           AND ARDE_ART = R.ARTICULO
           AND A.ARDE_DEP = R.DEPOSITO
           AND ARDE_SUC = R.SUCURSAL
           AND A.ARDE_PERIODO = I_PERIODO;
      
      END LOOP;
    
      FOR R IN ARDE_EMIS LOOP
        UPDATE STK_ARTICULO_DEPOSITO_HIST A
           SET A.ARDE_CANT_INI = R.CANT_INI,
               A.ARDE_CANT_ACT = R.CANT_ACTUAL,
               A.ARDE_CANT_ENT = R.CANT_ENTRADA,
               A.ARDE_CANT_SAL = R.CANT_SALIDA
         WHERE A.ARDE_EMPR = R.DOCU_EMPR
           AND ARDE_ART = R.ARTICULO
           AND A.ARDE_DEP = R.DEPOSITO
           AND ARDE_SUC = R.SUCURSAL
           AND A.ARDE_PERIODO = I_PERIODO;
      
      END LOOP;
    
    END IF;
  
    --     RECALCULAR LAS EXISTENCIAS INICIALES DE STK_ART_EMPR_PERIODO ANTES DE HACER LA VERIFICACION DE SISTEMA
  
    FOR R IN AEP_OPER LOOP
    
      UPDATE STK_ART_EMPR_PERI A
         SET A.AEP_EXIST_INI_OPER = R.CANT_INI,
             A.AEP_TOT_EXIST_OPER = R.CANT_ACTUAL
       WHERE A.AEP_EMPR = R.DOCU_EMPR
         AND A.AEP_ART = R.ARTICULO
         AND A.AEP_PERIODO = I_PERIODO;
    
    END LOOP;
  
    FOR R IN AEP_EMIS LOOP
      UPDATE STK_ART_EMPR_PERI A
         SET A.AEP_EXIST_INI = R.CANT_INI, A.AEP_TOT_EXIST = R.CANT_ACTUAL
       WHERE A.AEP_EMPR = R.DOCU_EMPR
         AND A.AEP_ART = R.ARTICULO
         AND A.AEP_PERIODO = I_PERIODO;
    END LOOP;
  
    COMMIT;
  
  EXCEPTION
    WHEN SALIR THEN
      NULL;
    WHEN OTHERS THEN
      ROLLBACK;
  END PP_RECALCULAR_ARDE;

  PROCEDURE PP_VERIF_AEP_TAGRO(I_EMPRESA IN NUMBER,
                               I_FECINI  IN DATE,
                               I_FECFIN  IN DATE,
                               I_PERIODO IN NUMBER,
                               I_ART     IN NUMBER DEFAULT NULL) IS
  
    CURSOR MOVI_CUR IS
      SELECT DOCU_EMPR,
             DETA_ART,
             SUM(DECODE(OPER_DESC, 'COMPRA', DETA_CANT, 0)) EC_COMPRA,
             SUM(DECODE(OPER_DESC, 'COMPRA', DETA_IMP_NETO_LOC, 0)) EL_COMPRA,
             SUM(DECODE(OPER_DESC,
                        'COMPRA',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_COMPRA,
             SUM(DECODE(OPER_DESC, 'DEV_COM', DETA_CANT, 0)) SC_DEV_COMPRA,
             SUM(DECODE(OPER_DESC, 'DEV_COM', DETA_IMP_NETO_LOC, 0)) SL_DEV_COMPRA,
             SUM(DECODE(OPER_DESC,
                        'DEV_COM',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) SM_DEV_COMPRA,
             SUM(DECODE(OPER_DESC, 'VENTA', DETA_CANT, 0)) SC_VTA,
             SUM(DECODE(OPER_DESC, 'VENTA', DETA_IMP_NETO_LOC, 0)) SL_VTA,
             SUM(DECODE(OPER_DESC,
                        'VENTA',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) SM_VTA,
             SUM(DECODE(OPER_DESC, 'DEV_VENTA', DETA_CANT, 0)) EC_DEV_VTA,
             SUM(DECODE(OPER_DESC, 'DEV_VENTA', DETA_IMP_NETO_LOC, 0)) EL_DEV_VTA,
             SUM(DECODE(OPER_DESC,
                        'DEV_VENTA',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_DEV_VTA,
             SUM(DECODE(OPER_DESC, 'ENT_PROD', DETA_CANT, 0)) EC_PROD,
             SUM(DECODE(OPER_DESC, 'ENT_PROD', DETA_IMP_NETO_LOC, 0)) EL_PROD,
             SUM(DECODE(OPER_DESC,
                        'ENT_PROD',
                        ROUND(decode(docu_mon,
                                     1,
                                     (DETA_IMP_NETO_LOC / DOCU_TASA_US),
                                     DETA_IMP_NETO_MON),
                              2),
                        0)) EM_PROD,
             
             SUM(DECODE(OPER_DESC, 'DEV_PROD', DETA_CANT, 0)) EC_DEV_PROD,
             SUM(DECODE(OPER_DESC, 'DEV_PROD', DETA_IMP_NETO_LOC, 0)) EL_DEV_PROD,
             
             SUM(DECODE(OPER_DESC,
                        'DEV_PROD',
                        ROUND(decode(docu_mon,
                                     1,
                                     (DETA_IMP_NETO_LOC / DOCU_TASA_US),
                                     DETA_IMP_NETO_MON),
                              2),
                        0)) EM_DEV_PROD,
             
             SUM(DECODE(OPER_DESC, 'SAL_PROD', DETA_CANT, 0)) SC_PROD,
             
             SUM(DECODE(OPER_DESC, 'ENT_KIT', DETA_CANT, 0)) EC_KIT,
             SUM(DECODE(OPER_DESC, 'ENT_KIT', DETA_IMP_NETO_LOC, 0)) EL_KIT,
             SUM(DECODE(OPER_DESC,
                        'ENT_KIT',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_KIT,
             SUM(DECODE(OPER_DESC, 'SAL_KIT', DETA_CANT, 0)) SC_KIT,
             SUM(DECODE(OPER_DESC, 'SAL_KIT', DETA_IMP_NETO_LOC, 0)) SL_KIT,
             SUM(DECODE(OPER_DESC,
                        'SAL_KIT',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) SM_KIT,
             SUM(DECODE(OPER_DESC, 'PERDIDA', DETA_CANT, 0)) SC_PERDIDA,
             SUM(DECODE(OPER_DESC, 'CONSUMO', DETA_CANT, 0)) SC_CONS,
             SUM(DECODE(OPER_DESC, 'DIF_MAS', DETA_CANT, 0)) EC_DIF_MAS,
             SUM(DECODE(OPER_DESC, 'DIF_MEN', DETA_CANT, 0)) SC_DIF_MEN,
             SUM(DECODE(OPER_DESC, 'TRAN_SAL', DETA_CANT, 0)) SC_TRANSF,
             SUM(DECODE(OPER_DESC, 'TRAN_ENT', DETA_CANT, 0)) EC_TRANSF,
             SUM(DECODE(OPER_DESC, 'REMISION', DETA_CANT, 0)) C_REM,
             SUM(DECODE(OPER_DESC, 'ENT_ARM_DES', DETA_CANT, 0)) EC_ARM_DES,
             SUM(DECODE(OPER_DESC, 'ENT_ARM_DES', DETA_IMP_NETO_LOC, 0)) EL_ARM_DES,
             SUM(DECODE(OPER_DESC,
                        'ENT_ARM_DES',
                        ROUND(decode(docu_mon,
                                     1,
                                     (DETA_IMP_NETO_LOC / DOCU_TASA_US),
                                     DETA_IMP_NETO_MON),
                              2),
                        0)) EM_ARM_DES,
             
             SUM(DECODE(OPER_DESC, 'SAL_ARM_DES', DETA_CANT, 0)) SC_ARM_DES,
             SUM(DECODE(OPER_DESC, 'SAL_ARM_DES', DETA_IMP_NETO_LOC, 0)) SL_ARM_DES,
             
             SUM(DECODE(OPER_DESC,
                        'SAL_ARM_DES',
                        ROUND(decode(docu_mon,
                                     1,
                                     (DETA_IMP_NETO_LOC / DOCU_TASA_US),
                                     DETA_IMP_NETO_MON),
                              2),
                        0)) SM_ARM_DES,
             
             SUM(DECODE(OPER_ENT_SAL, 'E', DETA_CANT, 0)) C_ENT,
             SUM(DECODE(OPER_ENT_SAL, 'S', DETA_CANT, 0)) C_SAL,
             
             SUM(DECODE(OPER_DESC, 'REVALUO', DETA_IMP_NETO_LOC, 0)) EL_REVALUO,
             SUM(DECODE(OPER_DESC, 'DEPRECIACION', DETA_IMP_NETO_LOC, 0)) SL_DEPREC,
             
             SUM(DECODE(OPER_DESC,
                        'REVALUO',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) EM_REVALUO,
             SUM(DECODE(OPER_DESC,
                        'DEPRECIACION',
                        ROUND(DETA_IMP_NETO_LOC / DOCU_TASA_US, 2),
                        0)) SM_DEPREC
      
        FROM STK_OPERACION     OP,
             STK_DOCUMENTO     DO,
             STK_ARTICULO      AR,
             STK_DOCUMENTO_DET DE
      
       WHERE OPER_EMPR = DOCU_EMPR
         AND OPER_CODIGO = DOCU_CODIGO_OPER
         AND DOCU_EMPR = DETA_EMPR
         AND DOCU_CLAVE = DETA_CLAVE_DOC
         AND DETA_EMPR = ART_EMPR
         AND DETA_ART = ART_CODIGO
            -------------------------------------------------------------------------------
         AND (ART_TIPO <> 4 OR AR.ART_IND_INV_VALOR_COSTO = 'S')
            -------------------------------------------------------------------------------
         AND DOCU_EMPR = I_EMPRESA
     --    AND (DETA_ART = I_ART OR I_ART IS NULL) --*
         AND DOCU_FEC_EMIS BETWEEN I_FECINI AND I_FECFIN
       GROUP BY DOCU_EMPR, DETA_ART
       ORDER BY DOCU_EMPR, DETA_ART;
  
    CURSOR AEP_CUR IS
      SELECT AEP_PERIODO,
             AEP_EMPR,
             AEP_ART,
             AEP_TOT_EXIST,
             AEP_EXIST_INI,
             AEP_COSTO_INI_LOC,
             AEP_COSTO_PROM_INI_LOC,
             AEP_COSTO_PROM_LOC,
             AEP_COSTO_INI_MON,
             AEP_COSTO_PROM_INI_MON,
             AEP_COSTO_PROM_MON,
             AEP_EC_COMPRA,
             AEP_EL_COMPRA,
             AEP_EM_COMPRA,
             AEP_SC_DEV_COMPRA,
             AEP_SL_DEV_COMPRA,
             AEP_SM_DEV_COMPRA,
             AEP_SC_VTA,
             AEP_SL_VTA,
             AEP_SM_VTA,
             AEP_EC_DEV_VTA,
             AEP_EL_DEV_VTA,
             AEP_EM_DEV_VTA,
             AEP_EC_PROD,
             AEP_EL_PROD,
             AEP_EM_PROD,
             AEP_SC_PROD,
             AEP_EC_KIT,
             AEP_EL_KIT,
             AEP_EM_KIT,
             AEP_SC_KIT,
             AEP_SC_PERDIDA,
             AEP_SC_CONS,
             AEP_C_DIF_INV,
             AEP_C_TRANSF,
             AEP_C_REM,
             AEP_EC_ARM_DES,
             AEP_EL_ARM_DES,
             AEP_EM_ARM_DES,
             AEP_SC_ARM_DES,
             AEP_SL_ARM_DES,
             AEP_SM_ARM_DES,
             AEP_SL_KIT,
             AEP_SM_KIT,
             AEP_EC_DEV_PROD,
             AEP_EL_DEV_PROD,
             AEP_EM_DEV_PROD,
             
             AP.AEP_EL_REVALUO,
             AP.AEP_EM_REVALUO,
             AP.AEP_SL_DEPREC,
             AP.AEP_SM_DEPREC
      
        FROM STK_ART_EMPR_PERI AP, STK_ARTICULO AR
      
       WHERE AEP_EMPR = I_EMPRESA
         AND AEP_EMPR = ART_EMPR
         AND AEP_ART = ART_CODIGO
            -------------------------------------------------------------------------------
         AND (ART_TIPO <> 4 OR AR.ART_IND_INV_VALOR_COSTO = 'S')
            -------------------------------------------------------------------------------
   --      AND (ART_CODIGO = I_ART OR I_ART IS NULL) --*
         AND AEP_PERIODO = I_PERIODO
       ORDER BY AEP_EMPR, AEP_ART;
    --
    RMOVI            MOVI_CUR%ROWTYPE;
    RAEP             AEP_CUR%ROWTYPE;
    V_TOT_CANT       STK_ART_EMPR_PERI.AEP_TOT_EXIST%TYPE := 0;
    V_TOT_IMP_MON    STK_ART_EMPR_PERI.AEP_EM_COMPRA%TYPE := 0;
    V_COSTO_PROM_MON STK_ART_EMPR_PERI.AEP_COSTO_PROM_MON%TYPE := 0;
    V_TOT_IMP_LOC    STK_ART_EMPR_PERI.AEP_EL_COMPRA%TYPE := 0;
    V_COSTO_PROM_LOC STK_ART_EMPR_PERI.AEP_COSTO_PROM_LOC%TYPE := 0;
    V_MESSAGE        VARCHAR2(500);
  
  BEGIN
  
    OPEN MOVI_CUR;
    FETCH MOVI_CUR
      INTO RMOVI;
  
    OPEN AEP_CUR;
    FETCH AEP_CUR
      INTO RAEP;
    LOOP
    
      EXIT WHEN MOVI_CUR%NOTFOUND OR AEP_CUR%NOTFOUND;
      LOOP
        EXIT WHEN MOVI_CUR%NOTFOUND OR(RMOVI.DOCU_EMPR > RAEP.AEP_EMPR OR
                                       (RMOVI.DOCU_EMPR = RAEP.AEP_EMPR AND
                                       RMOVI.DETA_ART >= RAEP.AEP_ART));
        -- ENTRA AQUI SI EXISTEN MOVIMIENTOS SIN MAESTRO
        V_MESSAGE := 'Error 601: Falta reg. en STK_ART_EMPR_PERI' ||
                     ' para Empr=' || TO_CHAR(RMOVI.DOCU_EMPR) || ', Art=' ||
                     TO_CHAR(RMOVI.DETA_ART) || '';
      
        PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
      
        INSERT INTO STK_ART_EMPR_PERI
          (AEP_PERIODO,
           AEP_EMPR,
           AEP_ART,
           AEP_TOT_EXIST,
           AEP_EXIST_INI,
           AEP_COSTO_INI_LOC,
           AEP_COSTO_INI_MON,
           AEP_COSTO_PROM_INI_LOC,
           AEP_COSTO_PROM_INI_MON,
           AEP_COSTO_PROM_LOC,
           AEP_COSTO_PROM_MON,
           AEP_EC_COMPRA,
           AEP_EL_COMPRA,
           AEP_EM_COMPRA,
           AEP_SC_DEV_COMPRA,
           AEP_SL_DEV_COMPRA,
           AEP_SM_DEV_COMPRA,
           AEP_SC_VTA,
           AEP_SL_VTA,
           AEP_SM_VTA,
           AEP_EC_DEV_VTA,
           AEP_EL_DEV_VTA,
           AEP_EM_DEV_VTA,
           AEP_EC_PROD,
           AEP_EL_PROD,
           AEP_EM_PROD,
           AEP_SC_PROD,
           AEP_EC_KIT,
           AEP_EL_KIT,
           AEP_EM_KIT,
           AEP_SC_KIT,
           AEP_SL_KIT,
           AEP_SM_KIT,
           AEP_SC_PERDIDA,
           AEP_SC_CONS,
           AEP_C_DIF_INV,
           AEP_C_TRANSF,
           AEP_C_REM,
           AEP_EC_ARM_DES,
           AEP_EL_ARM_DES,
           AEP_EM_ARM_DES,
           AEP_SC_ARM_DES,
           AEP_SL_ARM_DES,
           AEP_SM_ARM_DES,
           AEP_EC_DEV_PROD,
           AEP_EL_DEV_PROD,
           AEP_EM_DEV_PROD,
           -------------------------------------
           AEP_EL_REVALUO,
           AEP_EM_REVALUO,
           AEP_SL_DEPREC,
           AEP_SM_DEPREC
           -------------------------------------
           )
        VALUES
          (I_PERIODO,
           RMOVI.DOCU_EMPR,
           RMOVI.DETA_ART,
           RMOVI.C_ENT - RMOVI.C_SAL,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           RMOVI.EC_COMPRA,
           RMOVI.EL_COMPRA,
           RMOVI.EM_COMPRA,
           RMOVI.SC_DEV_COMPRA,
           RMOVI.SL_DEV_COMPRA,
           RMOVI.SM_DEV_COMPRA,
           RMOVI.SC_VTA,
           RMOVI.SL_VTA,
           RMOVI.SM_VTA,
           RMOVI.EC_DEV_VTA,
           RMOVI.EL_DEV_VTA,
           RMOVI.EM_DEV_VTA,
           RMOVI.EC_PROD,
           RMOVI.EL_PROD,
           RMOVI.EM_PROD,
           RMOVI.SC_PROD,
           RMOVI.EC_KIT,
           RMOVI.EL_KIT,
           RMOVI.EM_KIT,
           RMOVI.SC_KIT,
           RMOVI.SL_KIT,
           RMOVI.SM_KIT,
           RMOVI.SC_PERDIDA,
           RMOVI.SC_CONS,
           RMOVI.EC_DIF_MAS - RMOVI.SC_DIF_MEN,
           RMOVI.EC_TRANSF - RMOVI.SC_TRANSF,
           RMOVI.C_REM,
           RMOVI.EC_ARM_DES,
           RMOVI.EL_ARM_DES,
           RMOVI.EM_ARM_DES,
           RMOVI.SC_ARM_DES,
           
           RMOVI.SL_ARM_DES,
           RMOVI.SM_ARM_DES,
           RMOVI.EC_DEV_PROD,
           RMOVI.EL_DEV_PROD,
           RMOVI.EM_DEV_PROD,
           -------------------------------------
           RMOVI.EL_REVALUO,
           RMOVI.EM_REVALUO,
           RMOVI.SL_DEPREC,
           RMOVI.SM_DEPREC
           -------------------------------------
           );
        FETCH MOVI_CUR
          INTO RMOVI;
      END LOOP;
    
      LOOP
        EXIT WHEN AEP_CUR%NOTFOUND OR(RAEP.AEP_EMPR > RMOVI.DOCU_EMPR OR
                                      (RAEP.AEP_EMPR = RMOVI.DOCU_EMPR AND
                                      RAEP.AEP_ART >= RMOVI.DETA_ART));
        -- ENTRA AQUI SI EXISTEN REG.MAESTROS SIN MOVIMIENTOS
      
        IF ((ROUND(RAEP.AEP_COSTO_PROM_LOC, 0) <>
           ROUND(RAEP.AEP_COSTO_PROM_INI_LOC, 0) OR
           RAEP.AEP_COSTO_PROM_MON <> RAEP.AEP_COSTO_PROM_INI_MON) AND
           RAEP.AEP_TOT_EXIST <> 0) OR
           RAEP.AEP_TOT_EXIST <> RAEP.AEP_EXIST_INI OR
           RAEP.AEP_EC_COMPRA <> 0 OR RAEP.AEP_EL_COMPRA <> 0 OR
           RAEP.AEP_EM_COMPRA <> 0 OR RAEP.AEP_SC_DEV_COMPRA <> 0 OR
           RAEP.AEP_SL_DEV_COMPRA <> 0 OR RAEP.AEP_SM_DEV_COMPRA <> 0 OR
           RAEP.AEP_SC_VTA <> 0 OR RAEP.AEP_SL_VTA <> 0 OR
           RAEP.AEP_SM_VTA <> 0 OR RAEP.AEP_EC_DEV_VTA <> 0 OR
           RAEP.AEP_EL_DEV_VTA <> 0 OR RAEP.AEP_EM_DEV_VTA <> 0 OR
           RAEP.AEP_EC_PROD <> 0 OR RAEP.AEP_EL_PROD <> 0 OR
           RAEP.AEP_EM_PROD <> 0 OR RAEP.AEP_EC_DEV_PROD <> 0 OR
           RAEP.AEP_EL_DEV_PROD <> 0 OR RAEP.AEP_EM_DEV_PROD <> 0 OR
           RAEP.AEP_SC_PROD <> 0 OR RAEP.AEP_EC_KIT <> 0 OR
           RAEP.AEP_EL_KIT <> 0 OR RAEP.AEP_EM_KIT <> 0 OR
           RAEP.AEP_SC_KIT <> 0 OR RAEP.AEP_SL_KIT <> 0 OR
           RAEP.AEP_SM_KIT <> 0 OR RAEP.AEP_SC_PERDIDA <> 0 OR
           RAEP.AEP_SC_CONS <> 0 OR RAEP.AEP_C_DIF_INV <> 0 OR
           RAEP.AEP_C_TRANSF <> 0 OR RAEP.AEP_C_REM <> 0 OR
           RAEP.AEP_EC_ARM_DES <> 0 OR RAEP.AEP_EL_ARM_DES <> 0 OR
           RAEP.AEP_EM_ARM_DES <> 0 OR RAEP.AEP_SC_ARM_DES <> 0 OR
           RAEP.AEP_SL_ARM_DES <> 0 OR RAEP.AEP_SM_ARM_DES <> 0 OR
           RAEP.AEP_EL_REVALUO <> 0 OR RAEP.AEP_EM_REVALUO <> 0 OR
           RAEP.AEP_SL_DEPREC <> 0 OR RAEP.AEP_SM_DEPREC <> 0
        
         THEN
          V_MESSAGE := 'Error 602: Cantidades incorrectas' ||
                       ' en STK_ART_EMPR_PERI para ' || 'Empr=' ||
                       TO_CHAR(RAEP.AEP_EMPR) || ', Art=' ||
                       TO_CHAR(RAEP.AEP_ART);
          UPDATE STK_ART_EMPR_PERI
             SET AEP_TOT_EXIST      = AEP_EXIST_INI,
                 AEP_COSTO_PROM_LOC = AEP_COSTO_PROM_INI_LOC,
                 AEP_COSTO_PROM_MON = AEP_COSTO_PROM_INI_MON,
                 AEP_EC_COMPRA      = 0,
                 AEP_EL_COMPRA      = 0,
                 AEP_EM_COMPRA      = 0,
                 AEP_SC_DEV_COMPRA  = 0,
                 AEP_SL_DEV_COMPRA  = 0,
                 AEP_SM_DEV_COMPRA  = 0,
                 AEP_SC_VTA         = 0,
                 AEP_SL_VTA         = 0,
                 AEP_SM_VTA         = 0,
                 AEP_EC_DEV_VTA     = 0,
                 AEP_EL_DEV_VTA     = 0,
                 AEP_EM_DEV_VTA     = 0,
                 AEP_EC_PROD        = 0,
                 AEP_EL_PROD        = 0,
                 AEP_EM_PROD        = 0,
                 AEP_EC_DEV_PROD    = 0,
                 AEP_EL_DEV_PROD    = 0,
                 AEP_EM_DEV_PROD    = 0,
                 AEP_SC_PROD        = 0,
                 AEP_EC_KIT         = 0,
                 AEP_EL_KIT         = 0,
                 AEP_EM_KIT         = 0,
                 AEP_SC_KIT         = 0,
                 AEP_SL_KIT         = 0,
                 AEP_SM_KIT         = 0,
                 AEP_SC_PERDIDA     = 0,
                 AEP_SC_CONS        = 0,
                 AEP_C_DIF_INV      = 0,
                 AEP_C_TRANSF       = 0,
                 AEP_EC_ARM_DES     = 0,
                 AEP_EL_ARM_DES     = 0,
                 AEP_EM_ARM_DES     = 0,
                 AEP_SC_ARM_DES     = 0,
                 AEP_SL_ARM_DES     = 0,
                 AEP_SM_ARM_DES     = 0,
                 AEP_C_REM          = 0,
                 AEP_EL_REVALUO     = 0,
                 AEP_EM_REVALUO     = 0,
                 AEP_SL_DEPREC      = 0,
                 AEP_SM_DEPREC      = 0
           WHERE AEP_PERIODO = I_PERIODO
             AND AEP_EMPR = RAEP.AEP_EMPR
             AND AEP_ART = RAEP.AEP_ART;
        END IF;
        FETCH AEP_CUR
          INTO RAEP;
      END LOOP;
    
      EXIT WHEN MOVI_CUR%NOTFOUND OR AEP_CUR%NOTFOUND;
      IF RMOVI.DOCU_EMPR = RAEP.AEP_EMPR AND RMOVI.DETA_ART = RAEP.AEP_ART THEN
        -- ENTRA AQUI SI EXISTEN MOVIMIENTOS Y SU REG.MAESTRO
        V_TOT_CANT := 0;
        --DD  IF RAEP.AEP_EXIST_INI > 0 THEN
        V_TOT_CANT := RAEP.AEP_EXIST_INI;
        --DD  END IF;
      
        V_TOT_CANT := V_TOT_CANT + RAEP.AEP_EC_COMPRA + RAEP.AEP_EC_PROD +
                      RAEP.AEP_EC_DEV_VTA -
                      RAEP.AEP_EC_DEV_PROD + RAEP.AEP_EC_KIT +
                      RAEP.AEP_EC_ARM_DES - RAEP.AEP_SC_DEV_COMPRA;
        /* CALCULO DEL COSTO PROMEDIO EXPRESADO EN MONEDA LOCAL */
        V_TOT_IMP_LOC := 0;
        --DD IF RAEP.AEP_COSTO_INI_LOC > 0 THEN
        V_TOT_IMP_LOC := RAEP.AEP_COSTO_INI_LOC;
        --DD   END IF;
        V_TOT_IMP_LOC := V_TOT_IMP_LOC + RAEP.AEP_EL_COMPRA +
                         RAEP.AEP_EL_PROD + RAEP.AEP_EL_DEV_PROD +
                         RAEP.AEP_EL_DEV_VTA +
                         RAEP.AEP_EL_KIT + RAEP.AEP_EL_ARM_DES -
                         RAEP.AEP_SL_DEV_COMPRA + RAEP.AEP_EL_REVALUO -
                         RAEP.AEP_SL_DEPREC;
        --+ RAEP.AEP_EL_DEV_VTA;
        IF V_TOT_CANT <= 0 OR V_TOT_IMP_LOC <= 0 THEN
          -- PARA QUE CALCULE EL COSTO DE LOS VALORES DE COMPRA
          -- QUE ES IGUAL AL ULTIMO COSTO
          V_TOT_CANT    := RAEP.AEP_EC_COMPRA; -- + RAEP.AEP_EC_DEV_VTA;
          V_TOT_IMP_LOC := RAEP.AEP_EL_COMPRA; -- + RAEP.AEP_EL_DEV_VTA;
          
        END IF;
      
        IF V_TOT_CANT <= 0 OR V_TOT_IMP_LOC <= 0 THEN
          -- PARA QUE NO SE PIERDA EL COSTO
          V_COSTO_PROM_LOC := RAEP.AEP_COSTO_PROM_INI_LOC;
        ELSE
          V_COSTO_PROM_LOC := V_TOT_IMP_LOC / V_TOT_CANT;
        END IF;
      
        /* CALCULO DEL COSTO PROMEDIO EXPRESADO EN MONEDA DE REFERENCIA (U$) */
        V_TOT_IMP_MON := 0;
        --DD   IF RAEP.AEP_COSTO_INI_MON > 0 THEN
        V_TOT_IMP_MON := RAEP.AEP_COSTO_INI_MON;
        --DD   END IF;
      
        V_TOT_IMP_MON := V_TOT_IMP_MON + RAEP.AEP_EM_COMPRA +
                         RAEP.AEP_EM_PROD + RAEP.AEP_EM_DEV_PROD +
                         RAEP.AEP_EM_KIT + RAEP.AEP_EM_ARM_DES -
                         RAEP.AEP_SM_DEV_COMPRA + RAEP.AEP_EM_REVALUO -
                         RAEP.AEP_SM_DEPREC;
      
        -- + RAEP.AEP_EM_DEV_VTA;
        IF V_TOT_CANT <= 0 OR V_TOT_IMP_MON <= 0 THEN
          -- PARA QUE CALCULE EL COSTO DE LOS VALORES DE COMPRA
          -- QUE ES IGUAL AL ULTIMO COSTO
          V_TOT_CANT    := RAEP.AEP_EC_COMPRA; -- + RAEP.AEP_EC_DEV_VTA;
          V_TOT_IMP_MON := RAEP.AEP_EM_COMPRA; -- + RAEP.AEP_EM_DEV_VTA;
        END IF;
      
        IF V_TOT_CANT <= 0 OR V_TOT_IMP_MON <= 0 THEN
          -- PARA QUE NO SE PIERDA EL COSTO
          V_COSTO_PROM_MON := RAEP.AEP_COSTO_PROM_INI_MON;
        ELSE
          V_COSTO_PROM_MON := V_TOT_IMP_MON / V_TOT_CANT;
        END IF;
      
        IF RAEP.AEP_TOT_EXIST <>
           (RAEP.AEP_EXIST_INI + RMOVI.C_ENT - RMOVI.C_SAL) OR
           (RAEP.AEP_COSTO_PROM_LOC <> V_COSTO_PROM_LOC AND
           (RAEP.AEP_TOT_EXIST > 0 OR V_TOT_CANT > 0)) OR
           (RAEP.AEP_COSTO_PROM_MON <> V_COSTO_PROM_MON AND
           (RAEP.AEP_TOT_EXIST > 0 OR V_TOT_CANT > 0))
          --FIN EL 21-03-2002
           OR RAEP.AEP_EC_COMPRA <> RMOVI.EC_COMPRA OR
           RAEP.AEP_EL_COMPRA <> RMOVI.EL_COMPRA OR
           RAEP.AEP_EM_COMPRA <> RMOVI.EM_COMPRA OR
           RAEP.AEP_SC_DEV_COMPRA <> RMOVI.SC_DEV_COMPRA OR
           RAEP.AEP_SL_DEV_COMPRA <> RMOVI.SL_DEV_COMPRA OR
           RAEP.AEP_SM_DEV_COMPRA <> RMOVI.SM_DEV_COMPRA OR
           RAEP.AEP_SC_VTA <> RMOVI.SC_VTA OR
           RAEP.AEP_SL_VTA <> RMOVI.SL_VTA OR
           RAEP.AEP_SM_VTA <> RMOVI.SM_VTA OR
           RAEP.AEP_EC_DEV_VTA <> RMOVI.EC_DEV_VTA OR
           RAEP.AEP_EL_DEV_VTA <> RMOVI.EL_DEV_VTA OR
           RAEP.AEP_EM_DEV_VTA <> RMOVI.EM_DEV_VTA OR
           RAEP.AEP_EC_PROD <> RMOVI.EC_PROD OR
           RAEP.AEP_EL_PROD <> RMOVI.EL_PROD OR
           RAEP.AEP_EM_PROD <> RMOVI.EM_PROD OR
           RAEP.AEP_EC_DEV_PROD <> RMOVI.EC_DEV_PROD OR
           RAEP.AEP_EL_DEV_PROD <> RMOVI.EL_DEV_PROD OR
           RAEP.AEP_EM_DEV_PROD <> RMOVI.EM_DEV_PROD OR
           RAEP.AEP_SC_PROD <> RMOVI.SC_PROD OR
           RAEP.AEP_EC_KIT <> RMOVI.EC_KIT OR
           RAEP.AEP_EL_KIT <> RMOVI.EL_KIT OR
           RAEP.AEP_EM_KIT <> RMOVI.EM_KIT OR
           RAEP.AEP_SC_KIT <> RMOVI.SC_KIT OR
           RAEP.AEP_SL_KIT <> RMOVI.SL_KIT OR
           RAEP.AEP_SM_KIT <> RMOVI.SM_KIT OR
           RAEP.AEP_SC_PERDIDA <> RMOVI.SC_PERDIDA OR
           RAEP.AEP_SC_CONS <> RMOVI.SC_CONS OR
           RAEP.AEP_C_DIF_INV <> (RMOVI.EC_DIF_MAS - RMOVI.SC_DIF_MEN) OR
           RAEP.AEP_C_TRANSF <> (RMOVI.EC_TRANSF - RMOVI.SC_TRANSF) OR
           RAEP.AEP_C_REM <> RMOVI.C_REM OR
           RAEP.AEP_EC_ARM_DES <> RMOVI.EC_ARM_DES OR
           RAEP.AEP_EL_ARM_DES <> RMOVI.EL_ARM_DES OR
           RAEP.AEP_EM_ARM_DES <> RMOVI.EM_ARM_DES OR
           RAEP.AEP_SC_ARM_DES <> RMOVI.SC_ARM_DES OR
           RAEP.AEP_SL_ARM_DES <> RMOVI.SL_ARM_DES OR
           RAEP.AEP_SM_ARM_DES <> RMOVI.SM_ARM_DES OR
           RAEP.AEP_EL_REVALUO <> RMOVI.EL_REVALUO OR
           RAEP.AEP_EM_REVALUO <> RMOVI.EM_REVALUO OR
           RAEP.AEP_SL_DEPREC <> RMOVI.SL_DEPREC OR
           RAEP.AEP_SM_DEPREC <> RMOVI.SM_DEPREC
        
         THEN
        
          -- ACTUALIZAR SI HAY DIFERENCIAS ENTRE MOV.Y MAESTRO
          V_MESSAGE := 'Error 603: En STK_ART_EMPR_PERI' || ' para Empr=' ||
                       TO_CHAR(RAEP.AEP_EMPR) || ', Art=' ||
                       TO_CHAR(RAEP.AEP_ART);
          PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
        
          UPDATE STK_ART_EMPR_PERI AP
             SET AEP_TOT_EXIST     =
                 (RAEP.AEP_EXIST_INI + RMOVI.C_ENT - RMOVI.C_SAL),
                 AEP_COSTO_PROM_LOC = V_COSTO_PROM_LOC,
                 AEP_COSTO_PROM_MON = V_COSTO_PROM_MON,
                 AEP_EC_COMPRA      = RMOVI.EC_COMPRA,
                 AEP_EL_COMPRA      = RMOVI.EL_COMPRA,
                 AEP_EM_COMPRA      = RMOVI.EM_COMPRA,
                 AEP_SC_DEV_COMPRA  = RMOVI.SC_DEV_COMPRA,
                 AEP_SL_DEV_COMPRA  = RMOVI.SL_DEV_COMPRA,
                 AEP_SM_DEV_COMPRA  = RMOVI.SM_DEV_COMPRA,
                 AEP_SC_VTA         = RMOVI.SC_VTA,
                 AEP_SL_VTA         = RMOVI.SL_VTA,
                 AEP_SM_VTA         = RMOVI.SM_VTA,
                 AEP_EC_DEV_VTA     = RMOVI.EC_DEV_VTA,
                 AEP_EL_DEV_VTA     = RMOVI.EL_DEV_VTA,
                 AEP_EM_DEV_VTA     = RMOVI.EM_DEV_VTA,
                 AEP_EC_PROD        = RMOVI.EC_PROD,
                 AEP_EL_PROD        = RMOVI.EL_PROD,
                 AEP_EM_PROD        = RMOVI.EM_PROD,
                 AEP_EC_DEV_PROD    = RMOVI.EC_DEV_PROD,
                 AEP_EL_DEV_PROD    = RMOVI.EL_DEV_PROD,
                 AEP_EM_DEV_PROD    = RMOVI.EM_DEV_PROD,
                 AEP_SC_PROD        = RMOVI.SC_PROD,
                 AEP_EC_KIT         = RMOVI.EC_KIT,
                 AEP_EL_KIT         = RMOVI.EL_KIT,
                 AEP_EM_KIT         = RMOVI.EM_KIT,
                 AEP_SC_KIT         = RMOVI.SC_KIT,
                 AEP_SL_KIT         = RMOVI.SL_KIT,
                 AEP_SM_KIT         = RMOVI.SM_KIT,
                 AEP_SC_PERDIDA     = RMOVI.SC_PERDIDA,
                 AEP_SC_CONS        = RMOVI.SC_CONS,
                 AEP_C_DIF_INV     =
                 (RMOVI.EC_DIF_MAS - RMOVI.SC_DIF_MEN),
                 AEP_C_TRANSF      =
                 (RMOVI.EC_TRANSF - RMOVI.SC_TRANSF),
                 AEP_C_REM          = RMOVI.C_REM,
                 AEP_EC_ARM_DES     = RMOVI.EC_ARM_DES,
                 AEP_EL_ARM_DES     = RMOVI.EL_ARM_DES,
                 AEP_EM_ARM_DES     = RMOVI.EM_ARM_DES,
                 
                 AEP_SC_ARM_DES    = RMOVI.SC_ARM_DES,
                 AEP_SL_ARM_DES    = RMOVI.SL_ARM_DES,
                 AEP_SM_ARM_DES    = RMOVI.SM_ARM_DES,
                 AP.AEP_EL_REVALUO = RMOVI.EL_REVALUO,
                 AP.AEP_EM_REVALUO = RMOVI.EM_REVALUO,
                 AP.AEP_SL_DEPREC  = RMOVI.SL_DEPREC,
                 AP.AEP_SM_DEPREC  = RMOVI.SM_DEPREC
           WHERE AEP_PERIODO = I_PERIODO
             AND AEP_EMPR = RAEP.AEP_EMPR
             AND AEP_ART = RAEP.AEP_ART;
        END IF;
        FETCH MOVI_CUR
          INTO RMOVI;
        FETCH AEP_CUR
          INTO RAEP;
      END IF;
    
    END LOOP;
  
    IF MOVI_CUR%FOUND AND AEP_CUR%NOTFOUND THEN
      LOOP
        EXIT WHEN MOVI_CUR%NOTFOUND;
        -- ENTRA AQUI SI EXISTEN MOVIMIENTOS SIN MAESTRO
        V_MESSAGE := 'Error 604: Falta reg. en STK_ART_EMPR_PERI para ' ||
                     'Empr=' || TO_CHAR(RMOVI.DOCU_EMPR) || ', Art=' ||
                     TO_CHAR(RMOVI.DETA_ART);
        PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
        INSERT INTO STK_ART_EMPR_PERI
          (AEP_PERIODO,
           AEP_EMPR,
           AEP_ART,
           AEP_TOT_EXIST,
           AEP_EXIST_INI,
           AEP_COSTO_INI_LOC,
           AEP_COSTO_INI_MON,
           AEP_COSTO_PROM_INI_LOC,
           AEP_COSTO_PROM_INI_MON,
           AEP_COSTO_PROM_LOC,
           AEP_COSTO_PROM_MON,
           AEP_EC_COMPRA,
           AEP_EL_COMPRA,
           AEP_EM_COMPRA,
           AEP_SC_DEV_COMPRA,
           AEP_SL_DEV_COMPRA,
           AEP_SM_DEV_COMPRA,
           AEP_SC_VTA,
           AEP_SL_VTA,
           AEP_SM_VTA,
           AEP_EC_DEV_VTA,
           AEP_EL_DEV_VTA,
           AEP_EM_DEV_VTA,
           AEP_EC_PROD,
           AEP_EL_PROD,
           AEP_EM_PROD,
           AEP_SC_PROD,
           AEP_EC_KIT,
           AEP_EL_KIT,
           AEP_EM_KIT,
           AEP_SC_KIT,
           AEP_SL_KIT,
           AEP_SM_KIT,
           AEP_SC_PERDIDA,
           AEP_SC_CONS,
           AEP_C_DIF_INV,
           AEP_C_TRANSF,
           AEP_C_REM,
           
           AEP_EC_ARM_DES,
           AEP_EL_ARM_DES,
           AEP_EM_ARM_DES,
           AEP_SC_ARM_DES,
           AEP_SL_ARM_DES,
           AEP_SM_ARM_DES,
           
           AEP_EC_DEV_PROD,
           AEP_EL_DEV_PROD,
           AEP_EM_DEV_PROD,
           
           AEP_EL_REVALUO,
           AEP_EM_REVALUO,
           AEP_SL_DEPREC,
           AEP_SM_DEPREC
           
           )
        VALUES
          (I_PERIODO,
           RMOVI.DOCU_EMPR,
           RMOVI.DETA_ART,
           RMOVI.C_ENT - RMOVI.C_SAL,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           RMOVI.EC_COMPRA,
           RMOVI.EL_COMPRA,
           RMOVI.EM_COMPRA,
           RMOVI.SC_DEV_COMPRA,
           RMOVI.SL_DEV_COMPRA,
           RMOVI.SM_DEV_COMPRA,
           RMOVI.SC_VTA,
           RMOVI.SL_VTA,
           RMOVI.SM_VTA,
           RMOVI.EC_DEV_VTA,
           RMOVI.EL_DEV_VTA,
           RMOVI.EM_DEV_VTA,
           RMOVI.EC_PROD,
           RMOVI.EL_PROD,
           RMOVI.EM_PROD,
           RMOVI.SC_PROD,
           RMOVI.EC_KIT,
           RMOVI.EL_KIT,
           RMOVI.EM_KIT,
           RMOVI.SC_KIT,
           RMOVI.SL_KIT,
           RMOVI.SM_KIT,
           RMOVI.SC_PERDIDA,
           RMOVI.SC_CONS,
           RMOVI.EC_DIF_MAS - RMOVI.SC_DIF_MEN,
           RMOVI.EC_TRANSF - RMOVI.SC_TRANSF,
           RMOVI.C_REM,
           RMOVI.EC_ARM_DES,
           RMOVI.EL_ARM_DES,
           RMOVI.EM_ARM_DES,
           
           RMOVI.SC_ARM_DES,
           RMOVI.SL_ARM_DES,
           RMOVI.SM_ARM_DES,
           RMOVI.EC_DEV_PROD,
           RMOVI.EL_DEV_PROD,
           RMOVI.EM_DEV_PROD,
           
           RMOVI.EL_REVALUO,
           RMOVI.EM_REVALUO,
           RMOVI.SL_DEPREC,
           RMOVI.SM_DEPREC
           
           );
      
        FETCH MOVI_CUR
          INTO RMOVI;
      END LOOP;
    
    END IF;
  
    IF MOVI_CUR%NOTFOUND AND AEP_CUR%FOUND THEN
      LOOP
        EXIT WHEN AEP_CUR%NOTFOUND;
        -- ENTRA AQUI SI EXISTEN MAESTROS SIN MOVIMIENTOS
        IF RAEP.AEP_COSTO_PROM_LOC <> RAEP.AEP_COSTO_PROM_INI_LOC OR
           RAEP.AEP_COSTO_PROM_MON <> RAEP.AEP_COSTO_PROM_INI_MON OR
           RAEP.AEP_TOT_EXIST <> RAEP.AEP_EXIST_INI OR
           RAEP.AEP_EC_COMPRA <> 0 OR RAEP.AEP_EL_COMPRA <> 0 OR
           RAEP.AEP_EM_COMPRA <> 0 OR RAEP.AEP_SC_DEV_COMPRA <> 0 OR
           RAEP.AEP_SL_DEV_COMPRA <> 0 OR RAEP.AEP_SM_DEV_COMPRA <> 0 OR
           RAEP.AEP_SC_VTA <> 0 OR RAEP.AEP_SL_VTA <> 0 OR
           RAEP.AEP_SM_VTA <> 0 OR RAEP.AEP_EC_DEV_VTA <> 0 OR
           RAEP.AEP_EL_DEV_VTA <> 0 OR RAEP.AEP_EM_DEV_VTA <> 0 OR
           RAEP.AEP_EC_PROD <> 0 OR RAEP.AEP_EL_PROD <> 0 OR
           RAEP.AEP_EM_PROD <> 0 OR RAEP.AEP_EC_DEV_PROD <> 0 OR
           RAEP.AEP_EL_DEV_PROD <> 0 OR RAEP.AEP_EM_DEV_PROD <> 0 OR
           RAEP.AEP_SC_PROD <> 0 OR RAEP.AEP_EC_KIT <> 0 OR
           RAEP.AEP_EL_KIT <> 0 OR RAEP.AEP_EM_KIT <> 0 OR
           RAEP.AEP_SC_KIT <> 0 OR RAEP.AEP_SL_KIT <> 0 OR
           RAEP.AEP_SM_KIT <> 0 OR RAEP.AEP_SC_PERDIDA <> 0 OR
           RAEP.AEP_SC_CONS <> 0 OR RAEP.AEP_C_DIF_INV <> 0 OR
           RAEP.AEP_C_TRANSF <> 0 OR RAEP.AEP_C_REM <> 0 OR
           RAEP.AEP_EC_ARM_DES <> 0 OR RAEP.AEP_EL_ARM_DES <> 0 OR
           RAEP.AEP_EM_ARM_DES <> 0 OR RAEP.AEP_SC_ARM_DES <> 0 OR
           RAEP.AEP_SL_ARM_DES <> 0 OR RAEP.AEP_SM_ARM_DES <> 0 OR
           RAEP.AEP_EL_REVALUO <> 0 OR RMOVI.EL_REVALUO <> 0 OR
           RAEP.AEP_EM_REVALUO <> 0 OR RMOVI.EM_REVALUO <> 0 OR
           RAEP.AEP_SL_DEPREC <> 0 OR RMOVI.SL_DEPREC <> 0 OR
           RAEP.AEP_SM_DEPREC <> 0 OR RMOVI.SM_DEPREC <> 0
        
         THEN
        
          V_MESSAGE := 'Error 605: Cantidades incorrectas' ||
                       ' en STK_ART_EMPR_PERI para ' || 'Empr=' ||
                       TO_CHAR(RAEP.AEP_EMPR) || ', Art=' ||
                       TO_CHAR(RAEP.AEP_ART);
        
          PP_MESSAGE(I_MESSAGE => V_MESSAGE, I_EMPRESA => I_EMPRESA);
          UPDATE STK_ART_EMPR_PERI AP
             SET AEP_TOT_EXIST      = AEP_EXIST_INI,
                 AEP_COSTO_PROM_LOC = AEP_COSTO_PROM_INI_LOC,
                 AEP_COSTO_PROM_MON = AEP_COSTO_PROM_INI_MON,
                 AEP_EC_COMPRA      = 0,
                 AEP_EL_COMPRA      = 0,
                 AEP_EM_COMPRA      = 0,
                 AEP_SC_DEV_COMPRA  = 0,
                 AEP_SL_DEV_COMPRA  = 0,
                 AEP_SM_DEV_COMPRA  = 0,
                 
                 AEP_SC_VTA      = 0,
                 AEP_SL_VTA      = 0,
                 AEP_SM_VTA      = 0,
                 AEP_EC_DEV_VTA  = 0,
                 AEP_EL_DEV_VTA  = 0,
                 AEP_EM_DEV_VTA  = 0,
                 AEP_EC_PROD     = 0,
                 AEP_EL_PROD     = 0,
                 AEP_EM_PROD     = 0,
                 AEP_EC_DEV_PROD = 0,
                 AEP_EL_DEV_PROD = 0,
                 AEP_EM_DEV_PROD = 0,
                 AEP_SC_PROD     = 0,
                 AEP_EC_KIT      = 0,
                 AEP_EL_KIT      = 0,
                 AEP_EM_KIT      = 0,
                 AEP_SC_KIT      = 0,
                 
                 AEP_SL_KIT     = 0,
                 AEP_SM_KIT     = 0,
                 AEP_SC_PERDIDA = 0,
                 AEP_SC_CONS    = 0,
                 AEP_C_DIF_INV  = 0,
                 AEP_C_TRANSF   = 0,
                 AEP_C_REM      = 0,
                 
                 AEP_EC_ARM_DES = 0,
                 AEP_EL_ARM_DES = 0,
                 AEP_EM_ARM_DES = 0,
                 AEP_SC_ARM_DES = 0,
                 AEP_SL_ARM_DES = 0,
                 AEP_SM_ARM_DES = 0,
                 
                 AP.AEP_EL_REVALUO = 0,
                 AP.AEP_EM_REVALUO = 0,
                 AP.AEP_SL_DEPREC  = 0,
                 AP.AEP_SM_DEPREC  = 0
          
           WHERE AEP_PERIODO = I_PERIODO
             AND AEP_EMPR = RAEP.AEP_EMPR
             AND AEP_ART = RAEP.AEP_ART;
        END IF;
        FETCH AEP_CUR
          INTO RAEP;
      END LOOP;
    END IF;
    COMMIT;
  
  END PP_VERIF_AEP_TAGRO;

  PROCEDURE PP_VALIDAR_PERIODO(I_PERIODO IN NUMBER, I_EMPRESA IN NUMBER) AS
    V_PERIODO_ACTUAL NUMBER;
    V_EXISTE         DATE;
  BEGIN
    IF I_PERIODO IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010, 'No puede ser nulo!');
    END IF;
  
    SELECT T.CONF_PERIODO_ACT
      INTO V_PERIODO_ACTUAL
      FROM STK_CONFIGURACION T
     WHERE T.CONF_EMPR = I_EMPRESA;
  
    IF I_PERIODO > V_PERIODO_ACTUAL THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'No puede ser posterior al periodo actual: ' ||
                              V_PERIODO_ACTUAL);
    END IF;
  
    SELECT PERI_FEC_INI
      INTO V_EXISTE
      FROM STK_PERIODO
     WHERE PERI_CODIGO = I_PERIODO
       AND PERI_EMPR = I_EMPRESA;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20010, 'Periodo inexistente!');
  END PP_VALIDAR_PERIODO;

  PROCEDURE PP_VERIFICAR_SISTEMA(I_EMPRESA     IN NUMBER,
                                 I_PERI_CODIGO IN NUMBER,
                                 I_ESTADISTICA IN VARCHAR2,
                                 I_ART         IN NUMBER DEFAULT NULL) IS
    V_PERI_SGTE_FEC_INI DATE;
    V_PERI_SGTE_FEC_FIN DATE;
    V_PERI_SGTE         NUMBER := NULL;
    V_PARAM_PERI_ACT    NUMBER;
    V_PERI_FEC_INI      DATE;
    V_PERI_FEC_FIN      DATE;
  BEGIN
  
    SELECT ACT.PERI_CODIGO
      INTO V_PARAM_PERI_ACT
      FROM STK_PERIODO ACT, STK_CONFIGURACION C
     WHERE ACT.PERI_CODIGO = C.CONF_PERIODO_ACT
       AND ACT.PERI_EMPR = C.CONF_EMPR
       AND C.CONF_EMPR = I_EMPRESA;
  
    BEGIN
      SELECT MIN(PERI_CODIGO)
        INTO V_PERI_SGTE
        FROM STK_PERIODO
       WHERE PERI_CODIGO > I_PERI_CODIGO
         AND PERI_EMPR = I_EMPRESA;
    
    EXCEPTION
    
      WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20020, 'Periodo siguiente no encontrado!');
    END;
  
    SELECT PERI_FEC_INI, PERI_FEC_FIN
      INTO V_PERI_SGTE_FEC_INI, V_PERI_SGTE_FEC_FIN
      FROM STK_PERIODO
     WHERE PERI_CODIGO = V_PERI_SGTE
       AND PERI_EMPR = I_EMPRESA;
  
    SELECT PERI_FEC_INI, PERI_FEC_FIN
      INTO V_PERI_FEC_INI, V_PERI_FEC_FIN
      FROM STK_PERIODO
     WHERE PERI_CODIGO = I_PERI_CODIGO
       AND PERI_EMPR = I_EMPRESA;
    ------------------------------------------------------------------------------------------
    DBMS_OUTPUT.PUT_LINE('1 = ' ||
                         TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
                         
    IF I_EMPRESA in( 1 , 20) then -->ALIMENTEC TOO
      STK_ACT_STK_ART_DEP(I_EMPRESA); --CARGA LOS DEPOSITOS CON LOS ART?CULOS QUE FALTAN.
    
    END IF;
    DBMS_OUTPUT.PUT_LINE('2 = ' ||
                         TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
    PP_RECALCULAR_ARDE(I_EMPRESA, I_PERI_CODIGO, I_ART);
  
    DBMS_OUTPUT.PUT_LINE('3 = ' ||
                         TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
  
    PP_VERIF_ART_DEP_EX_INI(I_EMPRESA => I_EMPRESA, I_ART => I_ART);
    PP_VERIF_ART_DEP_EX_INI_OPER(I_EMPRESA => I_EMPRESA, I_ART => I_ART); --PROCEDIMIENTO POR FECHA DE OPERACION
  
    DBMS_OUTPUT.PUT_LINE('4 = ' ||
                         TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
  
    IF I_PERI_CODIGO = V_PARAM_PERI_ACT THEN
      PP_VERIF_ART_DEP_EX_ACT(I_EMPRESA => I_EMPRESA, I_ART => I_ART);
      PP_VERIF_ART_DEP_EX_ACT_OPER(I_EMPRESA => I_EMPRESA, I_ART => I_ART); --PROCEDIMIENTO POR FECHA DE OPERACION
    END IF;
  
    DBMS_OUTPUT.PUT_LINE('5 = ' ||
                         TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
  
    PP_COMPARAR_CON_PERI_ANT(I_EMPRESA => I_EMPRESA,
                             I_PERIODO => I_PERI_CODIGO,
                             I_ART     => I_ART);
  
    DBMS_OUTPUT.PUT_LINE('6 = ' ||
                         TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
  
    PP_COMPARAR_CON_PERI_ANT_OPER(I_EMPRESA => I_EMPRESA,
                                  I_PERIODO => I_PERI_CODIGO,
                                  I_ART     => I_ART);
  
    DBMS_OUTPUT.PUT_LINE('7 = ' ||
                         TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
  
    -- AN: 10-06-2021.. Transagro y restro de empresas misma verificacion, solo Hilagro Diferente..
    --IF I_EMPRESA = 2 THEN
    IF I_EMPRESA in ( 1 , 20) THEN
    
      PP_VERIF_AEP(I_EMPRESA => I_EMPRESA,
                   I_FECINI  => V_PERI_FEC_INI,
                   I_FECFIN  => V_PERI_FEC_FIN,
                   I_PERIODO => I_PERI_CODIGO,
                   I_ART     => I_ART);
    ELSE
      PP_VERIF_AEP_TAGRO(I_EMPRESA => I_EMPRESA,
                         I_FECINI  => V_PERI_FEC_INI,
                         I_FECFIN  => V_PERI_FEC_FIN,
                         I_PERIODO => I_PERI_CODIGO,
                         I_ART     => I_ART);
    
    END IF;
  
    DBMS_OUTPUT.PUT_LINE('8 = ' ||
                         TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
  
    PP_VERIF_AEP_OPER(I_EMPRESA => I_EMPRESA,
                      I_FECINI  => V_PERI_FEC_INI,
                      I_FECFIN  => V_PERI_FEC_FIN,
                      I_PERIODO => I_PERI_CODIGO,
                      I_ART     => I_ART);
  
    DBMS_OUTPUT.PUT_LINE('9 = ' ||
                         TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
  
    PP_COMPARAR_CON_PERI_ANT(I_EMPRESA => I_EMPRESA,
                             I_PERIODO => V_PERI_SGTE,
                             I_ART     => I_ART);
    DBMS_OUTPUT.PUT_LINE('10 = ' ||
                         TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
  
    PP_COMPARAR_CON_PERI_ANT_OPER(I_EMPRESA => I_EMPRESA,
                                  I_PERIODO => V_PERI_SGTE,
                                  I_ART     => I_ART);
    DBMS_OUTPUT.PUT_LINE('11 = ' ||
                         TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
  
    IF I_EMPRESA in( 1, 20 ) THEN
      PP_VERIF_AEP(I_EMPRESA => I_EMPRESA,
                   I_FECINI  => V_PERI_SGTE_FEC_INI,
                   I_FECFIN  => V_PERI_SGTE_FEC_FIN,
                   I_PERIODO => V_PERI_SGTE,
                   I_ART     => I_ART);
    
    ELSE
    
      PP_VERIF_AEP_TAGRO(I_EMPRESA => I_EMPRESA,
                         I_FECINI  => V_PERI_SGTE_FEC_INI,
                         I_FECFIN  => V_PERI_SGTE_FEC_FIN,
                         I_PERIODO => V_PERI_SGTE,
                         I_ART     => I_ART);
    
    END IF;
  
    DBMS_OUTPUT.PUT_LINE('12 = ' ||
                         TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
  
    PP_VERIF_AEP_OPER(I_EMPRESA => I_EMPRESA,
                      I_FECINI  => V_PERI_SGTE_FEC_INI,
                      I_FECFIN  => V_PERI_SGTE_FEC_FIN,
                      I_PERIODO => V_PERI_SGTE,
                      I_ART     => I_ART);
  
    DBMS_OUTPUT.PUT_LINE('13 = ' ||
                         TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
  
    IF I_PERI_CODIGO = V_PARAM_PERI_ACT THEN
      PP_COMPARAR_ARDE_AEP(I_EMPRESA => I_EMPRESA, I_ART => I_ART);
      PP_COMPARAR_ARDE_AEP_OPER(I_EMPRESA => I_EMPRESA, I_ART => I_ART);
    END IF;
  
    DBMS_OUTPUT.PUT_LINE('14 = ' ||
                         TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
  
    IF I_PERI_CODIGO = V_PARAM_PERI_ACT THEN
      IF I_ESTADISTICA = 'Y' THEN
        PP_VERIF_EST(I_EMPRESA => I_EMPRESA, I_ART => I_ART);
      END IF;
    END IF;
  
    DBMS_OUTPUT.PUT_LINE('15 = ' ||
                         TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
  
    DBMS_OUTPUT.PUT_LINE('16 = ' ||
                         TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
  
    IF I_EMPRESA in( 1, 20 ) THEN
      GENERAL.PP_AUDITAR_VER_STK(P_EMPRESA => I_EMPRESA,
                                 P_PERIODO => I_PERI_CODIGO);
    
    END IF;
  
    DBMS_OUTPUT.PUT_LINE('17 = ' ||
                         TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
  
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      PP_ALTER_TRIGGERS('ENABLE');
      RAISE_APPLICATION_ERROR(-20021, SQLERRM);
  END PP_VERIFICAR_SISTEMA;

  
END STKP002;
/
