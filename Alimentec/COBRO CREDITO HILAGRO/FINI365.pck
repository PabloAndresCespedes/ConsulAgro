create or replace package FINI365 is

  -- Author  : @PabloACespedes
  -- Created : 29/08/2022 16:15:07
  -- Purpose : Generar los documentos para funcionarios de grupo
  
  function obt_funcionario_grupo(
    in_empresa in fin_configuracion.conf_empr%type
  )return fin_configuracion.conf_cat_cli_func_grupo%type;
  
  -- 29/08/2022 16:49:25 @PabloACespedes \(^-^)/
  -- genera una coleccion de APEX con los datos disponibles
  -- para ser cobrados, por cliente o todos los de la categoria
  -- funcionario de grupo (CATEGORIA CLIENTE)
  -- @out_error: tiene reglas de nomenclaturas de output, 
  -- para el Js del APEX
  procedure generar_detalle(
    in_empresa         in fin_documento.doc_empr%type,
    in_empresa_grupo   in gen_empresa.empr_codigo%type,
    in_cliente         in fin_documento.doc_cli%type,
    in_nro_recibo      in varchar2,
    in_fecha_operacion in fin_documento.doc_fec_oper%type,
    in_fecha_documento in fin_documento.doc_fec_doc%type,
    in_fecha_corte     in fin_cuota.cuo_fec_vto%type,
    out_error          out varchar2
  );
  
end FINI365;
/
create or replace package body FINI365 is
  
  co_col_detalle constant varchar2(15 char) := 'DET_FINI365';
       
  function obt_funcionario_grupo(
    in_empresa in fin_configuracion.conf_empr%type
  )return fin_configuracion.conf_cat_cli_func_grupo%type is
    l_r fin_configuracion.conf_cat_cli_func_grupo%type;
  begin
    select c.conf_cat_cli_func_grupo
    into l_r 
    from fin_configuracion c
    where c.conf_empr = in_empresa;
    
    if l_r is not null then
      return l_r;
    else
      raise no_data_found;
    end if;
                                    
  exception
    when no_data_found then
      Raise_application_error(-20000, 'Es necesario configurar el funcionario de grupo');
  end obt_funcionario_grupo;
  
  --
  function obt_factura_credito_emit(
    in_empresa in fin_configuracion.conf_empr%type
  ) return fin_configuracion.conf_fact_cr_emit%type is
   l_r fin_configuracion.conf_fact_cr_emit%type;
  begin
    select c.conf_fact_cr_emit
    into l_r
    from fin_configuracion c
    where c.conf_empr = in_empresa ;
    
    if l_r is not null then
      return l_r;
    else
      raise no_data_found;
    end if;
  exception
    when no_data_found then
      Raise_application_error(-20000, 'Es necesario configurar el tipo de movimiento Factura Cr'||chr(233)||'dito Emitida');
  end obt_factura_credito_emit;
  -- 
  procedure generar_detalle(
    in_empresa         in fin_documento.doc_empr%type,
    in_empresa_grupo   in gen_empresa.empr_codigo%type,
    in_cliente         in fin_documento.doc_cli%type,
    in_nro_recibo      in varchar2,
    in_fecha_operacion in fin_documento.doc_fec_oper%type,
    in_fecha_documento in fin_documento.doc_fec_doc%type,
    in_fecha_corte     in fin_cuota.cuo_fec_vto%type,
    out_error          out varchar2
  )as
   l_tmv_fact_cred_emit fin_configuracion.conf_fact_cr_emit%type;
   l_cat_cli_func_grupo fin_configuracion.conf_cat_cli_func_grupo%type;
   
   l_cliente  apex_application_global.vc_arr2;
   l_importe  apex_application_global.vc_arr2;
   l_cli_desc apex_application_global.vc_arr2;
   l_vto_cuo  apex_application_global.vc_arr2;
   l_tipo_mov apex_application_global.vc_arr2;
   l_f_doc    apex_application_global.vc_arr2;
   
   /*
    Errores configuracion:
    NO_FG      >> Sin conf. clie. func. grupo
    NO_EMP_G   >> Sin Empresa grupo
    NO_NRO_REC >> Sin nro recibo
    NO_F_OPE   >> Sin Fecha operacion
    NO_F_DOC   >> Sin Fecha documento
    NO_F_COR   >> Sin Fecha corte
  */
   l_error    varchar2(50 char);
  begin
    apex_collection.create_or_truncate_collection(p_collection_name => co_col_detalle);
    
    l_tmv_fact_cred_emit := obt_factura_credito_emit(in_empresa => in_empresa);
    l_cat_cli_func_grupo := obt_funcionario_grupo(in_empresa => in_empresa);
    
    <<v_data>>
    case
      when l_cat_cli_func_grupo is null then
        l_error := 'NO_FG';
        
      when in_empresa_grupo is null then
        l_error := 'NO_EMP_G';
      
      when nvl(in_nro_recibo, 0) <= 0 then
        l_error := 'NO_NRO_REC';
      
      when in_fecha_operacion is null then
        l_error := 'NO_F_OPE';
        
      when in_fecha_documento is null then
        l_error := 'NO_F_DOC';
        
      when in_fecha_corte is null then
        l_error := 'NO_F_COR';
        
      else
        l_error := null;
    end case v_data;
    
    if l_error is null then    
      select f.doc_cli      cliente,
             fc.cuo_imp_loc importe,
             c.cli_nom      cliente_desc,
             fc.cuo_fec_vto vto_cuota,
             f.doc_tipo_mov tipo_mov,
             f.doc_fec_doc  fecha_documento
      bulk collect into l_cliente,
                        l_importe,
                        l_cli_desc,
                        l_vto_cuo,
                        l_tipo_mov,
                        l_f_doc
      from fin_documento f
      inner join fin_cliente c on ( c.cli_codigo = f.doc_cli and c.cli_empr = f.doc_empr )
      inner join fin_cuota fc on (fc.cuo_clave_doc = f.doc_clave and fc.cuo_empr = f.doc_empr)
      inner join table (fini237.pipe_per_empleado_grupo(in_empr_grupo => to_char(in_empresa_grupo))) emp_x 
            on (emp_x.cod_empl = c.cli_cod_empl_empr_orig)
      where f.doc_empr = in_empresa
      and   c.cli_categ = l_cat_cli_func_grupo
      and   f.doc_saldo_loc > 0
      and   fc.cuo_saldo_loc > 0
      and   f.doc_tipo_mov = l_tmv_fact_cred_emit
      and   ( in_cliente is null or f.doc_cli = in_cliente );
            
      if l_cliente.count > 0 then
          
          apex_collection.add_members(p_collection_name => co_col_detalle,
                                      p_c001            => l_cliente,
                                      p_c002            => l_importe,
                                      p_c003            => l_cli_desc,
                                      p_c004            => l_vto_cuo,
                                      p_c005            => l_tipo_mov,
                                      p_c006            => l_f_doc
                                      );
      end if;
    else
      out_error := l_error;
    end if;          
  end generar_detalle;
  
end FINI365;
/
