CREATE OR REPLACE PACKAGE PRDI016 IS

  COLEC_ENT VARCHAR2(40) := 'PRDI016_ARTICULO_ENTRADAS';
  COLEC_SAL VARCHAR2(40) := 'PRDI016_ARTICULO_SALIDAS';

  FUNCTION FC_COLECCION_DUPLICADO(P_S_ART IN VARCHAR2, P_COL IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PR_CREAR_COLECCIONES;

  PROCEDURE PL_VALIDAR_HAB_MES_STK(FECHA   IN DATE,
                                   EMPRESA IN NUMBER,
                                   USUARIO IN VARCHAR2);

  PROCEDURE PP_MOSTRAR_DESC_DEPOSITO(P_DOCU_DEP_ORIG IN NUMBER,
                                     P_EMPRESA       IN NUMBER,
                                     P_SUCURSAL      IN NUMBER,
                                     P_S_DESC_DEP    OUT VARCHAR2);
  FUNCTION FL_MON_US(V_EMPRESA IN NUMBER) RETURN NUMBER;

  PROCEDURE PP_MOSTRAR_DESC_MONEDA(P_DOCU_MON      IN NUMBER,
                                   P_EMPRESA       IN NUMBER,
                                   P_W_MON_DEC_IMP OUT VARCHAR2,
                                   P_MON_DESC      OUT VARCHAR2,
                                   P_MON_SIM       OUT VARCHAR2);
  FUNCTION FP_BUSCAR_PERI_CODIGO(FECHA IN DATE, P_EMPRESA IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PP_BUSCAR_DESC_ARTICULO_SAL(ARTICULO IN NUMBER
                                        ,DESCRIPCION IN OUT VARCHAR2
                                        ,P_EMPRESA IN NUMBER);

  FUNCTION FP_COTIZACION(MONEDA          IN NUMBER,
                         P_EMPRESA       IN VARCHAR2,
                         P_DOCU_FEC_EMIS IN DATE) RETURN NUMBER;

  PROCEDURE PR_ELIMINAR_ARTICULO_ENT(P_SEQ_ID IN NUMBER);

  PROCEDURE PR_GUARDAR_ARTICULO_ENT(P_SEQ_ID   IN VARCHAR2,
                                    P_ART_ENT  IN VARCHAR2,
                                    P_CANTIDAD IN VARCHAR2);


   PROCEDURE PR_NUEVO_ARTICULO(P_ART_ENT  IN VARCHAR2
                             ,P_CANTIDAD IN VARCHAR2
                             ,P_FORM_NRO IN varchar2
                             ,P_EMPRESA       IN VARCHAR2,
                              P_W_PERI_CODIGO IN NUMBER,
                              P_FEC_EMIS      IN DATE);

  PROCEDURE PP_BUSCAR_COSTO_UNIT(Articulo IN NUMBER,
                                   CostoMon OUT NUMBER,
                                   CostoLoc OUT NUMBER,
                                   fecha_inic IN DATE,
                                   W_PERI_CODIGO IN NUMBER);

  PROCEDURE PR_BORRAR_ART_COLEC(P_SEQ_ID IN VARCHAR2, P_COLEC IN VARCHAR2);


  PROCEDURE PP_VALIDAR_SI_EXISTE_DOCUMENTO(P_EMPRESA      IN VARCHAR2,
                                           P_DOCU_NRO_DOC IN NUMBER,
                                           P_QUERY        OUT VARCHAR2);

  PROCEDURE PP_CARGAR_DOCUMENTO_SAL(P_EMPRESA       IN VARCHAR2,
                                    P_DOCU_NRO_DOC  IN NUMBER,
                                    P_DOCU_FEC_EMIS OUT DATE,
                                    P_DOCU_DEP_ORIG OUT VARCHAR2,
                                    P_DOCU_MON      OUT NUMBER);

  PROCEDURE PP_CARGAR_DOCUMENTO_ENT(P_EMPRESA      IN VARCHAR2,
                                    P_DOCU_NRO_DOC IN NUMBER);

  PROCEDURE PP_IMPRIMIR_DOC(I_EMPRESA              IN NUMBER,
                            I_DOCUT_CLAVE          IN NUMBER,
                            I_P_CANT_DECIMALES_LOC IN NUMBER,
                            I_DOCU_ENTRADA         IN VARCHAR2,
                            I_DOCU_SALIDA          IN VARCHAR2);

  PROCEDURE PP_VALIDAR_EXISTENCIA_SAL;

  PROCEDURE PP_GRABAR_DOCUMENTOS(P_W_CLAVE_PADRE      IN OUT VARCHAR2,
                                 P_W_CLAVE_HIJO       IN OUT VARCHAR2,
                                 P_W_TASA             IN VARCHAR2 DEFAULT NULL,
                                 P_OPER_ENT           IN VARCHAR2 DEFAULT NULL,
                                 P_EMPRESA            IN NUMBER DEFAULT V('P_EMPRESA'),
                                 P_SUCURSAL           IN NUMBER,
                                 P_DOCU_NRO_DOC       IN VARCHAR2,
                                 P_DOCU_SUC_ORIG      IN VARCHAR2,
                                 P_DOCU_DEP_ORIG      IN VARCHAR2,
                                 P_DOCU_MON           IN VARCHAR2,
                                 P_DOCU_FEC_EMIS      IN VARCHAR2,
                                 P_DOCU_EXEN_NETO_LOC IN VARCHAR2 DEFAULT NULL,
                                 P_DOCU_EXEN_NETO_MON IN VARCHAR2 DEFAULT NULL,
                                 P_CANT_DECIMALES_LOC IN NUMBER,
                                 P_CANT_DECIMALES_US  IN NUMBER,
                                 P_OPER               IN VARCHAR2,
                                 P_PROGRAMA           IN VARCHAR2);

  PROCEDURE VALIDAR_CABECERA(I_COMPROBANTE  IN NUMBER,
               I_FECHA      IN DATE,
               I_DEPOSITO   IN NUMBER,
               I_MONEDA     IN NUMBER);

  PROCEDURE PP_BUSCAR_COMPONENTES2 (I_DETA_ART      IN NUMBER,
                  I_S_FORM_NRO      IN NUMBER,
                  I_DETA_CANT     IN NUMBER,
                                  I_FEC_EMIS        IN DATE,
                                  I_W_PERI_COD      IN NUMBER,
                                  O_P_IMPORTE_MON   OUT NUMBER,
                                  O_P_IMPORTE_LOC   OUT NUMBER);

  FUNCTION FP_VERIF_UM (UNIDAD_MEDIDA IN VARCHAR2,
                        CANTIDAD      IN NUMBER) RETURN NUMBER;

  PROCEDURE PP_RECALCULAR_IMPORTES;

  PROCEDURE PP_MONTOS_COLEC (I_FEC_EMIS   IN DATE,
                           I_W_PERI_COD IN NUMBER,
                           O_P_IMPORTE_MON  OUT NUMBER,
                           o_P_IMPORTE_LOC OUT NUMBER);

END;
/
CREATE OR REPLACE PACKAGE BODY PRDI016 IS

  FUNCTION FC_COLECCION_DUPLICADO(P_S_ART IN VARCHAR2, P_COL IN VARCHAR2)
    RETURN NUMBER IS
    V_SEQ_ID NUMBER;
  BEGIN

    SELECT SEQ_ID
      INTO V_SEQ_ID
      FROM APEX_COLLECTIONS
     WHERE UPPER(COLLECTION_NAME) = UPPER(P_COL)
       AND UPPER(C002) = UPPER(P_S_ART);

    RETURN V_SEQ_ID;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN - 1;
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'Error obteniendo coleccion duplicado');
  END;

  PROCEDURE PR_CREAR_COLECCIONES IS
  BEGIN
    IF APEX_COLLECTION.COLLECTION_EXISTS(COLEC_SAL) THEN
      APEX_COLLECTION.CREATE_OR_TRUNCATE_COLLECTION(COLEC_SAL);
    ELSE
      APEX_COLLECTION.CREATE_COLLECTION(COLEC_SAL);
    END IF;
    IF APEX_COLLECTION.COLLECTION_EXISTS(COLEC_ENT) THEN
      APEX_COLLECTION.CREATE_OR_TRUNCATE_COLLECTION(COLEC_ENT);
    ELSE
      APEX_COLLECTION.CREATE_COLLECTION(COLEC_ENT);
    END IF;
  END;

  PROCEDURE PL_VALIDAR_HAB_MES_STK(FECHA   IN DATE,
                                   EMPRESA IN NUMBER,
                                   USUARIO IN VARCHAR2) IS
    V_VARIABLE             VARCHAR2(1);
    V_OPER_IND_HAB_MES_ACT VARCHAR2(1);

  BEGIN
    --VALIDAR SI ESTA DESHABILITADO EL PERIODO ACTUAL DE STOCK
    BEGIN
      SELECT 'X'
        INTO V_VARIABLE
        FROM STK_CONFIGURACION, STK_PERIODO
       WHERE CONF_PERIODO_ACT = PERI_CODIGO
         AND CONF_EMPR = PERI_EMPR
         AND FECHA BETWEEN PERI_FEC_INI AND PERI_FEC_FIN
            --AND CONF_IND_HAB_MES_ACT = 'N'
         AND CONF_IND_HAB_MES_ACT IN ('N', 'S')
         AND CONF_EMPR = EMPRESA;
      -----------------------------------------------------------------------------------------------------------
      IF V_VARIABLE = 'X' THEN
        BEGIN
          SELECT NVL(OPEM_IND_HAB_MES_STK, 'N')
            INTO V_OPER_IND_HAB_MES_ACT
            FROM GEN_OPERADOR, GEN_OPERADOR_EMPRESA
           WHERE OPER_CODIGO = OPEM_OPER
             AND OPER_LOGIN = USUARIO
             AND OPEM_EMPR = EMPRESA;

          IF V_OPER_IND_HAB_MES_ACT = 'N' THEN
            RAISE_APPLICATION_ERROR(-20010,
                                    'El periodo actual de STOCK esta deshabilitado para este usuario, avise al dpto. de informatica!.');
          END IF;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            NULL;
        END;
      END IF;
      -----------------------------------------------------------------------------------------------------------------
      --PL_EXHIBIR_ERROR('El periodo actual esta deshabilitado para el sistema de stock, avise al dpto. de informatica!.');
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;

    --SI YA SE HIZO EL PRE-CIERRE DEL SISTEMA STK ENTONCES
    --NO SE DEBE PERMITIR INGRESAR MOVIMIENTOS
    DECLARE
      V_COCI_IND_PRE_CERRADO VARCHAR2(1);
      V_CONF_EMPR            NUMBER;
      V_CONF_SUC             NUMBER;
    BEGIN
      SELECT CONF_EMPR, CONF_SUC
        INTO V_CONF_EMPR, V_CONF_SUC
        FROM GEN_CONFIGURACION
       WHERE CONF_EMPR = EMPRESA;
      SELECT COCI_IND_PRE_CERRADO
        INTO V_COCI_IND_PRE_CERRADO
        FROM GEN_CONTROL_CIERRE
       WHERE COCI_EMPR = V_CONF_EMPR
         AND COCI_SUC = V_CONF_SUC
         AND COCI_SISTEMA = (SELECT SIST_CODIGO
                               FROM GEN_SISTEMA
                              WHERE SIST_DESC_ABREV = 'STK')
         AND COCI_PERIODO = (SELECT CONF_PERIODO_ACT
                               FROM STK_CONFIGURACION
                              WHERE CONF_EMPR = EMPRESA)
         AND FECHA = (SELECT FECHA
                        FROM STK_CONFIGURACION, STK_PERIODO
                       WHERE CONF_PERIODO_ACT = PERI_CODIGO
                         AND CONF_EMPR = PERI_EMPR
                         AND FECHA BETWEEN PERI_FEC_INI AND PERI_FEC_FIN
                         AND CONF_EMPR = V_CONF_EMPR);
      IF V_COCI_IND_PRE_CERRADO = 'S' THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'El periodo actual ya fue pre-cerrado para el sistema de stock!');
      END IF;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
  END;

  PROCEDURE PP_MOSTRAR_DESC_DEPOSITO(P_DOCU_DEP_ORIG IN NUMBER,
                                     P_EMPRESA       IN NUMBER,
                                     P_SUCURSAL      IN NUMBER,
                                     P_S_DESC_DEP    OUT VARCHAR2) IS
  BEGIN
    SELECT DEP_DESC
      INTO P_S_DESC_DEP
      FROM STK_DEPOSITO
     WHERE DEP_EMPR = P_EMPRESA
       AND DEP_SUC = P_SUCURSAL
       AND DEP_CODIGO = P_DOCU_DEP_ORIG;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      P_S_DESC_DEP := NULL;
      RAISE_APPLICATION_ERROR(-20010, 'Deposito inexistente!');
  END;

  FUNCTION FL_MON_US(V_EMPRESA IN NUMBER) RETURN NUMBER IS
    V_MON_US GEN_CONFIGURACION.CONF_MON_US%TYPE;
  BEGIN
    SELECT CONF_MON_US
      INTO V_MON_US
      FROM GEN_CONFIGURACION
     WHERE CONF_EMPR = V_EMPRESA;

    RETURN V_MON_US;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'Moneda U$ inexistente en GEN_CONFIGURACION!');
    WHEN OTHERS THEN
      NULL;
  END;

  PROCEDURE PP_MOSTRAR_DESC_MONEDA(P_DOCU_MON      IN NUMBER,
                                   P_EMPRESA       IN NUMBER,
                                   P_W_MON_DEC_IMP OUT VARCHAR2,
                                   P_MON_DESC      OUT VARCHAR2,
                                   P_MON_SIM       OUT VARCHAR2) IS
  BEGIN
    SELECT MON_DESC, MON_DEC_IMP, MON_SIMBOLO
      INTO P_MON_DESC, P_W_MON_DEC_IMP, P_MON_SIM
      FROM GEN_MONEDA
     WHERE MON_CODIGO = P_DOCU_MON
       AND MON_EMPR = P_EMPRESA;

    -- ESTABLECER MASCARAS DE EDICION PARA LOS CAMPOS DE IMPORTES

    --PP_FORMAT_MASK('BSAL.DETA_IMP_NETO_LOC', :PARAMETER.P_CANT_DECIMALES_LOC);
    --PP_FORMAT_MASK('BENT.DETA_IMP_NETO_LOC', :PARAMETER.P_CANT_DECIMALES_LOC);
    --PP_FORMAT_MASK('BSAL.W_TOT_IMP_SAL_LOC', :PARAMETER.P_CANT_DECIMALES_LOC);
    --PP_FORMAT_MASK('BENT.W_TOT_IMP_ENT_LOC', :PARAMETER.P_CANT_DECIMALES_LOC);

    /*PP_FORMAT_MASK('BSAL.W_TOT_IMP_SAL_MON', :PARAMETER.P_CANT_DECIMALES_US);
    PP_FORMAT_MASK('BENT.W_TOT_IMP_ENT_MON', :PARAMETER.P_CANT_DECIMALES_US);
    PP_FORMAT_MASK('BSAL.DETA_IMP_NETO_MON', :PARAMETER.P_CANT_DECIMALES_US);
    PP_FORMAT_MASK('BENT.DETA_IMP_NETO_MON', :PARAMETER.P_CANT_DECIMALES_US);*/
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20010, 'Moneda inexistente!');
  END;

  PROCEDURE PR_ELIMINAR_ARTICULO_ENT(P_SEQ_ID IN NUMBER) IS
  BEGIN
    APEX_COLLECTION.DELETE_MEMBER(P_COLLECTION_NAME => COLEC_SAL,
                                  P_SEQ             => P_SEQ_ID);
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20010, 'Error eliminando el articulo!');
  END;

  PROCEDURE PR_GUARDAR_ARTICULO_ENT(P_SEQ_ID   IN VARCHAR2,
                                    P_ART_ENT  IN VARCHAR2,
                                    P_CANTIDAD IN VARCHAR2) IS
    V_DUMMY VARCHAR2(2);
  BEGIN
    SELECT 1
      INTO V_DUMMY
      FROM APEX_COLLECTIONS
     WHERE COLLECTION_NAME = COLEC_SAL
       AND SEQ_ID = P_SEQ_ID;

    APEX_COLLECTION.UPDATE_MEMBER_ATTRIBUTE(P_COLLECTION_NAME => COLEC_SAL,
                                            P_SEQ             => P_SEQ_ID,
                                            P_ATTR_NUMBER     => '1',
                                            P_ATTR_VALUE      => P_ART_ENT);

    APEX_COLLECTION.UPDATE_MEMBER_ATTRIBUTE(P_COLLECTION_NAME => COLEC_SAL,
                                            P_SEQ             => P_SEQ_ID,
                                            P_ATTR_NUMBER     => '3',
                                            P_ATTR_VALUE      => P_CANTIDAD);

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => COLEC_SAL,
                                 P_C001            => P_ART_ENT,
                                 P_C003            => P_CANTIDAD);
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20010, 'Error guardando el articulo!');
  END;

  FUNCTION FP_BUSCAR_ART_COD_ALFANUM_SAL(P_S_ART_SAL IN NUMBER,
                                         P_EMPRESA   IN NUMBER,
                                         P_DETA_ART  OUT VARCHAR2,
                                         P_ART_DESC  OUT VARCHAR2,
                                         P_S_ART_UM  OUT VARCHAR2)
    RETURN BOOLEAN IS
    V_EST STK_ARTICULO.ART_EST%TYPE;
  BEGIN
    SELECT ART_CODIGO, ART_DESC, ART_EST, ART_UNID_MED
      INTO P_DETA_ART, P_ART_DESC, V_EST, P_S_ART_UM
      FROM STK_ARTICULO
     WHERE ART_COD_ALFANUMERICO = P_S_ART_SAL
       AND ART_EMPR = P_EMPRESA;

    IF V_EST = 'I' THEN
      RAISE_APPLICATION_ERROR(-20010, 'Articulo inactivo');
      RETURN FALSE;
    ELSE
      RETURN TRUE;
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN FALSE;
  END;

  PROCEDURE PR_NUEVO_ARTICULO(P_ART_ENT  IN VARCHAR2
                             ,P_CANTIDAD IN VARCHAR2
                             ,P_FORM_NRO IN varchar2
                             ,P_EMPRESA       IN VARCHAR2,
                              P_W_PERI_CODIGO IN NUMBER,
                              P_FEC_EMIS      IN DATE) IS
    V_ART_DESC      VARCHAR2(400);
    V_UNIDAD_MEDIDA VARCHAR2(100);
    W_COSTO_LOC     VARCHAR2(100);
    W_COSTO_MON     VARCHAR2(100);
    W_MON           VARCHAR2(100);
    V_NRO_ITEM      NUMBER;

    V_SEQ_ID_DUPLICADO NUMBER;
    V_DETA_CANT_D      NUMBER;
  BEGIN

    -- validar disponibilidad de los ingredientes del articulo seleciconado --

    --  PP_BUSCAR_DESC_ARTICULO_SAL
    SELECT ART_DESC, ART_UNID_MED
      INTO V_ART_DESC, V_UNIDAD_MEDIDA
      FROM STK_ARTICULO
     WHERE ART_CODIGO = P_ART_ENT
       AND ART_EMPR = P_EMPRESA;
    --PP_BUSCAR_COSTO_UNIT
    PP_BUSCAR_COSTO_UNIT(P_ART_ENT,
                         W_COSTO_LOC,
                         W_COSTO_MON,
                         P_FEC_EMIS,
                         P_W_PERI_CODIGO);

    V_SEQ_ID_DUPLICADO := FC_COLECCION_DUPLICADO(P_ART_ENT,
                                                         COLEC_SAL);

    V_NRO_ITEM := APEX_COLLECTION.COLLECTION_MEMBER_COUNT(P_collection_name => 'PRDI016_ARTICULO_SALIDAS') + 1;

    IF V_SEQ_ID_DUPLICADO = -1 THEN
      APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => COLEC_SAL,
                                 P_C001            => V_NRO_ITEM,
                                 P_C002            => P_ART_ENT,
                                 P_C003            => P_CANTIDAD,
                                 P_C004            => V_UNIDAD_MEDIDA,
                                 P_C005            => P_ART_ENT,
                                 P_C006            => V_ART_DESC
                                 --,P_C007 => P_COSTO
                                ,
                                 P_C011 => W_COSTO_LOC,
                                 P_C012 => W_COSTO_MON,
                                 P_C013 => W_MON,
                                 P_C014 => P_FORM_NRO);
    ELSE
      SELECT C003
        INTO V_DETA_CANT_D
        FROM APEX_COLLECTIONS
       WHERE UPPER(COLLECTION_NAME) = UPPER(COLEC_SAL)
         AND SEQ_ID = V_SEQ_ID_DUPLICADO;

      APEX_COLLECTION.UPDATE_MEMBER_ATTRIBUTE(P_COLLECTION_NAME => COLEC_SAL,
                                              P_SEQ             => V_SEQ_ID_DUPLICADO,
                                              P_ATTR_NUMBER     => 3,
                                              P_ATTR_VALUE      => V_DETA_CANT_D +
                                                                   P_CANTIDAD);

    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20010, 'Error agregando el articulo! ' || SQLERRM);
  END;

  FUNCTION FP_BUSCAR_PERI_CODIGO(FECHA IN DATE, P_EMPRESA IN VARCHAR2)
    RETURN NUMBER IS
    V_PERI_CODIGO NUMBER;
  BEGIN
    SELECT PERI_CODIGO
      INTO V_PERI_CODIGO
      FROM STK_PERIODO
     WHERE FECHA BETWEEN PERI_FEC_INI AND PERI_FEC_FIN
       AND PERI_EMPR = P_EMPRESA;
    RETURN V_PERI_CODIGO;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'No puede encontrar en codigo de periodo!');
  END;

  PROCEDURE PP_BUSCAR_DESC_ARTICULO_SAL(ARTICULO IN NUMBER
                                        ,DESCRIPCION IN OUT VARCHAR2
                                        ,P_EMPRESA IN NUMBER) IS
    --V_DESC VARCHAR2(80);
    V_EST STK_ARTICULO.ART_EST%TYPE;
  BEGIN
        SELECT  ART_EST
        INTO  V_EST
        FROM STK_ARTICULO,
             STK_ARTICULO_EMPRESA
       WHERE ART_CODIGO = AREM_ART
         AND ART_EMPR = AREM_EMPR
         AND AREM_EMPR = P_EMPRESA
         AND ART_CODIGO = ARTICULO;

      IF V_EST = 'I' THEN
        DESCRIPCION := NULL;
        RAISE_APPLICATION_ERROR(-20010,'Articulo inactivo!.');
      END IF;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
         DESCRIPCION := NULL;
         RAISE_APPLICATION_ERROR(-20010,'Articulo inexistente!');
  END;

  PROCEDURE PP_BUSCAR_COSTO_UNIT(Articulo IN NUMBER,
                                   CostoMon OUT NUMBER,
                                   CostoLoc OUT NUMBER,
                                   fecha_inic IN DATE,
                                   W_PERI_CODIGO IN NUMBER) IS

    CURSOR C_PREC_POR_DIA IS

        SELECT G.CD_FEC,
                ROUND(G.CD_COSTO_PROM_LOC),
                ROUND(G.CD_COSTO_PROM_LOC/C.COT_VENTA,4)
          FROM STK_COSTO_DIARIO G,
                STK_COTIZACION C
         WHERE G.CD_ART = ARTICULO
           AND G.CD_FEC= fecha_inic
           AND G.CD_FEC = C.COT_FEC
           AND G.CD_EMPR = C.COT_EMPR
           AND G.CD_EMPR = NV('P_EMPRESA')
           AND C.COT_MON = 2;

    V_FEC DATE;
    V_C_GS NUMBER;
    V_C_USD NUMBER;
    SALIR EXCEPTION;

    BEGIN

        --1ERA FORMA DE BUSCAR COSTOS, A TRAVEZ DE LA FECHA DE LA OPERACION, (EN FORMA DIFERIDA)

        OPEN C_PREC_POR_DIA;
        LOOP
        FETCH C_PREC_POR_DIA INTO
            V_FEC, COSTOLOC,COSTOMON;
        EXIT WHEN C_PREC_POR_DIA%NOTFOUND OR (COSTOLOC>0 AND COSTOMON>0) ;
        NULL;
      END LOOP;
      CLOSE C_PREC_POR_DIA;


        IF COSTOLOC>0 AND COSTOMON>0 THEN
            RAISE SALIR ;
        END IF;


        --2DA FORMA
      SELECT AEP_COSTO_PROM_LOC, AEP_COSTO_PROM_MON
        INTO CostoLoc, CostoMon
        FROM STK_ART_EMPR_PERI
       WHERE AEP_PERIODO = W_PERI_CODIGO
         AND AEP_EMPR =  NV('P_EMPRESA')
         AND AEP_ART = Articulo;


    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        CostoMon := NULL;
        CostoLoc := NULL;
        RAISE_APPLICATION_ERROR(-20010,'Costo unitario inexistente. Debe habilitar el articulo para la empresa!');
      when salir then
        null;
    END;


  FUNCTION FP_COTIZACION(MONEDA          IN NUMBER,
                         P_EMPRESA       IN VARCHAR2,
                         P_DOCU_FEC_EMIS IN DATE) RETURN NUMBER IS
    V_TASA NUMBER;
  BEGIN
    SELECT COT_TASA
      INTO V_TASA
      FROM STK_COTIZACION
     WHERE COT_FEC = P_DOCU_FEC_EMIS
       AND COT_MON = MONEDA
       AND COT_EMPR = P_EMPRESA;
    RETURN V_TASA;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'Primero debe ingresar la cotizacion del ' ||
                              'dia ' || P_DOCU_FEC_EMIS ||
                              ' para la moneda ' || TO_CHAR(MONEDA) || '!');
  END;

  PROCEDURE PR_BORRAR_ART_COLEC(P_SEQ_ID IN VARCHAR2, P_COLEC IN VARCHAR2) IS
  BEGIN
    APEX_COLLECTION.DELETE_MEMBER(P_COLLECTION_NAME => P_COLEC,
                                  P_SEQ             => P_SEQ_ID);
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20010, 'Error eliminando el articulo.');
  END;

  PROCEDURE PP_VALIDAR_SI_EXISTE_DOCUMENTO(P_EMPRESA      IN VARCHAR2,
                                           P_DOCU_NRO_DOC IN NUMBER,
                                           P_QUERY        OUT VARCHAR2) IS
    V_EXISTE NUMBER := NULL;
  BEGIN

    SELECT COUNT(*)
      INTO V_EXISTE
      FROM STK_DOCUMENTO_PRD, STK_OPERACION
     WHERE DOCU_EMPR = P_EMPRESA
       AND DOCU_NRO_DOC = P_DOCU_NRO_DOC
       AND DOCU_CODIGO_OPER = OPER_CODIGO
       AND DOCU_EMPR = OPER_EMPR
       AND OPER_DESC IN ('SAL_PROD','SAL_PROD_RP');

    IF V_EXISTE > 0 THEN
      P_QUERY := 'S';
    ELSE
      P_QUERY := 'N';
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'Error validando si existe documento');
  END;

  PROCEDURE PP_CARGAR_DOCUMENTO_SAL(P_EMPRESA       IN VARCHAR2,
                                    P_DOCU_NRO_DOC  IN NUMBER,
                                    P_DOCU_FEC_EMIS OUT DATE,
                                    P_DOCU_DEP_ORIG OUT VARCHAR2,
                                    P_DOCU_MON      OUT NUMBER) IS
    V_DETA_ART VARCHAR2(400);
    V_ART_DESC VARCHAR2(400);
    V_S_ART_UM VARCHAR2(400);
    V_ART_COD_ALFANUMERICO VARCHAR2(400);

    CURSOR CARGA_DOC IS
    SELECT DETA_NRO_ITEM,
           DETA_ART,
           DETA_CANT,
           DETA_IMP_NETO_MON,
           DETA_IMP_NETO_LOC,
           DOCU_CLAVE
      FROM STK_DOCUMENTO_PRD,
           STK_DOCUMENTO_DET_PRD,
           STK_OPERACION
     WHERE DOCU_EMPR = P_EMPRESA
       AND DOCU_SUC_ORIG = NV('P_SUCURSAL')
       AND DOCU_NRO_DOC = P_DOCU_NRO_DOC
       AND DOCU_CODIGO_OPER = OPER_CODIGO
       AND DOCU_EMPR = OPER_EMPR
       AND DOCU_CLAVE = DETA_CLAVE_DOC
       AND DOCU_EMPR = DETA_EMPR
       AND OPER_DESC IN ('ENT_PROD', 'ENT_PROD_RP');

  BEGIN

    SELECT DOCU_FEC_EMIS, DOCU_DEP_ORIG, DOCU_MON
      INTO P_DOCU_FEC_EMIS, P_DOCU_DEP_ORIG, P_DOCU_MON
      FROM STK_DOCUMENTO_PRD, STK_OPERACION
     WHERE DOCU_EMPR = P_EMPRESA
       AND DOCU_NRO_DOC = P_DOCU_NRO_DOC
       AND DOCU_CODIGO_OPER = OPER_CODIGO
       AND DOCU_EMPR = OPER_EMPR
       AND OPER_DESC IN ('ENT_PROD', 'ENT_PROD_RP');

    FOR RSAL IN CARGA_DOC
    LOOP

         SELECT ART_COD_ALFANUMERICO
         INTO V_ART_COD_ALFANUMERICO
         FROM STK_ARTICULO
         WHERE ART_CODIGO = RSAL.DETA_ART;


         SELECT ART_CODIGO, ART_DESC,ART_UNID_MED
         INTO V_DETA_ART, V_ART_DESC,V_S_ART_UM
         FROM STK_ARTICULO
         WHERE ART_EMPR = P_EMPRESA
         AND ART_COD_ALFANUMERICO = V_ART_COD_ALFANUMERICO;

         APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => COLEC_SAL,
                                 P_C001            => RSAL.DETA_NRO_ITEM,
                                 P_C002            => V_ART_COD_ALFANUMERICO,
                                 P_C003            => RSAL.DETA_CANT,
                                 P_C004            => V_S_ART_UM,
                                 P_C005            => V_DETA_ART,
                                 P_C006            => V_ART_DESC,
                                 P_C007            => RSAL.DETA_IMP_NETO_LOC,
                                 P_C008            => RSAL.DETA_IMP_NETO_MON,
                                 P_C009            => RSAL.DOCU_CLAVE);

    END LOOP;

  EXCEPTION
    WHEN TOO_MANY_ROWS THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'Demasiados documentos con el mismo numero! Primero debe eliminar uno!');
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'Error en PP_CARGAR_DOCUMENTO_SAL ' ||
                              SQLERRM);
  END;

  PROCEDURE PP_CARGAR_DOCUMENTO_ENT(P_EMPRESA      IN VARCHAR2,
                                    P_DOCU_NRO_DOC IN NUMBER) IS
    V_DETA_ART VARCHAR2(400);
    V_ART_DESC VARCHAR2(400);
    V_S_ART_UM VARCHAR2(400);
    V_ART_COD_ALFANUMERICO VARCHAR2(400);
    V_AUXI_CANT NUMBER;

    CURSOR CARGA_DOC_ENT IS
    SELECT DETA_NRO_ITEM,
           DETA_ART,
           DETA_CANT,
           DETA_IMP_NETO_MON,
           DETA_IMP_NETO_LOC,
           DOCU_CLAVE --,ART_COD_ALFANUMERICO,DOCU_CLAVE
      FROM STK_DOCUMENTO_PRD, STK_DOCUMENTO_DET_PRD, STK_OPERACION --,STK_ARTICULO
     WHERE DOCU_EMPR = P_EMPRESA
       AND DOCU_SUC_ORIG = V('P_SUCURSAL')
       AND DOCU_NRO_DOC = P_DOCU_NRO_DOC
       AND DOCU_CODIGO_OPER = OPER_CODIGO
       AND DOCU_EMPR = OPER_EMPR
       AND DOCU_CLAVE = DETA_CLAVE_DOC
       AND DOCU_EMPR = DETA_EMPR
       AND OPER_DESC IN ('SAL_PROD', 'SAL_PROD_RP');

  BEGIN

    FOR RENT IN CARGA_DOC_ENT
    LOOP

         SELECT ART_COD_ALFANUMERICO
         INTO V_ART_COD_ALFANUMERICO
         FROM STK_ARTICULO
         WHERE ART_CODIGO = RENT.DETA_ART;

         SELECT ART_CODIGO, ART_DESC,ART_UNID_MED
         INTO V_DETA_ART, V_ART_DESC,V_S_ART_UM
         FROM STK_ARTICULO
         WHERE ART_EMPR = P_EMPRESA
         AND ART_COD_ALFANUMERICO = V_ART_COD_ALFANUMERICO;

         V_AUXI_CANT := FP_VERIF_UM (UNIDAD_MEDIDA => V_S_ART_UM,
                                    CANTIDAD      => RENT.DETA_CANT);

        APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME  =>  'PRDI016_ARTICULO_ENTRADAS',
                   P_C001       =>  V_ART_COD_ALFANUMERICO,
                   P_C002       =>  RENT.DETA_ART,
                   P_C003       =>  V_S_ART_UM,
                   P_C004       =>  V_ART_DESC,
                   P_C006       =>  RENT.DETA_NRO_ITEM,
                   P_C008       =>  RENT.DETA_CANT,
                                   P_C012               =>  RENT.DETA_IMP_NETO_LOC,
                                   P_C013               =>  RENT.DETA_IMP_NETO_MON,
                                   P_C018               =>  V_AUXI_CANT,
                                   P_C019               =>  RENT.DOCU_CLAVE);
    END LOOP;

  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'Error en PP_CARGAR_DOCUMENTO_ENT ' ||
                              SQLERRM);
  END;

  FUNCTION FP_BUSCAR_COD_OP(DESC_OPER_IN IN VARCHAR2, P_EMPRESA IN NUMBER)
    RETURN NUMBER IS
    V_OPER_CODIGO NUMBER := 0;

  BEGIN
    SELECT OPER_CODIGO
      INTO V_OPER_CODIGO
      FROM STK_OPERACION
     WHERE OPER_DESC = DESC_OPER_IN
       AND OPER_EMPR = P_EMPRESA;

    RETURN V_OPER_CODIGO;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20010, 'Pck: PRDI016, FP_BUSCAR_COD_OP: Tipo operacion inexistente! >> '||DESC_OPER_IN);-- 29/07/2022 14:56:59 @PabloACespedes \(^-^)/ se_agrego descripcion mas especifica
  END;

  PROCEDURE PP_IMPRIMIR_DOC(I_EMPRESA              IN NUMBER,
                            I_DOCUT_CLAVE          IN NUMBER,
                            I_P_CANT_DECIMALES_LOC IN NUMBER,
                            I_DOCU_ENTRADA         IN VARCHAR2,
                            I_DOCU_SALIDA          IN VARCHAR2) AS

    V_REPORT        VARCHAR2(10) := 'PRDI016';
    V_PARAMETROS    VARCHAR2(600);
    V_IDENTIFICADOR VARCHAR2(2) := '&'; --SE UTILIZA PARA QUE AL COMPILAR NO LO TOME COMO PARAMETROS

  BEGIN

    V_PARAMETROS := 'P_FORMATO=' || APEX_UTIL.URL_ENCODE('pdf');

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_EMPRESA=' ||
                    APEX_UTIL.URL_ENCODE(I_EMPRESA);

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_CLAVE=' ||
                    APEX_UTIL.URL_ENCODE(I_DOCUT_CLAVE);

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR ||
                    'P_CANT_DEC_MON=' ||
                    APEX_UTIL.URL_ENCODE(I_P_CANT_DECIMALES_LOC);

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_DOCU_ENTRADA=' ||
                    APEX_UTIL.URL_ENCODE(I_DOCU_SALIDA);

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_DOCU_SALIDA=' ||
                    APEX_UTIL.URL_ENCODE(I_DOCU_ENTRADA);

    DELETE FROM GEN_PARAMETROS_REPORT WHERE USUARIO = GEN_DEVUELVE_USER;

    INSERT INTO GEN_PARAMETROS_REPORT
      (PARAMETROS, USUARIO, NOMBRE_REPORTE, FORMATO_SALIDA)
    VALUES
      (V_PARAMETROS, GEN_DEVUELVE_USER, V_REPORT, 'pdf');

  END PP_IMPRIMIR_DOC;

  PROCEDURE PP_VALIDAR_EXISTENCIA_SAL  IS

    CURSOR SALIDAS IS
       SELECT
           C002 , --DETA ART
           C004 , -- ART DESC
           SUM(TO_NUMBER(C008))  AS  DETA_CANT
    FROM APEX_COLLECTIONS
    WHERE COLLECTION_NAME = 'PRDI016_ARTICULO_ENTRADAS' GROUP BY C002, C004;

    CURSOR BUSQUEDA (ART NUMBER) IS
           SELECT ARDE_ART, ARDE_CANT_ACT FROM STK_ARTICULO_DEPOSITO
           WHERE ARDE_ART = ART
           AND ARDE_EMPR = NV('P_EMPRESA')
           AND ARDE_SUC = NV('P_SUCURSAL')
           AND ARDE_DEP = NV('P8004_DOCU_DEP_ORIG');

  MSG VARCHAR2 (3267);
  BEGIN

       FOR SAL IN SALIDAS LOOP

           FOR E IN BUSQUEDA (SAL.C002) LOOP
             IF TO_NUMBER(E.ARDE_CANT_ACT) < TO_NUMBER(SAL.DETA_CANT) THEN
               MSG := MSG ||' " ' || SAL.C002 ||'-' ||SAL.C004|| ' " ' ||' REQUIERE ' ||SAL.DETA_CANT || ' , HAY DISPONIBLE ' || E.ARDE_CANT_ACT  ;
               MSG := MSG ||chr(10)||chr(13);
             END IF;
           END LOOP;
       END LOOP;

       IF MSG IS NOT NULL THEN
          RAISE_APPLICATION_ERROR (-20010, MSG);
       END IF;

  END;
  PROCEDURE PP_GRABAR_DOCUMENTOS(P_W_CLAVE_PADRE      IN OUT VARCHAR2,
                                 P_W_CLAVE_HIJO       IN OUT VARCHAR2,
                                 P_W_TASA             IN VARCHAR2 DEFAULT NULL,
                                 P_OPER_ENT           IN VARCHAR2 DEFAULT NULL,
                                 P_EMPRESA            IN NUMBER DEFAULT V('P_EMPRESA'),
                                 P_SUCURSAL           IN NUMBER,
                                 P_DOCU_NRO_DOC       IN VARCHAR2,
                                 P_DOCU_SUC_ORIG      IN VARCHAR2,
                                 P_DOCU_DEP_ORIG      IN VARCHAR2,
                                 P_DOCU_MON           IN VARCHAR2,
                                 P_DOCU_FEC_EMIS      IN VARCHAR2,
                                 P_DOCU_EXEN_NETO_LOC IN VARCHAR2 DEFAULT NULL,
                                 P_DOCU_EXEN_NETO_MON IN VARCHAR2 DEFAULT NULL,
                                 P_CANT_DECIMALES_LOC IN NUMBER,
                                 P_CANT_DECIMALES_US  IN NUMBER,
                                 P_OPER               IN VARCHAR2,
                                 P_PROGRAMA           IN VARCHAR2) IS
    V_OPER_CODIGO NUMBER;
    SALIR EXCEPTION;
    V_P_W_CLAVE_PADRE VARCHAR2(100) := P_W_CLAVE_PADRE;
    V_P_W_CLAVE_HIJO  VARCHAR2(100) := P_W_CLAVE_HIJO;
    V_MENSAJE         VARCHAR2(1000);

    V_WHERE_SAL VARCHAR2(200);
    V_WHERE_ENT VARCHAR2(200);

  V_DOCU_EXEN_NETO_LOC NUMBER;
  V_DOCU_EXEN_NETO_MON NUMBER;
  V_TASA  NUMBER;

  BEGIN
    -- VERIFICAR EXISTENCIA DE ITEMS DE SALIDA --

    PP_VALIDAR_EXISTENCIA_SAL;

    --GRABAR EL DOCUMENTO DE ENTRADA DE ARTICULOS
    IF P_W_CLAVE_PADRE IS NULL THEN
      V_P_W_CLAVE_PADRE := STK_SEQ_DOCU_NEXTVAL;
      V_P_W_CLAVE_HIJO  := STK_SEQ_DOCU_NEXTVAL;

      P_W_CLAVE_PADRE := V_P_W_CLAVE_PADRE;
      P_W_CLAVE_HIJO  := V_P_W_CLAVE_HIJO;
    END IF;
    V_DOCU_EXEN_NETO_LOC := V('P8004_W_TOT_IMP_SAL_LOC');
    V_DOCU_EXEN_NETO_MON := V('P8004_W_TOT_IMP_SAL_MON');
    V_TASA           := V('P8004_W_TASA');
    V_OPER_CODIGO := FP_BUSCAR_COD_OP(P_OPER_ENT, P_EMPRESA);

  BEGIN
      INSERT INTO STK_DOCUMENTO_PRD
        (DOCU_CLAVE,
         DOCU_CODIGO_OPER,
         DOCU_EMPR,
         DOCU_NRO_DOC,
         DOCU_SUC_ORIG,
         DOCU_DEP_ORIG,
         DOCU_MON,
         DOCU_FEC_EMIS,
         DOCU_GRAV_NETO_LOC,
         DOCU_GRAV_NETO_MON,
         DOCU_EXEN_NETO_LOC,
         DOCU_EXEN_NETO_MON,
         DOCU_TASA_US,
         DOCU_LOGIN,
         DOCU_FEC_GRAB,
         DOCU_SIST,
     DOCU_FORM,
     DOCU_LOR)
      VALUES
        (V_P_W_CLAVE_PADRE,
         V_OPER_CODIGO,
         P_EMPRESA,
         P_DOCU_NRO_DOC,
         P_DOCU_SUC_ORIG,
         P_DOCU_DEP_ORIG,
         P_DOCU_MON,
         P_DOCU_FEC_EMIS,
         0,
         0,
         P_DOCU_EXEN_NETO_LOC,
         P_DOCU_EXEN_NETO_MON,
         P_W_TASA,
         V('APP_USER'),
         SYSDATE,
         'STK',
     'PRDI012',
     GEN_LUGAR_ORIGEN);
    EXCEPTION
      WHEN OTHERS THEN
        RAISE;
    END;

    --SE REEMPLAZO SUBSTR(:PARAMETER.P_PROGRAMA,1,3) POR 'STK' PORQUE SE NECESITA
    --PARA PODER BORRAR REGISTROS GUARDADOS POR ESTE PROGRAMA DESDE STOCK.

    DECLARE

      V_DETA_IMP_NETO_LOC NUMBER;
      V_DETA_IMP_NETO_MON NUMBER;
    BEGIN
      FOR I IN (SELECT SEQ_ID DETA_NRO_ITEM,
                       C003   DETA_CANT,
                       C005   DETA_ART,
                       C006   ART_DESC,
                       C007   DETA_IMP_NETO_LOC,
                       C008   DETA_IMP_NETO_MON,
             C014   S_FORM_NRO
                  FROM APEX_COLLECTIONS
                 WHERE COLLECTION_NAME = COLEC_SAL) LOOP
        V_DETA_IMP_NETO_LOC := ROUND(I.DETA_IMP_NETO_LOC,
                                     P_CANT_DECIMALES_LOC);
        V_DETA_IMP_NETO_MON := ROUND(I.DETA_IMP_NETO_MON,
                                     P_CANT_DECIMALES_US);
        IF NVL(V_DETA_IMP_NETO_LOC, 0) < 0 OR
           NVL(V_DETA_IMP_NETO_MON, 0) < 0 THEN
          RAISE_APPLICATION_ERROR(-20010,
                                  'No se puede registrar importes negativos, Verifique bien los movimientos que tuvo el articulo :' ||
                                  I.DETA_ART || '  ' || I.ART_DESC || ' !.');
        END IF;
        BEGIN
          INSERT INTO STK_DOCUMENTO_DET_PRD
            (DETA_CLAVE_DOC,
             DETA_NRO_ITEM,
             DETA_ART,
             DETA_EMPR,
             DETA_CANT,
             DETA_IMP_NETO_LOC,
             DETA_IMP_NETO_MON,
             DETA_IMPU,
       DETA_NRO_FORMULA)
          VALUES
            (V_P_W_CLAVE_PADRE,
             I.DETA_NRO_ITEM,
             I.DETA_ART,
             P_EMPRESA,
             I.DETA_CANT,
             V_DETA_IMP_NETO_LOC,
             V_DETA_IMP_NETO_MON,
             'N',
       I.S_FORM_NRO);
        EXCEPTION
          WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR(-20010, SQLERRM);
        END;

      END LOOP;

      -- GRABAR EL DOCUMENTO DE SALIDA DE ARTICULOS
      V_OPER_CODIGO := FP_BUSCAR_COD_OP(P_OPER, P_EMPRESA);
      BEGIN
        INSERT INTO STK_DOCUMENTO_PRD
          (DOCU_CLAVE,
           DOCU_CLAVE_PADRE,
           DOCU_CODIGO_OPER,
           DOCU_EMPR,
           DOCU_NRO_DOC,
           DOCU_SUC_ORIG,
           DOCU_DEP_ORIG,
           DOCU_MON,
           DOCU_FEC_EMIS,
           DOCU_GRAV_NETO_LOC,
           DOCU_GRAV_NETO_MON,
           DOCU_EXEN_NETO_LOC,
           DOCU_EXEN_NETO_MON,
           DOCU_TASA_US,
           DOCU_LOGIN,
           DOCU_FEC_GRAB,
           DOCU_SIST,
           DOCU_FORM,
           DOCU_LOR)
        VALUES
          (V_P_W_CLAVE_HIJO,
           V_P_W_CLAVE_PADRE,
           V_OPER_CODIGO,
           P_EMPRESA,
           P_DOCU_NRO_DOC,
           P_DOCU_SUC_ORIG,
           P_DOCU_DEP_ORIG,
           P_DOCU_MON,
           P_DOCU_FEC_EMIS,
           0,
           0,
           P_DOCU_EXEN_NETO_LOC,
           P_DOCU_EXEN_NETO_MON,
           P_W_TASA,
           V('APP_USER'),
           SYSDATE,
           SUBSTR(P_PROGRAMA, 1, 3),
       'PRDI012',
       GEN_LUGAR_ORIGEN);
      EXCEPTION
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20010, SQLERRM);
      END;
    END;
    -- POST;

    --GRABAR LOS DETALLES DEL DOCUMENTO DE ENTRADA DE LOS ARTICULOS
    --GO_BLOCK('BENT');
    --FIRST_RECORD;
    DECLARE
      V_DETA_IMP_NETO_LOC NUMBER;
      V_DETA_IMP_NETO_MON NUMBER;
      V_C                 NUMBER;
    BEGIN
      /*SELECT COUNT(1)
        INTO V_C
        FROM APEX_COLLECTIONS
       WHERE COLLECTION_NAME = COLEC_ENT;*/
      --FOR I IN (SELECT (V_C + SEQ_ID) DETA_NRO_ITEM,
      FOR I IN (SELECT SEQ_ID DETA_NRO_ITEM,
                       C008 DETA_CANT,
                       C002 DETA_ART,
                       C004 ART_DESC,
                       C012 DETA_IMP_NETO_LOC,
                       C013 DETA_IMP_NETO_MON,
             C005 S_FORM_NRO,
                       C011 DETA_ART_ENT,
                       C019 DETA_CLAVE_ENT_PROD
                  FROM APEX_COLLECTIONS
                 WHERE COLLECTION_NAME = COLEC_ENT) LOOP
        V_DETA_IMP_NETO_LOC := ROUND(I.DETA_IMP_NETO_LOC,
                                     P_CANT_DECIMALES_LOC);
        V_DETA_IMP_NETO_MON := ROUND(I.DETA_IMP_NETO_MON,
                                     P_CANT_DECIMALES_US);
        IF NVL(V_DETA_IMP_NETO_LOC, 0) < 0 OR
           NVL(V_DETA_IMP_NETO_MON, 0) < 0 THEN
          RAISE_APPLICATION_ERROR(-20010,
                                  'No se puede registrar importes negativos, Verifique bien los movimientos que tuvo el articulo :' ||
                                  I.DETA_ART || '  ' || I.ART_DESC || ' !.');
        END IF;

        BEGIN
          INSERT INTO STK_DOCUMENTO_DET_PRD
            (DETA_CLAVE_DOC,
             DETA_NRO_ITEM,
             DETA_ART,
             DETA_EMPR,
             DETA_CANT,
             DETA_IMP_NETO_LOC,
             DETA_IMP_NETO_MON,
             DETA_IMPU,
             DETA_ART_ENT,
             DETA_CLAVE_ENT_PROD,
       DETA_NRO_FORMULA)
          VALUES
            (V_P_W_CLAVE_HIJO,
             I.DETA_NRO_ITEM,
             I.DETA_ART,
             P_EMPRESA,
             I.DETA_CANT,
             V_DETA_IMP_NETO_LOC,
             V_DETA_IMP_NETO_MON,
             'N',
             I.DETA_ART_ENT,
             I.DETA_CLAVE_ENT_PROD,
       I.S_FORM_NRO);
        EXCEPTION
          WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR(-20010, SQLERRM);
        END;
      END LOOP;

    END;

    V_MENSAJE := 'Guardado correctamente numero :' || P_DOCU_NRO_DOC ||
                 '</br>' ||
                 '<a href="javascript:$s(''P8004_W_CLAVE_HIJO'',' ||
                 V_P_W_CLAVE_HIJO || ');$s(''P8004_CLAVE_IMPRIMIR'',' ||
                 V_P_W_CLAVE_PADRE || ');">imprimir</a> ';

    APEX_APPLICATION.G_PRINT_SUCCESS_MESSAGE := V_MENSAJE;

  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20020, SQLERRM);
  END;

    PROCEDURE VALIDAR_CABECERA(I_COMPROBANTE  IN NUMBER,
                               I_FECHA      IN DATE,
                               I_DEPOSITO   IN NUMBER,
                               I_MONEDA     IN NUMBER)IS
    BEGIN

        IF I_COMPROBANTE IS NULL THEN
            RAISE_APPLICATION_ERROR(-20010, 'El comprobante no debe ser nulo!');
        END IF;

        IF I_FECHA IS NULL THEN
            RAISE_APPLICATION_ERROR(-20010, 'La fecha no debe ser nula!');
        END IF;

        IF I_DEPOSITO IS NULL THEN
            RAISE_APPLICATION_ERROR(-20010, 'El deposito no debe ser nulo!');
        END IF;

        IF I_MONEDA IS NULL THEN
            RAISE_APPLICATION_ERROR(-20010, 'La moneda no debe ser nula!');
        END IF;

    END;
---------------------------------------------------------------------------------

PROCEDURE PP_BUSCAR_COMPONENTES2 (I_DETA_ART      IN NUMBER,
                  I_S_FORM_NRO      IN NUMBER,
                  I_DETA_CANT     IN NUMBER,
                                  I_FEC_EMIS        IN DATE,
                                  I_W_PERI_COD      IN NUMBER,
                                  O_P_IMPORTE_MON   OUT NUMBER,
                                  O_P_IMPORTE_LOC   OUT NUMBER) IS
  V_EST STK_ARTICULO.ART_EST%TYPE;
  l_count NUMBER;
  --variables para coleccion
  V_S_ART_ENT       VARCHAR2(200);
  V_DETA_ART        VARCHAR2(200);
  V_S_ART_UM        VARCHAR2(10);
  V_ART_DESC        VARCHAR2(200);
  V_DETA_NRO_ITEM     NUMBER := 0;
  V_W_COSTO_MON         NUMBER := 0;
  V_W_COSTO_LOC         NUMBER := 0;
  V_W_MON               NUMBER := 0;
  V_DETA_IMP_NETO_MON   NUMBER := 0;
  V_DETA_IMP_NETO_LOC   NUMBER := 0;
  V_P_IMPORTE_MON       NUMBER := 0;
  V_P_IMPORTE_LOC       NUMBER := 0;
  V_DETA_CANT           NUMBER;
  V_AUXI_CANT           NUMBER;

  CURSOR C_COMP IS
    SELECT  ART_COD_ALFANUMERICO,
      FORM_ART_COMPONENTE,
      FORM_CANT,
      ART_UNID_MED,
      ART_DESC,
      ART_EST,
      FORM_NRO
      FROM  PRD_FORMULA,
      STK_ARTICULO
     WHERE FORM_ART_COMPONENTE = ART_CODIGO
       AND FORM_EMPR = ART_EMPR
       AND FORM_EMPR = NV('P_EMPRESA')
       AND FORM_ART = I_DETA_ART
       and form_nro= I_S_FORM_NRO
  ORDER BY FORM_ART_COMPONENTE;
  ITEMNRO NUMBER(4);
  V_CANT NUMBER;
  V_CANT_PADRE NUMBER := I_DETA_CANT;
  V_FORM_NRO NUMBER;
BEGIN

  l_count :=  APEX_COLLECTION.COLLECTION_MEMBER_COUNT(P_collection_name => 'PRDI016_ARTICULO_SALIDAS');

    OPEN C_COMP;
    LOOP
      FETCH C_COMP INTO
        V_S_ART_ENT,  --BENT.S_ART_ENT
    V_DETA_ART,   --:BENT.DETA_ART
    V_CANT,     --
    V_S_ART_UM,   --:BENT.S_ART_UM
    V_ART_DESC,   --:BENT.ART_DESC
    V_EST,
    V_FORM_NRO;   --
      EXIT WHEN C_COMP%NOTFOUND;

        IF V_EST = 'I' THEN
        RAISE_APPLICATION_ERROR(-20010, 'Articulo '||V_ART_DESC||' inactivo!.');
    END IF;

        ---------------------------------obtener costos---------------------------------

        PP_BUSCAR_COSTO_UNIT(Articulo => V_DETA_ART,
                             CostoMon => V_W_COSTO_MON,
                             CostoLoc => V_W_COSTO_LOC,
                             fecha_inic => I_FEC_EMIS,
                             W_PERI_CODIGO => I_W_PERI_COD);

        V_DETA_CANT := V_CANT * V_CANT_PADRE;

        V_DETA_IMP_NETO_LOC := ROUND(V_DETA_CANT * V_W_COSTO_LOC, 0);
        V_DETA_IMP_NETO_MON := ROUND(V_DETA_CANT * V_W_COSTO_MON, 2);

        V_P_IMPORTE_MON := V_P_IMPORTE_MON + V_DETA_IMP_NETO_MON;
        V_P_IMPORTE_LOC := V_P_IMPORTE_LOC + V_DETA_IMP_NETO_LOC;
        ---------------------------------------------------------------------------------
        ---------------------------calcular cantidad auxiliar------------------------------
        V_AUXI_CANT := FP_VERIF_UM (UNIDAD_MEDIDA => V_S_ART_UM,
                                    CANTIDAD      => V_DETA_CANT);
        -----------------------------------------------------------------------------------

    APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME  =>  'PRDI016_ARTICULO_ENTRADAS',
                   P_C001       =>  V_S_ART_ENT,
                   P_C002       =>  V_DETA_ART,
                   P_C003       =>  V_S_ART_UM,
                   P_C004       =>  V_ART_DESC,
                   P_C005       =>  V_FORM_NRO, --S_FORM_NRO
                   P_C006       =>  V_DETA_NRO_ITEM + 1, --DETA_NRO_ITEM
                   P_C007       =>  l_count,  --W_SECUENCIA
                   P_C008       =>  V_DETA_CANT,  --DETA_CANT
                   P_C009       =>  V_CANT, --CANT_FORM
                   P_C010       =>  l_count,  --W_DETA_NRO_ITEM_SAL
                   P_C011       =>  I_DETA_ART,     --DETA_ART_ENT
                                   P_C012               =>  V_DETA_IMP_NETO_LOC,
                                   P_C013               =>  V_DETA_IMP_NETO_MON,
                                   P_C014               =>  V_W_COSTO_MON,
                                   P_C015               =>  V_W_COSTO_LOC,
                                   P_C016               =>  V_W_MON,
                                   P_C017               =>  NULL, --DETA_NRO_ENT_PROD
                                   P_C018               =>  V_AUXI_CANT); --AUXI_CANT

        V_DETA_NRO_ITEM := V_DETA_NRO_ITEM + 1;
     END LOOP;
    CLOSE C_COMP;

   O_P_IMPORTE_MON := V_P_IMPORTE_MON;
   O_P_IMPORTE_LOC := V_P_IMPORTE_LOC;

END;

---------------------------------------------------------------------------------

FUNCTION FP_VERIF_UM (UNIDAD_MEDIDA IN VARCHAR2,
                      CANTIDAD      IN NUMBER)
RETURN NUMBER IS
  V_CANTIDAD NUMBER;
  V_UM VARCHAR2(10);
BEGIN
  --V_UM := UPPER(:BENT.S_ART_UM);
    V_UM := UPPER(UNIDAD_MEDIDA);
  IF V_UM IS NOT NULL AND V_UM <> 'UN' THEN
    --V_CANTIDAD := :BENT.DETA_CANT;
    V_CANTIDAD := CANTIDAD;
  ELSE
    V_CANTIDAD := 0;
  END IF;

RETURN V_CANTIDAD;
END;
--------------------------------------------------------------------------------

PROCEDURE PP_RECALCULAR_IMPORTES IS

V_ART_ENT NUMBER:=0;
V_AUX_REG NUMBER:=0;
V_IMPORTE_MON NUMBER:=0;
V_IMPORTE_LOC NUMBER:=0;
V_CANT_PADRE NUMBER :=NULL;

BEGIN

  FOR C IN (SELECT SEQ_ID NRO_ITEM,
                      C001  DETA_NRO_ITEM,
                      C002  ARC_COD_ALFANUMERICO,
                      C003  DETA_CANT,
                      C004  UM,
                      C005  DETA_ART,
                      C006  ART_DESC,
                      C007  DETA_IMP_NETO_LOC,
                      C008  DETA_IMP_NETO_MON,
                      C009  DOCU_CLAVE,
                      C014  S_FORM_NRO
    FROM APEX_COLLECTIONS
    WHERE COLLECTION_NAME = 'PRDI016_ARTICULO_SALIDAS')
  LOOP

  V_ART_ENT := C.DETA_NRO_ITEM;
  V_CANT_PADRE := C.DETA_CANT;
                FOR D IN (SELECT  SEQ_ID  SEQ_ID,
                                C001    S_ART_ENT,
                                C002  DETA_ART,
                                C003    S_ART_UM,
                                C004    ART_DESC,
                                C005    S_FORM_NRO,
                                C006    DETA_NRO_ITEM,
                                C007  W_SECUENCIA,
                                C008  DETA_CANT,
                                C009  CANT_FORM,
                                C010  W_DETA_NRO_ITEM_SAL,
                                C011  DETA_ART_ENT,
                                C012    DETA_IMP_NETO_LOC,
                                C013    DETA_IMP_NETO_MON,
                                C014    W_COSTO_MON,
                                C015    W_COSTO_LOC,
                                C016    W_MON,
                                C017    DETA_NRO_ENT_PROD,
                                C018    AUXI_CANT
                               -- C019    DETA_CLAVE_ENT_PROD
                                FROM    APEX_COLLECTIONS
                                WHERE   COLLECTION_NAME = 'PRDI016_ARTICULO_ENTRADAS')
        LOOP
              IF D.W_DETA_NRO_ITEM_SAL = V_ART_ENT THEN --:BENT.DETA_ART_ENT = V_ART_ENT THEN
                  D.DETA_CANT := TO_NUMBER(D.CANT_FORM) * V_CANT_PADRE;
                V_IMPORTE_MON := V_IMPORTE_MON + TO_NUMBER(D.DETA_IMP_NETO_MON);
                V_IMPORTE_LOC := V_IMPORTE_LOC + TO_NUMBER(D.DETA_IMP_NETO_LOC);
              END IF;

                        APEX_COLLECTION.UPDATE_MEMBER_ATTRIBUTE(P_COLLECTION_NAME => 'PRDI016_ARTICULO_ENTRADAS',
                                              P_SEQ             => D.SEQ_ID,
                                              P_ATTR_NUMBER     => 8,
                                              P_ATTR_VALUE      => D.DETA_CANT);

        END LOOP;


    APEX_COLLECTION.UPDATE_MEMBER_ATTRIBUTE(P_COLLECTION_NAME => 'PRDI016_ARTICULO_SALIDAS',
                                              P_SEQ             => C.NRO_ITEM,
                                              P_ATTR_NUMBER     => 8,
                                              P_ATTR_VALUE      => V_IMPORTE_MON);

    APEX_COLLECTION.UPDATE_MEMBER_ATTRIBUTE(P_COLLECTION_NAME => 'PRDI016_ARTICULO_SALIDAS',
                                              P_SEQ             => C.NRO_ITEM,
                                              P_ATTR_NUMBER     => 7,
                                              P_ATTR_VALUE      => V_IMPORTE_LOC);

    V_IMPORTE_MON := 0;
    V_IMPORTE_LOC := 0;

  END LOOP;
END;


PROCEDURE PP_MONTOS_COLEC (I_FEC_EMIS   IN DATE,
                           I_W_PERI_COD IN NUMBER,
                           O_P_IMPORTE_MON  OUT NUMBER,
                           o_P_IMPORTE_LOC OUT NUMBER) IS

V_W_COSTO_MON   NUMBER;
V_W_COSTO_LOC   NUMBER;
V_DETA_IMP_NETO_LOC NUMBER;
V_DETA_IMP_NETO_MON NUMBER;
V_P_IMPORTE_MON NUMBER;
V_P_IMPORTE_LOC NUMBER;

BEGIN
    FOR C IN (SELECT  SEQ_ID  SEQ_ID,
                        C001    S_ART_ENT,
                        C002  DETA_ART,
                        C003    S_ART_UM,
                        C004    ART_DESC,
                        C005    S_FORM_NRO,
                        C006    DETA_NRO_ITEM,
                        C007  W_SECUENCIA,
                        C008  DETA_CANT,
                        C009  CANT_FORM,
                        C010  W_DETA_NRO_ITEM_SAL,
                        C011  DETA_ART_ENT,
                        C012    DETA_IMP_NETO_LOC,
                        C013    DETA_IMP_NETO_MON,
                        C014    W_COSTO_MON,
                        C015    W_COSTO_LOC,
                        C016    W_MON,
                        C017    DETA_NRO_ENT_PROD,
                        C018    AUXI_CANT
                       -- C019    DETA_CLAVE_ENT_PROD
                        FROM    APEX_COLLECTIONS
                        WHERE   COLLECTION_NAME = 'PRDI016_ARTICULO_ENTRADAS')
            LOOP
                PP_BUSCAR_COSTO_UNIT(Articulo => C.DETA_ART,
                             CostoMon => V_W_COSTO_MON,
                             CostoLoc => V_W_COSTO_LOC,
                             fecha_inic => I_FEC_EMIS,
                             W_PERI_CODIGO => I_W_PERI_COD);

                V_DETA_IMP_NETO_LOC := ROUND(TO_NUMBER(C.DETA_CANT) * V_W_COSTO_LOC, 0);
                V_DETA_IMP_NETO_MON := ROUND(TO_NUMBER(C.DETA_CANT) * V_W_COSTO_MON, 2);

                V_P_IMPORTE_MON := V_P_IMPORTE_MON + V_DETA_IMP_NETO_MON;
                V_P_IMPORTE_LOC := V_P_IMPORTE_LOC + V_DETA_IMP_NETO_LOC;
                ---------------------------------------------------------------------------------

                APEX_COLLECTION.UPDATE_MEMBER (P_COLLECTION_NAME => 'PRDI016_ARTICULO_ENTRADAS',
                                                P_SEQ   => C.SEQ_ID,
                                                P_C001  => C.S_ART_ENT,
                                                P_C002  => C.DETA_ART,
                                                P_C003  => C.S_ART_UM,
                                                P_C004  => C.ART_DESC,
                                                P_C005  => C.S_FORM_NRO,
                                                P_C006  => C.DETA_NRO_ITEM,
                                                P_C007  => C.W_SECUENCIA,
                                                P_C008  => C.DETA_CANT,
                                                P_C009  => C.CANT_FORM,
                                                P_C010  => C.W_DETA_NRO_ITEM_SAL,
                                                P_C011  => C.DETA_ART_ENT,
                                                P_C012  => V_DETA_IMP_NETO_LOC,
                                                P_C013  => V_DETA_IMP_NETO_MON,
                                                P_C014  => V_W_COSTO_MON,
                                                P_C015  => V_W_COSTO_LOC,
                                                P_C016  => C.W_MON,
                                                P_C017  => C.DETA_NRO_ENT_PROD,
                                                P_C018  => C.AUXI_CANT);
            END LOOP;

            O_P_IMPORTE_MON := V_P_IMPORTE_MON;
            o_P_IMPORTE_LOC := V_P_IMPORTE_LOC;
END;

END PRDI016;
/
