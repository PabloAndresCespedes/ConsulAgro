create or replace trigger stk_remision_manual_biu
    before insert or update  
    on stk_remision_manual 
    for each row 
begin 
    if :new.id is null then 
        :new.id := stk_remision_manual_seq.nextval; 
    end if; 
    if inserting then 
        :new.created := sysdate; 
        :new.created_by := nvl(sys_context('APEX$SESSION','APP_USER'),user); 
    end if; 
    :new.updated := sysdate; 
    :new.updated_by := nvl(sys_context('APEX$SESSION','APP_USER'),user); 
end stk_remision_manual_biu; 
/
