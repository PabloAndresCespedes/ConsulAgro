CREATE OR REPLACE PACKAGE STKI004_ALIMENTEC AS

  -- AUTHOR  : INTHEGRA
  -- CREATED : 20/07/2021 22:24:37
  /*
    Author  : @PabloACespedes \(^-^)/
    Modified : 25/08/2022 8:08:57
    COPIA PARA ALIMENTEC, PAQUETE AISLADO
  */
  co_pck_name constant varchar2(17 char) := 'STKI004_ALIMENTEC';
  
  PROCEDURE PP_INICIAR_COLLECTION;

  PROCEDURE PP_BORRAR_DET(I_SEQ IN NUMBER);

  PROCEDURE PP_ADD_ITEM(I_EMPRESA  IN NUMBER,
                        I_SEQ      NUMBER,
                        I_ARTICULO IN NUMBER,
                        I_CANT     IN NUMBER,
                        I_PESO     IN NUMBER);

  PROCEDURE PP_VAL_CAB(I_EMPRESA          IN NUMBER,
                       I_SUCURSAL         IN NUMBER,
                       I_DESC_SUC         IN NUMBER,
                       I_DESC_DEP         IN NUMBER,
                       I_NRO_COMP         IN NUMBER,
                       I_TIPO_TRASLADO    IN VARCHAR2,
                       I_KM_RECORRIDO     IN NUMBER,
                       I_FEC_FIN_TRASLADO IN DATE,
                       I_RCONDUCT_NOMB    IN VARCHAR2,
                       I_RCONDUCT_CI      IN VARCHAR2,
                       I_RCONDUCT_DIR     IN VARCHAR2);

  PROCEDURE PP_ACTUALIZAR_REGISTRO(I_EMPRESA           IN NUMBER,
                                   I_SUCURSAL          IN NUMBER,
                                   I_SUC_ORIG          IN NUMBER,
                                   I_SUC_DEP           IN NUMBER,
                                   I_NRO_COMP          IN NUMBER,
                                   I_FEC_EMIS          IN DATE,
                                   I_DOCU_MON          IN NUMBER,
                                   I_DOCU_CLI          IN NUMBER,
                                   I_CLI_NOMB          IN VARCHAR2,
                                   I_TASA_US           IN NUMBER,
                                   I_DOCU_SERIE        IN VARCHAR2,
                                   I_RCONDUCT_COD      IN NUMBER,
                                   I_RTRANSP_COD       IN NUMBER,
                                   I_RTRANSP_NOMB      IN VARCHAR2,
                                   I_RTRANSP_RUC       IN VARCHAR2,
                                   I_RMAR_VEH_COD      IN NUMBER,
                                   I_RMAR_VEH_DESC     IN VARCHAR2,
                                   I_RCONDUCT_NOMB     IN VARCHAR2,
                                   I_RCONDUCT_CI       IN VARCHAR2,
                                   I_RCONDUCT_DIR      IN VARCHAR2,
                                   I_CAMION            IN NUMBER,
                                   I_IND_ORD_CARGA     IN VARCHAR2,
                                   I_OCARGA_LONDON     IN NUMBER,
                                   I_DOCU_TIMBRADO     IN NUMBER,
                                   I_TIPO_TRASLADO     IN VARCHAR2,
                                   I_FEC_FIN_TRANSLADO IN DATE,
                                   I_KM_RECORRIDO      IN NUMBER,
                                   I_VEH_CHAPA         IN VARCHAR2,
                                   I_CLAVE_FAC_REM     IN NUMBER,
                                   I_TIPO_MOV          IN NUMBER);

  PROCEDURE PP_IMPRIMIR_DOC(I_EMPRESA IN NUMBER, I_DOCUT_CLAVE IN NUMBER);

  FUNCTION VALIDAR_ART(I_EMPRESA  NUMBER,
                       I_ARTICULO NUMBER,
                       I_DESC_SUC IN NUMBER,
                       I_DESC_DEP IN NUMBER) RETURN BOOLEAN;

  -- 25/08/2022 14:09:50 @PabloACespedes \(^-^)/
  -- se_crea otros metodos de registro de remision unicamente para 
  -- alimentec
  procedure add_upd_remision(
    in_page_id in varchar2,
    out_nro    out number
  );
  
  -- 29/08/2022 9:50:18 @PabloACespedes \(^-^)/
  -- agrega el detalle de la remision
  -- devuelve nulo en caso que sea success full 
  /*
    Errores configuracion para @out_error
    NO_ART >> Articulo no encontrado
    NO_CANT >> Sin Cantidad mayor a 0
    NO_DESC >> Sin Descripcion de articulo
    NO_UM >> sin unidad de medida del articulo
  */
  procedure add_det_remision(
    in_empresa    in  stk_remision_manual_det.empresa_id%type,
    in_remision   in  stk_remision_manual_det.remision_id%type,
    in_articulo   in  stk_remision_manual_det.articulo_id%type,
    in_cantidad   in  stk_remision_manual_det.cantidad%type,
    in_um         in  stk_remision_manual_det.unid_med%type,
    in_desc_art   in  stk_remision_manual_det.descripcion%type, 
    out_error     out varchar2
  );
    
  -- 29/08/2022 10:26:47 @PabloACespedes \(^-^)/
  -- eliminar remision
  procedure del_remision(
    in_remision in stk_remision_manual.id%type
  );
  
  
END STKI004_ALIMENTEC;
/
CREATE OR REPLACE PACKAGE BODY STKI004_ALIMENTEC AS

  PROCEDURE PP_INICIAR_COLLECTION AS
  BEGIN
    APEX_COLLECTION.CREATE_OR_TRUNCATE_COLLECTION(P_COLLECTION_NAME => 'STKI004_DETALLE');
  END PP_INICIAR_COLLECTION;

  PROCEDURE PP_BORRAR_DET(I_SEQ IN NUMBER) AS
  BEGIN
    APEX_COLLECTION.DELETE_MEMBER(P_COLLECTION_NAME => 'STKI004_DETALLE',
                                  P_SEQ             => I_SEQ);

    APEX_COLLECTION.RESEQUENCE_COLLECTION(P_COLLECTION_NAME => 'STKI004_DETALLE');

  END PP_BORRAR_DET;

  PROCEDURE PP_ADD_ITEM(I_EMPRESA  IN NUMBER,
                        I_SEQ      NUMBER,
                        I_ARTICULO IN NUMBER,
                        I_CANT     IN NUMBER,
                        I_PESO     IN NUMBER) AS
  BEGIN
    NULL;
    IF I_SEQ IS NULL THEN

      APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'STKI004_DETALLE',
                                 P_N001            => I_ARTICULO,
                                 P_N002            => I_CANT,
                                 P_N003            => I_PESO);
    END IF;

  END PP_ADD_ITEM;

  FUNCTION VALIDAR_ART(I_EMPRESA  NUMBER,
                       I_ARTICULO NUMBER,
                       I_DESC_SUC IN NUMBER,
                       I_DESC_DEP IN NUMBER) RETURN BOOLEAN AS
    COD_ART NUMBER;
  BEGIN

    SELECT ART_CODIGO
      INTO COD_ART
      FROM STK_ARTICULO, STK_ARTICULO_EMPRESA
     WHERE ART_CODIGO = AREM_ART
       AND ART_EMPR = AREM_EMPR
       AND AREM_EMPR = I_EMPRESA
       AND ART_EST = 'A'
       AND ART_TIPO <> 2
       AND STK_ARTICULO.ART_EMPR = STK_ARTICULO_EMPRESA.AREM_EMPR
       AND ART_CODIGO = I_ARTICULO
     ORDER BY 1;

    RETURN TRUE;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN FALSE;
  END;

  PROCEDURE PP_VAL_CAB(I_EMPRESA          IN NUMBER,
                       I_SUCURSAL         IN NUMBER,
                       I_DESC_SUC         IN NUMBER,
                       I_DESC_DEP         IN NUMBER,
                       I_NRO_COMP         IN NUMBER,
                       I_TIPO_TRASLADO    IN VARCHAR2,
                       I_KM_RECORRIDO     IN NUMBER,
                       I_FEC_FIN_TRASLADO IN DATE,
                       I_RCONDUCT_NOMB    IN VARCHAR2,
                       I_RCONDUCT_CI      IN VARCHAR2,
                       I_RCONDUCT_DIR     IN VARCHAR2) AS
    V_CLAVE NUMBER;
  BEGIN

    IF I_DESC_SUC IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010, 'Sucursal no puede ser nulo.');
    END IF;

    IF I_DESC_DEP IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010, 'Deposito no puede ser nulo.');
    END IF;

    IF I_NRO_COMP IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'El n?mero de comprobante no puede ser nulo.');
    END IF;

    IF I_TIPO_TRASLADO IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'El tipo de traslado no puede quedar en blanco!');
    END IF;

    IF I_KM_RECORRIDO IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'El elemento KM recorridos no puede quedar en blanco!');
    END IF;

    IF I_TIPO_TRASLADO NOT IN ('I', 'R') THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'Los tipos de traslados deben ser I: Interno o R: Remisi?n');
    END IF;

    IF I_FEC_FIN_TRASLADO IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'La fecha de fin de traslado no puede quedar en blanco!');
    END IF;

    IF I_RCONDUCT_NOMB IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'El nombre del conductor no puede quedar en blanco!');
    END IF;

    IF I_RCONDUCT_CI IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'La C.I del conductor no puede quedar en blanco!');
    END IF;

    IF I_RCONDUCT_DIR IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'La direcci?n del conductor no puede quedar en blanco!');
    END IF;

    --  VERIFICA SI EL USUARIO ESTA HABILITADO PARA REALIZAR LA OPERACION DE EXTRACCION DE UN DEPOSITO
    FACI039.PL_CONTROL_OPER_DEP(I_EMPRESA   => I_EMPRESA,
                                I_SUCURSAL  => I_DESC_SUC,
                                I_DEPOSITO  => I_DESC_DEP,
                                I_OPERACION => 'EXT',
                                P_USUARIO   => GEN_DEVUELVE_USER,
                                I_CLAVE     => V_CLAVE);

    --VALIDAR SI ESTA DESHABILITADO EL PERIODO ACTUAL DE STOCK
    FACI039.PL_VALIDAR_HAB_MES_STK(P_FECHA   => TRUNC(SYSDATE),
                                   P_EMPRESA => I_EMPRESA,
                                   P_USUARIO => GEN_DEVUELVE_USER);

  END PP_VAL_CAB;

  PROCEDURE PP_ACTUALIZAR_REGISTRO(I_EMPRESA           IN NUMBER,
                                   I_SUCURSAL          IN NUMBER,
                                   I_SUC_ORIG          IN NUMBER,
                                   I_SUC_DEP           IN NUMBER,
                                   I_NRO_COMP          IN NUMBER,
                                   I_FEC_EMIS          IN DATE,
                                   I_DOCU_MON          IN NUMBER,
                                   I_DOCU_CLI          IN NUMBER,
                                   I_CLI_NOMB          IN VARCHAR2,
                                   I_TASA_US           IN NUMBER,
                                   I_DOCU_SERIE        IN VARCHAR2,
                                   I_RCONDUCT_COD      IN NUMBER,
                                   I_RTRANSP_COD       IN NUMBER,
                                   I_RTRANSP_NOMB      IN VARCHAR2,
                                   I_RTRANSP_RUC       IN VARCHAR2,
                                   I_RMAR_VEH_COD      IN NUMBER,
                                   I_RMAR_VEH_DESC     IN VARCHAR2,
                                   I_RCONDUCT_NOMB     IN VARCHAR2,
                                   I_RCONDUCT_CI       IN VARCHAR2,
                                   I_RCONDUCT_DIR      IN VARCHAR2,
                                   I_CAMION            IN NUMBER,
                                   I_IND_ORD_CARGA     IN VARCHAR2,
                                   I_OCARGA_LONDON     IN NUMBER,
                                   I_DOCU_TIMBRADO     IN NUMBER,
                                   I_TIPO_TRASLADO     IN VARCHAR2,
                                   I_FEC_FIN_TRANSLADO IN DATE,
                                   I_KM_RECORRIDO      IN NUMBER,
                                   I_VEH_CHAPA         IN VARCHAR2,
                                   I_CLAVE_FAC_REM     IN NUMBER,
                                   I_TIPO_MOV          IN NUMBER) AS

    V_CLAVE_AUT_ESP_EXT NUMBER;
    V_CLAVE_AUT_ESP_DEP NUMBER;
    V_COD_OPER          NUMBER;
    V_DOCUT_CLAVE       NUMBER;
    V_MENSAJE           VARCHAR2(1000);
    V_NRO_DOCUMENTO     NUMBER;
  BEGIN

    IF APEX_COLLECTION.COLLECTION_MEMBER_COUNT(P_COLLECTION_NAME => 'STKI004_DETALLE') > 999 THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'La cantidad m?xima de art?culos a transferir es: 999');
    END IF;

    SELECT MAX(NRO) + 1
      INTO V_NRO_DOCUMENTO
      FROM (SELECT DOCU_NRO_DOC NRO
              FROM STK_DOCUMENTO
             WHERE DOCU_TIPO_TRASLADO IS NULL
               AND DOCU_CODIGO_OPER IN (11)
               AND DOCU_SUC_ORIG = I_SUC_ORIG
               AND DOCU_EMPR = I_EMPRESA
            UNION ALL
            SELECT DOCUT_NRO_DOC NRO
              FROM STK_DOCUMENTO_TEMP
             WHERE DOCUT_EMPR = I_EMPRESA
             );

    FACI039.PL_CONTROL_OPER_DEP(I_EMPRESA   => I_EMPRESA,
                                I_SUCURSAL  => I_SUC_ORIG,
                                I_DEPOSITO  => I_SUC_DEP,
                                I_OPERACION => 'EXT',
                                P_USUARIO   => GEN_DEVUELVE_USER,
                                I_CLAVE     => V_CLAVE_AUT_ESP_EXT);

    SELECT OPER_CODIGO
      INTO V_COD_OPER
      FROM STK_OPERACION
     WHERE OPER_DESC = 'REMISION VENTA'
       AND OPER_EMPR = I_EMPRESA;

    V_DOCUT_CLAVE := STK_SEQ_DOCU_NEXTVAL;

    --INSERTO EN STK_DOCUMENTO
    INSERT INTO STK_DOCUMENTO
      (DOCU_CLAVE,
       DOCU_EMPR,
       DOCU_CODIGO_OPER,
       DOCU_NRO_DOC,
       DOCU_SUC_ORIG,
       DOCU_DEP_ORIG,
       DOCU_MON,
       DOCU_CLI,
       DOCU_CLI_NOM,
       DOCU_FEC_EMIS,
       DOCU_TASA_US,
       DOCU_LOGIN,
       DOCU_FEC_GRAB,
       DOCU_SIST,
       DOCU_SERIE,
       DOCU_RCONDUCT_COD,
       DOCU_RTRANSP_COD,
       DOCU_RMAR_VEH_COD,
       DOCU_RCONDUCT_NOMB,
       DOCU_RCONDUCT_CI,
       DOCU_RCONDUCT_DIR,
       DOCU_CAMION,
       DOCU_IND_ORD_CARGA,
       DOCU_OCARGA_LONDON,
       DOCU_TIMBRADO,
       DOCU_TIPO_TRASLADO,
       DOCU_FEC_FIN_TRANSLADO,
       DOCU_KM_RECORRIDO,
       DOCU_VEH_CHAPA,
       DOCU_VEH_MARCA,
       DOCU_CLAVE_FAC_REM,
       DOCU_TIPO_MOV)
    VALUES
      (V_DOCUT_CLAVE,
       I_EMPRESA,
       V_COD_OPER,
       I_NRO_COMP,
       I_SUC_ORIG,
       I_SUC_DEP,
       I_DOCU_MON,
       I_DOCU_CLI,
       I_CLI_NOMB,
       I_FEC_EMIS,
       I_TASA_US,
       GEN_DEVUELVE_USER,
       SYSDATE,
       'STK',
       I_DOCU_SERIE,
       I_RCONDUCT_COD,
       I_RTRANSP_COD,
       I_RMAR_VEH_COD,
       I_RCONDUCT_NOMB,
       I_RCONDUCT_CI,
       I_RCONDUCT_DIR,
       I_CAMION,
       I_IND_ORD_CARGA,
       I_OCARGA_LONDON,
       I_DOCU_TIMBRADO,
       I_TIPO_TRASLADO,
       I_FEC_FIN_TRANSLADO,
       I_KM_RECORRIDO,
       I_VEH_CHAPA,
       I_RMAR_VEH_DESC,
       I_CLAVE_FAC_REM,
       I_TIPO_MOV);

    FOR C IN (SELECT A.SEQ_ID NRO_ITEM,
                     A.N001   DETA_ART,
                     A.N002   DETA_CANT,
                     A.N003   DETA_PESO
                FROM APEX_COLLECTIONS A
               WHERE A.COLLECTION_NAME = 'STKI004_DETALLE'
              ) LOOP

      INSERT INTO STK_DOCUMENTO_DET
        (DETA_CLAVE_DOC,
         DETA_NRO_ITEM,
         DETA_ART,
         DETA_CANT,
         DETA_EMPR,
         DETA_PESO_BRUTO)
      VALUES
        (V_DOCUT_CLAVE,
         C.NRO_ITEM,
         C.DETA_ART,
         C.DETA_CANT,
         I_EMPRESA,
         C.DETA_PESO);
    END LOOP;

    UPDATE GEN_IMPRESORA
       SET IMP_ULT_NRO_REMISION = I_NRO_COMP
     WHERE IMPR_IP = FP_IP_USER --IMP_CODIGO = I_IMPRESORA
       AND IMP_EMPR = I_EMPRESA;

    V_MENSAJE := 'Guardado correctamente n?mero :' || V_NRO_DOCUMENTO ||
                 '</br>' ||
                 '<a href="javascript:$s(''P8001_CLAVE_IMPRIMIR'',' ||
                 V_DOCUT_CLAVE || ');">imprimir</a> ';

    APEX_APPLICATION.G_PRINT_SUCCESS_MESSAGE := V_MENSAJE;

  END PP_ACTUALIZAR_REGISTRO;

  PROCEDURE PP_IMPRIMIR_DOC(I_EMPRESA IN NUMBER, I_DOCUT_CLAVE IN NUMBER) AS
    V_PARAMETROS    VARCHAR2(32767);
    V_IDENTIFICADOR VARCHAR2(2) := '&'; --SE UTILIZA PARA QUE AL COMPILAR NO LO TOME COMO PARAMETROS
    V_REPORT        VARCHAR2(30) := 'STKI003(AUTOI)';

  BEGIN

    V_PARAMETROS := 'P_FORMATO=' || APEX_UTIL.URL_ENCODE('pdf');

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_EMPRESA=' ||
                    APEX_UTIL.URL_ENCODE(I_EMPRESA);

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_CLAVE=' ||
                    APEX_UTIL.URL_ENCODE(I_DOCUT_CLAVE);

    DELETE FROM GEN_PARAMETROS_REPORT WHERE USUARIO = GEN_DEVUELVE_USER;

    INSERT INTO GEN_PARAMETROS_REPORT
      (PARAMETROS, USUARIO, NOMBRE_REPORTE, FORMATO_SALIDA)
    VALUES
      (V_PARAMETROS, GEN_DEVUELVE_USER, V_REPORT, 'pdf');

  END PP_IMPRIMIR_DOC;
  
  procedure add_upd_remision(
    in_page_id in varchar2,
    out_nro    out number
  )as
     l_empresa_id      stk_remision_manual.empresa_id%type;
     l_nro             stk_remision_manual.nro%type;
     l_fecha_emision   stk_remision_manual.fecha_emision%type;
     l_suc             stk_remision_manual.suc%type;
     l_dest_raz_soc    stk_remision_manual.dest_raz_soc%type;
     l_dest_ruc_ci     stk_remision_manual.dest_ruc_ci%type;
     l_dest_domicilio  stk_remision_manual.dest_domicilio%type;
     l_trasl_motivo    stk_remision_manual.trasl_motivo%type;
     l_trasl_comp      stk_remision_manual.trasl_comp%type;
     l_trasl_nro_comp  stk_remision_manual.trasl_nro_comp%type;
     l_trasl_nro_timb  stk_remision_manual.trasl_nro_timb%type;
     l_trasl_fecha_exp stk_remision_manual.trasl_fecha_exp%type;
     l_trasl_fecha_inicio stk_remision_manual.trasl_fecha_inicio%type;
     l_trasl_fecha_fin    stk_remision_manual.trasl_fecha_fin%type;
     l_trasl_dir_part     stk_remision_manual.trasl_dir_part%type;
     l_trasl_ciu_part     stk_remision_manual.trasl_ciu_part%type;
     l_trasl_dep_part     stk_remision_manual.trasl_dep_part%type;
     l_trasl_dir_lleg     stk_remision_manual.trasl_dir_lleg%type;
     l_trasl_ciu_lleg     stk_remision_manual.trasl_ciu_lleg%type;
     l_trasl_dep_lleg     stk_remision_manual.trasl_dep_lleg%type;
     l_trasl_km           stk_remision_manual.trasl_km%type;
     l_trasl_camb_fecha_term stk_remision_manual.trasl_camb_fecha_term%type;
     l_trasl_mot_camb_fecha  stk_remision_manual.trasl_mot_camb_fecha%type;
     l_veh_marca             stk_remision_manual.veh_marca%type;
     l_veh_nro_rua           stk_remision_manual.veh_nro_rua%type;
     l_veh_nro_rua_remolque  stk_remision_manual.veh_nro_rua_remolque%type;
     l_conduc_raz_soc        stk_remision_manual.conduc_raz_soc%type;
     l_conduc_ruc_ci         stk_remision_manual.conduc_ruc_ci%type;
     l_conduc_domicilio      stk_remision_manual.conduc_domicilio%type;
     
     l_new boolean;
  begin
    -- inicializando de pag APEX
    -- Si se_necesita reutilizar hay que respetar la convension de nombres
    -- o recuperar data de otra manera
    l_empresa_id            := ap.v('P_EMPRESA');
    l_nro                   := ap.v('P'||in_page_id||'_PR_NRO_REMISION');
    l_fecha_emision         := ap.v('P'||in_page_id||'_PR_FECHA_EMISION');
    l_suc                   := ap.v('P_SUCURSAL');
    
    l_dest_raz_soc          := ap.v('P'||in_page_id||'_DD_RAZ_SOC');
    l_dest_ruc_ci           := ap.v('P'||in_page_id||'_DD_RUC_CI');
    l_dest_domicilio        := ap.v('P'||in_page_id||'_DD_DOMICILIO');

    l_trasl_motivo          := ap.v('P'||in_page_id||'_DT_MOTIVO');
    l_trasl_comp            := ap.v('P'||in_page_id||'_DT_COMPROBANTE_VENTA');
    l_trasl_nro_comp        := ap.v('P'||in_page_id||'_DT_COMP_VENTA_NRO');
    l_trasl_nro_timb        := ap.v('P'||in_page_id||'_DT_NRO_TIMBRADO');
    l_trasl_fecha_exp       := ap.v('P'||in_page_id||'_DT_FECHA_EXP_DOC');
    l_trasl_fecha_inicio    := ap.v('P'||in_page_id||'_DT_FECHA_INICIO');
    l_trasl_fecha_fin       := ap.v('P'||in_page_id||'_DT_FECHA_TERMINO');
    l_trasl_dir_part        := ap.v('P'||in_page_id||'_DT_DIR_PUNTO_PARTIDA');
    l_trasl_ciu_part        := ap.v('P'||in_page_id||'_DT_PUNTO_PART_CIUDAD');
    l_trasl_dep_part        := ap.v('P'||in_page_id||'_DT_PUNTO_PART_DEP');
    l_trasl_dir_lleg        := ap.v('P'||in_page_id||'_DT_DIR_PUNTO_LLEGADA');
    l_trasl_ciu_lleg        := ap.v('P'||in_page_id||'_DT_DIR_PUN_LLEG_CIUDAD');
    l_trasl_dep_lleg        := ap.v('P'||in_page_id||'_DT_PUNTO_LLEG_DEP');
    l_trasl_km              := ap.v('P'||in_page_id||'_DT_KM_ESTIMADO');
    l_trasl_camb_fecha_term := ap.v('P'||in_page_id||'_DT_CAMB_FECHA_TERM');
    l_trasl_mot_camb_fecha  := ap.v('P'||in_page_id||'_DT_MOTIVO_CAMBIO_FECHA');

    l_veh_marca             := ap.v('P'||in_page_id||'_DV_MARCA');
    l_veh_nro_rua           := ap.v('P'||in_page_id||'_DV_RUA');
    l_veh_nro_rua_remolque  := ap.v('P'||in_page_id||'_DV_RUA_REMOLQUE');

    l_conduc_raz_soc        := ap.v('P'||in_page_id||'_DC_RAZON_SOCIAL');
    l_conduc_ruc_ci         := ap.v('P'||in_page_id||'_DC_RUC_CI');
    l_conduc_domicilio      := ap.v('P'||in_page_id||'_DC_DOMICILIO');
    
    
    -- Begin Validations
    -- principal
    if l_empresa_id is null then
        raise_application_error(-20000, 'Cierre sesion e inicie nuevamente (Empresa no valida)');
    end if;

    if l_fecha_emision is null then
        raise_application_error(-20000, 'Es necesario Completar la fecha de emision');
    end if;

    if l_suc is null then
        raise_application_error(-20000, 'Cierre sesion e inicie nuevamente (Sucursal no valida) ');
    end if;
    
    -- destino
    if l_dest_raz_soc is null then
        raise_application_error(-20000, 'Es necesario Completar La raz social destino');
    end if;

    if l_dest_ruc_ci is null then
        raise_application_error(-20000, 'Es necesario Completar RUC CI Destino');
    end if;

    if l_dest_domicilio is null then
        raise_application_error(-20000, 'Es necesario Completar Domicilio Destino');
    end if;
    
    -- vehiculo
    if l_veh_marca is null then
        raise_application_error(-20000, 'Es necesario Completar Marca del vehiculo');
    end if;

    if l_veh_nro_rua is null then
        raise_application_error(-20000, 'Es necesario Completar RUA del Vehiculo');
    end if;
    
    -- conductor
    if l_conduc_raz_soc is null then
        raise_application_error(-20000, 'Es necesario Completar Razon social del conductor');
    end if;

    if l_conduc_ruc_ci is null then
        raise_application_error(-20000, 'Es necesario Completar CI o RUC del conductor');
    end if;

    if l_conduc_domicilio is null then
        raise_application_error(-20000, 'Es necesario Completar Domicilio del conductor');
    end if;
    
    -- traslado
    if l_trasl_motivo is null then
        raise_application_error(-20000, 'Es necesario Completar el motivo traslado');
    end if;

    if l_trasl_comp is null then
        raise_application_error(-20000, 'Es necesario Completar el Comprobante traslado');
    end if;

    if l_trasl_nro_comp is null then
        raise_application_error(-20000, 'Es necesario Completar Nro Comprobante Traslado');
    end if;

    if l_trasl_nro_timb is null then
        raise_application_error(-20000, 'Es necesario Completar Nro Timbrado Traslado');
    end if;

    if l_trasl_fecha_exp is null then
        raise_application_error(-20000, 'Es necesario Completar Fecha Expedicion documento traslado');
    end if;

    if l_trasl_fecha_inicio is null then
        raise_application_error(-20000, 'Es necesario Completar Fecha Inicio Traslado');
    end if;

    if l_trasl_fecha_fin is null then
        raise_application_error(-20000, 'Es necesario Completar Fecha Termino Traslado');
    end if;

    if l_trasl_dir_part is null then
        raise_application_error(-20000, 'Es necesario Completar Direccion Partida Traslado');
    end if;

    if l_trasl_ciu_part is null then
        raise_application_error(-20000, 'Es necesario Completar Ciudad Partida Traslado');
    end if;

    if l_trasl_dep_part is null then
        raise_application_error(-20000, 'Es necesario Completar Departamento Partida Traslado');
    end if;

    if l_trasl_dir_lleg is null then
        raise_application_error(-20000, 'Es necesario Completar direccion llegada Traslado');
    end if;

    if l_trasl_ciu_lleg is null then
        raise_application_error(-20000, 'Es necesario Completar ciudad llegada Traslado');
    end if;

    if l_trasl_dep_lleg is null then
        raise_application_error(-20000, 'Es necesario Completar departamento llegada Traslado');
    end if;

    if l_trasl_km is null then
        raise_application_error(-20000, 'Es necesario Completar los KMs');
    end if;
    --> End validations
    -- numeracion
    if l_nro is null then
      l_new := true;
      
      begin
        SELECT NVL(IMP_ULT_NRO_REMISION, 0) + 1
          INTO l_nro
          FROM GEN_IMPRESORA
         WHERE IMP_EMPR = l_empresa_id
           AND IMPR_IP = FP_IP_USER;   
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          RAISE_APPLICATION_ERROR(-20010, 'Falta configurar ip impresora para la remision Nro: '||l_nro);
      END;
    else
      l_new := false;
    end if;
    
    merge into stk_remision_manual rm
    using (select
          l_empresa_id empresa_id,
          l_nro nro,
          l_fecha_emision fecha_emision,
          l_suc suc,
          l_dest_raz_soc dest_raz_soc,
          l_dest_ruc_ci dest_ruc_ci,
          l_dest_domicilio dest_domicilio,
          l_trasl_motivo trasl_motivo,
          l_trasl_comp trasl_comp,
          l_trasl_nro_comp trasl_nro_comp,
          l_trasl_nro_timb trasl_nro_timb,
          l_trasl_fecha_exp trasl_fecha_exp,
          l_trasl_fecha_inicio trasl_fecha_inicio,
          l_trasl_fecha_fin trasl_fecha_fin,
          l_trasl_dir_part trasl_dir_part,
          l_trasl_ciu_part trasl_ciu_part,
          l_trasl_dep_part trasl_dep_part,
          l_trasl_dir_lleg trasl_dir_lleg,
          l_trasl_ciu_lleg trasl_ciu_lleg,
          l_trasl_dep_lleg trasl_dep_lleg,
          l_trasl_km trasl_km,
          l_trasl_camb_fecha_term trasl_camb_fecha_term,
          l_trasl_mot_camb_fecha trasl_mot_camb_fecha,
          l_veh_marca veh_marca,
          l_veh_nro_rua veh_nro_rua,
          l_veh_nro_rua_remolque veh_nro_rua_remolque,
          l_conduc_raz_soc conduc_raz_soc,
          l_conduc_ruc_ci conduc_ruc_ci,
          l_conduc_domicilio conduc_domicilio
          from dual) rmn
    on (rm.empresa_id = rmn.empresa_id and rm.nro = rmn.nro)
    when matched then update set  rm.fecha_emision = rmn.fecha_emision,
                                  rm.suc = rmn.suc,
                                  rm.dest_raz_soc = rmn.dest_raz_soc,
                                  rm.dest_ruc_ci = rmn.dest_ruc_ci,
                                  rm.dest_domicilio = rmn.dest_domicilio,
                                  rm.trasl_motivo = rmn.trasl_motivo,
                                  rm.trasl_comp = rmn.trasl_comp,
                                  rm.trasl_nro_comp = rmn.trasl_nro_comp,
                                  rm.trasl_nro_timb = rmn.trasl_nro_timb,
                                  rm.trasl_fecha_exp = rmn.trasl_fecha_exp,
                                  rm.trasl_fecha_inicio = rmn.trasl_fecha_inicio,
                                  rm.trasl_fecha_fin = rmn.trasl_fecha_fin,
                                  rm.trasl_dir_part = rmn.trasl_dir_part,
                                  rm.trasl_ciu_part = rmn.trasl_ciu_part,
                                  rm.trasl_dep_part = rmn.trasl_dep_part,
                                  rm.trasl_dir_lleg = rmn.trasl_dir_lleg,
                                  rm.trasl_ciu_lleg = rmn.trasl_ciu_lleg,
                                  rm.trasl_dep_lleg = rmn.trasl_dep_lleg,
                                  rm.trasl_km = rmn.trasl_km,
                                  rm.trasl_camb_fecha_term = rmn.trasl_camb_fecha_term,
                                  rm.trasl_mot_camb_fecha = rmn.trasl_mot_camb_fecha,
                                  rm.veh_marca = rmn.veh_marca,
                                  rm.veh_nro_rua = rmn.veh_nro_rua,
                                  rm.veh_nro_rua_remolque = rmn.veh_nro_rua_remolque,
                                  rm.conduc_raz_soc = rmn.conduc_raz_soc,
                                  rm.conduc_ruc_ci = rmn.conduc_ruc_ci,
                                  rm.conduc_domicilio = rmn.conduc_domicilio
    when not matched then
         insert (rm.empresa_id,
                 rm.nro,
                 rm.fecha_emision,
                 rm.suc,
                 rm.dest_raz_soc,
                 rm.dest_ruc_ci,
                 rm.dest_domicilio,
                 rm.trasl_motivo,
                 rm.trasl_comp,
                 rm.trasl_nro_comp,
                 rm.trasl_nro_timb,
                 rm.trasl_fecha_exp,
                 rm.trasl_dir_part,
                 rm.trasl_ciu_part,
                 rm.trasl_dep_part,
                 rm.trasl_dir_lleg,
                 rm.trasl_ciu_lleg,
                 rm.trasl_dep_lleg,
                 rm.trasl_km,
                 rm.trasl_camb_fecha_term,
                 rm.trasl_mot_camb_fecha,
                 rm.veh_marca,
                 rm.veh_nro_rua,
                 rm.veh_nro_rua_remolque,
                 rm.conduc_raz_soc,
                 rm.conduc_ruc_ci,
                 rm.conduc_domicilio,
                 rm.trasl_fecha_inicio,
                 rm.trasl_fecha_fin
                 )
         values(rmn.empresa_id,
                rmn.nro,
                rmn.fecha_emision,
                rmn.suc,
                rmn.dest_raz_soc,
                rmn.dest_ruc_ci,
                rmn.dest_domicilio,
                rmn.trasl_motivo,
                rmn.trasl_comp,
                rmn.trasl_nro_comp,
                rmn.trasl_nro_timb,
                rmn.trasl_fecha_exp,
                rmn.trasl_dir_part,
                rmn.trasl_ciu_part,
                rmn.trasl_dep_part,
                rmn.trasl_dir_lleg,
                rmn.trasl_ciu_lleg,
                rmn.trasl_dep_lleg,
                rmn.trasl_km,
                rmn.trasl_camb_fecha_term,
                rmn.trasl_mot_camb_fecha,
                rmn.veh_marca,
                rmn.veh_nro_rua,
                rmn.veh_nro_rua_remolque,
                rmn.conduc_raz_soc,
                rmn.conduc_ruc_ci,
                rmn.conduc_domicilio,
                rmn.trasl_fecha_inicio,
                rmn.trasl_fecha_fin);
     
    -- upd numeracion
    if l_new then
      UPDATE GEN_IMPRESORA
      SET IMP_ULT_NRO_REMISION = l_nro
      WHERE IMPR_IP = FP_IP_USER
      AND IMP_EMPR = l_empresa_id;
    end if;
    
    out_nro := l_nro;
    
  end add_upd_remision;
  
  procedure add_det_remision(
    in_empresa    in  stk_remision_manual_det.empresa_id%type,
    in_remision   in  stk_remision_manual_det.remision_id%type,
    in_articulo   in  stk_remision_manual_det.articulo_id%type,
    in_cantidad   in  stk_remision_manual_det.cantidad%type,
    in_um         in  stk_remision_manual_det.unid_med%type,
    in_desc_art   in  stk_remision_manual_det.descripcion%type,   
    out_error     out varchar2
  )as
    co_prc_name constant varchar2(255 char) := 'add_det_remision';
   
    l_item     stk_remision_manual_det.item%type;
    l_empr     stk_remision_manual_det.empresa_id%type;
    l_remi     stk_remision_manual_det.remision_id%type;
    l_art      stk_remision_manual_det.articulo_id%type;
    l_cant     stk_remision_manual_det.cantidad%type;
    l_um       stk_remision_manual_det.unid_med%type;
    l_desc_art stk_remision_manual_det.descripcion%type;
    l_error    varchar2(20 char);

  begin
    l_empr := in_empresa;
    l_remi := in_remision;
    l_art  := in_articulo;
    l_cant := in_cantidad;
    l_um   := in_um;
    l_desc_art := in_desc_art;
    
    -- los sgtes errores estan fijados para el JS en APEX
    <<v_data>>
    case
      when l_art is null then
          l_error := 'NO_ART';
      when  nvl(l_cant, 0) <= 0 then
          l_error := 'NO_CANT';
      when l_desc_art is null then
          l_error := 'NO_DESC';
      when l_um is null then
          l_error := 'NO_UM';
      else
          l_error := null;
    end case v_data;
    
    if l_error is null then
      <<obt_item>>
      begin
        select nvl(max(nvl(d.item, 0 ) ), 0) + 1
        into l_item
        from stk_remision_manual_det d
        where d.empresa_id  = l_empr
        and   d.remision_id = l_remi;
      exception
        when no_Data_found then
          l_item := 1;
      end obt_item;
      
      --> si existe solo actualiza la cantidad
      merge into stk_remision_manual_det rm
      using (select l_empr empresa_id
            ,       l_remi remision_id
            ,       l_art  articulo_id
            ,       l_um   unid_med
            ,       l_desc_art descripcion 
            from dual
            ) rmn
      on (rm.empresa_id  = rmn.empresa_id  and
          rm.remision_id = rmn.remision_id and
          rm.articulo_id = rmn.articulo_id and
          rm.unid_med    = rmn.unid_med    and
          rm.descripcion = rmn.descripcion 
          )
      when matched then update set
          rm.cantidad = (rm.cantidad + l_cant)
      when not matched then 
          insert (empresa_id,  remision_id,   item,        articulo_id,
                  cantidad,    unid_med,      descripcion)
           values(l_empr,      l_remi,        l_item,       l_art, 
                  l_cant,      l_um,          l_desc_art);

    end if;
   
    out_error := l_error;
    
  exception
    when others then
      Raise_application_error(-20000, 'Error en '||co_prc_name||': '||sqlerrm); 
  end add_det_remision;
  
  procedure del_remision(
    in_remision in stk_remision_manual.id%type
  )as
   l_ult_a_eliminar number;
   l_ult_actual     number;
   l_empresa        number;
   l_nro_rem        stk_remision_manual.nro%type;
   l_ip_user        varchar2(100);
  begin
    -- recordar si es VPN va a cambiar
    l_ip_user := fp_ip_user;
    
    <<obt_empresa_rem>>
    begin
      select m.empresa_id,
             m.nro
      into l_empresa,
           l_nro_rem
      from stk_remision_manual m
      where m.id = in_remision;
      
    exception
      when no_data_found then
        l_empresa := ap.v(p_item => 'P_EMPRESA');
    end obt_empresa_rem;
    
    <<obt_ult_nro_actual>>
    begin
      select ia.imp_ult_nro_remision
      into l_ult_actual
      from gen_impresora ia
      WHERE ia.IMPR_IP = l_ip_user
      AND ia.IMP_EMPR  = l_empresa;
      
      if l_ult_actual = l_nro_rem then
        update gen_impresora
        set imp_ult_nro_remision = (l_ult_actual - 1)
        where impr_ip  = l_ip_user
        and   imp_empr = l_empresa;
      end if;
    exception
      when no_data_found then
        null;
    end obt_ult_nro_actual;    
    
    delete from stk_remision_manual s
    where s.id = in_remision;
    
  end del_remision;
END STKI004_ALIMENTEC;
/
