prompt --application/set_environment
set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_200100 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2020.03.31'
,p_release=>'20.1.0.00.13'
,p_default_workspace_id=>2933390818164287
,p_default_application_id=>102
,p_default_id_offset=>0
,p_default_owner=>'ADCS'
);
end;
/
 
prompt APPLICATION 102 - CONTROL DE STOCK
--
-- Application Export:
--   Application:     102
--   Name:            CONTROL DE STOCK
--   Date and Time:   15:28 Monday August 29, 2022
--   Exported By:     PABLOC
--   Flashback:       0
--   Export Type:     Page Export
--   Manifest
--     PAGE: 103
--   Manifest End
--   Version:         20.1.0.00.13
--   Instance ID:     211687171918188
--

begin
null;
end;
/
prompt --application/pages/delete_00103
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>103);
end;
/
prompt --application/pages/page_00103
begin
wwv_flow_api.create_page(
 p_id=>103
,p_user_interface_id=>wwv_flow_api.id(4023662047352556)
,p_name=>'STKI004 - REMISION MANUAL ALIMENTEC'
,p_alias=>'STKI004-REMISION-MANUAL-ALIMENTEC'
,p_step_title=>'STKI004 - REMISION MANUAL ALIMENTEC'
,p_autocomplete_on_off=>'OFF'
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'PABLOC'
,p_last_upd_yyyymmddhh24miss=>'20220829152807'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(711240178080623017)
,p_plug_name=>'Principal'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(3990068395352506)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(711240531346623021)
,p_plug_name=>'Datos Destinatario'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody:t-Form--slimPadding'
,p_plug_template=>wwv_flow_api.id(3990068395352506)
,p_plug_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(711240959580623025)
,p_plug_name=>'Datos Traslado'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody:t-Form--slimPadding'
,p_plug_template=>wwv_flow_api.id(3990068395352506)
,p_plug_display_sequence=>70
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_grid_column_span=>8
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(711242411272623040)
,p_plug_name=>unistr('Datos Veh\00EDculo')
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody:t-Form--slimPadding'
,p_plug_template=>wwv_flow_api.id(3990068395352506)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(711242855298623044)
,p_plug_name=>'Datos Conductor'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody:t-Form--slimPadding'
,p_plug_template=>wwv_flow_api.id(3990068395352506)
,p_plug_display_sequence=>50
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(711243277388623048)
,p_plug_name=>'Detalle'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(3990068395352506)
,p_plug_display_sequence=>60
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_grid_column_span=>4
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(712136995284238604)
,p_name=>'{listDetail}'
,p_region_name=>'regListDetail'
,p_parent_plug_id=>wwv_flow_api.id(711243277388623048)
,p_template=>wwv_flow_api.id(3980041809352498)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#:t-Report--stretch:t-Report--altRowsDefault:t-Report--rowHighlight'
,p_display_point=>'BODY'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'TABLE'
,p_query_table=>'STK_REMISION_MANUAL_DET'
,p_query_where=>'REMISION_ID = :P103_REMISION_ID'
,p_query_order_by=>'ITEM DESC'
,p_include_rowid_column=>false
,p_ajax_enabled=>'Y'
,p_ajax_items_to_submit=>'P103_REMISION_ID'
,p_query_row_template=>wwv_flow_api.id(4000084859352516)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_no_data_found=>'Sin registros'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(712139136303238626)
,p_query_column_id=>1
,p_column_alias=>'ID'
,p_column_display_sequence=>1
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(712139240495238627)
,p_query_column_id=>2
,p_column_alias=>'EMPRESA_ID'
,p_column_display_sequence=>2
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(712139396611238628)
,p_query_column_id=>3
,p_column_alias=>'REMISION_ID'
,p_column_display_sequence=>3
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(712139441345238629)
,p_query_column_id=>4
,p_column_alias=>'ITEM'
,p_column_display_sequence=>4
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_report_column_width=>1
,p_derived_column=>'N'
,p_include_in_export=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(712139593530238630)
,p_query_column_id=>5
,p_column_alias=>'ARTICULO_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Cod Art'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(712139678820238631)
,p_query_column_id=>6
,p_column_alias=>'UNID_MED'
,p_column_display_sequence=>8
,p_column_heading=>'UM'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(712139784993238632)
,p_query_column_id=>7
,p_column_alias=>'DESCRIPCION'
,p_column_display_sequence=>7
,p_column_heading=>unistr('Descripci\00F3n')
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(712318493914434701)
,p_query_column_id=>8
,p_column_alias=>'CANTIDAD'
,p_column_display_sequence=>6
,p_column_heading=>'Cant'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'1'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(712140229368238637)
,p_query_column_id=>9
,p_column_alias=>'DERIVED$01'
,p_column_display_sequence=>9
,p_column_heading=>'&nbsp'
,p_use_as_row_header=>'N'
,p_column_link=>'#'
,p_column_linktext=>'<span class="fa fa-times-circle u-danger-text" aria-hidden="true"></span>'
,p_column_link_attr=>'onclick="$s(''P103_DET_DEL'', #ID#)"'
,p_column_alignment=>'CENTER'
,p_derived_column=>'Y'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(712137619886238611)
,p_button_sequence=>60
,p_button_plug_id=>wwv_flow_api.id(711243277388623048)
,p_button_name=>'AGREGAR'
,p_button_static_id=>'btnAddDetail'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--success:t-Button--stretch:t-Button--padBottom'
,p_button_template_id=>wwv_flow_api.id(4012807212352532)
,p_button_image_alt=>'Agregar'
,p_button_position=>'BODY'
,p_warn_on_unsaved_changes=>null
,p_button_css_classes=>'u-pullRight'
,p_grid_new_row=>'Y'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(712137842545238613)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(711240178080623017)
,p_button_name=>'GUARDAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#:t-Button--success'
,p_button_template_id=>wwv_flow_api.id(4012807212352532)
,p_button_image_alt=>'Guardar'
,p_button_position=>'REGION_TEMPLATE_EDIT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(712137979084238614)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(711240178080623017)
,p_button_name=>'BORRAR'
,p_button_static_id=>'btnDelete'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#:t-Button--danger'
,p_button_template_id=>wwv_flow_api.id(4012807212352532)
,p_button_image_alt=>'Borrar'
,p_button_position=>'REGION_TEMPLATE_EDIT'
,p_button_execute_validations=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(712141173969238646)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(711240178080623017)
,p_button_name=>'IMPRIMIR'
,p_button_static_id=>'btnPrint'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--warning'
,p_button_template_id=>wwv_flow_api.id(4012807212352532)
,p_button_image_alt=>'Imprimir'
,p_button_position=>'REGION_TEMPLATE_EDIT'
,p_warn_on_unsaved_changes=>null
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711240264834623018)
,p_name=>'P103_PR_FECHA_EMISION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(711240178080623017)
,p_item_default=>'current_date'
,p_item_default_type=>'PLSQL_EXPRESSION'
,p_prompt=>'Fecha Emision'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711240386455623019)
,p_name=>'P103_PR_NRO_REMISION'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(711240178080623017)
,p_prompt=>unistr('Nro Remisi\00F3n')
,p_placeholder=>unistr('Dejar vac\00EDo si es nuevo')
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select s.nro||'' ''||s.fecha_emision d,',
'       s.nro',
'from stk_remision_manual s',
'where s.empresa_id = :p_empresa'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'%'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_08=>'650'
,p_attribute_09=>'650'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711240644827623022)
,p_name=>'P103_DD_RAZ_SOC'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(711240531346623021)
,p_prompt=>unistr('Raz\00F3n Social')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711240719732623023)
,p_name=>'P103_DD_RUC_CI'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(711240531346623021)
,p_prompt=>'RUC o CI'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711240812123623024)
,p_name=>'P103_DD_DOMICILIO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(711240531346623021)
,p_prompt=>'Domicilio'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711241030310623026)
,p_name=>'P103_DT_MOTIVO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(711240959580623025)
,p_prompt=>'Motivo Traslado'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>8
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711241120600623027)
,p_name=>'P103_DT_COMP_VENTA_NRO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(711240959580623025)
,p_prompt=>unistr('N\00BA Comprobante Venta')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711241201702623028)
,p_name=>'P103_DT_FECHA_INICIO'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(711240959580623025)
,p_item_default=>'current_date'
,p_item_default_type=>'PLSQL_EXPRESSION'
,p_prompt=>'Fecha Inicio'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711241335839623029)
,p_name=>'P103_DT_COMPROBANTE_VENTA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(711240959580623025)
,p_prompt=>'Comprobante Venta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711241495564623030)
,p_name=>'P103_DT_NRO_TIMBRADO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(711240959580623025)
,p_prompt=>unistr('N\00BA Timbrado')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711241562108623031)
,p_name=>'P103_DT_FECHA_TERMINO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(711240959580623025)
,p_item_default=>'current_date'
,p_item_default_type=>'PLSQL_EXPRESSION'
,p_prompt=>'Fecha Termino Traslado'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711241612502623032)
,p_name=>'P103_DT_DIR_PUNTO_PARTIDA'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(711240959580623025)
,p_prompt=>unistr('Direcci\00F3n Punto Partida')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711241737821623033)
,p_name=>'P103_DT_PUNTO_PART_CIUDAD'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(711240959580623025)
,p_prompt=>'Ciudad punto partida'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711241825786623034)
,p_name=>'P103_DT_PUNTO_PART_DEP'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(711240959580623025)
,p_prompt=>'Departamento punto Partida'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711241951664623035)
,p_name=>'P103_DT_DIR_PUNTO_LLEGADA'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(711240959580623025)
,p_prompt=>unistr('Direcci\00F3n punto llegada')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711242002042623036)
,p_name=>'P103_DT_DIR_PUN_LLEG_CIUDAD'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_api.id(711240959580623025)
,p_prompt=>unistr('Ciudad Direcci\00F3n Llegada')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711242125837623037)
,p_name=>'P103_DT_KM_ESTIMADO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(711240959580623025)
,p_prompt=>'KM Estimados'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711242201921623038)
,p_name=>'P103_DT_CAMB_FECHA_TERM'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_api.id(711240959580623025)
,p_prompt=>unistr('Cambio Fecha de t\00E9rmino de traslado')
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_colspan=>4
,p_field_template=>wwv_flow_api.id(4012309661352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711242310204623039)
,p_name=>'P103_DT_MOTIVO_CAMBIO_FECHA'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_api.id(711240959580623025)
,p_prompt=>'Motivo Cambio Fecha'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(4012309661352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711242539175623041)
,p_name=>'P103_DV_MARCA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(711242411272623040)
,p_prompt=>'Marca'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711242681729623042)
,p_name=>'P103_DV_RUA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(711242411272623040)
,p_prompt=>unistr('N\00BA RUA')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711242773910623043)
,p_name=>'P103_DV_RUA_REMOLQUE'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(711242411272623040)
,p_prompt=>'RUA Remolque'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(4012309661352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711242957812623045)
,p_name=>'P103_DC_RAZON_SOCIAL'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(711242855298623044)
,p_prompt=>unistr('Raz\00F3n Social')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711243041066623046)
,p_name=>'P103_DC_RUC_CI'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(711242855298623044)
,p_prompt=>'RUC o CI'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711243189446623047)
,p_name=>'P103_DC_DOMICILIO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(711242855298623044)
,p_prompt=>'Domicilio'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711243307608623049)
,p_name=>'P103_DET_ARTICULO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(711243277388623048)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Art\00EDculo')
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'ART_REMISION_ALIMENTEC'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select art_desc,',
'       art_codigo,',
'       art_cod_alfanumerico,art_codigo_fabrica',
'  from stk_articulo,',
'       stk_articulo_empresa',
' where art_codigo = arem_art',
'       AND ART_EMPR=AREM_EMPR',
'       and arem_empr = :p_empresa',
'       and art_est = ''A''',
'       and art_tipo <> 2 --> PRODUCCION',
'       and stk_articulo.art_empr = stk_articulo_empresa.arem_empr',
'order by 1',
''))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'%'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_08=>'850'
,p_attribute_09=>'650'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(711243422669623050)
,p_name=>'P103_DET_CANTIDAD'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(711243277388623048)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cantidad'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_03=>'center'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(712136688508238601)
,p_name=>'P103_DET_UM'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(711243277388623048)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Unidad Medida'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(712136740237238602)
,p_name=>'P103_DET_DESCRIPCION'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(711243277388623048)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Descripci\00F3n')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(712136801510238603)
,p_name=>'P103_DT_PUNTO_LLEG_DEP'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_api.id(711240959580623025)
,p_prompt=>'Departamento punto llegada'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(712137182441238606)
,p_name=>'P103_PR_TIPO_IMPRESION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(711240178080623017)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(712138106999238616)
,p_name=>'P103_DT_FECHA_EXP_DOC'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(711240959580623025)
,p_prompt=>unistr('Fecha Expedici\00F3n Documento')
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(4012590370352530)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(712138347934238618)
,p_name=>'P103_REMISION_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(711240178080623017)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(712140675625238641)
,p_name=>'P103_DET_DEL'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(712136995284238604)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(712318791579434704)
,p_name=>'P103_DET_ERROR'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(711243277388623048)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(712319190530434708)
,p_name=>'P103_SIG_NRO_REM'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(711240178080623017)
,p_use_cache_before_default=>'NO'
,p_item_default=>'Configure su impresora en el programa 99-1-105'
,p_prompt=>unistr('Siguiente Nro Remisi\00F3n')
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select nvl(ia.imp_ult_nro_remision, 0) + 1 X',
'from gen_impresora ia',
'WHERE ia.IMPR_IP = fp_ip_user',
'AND  ia.IMP_EMPR  = :p_empresa;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_css_classes=>'u-textCenter u-bold'
,p_tag_attributes=>'style="border-style:double;"'
,p_field_template=>wwv_flow_api.id(6252202093336734)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(712319427215434711)
,p_name=>'P103_URL_PDF'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(711240178080623017)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(712137396312238608)
,p_name=>'get Info art'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P103_DET_ARTICULO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(712137498199238609)
,p_event_id=>wwv_flow_api.id(712137396312238608)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'select art_desc,',
'       ar.art_unid_med',
'into :P103_DET_DESCRIPCION,',
':P103_DET_UM',
'from stk_articulo ar,',
'     stk_articulo_empresa ae',
'where art_codigo = arem_art',
'AND ar.ART_EMPR=ae.AREM_EMPR',
'and ae.arem_empr = :p_empresa',
'and ar.art_est = ''A''',
'and ar.art_tipo <> 2 --> PRODUCCION',
'and ar.art_empr = ae.arem_empr',
'and ar.art_codigo=:P103_DET_ARTICULO;',
'',
'',
'exception',
'    when others then',
'     :P103_DET_DESCRIPCION := null;',
'     :P103_DET_UM  := null;',
'end;'))
,p_attribute_02=>'P103_DET_ARTICULO,P_EMPRESA'
,p_attribute_03=>'P103_DET_UM,P103_DET_DESCRIPCION'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(712138426631238619)
,p_name=>'Busca remision'
,p_event_sequence=>20
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P103_PR_NRO_REMISION'
,p_condition_element=>'P103_PR_NRO_REMISION'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(712138519362238620)
,p_event_id=>wwv_flow_api.id(712138426631238619)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  select m.id into :P103_REMISION_ID',
'  from stk_remision_manual m',
'  where m.nro=:P103_PR_NRO_REMISION',
'  and   m.empresa_id = :P_EMPRESA;',
'exception',
'  when others then',
'    :P103_REMISION_ID := null;',
'end;',
''))
,p_attribute_02=>'P103_PR_NRO_REMISION,P_EMPRESA'
,p_attribute_03=>'P103_REMISION_ID'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(712318509003434702)
,p_event_id=>wwv_flow_api.id(712138426631238619)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P103_REMISION_ID'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(712138883871238623)
,p_name=>'obt datos'
,p_event_sequence=>30
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P103_REMISION_ID'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(712138909804238624)
,p_event_id=>wwv_flow_api.id(712138883871238623)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'select r.fecha_emision,',
'       r.dest_raz_soc,',
'       r.dest_ruc_ci,',
'       r.dest_domicilio,',
'       r.trasl_motivo,',
'       r.trasl_comp,',
'       r.trasl_nro_comp,',
'       r.trasl_nro_timb,',
'       r.trasl_fecha_exp,',
'       r.trasl_fecha_inicio,',
'       r.trasl_fecha_fin,',
'       r.trasl_km,',
'       r.trasl_dir_part,',
'       r.trasl_ciu_part,',
'       r.trasl_dep_part,',
'       r.trasl_dir_lleg,',
'       r.trasl_ciu_lleg,',
'       r.trasl_dep_lleg,',
'       r.trasl_camb_fecha_term,',
'       r.trasl_mot_camb_fecha,',
'       r.veh_marca,',
'       r.veh_nro_rua,',
'       r.veh_nro_rua_remolque,',
'       r.conduc_raz_soc,',
'       r.conduc_ruc_ci,',
'       r.conduc_domicilio',
'into :P103_PR_FECHA_EMISION,',
'	 :P103_DD_RAZ_SOC,',
'	 :P103_DD_RUC_CI,',
'	 :P103_DD_DOMICILIO,',
'	 :P103_DT_MOTIVO,',
'	 :P103_DT_COMPROBANTE_VENTA,',
'	 :P103_DT_COMP_VENTA_NRO,',
'	 :P103_DT_NRO_TIMBRADO,',
'	 :P103_DT_FECHA_EXP_DOC,',
'	 :P103_DT_FECHA_INICIO,',
'	 :P103_DT_FECHA_TERMINO,',
'	 :P103_DT_KM_ESTIMADO,',
'	 :P103_DT_DIR_PUNTO_PARTIDA,',
'	 :P103_DT_PUNTO_PART_CIUDAD,',
'	 :P103_DT_PUNTO_PART_DEP,',
'	 :P103_DT_DIR_PUNTO_LLEGADA,',
'	 :P103_DT_DIR_PUN_LLEG_CIUDAD,',
'	 :P103_DT_PUNTO_LLEG_DEP,',
'	 :P103_DT_CAMB_FECHA_TERM,',
'	 :P103_DT_MOTIVO_CAMBIO_FECHA,',
'	 :P103_DV_MARCA,',
'	 :P103_DV_RUA,',
'	 :P103_DV_RUA_REMOLQUE,',
'	 :P103_DC_RAZON_SOCIAL,',
'	 :P103_DC_RUC_CI,',
'	 :P103_DC_DOMICILIO',
'from stk_remision_manual r',
'where r.id = :P103_REMISION_ID;',
'exception',
'when no_data_found then',
'    :P103_PR_FECHA_EMISION := null;',
'	:P103_DD_RAZ_SOC := null;',
'	:P103_DD_RUC_CI := null;',
'	:P103_DD_DOMICILIO := null;',
'	:P103_DT_MOTIVO := null;',
'	:P103_DT_COMPROBANTE_VENTA := null;',
'	:P103_DT_COMP_VENTA_NRO := null;',
'	:P103_DT_NRO_TIMBRADO := null;',
'	:P103_DT_FECHA_EXP_DOC := null;',
'	:P103_DT_FECHA_INICIO := null;',
'	:P103_DT_FECHA_TERMINO := null;',
'	:P103_DT_KM_ESTIMADO := null;',
'	:P103_DT_DIR_PUNTO_PARTIDA := null;',
'	:P103_DT_PUNTO_PART_CIUDAD := null;',
'	:P103_DT_PUNTO_PART_DEP := null;',
'	:P103_DT_DIR_PUNTO_LLEGADA := null;',
'	:P103_DT_DIR_PUN_LLEG_CIUDAD := null;',
'	:P103_DT_PUNTO_LLEG_DEP := null;',
'	:P103_DT_CAMB_FECHA_TERM := null;',
'	:P103_DT_MOTIVO_CAMBIO_FECHA := null;',
'	:P103_DV_MARCA := null;',
'	:P103_DV_RUA := null;',
'	:P103_DV_RUA_REMOLQUE := null;',
'	:P103_DC_RAZON_SOCIAL := null;',
'	:P103_DC_RUC_CI := null;',
'	:P103_DC_DOMICILIO := null;',
'end;'))
,p_attribute_02=>'P103_REMISION_ID'
,p_attribute_03=>'P103_PR_FECHA_EMISION,P103_DD_RAZ_SOC,P103_DD_RUC_CI,P103_DD_DOMICILIO,P103_DT_MOTIVO,P103_DT_COMPROBANTE_VENTA,P103_DT_COMP_VENTA_NRO,P103_DT_NRO_TIMBRADO,P103_DT_FECHA_EXP_DOC,P103_DT_FECHA_INICIO,P103_DT_FECHA_TERMINO,P103_DT_KM_ESTIMADO,P103_DT_D'
||'IR_PUNTO_PARTIDA,P103_DT_PUNTO_PART_CIUDAD,P103_DT_PUNTO_PART_DEP,P103_DT_DIR_PUNTO_LLEGADA,P103_DT_DIR_PUN_LLEG_CIUDAD,P103_DT_PUNTO_LLEG_DEP,P103_DT_CAMB_FECHA_TERM,P103_DT_MOTIVO_CAMBIO_FECHA,P103_DV_MARCA,P103_DV_RUA,P103_DV_RUA_REMOLQUE,P103_DC_'
||'RAZON_SOCIAL,P103_DC_RUC_CI,P103_DC_DOMICILIO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
end;
/
begin
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(712139038178238625)
,p_event_id=>wwv_flow_api.id(712138883871238623)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'var elem = [''P103_DET_ARTICULO'',''P103_DET_CANTIDAD'',''P103_DET_DESCRIPCION'',''P103_DET_UM'', ''btnAddDetail'', ''btnPrint'', ''btnDelete''];',
'var remId = apex.item(''P103_REMISION_ID'');',
'',
'$x_Value([''P103_DET_ARTICULO'',''P103_DET_CANTIDAD'',''P103_DET_DESCRIPCION'',''P103_DET_UM''], '''');',
'',
'if (remId.isEmpty()){',
'    $x_disableItem(elem, true);',
'}else{',
'    $x_disableItem(elem, false);',
'}'))
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(712141011253238645)
,p_event_id=>wwv_flow_api.id(712138883871238623)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(712136995284238604)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(712140393148238638)
,p_name=>'add detail'
,p_event_sequence=>40
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(712137619886238611)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(712140466378238639)
,p_event_id=>wwv_flow_api.id(712140393148238638)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'stki004_alimentec.add_det_remision(',
'  in_empresa  => :P_EMPRESA,',
'  in_remision => :P103_REMISION_ID,',
'  in_articulo => :P103_DET_ARTICULO,',
'  in_cantidad => :P103_DET_CANTIDAD,',
'  in_um       => :P103_DET_UM,',
'  in_desc_art => :P103_DET_DESCRIPCION,',
'  out_error   => :P103_DET_ERROR',
');'))
,p_attribute_02=>'P103_REMISION_ID,P_EMPRESA,P103_DET_ARTICULO,P103_DET_CANTIDAD,P103_DET_UM,P103_DET_DESCRIPCION'
,p_attribute_03=>'P103_DET_ERROR'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(712140722835238642)
,p_name=>'Del item'
,p_event_sequence=>50
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P103_DET_DEL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(712140851601238643)
,p_event_id=>wwv_flow_api.id(712140722835238642)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'delete from stk_remision_manual_det d',
'where d.id = :P103_DET_DEL;'))
,p_attribute_02=>'P103_DET_DEL'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(712140962397238644)
,p_event_id=>wwv_flow_api.id(712140722835238642)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(712136995284238604)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(712318828042434705)
,p_name=>'onChange show or hide error'
,p_event_sequence=>60
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P103_DET_ERROR'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(712318902904434706)
,p_event_id=>wwv_flow_api.id(712318828042434705)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'const REG_DET = ''regListDetail'';',
'',
'let iError = apex.item(''P103_DET_ERROR'');',
'',
'function indTipo (indD) {',
'  var optionInd = {',
'    ''NO_ART'': function () {',
'      return {',
'                type:       "error",',
'                location:   [ "inline" ],',
'                pageItem:   "P103_DET_ARTICULO",',
unistr('                message:    "Seleccione un art\00EDculo del listado",'),
'                unsafe:     false',
'            }',
'    },',
'      ',
'    ''NO_CANT'': function () {',
'      return {',
'                type:       "error",',
'                location:   [ "inline" ],',
'                pageItem:   "P103_DET_CANTIDAD",',
'                message:    "Complete la cantidad",',
'                unsafe:     false',
'            }',
'    },',
'',
'    ''NO_DESC'': function () {',
'      return {',
'                type:       "error",',
'                location:   [ "inline" ],',
'                pageItem:   "P103_DET_DESCRIPCION",',
unistr('                message:    "Complete la descripci\00F3n del art\00EDculo",'),
'                unsafe:     false',
'            }',
'    },',
'',
'    ''NO_UM'': function () {',
'      return {',
'                type:       "error",',
'                location:   [ "inline" ],',
'                pageItem:   "P103_DET_UM",',
unistr('                message:    "Complete la Unidad de Medida del art\00EDculo",'),
'                unsafe:     false',
'            }',
'    }',
'  };',
'  return optionInd[indD]();',
'}',
'',
'',
'if (iError.isEmpty()){',
'   apex.region(REG_DET).refresh();',
'   apex.item(''P103_DET_ARTICULO'').setValue('''');',
'}else{',
'    // First clear the errors',
'    apex.message.clearErrors();',
'    ',
'    // get Error',
'    var errorData = indTipo(iError.getValue());',
'',
'    // Now show new errors',
'    apex.message.showErrors([errorData]);',
'}'))
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(712319246942434709)
,p_name=>'getUrl PDF'
,p_event_sequence=>70
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(712141173969238646)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(712319378649434710)
,p_event_id=>wwv_flow_api.id(712319246942434709)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'jripacr.get_report_url(',
'  p_rep_name => ''STKI003_PREIMPRESO'',',
'  in_empresa => :P_EMPRESA,',
'  p_additional_params => ''REMISION_ID=''||:P103_REMISION_ID,',
'  p_url      => :P103_URL_PDF',
'  );'))
,p_attribute_02=>'P_EMPRESA,P103_REMISION_ID'
,p_attribute_03=>'P103_URL_PDF'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(712319512636434712)
,p_name=>'open pdf'
,p_event_sequence=>80
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P103_URL_PDF'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(712319620748434713)
,p_event_id=>wwv_flow_api.id(712319512636434712)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'let iUrl = apex.item(''P103_URL_PDF'').getValue();',
'apex.navigation.openInNewWindow( iUrl, "MyWindow" );'))
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(712138264689238617)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'guardar'
,p_process_sql_clob=>'stki004_alimentec.add_upd_remision(in_page_id => :APP_PAGE_ID, out_nro => :P103_PR_NRO_REMISION);'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'GUARDAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(712319017848434707)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'delete'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'stki004_alimentec.del_remision(in_remision => to_number(:P103_REMISION_ID));',
':P103_PR_NRO_REMISION := null;',
':P103_REMISION_ID     := null;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>':REQUEST = ''BORRAR'' and :P103_REMISION_ID is not null'
,p_process_when_type=>'PLSQL_EXPRESSION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(712137039472238605)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Cargar Datos'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  SELECT T.IMP_NR_TIPO_IMPRESION',
'    INTO :P103_PR_TIPO_IMPRESION',
'    FROM GEN_IMPRESORA T',
'   WHERE IMPR_IP = FP_IP_USER',
'     AND IMP_EMPR = :P_EMPRESA;',
'',
'  :P103_PR_TIPO_IMPRESION := NVL(:P103_PR_TIPO_IMPRESION, ''P'');',
'',
'EXCEPTION',
'  WHEN NO_DATA_FOUND THEN',
'    :P8001_P_TIPO_IMPRESION := NVL(:P103_PR_TIPO_IMPRESION, ''P'');',
'  WHEN OTHERS THEN',
'    NULL;',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
prompt --application/end_environment
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
