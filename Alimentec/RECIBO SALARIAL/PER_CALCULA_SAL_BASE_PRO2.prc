CREATE OR REPLACE PROCEDURE PER_CALCULA_SAL_BASE_PRO2(V_LEGAJO      IN NUMBER,
                                                      V_EMPRESA     IN NUMBER,
                                                      V_MARC        OUT CHAR,
                                                      V_SALARIO_SAL OUT NUMBER,
                                                      V_INCLUIR     OUT VARCHAR2,
                                                      V_FORMA_PAGO  IN NUMBER,
                                                      V_CANT_DIAS   OUT NUMBER,
                                                      V_FEC_SALIDA  IN DATE DEFAULT NULL,
                                                      V_FEC_DESDE   IN DATE  DEFAULT NULL) IS


  V_TOTAL_DIAS                  NUMBER := 0;
  V_CANT_DIA_NORMAL             NUMBER := 0;
  V_CANT_REPOSO                 NUMBER := 0;
  V_CANT_AUSENCIAS              NUMBER := 0;
  V_CANT_VACACION               NUMBER := 0;
  V_DIAS_TRABAJADOS             NUMBER := 0;
  V_CANT_REPO_PAGADO            NUMBER := 0;
  P_FECHA                       DATE   := NVL(V_FEC_DESDE, TRUNC(SYSDATE,'MM'));



  V_IMP_DIA_VAC                 NUMBER := 0;
  V_IMP_AUS_HOR                 NUMBER := 0;

  V_IMPORTE_VACACION            NUMBER := 0;
  V_IMPORTE_AUSENCIA            NUMBER := 0;
  V_IMPORTE_AUS_HORA            NUMBER := 0;
  V_IMPORTE_REPOSO              NUMBER:= 0;

  V_IMPORTE_DIA_SALARIO         NUMBER :=0;
  V_SALARIO                     NUMBER := 0;

  m_salario_base                number:= 0;
v_cant_dia_mes                  number;


   CURSOR EMPLEADO_FECHA is 
      SELECT EMPL_LEGAJO LEGAJO,
      EMPL_FEC_INGRESO,
      CASE WHEN EMPL_SALARIO_BASE IS NULL THEN
                HEMPL_SALARIO
           ELSE
               EMPL_SALARIO_BASE
           END SALARIO,
      EMPL_FORMA_PAGO,
      FECHA,
      HEMPL_FORMA_PAGO,
      EMPL_EMPRESA EMPRESA,
      PER_VERIFICAR_DIA_LABORAL(V_EMPL              => EMPL_LEGAJO,
                                V_EMPRESA           => EMPL_EMPRESA,
                                V_FECHA             => FECHA,---,'DD/MM/YYYY',
                                V_FEC_INGRESO_FUN   => EMPL_FEC_INGRESO,
                                V_FEC_SALIDA_FUN    => EMPL_FEC_SALIDA,
                                V_TIP_COB           => DECODE(HEMPL_FORMA_PAGO,2,'M',5,'M','S'),
                                V_PAGADOS           => 'N') DIA_LABORAL
        FROM PER_EMPLEADO P,
             PER_HIST_PAGO_EMPL_V A,
             (SELECT (TO_DATE('01/'||TO_CHAR(TO_DATE(P_FECHA),'MM/YYYY'))-1)+ LEVEL FECHA
               FROM DUAL
              CONNECT BY LEVEL <= CASE WHEN V_FEC_SALIDA IS NULL THEN
                                       TO_CHAR(LAST_DAY(TO_DATE(P_FECHA)),'DD')
                                       ELSE
                                        TO_CHAR(TO_DATE(V_FEC_SALIDA),'DD')
                                       end
             ) FEC
        WHERE P.EMPL_LEGAJO =V_LEGAJO
          AND P.EMPL_EMPRESA = V_EMPRESA
          AND P.EMPL_LEGAJO = A.HEMPL_LEGAJO
          AND P.EMPL_EMPRESA = A.HEMPL_EMPR
          AND FEC.FECHA BETWEEN  A.HFEC_DESDE AND A.HFEC_HASTA
          AND HEMPL_FORMA_PAGO = V_FORMA_PAGO
          AND HEMPL_TIPO_PAGO = 1
          AND FEC.FECHA BETWEEN P_FECHA  AND (CASE WHEN V_FEC_SALIDA IS NULL THEN
                                              LAST_DAY(TO_DATE(P_FECHA))
                                       ELSE
                                              V_FEC_SALIDA
                                       END ) 
          GROUP BY EMPL_LEGAJO,
                   EMPL_FEC_INGRESO,
                   EMPL_SALARIO_BASE,
                   HEMPL_SALARIO,
                   EMPL_FORMA_PAGO,
                   FECHA,
                   HEMPL_FORMA_PAGO,
                   EMPL_FEC_SALIDA,
                   EMPL_EMPRESA
         ORDER BY FEC.FECHA;

X_DIAS_LABORAL  VARCHAR2(2);
l_count number := 0;
BEGIN

   DELETE PER_PERM056_DET
              WHERE LEGAJO = V_LEGAJO
                AND EMPR = V_EMPRESA;
                commit;
   
      
  FOR  V IN EMPLEADO_FECHA   loop
    
DELETE PER_PERM056_DET
WHERE LEGAJO = V_LEGAJO
  AND FECHA=  V.FECHA
  AND EMPR = V_EMPRESA;



IF  V.DIA_LABORAL = 'W' THEN
  X_DIAS_LABORAL :=  'S';
  else
     X_DIAS_LABORAL :=  V.DIA_LABORAL;
end if;

IF V.FECHA  > '25/11/2021'  and V.DIA_LABORAL = 'W' THEN
  X_DIAS_LABORAL :=  'S';

END IF;

IF V.FECHA  = '13/11/2021'  and V.DIA_LABORAL = 'W' THEN
  X_DIAS_LABORAL :=  'S';

END IF;


       INSERT INTO PER_PERM056_DET
             (LEGAJO, FECHA, DIA_LABORAL, EMPR)
           VALUES
             (V_LEGAJO, V.FECHA, X_DIAS_LABORAL, V.EMPRESA);




--------------*********SOLO PARA TIPO DE PAGO MENSUALERO Y AHM*********--------------
       IF V.HEMPL_FORMA_PAGO IN (2,5) then
          
          V_INCLUIR := 'S';

           V_TOTAL_DIAS := V_TOTAL_DIAS +1;----------- TODOS LOS DIAS TRABAJADOS DESDE QUE EL EMPLEADO EMPIEZA

           IF nvl(V.SALARIO,0) IS NULL THEN
             RAISE_APPLICATION_ERROR (-20001, 'El empleado no tiene cargado el importe de salario base: '||V_LEGAJO);
           END IF;

           V_IMPORTE_DIA_SALARIO := (V.SALARIO/30);        ----------- IMPORTE POR DIA
 ---DBMS_OUTPUT.put_line (V.DIA_LABORAL||'  --  '||V_TOTAL_DIAS||'  --  '|| V_CANT_DIA_NORMAL);--;||'  --  '|| V_IMPORTE_DIA_SALARIO||'  --  '||V.SALARIO||'  --  '||V_SALARIO||'  --  '|| V.FECHA);






     --IF V.DIA_LABORAL IN ('S','F','D') THEN
        V_CANT_DIA_NORMAL:= V_CANT_DIA_NORMAL +1;
        
        IF  TO_CHAR(TO_DATE(P_FECHA), 'MM') = 2 AND 28  = V_CANT_DIA_NORMAL THEN
          V_SALARIO := V_SALARIO + V_IMPORTE_DIA_SALARIO+V_IMPORTE_DIA_SALARIO+V_IMPORTE_DIA_SALARIO;
        --  raise_application_error(-20001,'sdfads');
          V_DIAS_TRABAJADOS := 30;
        ELSIF TO_CHAR(TO_DATE(P_FECHA), 'MM') = 2 AND 29 = V_CANT_DIA_NORMAL THEN
           V_SALARIO := V_SALARIO + V_IMPORTE_DIA_SALARIO+V_IMPORTE_DIA_SALARIO;
           V_DIAS_TRABAJADOS := 30;
        ELSIF V_CANT_DIA_NORMAL <31 THEN
            IF TO_CHAR(TO_DATE(P_FECHA), 'MM') <> 2 AND TO_CHAR(LAST_DAY(TO_DATE(P_FECHA)), 'DD') IN(29,28) THEN
              NULL;
            ELSE
              V_SALARIO := V_SALARIO + V_IMPORTE_DIA_SALARIO;
              V_DIAS_TRABAJADOS := V_CANT_DIA_NORMAL;
             END IF;
          ELSIF V_CANT_DIA_NORMAL =31  THEN
               V_DIAS_TRABAJADOS :=30;
        END IF;
   ---  END IF;
 DBMS_OUTPUT.put_line (X_DIAS_LABORAL||'  --  '||V_TOTAL_DIAS||'  --  '|| V_CANT_DIA_NORMAL||'  --  '|| V_IMPORTE_DIA_SALARIO||'  --  '||V.SALARIO||'  --  '||V_SALARIO);
--------------*********CONTROLAMOS TODOS LOS TIPOS DE AUSENCIAS QUE PUDO HABER TENIDO EN EL MES*********--------------
          IF X_DIAS_LABORAL IN ('L', 'R', 'P','U','A', 'W','C') THEN
            V_CANT_AUSENCIAS := V_CANT_AUSENCIAS +1;
            V_IMPORTE_AUSENCIA := V_IMPORTE_AUSENCIA+ V_IMPORTE_DIA_SALARIO;
          END IF;


--------------*********CONTROLAMOS LA VACACIONES Y EL IMPORTE POR DIA*********--------------

          IF V.DIA_LABORAL ='V' THEN
            V_CANT_VACACION := V_CANT_VACACION +1;

         /* BEGIN
            SELECT  A.EMPLV_IMP_DIA
              INTO V_IMP_DIA_VAC
              FROM PER_EMPLEADO_VACACION A
             WHERE EMPLV_ESTADO = 'C'
               AND A.EMPLV_EMPR = V.EMPRESA
               AND A.EMPLV_LEGAJO = V.LEGAJO
               AND V.FECHA BETWEEN EMPLV_FEC_DESDE AND EMPLV_FEC_HASTA;
           EXCEPTION
              WHEN NO_DATA_FOUND THEN
                V_IMPORTE_VACACION := 0;
              END;
                V_IMPORTE_VACACION := V_IMPORTE_VACACION + V_IMP_DIA_VAC;*/
         BEGIN
             SELECT B.PDDET_IMP
             INTO V_IMPORTE_VACACION
              FROM PER_DOCUMENTO A, PER_DOCUMENTO_DET B
             WHERE A.PDOC_CLAVE = B.PDDET_CLAVE_DOC
               AND A.PDOC_EMPR = B.PDDET_EMPR
               AND A.PDOC_EMPR =V.EMPRESA
               AND B.PDDET_CLAVE_CONCEPTO = 6
               AND TO_CHAR(A.PDOC_FEC,'MM/YYYY') = TO_CHAR(P_FECHA,'MM/YYYY')
               AND A.PDOC_EMPLEADO =  V.LEGAJO;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            V_IMPORTE_VACACION := 0;
          END;




          END IF;

--------------*********VERIFICAMOS SI LOS REPOSOS SE PAGAN(HASTA 2 DIAS)*********--------------
      --------------*********SE ABONA EL 50% DEL JORNAL DIARIO*********--------------
          /*IF V.DIA_LABORAL ='R' THEN
               V_CANT_REPOSO := V_CANT_REPOSO +1;
              BEGIN
                 SELECT  A.AUSE_CANT_DIAS
                   INTO V_CANT_REPO_PAGADO
                    FROM PER_AUSENCIAS_JUSTIFICADAS A, PER_TIPOS_AUSENC_JUST B
                   WHERE A.AUSE_EMPR = V.EMPRESA
                     AND A.AUSE_MOTIVO = 'R'
                     ---AND B.TIPAUS_MARC_PAGA = 'S'
                     AND TIPAUS_CLAVE = AUSE_TIPO
                     AND TIPAUS_EMPR = AUSE_EMPR
                     AND AUSE_EMPL_LEGAJO = V.LEGAJO
                     AND AUSE_ESTADO_APROB_ES = 'C'
                     AND A.AUSE_INDICADOR_HS = 'D'
                     AND V.FECHA BETWEEN A.AUSE_DESDE AND A.AUSE_HASTA;
               EXCEPTION WHEN NO_DATA_FOUND THEN
                   V_CANT_REPO_PAGADO :=0;
                END;

             IF V_CANT_REPO_PAGADO IN (1,2) THEN
                V_IMPORTE_REPOSO :=V_IMPORTE_REPOSO +(V_IMPORTE_DIA_SALARIO/2);
             END IF;


          END IF;*/

--------------*********TAMBIEN CONTROLAMOS LAS AUSENCIAS POR HORAS PARA DESCONTAR*********--------------

       BEGIN
             SELECT  nvl(SUM(a.ause_cant_hs),0)----NVL(SUM(AUSE_MONTO_DES), 0)
               INTO V_IMP_AUS_HOR
              FROM PER_AUSENCIAS_JUSTIFICADAS A, PER_TIPOS_AUSENC_JUST B
             WHERE A.AUSE_EMPR = V.EMPRESA
               AND A.AUSE_MOTIVO IN ('P', 'A')
               AND TIPAUS_CLAVE = AUSE_TIPO
               AND TIPAUS_EMPR = AUSE_EMPR
               AND AUSE_EMPL_LEGAJO = V_LEGAJO
               AND AUSE_ESTADO_APROB_ES = 'C'
               AND A.AUSE_INDICADOR_HS = 'H'
               AND V.FECHA  BETWEEN A.AUSE_DESDE AND  A.AUSE_HASTA;

            V_IMPORTE_AUS_HORA  := V_IMPORTE_AUS_HORA+(V_IMP_AUS_HOR*(V_IMPORTE_DIA_SALARIO/8));


     EXCEPTION
       WHEN NO_DATA_FOUND THEN
            V_IMP_AUS_HOR := 0;
       END;

   END IF;






  END LOOP;



--------------*********CALCULO PARA EL SALARIO BASE*********--------------
 
    V_SALARIO := V_SALARIO - (NVL(V_IMPORTE_AUS_HORA,0)+NVL(V_IMPORTE_AUSENCIA,0) +NVL(V_IMPORTE_VACACION,0));
--DBMS_OUTPUT.put_line (V_SALARIO||'*******'||NVL(V_IMPORTE_AUS_HORA,0)||'*'||NVL(V_IMPORTE_AUSENCIA,0)||'*'|| NVL(V_IMPORTE_VACACION,0));



      if TO_CHAR(LAST_DAY(TO_DATE(P_FECHA)),'DD') >30 then
        v_cant_dia_mes := 30;
      else
        v_cant_dia_mes := TO_CHAR(LAST_DAY(TO_DATE(P_FECHA)),'DD');
      end if;
      if V_CANT_DIA_NORMAL < v_cant_dia_mes then
            if  NVL(V_IMPORTE_VACACION,0) > m_salario_base then
               
                V_SALARIO := V_SALARIO - (NVL(V_IMPORTE_AUS_HORA,0)+NVL(V_IMPORTE_AUSENCIA,0) +NVL(V_IMPORTE_VACACION,0));
---DBMS_OUTPUT.put_line (V_SALARIO||'++++++'||NVL(V_IMPORTE_AUS_HORA,0)||'*'||NVL(V_IMPORTE_AUSENCIA,0)||'*'|| NVL(V_IMPORTE_VACACION,0));

            else
              
               V_SALARIO := ((V_DIAS_TRABAJADOS-(V_CANT_AUSENCIAS+V_CANT_VACACION))* V_IMPORTE_DIA_SALARIO)-V_IMPORTE_AUS_HORA;
           DBMS_OUTPUT.put_line (V_SALARIO||'-------'||NVL(V_IMPORTE_AUS_HORA,0)||'*'||NVL(V_IMPORTE_AUSENCIA,0)||'*'|| NVL(V_IMPORTE_VACACION,0));

       end if;
     end if;
 DBMS_OUTPUT.put_line (V_SALARIO||'*'||NVL(V_IMPORTE_AUS_HORA,0)||'*'||NVL(V_IMPORTE_AUSENCIA,0)||'*'|| NVL(V_IMPORTE_VACACION,0));


     -- V_SALARIO_SAL := V_SALARIO;
     IF  TO_CHAR(P_FECHA,'MM/YYYY') = '02/2021'   AND V_CANT_AUSENCIAS+V_CANT_VACACION = 28 THEN--;
        V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS -V_CANT_AUSENCIAS-V_CANT_VACACION-2;
     ELSE
         V_DIAS_TRABAJADOS := V_DIAS_TRABAJADOS -V_CANT_AUSENCIAS-V_CANT_VACACION;
     END IF;

     IF  V_DIAS_TRABAJADOS >0 then
       IF  TO_CHAR(P_FECHA,'MM/YYYY') = '02/2021'  AND V_CANT_AUSENCIAS+V_CANT_VACACION = 28 THEN

        V_CANT_DIAS := V_DIAS_TRABAJADOS-2;
       ELSE
         V_CANT_DIAS := V_DIAS_TRABAJADOS;
       END IF;

      IF V_SALARIO >0 THEN
       V_SALARIO_SAL := V_SALARIO;
        DBMS_OUTPUT.put_line ('B');
       ELSE
       DBMS_OUTPUT.put_line ('A');
        V_SALARIO_SAL := 0;
      END IF;
     ELSE
       V_CANT_DIAS := 0;
       V_SALARIO_SAL := 0;
     END IF;


--------------********* COLOR PARA EL PROGRAMA 17-1-155 *********--------------

    IF V_DIAS_TRABAJADOS = 30 AND V_CANT_AUSENCIAS = 0  AND V_IMPORTE_AUS_HORA  = 0  AND V_CANT_VACACION = 0  THEN
       V_MARC := 'N';
    ELSIF V_CANT_VACACION = 0 AND V_CANT_AUSENCIAS > 0 OR (V_CANT_VACACION = 0 AND V_IMPORTE_AUS_HORA > 0) THEN
       V_MARC := 'L';
    ELSIF V_CANT_VACACION > 0 AND V_CANT_AUSENCIAS = 0 AND V_IMPORTE_AUS_HORA = 0 THEN
       V_MARC := 'V';
    ELSIF V_CANT_VACACION > 0 AND V_CANT_AUSENCIAS > 0 OR (V_CANT_VACACION >0 AND V_IMPORTE_AUS_HORA > 0) THEN
       V_MARC := 'M';
    ELSIF V_TOTAL_DIAS <> TO_CHAR(LAST_DAY(SYSDATE), 'DD') THEN
      V_MARC := 'S';

    END IF;



--RAISE_APPLICATION_ERROR (-20001,V_DIAS_TRABAJADOS );



END;
/
