CREATE OR REPLACE TRIGGER "PER_EMPL_AI"
  after insert  or update on per_empleado
  for each row
declare
  v_perempdep_fec date;
begin
 IF INSERTING THEN
  select p.peri_fec_ini
    into v_perempdep_fec
    from per_periodo p
   where p.peri_codigo =
         (select a.conf_peri_act
            from per_configuracion a
           where a.conf_empr = :new.empl_empresa)
     and p.peri_empr = :new.empl_empresa;

  insert into per_empleado_depto
    (perempdep_empl,
     perempdep_depto,
     perempdep_fec,
     perempdep_suc,
     perempdep_empr)
  values
    (:new.empl_legajo,
     nvl(:new.empl_departamento, 1),
     v_perempdep_fec,
     nvl(:new.empl_sucursal, 1),
     :new.empl_empresa);


-- IF :NEW.EMPL_EMPRESA in(2, 1) THEN
     INSERT INTO PER_EMPL_PAGO_HIS
      (EMPL_LEGAJO,       
      EMPL_FORMA_PAGO,      
      EMPL_TIPO_PAGO,      
      EMPL_FECHA_MOD, 
      EMPL_EMPR,        
      EMPL_SALARIO, 
      EMPL_HS_EXTRAS,  
      EMPL_TIPO_COMIS, 
      EMPL_MARC_COMIS_SIST,
      EMPL_SUCURSAL, 
      EMPL_DEPARTAMENTO,
      EMPL_CARGO, 
      LOGIN,
      EMPL_IMP_HORA_N_D,
      EMPL_IMP_HORA_N_N,
      EMPL_IMP_HORA_E_D,
      EMPL_IMP_HORA_E_N,
      EMPL_IMP_HORA_DF_D,
      EMPL_IMP_HORA_DF_N,
      EMPL_IMP_HORA_E_M)
    VALUES
      (:NEW.EMPL_LEGAJO,  
      :NEW.EMPL_FORMA_PAGO, 
      :NEW.EMPL_TIPO_SALARIO, 
      NVL(:NEW.EMPL_FEC_INGRESO,SYSDATE),     
      :NEW.EMPL_EMPRESA, 
      :NEW.EMPL_SALARIO_BASE, 
      :NEW.EMPL_IND_HORA_EXTRA,  
      :NEW.EMPL_TIPO_COMIS, 
      :NEW.EMPL_MARC_COMIS_SIST, 
      :NEW.EMPL_SUCURSAL,
      :NEW.EMPL_DEPARTAMENTO,
      :NEW.EMPL_CARGO, 
      gen_devuelve_user,
      :NEW.EMPL_IMP_HORA_N_D,
      :NEW.EMPL_IMP_HORA_N_N,
      :NEW.EMPL_IMP_HORA_E_D,
      :NEW.EMPL_IMP_HORA_E_N,
      :NEW.EMPL_IMP_HORA_DF_D,
      :NEW.EMPL_IMP_HORA_DF_N,
      :NEW.EMPL_IMP_HORA_E_M);
 --END IF;

 ELSIF UPDATING  THEN ------LV 27/05/2020

 IF :NEW.EMPL_DEPARTAMENTO <> :OLD.EMPL_DEPARTAMENTO  THEN

 DELETE PER_EMPLEADO_DEPTO
 WHERE TRUNC(PEREMPDEP_FEC) = TRUNC(SYSDATE)
   AND PEREMPDEP_EMPL = :NEW.EMPL_LEGAJO
   AND PEREMPDEP_EMPR = :NEW.EMPL_EMPRESA;


   INSERT INTO PER_EMPLEADO_DEPTO
    (PEREMPDEP_EMPL,
     PEREMPDEP_DEPTO,
     PEREMPDEP_FEC,
     PEREMPDEP_SUC,
     PEREMPDEP_EMPR)
  VALUES
    (:NEW.EMPL_LEGAJO,
     NVL(:NEW.EMPL_DEPARTAMENTO, 1),
     SYSDATE,
     NVL(:NEW.EMPL_SUCURSAL, 1),
     :NEW.EMPL_EMPRESA);

 END IF;


        --IF :NEW.EMPL_EMPRESA in (1,2) THEN


             IF :OLD.EMPL_FORMA_PAGO <> :NEW.EMPL_FORMA_PAGO
               OR :OLD.EMPL_TIPO_SALARIO <> :NEW.EMPL_TIPO_SALARIO
               OR :NEW.EMPL_IND_HORA_EXTRA <> :OLD.EMPL_IND_HORA_EXTRA
               OR :NEW.EMPL_TIPO_COMIS <> :OLD.EMPL_TIPO_COMIS
               OR :NEW.EMPL_MARC_COMIS_SIST <>  :OLD.EMPL_MARC_COMIS_SIST 
               OR :OLD.EMPL_SITUACION <> :NEW.EMPL_SITUACION
               OR :OLD.EMPL_IMP_HORA_N_D <> :NEW.EMPL_IMP_HORA_N_D
               THEN

               IF :NEW.EMPL_SITUACION <> 'I' THEN
                           DELETE PER_EMPL_PAGO_HIS
                             WHERE EMPL_LEGAJO = :NEW.EMPL_LEGAJO
                               AND TRUNC(EMPL_FECHA_MOD) = TRUNC(SYSDATE)
                               AND EMPL_EMPR = :NEW.EMPL_EMPRESA;

                    INSERT INTO PER_EMPL_PAGO_HIS SS
                         (EMPL_LEGAJO, 
                          EMPL_FORMA_PAGO, 
                          EMPL_TIPO_PAGO, 
                          EMPL_FECHA_MOD, 
                          EMPL_EMPR, 
                          EMPL_SALARIO, 
                          EMPL_HS_EXTRAS,
                          EMPL_TIPO_COMIS, 
                          EMPL_MARC_COMIS_SIST,
                          EMPL_SUCURSAL, 
                          EMPL_DEPARTAMENTO,
                          EMPL_CARGO, 
                          LOGIN,
                          EMPL_IMP_HORA_N_D,
                          EMPL_IMP_HORA_N_N,
                          EMPL_IMP_HORA_E_D,
                          EMPL_IMP_HORA_E_N,
                          EMPL_IMP_HORA_DF_D,
                          EMPL_IMP_HORA_DF_N,
                          EMPL_IMP_HORA_E_M)
                       VALUES
                         ( :NEW.EMPL_LEGAJO, 
                           :NEW.EMPL_FORMA_PAGO, 
                           :NEW.EMPL_TIPO_SALARIO,
                           SYSDATE, 
                           :NEW.EMPL_EMPRESA, 
                           :NEW.EMPL_SALARIO_BASE,
                           :NEW.EMPL_IND_HORA_EXTRA, 
                           :NEW.EMPL_TIPO_COMIS, 
                           :NEW.EMPL_MARC_COMIS_SIST, 
                           :NEW.EMPL_SUCURSAL,
                           :NEW.EMPL_DEPARTAMENTO,
                           :NEW.EMPL_CARGO, 
                           gen_devuelve_user,
                           :NEW.EMPL_IMP_HORA_N_D,
                           :NEW.EMPL_IMP_HORA_N_N,
                           :NEW.EMPL_IMP_HORA_E_D,
                           :NEW.EMPL_IMP_HORA_E_N,
                           :NEW.EMPL_IMP_HORA_DF_D,
                           :NEW.EMPL_IMP_HORA_DF_N,
                           :NEW.EMPL_IMP_HORA_E_M);
             END IF;
         END IF;

       --END IF;


 END IF;

end PER_EMPL_BI;
/
