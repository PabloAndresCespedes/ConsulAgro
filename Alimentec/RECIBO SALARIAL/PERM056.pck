create or replace package PERM056 is

  -- Author  : PRO4
  -- Created : 24/05/2021 10:14:32
  -- Purpose : PERM056

  PROCEDURE PP_BUSCAR_CONCEPTO(I_EMPRESA  IN NUMBER,
                               I_CONCEPTO IN NUMBER,
                               O_CON_DESC OUT VARCHAR2);

  PROCEDURE PP_BUSCAR_FORMA_PAGO(I_EMPRESA    IN NUMBER,
                                 I_FORMA_PAGO IN NUMBER,
                                 O_FP_DESCR   OUT VARCHAR2);

  PROCEDURE PP_BUSCAR_SUCURSAL(I_EMPRESA    IN NUMBER,
                               I_SUC_CODIGO IN NUMBER,
                               O_SUC_dESC   OUT VARCHAR2);

  PROCEDURE PP_BUSCAR_MONEDA(I_EMPRESA    IN NUMBER,
                             I_MON_CODIGO IN NUMBER,
                             O_MON_DESC   OUT VARCHAR2);

  PROCEDURE PP_BUSCAR_BANCO(I_EMPRESA    IN NUMBER,
                            I_BCO_CODIGO IN NUMBER,
                            O_BCO_DESC   OUT VARCHAR2);

  PROCEDURE PP_CARGAR_EMPLEADO(I_EMPRESA    IN NUMBER,
                               I_CONCEPTO   IN NUMBER,
                               I_SUC_CODIGO IN NUMBER,
                               I_MON_CODIGO IN NUMBER,
                               I_FORMA_PAGO IN NUMBER,
                               I_GRUPOS     IN NUMBER,
                               I_BANCO      IN NUMBER,
                               I_ORDEN      IN NUMBER,
                               O_TOTAL      OUT VARCHAR2);

  PROCEDURE PP_ELIMINAR_CON_EMPL(I_EMPLEADO IN NUMBER,
                                 I_CONCEPTO IN NUMBER,
                                 I_SEQ_ID   IN NUMBER,
                                 I_EMPRESA  IN NUMBER,
                                 I_ELIMINAR IN VARCHAR2,
                                 O_TOTAL    OUT VARCHAR2);

  PROCEDURE PP_ELIMINAR_CONCEPTO(I_CONCEPTO IN NUMBER,
                                 I_EMPRESA  IN NUMBER,
                                 I_SUCURSAL IN NUMBER,
                                 O_TOTAL    OUT VARCHAR2);

  PROCEDURE PP_BUSCAR_EMPLEADOS(I_EMPRESA    IN NUMBER,
                                I_LEGAJO     IN NUMBER,
                                I_SUCURSAL   IN NUMBER,
                                I_CONCEPTO   IN NUMBER,
                                I_FORMA_PAGO IN NUMBER,
                                O_NOMBRE     OUT VARCHAR2,
                                O_DPTO       OUT NUMBER,
                                O_DPTO_DESC  OUT VARCHAR2,
                                O_FORMA_PAGO OUT NUMBER,
                                O_FORMA_DEC  OUT VARCHAR2,
                                O_IMPORTE    OUT NUMBER,
                                O_COLOR      OUT VARCHAR2,
                                O_CANT_DIAS  OUT NUMBER,
                                O_AGREGAR    OUT VARCHAR2);

  PROCEDURE PP_AGREGAR_EMPLEADO(I_EMPRESA     IN NUMBER,
                                I_EMPLEADO    IN NUMBER,
                                I_SUCURSAL    IN NUMBER,
                                I_CONCEPTO    IN NUMBER,
                                I_IMPORTE     IN VARCHAR2,
                                I_FORMA_PAGO  IN NUMBER,
                                I_DPTO        IN NUMBER,
                                I_DPTO_DESC   IN VARCHAR2,
                                I_EMPL_NOMBRE IN VARCHAR2,
                                I_MONEDA      IN NUMBER,
                                I_COLOR       IN VARCHAR2,
                                I_CANT_DIAS   IN NUMBER,
                                I_SEQ_ID      IN NUMBER,
                                I_BANCO       IN NUMBER,
                                O_TOTAL       OUT VARCHAR2);

  PROCEDURE PP_GUARDAR_DATOS(I_EMPRESA  IN NUMBER,
                             I_SUCURSAL IN NUMBER,
                             I_CONCEPTO IN NUMBER,
                             I_MONEDA   IN NUMBER,
                             I_BANCO    IN NUMBER);

end PERM056;
/
CREATE OR REPLACE PACKAGE BODY PERM056 IS

PROCEDURE PP_BUSCAR_CONCEPTO  (I_EMPRESA  IN NUMBER,
                               I_CONCEPTO IN NUMBER,
                               O_CON_DESC OUT VARCHAR2) IS



BEGIN
    SELECT PCON_DESC
      INTO O_CON_DESC
      FROM PER_CONCEPTO
     WHERE PCON_CLAVE = I_CONCEPTO
       AND PCON_EMPR = I_EMPRESA;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
       RAISE_APPLICATION_ERROR (-20001,'El concepto no existe');
END PP_BUSCAR_CONCEPTO;



PROCEDURE PP_BUSCAR_FORMA_PAGO  (I_EMPRESA    IN NUMBER,
                                 I_FORMA_PAGO IN NUMBER,
                                 O_FP_DESCR   OUT VARCHAR2) IS

BEGIN
    SELECT FORMA_DESC
      INTO O_FP_DESCR
      FROM PER_FORMA_PAGO
     WHERE FORMA_CODIGO = I_FORMA_PAGO
       AND FORMA_EMPR = I_EMPRESA;

EXCEPTION
    WHEN NO_DATA_FOUND THEN
        NULL;
END PP_BUSCAR_FORMA_PAGO;


PROCEDURE PP_BUSCAR_SUCURSAL (I_EMPRESA    IN NUMBER,
                              I_SUC_CODIGO IN NUMBER,
                              O_SUC_dESC   OUT VARCHAR2) IS

BEGIN
  SELECT SUC_DESC
    INTO O_SUC_dESC
    FROM GEN_SUCURSAL
   WHERE SUC_CODIGO = I_SUC_CODIGO
     AND SUC_EMPR = I_EMPRESA;

EXCEPTION
    WHEN NO_DATA_FOUND THEN
       RAISE_APPLICATION_ERROR(-20001, 'No existe sucursal');

END PP_BUSCAR_SUCURSAL;



PROCEDURE PP_BUSCAR_MONEDA (I_EMPRESA    IN NUMBER,
                            I_MON_CODIGO IN NUMBER,
                            O_MON_DESC   OUT VARCHAR2) IS


BEGIN
  SELECT MON_DESC
    INTO O_MON_DESC
    FROM GEN_MONEDA
   WHERE MON_CODIGO = I_MON_CODIGO
     AND MON_EMPR = I_EMPRESA;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
     RAISE_APPLICATION_ERROR(-20001, 'No existe la moneda');

END PP_BUSCAR_MONEDA;


PROCEDURE PP_BUSCAR_BANCO (I_EMPRESA    IN NUMBER,
                           I_BCO_CODIGO IN NUMBER,
                           O_BCO_DESC   OUT VARCHAR2) IS

BEGIN
  SELECT BCO_DESC
    INTO O_BCO_DESC
    FROM FIN_BANCO
   WHERE BCO_EMPR = I_EMPRESA
     AND BCO_CODIGO = I_BCO_CODIGO;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RAISE_APPLICATION_ERROR(-20001,'El banco no existe!');

END PP_BUSCAR_BANCO;



PROCEDURE PP_CARGAR_EMPLEADO(I_EMPRESA        IN NUMBER,
                             I_CONCEPTO       IN NUMBER,
                             I_SUC_CODIGO     IN NUMBER,
                             I_MON_CODIGO     IN NUMBER,
                             I_FORMA_PAGO     IN NUMBER,
                             I_GRUPOS         IN NUMBER,
                             I_BANCO          IN NUMBER,
                             I_ORDEN          IN NUMBER,
                             O_TOTAL          OUT VARCHAR2) IS

V_EXISTE NUMBER;
TYPE CURTYP IS REF CURSOR;
C_PERM056   CURTYP;

V_SQL   VARCHAR2(30000);
V_WHERE VARCHAR2(30000);
V_ORDEN VARCHAR2(200);

TYPE CUR_TYPE IS RECORD(EMPRESA           NUMBER,
                        LEGAJO            NUMBER,
                        CONCEPTO          NUMBER,
                        CONCEPTO_DESC     VARCHAR2(500),
                        IMPORTE           NUMBER,
                        FORMA_PAGO        NUMBER,
                        DPTO_CODIGO       NUMBER,
                        NOMBRE_EMPL       VARCHAR2(500),
                        EMPL_SUC          NUMBER,
                        MONEDA            NUMBER,
                        DPTO_GRUPO_PAGO   NUMBER,
                        DPTO_DESC         VARCHAR2(500),
                        PERCON_SUC        NUMBER,
                        PERCON_USER       VARCHAR2(500),
                        PERCON_FEC        DATE,
                        PERCON_IND_COLOR  VARCHAR2(1),
                        PERCON_CANT_DIAS  NUMBER,
                        PERCON_BCO        NUMBER,
                        ADEL_PROV         VARCHAR2(500),
                        CAMBIO_FP         VARCHAR2(1),
                        FEC_INGRESO       DATE,
                        PERIODO         NUMBER);
                        K CUR_TYPE;

  V_COLOR               VARCHAR2(2);
  V_SALARIO_BASE        NUMBER :=0;
  V_LISTA               VARCHAR2(2);
  V_CANT_DIAS           NUMBER:=0;
  USUARIO               VARCHAR2(50);
  X_CAMB_FORMA_PAGO     VARCHAR2(20);
  W_PERIODO             NUMBER;
BEGIN

    IF I_CONCEPTO IS NULL THEN
        RAISE_APPLICATION_ERROR(-20015, 'EL concepto de pago no puede quedar nulo!');
    END IF;
    IF I_SUC_CODIGO IS NULL THEN
        RAISE_APPLICATION_ERROR(-20015, 'Codigo Sucursal no puede quedar nulo!');
    END IF;
    IF I_MON_CODIGO IS NULL THEN
        RAISE_APPLICATION_ERROR(-20015, 'Codigo Moneda no puede quedar nulo!');
    END IF;

      APEX_COLLECTION.CREATE_OR_TRUNCATE_COLLECTION (P_COLLECTION_NAME =>'CONCEPTOS_FIJOS');

         SELECT COUNT(*)
           INTO V_EXISTE
           FROM PER_EMPL_CONC A
          WHERE PERCON_CONCEPTO = I_CONCEPTO
            AND PERCON_MONEDA   = I_MON_CODIGO
            AND PERCON_EMPR     = I_EMPRESA
            AND PERCON_SUC      = I_SUC_CODIGO;
            
  USUARIO := gen_devuelve_user ;

  SELECT P.PERI_CODIGO
     INTO W_PERIODO
    FROM PER_PERIODO P
    WHERE TRUNC(SYSDATE) BETWEEN P.PERI_FEC_INI AND P.PERI_FEC_FIN
      AND P.PERI_EMPR = I_EMPRESA;
  
   IF I_EMPRESA != 2 then -- DISTINTO A TRANSAGRO
         IF V_EXISTE > 0  THEN

                      V_WHERE := V_WHERE ||' AND PERCON_CONCEPTO =' || I_CONCEPTO || ' ';
                      V_WHERE := V_WHERE || 'AND EMPL_SUCURSAL = '  || I_SUC_CODIGO || ' ';
                      V_WHERE := V_WHERE || 'AND PERCON_MONEDA = '  || I_MON_CODIGO || ' ';

                     IF I_BANCO IS NOT NULL THEN
                       V_WHERE := V_WHERE || ' AND PERCON_BCO = ' || I_BANCO|| ' ';
                     END IF;

                     IF I_FORMA_PAGO IS NOT NULL THEN
                       V_WHERE := V_WHERE || ' AND EMPL_FORMA_PAGO = ' || I_FORMA_PAGO|| ' ';
                     END IF;
                     IF I_GRUPOS <>3 THEN
                       V_WHERE := V_WHERE || ' AND DPTO_GRUPO_PAGO = ' ||I_GRUPOS|| ' ';
                     END IF;

                     IF I_ORDEN = 1 THEN
                        V_ORDEN := V_ORDEN || 'ORDER BY EMPL_NOMBRE ASC, EMPL_NOMBRE';
                     ELSE
                        V_ORDEN := V_ORDEN || 'ORDER BY DPTO_DESC ASC, EMPL_NOMBRE';

                     END IF;
                      
                     if I_EMPRESA = 1 then --@Hila
                      V_SQL := 'SELECT T.PERCON_EMPR,
                                       T.PERCON_EMPLEADO,
                                       T.PERCON_CONCEPTO,
                                       T.PCON_DESC,
                                       T.PERCON_IMP,
                                       T.EMPL_FORMA_PAGO,
                                       T.EMPL_DEPARTAMENTO,
                                       T.EMPL_NOMBRE,
                                       T.EMPL_SUCURSAL,
                                       T.PERCON_MONEDA,
                                       T.DPTO_GRUPO_PAGO,
                                       T.DPTO_DESC,
                                       T.PERCON_SUC,
                                       T.PERCON_USER,
                                       T.PERCON_FEC,
                                       T.PERCON_IND_COLOR,
                                       T.PERCON_CANT_DIAS,
                                       T.PERCON_BCO,
                                       NULL ADEL_PROV,
                                       T.PERCON_CAM_FP CAMBIO_FP,
                                       NULL,
                                       T.PERCON_PERIODO
                                 FROM PER_EMPL_CONC_V T
                                WHERE T.PERCON_EMPR =   '||I_EMPRESA||'
                                '||V_WHERE||'' ||V_ORDEN;
                      else --@holding diferente a HILA y TAGRO
                        
                        -- 10/08/2022 14:04:55 @PabloACespedes \(^-^)/
                        -- esta vista se_basa en la tabla de historico de pagos del empleado
                        -- tener en cuenta estos registros
                        V_SQL := 'SELECT T.PERCON_EMPR,
                                       T.PERCON_EMPLEADO,
                                       T.PERCON_CONCEPTO,
                                       T.PCON_DESC,
                                       T.PERCON_IMP,
                                       T.EMPL_FORMA_PAGO,
                                       T.EMPL_DEPARTAMENTO,
                                       T.EMPL_NOMBRE,
                                       T.EMPL_SUCURSAL,
                                       T.PERCON_MONEDA,
                                       T.DPTO_GRUPO_PAGO,
                                       T.DPTO_DESC,
                                       T.PERCON_SUC,
                                       T.PERCON_USER,
                                       T.PERCON_FEC,
                                       T.PERCON_IND_COLOR,
                                       T.PERCON_CANT_DIAS,
                                       T.PERCON_BCO,
                                       NULL ADEL_PROV,
                                       T.PERCON_CAM_FP CAMBIO_FP,
                                       NULL,
                                       T.PERCON_PERIODO
                                 FROM PER_EMPL_CONC_HOLDING_V T
                                WHERE T.PERCON_EMPR =   '||I_EMPRESA||'
                                '||V_WHERE||'' ||V_ORDEN;
                        
                      end if;
                      
                     OPEN C_PERM056 FOR V_SQL;

                       LOOP
                            FETCH C_PERM056
                            INTO K;
                            EXIT WHEN C_PERM056%NOTFOUND;


                     APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'CONCEPTOS_FIJOS',
                                                           P_C001 => K.EMPRESA,
                                                           P_C002 => K.LEGAJO,
                                                           P_C003 => K.CONCEPTO,
                                                           P_C004 => K.CONCEPTO_DESC,
                                                           P_C005 => K.IMPORTE,
                                                           P_C006 => K.FORMA_PAGO,
                                                           P_C007 => K.DPTO_CODIGO,
                                                           P_C008 => K.NOMBRE_EMPL,
                                                           P_C009 => K.EMPL_SUC,
                                                           P_C010 => K.MONEDA,
                                                           P_C011 => K.DPTO_GRUPO_PAGO,
                                                           P_C012 => K.DPTO_DESC,
                                                           P_C013 => K.PERCON_SUC,
                                                           P_C014 => K.PERCON_USER,
                                                           P_C015 => K.PERCON_FEC,
                                                           P_C016 => K.PERCON_IND_COLOR,
                                                           P_C017 => K.PERCON_CANT_DIAS,
                                                           P_C018 => K.PERCON_BCO,
                                                           P_C019 => K.ADEL_PROV,
                                                           P_C020 => K.CAMBIO_FP,
                                                           P_C021 => K.PERIODO);
                  END LOOP;

         ELSE
           IF I_CONCEPTO = 2 then -->SALARIO

                      V_WHERE := V_WHERE || 'AND EMPL_SUCURSAL = '  || I_SUC_CODIGO || ' ';
                      V_WHERE := V_WHERE || 'AND EMPL_MON_PAGO = '  || I_MON_CODIGO || ' ';

                     IF I_BANCO IS NOT NULL THEN
                       V_WHERE := V_WHERE || ' AND EMPL_BCO_PAGO = ' || I_BANCO|| ' ';
                     END IF;

                    /* IF I_FORMA_PAGO IS NOT NULL THEN
                       V_WHERE := V_WHERE || ' AND EMPL_FORMA_PAGO = ' || I_FORMA_PAGO|| ' ';
                     END IF;*/
                     IF I_GRUPOS <> 3 THEN
                       V_WHERE := V_WHERE || ' AND DPTO_GRUPO_PAGO = ' ||I_GRUPOS|| ' ';
                     END IF;

                     IF I_ORDEN = 1 THEN
                        V_ORDEN := V_ORDEN || 'ORDER BY EMPL_NOMBRE ASC, EMPL_NOMBRE';
                     ELSE
                        V_ORDEN := V_ORDEN || 'ORDER BY DPTO_DESC ASC, EMPL_NOMBRE';

                     END IF;

           V_SQL := 'SELECT '||I_EMPRESA||' ,
                            EMPL_LEGAJO,
                            '||I_CONCEPTO||',
                            NULL CON_DESC,
                            NULL IMPORTE,
                            EMPL_FORMA_PAGO,
                            EMPL_DEPARTAMENTO,
                            EMPL_NOMBRE||'' ''||EMPL_APE NOMBRE,
                            EMPL_SUCURSAL,
                            EMPL_MON_PAGO,
                            DPTO_GRUPO_PAGO,
                            DPTO_DESC,
                            '||I_SUC_CODIGO||',
                            NULL USURIO,
                            NULL FECHA,
                            NULL IND_COLOR,
                            NULL CANT_DIAS,
                            EMPL_BCO_PAGO,
                            NULL ADEL_PROV,
                            NULL CAMBIO_FP,
                            EMPL_FEC_INGRESO,
                            NULL PERIODO
                      FROM PER_EMPLEADO, GEN_DEPARTAMENTO
                     WHERE EMPL_SITUACION = ''A''
                       AND EMPL_DEPARTAMENTO = DPTO_CODIGO
                       AND EMPL_EMPRESA = DPTO_EMPR
                      -- AND EMPL_TIPO_SALARIO = 1
                       AND EMPL_EMPRESA =  '||I_EMPRESA||'
                        '||V_WHERE||'' ||V_ORDEN;

           -- delete from x where x.otro='CONC_FIJOS_PACR';
          --  INSERT INTO X (CAMPO1,Otro) VALUES (V_SQL,'CONC_FIJOS_PACR');
       
                    OPEN C_PERM056 FOR V_SQL;

                       LOOP
                            FETCH C_PERM056
                            INTO K;
                            EXIT WHEN C_PERM056%NOTFOUND;
                --

                    PER_CALCULA_SAL_BASE_PRO2(V_LEGAJO      => K.LEGAJO,      -- In
                                              V_EMPRESA     => I_EMPRESA,     -- In
                                              V_MARC        => V_COLOR,       -- out
                                              V_SALARIO_SAL => V_SALARIO_BASE,-- out
                                              V_INCLUIR     => V_LISTA,       -- out
                                              V_FORMA_PAGO  => 2,             --> MENSUAL -- in
                                              V_CANT_DIAS   => V_CANT_DIAS    -- out
                                              );

                      IF V_LISTA  = 'S'  THEN
                          IF  TO_CHAR(K.FEC_INGRESO,'MM/YYYY') <> TO_CHAR(SYSDATE-5,'MM/YYYY') AND LTRIM(RTRIM(V_COLOR)) = 'S' THEN
                              X_CAMB_FORMA_PAGO := 'S';
                           ELSE
                              X_CAMB_FORMA_PAGO := 'N';

                          END IF;


                             APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'CONCEPTOS_FIJOS',
                                                                 P_C001 => I_EMPRESA,
                                                                 P_C002 => K.LEGAJO,
                                                                 P_C003 => K.CONCEPTO,
                                                                 P_C004 => K.CONCEPTO_DESC,
                                                                 P_C005 => NVL(ROUND(V_SALARIO_BASE),0),
                                                                 P_C006 => 2,--K.FORMA_PAGO,
                                                                 P_C007 => K.DPTO_CODIGO,
                                                                 P_C008 => K.NOMBRE_EMPL,
                                                                 P_C009 => K.EMPL_SUC,
                                                                 P_C010 => K.MONEDA,
                                                                 P_C011 => K.DPTO_GRUPO_PAGO,
                                                                 P_C012 => K.DPTO_DESC,
                                                                 P_C013 => K.PERCON_SUC,
                                                                 P_C014 => USUARIO,
                                                                 P_C015 => SYSDATE,
                                                                 P_C016 => LTRIM(RTRIM(V_COLOR)),
                                                                 P_C017 => V_CANT_DIAS,
                                                                 P_C018 => K.PERCON_BCO,
                                                                 P_C019 => K.ADEL_PROV,
                                                                 P_C020 => X_CAMB_FORMA_PAGO,
                                                                 P_C021 => W_PERIODO);

                       END IF;
             END LOOP;
             
           END IF;

         END IF;
         ------------------------TRANSAGRO--------------------------
   ELSE

           IF V_EXISTE > 0  THEN
                      V_WHERE := V_WHERE ||' AND PERCON_CONCEPTO =' || I_CONCEPTO || ' ';
                      V_WHERE := V_WHERE || 'AND EMPL_SUCURSAL = '  || I_SUC_CODIGO || ' ';
                      V_WHERE := V_WHERE || 'AND PERCON_MONEDA = '  || I_MON_CODIGO || ' ';



                     IF I_FORMA_PAGO IS NOT NULL THEN
                       V_WHERE := V_WHERE || ' AND EMPL_FORMA_PAGO = ' || I_FORMA_PAGO|| ' ';
                       V_WHERE := V_WHERE || ' AND EMPL_FORMA_PAGO = ' || I_FORMA_PAGO|| ' ';
                     END IF;
                     IF I_GRUPOS <>3 THEN
                       V_WHERE := V_WHERE || ' AND DPTO_GRUPO_PAGO = ' ||I_GRUPOS|| ' ';
                     END IF;

                     IF I_ORDEN = 1 THEN
                        V_ORDEN := V_ORDEN || 'ORDER BY EMPL_NOMBRE ASC, EMPL_NOMBRE';
                     ELSE
                        V_ORDEN := V_ORDEN || 'ORDER BY DPTO_DESC ASC, EMPL_NOMBRE';

                     END IF;

                      V_SQL := 'SELECT T.PERCON_EMPR,
                                       T.PERCON_EMPLEADO,
                                       T.PERCON_CONCEPTO,
                                       T.PCON_DESC,
                                       T.PERCON_IMP,
                                       T.EMPL_FORMA_PAGO,
                                       T.EMPL_DEPARTAMENTO,
                                       T.EMPL_NOMBRE,
                                       T.EMPL_SUCURSAL,
                                       T.PERCON_MONEDA,
                                       T.DPTO_GRUPO_PAGO,
                                       T.DPTO_DESC,
                                       T.PERCON_SUC,
                                       T.PERCON_USER,
                                       T.PERCON_FEC,
                                       T.PERCON_IND_COLOR,
                                       T.PERCON_CANT_DIAS,
                                       T.PERCON_BCO,
                                       NULL ADEL_PROV,
                                       T.PERCON_CAM_FP CAMBIO_FP,
                                       NULL,
                                       PERCON_PERIODO
                                 FROM PER_EMPL_CONC_V_TAGRO T
                                WHERE T.PERCON_EMPR =   '||I_EMPRESA||'
                                '||V_WHERE||'' ||V_ORDEN;

             --INSERT INTO X (CAMPO1,Otro) VALUES (V_SQL,'MARCOS ANTONIO SOLIS');
             -- COMMIT;
                     OPEN C_PERM056 FOR V_SQL;

                       LOOP
                            FETCH C_PERM056
                            INTO K;
                            EXIT WHEN C_PERM056%NOTFOUND;


                     APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'CONCEPTOS_FIJOS',
                                                           P_C001 => K.EMPRESA,
                                                           P_C002 => K.LEGAJO,
                                                           P_C003 => K.CONCEPTO,
                                                           P_C004 => K.CONCEPTO_DESC,
                                                           P_C005 => K.IMPORTE,
                                                           P_C006 => K.FORMA_PAGO,
                                                           P_C007 => K.DPTO_CODIGO,
                                                           P_C008 => K.NOMBRE_EMPL,
                                                           P_C009 => K.EMPL_SUC,
                                                           P_C010 => K.MONEDA,
                                                           P_C011 => K.DPTO_GRUPO_PAGO,
                                                           P_C012 => K.DPTO_DESC,
                                                           P_C013 => K.PERCON_SUC,
                                                           P_C014 => K.PERCON_USER,
                                                           P_C015 => K.PERCON_FEC,
                                                           P_C016 => K.PERCON_IND_COLOR,
                                                           P_C017 => K.PERCON_CANT_DIAS,
                                                           P_C018 => K.PERCON_BCO,
                                                           P_C019 => K.ADEL_PROV,
                                                           P_C020 => K.CAMBIO_FP,
                                                           P_C021 => K.PERIODO);
                  END LOOP;


           ELSE

               IF I_CONCEPTO = 2 THEN

                      V_WHERE := V_WHERE || 'AND EMPL_SUCURSAL = '  || I_SUC_CODIGO || ' ';
                      V_WHERE := V_WHERE || 'AND EMPL_MON_PAGO = '  || I_MON_CODIGO || ' ';



                    IF I_FORMA_PAGO IS NOT NULL THEN
                       V_WHERE := V_WHERE || ' AND EMPL_FORMA_PAGO = ' || I_FORMA_PAGO|| ' ';
                     END IF;
                     IF I_GRUPOS <>3 THEN
                       V_WHERE := V_WHERE || ' AND DPTO_GRUPO_PAGO = ' ||I_GRUPOS|| ' ';
                     END IF;

                     IF I_ORDEN = 1 THEN
                        V_ORDEN := V_ORDEN || 'ORDER BY EMPL_NOMBRE ASC, EMPL_NOMBRE';
                     ELSE
                        V_ORDEN := V_ORDEN || 'ORDER BY DPTO_DESC ASC, EMPL_NOMBRE';

                     END IF;




           V_SQL := 'SELECT '||I_EMPRESA||' ,
                            EMPL_LEGAJO,
                            '||I_CONCEPTO||',
                            NULL CON_DESC,
                            NULL IMPORTE,
                            EMPL_FORMA_PAGO,
                            EMPL_DEPARTAMENTO,
                            EMPL_NOMBRE||'' ''||EMPL_APE NOMBRE,
                            EMPL_SUCURSAL,
                            EMPL_MON_PAGO,
                            DPTO_GRUPO_PAGO,
                            DPTO_DESC,
                            '||I_SUC_CODIGO||',
                            NULL USURIO,
                            NULL FECHA,
                            NULL IND_COLOR,
                            NULL CANT_DIAS,
                            EMPL_BCO_PAGO,
                            NULL ADEL_PROV,
                            NULL CAMBIO_FP,
                            EMPL_FEC_INGRESO,
                            NULL PERIODO
                      FROM PER_EMPLEADO, GEN_DEPARTAMENTO
                     WHERE EMPL_SITUACION = ''A''
                       AND EMPL_DEPARTAMENTO = DPTO_CODIGO
                       AND EMPL_EMPRESA = DPTO_EMPR
                       AND EMPL_TIPO_SALARIO IN (1)--,5)
                       AND EMPL_EMPRESA =  '||I_EMPRESA||'
                        '||V_WHERE||'' ||V_ORDEN;


           -- INSERT INTO X (CAMPO1,Otro) VALUES (V_SQL,'chayanne');
          --   COMMIT;
                    OPEN C_PERM056 FOR V_SQL;

                       LOOP
                            FETCH C_PERM056
                            INTO K;
                            EXIT WHEN C_PERM056%NOTFOUND;
               DELETE PER_PERM056_DET
              WHERE LEGAJO = K.LEGAJO
                AND EMPR = I_EMPRESA;

            --        PER_CALCULA_SAL_BASE_PRO (K.LEGAJO,I_EMPRESA,V_COLOR,V_SALARIO_BASE);
                    PER_CALCULA_SAL_BASE_PRO2 (K.LEGAJO,I_EMPRESA,V_COLOR,V_SALARIO_BASE,V_LISTA,2,V_CANT_DIAS);





                                       APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'CONCEPTOS_FIJOS',
                                                                           P_C001 => I_EMPRESA,
                                                                           P_C002 => K.LEGAJO,
                                                                           P_C003 => K.CONCEPTO,
                                                                           P_C004 => K.CONCEPTO_DESC,
                                                                           P_C005 => NVL(ROUND(V_SALARIO_BASE),0),
                                                                           P_C006 => K.FORMA_PAGO,
                                                                           P_C007 => K.DPTO_CODIGO,
                                                                           P_C008 => K.NOMBRE_EMPL,
                                                                           P_C009 => K.EMPL_SUC,
                                                                           P_C010 => K.MONEDA,
                                                                           P_C011 => K.DPTO_GRUPO_PAGO,
                                                                           P_C012 => K.DPTO_DESC,
                                                                           P_C013 => K.PERCON_SUC,
                                                                           P_C014 => USUARIO,
                                                                           P_C015 => SYSDATE,
                                                                           P_C016 => LTRIM(RTRIM(V_COLOR)),
                                                                           P_C017 => V_CANT_DIAS,
                                                                           P_C018 => K.PERCON_BCO,
                                                                           P_C019 => K.ADEL_PROV,
                                                                           P_C020 => X_CAMB_FORMA_PAGO,
                                                                           P_C021 => W_PERIODO);
                    END LOOP;
               END IF;
           END IF;



  END IF;


    SELECT 'TOTAL: '||TO_CHAR(NVL(SUM(C005),0),'999G999G999G999G999') IMPORTE
      INTO O_TOTAL
      FROM APEX_COLLECTIONS
     WHERE COLLECTION_NAME = 'CONCEPTOS_FIJOS';


END PP_CARGAR_EMPLEADO;

PROCEDURE PP_ELIMINAR_CON_EMPL (I_EMPLEADO IN NUMBER,
                              I_CONCEPTO IN NUMBER,
                              I_SEQ_ID   IN NUMBER,
                              I_EMPRESA  IN NUMBER,
                              I_ELIMINAR IN VARCHAR2,
                              O_TOTAL    OUT VARCHAR2) IS


 BEGIN
      IF I_ELIMINAR IS NOT NULL THEN
      APEX_COLLECTION.DELETE_MEMBER (
         P_COLLECTION_NAME =>'CONCEPTOS_FIJOS',
         P_SEQ => I_SEQ_ID);

        DELETE FROM PER_EMPL_CONC PC
         WHERE PC.PERCON_CONCEPTO = I_CONCEPTO
           AND PC.PERCON_EMPLEADO = I_EMPLEADO
           AND PC.PERCON_EMPR = I_EMPRESA;


       END IF;

    SELECT 'TOTAL: '||TO_CHAR(NVL(SUM(C005),0),'999G999G999G999G999') IMPORTE
      INTO O_TOTAL
      FROM APEX_COLLECTIONS
     WHERE COLLECTION_NAME = 'CONCEPTOS_FIJOS';

END PP_ELIMINAR_CON_EMPL;


PROCEDURE PP_ELIMINAR_CONCEPTO (I_CONCEPTO IN NUMBER,
                                I_EMPRESA  IN NUMBER,
                                I_SUCURSAL IN NUMBER,
                                O_TOTAL    OUT VARCHAR2) IS


 BEGIN

  APEX_COLLECTION.CREATE_OR_TRUNCATE_COLLECTION (P_COLLECTION_NAME =>'CONCEPTOS_FIJOS');


        DELETE FROM PER_EMPL_CONC PC
         WHERE PC.PERCON_CONCEPTO = I_CONCEPTO
           AND PC.PERCON_EMPR = I_EMPRESA
           AND PC.PERCON_SUC = I_SUCURSAL;


    SELECT 'TOTAL: '||TO_CHAR(NVL(SUM(C005),0),'999G999G999G999G999') IMPORTE
      INTO O_TOTAL
      FROM APEX_COLLECTIONS
     WHERE COLLECTION_NAME = 'CONCEPTOS_FIJOS';

END PP_ELIMINAR_CONCEPTO;


PROCEDURE PP_BUSCAR_EMPLEADOS (I_EMPRESA    IN NUMBER,
                               I_LEGAJO     IN NUMBER,
                               I_SUCURSAL   IN NUMBER,
                               I_CONCEPTO   IN NUMBER,
                               I_FORMA_PAGO IN NUMBER,
                               O_NOMBRE     OUT VARCHAR2,
                               O_DPTO       OUT NUMBER,
                               O_DPTO_DESC  OUT VARCHAR2,
                               O_FORMA_PAGO OUT NUMBER,
                               O_FORMA_DEC  OUT VARCHAR2,
                               O_IMPORTE    OUT NUMBER,
                               O_COLOR      OUT VARCHAR2,
                               O_CANT_DIAS  OUT NUMBER,
                               O_AGREGAR    OUT VARCHAR2)IS



V_SUCURSAL             VARCHAR2(45);
V_WHERE                VARCHAR2(500);

V_MARC_SUEL_PARC       CHAR(1);

V_COLOR               VARCHAR2(2);
V_SALARIO_BASE        NUMBER :=0;
V_LISTA               VARCHAR2(2);
V_CANT_DIAS           NUMBER:=0;
V_SUC_CODIGO          NUMBER;
BEGIN


    SELECT A.EMPL_NOMBRE||' '||A.EMPL_APE ,
           F.FORMA_CODIGO,
           F.FORMA_DESC,
           DPTO_CODIGO,
           D.DPTO_DESC,
           E.SUC_DESC,
           SUC_CODIGO
      INTO
           O_NOMBRE,
           O_FORMA_PAGO,
           O_FORMA_DEC,
           O_DPTO,
           O_DPTO_DESC,
           V_SUCURSAL,
           V_SUC_CODIGO
      FROM PER_EMPLEADO A, PER_FORMA_PAGO F, GEN_DEPARTAMENTO D, GEN_SUCURSAL E
      WHERE A.EMPL_FORMA_PAGO = F.FORMA_CODIGO(+)
        AND A.EMPL_EMPRESA  = F.FORMA_EMPR (+)
        AND A.EMPL_DEPARTAMENTO = D.DPTO_CODIGO(+)
        AND A.EMPL_EMPRESA = D.DPTO_EMPR (+)
        AND A.EMPL_SUCURSAL = E.SUC_CODIGO(+)
        AND A.EMPL_EMPRESA  = SUC_EMPR
        AND (A.EMPL_SUCURSAL = I_SUCURSAL OR I_SUCURSAL IS NULL)
        AND A.EMPL_LEGAJO = I_LEGAJO
        AND EMPL_SITUACION  = 'A'
        AND A.EMPL_EMPRESA = I_EMPRESA;


        IF I_CONCEPTO = 2 and I_EMPRESA = 1 then

            PER_CALCULA_SAL_BASE_PRO2 (I_LEGAJO,I_EMPRESA,V_COLOR,V_SALARIO_BASE,V_LISTA,2,V_CANT_DIAS);
              O_IMPORTE    := ROUND(NVL(V_SALARIO_BASE,0));
              O_COLOR      := V_COLOR;
              O_CANT_DIAS  := V_CANT_DIAS;

       ELSE

        PER_CALCULA_SAL_BASE_PRO (I_LEGAJO,I_EMPRESA,V_COLOR,V_SALARIO_BASE);
        O_IMPORTE    := ROUND(NVL(V_SALARIO_BASE,0));
        O_COLOR      := V_COLOR;
        O_CANT_DIAS  := NULL;
        END IF;

    IF I_FORMA_PAGO <> O_FORMA_PAGO THEN
      RAISE_APPLICATION_ERROR (-20001,'El empleado tiene asignado otra forma de pago!');
    END IF;


    IF O_DPTO_DESC IS NULL THEN
       RAISE_APPLICATION_ERROR (-20001,'El empleado no tiene departamento asignado!');
    END  IF;

    IF V_SUCURSAL IS NULL THEN
        RAISE_APPLICATION_ERROR (-20001,'El empleado no tiene sucursal asignado!');
    END  IF;


    IF V_SUC_CODIGO <> I_SUCURSAL THEN
        RAISE_APPLICATION_ERROR (-20001,'El empleado pertenece a otra Sucursal!');
    END  IF;

    IF O_FORMA_PAGO IS NULL THEN
        RAISE_APPLICATION_ERROR (-20001, 'El empleado  no tiene forma de pago asignado!');
    END  IF;

O_AGREGAR :='S';



    EXCEPTION
        WHEN NO_DATA_FOUND THEN
          O_AGREGAR :='N';
            NULL;

END PP_BUSCAR_EMPLEADOS;



PROCEDURE PP_AGREGAR_EMPLEADO (I_EMPRESA      IN NUMBER,
                               I_EMPLEADO     IN NUMBER,
                               I_SUCURSAL     IN NUMBER,
                               I_CONCEPTO     IN NUMBER,
                               I_IMPORTE      IN VARCHAR2,
                               I_FORMA_PAGO   IN NUMBER,
                               I_DPTO         IN NUMBER,
                               I_DPTO_DESC    IN VARCHAR2,
                               I_EMPL_NOMBRE  IN VARCHAR2,
                               I_MONEDA       IN NUMBER,
                               I_COLOR        IN VARCHAR2,
                               I_CANT_DIAS    IN NUMBER,
                               I_SEQ_ID       IN NUMBER,
                               I_BANCO        IN NUMBER,
                               O_TOTAL        OUT VARCHAR2 ) IS

V_IMPORTE NUMBER;
X_EXISTE NUMBER;
W_PERIODO NUMBER;
BEGIN



SELECT TO_NUMBER(I_IMPORTE,
                 '9999999999999D99',
                 'NLS_NUMERIC_CHARACTERS='',.''')
  INTO V_IMPORTE
  FROM DUAL;


IF I_SEQ_ID IS NULL THEN
     SELECT COUNT(*)
      INTO X_EXISTE
      FROM APEX_COLLECTIONS
     WHERE COLLECTION_NAME = 'CONCEPTOS_FIJOS'
       AND C002    = I_EMPLEADO;


 SELECT P.PERI_CODIGO
   INTO W_PERIODO
  FROM PER_PERIODO P
  WHERE TRUNC(SYSDATE) BETWEEN P.PERI_FEC_INI AND P.PERI_FEC_FIN
    AND P.PERI_EMPR = I_EMPRESA;

IF X_EXISTE > 0 THEN
  RAISE_APPLICATION_ERROR (-20001, 'Registro ya ingresado previamente');
END IF;

END IF;
 IF V_IMPORTE IS NULL OR V_IMPORTE = 0 THEN
   RAISE_APPLICATION_ERROR (-20001, 'El importe no puede estar vacio o 0');
 END IF;



 IF I_SEQ_ID  IS NULL THEN

  IF I_DPTO IS NULL THEN
   RAISE_APPLICATION_ERROR (-20001, 'No existe el departamento');
 END IF;

   APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'CONCEPTOS_FIJOS',
                                         P_C001 => I_EMPRESA,
                                         P_C002 => I_EMPLEADO,
                                         P_C003 => I_CONCEPTO,
                                         P_C004 => NULL,
                                         P_C005 => NVL(V_IMPORTE,0),
                                         P_C006 => I_FORMA_PAGO,
                                         P_C007 => I_DPTO,
                                         P_C008 => I_EMPL_NOMBRE,
                                         P_C009 => I_SUCURSAL,
                                         P_C010 => I_MONEDA,
                                         P_C011 => NULL,
                                         P_C012 => I_DPTO_DESC,
                                         P_C013 => I_SUCURSAL,
                                         P_C014 => gen_devuelve_user,
                                         P_C015 => SYSDATE,
                                         P_C016 => I_COLOR,
                                         P_C017 => I_CANT_DIAS,
                                         P_C018 => I_BANCO,
                                         P_C019 => NULL,
                                         P_C020 => NULL,
                                         P_C021 => W_PERIODO);
 ELSE

       APEX_COLLECTION.UPDATE_MEMBER_ATTRIBUTE (
        P_COLLECTION_NAME => 'CONCEPTOS_FIJOS',
        P_SEQ             => I_SEQ_ID,
        P_ATTR_NUMBER     => 5,
        P_ATTR_VALUE      => NVL(V_IMPORTE,0));


 END IF;

     SELECT 'TOTAL: '||TO_CHAR(NVL(SUM(C005),0),'999G999G999G999G999') IMPORTE
      INTO O_TOTAL
      FROM APEX_COLLECTIONS
     WHERE COLLECTION_NAME = 'CONCEPTOS_FIJOS';


END PP_AGREGAR_EMPLEADO;



PROCEDURE PP_GUARDAR_DATOS (I_EMPRESA    IN NUMBER,
                            I_SUCURSAL   IN NUMBER,
                            I_CONCEPTO   IN NUMBER,
                            I_MONEDA     IN NUMBER,
                            I_BANCO      IN NUMBER) IS

V_USUARIO VARCHAR2(60):= gen_devuelve_user;
BEGIN

   IF I_SUCURSAL IS NULL THEN
        RAISE_APPLICATION_ERROR (-20001, 'La sucursal no puede quedar vacia!');
    END  IF;
   IF I_CONCEPTO IS NULL THEN
        RAISE_APPLICATION_ERROR (-20001, 'El concepto no puede quedar vacio!');
    END  IF;

    IF I_MONEDA IS NULL THEN
        RAISE_APPLICATION_ERROR (-20001, 'La moneda no puede quedar vacia!');
    END  IF;
           DELETE FROM PER_EMPL_CONC PC
         WHERE PC.PERCON_CONCEPTO = I_CONCEPTO
           AND PC.PERCON_EMPR = I_EMPRESA
           AND PC.PERCON_SUC = I_SUCURSAL;


          INSERT INTO PER_EMPL_CONC
                      (PERCON_EMPLEADO,
                       PERCON_CONCEPTO,
                       PERCON_IMP,
                       PERCON_MONEDA,
                       PERCON_EMPR,
                       PERCON_SUC,
                       PERCON_USER,
                       PERCON_FEC,
                       PERCON_IND_COLOR,
                       PERCON_CANT_DIAS,
                       PERCON_BCO,
                       PERCON_CAM_FP,
                       PERCON_PERIODO)
                SELECT C002 LEGAJO,
                       I_CONCEPTO,
                       C005 IMPORTE,
                       I_MONEDA,
                       I_EMPRESA,
                       I_SUCURSAL,
                       V_USUARIO,
                       SYSDATE,
                       LTRIM(RTRIM(C016))  COLOR,
                       C017 CANT_DIAS,
                       I_BANCO,
                       C020 CAMBIO_FORMA_PAGO,
                       C021 PERIODO
                  FROM APEX_COLLECTIONS
                 WHERE COLLECTION_NAME = 'CONCEPTOS_FIJOS';


END  PP_GUARDAR_DATOS;


END PERM056;
/
