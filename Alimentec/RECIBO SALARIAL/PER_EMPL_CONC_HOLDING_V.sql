CREATE OR REPLACE VIEW PER_EMPL_CONC_HOLDING_V AS
SELECT PERCON_EMPR,
       PERCON_EMPLEADO,
       PERCON_CONCEPTO,
       PC.PCON_DESC,
       PERCON_IMP,
       empl.empl_forma_pago EMPL_FORMA_PAGO,
       empl.empl_departamento EMPL_DEPARTAMENTO,
       empl.EMPL_NOMBRE||' '||empl.EMPL_APE EMPL_NOMBRE,
       empl.EMPL_SUCURSAL EMPL_SUCURSAL,
       PERCON_MONEDA,
       dpto_grupo_pago,
       dpto_desc,
       PERCON_SUC,
       PERCON_USER,
       PERCON_FEC,
       PERCON_IND_COLOR,
       PERCON_CANT_DIAS,
       PERCON_BCO,
       PERCON_CAM_FP,
       PERCON_PERIODO
  FROM PER_EMPL_CONC empl_conc,
       gen_departamento dep,
       per_concepto pc,
       PER_EMPLEADO EMPL
  where EMPL.empl_legajo = empl_conc.percon_empleado
   and  EMPL.empl_empresa = empl_conc.percon_empr
   and EMPL.EMPL_DEPARTAMENTO = dpto_codigo
   AND empl_conc.PERCON_EMPR= dep.dpto_empr
   AND Percon_Concepto = pc.pcon_clave
   AND percon_empr = pc.pcon_empr
   AND PERCON_EMPR not in (1, 2) --> HILA y TAGRO que tienen conf especial
   AND dep.dpto_empr not in (1, 2)--> HILA y TAGRO que tienen conf especial
   and empl.empl_empresa not in (1, 2)--> HILA y TAGRO que tienen conf especial
   ORDER BY EMPL_NOMBRE ASC
;
