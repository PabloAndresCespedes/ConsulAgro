CREATE OR REPLACE VIEW PER_HIST_PAGO_EMPL_V AS
SELECT TRUNC(T.EMPL_FECHA_MOD) HFEC_DESDE,
       TRUNC(NVL(LEAD(T.EMPL_FECHA_MOD-1)
                 OVER(PARTITION BY t.empl_empr,T.EMPL_LEGAJO ORDER BY t.empl_empr,T.EMPL_LEGAJO,
                      T.EMPL_FECHA_MOD),
                 SYSDATE + 100)) HFEC_HASTA,
       EMPL_LEGAJO HEMPL_LEGAJO,
       T.EMPL_FORMA_PAGO HEMPL_FORMA_PAGO,
       T.EMPL_TIPO_PAGO HEMPL_TIPO_PAGO,
       T.EMPL_EMPR HEMPL_EMPR,
       T.EMPL_SALARIO HEMPL_SALARIO,
       T.EMPL_HS_EXTRAS HEMPL_HS_EXTRAS,
       T.EMPL_TIPO_COMIS HEMPL_TIPO_COMIS,
       NVL(T.EMPL_MARC_COMIS_SIST,'N') HEMPL_MARC_COMIS_SIST,
       T.EMPL_SUCURSAL HEMPL_SUCURSAL,
       T.EMPL_DEPARTAMENTO HEMPL_DEPARTAMENTO,
       T.EMPL_CARGO HEMPL_CARGO,
       T.EMPL_IMP_HORA_N_D IMP_HD,
       T.EMPL_IMP_HORA_N_N IMP_HN,
       T.EMPL_IMP_HORA_E_D IMP_HDE,
       T.EMPL_IMP_HORA_E_N IMP_HNE,
       T.EMPL_IMP_HORA_DF_D IMP_HFD,
       T.EMPL_IMP_HORA_DF_N IMP_HFN,
       T.EMPL_IMP_HORA_E_M IMP_HME
  FROM PER_EMPL_PAGO_HIS T
  WHERE T.EMPL_EMPR != 2 -- 10/08/2022 10:57:16 @PabloACespedes \(^-^)/, todos menos TAGRO
   UNION ALL
  SELECT TRUNC(T.EMPL_FEC_INGRESO) HFEC_DESDE,
       TRUNC(NVL(LEAD(T.EMPL_FEC_INGRESO)
                 OVER(PARTITION BY T.EMPL_EMPRESA,T.EMPL_LEGAJO ORDER BY T.EMPL_EMPRESA,T.EMPL_LEGAJO,
                      T.EMPL_FEC_INGRESO),
                 SYSDATE + 100)) HFEC_HASTA,
       EMPL_LEGAJO HEMPL_LEGAJO,
       T.EMPL_FORMA_PAGO HEMPL_FORMA_PAGO,
       T.EMPL_TIPO_SALARIO HEMPL_TIPO_PAGO,
       T.EMPL_EMPRESA HEMPL_EMPR,
       T.EMPL_SALARIO_BASE HEMPL_SALARIO,
       T.EMPL_IND_HORA_EXTRA HEMPL_HS_EXTRAS,
       T.EMPL_TIPO_COMIS HEMPL_TIPO_COMIS,
       NVL(T.EMPL_MARC_COMIS_SIST,'N') HEMPL_MARC_COMIS_SIST,
       T.EMPL_SUCURSAL HEMPL_SUCURSAL,
       T.EMPL_DEPARTAMENTO HEMPL_DEPARTAMENTO,
       T.EMPL_CARGO HEMPL_CARGO,
       t.EMPL_IMP_HORA_N_D IMP_HD,
       t.EMPL_IMP_HORA_N_N IMP_HN,
       t.EMPL_IMP_HORA_E_D IMP_HDE,
       t.EMPL_IMP_HORA_E_N IMP_HNE,
       t.EMPL_IMP_HORA_DF_D IMP_HFD,
       t.EMPL_IMP_HORA_DF_N IMP_HFN,
       t.EMPL_IMP_HORA_E_M IMP_HME
  FROM PER_EMPLEADO T
  WHERE EMPL_EMPRESA = 2
  AND EMPL_SITUACION = 'A';
