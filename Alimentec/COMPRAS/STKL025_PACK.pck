CREATE OR REPLACE PACKAGE STKL025_PACK IS

  -- AUTHOR  : PROGRAMACION4
  -- CREATED : 26/04/2018 14:42:47
  -- PURPOSE :

  PROCEDURE PP_GENERAR_CONSULTA(P_EMPRESA    IN NUMBER,
                                I_SUC        IN VARCHAR2,
                                I_DEP        IN VARCHAR2,
                                I_MARCA      IN VARCHAR2,
                                I_FECHA_INI  IN DATE,
                                I_FECHA_FIN  IN DATE,
                                I_LINEA      IN NUMBER,
                                I_ENVASE     IN NUMBER,
                                I_GRUPO      IN NUMBER,
                                I_PERIODO    IN NUMBER,
                                I_TIPO_FECHA VARCHAR2,
                                I_LOGIN      varchar2,
                                I_SESSION_ID in varchar2);

  PROCEDURE PP_GENERAR_COLSULTA2(P_EMPRESA       IN NUMBER,
                                 I_SUC           IN NUMBER,
                                 I_DEP           IN NUMBER,
                                 I_MARCA         IN NUMBER,
                                 I_FECHA_INI     IN DATE,
                                 I_FECHA_FIN     IN DATE,
                                 I_LINEA         IN NUMBER,
                                 I_ENVASE        IN NUMBER,
                                 I_GRUPO         IN NUMBER,
                                 I_PERIODO       IN NUMBER,
                                 I_IMPRIMIR_CERO IN VARCHAR2,
                                 I_LOGIN         IN VARCHAR2,
                                 I_SESSION_ID    IN NUMBER);

  PROCEDURE PP_GENERAR_REPORTE(P_EMPRESA       IN NUMBER,
                               I_SUC           IN NUMBER,
                               I_DEP           IN NUMBER,
                               I_MARCA         IN NUMBER,
                               I_LINEA         IN NUMBER,
                               I_GRUPO         IN NUMBER,
                               I_FECHA_INICIO  IN DATE,
                               I_FECHA_FIN     IN DATE,
                               I_PERIODO       IN NUMBER,
                               I_IMPRIMIR_CERO IN VARCHAR2,
                               I_SUCURSAL      IN VARCHAR2,
                               I_ENVASE        IN NUMBER,
                               P_TIPO_FECHA    IN VARCHAR2,
                               I_SESSION_ID    NUMBER,
                               I_LOGIN         VARCHAR2);

  PROCEDURE PP_INSERTAR_TABLA_AUXI(I_EMPRESA    NUMBER,
                                   I_SESSION_ID NUMBER,
                                   I_LOGIN      VARCHAR2);

  PROCEDURE PP_ENVIAR_CORREO(I_EMPRESA IN NUMBER);
END STKL025_PACK;
/
CREATE OR REPLACE PACKAGE BODY STKL025_PACK IS

  PROCEDURE PP_GENERAR_CONSULTA(P_EMPRESA    IN NUMBER,
                                I_SUC        IN VARCHAR2,
                                I_DEP        IN VARCHAR2,
                                I_MARCA      IN VARCHAR2,
                                I_FECHA_INI  IN DATE,
                                I_FECHA_FIN  IN DATE,
                                I_LINEA      IN NUMBER,
                                I_ENVASE     IN NUMBER,
                                I_GRUPO      IN NUMBER,
                                I_PERIODO    IN NUMBER,
                                I_TIPO_FECHA VARCHAR2,
                                I_LOGIN      VARCHAR2,
                                I_SESSION_ID IN VARCHAR2) IS
    V_QUERY1    VARCHAR2(30000);
    V_MARCA     VARCHAR2(500) := REGEXP_REPLACE(REGEXP_REPLACE(I_MARCA,
                                                               ':',
                                                               ','),
                                                '^,',
                                                '');

     V_SUCURSAL     VARCHAR2(500) := REGEXP_REPLACE(REGEXP_REPLACE(I_SUC,
                                                               ':',
                                                               ','),
                                                '^,',
                                                '');

     V_DEP     VARCHAR2(30000) := REGEXP_REPLACE(REGEXP_REPLACE(I_DEP,
                                                               ':',
                                                               ','),
                                                '^,',
                                                '');

    V_PERI_SGTE NUMBER;
    V_PERI_ACT  NUMBER;

    V_FECHA_INI DATE := I_FECHA_INI;
    V_FECHA_FIN DATE;

    V_COL_ARDE_CANT_INI           VARCHAR2(50);
    V_COL_DOCU_FEC                VARCHAR2(50);
    V_TABLA_STK_ARTICULO_DEPOSITO VARCHAR2(50);
    V_WHERE_PERIODO               VARCHAR2(50);
    V_SUC_1_5 VARCHAR2(100);
    V_DEPOSITO VARCHAR2(30000);
    V_TIPO_REPORT VARCHAR(30000) := v('P206_TIPO_REPORTE');

  BEGIN


    IF I_PERIODO IS NULL THEN
      RAISE_APPLICATION_ERROR(-20000, 'Codigo de periodo es obligatorio');
    END IF;

    SELECT MAX(PERI_CODIGO) PERI_SGTE, MAX(PERI_CODIGO - 1) V_PERI_ACT
      INTO V_PERI_SGTE, V_PERI_ACT
      FROM STK_PERIODO
     WHERE PERI_EMPR = P_EMPRESA;

    IF I_TIPO_FECHA = 'OPER' THEN
      V_COL_ARDE_CANT_INI := 'ARDE_CANT_INI_OPER';
      V_COL_DOCU_FEC      := 'DOCU_FEC_OPER';
    ELSE
      V_COL_ARDE_CANT_INI := 'ARDE_CANT_INI';
      V_COL_DOCU_FEC      := 'DOCU_FEC_EMIS';
    END IF;

    IF I_PERIODO BETWEEN V_PERI_ACT AND V_PERI_SGTE THEN
      SELECT TO_CHAR(PERI_FEC_INI, 'DD/MM/YYYY')
        INTO V_FECHA_INI
        FROM STK_PERIODO
       WHERE PERI_EMPR = P_EMPRESA
         AND PERI_CODIGO = (SELECT MAX(PERI_CODIGO) - 1
                              FROM STK_PERIODO
                             WHERE PERI_EMPR = P_EMPRESA);
      V_TABLA_STK_ARTICULO_DEPOSITO := 'STK_ARTICULO_DEPOSITO';

    ELSE
      SELECT TO_CHAR(PERI_FEC_INI, 'DD/MM/YYYY')
        INTO V_FECHA_INI
        FROM STK_PERIODO
       WHERE PERI_CODIGO = I_PERIODO
         AND PERI_EMPR = P_EMPRESA;
      V_TABLA_STK_ARTICULO_DEPOSITO := 'STK_ARTICULO_DEPOSITO_HIST';
      V_WHERE_PERIODO               := ' AND ARDE_PERIODO =' || I_PERIODO;
    END IF;

    V_FECHA_FIN := I_FECHA_FIN;

  IF  V_SUCURSAL  LIKE ('%1%') AND V_SUCURSAL  LIKE ('%5%') AND  NVL(v('P206_TIPO_REPORTE'),'S')='N'  THEN
 SELECT wm_concat(F.SUC_CODIGO||' - '||F.SUC_DESC)  INTO V_SUC_1_5
 FROM GEN_SUCURSAL F
 WHERE F.SUC_EMPR=1
 AND F.SUC_CODIGO IN (1,5);


    SELECT wm_concat(T.DEP_CODIGO || '-' || T.DEP_DESC) A
     INTO V_DEPOSITO
   FROM STK_DEPOSITO T
  WHERE T.DEP_SUC IN (1,5)
    AND T.DEP_EMPR = P_EMPRESA
    AND T.DEP_DESC IN ((SELECT REGEXP_SUBSTR(I_DEP, '[^:]+', 1, LEVEL) A
                 FROM DUAL
                 CONNECT BY REGEXP_SUBSTR(I_DEP, '[^:]+', 1, LEVEL) IS NOT NULL)) ;
 END IF;





    V_QUERY1 := 'SELECT   (CASE WHEN AD.ARDE_SUC IN (1,5) AND NVL('''||V_TIPO_REPORT||''',''S'')=''N'' THEN
                            '''||V_SUC_1_5||'''
                            ELSE
                            to_char(AD.ARDE_SUC)
                            END) SUCURSAL,';
    IF I_DEP IS NOT NULL THEN

       V_QUERY1 := V_QUERY1 || '
       (CASE WHEN AD.ARDE_SUC IN (1,5) AND NVL('''||V_TIPO_REPORT||''',''S'')=''N'' THEN
          '''||V_DEPOSITO||'''
          else
        ' || 'DP.DEP_CODIGO ||' || CHR(39) || ' - ' ||
         CHR(39) || ' ||DP.DEP_DESC END) DEPOSITO,';
    ELSE

      V_QUERY1 := V_QUERY1 || ' ' || CHR(39) || ' TODOS ' || CHR(39) || ',';
    END IF;


    V_QUERY1 := V_QUERY1 || ' ' || '
                     AD.ARDE_ART CODIGO,
                     SA.ART_COD_ALFANUMERICO ARTICULO,
                     SA.ART_DESC DESCRIPCION,
                     ROUND(SUM((CANT_INI + NVL(TOT_ENT, 0)) - NVL(TOT_SAL, 0)) *
                           (P.AEP_COSTO_PROM_LOC),
                           0) GUARANIES,
                     ROUND(SUM((CANT_INI + NVL(TOT_ENT, 0)) - NVL(TOT_SAL, 0)) *
                           (P.AEP_COSTO_PROM_MON),
                           2) DOLARES,
                     SUM((CANT_INI + NVL(TOT_ENT, 0)) - NVL(TOT_SAL, 0)) EXISTENCIA,
                     SA.ART_KG_CONTENIDO * SUM((CANT_INI + NVL(TOT_ENT, 0)) - NVL(TOT_SAL, 0)) KG_TOTAL,
                     SA.ART_UNID_MED UM,
                     NVL(SUM(AUX.EXISTENCIA), 0) DEPOSITO,
                     SA.ART_LINEA,
                     (SELECT G.GRUP_CODIGO ||' || CHR(39) ||
                ' - ' || CHR(39) ||
                '||G.GRUP_DESC
                      FROM STK_GRUPO G
                      WHERE G.GRUP_LINEA = SA.ART_LINEA
                        AND G.GRUP_CODIGO = SA.ART_GRUPO
                        AND G.GRUP_EMPR = SA.ART_EMPR) GRUPO
                FROM STK_ARTICULO SA,
                     STK_DEPOSITO DP,
                     STK_ART_EMPR_PERI P,
                     (
                              SELECT AD.ARDE_EMPR,
                                             AD.ARDE_SUC,
                                             AD.ARDE_DEP,
                                             AD.ARDE_ART,' ||
                V_COL_ARDE_CANT_INI ||
                ' CANT_INI,
                                             0 TOT_ENT,
                                             0 TOT_SAL
                                        FROM ' ||
                V_TABLA_STK_ARTICULO_DEPOSITO || ' AD
                                       WHERE AD.ARDE_EMPR = ' ||
                P_EMPRESA || ' ' || V_WHERE_PERIODO || '
                                  UNION ALL
                                SELECT DOCU_EMPR,
                                       DOCU_SUC_ORIG,
                                       DOCU_DEP_ORIG,
                                       DETA_ART,
                                       0,
                                       SUM(DECODE(OPER_ENT_SAL,' ||
                CHR(39) || 'E' || CHR(39) ||
                ', DETA_CANT, 0)) AS TOT_ENT,
                                       SUM(DECODE(OPER_ENT_SAL,' ||
                CHR(39) || 'S' || CHR(39) ||
                ', DETA_CANT, 0)) AS TOT_SAL
                                  FROM STK_OPERACION, STK_DOCUMENTO, STK_DOCUMENTO_DET
                                 WHERE DOCU_CLAVE = DETA_CLAVE_DOC
                                   AND DOCU_EMPR = DETA_EMPR
                                   AND DOCU_CODIGO_OPER = OPER_CODIGO
                                   AND DOCU_EMPR = OPER_EMPR
                                   AND DOCU_EMPR = ' ||
                P_EMPRESA || '
                                   AND ' || V_COL_DOCU_FEC ||
                ' BETWEEN ' || CHR(39) || V_FECHA_INI || CHR(39) || ' AND ' ||
                CHR(39) || V_FECHA_FIN || CHR(39) ||
                'GROUP BY DOCU_EMPR,
                                          DOCU_SUC_ORIG,
                                          DOCU_DEP_ORIG,
                                          DETA_ART
                        ) AD,
                   (SELECT SUM(B.entmerdet_cant) EXISTENCIA,
                          -- C.OCOM_SUC SUCURSAL,
                          -- C.OCOM_DEPOSITO DEPOSITO,
                           DECODE(A.ENTMER_DEP_SUC,NULL,C.OCOM_SUC,ENTMER_DEP_SUC)  SUCURSAL,
                           DECODE(A.ENTMER_DEPOSITO_ACT,NULL, C.OCOM_DEPOSITO,A.ENTMER_DEPOSITO_ACT) DEPOSITO,
                           D.OCOMD_ART ARTICULO,
                           D.OCOMD_EMPR EMPR
                      FROM STK_ENTRADA_MERC     A,
                           STK_ENTRADA_MERC_DET B,
                           STK_ORDEN_COMPRA     C,
                           STK_ORDEN_COMPRA_DET D
                     WHERE B.ENTMERDET_ORDEN  = A.ENTMER_CLAVE_ORDEN
                       AND B.ENTMERDET_NUM    = A.ENTMER_NUM_ENTR
                       AND B.ENTMERDET_EMPR   = A.ENTMER_EMPR

                       AND C.OCOM_CLAVE       = A.ENTMER_CLAVE_ORDEN
                       AND C.OCOM_EMPR        = A.ENTMER_EMPR

                       AND D.OCOMD_NRO_ITEM   = B.ENTMERDET_ITEM
                       AND D.OCOMD_CLAVE_OCOM = B.ENTMERDET_ORDEN
                       AND D.OCOMD_EMPR       = B.ENTMERDET_EMPR

                       AND A.ENTMER_CLAVE_DOC_FIN IS NULL
                       AND C.OCOM_ESTADO = ''C''


                     GROUP BY  DECODE(A.ENTMER_DEP_SUC,NULL,C.OCOM_SUC,ENTMER_DEP_SUC),
                           DECODE(A.ENTMER_DEPOSITO_ACT,NULL, C.OCOM_DEPOSITO,A.ENTMER_DEPOSITO_ACT),--C.OCOM_SUC, C.OCOM_DEPOSITO,
                           D.OCOMD_ART, D.OCOMD_EMPR) AUX

                 WHERE SA.ART_TIPO <> 4

                   AND SA.ART_CODIGO = AD.ARDE_ART
                   AND SA.ART_EMPR   = AD.ARDE_EMPR

                   AND AD.ARDE_EMPR  = AUX.EMPR (+)
                   AND AD.ARDE_SUC   = AUX.SUCURSAL (+)
                   AND AD.ARDE_DEP   = AUX.DEPOSITO (+)
                   AND AD.ARDE_ART   = AUX.ARTICULO (+)

                   AND P.AEP_ART     = SA.ART_CODIGO
                   AND P.AEP_EMPR    = SA.ART_EMPR ' ||
                'AND P.AEP_PERIODO = ' || I_PERIODO || '

                   AND AD.ARDE_EMPR = DP.DEP_EMPR
                   AND AD.ARDE_SUC  = DP.DEP_SUC
                   AND AD.ARDE_DEP  = DP.DEP_CODIGO';

    IF V_SUCURSAL IS NOT NULL  THEN
    --  V_QUERY1 := V_QUERY1 || ' ' || 'AND AD.ARDE_SUC = ' || I_SUC;
     V_QUERY1 := V_QUERY1 || ' ' || 'AND  AD.ARDE_SUC IN (' || V_SUCURSAL || ')';
    END IF;

    IF V_DEP IS NOT NULL AND P_EMPRESA<>1 THEN
    --  V_QUERY1 := V_QUERY1 || ' ' || 'AND AD.ARDE_DEP = ' || I_DEP;
     V_QUERY1 := V_QUERY1 || ' ' || 'AND  AD.ARDE_DEP IN (' || V_DEP || ')';
     ELSIF V_DEP IS NOT NULL THEN
      --V_QUERY1 := V_QUERY1 || ' ' || 'AND DP.DEP_DESC IN (''' || V_DEP || ''')';
        V_QUERY1 := V_QUERY1 || ' AND DP.DEP_DESC IN
             (SELECT REGEXP_SUBSTR(''' || I_DEP ||
                 ''', ''[^:]+'', 1, LEVEL) A
                FROM DUAL
              CONNECT BY REGEXP_SUBSTR(''' || I_DEP ||
                 ''', ''[^:]+'', 1, LEVEL) IS NOT NULL)';

    END IF;

    IF V_MARCA IS NOT NULL THEN
      V_QUERY1 := V_QUERY1 || ' ' || 'AND SA.ART_MARCA IN (' || V_MARCA || ')';
    END IF;

    IF I_LINEA IS NOT NULL THEN
      V_QUERY1 := V_QUERY1 || ' ' || 'AND SA.ART_LINEA = ' || I_LINEA;
    END IF;
    IF I_ENVASE IS NOT NULL THEN
      V_QUERY1 := V_QUERY1 || ' ' || 'AND SA.ART_ENVASE = ' || I_ENVASE;
    END IF;

    IF I_GRUPO IS NOT NULL THEN
      V_QUERY1 := V_QUERY1 || ' ' || 'AND SA.ART_GRUPO = ' || I_GRUPO;
    END IF;

    V_QUERY1 := V_QUERY1 || '
                 GROUP BY AD.ARDE_EMPR,
                          (CASE WHEN AD.ARDE_SUC IN (1,5) AND NVL('''||V_TIPO_REPORT||''',''S'')=''N'' THEN
                            '''||V_SUC_1_5||'''
                            ELSE
                            to_char(AD.ARDE_SUC)
                            END),';
    IF I_DEP IS NOT NULL  THEN
     -- V_QUERY1 := V_QUERY1 || ' ' || 'DP.DEP_CODIGO,DP.DEP_DESC,';
      V_QUERY1 := V_QUERY1 || '
       (CASE WHEN AD.ARDE_SUC IN (1,5) AND NVL('''||V_TIPO_REPORT||''',''S'')=''N'' THEN
          '''||V_DEPOSITO||'''
          else
        ' || 'DP.DEP_CODIGO ||' || CHR(39) || ' - ' ||
         CHR(39) || ' ||DP.DEP_DESC END) ,';
    ELSE
      V_QUERY1 := V_QUERY1 || ' ' || CHR(39) || '


                                      TODOS



                                      ' || CHR(39) || ',';
    END IF;
    V_QUERY1 := V_QUERY1 || ' ' || '
                          AD.ARDE_ART,
                          SA.ART_EMPR,
                          SA.ART_LINEA,
                          SA.ART_GRUPO,
                          SA.ART_KG_CONTENIDO,
                          (P.AEP_COSTO_PROM_LOC),
                          (P.AEP_COSTO_PROM_MON),
                          SA.ART_DESC,
                          SA.ART_COD_ALFANUMERICO,
                          SA.ART_UNID_MED
                     HAVING SUM(ROUND((CANT_INI + NVL(TOT_ENT, 0)) - NVL(TOT_SAL, 0), 3)) <> 0  ';
 DELETE FROM X WHERE OTRO = 'STKL025A';
    INSERT INTO X (CAMPO1, OTRO) VALUES (V_QUERY1, 'STKL025A');
    COMMIT;

    -- DBMS_OUTPUT.PUT_LINE(V_QUERY1);

    IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => 'STKL025') THEN
      APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => 'STKL025');
    END IF;

    APEX_COLLECTION.CREATE_COLLECTION_FROM_QUERY_B(P_COLLECTION_NAME => 'STKL025',
                                                   P_QUERY           => V_QUERY1);

    ----------------------------------------------------------------------------
    BEGIN
      -- CALL THE PROCEDURE
      STKL025_PACK.PP_INSERTAR_TABLA_AUXI(I_EMPRESA    => P_EMPRESA,
                                          I_SESSION_ID => I_SESSION_ID,
                                          I_LOGIN      => I_LOGIN);
    END;

    ------------------------------------------------------------------------------

  END;

  PROCEDURE PP_GENERAR_COLSULTA2(P_EMPRESA       IN NUMBER,
                                 I_SUC           IN NUMBER,
                                 I_DEP           IN NUMBER,
                                 I_MARCA         IN NUMBER,
                                 I_FECHA_INI     IN DATE,
                                 I_FECHA_FIN     IN DATE,
                                 I_LINEA         IN NUMBER,
                                 I_ENVASE        IN NUMBER,
                                 I_GRUPO         IN NUMBER,
                                 I_PERIODO       IN NUMBER,
                                 I_IMPRIMIR_CERO IN VARCHAR2,
                                 I_LOGIN         IN VARCHAR2,
                                 I_SESSION_ID    IN NUMBER) IS
    V_CONSULTA VARCHAR2(32767);
    V_QUERY1   VARCHAR2(32767);

  BEGIN

    IF I_SUC IS NOT NULL THEN
      V_QUERY1 := V_QUERY1 || ' ' || '
      AND ARDE_SUC = ' || I_SUC;

    END IF;
    IF I_DEP IS NOT NULL THEN
      V_QUERY1 := V_QUERY1 || ' ' || '
      AND ARDE_DEP = ' || I_DEP;
    END IF;
    IF I_MARCA IS NOT NULL THEN
      V_QUERY1 := V_QUERY1 || ' ' || '
      AND ART_MARCA IN (' || I_MARCA || ')';
    END IF;

    IF I_LINEA IS NOT NULL THEN
      V_QUERY1 := V_QUERY1 || ' ' || '
      AND ART_LINEA = ' || I_LINEA;
    END IF;
    IF I_ENVASE IS NOT NULL THEN
      V_QUERY1 := V_QUERY1 || ' ' || '
      AND ART_ENVASE = ' || I_ENVASE;
    END IF;

    IF I_GRUPO IS NOT NULL THEN
      V_QUERY1 := V_QUERY1 || ' ' || '
      AND ART_GRUPO = ' || I_GRUPO;

    END IF;

    V_CONSULTA := 'SELECT
       EMPR_RAZON_SOCIAL EMPRESA,
       ARDE_SUC COD_SUC,
       SUC_DESC SUCURSAL,
       ARDE_ART CODIGO,
       ART_DESC ARTICULO,
       ART_UNID_MED UM,
        SUM((ARDE_CANT_INI + NVL(TOT_ENT, 0)) - NVL(TOT_SAL, 0)) * AEP_COSTO_PROM_LOC  GUARANIES,
        (SUM((ARDE_CANT_INI + NVL(TOT_ENT, 0)) - NVL(TOT_SAL, 0)) *  AEP_COSTO_PROM_MON)  DOLARES,
       SUM((ARDE_CANT_INI + NVL(TOT_ENT, 0)) - NVL(TOT_SAL, 0)) EXISTENCIA


  FROM GEN_EMPRESA,
       GEN_SUCURSAL,
       STK_ARTICULO,
       ( SELECT DOCU_EMPR, DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART,
                 SUM(DECODE(OPER_ENT_SAL,''E'',DETA_CANT,0)) AS TOT_ENT,
                   SUM(DECODE(OPER_ENT_SAL,''S'',DETA_CANT,0)) AS TOT_SAL
                   FROM STK_OPERACION, STK_DOCUMENTO, STK_DOCUMENTO_DET
                   WHERE DOCU_EMPR =' || P_EMPRESA || '
                     AND DOCU_CLAVE = DETA_CLAVE_DOC AND DOCU_EMPR = DETA_EMPR
                    AND OPER_CODIGO = DOCU_CODIGO_OPER AND OPER_EMPR = DOCU_EMPR

                    AND DOCU_FEC_EMIS BETWEEN ''' ||
                  I_FECHA_INI || ''' AND
                  ''' || I_FECHA_FIN || '''
                    GROUP BY DOCU_EMPR, DOCU_SUC_ORIG, DOCU_DEP_ORIG, DETA_ART),
       STK_ARTICULO_DEPOSITO,
       STK_ART_EMPR_PERI

 WHERE ART_EMPR = ' || P_EMPRESA || '

   AND ART_CODIGO = ARDE_ART
   AND ART_EMPR = ARDE_EMPR

   AND EMPR_CODIGO = ARDE_EMPR

   AND SUC_EMPR = ARDE_EMPR
   AND SUC_EMPR = ARDE_EMPR

   AND SUC_CODIGO = ARDE_SUC
   AND SUC_EMPR = ARDE_EMPR

   AND AEP_EMPR = ARDE_EMPR
   AND AEP_EMPR = ARDE_EMPR

   AND AEP_ART = ARDE_ART
   AND AEP_EMPR = ARDE_EMPR

   AND ARDE_EMPR = DOCU_EMPR(+)
   AND ARDE_EMPR = DOCU_EMPR(+)

   AND ARDE_SUC = DOCU_SUC_ORIG(+)
   AND ARDE_EMPR = DOCU_EMPR(+)

   AND ARDE_DEP = DOCU_DEP_ORIG(+)
   AND ARDE_EMPR = DOCU_EMPR(+)


   AND ARDE_ART = DETA_ART(+)
    AND( ART_TIPO <> 4 OR ART_IND_INV_VALOR_COSTO = ''S'')';
    IF I_IMPRIMIR_CERO = 'N' THEN
      V_CONSULTA := V_CONSULTA || ' AND AEP_TOT_EXIST <> 0';
    END IF;
    V_CONSULTA := V_CONSULTA || ' and aep_periodo = ' || I_PERIODO || ' ' ||
                  V_QUERY1 || '';
    IF I_IMPRIMIR_CERO = 'N' THEN
      V_CONSULTA := V_CONSULTA ||
                    'HAVING SUM((ARDE_CANT_INI + NVL(TOT_ENT, 0)) - NVL(TOT_SAL, 0) ) <> 0';
    END IF;
    V_CONSULTA := V_CONSULTA || ' GROUP BY arde_empr,
          empr_razon_social,
          arde_suc,
          suc_desc,
          arde_art,
          art_desc,
          art_cod_alfanumerico,
          art_unid_med,
          aep_costo_prom_loc,
          aep_costo_prom_mon
          ORDER BY ART_DESC';

    --DBMS_OUTPUT.PUT_LINE(V_CONSULTA);
    DELETE FROM X WHERE OTRO = 'STKL0251';
    INSERT INTO X (CAMPO1, OTRO) VALUES (V_CONSULTA, 'STKL0251');
    --  COMMIT;

    IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => 'STKL025_T') THEN
      APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => 'STKL025_T');
    END IF;

    APEX_COLLECTION.CREATE_COLLECTION_FROM_QUERY_B(P_COLLECTION_NAME => 'STKL025_T',
                                                   P_QUERY           => V_CONSULTA);

    ----------------------------------------------------------------------------
    BEGIN
      -- CALL THE PROCEDURE
      STKL025_PACK.PP_INSERTAR_TABLA_AUXI(I_EMPRESA    => P_EMPRESA,
                                          I_SESSION_ID => I_SESSION_ID,
                                          I_LOGIN      => I_LOGIN);
    END;

    ------------------------------------------------------------------------------

  END PP_GENERAR_COLSULTA2;

  PROCEDURE PP_INSERTAR_TABLA_AUXI(I_EMPRESA    NUMBER,
                                   I_SESSION_ID NUMBER,
                                   I_LOGIN      VARCHAR2) IS

    CURSOR C IS
      SELECT C001 EMPRESA,
             TO_NUMBER(C002) SUC_CODIGO,
             C003 SUCURSAL,
             TO_NUMBER(C004) ARTICULO,
             C005 DESCRIPCION_ART,
             C006 UM,

             TO_NUMBER(C007) GUARANIES,
             TO_NUMBER(C008) DOLARES,
             TO_NUMBER(C009) EXISTENCIA
        FROM APEX_COLLECTIONS
       WHERE COLLECTION_NAME = 'STKL025_T';

    CURSOR B IS
      SELECT C001 SUCURSAL,
             C002 DEPOSITO,
             TO_NUMBER(C003) CODIGO,
             C004 ARTICULO,
             C005 DESCRIPCION,
             TO_NUMBER(C008) EXISTENCIA,
             C010 UM,
             TO_NUMBER(C006) GUARANIES,
             TO_NUMBER(C007) DOLARES,
             TO_NUMBER(C011) RECEPCION,
             C012 LINEA,
             C013 GRUPO,
             TO_NUMBER(C009) TOTAL_KG
        FROM APEX_COLLECTIONS
       WHERE COLLECTION_NAME = 'STKL025';

  BEGIN

    DELETE FROM STK_STKL025_TEMP
     WHERE SESSION_ID = I_SESSION_ID
       AND LOGIN = GEN_DEVUELVE_USER;
    COMMIT;
    IF I_EMPRESA = 2 THEN
      FOR K IN C LOOP
        INSERT INTO STK_STKL025_TEMP
          (EMPRESA_DES,
           SUC_CODIGO,
           SUCURSAL,
           ARTICULO,
           DESCRIPCION_ART,
           UM,
           GUARANIES,
           DOLARES,
           EXISTENCIA,
           SESSION_ID,
           LOGIN,
           EMPR)
        VALUES
          (K.EMPRESA,
           K.SUC_CODIGO,
           K.SUCURSAL,
           K.ARTICULO,
           K.DESCRIPCION_ART,
           K.UM,
           K.GUARANIES,
           K.DOLARES,
           K.EXISTENCIA,
           I_SESSION_ID,
           GEN_DEVUELVE_USER,
           I_EMPRESA);
      END LOOP;
    END IF;
    COMMIT;

    IF I_EMPRESA != 2 then -->distinto a TAGRO
      FOR C IN B LOOP
        INSERT INTO STK_STKL025_TEMP
          (

           SUCURSAL,
           ARTICULO,
           DESCRIPCION_ART,
           UM,
           GUARANIES,
           DOLARES,
           EXISTENCIA,
           SESSION_ID,
           LOGIN,
           EMPR,
           DEPOSITO,
           RECEPCION,
           LINEA,
           GRUPO,
           TOTAL)
        VALUES
          (C.SUCURSAL,
           C.CODIGO,
           C.DESCRIPCION,
           C.UM,
           C.GUARANIES,
           C.DOLARES,
           C.EXISTENCIA,
           I_SESSION_ID,
           GEN_DEVUELVE_USER,
           I_EMPRESA,
           C.DEPOSITO,
           C.RECEPCION,
           C.LINEA,
           C.GRUPO,
           C.TOTAL_KG);
      END LOOP;
    END IF;
    COMMIT;

  END PP_INSERTAR_TABLA_AUXI;

  PROCEDURE PP_GENERAR_REPORTE(P_EMPRESA       IN NUMBER,
                               I_SUC           IN NUMBER,
                               I_DEP           IN NUMBER,
                               I_MARCA         IN NUMBER,
                               I_LINEA         IN NUMBER,
                               I_GRUPO         IN NUMBER,
                               I_FECHA_INICIO  IN DATE,
                               I_FECHA_FIN     IN DATE,
                               I_PERIODO       IN NUMBER,
                               I_IMPRIMIR_CERO IN VARCHAR2,
                               I_SUCURSAL      IN VARCHAR2,
                               I_ENVASE        IN NUMBER,
                               P_TIPO_FECHA    IN VARCHAR2,
                               I_SESSION_ID    NUMBER,
                               I_LOGIN         VARCHAR2) IS

    V_PARAMETROS    VARCHAR2(32767);
    V_IDENTIFICADOR VARCHAR2(2) := '&'; --SE UTILIZA PARA QUE AL COMPILAR NO LO TOME COMO PARAMETROS
    V_WHERE         VARCHAR2(400);
    V_EMPR_DESC     VARCHAR2(100);
    V_DESC_SUCURSAL VARCHAR2(100);
    V_MARCA         VARCHAR2(100);
    V_LINEA         VARCHAR2(100);
    V_GRUPO         VARCHAR2(100);
    V_REPORTE       VARCHAR2(100);
    V_SUCURSAL      VARCHAR2(100);
    V_DEPOSITO      VARCHAR2(50);
    V_ENVASE        VARCHAR2(50);

  BEGIN
    IF P_EMPRESA = 2 THEN
      V_REPORTE := 'STKL225';
    ELSE
      V_REPORTE := 'STKL025';
    END IF;

    BEGIN
      SELECT A.EMPR_RAZON_SOCIAL
        INTO V_EMPR_DESC
        FROM GEN_EMPRESA A
       WHERE A.EMPR_CODIGO = P_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20016, 'Empresa no valida!');
    END;

    BEGIN
      SELECT SUC_DESC
        INTO V_DESC_SUCURSAL
        FROM GEN_SUCURSAL
       WHERE SUC_CODIGO = I_SUC
         AND SUC_EMPR = P_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_DESC_SUCURSAL := 'Todas las Sucursales';
    END;
    BEGIN
      SELECT SUC_DESC
        INTO V_SUCURSAL
        FROM GEN_SUCURSAL
       WHERE SUC_CODIGO = I_SUCURSAL
         AND SUC_EMPR = P_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;

    BEGIN
      SELECT MARC_DESC
        INTO V_MARCA
        FROM STK_MARCA
       WHERE MARC_CODIGO = I_MARCA
         AND MARC_EMPR = P_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_MARCA := 'Todas las marca';
    END;
    BEGIN
      SELECT DEP_DESC
        INTO V_DEPOSITO
        FROM STK_DEPOSITO
       WHERE DEP_EMPR = P_EMPRESA
         AND DEP_SUC = I_SUC
         AND DEP_CODIGO = I_DEP;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_DEPOSITO := 'Todos los Depositos';
    END;

    BEGIN
      SELECT LIN_DESC
        INTO V_LINEA
        FROM STK_LINEA
       WHERE LIN_CODIGO = I_LINEA
         AND LIN_EMPR = P_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_LINEA := 'Todas las lineas';
    END;
    BEGIN
      SELECT GRUP_DESC
        INTO V_GRUPO
        FROM STK_GRUPO
       WHERE GRUP_LINEA = I_LINEA
         AND GRUP_CODIGO = I_GRUPO
         AND GRUP_EMPR = P_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_GRUPO := 'Todos los grupos';
    END;

    BEGIN
      SELECT ENVA_DESC
        INTO V_ENVASE
        FROM STK_ENVASES
       WHERE ENVA_CODIGO = I_ENVASE
         AND ENVA_LINEA = I_LINEA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_ENVASE := 'Todos los Envases';
    END;
    ----- PARAMETROS DEL P_WHERE
    IF P_EMPRESA = 2 THEN
      IF P_EMPRESA IS NOT NULL THEN
        V_WHERE := V_WHERE || 'AND ARDE_EMPR=' || TO_CHAR(P_EMPRESA);
      END IF;
      IF I_SUC IS NOT NULL THEN
        V_WHERE := V_WHERE || 'AND ARDE_SUC=' || TO_CHAR(I_SUC);
      END IF;
      IF I_DEP IS NOT NULL THEN
        V_WHERE := V_WHERE || 'AND ARDE_DEP=' || TO_CHAR(I_DEP);
      END IF;
      IF I_MARCA IS NOT NULL THEN
        V_WHERE := V_WHERE || 'AND ART_MARCA=' || TO_CHAR(I_MARCA);
      END IF;
      IF I_LINEA IS NOT NULL THEN
        V_WHERE := V_WHERE || 'AND ART_LINEA=' || TO_CHAR(I_LINEA);
      END IF;
      IF I_GRUPO IS NOT NULL THEN
        V_WHERE := V_WHERE || 'AND ART_GRUPO=' || TO_CHAR(I_GRUPO);
      END IF;
      IF I_IMPRIMIR_CERO = 'N' THEN
        V_WHERE := V_WHERE || 'AND AEP_TOT_EXIST <> 0';
        V_WHERE := V_WHERE ||
                   ' HAVING SUM((ARDE_CANT_INI + NVL(TOT_ENT, 0)) - NVL(TOT_SAL, 0) ) <> 0';
      END IF;
    ELSE
      IF I_SUC IS NOT NULL THEN
        V_WHERE := V_WHERE || 'AND AD.ARDE_SUC = ' || TO_CHAR(I_SUC);
      END IF;
      IF I_DEP IS NOT NULL THEN
        V_WHERE := V_WHERE || 'AND AD.ARDE_DEP = ' || TO_CHAR(I_DEP);
      END IF;
      IF V_MARCA IS NOT NULL THEN
        V_WHERE := V_WHERE || 'AND SA.ART_MARCA IN ' || TO_CHAR(I_MARCA);
      END IF;

      IF I_LINEA IS NOT NULL THEN
        V_WHERE := V_WHERE || 'AND SA.ART_LINEA = ' || TO_CHAR(I_LINEA);
      END IF;
      IF I_ENVASE IS NOT NULL THEN
        V_WHERE := V_WHERE || 'AND SA.ART_ENVASE = ' || TO_CHAR(I_ENVASE);
      END IF;

      IF I_GRUPO IS NOT NULL THEN
        V_WHERE := V_WHERE || 'AND SA.ART_GRUPO = ' || TO_CHAR(I_GRUPO);
      END IF;
    END IF;

    V_PARAMETROS := 'P_FORMATO=' || APEX_UTIL.URL_ENCODE('pdf');
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_LOGIN=' ||
                    APEX_UTIL.URL_ENCODE(GEN_DEVUELVE_USER);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_PROGRAMA=' ||
                    APEX_UTIL.URL_ENCODE('STKL225');

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_PERIODO=' ||
                    APEX_UTIL.URL_ENCODE(I_PERIODO);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_DESC_EMPRESA=' ||
                    APEX_UTIL.URL_ENCODE(V_EMPR_DESC);

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_SUCURSAL=' ||
                    APEX_UTIL.URL_ENCODE(V_DESC_SUCURSAL);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_DESC_SUCURSAL=' ||
                    APEX_UTIL.URL_ENCODE(V_SUCURSAL);

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_DESC_PROGRAMA=' ||
                    APEX_UTIL.URL_ENCODE(V_EMPR_DESC ||
                                         ' IVENTARIO VALORIZADO POR SUCURSAL');

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_MARCA=' ||
                    APEX_UTIL.URL_ENCODE(V_MARCA);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_LINEA=' ||
                    APEX_UTIL.URL_ENCODE(V_LINEA);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_ENVASE=' ||
                    APEX_UTIL.URL_ENCODE(V_ENVASE);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_GRUPO=' ||
                    APEX_UTIL.URL_ENCODE(V_GRUPO);

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_TIPO_FECHA=' ||
                    APEX_UTIL.URL_ENCODE(P_TIPO_FECHA);
    /* V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'p_existencia='||
    APEX_UTIL.URL_ENCODE(V_IMPRIMIR);*/

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_FECHA_INICIO=' ||
                    APEX_UTIL.URL_ENCODE(I_FECHA_INICIO);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_FECHA_FIN=' ||
                    APEX_UTIL.URL_ENCODE(I_FECHA_FIN);

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_INVENTARIO=' ||
                    APEX_UTIL.URL_ENCODE('  al  ' || I_FECHA_FIN);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_DEPOSITO=' ||
                    APEX_UTIL.URL_ENCODE(V_DEPOSITO);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_WHERE=' ||
                    APEX_UTIL.URL_ENCODE(V_WHERE);

    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_SESSION_ID=' ||
                    APEX_UTIL.URL_ENCODE(I_SESSION_ID);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_LOGIN=' ||
                    APEX_UTIL.URL_ENCODE(GEN_DEVUELVE_USER);

    DELETE FROM GEN_PARAMETROS_REPORT WHERE USUARIO = GEN_DEVUELVE_USER;
    INSERT INTO GEN_PARAMETROS_REPORT
      (PARAMETROS, USUARIO, NOMBRE_REPORTE, FORMATO_SALIDA)
    VALUES
      (V_PARAMETROS, GEN_DEVUELVE_USER, V_REPORTE, 'pdf');
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001, SQLERRM);

  END PP_GENERAR_REPORTE;

  PROCEDURE PP_ENVIAR_CORREO(I_EMPRESA IN NUMBER) AS

    CURSOR CUR_DOC(C_PERI     IN NUMBER,
                   C_PERI_INI IN DATE,
                   C_FEC      IN DATE,
                   C_SUC      IN NUMBER,
                   C_DEP      IN NUMBER,
                   C_LINEA    IN VARCHAR2,
                   C_MARCA    IN VARCHAR2,
                   C_TRIGO    IN VARCHAR2 DEFAULT NULL) IS

      SELECT SUCURSAL,
             LINEA,
             SUM(DOLARES) DOLARES,
             SUM(GUARANIES) GUARANIES,
             SUM(KG_TOTAL) KG_TOTAL,
             TO_CHAR(SUM(EXISTENCIA), '999G999G999G999G999D00') EXISTENCIA,
             SUM(EXISTENCIA) EXISTENCIA_NUMERO
        FROM ((SELECT AD.ARDE_SUC SUC,
                      AD.ARDE_DEP,
                      S.SUC_DESC SUCURSAL,
                      ROUND(SUM((CANT_INI + NVL(TOT_ENT, 0)) -
                                NVL(TOT_SAL, 0)) * (P.AEP_COSTO_PROM_LOC),
                            0) GUARANIES,
                      ROUND(SUM((CANT_INI + NVL(TOT_ENT, 0)) -
                                NVL(TOT_SAL, 0)) * (P.AEP_COSTO_PROM_MON),
                            2) DOLARES,
                      SUM((CANT_INI + NVL(TOT_ENT, 0)) - NVL(TOT_SAL, 0)) EXISTENCIA,
                      SA.ART_KG_CONTENIDO *
                      SUM((CANT_INI + NVL(TOT_ENT, 0)) - NVL(TOT_SAL, 0)) KG_TOTAL,
                      SA.ART_UNID_MED UM,
                      NVL(SUM(AUX.EXISTENCIA), 0) DEPOSITO,
                      SA.ART_LINEA,
                      CASE
                        WHEN C_LINEA IS NOT NULL THEN
                         L.LIN_DESC
                        WHEN C_TRIGO = 'S' THEN
                         SA.ART_DESC_ABREV
                        ELSE
                         M.MARC_DESC
                      END LINEA
                 FROM STK_ARTICULO SA,
                      STK_LINEA L,
                      STK_DEPOSITO DP,
                      STK_ART_EMPR_PERI P,
                      GEN_SUCURSAL S,
                      STK_MARCA M,
                      (SELECT AD.ARDE_EMPR,
                              AD.ARDE_SUC,
                              AD.ARDE_DEP,
                              AD.ARDE_ART,
                              ARDE_CANT_INI_OPER CANT_INI,
                              0                  TOT_ENT,
                              0                  TOT_SAL
                         FROM STK_ARTICULO_DEPOSITO AD
                        WHERE AD.ARDE_EMPR = I_EMPRESA
                       UNION ALL
                       SELECT DOCU_EMPR,
                              DOCU_SUC_ORIG,
                              DOCU_DEP_ORIG,
                              DETA_ART,
                              0,
                              SUM(DECODE(OPER_ENT_SAL, 'E', DETA_CANT, 0)) AS TOT_ENT,
                              SUM(DECODE(OPER_ENT_SAL, 'S', DETA_CANT, 0)) AS TOT_SAL
                         FROM STK_OPERACION, STK_DOCUMENTO, STK_DOCUMENTO_DET
                        WHERE DOCU_CLAVE = DETA_CLAVE_DOC
                          AND DOCU_EMPR = DETA_EMPR
                          AND DOCU_CODIGO_OPER = OPER_CODIGO
                          AND DOCU_EMPR = OPER_EMPR
                          AND DOCU_EMPR = I_EMPRESA
                          AND DOCU_FEC_OPER BETWEEN C_PERI_INI AND C_FEC
                        GROUP BY DOCU_EMPR,
                                 DOCU_SUC_ORIG,
                                 DOCU_DEP_ORIG,
                                 DETA_ART) AD,
                      (SELECT SUM(B.ENTMERDET_CANT) EXISTENCIA,
                              C.OCOM_SUC SUCURSAL,
                              C.OCOM_DEPOSITO DEPOSITO,
                              D.OCOMD_ART ARTICULO,
                              D.OCOMD_EMPR EMPR
                         FROM STK_ENTRADA_MERC     A,
                              STK_ENTRADA_MERC_DET B,
                              STK_ORDEN_COMPRA     C,
                              STK_ORDEN_COMPRA_DET D
                        WHERE B.ENTMERDET_ORDEN = A.ENTMER_CLAVE_ORDEN
                          AND B.ENTMERDET_NUM = A.ENTMER_NUM_ENTR
                          AND B.ENTMERDET_EMPR = A.ENTMER_EMPR

                          AND C.OCOM_CLAVE = A.ENTMER_CLAVE_ORDEN
                          AND C.OCOM_EMPR = A.ENTMER_EMPR

                          AND D.OCOMD_NRO_ITEM = B.ENTMERDET_ITEM
                          AND D.OCOMD_CLAVE_OCOM = B.ENTMERDET_ORDEN
                          AND D.OCOMD_EMPR = B.ENTMERDET_EMPR

                          AND A.ENTMER_CLAVE_DOC_FIN IS NULL
                          AND C.OCOM_ESTADO = 'C'

                        GROUP BY C.OCOM_SUC,
                                 C.OCOM_DEPOSITO,
                                 D.OCOMD_ART,
                                 D.OCOMD_EMPR) AUX

                WHERE SA.ART_TIPO <> 4

                  AND SA.ART_CODIGO = AD.ARDE_ART
                  AND SA.ART_EMPR = AD.ARDE_EMPR

                  AND SA.ART_LINEA = L.LIN_CODIGO
                  AND SA.ART_EMPR = L.LIN_EMPR

                  AND AD.ARDE_EMPR = AUX.EMPR(+)
                  AND AD.ARDE_SUC = AUX.SUCURSAL(+)
                  AND AD.ARDE_DEP = AUX.DEPOSITO(+)
                  AND AD.ARDE_ART = AUX.ARTICULO(+)

                  AND AD.ARDE_SUC = S.SUC_CODIGO
                  AND AD.ARDE_EMPR = S.SUC_EMPR

                  AND P.AEP_ART = SA.ART_CODIGO
                  AND P.AEP_EMPR = SA.ART_EMPR
                  AND P.AEP_PERIODO = C_PERI

                  AND M.MARC_CODIGO = SA.ART_MARCA
                  AND M.MARC_EMPR = SA.ART_EMPR

                  AND AD.ARDE_EMPR = DP.DEP_EMPR
                  AND AD.ARDE_SUC = DP.DEP_SUC
                  AND AD.ARDE_DEP = DP.DEP_CODIGO

                  AND AD.ARDE_SUC = C_SUC

                  AND 'S' = CASE
                        WHEN NVL(C_TRIGO, 'N') = 'S' AND
                             NOT (SA.ART_DESC_ABREV LIKE 'TRIGO%') THEN
                         'N'
                        WHEN NVL(C_TRIGO, 'N') <> 'S' AND
                             SA.ART_DESC_ABREV LIKE 'TRIGO%' THEN
                         'N'
                        ELSE
                         'S'
                      END
                  AND (AD.ARDE_DEP IN
                      (SELECT REGEXP_SUBSTR(C_DEP, '[^,]+', 1, LEVEL)
                          FROM DUAL
                        CONNECT BY REGEXP_SUBSTR(C_DEP, '[^,]+', 1, LEVEL) IS NOT NULL) OR
                      C_DEP IS NULL)

                  AND (SA.ART_LINEA IN
                      (SELECT REGEXP_SUBSTR(C_LINEA, '[^,]+', 1, LEVEL)
                          FROM DUAL
                        CONNECT BY REGEXP_SUBSTR(C_LINEA, '[^,]+', 1, LEVEL) IS NOT NULL) OR
                      C_LINEA IS NULL)

                  AND (SA.ART_MARCA IN
                      (SELECT REGEXP_SUBSTR(C_MARCA, '[^,]+', 1, LEVEL)
                          FROM DUAL
                        CONNECT BY REGEXP_SUBSTR(C_MARCA, '[^,]+', 1, LEVEL) IS NOT NULL) OR
                      C_MARCA IS NULL)

                GROUP BY AD.ARDE_EMPR,
                         AD.ARDE_SUC,
                         S.SUC_DESC,
                         AD.ARDE_DEP,
                         AD.ARDE_ART,
                         SA.ART_LINEA,
                         SA.ART_GRUPO,
                         SA.ART_KG_CONTENIDO,
                         (P.AEP_COSTO_PROM_LOC),
                         (P.AEP_COSTO_PROM_MON),
                         SA.ART_UNID_MED,
                         CASE
                           WHEN C_LINEA IS NOT NULL THEN
                            L.LIN_DESC
                           WHEN C_TRIGO = 'S' THEN
                            SA.ART_DESC_ABREV
                           ELSE
                            M.MARC_DESC
                         END
               HAVING SUM(ROUND((CANT_INI + NVL(TOT_ENT, 0)) - NVL(TOT_SAL, 0), 3)) > 0)
              UNION ALL (SELECT 1 SUC,
                                19 ARDE_DEP,
                                'CASA CENTRAL' SUCURSAL,
                                5000000 GUARANIES,
                                5000000 DOLARES,
                                11600000 EXISTENCIA,
                                11600000 KG_TOTAL,
                                'KG' UM,
                                11600000 DEPOSITO,
                                95 ART_LINEA,
                                'TRIGO EN GRANEL 2021' LINEA
                           FROM DUAL
                          where C_SUC = 1
                            and C_DEP = 98
                            and C_MARCA = 2
                            and C_TRIGO = 'S') UNION ALL
              (SELECT 1 SUC,
                      99 ARDE_DEP,
                      'CASA CENTRAL' SUCURSAL,
                      0 GUARANIES,
                      0 DOLARES,
                      11600000 EXISTENCIA,
                      11600000 KG_TOTAL,
                      'KG' UM,
                      11600000 DEPOSITO,
                      95 ART_LINEA,
                      'TRIGO EN GRANEL 2021' LINEA
                 FROM DUAL
                where C_SUC = 1
                  and C_DEP = 99
                  and C_MARCA = 2
                  and C_TRIGO = 'S'))
       GROUP BY LINEA, SUCURSAL;

    V_PERI        NUMBER;
    V_PERI_INI    DATE;
    V_FECHA       DATE;
    V_HEAD_HTML   VARCHAR2(5000);
    V_FOOTER_HTML VARCHAR2(5000);
    V_TABLE       CLOB;
    V_MENSAJE     CLOB;
    V_TABLE_P1    CLOB;

    FUNCTION FP_PREPARA_PLANTA1(I_LINEA IN VARCHAR2, I_MONTO IN NUMBER, I_OTRO IN VARCHAR2 DEFAULT NULL)
      RETURN VARCHAR2 IS
      V_TABLE VARCHAR2(500);
    BEGIN
      IF I_OTRO IS NULL THEN
      V_TABLE := '<tr>';
      V_TABLE := V_TABLE || '<td>' || I_LINEA || '</td>';
      V_TABLE := V_TABLE || '<td class="derecha">' ||TO_CHAR(I_MONTO, '999G999G999G999G999D00') || '</td>';
/*
      V_TABLE := V_TABLE || '<td class="derecha" id="' || I_LINEA ||'">-</td>';*/
      V_TABLE := V_TABLE || '<td class="derecha" id="t-' || I_LINEA ||'">-</td>';
      /*V_TABLE := V_TABLE || '<td class="derecha" id="nt-' || I_LINEA ||
      '">-</td>';*/
      V_TABLE := V_TABLE || '</tr>';
      ELSE
      V_TABLE := '<tr>';
      V_TABLE := V_TABLE || '<td>' || I_LINEA || '</td>';
      V_TABLE := V_TABLE || '<td class="derecha">' ||
                 TO_CHAR(I_MONTO, '999G999G999G999G999D00') || '</td>';

      /*V_TABLE := V_TABLE || '<td class="derecha" id="' || I_LINEA ||
                 '">-</td>';
      V_TABLE := V_TABLE || '<td class="derecha" id="t-' || I_LINEA ||
                 '">-</td>';*/
      /*V_TABLE := V_TABLE || '<td class="derecha" id="nt-' || I_LINEA ||
      '">-</td>';*/
      V_TABLE := V_TABLE || '</tr>';
      
      END IF;
      RETURN V_TABLE;
    END FP_PREPARA_PLANTA1;

    FUNCTION FP_PREPARA_PLANTA2(I_TABLA IN VARCHAR2,
                                I_LINEA IN VARCHAR2,
                                I_MONTO IN NUMBER) RETURN VARCHAR2 IS

    BEGIN
      RETURN REPLACE(I_TABLA,
                     '<td class="derecha" id="' || I_LINEA || '">-</td>',
                     '<td class="derecha">' ||
                     TO_CHAR(I_MONTO, '999G999G999G999G999D00') || '</td>');

    END FP_PREPARA_PLANTA2;

    FUNCTION FP_PREPARA_TAGRO(I_TABLA IN VARCHAR2,
                              I_LINEA IN VARCHAR2,
                              I_MONTO IN NUMBER) RETURN VARCHAR2 IS

    BEGIN
      RETURN REPLACE(I_TABLA,
                     '<td class="derecha" id="t-' || I_LINEA || '">-</td>',
                     '<td class="derecha">' ||
                     TO_CHAR(I_MONTO, '999G999G999G999G999D00') || '</td>');

    END FP_PREPARA_TAGRO;

    FUNCTION FP_PREPARA_NUEVA_TOLEDO(I_TABLA IN VARCHAR2,
                                     I_LINEA IN VARCHAR2,
                                     I_MONTO IN NUMBER) RETURN VARCHAR2 IS

    BEGIN
      RETURN REPLACE(I_TABLA,
                     '<td class="derecha" id="nt-' || I_LINEA || '">-</td>',
                     '<td class="derecha">' ||
                     TO_CHAR(I_MONTO, '999G999G999G999G999D00') || '</td>');

    END FP_PREPARA_NUEVA_TOLEDO;

    FUNCTION FP_FORMATO(I_NUMERO IN NUMBER) RETURN VARCHAR2 AS
    BEGIN
      RETURN TO_CHAR(I_NUMERO, '999G999G999G999G999D00');
    END FP_FORMATO;
    

    PROCEDURE PP_CORREO_SEGURO IS
      V_EMAIL_SEGURO       VARCHAR2(5000);
      V_EMAIL_CC_SEGURO    VARCHAR2(5000);
      V_EMAIL_CCO_SEGURO   VARCHAR2(200);
      V_MONTO_CNTR         NUMBER := 0;
      V_MONTO_CNTR_P1      NUMBER := 0;
      V_MONTO_MOL2         NUMBER := 0;
      V_MONTO_TAGRO        NUMBER := 0;
      V_MONTO_NUEVA_TOLEDO NUMBER := 0;
      V_PROMEDIO_MES       NUMBER := 0;

    BEGIN

      V_TABLE := '';
      
      SELECT T.CO_PARA, T.CO_CC, T.CO_CCO
        INTO V_EMAIL_SEGURO, V_EMAIL_CC_SEGURO, V_EMAIL_CCO_SEGURO
        FROM GEN_CORREOS T
       WHERE T.CO_ACCION = 'Correo_Seguro';

      --- *--------************* SUCURSAL 1
     /* FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 1,
                       C_DEP      => '',
                       C_LINEA    => '10,15,16', ----*** HARINA, FIDEO , BALANCEADO
                       C_MARCA    => 3) LOOP

        V_TABLE      := V_TABLE || FP_PREPARA_PLANTA1(C.LINEA,  '');
        V_MONTO_CNTR := 0;--V_MONTO_CNTR + NVL(C.DOLARES, 0);
        
        V_TABLE_P1      := V_TABLE_P1 || FP_PREPARA_PLANTA1(C.LINEA, NVL(C.DOLARES, 0));
        V_MONTO_CNTR_P1 := V_MONTO_CNTR_P1 + NVL(C.DOLARES, 0);
      END LOOP;

      --*-** ADITIVOS

      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 1,
                       C_DEP      => '',
                       C_LINEA    => '',
                       C_MARCA    => '4') LOOP

        V_TABLE      := V_TABLE || FP_PREPARA_PLANTA1(C.LINEA,  '');
        V_MONTO_CNTR := 0;--V_MONTO_CNTR + NVL(C.DOLARES, 0);
        
        V_TABLE_P1      := V_TABLE_P1 || FP_PREPARA_PLANTA1(C.LINEA, NVL(C.DOLARES, 0));
        V_MONTO_CNTR_P1 := V_MONTO_CNTR_P1 + NVL(C.DOLARES, 0);
      END LOOP;

      --*-** ENVASES

      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 1,
                       C_DEP      => '1,11',
                       C_LINEA    => '',
                       C_MARCA    => '5') LOOP

        V_TABLE      := V_TABLE || FP_PREPARA_PLANTA1(C.LINEA,  '');
        V_MONTO_CNTR := 0;--V_MONTO_CNTR + NVL(C.DOLARES, 0);
        
        V_TABLE_P1      := V_TABLE_P1 || FP_PREPARA_PLANTA1(C.LINEA, NVL(C.DOLARES, 0));
        V_MONTO_CNTR_P1 := V_MONTO_CNTR_P1 + NVL(C.DOLARES, 0);
      END LOOP;

      ---*** ART DISTRIBUCION

      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 1,
                       C_DEP      => '1,11',
                       C_LINEA    => '',
                       C_MARCA    => '11') LOOP

        V_TABLE      := V_TABLE || FP_PREPARA_PLANTA1(C.LINEA,  '');
        V_MONTO_CNTR := 0;--V_MONTO_CNTR + NVL(C.DOLARES, 0);
        
        V_TABLE_P1      := V_TABLE_P1 || FP_PREPARA_PLANTA1(C.LINEA, NVL(C.DOLARES, 0));
        V_MONTO_CNTR_P1 := V_MONTO_CNTR_P1 + NVL(C.DOLARES, 0);
      END LOOP;

      ---** GRANOS Y EXPELLER
      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 1,
                       C_DEP      => '1',
                       C_LINEA    => '',
                       C_MARCA    => '2') LOOP

        V_TABLE      := V_TABLE || FP_PREPARA_PLANTA1(C.LINEA, '');
        V_MONTO_CNTR := 0;--V_MONTO_CNTR + NVL(C.DOLARES, 0);
        
        V_TABLE_P1      := V_TABLE_P1 || FP_PREPARA_PLANTA1(C.LINEA, NVL(C.DOLARES, 0));
        V_MONTO_CNTR_P1 := V_MONTO_CNTR_P1 + NVL(C.DOLARES, 0);
      END LOOP;

      --*** TRIGO

      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 1,
                       C_DEP      => '1',
                       C_LINEA    => '',
                       C_MARCA    => '2',
                       C_TRIGO    => 'S') LOOP

        V_TABLE      := V_TABLE || FP_PREPARA_PLANTA1(C.LINEA,  '');
        V_MONTO_CNTR := 0;--V_MONTO_CNTR + NVL(C.DOLARES, 0);
        
        V_TABLE_P1      := V_TABLE_P1 || FP_PREPARA_PLANTA1(C.LINEA, NVL(C.DOLARES, 0));
        V_MONTO_CNTR_P1 := V_MONTO_CNTR_P1 + NVL(C.DOLARES, 0);
      END LOOP;
*/
      ------------------SUCURSAL 5 --- *****************

      ---*** ART DISTRIBUCION
      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 5,
                       C_DEP      => '5',
                       C_LINEA    => '',
                       C_MARCA    => '11') LOOP

        V_TABLE      := V_TABLE ||FP_PREPARA_PLANTA1(I_LINEA => C.LINEA,
                                           I_MONTO => C.DOLARES);
        V_MONTO_MOL2 := V_MONTO_MOL2 + NVL(C.DOLARES, 0);
      END LOOP;

      --*-** ADITIVOS
      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 5,
                       C_DEP      => '',
                       C_LINEA    => '',
                       C_MARCA    => '4') LOOP

        V_TABLE      := V_TABLE ||FP_PREPARA_PLANTA1(I_LINEA => C.LINEA,
                                           I_MONTO => C.DOLARES);
        V_MONTO_MOL2 := V_MONTO_MOL2 + NVL(C.DOLARES, 0);
      END LOOP;

      --*-**  ENVASES
      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 5,
                       C_DEP      => '1,3',
                       C_LINEA    => '',
                       C_MARCA    => '5') LOOP

          V_TABLE      := V_TABLE ||FP_PREPARA_PLANTA1(I_LINEA => C.LINEA,
                                           I_MONTO => C.DOLARES);
        V_MONTO_MOL2 := V_MONTO_MOL2 + NVL(C.DOLARES, 0);
      END LOOP;

      --- **** HARINA

      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 5,
                       C_DEP      => '',
                       C_LINEA    => '10',
                       C_MARCA    => 3) LOOP

         V_TABLE      := V_TABLE ||FP_PREPARA_PLANTA1(I_LINEA => C.LINEA,
                                           I_MONTO => C.DOLARES);
        V_MONTO_MOL2 := V_MONTO_MOL2 + NVL(C.DOLARES, 0);
      END LOOP;

      ---*/** BALANCEADOS
      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 5,
                       C_DEP      => '2,5',
                       C_LINEA    => '16',
                       C_MARCA    => 3) LOOP

        V_TABLE      := V_TABLE ||FP_PREPARA_PLANTA1(I_LINEA => C.LINEA,
                                           I_MONTO => C.DOLARES);
        V_MONTO_MOL2 := V_MONTO_MOL2 + NVL(C.DOLARES, 0);
      END LOOP;

      --*** GRANOS Y EXPELLER

      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 5,
                       C_DEP      => '1',
                       C_LINEA    => '',
                       C_MARCA    => 2) LOOP
                       
                       
          V_TABLE      := V_TABLE ||FP_PREPARA_PLANTA1(I_LINEA => C.LINEA,
                                           I_MONTO => C.DOLARES);
        V_MONTO_MOL2 := V_MONTO_MOL2 + NVL(C.DOLARES, 0);       

      END LOOP;

      ---- TRIGO
      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 5,
                       C_DEP      => '1',
                       C_LINEA    => '',
                       C_MARCA    => 2,
                       C_TRIGO    => 'S') LOOP

           V_TABLE      := V_TABLE ||FP_PREPARA_PLANTA1(I_LINEA => C.LINEA,
                                           I_MONTO => C.DOLARES);
        V_MONTO_MOL2 := V_MONTO_MOL2 + NVL(C.DOLARES, 0);
      END LOOP;

      ---******-TAGRO TRIGO

      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 1,
                       -- C_DEP      => 19,
                       C_DEP   => 99999, ---**** para transagro que solo muestre lo manual
                       C_LINEA => '',
                       C_MARCA => 2,
                       C_TRIGO => 'S') LOOP

        V_TABLE       := FP_PREPARA_TAGRO(I_TABLA => V_TABLE,
                                          I_LINEA => C.LINEA,
                                          I_MONTO => C.DOLARES);
        V_MONTO_TAGRO := V_MONTO_TAGRO + NVL(C.DOLARES, 0);
      END LOOP;

      ----********* NUEVA TOLEDO
      /* FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 1,
                       C_DEP      => 99, ----***** Nueva toledo manual
                       C_LINEA    => '',
                       C_MARCA    => 2,
                       C_TRIGO    => 'S') LOOP

        V_TABLE              := FP_PREPARA_NUEVA_TOLEDO(I_TABLA => V_TABLE,
                                                        I_LINEA => C.LINEA,
                                                        I_MONTO => C.DOLARES);
        V_MONTO_NUEVA_TOLEDO := V_MONTO_NUEVA_TOLEDO + NVL(C.DOLARES, 0);
      END LOOP;*/

      --V_MONTO_TAGRO := 0;
      ---***** total por columna
  
       V_TABLE := V_TABLE || '<tr class="total">';
       
       --V_TABLE := V_TABLE || '<td>TOTAL</td>';
        
       V_TABLE := V_TABLE || '<td>TOTAL</td> <td class="derecha"> ' ||
                 TO_CHAR(V_MONTO_MOL2, '999G999G999G999G999D00') || '</td>';
           
  
      V_TABLE := V_TABLE || '<td class="derecha">' ||
                 TO_CHAR(V_MONTO_TAGRO, '999G999G999G999G999D00') ||
                 '</td>';

      V_TABLE := V_TABLE || '</tr>';
      
      --******* total general
      V_TABLE := V_TABLE ||
                 '<tr><td>TOTAL GENERAL DEL DIA</td><td colspan="3" class="centro">';
      V_TABLE := V_TABLE || TO_CHAR(0 + V_MONTO_MOL2 +
                                    V_MONTO_TAGRO + V_MONTO_NUEVA_TOLEDO,
                                    '999G999G999G999G999D00');
      V_TABLE := V_TABLE || '</td></tr>';

      SELECT ROUND(AVG(TOTAL), 2) PROMEDIO
        INTO V_PROMEDIO_MES
        FROM (SELECT SUM(T.EDEC_TOTAL_USD) TOTAL
                FROM STK_EMAIL_DECLARACION_LOG T
               WHERE EDEC_TIPO = 'SEGURO'
                 AND EDEC_EMPR = I_EMPRESA
                 AND T.EDEC_OBS IS NULL
                 AND TO_CHAR(T.EDEC_FECHA, 'mm/yyyy') =
                     TO_CHAR(V_FECHA, 'mm/yyyy')
               GROUP BY T.EDEC_FECHA);

      V_TABLE := V_TABLE ||
                 '<tr class="total"><td>PROMEDIO DEL MES</td><td colspan="3" class="centro">' ||
                 TO_CHAR(V_PROMEDIO_MES, '999G999G999G999G999D00') ||
                 '</td></tr>';

      V_HEAD_HTML := '<!DOCTYPE html>
                        <html>
                        <head>
                        <style>
                        table {
                          border-collapse: collapse;
                          width: 700px;
                        }
                        table, th, td {
                          border: 1px solid black;
                        }
                        body {
                          border:5px;
                        }
                         th{
                         background-color: #eee;
                        }
                        .derecha{
                        text-align: right;
                        }
                        .centro{
                        text-align: center;
                        }
                        .total{
                          background-color: #eee;
                         }
                        </style>
                        </head>
                        <body>';

      V_HEAD_HTML := V_HEAD_HTML ||
                     '<h2> Declaracion de stock de mercaderia de fecha ' ||
                     TO_CHAR(V_FECHA, 'DD/MM/YYYY') || '</h2>';

      V_HEAD_HTML := V_HEAD_HTML || ' <table><tr>';
      V_HEAD_HTML := V_HEAD_HTML || '<th>HILAGRO S.A</th>';
   --   V_HEAD_HTML := V_HEAD_HTML || '<th>PLANTA 1</th>';
      V_HEAD_HTML := V_HEAD_HTML || '<th>PLANTA 2 Y 3</th>';
      V_HEAD_HTML := V_HEAD_HTML || '<th>TRANSAGRO</th>';
      -- V_HEAD_HTML := V_HEAD_HTML || '<th>NUEVA TOLEDO</th>';
      V_HEAD_HTML := V_HEAD_HTML || '</tr>';

      V_FOOTER_HTML := '</table></body></html>';

      V_MENSAJE := V_HEAD_HTML || V_TABLE || V_FOOTER_HTML;

      DBMS_OUTPUT.PUT_LINE(V_MENSAJE);
      ---***

      -- CALL THE PROCEDURE
      IF V_EMAIL_SEGURO IS NOT NULL THEN
      SEND_EMAIL(P_SENDER     => 'noreply',
                 P_RECIPIENTS => V_EMAIL_SEGURO,
                 P_CC         => V_EMAIL_CC_SEGURO,
                 P_BCC        => V_EMAIL_CCO_SEGURO,
                 P_SUBJECT    => 'Declaracion de Inventario',
                 P_MESSAGE    => V_MENSAJE,
                 P_MIME_TYPE  => 'text/html');
      END IF;

   /*   INSERT INTO STK_EMAIL_DECLARACION_LOG
        (EDEC_FECHA, EDEC_ITEM, EDEC_TOTAL_USD, EDEC_FEC_GRAB, EDEC_EMPR)
      VALUES
        (V_FECHA, 1, V_MONTO_CNTR, SYSDATE, I_EMPRESA);*/
      ---****

      INSERT INTO STK_EMAIL_DECLARACION_LOG
        (EDEC_FECHA, EDEC_ITEM, EDEC_TOTAL_USD, EDEC_FEC_GRAB, EDEC_EMPR)
      VALUES
        (V_FECHA, 2, V_MONTO_MOL2, SYSDATE, I_EMPRESA);

      ---***
      INSERT INTO STK_EMAIL_DECLARACION_LOG
        (EDEC_FECHA, EDEC_ITEM, EDEC_TOTAL_USD, EDEC_FEC_GRAB, EDEC_EMPR)
      VALUES
        (V_FECHA, 3, V_MONTO_TAGRO, SYSDATE, I_EMPRESA);

      COMMIT;

    END PP_CORREO_SEGURO;
    
    
    
    PROCEDURE PP_CORREO_P1 IS
      V_EMAIL_SEGURO       VARCHAR2(5000);
      V_EMAIL_CC_SEGURO    VARCHAR2(5000);
      V_EMAIL_CCO_SEGURO   VARCHAR2(200);
      V_MONTO_CNTR         NUMBER := 0;
      V_MONTO_CNTR_P1      NUMBER := 0;
      V_MONTO_MOL2         NUMBER := 0;
      V_MONTO_TAGRO        NUMBER := 0;
      V_MONTO_NUEVA_TOLEDO NUMBER := 0;
      V_PROMEDIO_MES       NUMBER := 0;

    BEGIN

      V_TABLE := '';
      
      SELECT T.CO_PARA, T.CO_CC, T.CO_CCO
        INTO V_EMAIL_SEGURO, V_EMAIL_CC_SEGURO, V_EMAIL_CCO_SEGURO
        FROM GEN_CORREOS T
       WHERE T.CO_ACCION = 'Correo_Seguro_P1';

      --- *--------************* SUCURSAL 1
      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 1,
                       C_DEP      => '',
                       C_LINEA    => '10,15,16', ----*** HARINA, FIDEO , BALANCEADO
                       C_MARCA    => 3) LOOP

        V_TABLE      := V_TABLE || FP_PREPARA_PLANTA1(C.LINEA,  NVL(C.DOLARES, 0),'I');
        V_MONTO_CNTR := V_MONTO_CNTR + NVL(C.DOLARES, 0);
      END LOOP;

      --*-** ADITIVOS

      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 1,
                       C_DEP      => '',
                       C_LINEA    => '',
                       C_MARCA    => '4') LOOP

       V_TABLE      := V_TABLE || FP_PREPARA_PLANTA1(C.LINEA,  NVL(C.DOLARES, 0),'I');
       V_MONTO_CNTR := V_MONTO_CNTR + NVL(C.DOLARES, 0);
      END LOOP;

      --*-** ENVASES

      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 1,
                       C_DEP      => '1,11',
                       C_LINEA    => '',
                       C_MARCA    => '5') LOOP

        V_TABLE      := V_TABLE || FP_PREPARA_PLANTA1(C.LINEA,  NVL(C.DOLARES, 0),'I');
        V_MONTO_CNTR := V_MONTO_CNTR + NVL(C.DOLARES, 0);
      END LOOP;

      ---*** ART DISTRIBUCION

      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 1,
                       C_DEP      => '1,11',
                       C_LINEA    => '',
                       C_MARCA    => '11') LOOP
                       
        V_TABLE      := V_TABLE || FP_PREPARA_PLANTA1(C.LINEA,  NVL(C.DOLARES, 0),'I');
        V_MONTO_CNTR := V_MONTO_CNTR + NVL(C.DOLARES, 0);
      END LOOP;

      ---** GRANOS Y EXPELLER
      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 1,
                       C_DEP      => '1',
                       C_LINEA    => '',
                       C_MARCA    => '2') LOOP

          V_TABLE      := V_TABLE || FP_PREPARA_PLANTA1(C.LINEA,  NVL(C.DOLARES, 0),'I');
        V_MONTO_CNTR := V_MONTO_CNTR + NVL(C.DOLARES, 0);
      END LOOP;

      --*** TRIGO

      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 1,
                       C_DEP      => '1',
                       C_LINEA    => '',
                       C_MARCA    => '2',
                       C_TRIGO    => 'S') LOOP

        V_TABLE      := V_TABLE || FP_PREPARA_PLANTA1(C.LINEA,  NVL(C.DOLARES, 0),'I');
        V_MONTO_CNTR := V_MONTO_CNTR + NVL(C.DOLARES, 0);
        
      END LOOP;


      ----********* NUEVA TOLEDO
      /* FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 1,
                       C_DEP      => 99, ----***** Nueva toledo manual
                       C_LINEA    => '',
                       C_MARCA    => 2,
                       C_TRIGO    => 'S') LOOP

        V_TABLE              := FP_PREPARA_NUEVA_TOLEDO(I_TABLA => V_TABLE,
                                                        I_LINEA => C.LINEA,
                                                        I_MONTO => C.DOLARES);
        V_MONTO_NUEVA_TOLEDO := V_MONTO_NUEVA_TOLEDO + NVL(C.DOLARES, 0);
      END LOOP;*/

      --V_MONTO_TAGRO := 0;
      ---***** total por columna
      V_TABLE := V_TABLE || '<tr class="total">';
      
     V_TABLE := V_TABLE || '<td>TOTAL</td><td class="derecha">' ||
                 TO_CHAR(V_MONTO_CNTR, '999G999G999G999G999D00') || '</td>';


                 
                 

      /*V_TABLE := V_TABLE || '<td class="derecha">' ||
      TO_CHAR(V_MONTO_NUEVA_TOLEDO, '999G999G999G999G999D00') ||
      '</td>';*/

      V_TABLE := V_TABLE || '</tr>';

      --******* total general
      V_TABLE := V_TABLE ||
                 '<tr><td>TOTAL GENERAL DEL DIA</td><td colspan="4" class="centro">';
      V_TABLE := V_TABLE || TO_CHAR(V_MONTO_CNTR ,'999G999G999G999G999D00');
      V_TABLE := V_TABLE || '</td></tr>';

      SELECT ROUND(AVG(TOTAL), 2) PROMEDIO
        INTO V_PROMEDIO_MES
        FROM (SELECT SUM(T.EDEC_TOTAL_USD) TOTAL
                FROM STK_EMAIL_DECLARACION_LOG T
               WHERE EDEC_TIPO = 'SEGURO'
                 AND EDEC_EMPR = I_EMPRESA
                 AND EDEC_OBS = 'PLANTA1'
                 AND TO_CHAR(T.EDEC_FECHA, 'mm/yyyy') =
                     TO_CHAR(V_FECHA, 'mm/yyyy')
               GROUP BY T.EDEC_FECHA);

      V_TABLE := V_TABLE ||
                 '<tr class="total"><td>PROMEDIO DEL MES</td><td colspan="4" class="centro">' ||
                 TO_CHAR(V_PROMEDIO_MES, '999G999G999G999G999D00') ||
                 '</td></tr>';

      V_HEAD_HTML := '<!DOCTYPE html>
                        <html>
                        <head>
                        <style>
                        table {
                          border-collapse: collapse;
                          width: 700px;
                        }
                        table, th, td {
                          border: 1px solid black;
                        }
                        body {
                          border:5px;
                        }
                         th{
                         background-color: #eee;
                        }
                        .derecha{
                        text-align: right;
                        }
                        .centro{
                        text-align: center;
                        }
                        .total{
                          background-color: #eee;
                         }
                        </style>
                        </head>
                        <body>';

      V_HEAD_HTML := V_HEAD_HTML ||
                     '<h2> Declaracion de stock de mercaderia de fecha ' ||
                     TO_CHAR(V_FECHA, 'DD/MM/YYYY') || '</h2>';

      V_HEAD_HTML := V_HEAD_HTML || ' <table><tr>';
      V_HEAD_HTML := V_HEAD_HTML || '<th>HILAGRO S.A</th>';
      V_HEAD_HTML := V_HEAD_HTML || '<th>PLANTA 1</th>';
      -- V_HEAD_HTML := V_HEAD_HTML || '<th>NUEVA TOLEDO</th>';
      V_HEAD_HTML := V_HEAD_HTML || '</tr>';

      V_FOOTER_HTML := '</table></body></html>';

      V_MENSAJE := V_HEAD_HTML || V_TABLE || V_FOOTER_HTML;

    
      ---***

      -- CALL THE PROCEDURE
      IF V_EMAIL_SEGURO IS NOT NULL THEN
      SEND_EMAIL(P_SENDER     => 'noreply',
                 P_RECIPIENTS => V_EMAIL_SEGURO,
                 P_CC         => V_EMAIL_CC_SEGURO,
                 P_BCC        => V_EMAIL_CCO_SEGURO,
                 P_SUBJECT    => 'Declaracion de Inventario',
                 P_MESSAGE    => V_MENSAJE,
                 P_MIME_TYPE  => 'text/html');
      END IF;

      INSERT INTO STK_EMAIL_DECLARACION_LOG
        (EDEC_FECHA, EDEC_ITEM,EDEC_OBS, EDEC_TOTAL_USD, EDEC_FEC_GRAB, EDEC_EMPR)
      VALUES
        (V_FECHA, 1,'PLANTA1', V_MONTO_CNTR, SYSDATE, I_EMPRESA);
      ---****

      COMMIT;
      END PP_CORREO_P1;

    PROCEDURE PP_CORREO_GICAL IS
      CURSOR CUR_MOV IS
        SELECT DECODE(OPERACION, 'E', 'Entrada', 'Salida') OPERACION,
               SUM(CENTRAL) CENTRAL,
               SUM(MOLINO2) MOLINO2
          FROM (SELECT OPERACION,
                       NVL(CENTRAL, 0) CENTRAL,
                       NVL(MOLINO2, 0) MOLINO2
                  FROM (SELECT OPER_ENT_SAL OPERACION,
                               SUM(MOVIMIENTO) ENTRADA,
                               DOCU_SUC_ORIG
                          FROM (SELECT DETA_ART,
                                       DETA_EMPR,
                                       DETA_IMP_NETO_LOC,
                                       DETA_IMP_NETO_MON,
                                       (DETA_IMP_BRUTO_LOC - DETA_IMP_NETO_LOC) DETA_IMP_DTO,
                                       DOCU_CODIGO_OPER,
                                       DOCU_NRO_DOC,
                                       DOCU_EMPR,
                                       DOCU_SUC_ORIG,
                                       DOCU_DEP_ORIG,
                                       DOCU_SUC_DEST,
                                       DOCU_DEP_DEST,
                                       C.DOCU_FEC_EMIS DOCU_FEC_EMIS,
                                       C.DOCU_FEC_OPER FECHA_OPERACION,
                                       DOCU_PRE_COMPRA,
                                       ART_DESC_ABREV,
                                       ART_DESC,
                                       OPER_DESC,
                                       DOCU_CLI,
                                       DOCU_PROV,
                                       OPER_ENT_SAL,
                                       MON_SIMBOLO,
                                       MON_DEC_IMP,
                                       DETA_NRO_FORMULA,
                                       DECODE(OPER_ENT_SAL, 'E', DETA_CANT, 0) DETA_CANT_ENTRADA,
                                       DECODE(OPER_ENT_SAL, 'S', DETA_CANT, 0) DETA_CANT_SALIDA,
                                       DECODE(OPER_ENT_SAL, 'S', -1, 1) *
                                       DETA_CANT MOVIMIENTO,
                                       DETA_CANT CANT_UNI,
                                       DECODE(OPER_ENT_SAL,
                                              'E',
                                              DETA_CANT *
                                              NVL(ART_KG_CONTENIDO, 1),
                                              0) DETA_CANT_ENTKG,
                                       DECODE(OPER_ENT_SAL,
                                              'S',
                                              DETA_CANT *
                                              NVL(ART_KG_CONTENIDO, 1),
                                              0) DETA_CANT_SALKG,
                                       DETA_CLAVE_CONCEPTO
                                  FROM GEN_MONEDA           A,
                                       STK_OPERACION        B,
                                       STK_DOCUMENTO        C,
                                       STK_ARTICULO         D,
                                       STK_ARTICULO_EMPRESA E,
                                       STK_DOCUMENTO_DET    F
                                 WHERE MON_CODIGO = DOCU_MON
                                   AND MON_EMPR = DOCU_EMPR
                                   AND OPER_CODIGO = DOCU_CODIGO_OPER
                                   AND OPER_EMPR = DOCU_EMPR
                                   AND DOCU_CLAVE = DETA_CLAVE_DOC
                                   AND DOCU_EMPR = DETA_EMPR
                                   AND ART_CODIGO = DETA_ART
                                   AND ART_EMPR = DETA_EMPR
                                   AND ART_EMPR = I_EMPRESA
                                   AND D.ART_DESC LIKE 'TRIGO%'
                                   AND D.ART_EST = 'A'
                                   AND AREM_EMPR = DETA_EMPR
                                   AND AREM_ART = DETA_ART
                                   AND DOCU_CODIGO_OPER IN
                                       (3, 8, 9, 30, 12, 13, 31, 14)
                                   AND DOCU_SUC_ORIG IN (1, 5)
                                   AND DOCU_DEP_ORIG IN (1)
                                   AND DOCU_FEC_OPER = (V_FECHA))
                         GROUP BY OPER_ENT_SAL, DOCU_SUC_ORIG)
                PIVOT(SUM(ENTRADA)
                   FOR DOCU_SUC_ORIG IN(1 CENTRAL, 5 MOLINO2)) X
                UNION
                SELECT 'E' OPERACION, 0 CENTRAL, 0 MOLINO2
                  FROM DUAL
                UNION
                SELECT 'S' OPERACION, 0 CENTRAL, 0 MOLINO2
                  FROM DUAL)
         GROUP BY DECODE(OPERACION, 'E', 'Entrada', 'Salida');
      V_EMAIL_GICAL     VARCHAR2(1000);
      V_EMAIL_CC_GICAL  VARCHAR2(600);
      V_EMAIL_CCO_GICAL VARCHAR2(200);
      V_EXISTENCIA_1    NUMBER := 0;
      V_EXISTENCIA_2    NUMBER := 0;
    BEGIN
      SELECT T.CO_PARA, T.CO_CC, T.CO_CCO
        INTO V_EMAIL_GICAL, V_EMAIL_CC_GICAL, V_EMAIL_CCO_GICAL
        FROM GEN_CORREOS T
       WHERE T.CO_ACCION = 'Correo_Gical';
    
      V_TABLE := '';

      FOR C IN CUR_MOV LOOP

        V_TABLE := V_TABLE || '<tr><td>' || C.OPERACION ||
                   '</td><td class="derecha">' ||
                   TO_CHAR(C.CENTRAL, '999G999G999G999G999D00') ||
                   '</td><td class="derecha">' ||
                   TO_CHAR(C.MOLINO2, '999G999G999G999G999D00') ||
                   '</td></tr>';
      END LOOP;

      V_TABLE := V_TABLE || '<tr><td>Stock final</td>';

      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 1,
                       C_DEP      => 1,
                       C_LINEA    => '',
                       C_MARCA    => 2,
                       C_TRIGO    => 'S') LOOP

        V_EXISTENCIA_1 := V_EXISTENCIA_1 + C.EXISTENCIA_NUMERO;
      END LOOP;

      V_TABLE := V_TABLE || '<td class="derecha">' ||
                 FP_FORMATO(V_EXISTENCIA_1) || '</td>';

      FOR C IN CUR_DOC(C_PERI     => V_PERI,
                       C_PERI_INI => V_PERI_INI,
                       C_FEC      => V_FECHA,
                       C_SUC      => 5,
                       C_DEP      => 1,
                       C_LINEA    => '',
                       C_MARCA    => 2,
                       C_TRIGO    => 'S') LOOP

        V_EXISTENCIA_2 := V_EXISTENCIA_2 + C.EXISTENCIA_NUMERO;
      END LOOP;
      V_TABLE := V_TABLE || '<td class="derecha">' ||
                 FP_FORMATO(V_EXISTENCIA_2) || '</td>';
      V_TABLE := V_TABLE || '</tr>';

      V_HEAD_HTML := '<!DOCTYPE html>
                        <html>
                        <head>
                        <style>
                        table {
                          border-collapse: collapse;
                          width: 500px;
                        }
                        table, th, td {
                          border: 1px solid black;
                        }
                        body {
                          border:5px;
                        }
                         th{
                         background-color: #eee;
                        }
                        .derecha{
                        text-align: right;
                        }
                        .centro{
                        text-align: center;
                        }

                        </style>
                        </head>
                        <body>

                        <h2> Declaracion de Trigo de fecha ' ||
                     TO_CHAR(V_FECHA, 'DD/MM/YYYY') ||
                     '</h2>
                        <table>
                        <tr>
                        <th>HILAGRO S.A</th>
                        <th>PLANTA 1</th>
                        <th>PLANTA 2</th>
                        </tr>';

      V_FOOTER_HTML := '</table></body>
</html>';

      V_MENSAJE := V_HEAD_HTML || V_TABLE || V_FOOTER_HTML;

      -- CALL THE PROCEDURE
      IF V_EMAIL_GICAL IS NOT NULL THEN
      SEND_EMAIL(P_SENDER     => 'noreply',
                 P_RECIPIENTS => V_EMAIL_GICAL,
                 P_CC         => V_EMAIL_CC_GICAL,
                 P_BCC        => V_EMAIL_CCO_GICAL,
                 P_SUBJECT    => 'Declaracion GICAL',
                 P_MESSAGE    => V_MENSAJE,
                 P_MIME_TYPE  => 'text/html');
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE(SQLERRM);
    END PP_CORREO_GICAL;

  BEGIN

    SELECT P.PERI_CODIGO + 1, P.PERI_FEC_INI --, P.PERI_FEC_FIN
      INTO V_PERI, V_PERI_INI
      FROM STK_CONFIGURACION C, STK_PERIODO P ---, STK_PERIODO
     WHERE C.CONF_EMPR = P.PERI_EMPR
       AND C.CONF_PERIODO_ACT = P.PERI_CODIGO
       AND CONF_EMPR = I_EMPRESA;

    --  V_FECHA := '21/10/2021';
    --  PP_CORREO_SEGURO;
    IF DIAS_HABILES_VENDEDORES(VFECDESDE => TRUNC(SYSDATE),
                               VFECHASTA => TRUNC(SYSDATE)) <> 0 THEN

     LOOP

        SELECT NVL(MAX(T.EDEC_FECHA), TRUNC(SYSDATE, 'MM') - 2) + 1 ULTIMA_FECHA
          INTO V_FECHA
          FROM STK_EMAIL_DECLARACION_LOG T
         WHERE EDEC_TIPO = 'SEGURO'
           AND T.EDEC_EMPR = I_EMPRESA;

        EXIT WHEN(TRUNC(SYSDATE) - V_FECHA) < 1;

        PP_CORREO_SEGURO;
        PP_CORREO_GICAL;
        PP_CORREO_P1;
      END LOOP;
    END IF;

  END PP_ENVIAR_CORREO;
  
  
  
  

END STKL025_PACK;
/
