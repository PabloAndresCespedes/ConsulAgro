prompt --application/set_environment
set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_200100 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2020.03.31'
,p_release=>'20.1.0.00.13'
,p_default_workspace_id=>2933390818164287
,p_default_application_id=>109
,p_default_id_offset=>15440771937534814
,p_default_owner=>'ADCS'
);
end;
/
 
prompt APPLICATION 109 - FACTURACION Y VENTAS
--
-- Application Export:
--   Application:     109
--   Name:            FACTURACION Y VENTAS
--   Date and Time:   21:16 Tuesday August 2, 2022
--   Exported By:     PABLOC
--   Flashback:       0
--   Export Type:     Page Export
--   Manifest
--     PAGE: 86
--   Manifest End
--   Version:         20.1.0.00.13
--   Instance ID:     211687171918188
--

begin
null;
end;
/
prompt --application/pages/delete_00086
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>86);
end;
/
prompt --application/pages/page_00086
begin
wwv_flow_api.create_page(
 p_id=>86
,p_user_interface_id=>wwv_flow_api.id(25547296363951795)
,p_name=>'Ingreso de Ventas Alimentec'
,p_alias=>'INGRESO-DE-VENTAS-ALIMENTEC'
,p_step_title=>'Ingreso de Ventas Alimentec'
,p_autocomplete_on_off=>'OFF'
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#WORKSPACE_IMAGES#sweetalert2.all.min.js',
'#WORKSPACE_IMAGES#sweetalert2.min.js',
''))
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'PABLOC'
,p_last_upd_yyyymmddhh24miss=>'20220802211109'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(963630287610068454)
,p_plug_name=>'FACI053 - INGRESO DE VENTAS (ALIMENTEC)'
,p_region_name=>'CABECERA'
,p_region_template_options=>'#DEFAULT#:t-Region--accent4:t-Region--scrollBody:t-Form--noPadding:t-Form--stretchInputs:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(25513702711951745)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(963630383935068455)
,p_plug_name=>'TOTALES'
,p_region_template_options=>'#DEFAULT#:t-Region--accent15:t-Region--scrollBody:t-Form--noPadding:t-Form--large:t-Form--leftLabels:t-Form--labelsAbove'
,p_plug_template=>wwv_flow_api.id(25513702711951745)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_grid_column_span=>2
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_header=>'<b>'
,p_plug_footer=>'</b>'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(979564349744323147)
,p_plug_name=>'Detalle'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(25513205524951745)
,p_plug_display_sequence=>70
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ITEM,',
'       UNIDAD_VTA,',
'       ARTICULO||''-''||SA.ART_DESC ARTICULO,',
'       NRO_REMISION,',
'       CANTIDAD,',
'       PRECIO,',
'       PRECIO * CANTIDAD TOTAL,',
'       PESO_UNIDAD,',
'       PESO_TONELADA,',
'       NULL ELIMINAR,',
'       ARTICULO ARTICULO_COD',
'       --',
'       --',
'FROM(SELECT ',
'       SEQ_ID ITEM,',
'       C001 UNIDAD_VTA,',
'       C002 ARTICULO,',
'       TO_NUMBER(C003) NRO_REMISION,',
'       TO_NUMBER(C004) CANTIDAD,',
'       TO_NUMBER(C005) PRECIO,',
'       TO_NUMBER(C007) PESO_UNIDAD,',
'       TO_NUMBER(C008) PESO_TONELADA',
'     --',
'       ',
'     --',
'     ',
'  FROM APEX_COLLECTIONS A',
'  inner join stk_articulo ar on (ar.art_codigo = to_number(a.c002) and ar.art_empr = :P_EMPRESA )',
'  inner join gen_impuesto i on (i.impu_codigo = ar.art_impu and i.impu_empr = ar.art_empr)',
' WHERE A.COLLECTION_NAME = ''DETALLES_FACI052'' ',
'       ) A, ',
'       STK_ARTICULO SA',
'WHERE SA.ART_CODIGO = A.ARTICULO',
'  AND SA.ART_EMPR = :P_EMPRESA ',
'  AND SA.ART_EST = ''A''',
'GROUP BY ITEM,',
'         UNIDAD_VTA,',
'         ARTICULO||''-''||SA.ART_DESC,',
'         NRO_REMISION,',
'         CANTIDAD,',
'         PRECIO,',
'         PESO_UNIDAD,',
'         PESO_TONELADA,',
'         ARTICULO',
'         --',
'       ',
'         --'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'MILLIMETERS'
,p_prn_paper_size=>'A4'
,p_prn_width=>297
,p_prn_height=>210
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header=>'Detalle'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#EEEEEE'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'bold'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#FFFFFF'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_prn_border_color=>'#666666'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(979564455294323148)
,p_max_row_count=>'1000000'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:EMAIL:XLS:PDF:RTF'
,p_owner=>'AMENDEZ'
,p_internal_uid=>979564455294323148
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703804639675171430)
,p_db_column_name=>'ITEM'
,p_display_order=>20
,p_column_identifier=>'L'
,p_column_label=>'ITEM'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703805063051171430)
,p_db_column_name=>'UNIDAD_VTA'
,p_display_order=>30
,p_column_identifier=>'M'
,p_column_label=>'UNIDAD'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703805461153171431)
,p_db_column_name=>'ARTICULO'
,p_display_order=>40
,p_column_identifier=>'N'
,p_column_label=>'ARTICULO'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703805861099171431)
,p_db_column_name=>'TOTAL'
,p_display_order=>70
,p_column_identifier=>'Y'
,p_column_label=>'TOTAL'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_format_mask=>'999G999G999G999G990D00'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703804209252171426)
,p_db_column_name=>'ELIMINAR'
,p_display_order=>110
,p_column_identifier=>'J'
,p_column_label=>'ELIMINAR'
,p_column_link=>'javascript:$s(''P86_DELETE_ID'',''#ITEM#'');'
,p_column_linktext=>'<i class="fa fa-times" aria-hidden="true"></i>'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703806263557171432)
,p_db_column_name=>'NRO_REMISION'
,p_display_order=>120
,p_column_identifier=>'AA'
,p_column_label=>unistr('NRO REMISI\00D3N')
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703806695314171432)
,p_db_column_name=>'CANTIDAD'
,p_display_order=>130
,p_column_identifier=>'AB'
,p_column_label=>'CANTIDAD'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_format_mask=>'999G999G999G999G999G999G990'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703807093545171432)
,p_db_column_name=>'PRECIO'
,p_display_order=>140
,p_column_identifier=>'AC'
,p_column_label=>'PRECIO UNITARIO'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_format_mask=>'999G999G999G999G990D00'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703807491150171432)
,p_db_column_name=>'PESO_UNIDAD'
,p_display_order=>150
,p_column_identifier=>'AD'
,p_column_label=>'PESO UNIDAD'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703807882346171432)
,p_db_column_name=>'PESO_TONELADA'
,p_display_order=>160
,p_column_identifier=>'AE'
,p_column_label=>'PESO TONELADA'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703808247736171432)
,p_db_column_name=>'ARTICULO_COD'
,p_display_order=>170
,p_column_identifier=>'AF'
,p_column_label=>'Articulo Cod'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(979593283553362349)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'7038086'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'ITEM:UNIDAD_VTA:ARTICULO:NRO_REMISION:CANTIDAD:PRECIO:TOTAL:ELIMINAR'
,p_sum_columns_on_break=>'TOTAL:CANTIDAD'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1052857745132402439)
,p_plug_name=>'contenendor_botones'
,p_region_template_options=>'#DEFAULT#:t-Region--noPadding:t-Region--removeHeader:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(25513702711951745)
,p_plug_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1112273233172739252)
,p_plug_name=>'contenedor_cabecera'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(25513702711951745)
,p_plug_display_sequence=>100
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1112275826089739278)
,p_plug_name=>'contenedor_conf'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(25513702711951745)
,p_plug_display_sequence=>140
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1131333483411954182)
,p_plug_name=>'contenedor_parametros'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(25513702711951745)
,p_plug_display_sequence=>120
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1133615516149962144)
,p_plug_name=>'contenedor indicadores'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(25513702711951745)
,p_plug_display_sequence=>110
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1137419162195941179)
,p_plug_name=>'contenedor proforma'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(25513702711951745)
,p_plug_display_sequence=>150
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1139574573093019647)
,p_plug_name=>'contenedor detalles y totalizadores'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(25513702711951745)
,p_plug_display_sequence=>160
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1139577059517019672)
,p_plug_name=>'contenedor Bexit'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(25513702711951745)
,p_plug_display_sequence=>130
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1150165108893515964)
,p_plug_name=>'Agregar Detalles'
,p_region_template_options=>'#DEFAULT#:t-Region--accent4:t-Region--scrollBody:t-Form--noPadding'
,p_plug_template=>wwv_flow_api.id(25513702711951745)
,p_plug_display_sequence=>50
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_grid_column_span=>4
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1151956920034758441)
,p_plug_name=>'contenedor_de_pesos'
,p_parent_plug_id=>wwv_flow_api.id(1150165108893515964)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--scrollBody:t-Form--noPadding:margin-top-sm'
,p_plug_template=>wwv_flow_api.id(25513702711951745)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_display_condition_type=>'NEVER'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1151960737685758479)
,p_plug_name=>'Generar Cuotas'
,p_region_name=>'contenendor_generar_cuotas'
,p_region_template_options=>'#DEFAULT#:js-dialog-size720x480'
,p_plug_template=>wwv_flow_api.id(25512216864951744)
,p_plug_display_sequence=>90
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1162900349574258157)
,p_plug_name=>'contenedor fco_concepto'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(25513702711951745)
,p_plug_display_sequence=>170
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1162901063624258164)
,p_plug_name=>'contenedor fac_documento_det'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(25513702711951745)
,p_plug_display_sequence=>180
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1165033756759214582)
,p_plug_name=>'contenedor stk_documento'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(25513702711951745)
,p_plug_display_sequence=>190
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1167429410501956872)
,p_plug_name=>'cdetalles'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(25513205524951745)
,p_plug_display_sequence=>60
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
' SELECT      SEQ_ID ITEM,',
'             C001 UNIDAD_VTA,',
'             C002 ARTICULO,',
'             TO_NUMBER(C003) NRO_REMISION,',
'             TO_NUMBER(C004) CANTIDAD,',
'             TO_NUMBER(C005) PRECIO,',
'             TO_NUMBER(C007) PESO_UNIDAD,',
'             TO_NUMBER(C008) PESO_TONELADA,',
'             TO_NUMBER(C009)            V_DOC_EXENTA_MON,',
'             TO_NUMBER(C010)            V_DOC_EXENTA_LOC,',
'             TO_NUMBER(C011)            V_DOC_GRAV_MON,',
'             TO_NUMBER(C012)            V_DOC_GRAV_LOC,',
'             TO_NUMBER(C013)            V_DOC_IVA_MON,',
'             TO_NUMBER(C014)            V_DOC_IVA_LOC,',
'             C015            ART_CLASIFICACION,',
'             C016            V_ART_CTA_CONTABLE,',
'             C017            V_IMPU_PORCENTAJE,',
'             TO_NUMBER(C018)            DET_BRUTO_LOC,',
'             TO_NUMBER(C019)            DET_BRUTO_MON,',
'             TO_NUMBER(C020)            DET_NETO_LOC,',
'             TO_NUMBER(C021)            DET_NETO_MON,',
'             TO_NUMBER(C022)            DET_IVA_LOC,',
'             TO_NUMBER(C023)            DET_IVA_MON ,',
'             C024                       PORCENTAJE_deSCUENTO,',
'             C025                       PRECIO_BRUTO',
'        FROM APEX_COLLECTIONS ',
'       WHERE COLLECTION_NAME = ''DETALLES_FACI052'''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'MILLIMETERS'
,p_prn_paper_size=>'A4'
,p_prn_width=>297
,p_prn_height=>210
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header=>'cdetalles'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#EEEEEE'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'bold'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#FFFFFF'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_prn_border_color=>'#000000'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(1167429562394956873)
,p_max_row_count=>'1000000'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:EMAIL:XLS:PDF:RTF'
,p_owner=>'JBRITEZ'
,p_internal_uid=>1167429562394956873
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703826953263171452)
,p_db_column_name=>'ITEM'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Item'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703827302679171452)
,p_db_column_name=>'UNIDAD_VTA'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Unidad Vta'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703827797738171453)
,p_db_column_name=>'ARTICULO'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Articulo'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703828104509171453)
,p_db_column_name=>'NRO_REMISION'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Nro Remision'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703828551315171453)
,p_db_column_name=>'CANTIDAD'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Cantidad'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703828962903171453)
,p_db_column_name=>'PRECIO'
,p_display_order=>60
,p_column_identifier=>'F'
,p_column_label=>'Precio'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703829343435171453)
,p_db_column_name=>'PESO_UNIDAD'
,p_display_order=>80
,p_column_identifier=>'H'
,p_column_label=>'Peso Unidad'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703829746499171454)
,p_db_column_name=>'PESO_TONELADA'
,p_display_order=>90
,p_column_identifier=>'I'
,p_column_label=>'Peso Tonelada'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703830144381171454)
,p_db_column_name=>'ART_CLASIFICACION'
,p_display_order=>160
,p_column_identifier=>'P'
,p_column_label=>'Art Clasificacion'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703830513659171454)
,p_db_column_name=>'V_ART_CTA_CONTABLE'
,p_display_order=>170
,p_column_identifier=>'Q'
,p_column_label=>'V Art Cta Contable'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703830961083171454)
,p_db_column_name=>'V_IMPU_PORCENTAJE'
,p_display_order=>180
,p_column_identifier=>'R'
,p_column_label=>'V Impu Porcentaje'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703831310618171454)
,p_db_column_name=>'V_DOC_EXENTA_MON'
,p_display_order=>190
,p_column_identifier=>'Z'
,p_column_label=>'V Doc Exenta Mon'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703831745424171455)
,p_db_column_name=>'V_DOC_EXENTA_LOC'
,p_display_order=>200
,p_column_identifier=>'AA'
,p_column_label=>'V Doc Exenta Loc'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703832180005171455)
,p_db_column_name=>'V_DOC_GRAV_MON'
,p_display_order=>210
,p_column_identifier=>'AB'
,p_column_label=>'V Doc Grav Mon'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703832532569171455)
,p_db_column_name=>'V_DOC_GRAV_LOC'
,p_display_order=>220
,p_column_identifier=>'AC'
,p_column_label=>'V Doc Grav Loc'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703832961987171455)
,p_db_column_name=>'V_DOC_IVA_MON'
,p_display_order=>230
,p_column_identifier=>'AD'
,p_column_label=>'V Doc Iva Mon'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703833399486171455)
,p_db_column_name=>'V_DOC_IVA_LOC'
,p_display_order=>240
,p_column_identifier=>'AE'
,p_column_label=>'V Doc Iva Loc'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703833728322171456)
,p_db_column_name=>'DET_BRUTO_LOC'
,p_display_order=>250
,p_column_identifier=>'AF'
,p_column_label=>'Det Bruto Loc'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703834134300171456)
,p_db_column_name=>'DET_BRUTO_MON'
,p_display_order=>260
,p_column_identifier=>'AG'
,p_column_label=>'Det Bruto Mon'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703834512729171456)
,p_db_column_name=>'DET_NETO_LOC'
,p_display_order=>270
,p_column_identifier=>'AH'
,p_column_label=>'Det Neto Loc'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703834991625171456)
,p_db_column_name=>'DET_NETO_MON'
,p_display_order=>280
,p_column_identifier=>'AI'
,p_column_label=>'Det Neto Mon'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703835308390171456)
,p_db_column_name=>'DET_IVA_LOC'
,p_display_order=>290
,p_column_identifier=>'AJ'
,p_column_label=>'Det Iva Loc'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703835713110171457)
,p_db_column_name=>'DET_IVA_MON'
,p_display_order=>300
,p_column_identifier=>'AK'
,p_column_label=>'Det Iva Mon'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703836123107171458)
,p_db_column_name=>'PORCENTAJE_DESCUENTO'
,p_display_order=>310
,p_column_identifier=>'AL'
,p_column_label=>'Porcentaje Descuento'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(703836560266171459)
,p_db_column_name=>'PRECIO_BRUTO'
,p_display_order=>320
,p_column_identifier=>'AM'
,p_column_label=>'Precio Bruto'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(1169903129591736604)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'7038369'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'ITEM:UNIDAD_VTA:ARTICULO:NRO_REMISION:CANTIDAD:PRECIO:PESO_UNIDAD:PESO_TONELADA:ART_CLASIFICACION:V_ART_CTA_CONTABLE:V_IMPU_PORCENTAJE:PORCENTAJE_DESCUENTO:PRECIO_BRUTO:V_DOC_IVA_LOC:V_DOC_IVA_MON:DET_BRUTO_LOC:DET_BRUTO_MON:DET_IVA_LOC:DET_IVA_MON:D'
||'ET_NETO_LOC:DET_NETO_MON:V_DOC_EXENTA_LOC:V_DOC_EXENTA_MON:V_DOC_GRAV_LOC:V_DOC_GRAV_MON:'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(703825890241171451)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(1052857745132402439)
,p_button_name=>'CUOTAS'
,p_button_static_id=>'CUOTAS'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(25536441528951771)
,p_button_image_alt=>'Cuotas'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_redirect_url=>'f?p=&APP_ID.:87:&SESSION.::&DEBUG.:RP,87::'
,p_warn_on_unsaved_changes=>null
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(703825036069171450)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(1052857745132402439)
,p_button_name=>'Cancelar_Todo'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#:t-Button--simple:t-Button--iconRight'
,p_button_template_id=>wwv_flow_api.id(25536586739951772)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Cancelar'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_execute_validations=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(703725864993171355)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(1150165108893515964)
,p_button_name=>'Agregar'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--primary:t-Button--simple:t-Button--iconRight'
,p_button_template_id=>wwv_flow_api.id(25536586739951772)
,p_button_image_alt=>'Agregar'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_warn_on_unsaved_changes=>null
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(703825489757171451)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(1052857745132402439)
,p_button_name=>'Aceptar'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(25536441528951771)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Aceptar'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_execute_validations=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(703725483823171354)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(1150165108893515964)
,p_button_name=>'Cancelar_Det'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--iconRight'
,p_button_template_id=>wwv_flow_api.id(25536586739951772)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Cancelar'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_warn_on_unsaved_changes=>null
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(703826298838171451)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(1052857745132402439)
,p_button_name=>'FPAGO'
,p_button_static_id=>'FPAGOS'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(25536441528951771)
,p_button_image_alt=>'Fpago'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'f?p=&APP_ID.:88:&SESSION.::&DEBUG.:RP,88::'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703726285291171356)
,p_name=>'P86_IND_ITEM_EDIT'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(1150165108893515964)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703726635238171359)
,p_name=>'P86_IND_EDIT'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(1150165108893515964)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703727060242171360)
,p_name=>'P86_S_UNIDAD_VTA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(1150165108893515964)
,p_item_default=>'U'
,p_prompt=>'UM'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC:Unidad;U,Kilogramo;K,Flete;F'
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:margin-top-sm'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703727404888171361)
,p_name=>'P86_S_ART'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(1150165108893515964)
,p_prompt=>unistr('Art\00EDculo')
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_ART_ALFANUMERICO_FACI052'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select  art_codigo||''-''||art_cod_alfanumerico||'' ''||art_desc d, ',
'        art_codigo a',
'from stk_articulo ',
'where art_est = ''A''',
'  AND ART_EMPR= :P_EMPRESA ',
'order by 1'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'%'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#:margin-top-sm'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
,p_attribute_08=>'900'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703727848712171362)
,p_name=>'P86_D_ART_DESC'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(1150165108893515964)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#:margin-top-sm'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703728282173171363)
,p_name=>'P86_D_NRO_REMIS'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(1150165108893515964)
,p_prompt=>unistr('Nro. Remisi\00F3n')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:margin-top-sm'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703728640832171363)
,p_name=>'P86_DET_CANT'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(1150165108893515964)
,p_prompt=>'Cantidad'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:margin-top-sm'
,p_attribute_03=>'right'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703729092240171363)
,p_name=>'P86_S_PRECIO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(1150165108893515964)
,p_prompt=>'Precio Unitario'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:margin-top-sm'
,p_help_text=>'Ingrese los montos en negativo para calcular su precio sin impuesto'
,p_attribute_03=>'right'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703729450880171363)
,p_name=>'P86_S_IMPORTE'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(1150165108893515964)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703729880445171363)
,p_name=>'P86_DELETE_ID'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_api.id(1150165108893515964)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703730514561171364)
,p_name=>'P86_DETA_PESO_UN'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(1151956920034758441)
,p_prompt=>'Peso UN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_display_when=>'P_EMPRESA'
,p_display_when2=>'1'
,p_display_when_type=>'VAL_OF_ITEM_IN_COND_NOT_EQ_COND2'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
end;
/
begin
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703730971192171364)
,p_name=>'P86_DETA_PESO_TT'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(1151956920034758441)
,p_prompt=>'Peso TT'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_display_when=>'P_EMPRESA'
,p_display_when2=>'1'
,p_display_when_type=>'VAL_OF_ITEM_IN_COND_NOT_EQ_COND2'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703731300175171365)
,p_name=>'P86_KG_GANCHO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(1151956920034758441)
,p_prompt=>'Kg Gancho'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_display_when=>'P_EMPRESA'
,p_display_when2=>'1'
,p_display_when_type=>'VAL_OF_ITEM_IN_COND_NOT_EQ_COND2'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703731768722171365)
,p_name=>'P86_DET_RENDIMIENTO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(1151956920034758441)
,p_prompt=>'Rendimiento'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_display_when=>'P_EMPRESA'
,p_display_when2=>'1'
,p_display_when_type=>'VAL_OF_ITEM_IN_COND_NOT_EQ_COND2'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703733288919171366)
,p_name=>'P86_CONF_PERIODO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'CONF PERIODO'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703733670751171366)
,p_name=>'P86_CONF_STK_PER_ACT_INI'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703734016866171366)
,p_name=>'P86_CONF_PER_ACT_FIN'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703734456623171367)
,p_name=>'P86_CONF_STK_PER_SGTE_FIN'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703734855797171368)
,p_name=>'P86_CONF_PER_ACT_INI'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703735219245171369)
,p_name=>'P86_CONF_PER_SGTE_INI'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'Conf Per Sgte Ini'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703735680145171369)
,p_name=>'P86_CONF_PER_SGTE_FIN'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703736073718171370)
,p_name=>'P86_CONF_IVA'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703736405665171370)
,p_name=>'P86_CONF_FACT_CERO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703736898123171371)
,p_name=>'P86_CONF_ACT_STK'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'CONF_ACT_STK'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703737224790171371)
,p_name=>'P86_CONF_IMPR'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'Conf Impr'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703737633161171371)
,p_name=>'P86_CONF_COD_FAC_CONT'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'P86_CONF_COD_FAC_CONT'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703738079943171371)
,p_name=>'P86_CONF_COD_FAC_CRED'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'CONF_CRED'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703738401032171372)
,p_name=>'P86_CONF_CONC_VTA_MERC'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'P86_CONF_CONC_VTA_MERC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703738811900171372)
,p_name=>'P86_CONF_CONC_VTA_EXPORT'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703739238328171372)
,p_name=>'P86_CONF_CONC_VTA_SERV'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703739647901171372)
,p_name=>'P86_CONF_CONC_DEV'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703740097376171372)
,p_name=>'P86_CONF_CONC_IMPU_VTA'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'P86_CONF_CONC_IMPU_VTA'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703740471055171372)
,p_name=>'P86_CONF_CONC_IMPU_DEV'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703740834124171373)
,p_name=>'P86_CONF_CATEG_CLI_ESPO'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'Conf Categ Cli Espo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703741223677171373)
,p_name=>'P86_CONF_IND_MODIFICAR_PRECIO'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_item_default=>'S'
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703741663238171373)
,p_name=>'P86_CONF_IND_CUOTA_AUTOMATICA'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703742095071171373)
,p_name=>'P86_CONF_CTA_CONT_DEU_FCON'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'Conf Cta Cont Deu Fcon'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703742490783171373)
,p_name=>'P86_P_IND_SUCURSAL'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_api.id(1112275826089739278)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703743173275171374)
,p_name=>'P86_DOC_EXP_PROFORMA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(1137419162195941179)
,p_prompt=>'Doc Exp Proforma'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703743586462171374)
,p_name=>'P86_PROF_ARTIC_FLETE'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(1137419162195941179)
,p_prompt=>'artic flete'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703743975158171374)
,p_name=>'P86_PROF_PREC_FLETE'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(1137419162195941179)
,p_prompt=>'PROF_PREC_FLETE'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703744328604171375)
,p_name=>'P86_PROF_FLETE_MON'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(1137419162195941179)
,p_prompt=>'pROF_FLETE_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703744731407171375)
,p_name=>'P86_D_ART_COD'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(1137419162195941179)
,p_prompt=>'D_ART_COD'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703745145731171375)
,p_name=>'P86_RETORNO_VALIDAR_MON'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(1137419162195941179)
,p_prompt=>'P86_RETORNO_VALIDAR_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703745513086171375)
,p_name=>'P86_RETORNO_VALIDAR2_MON'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(1137419162195941179)
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703745905647171375)
,p_name=>'P86_RETORNO_VALIDAR3_MON'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(1137419162195941179)
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703746397715171375)
,p_name=>'P86_VAL_MOROSO'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(1137419162195941179)
,p_prompt=>'Nuevo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703747012876171376)
,p_name=>'P86_TIPO_REPORTE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'Tipo Reporte'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703747469061171376)
,p_name=>'P86_URL_REPORTE'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'Url Reporte'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703747841424171378)
,p_name=>'P86_DOC_CLAVE'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'doc_clave'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703748270355171378)
,p_name=>'P86_W_CLI_PORC_EXEN_IVA'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'W_CLI_PORC_EXEN_IVA'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703748683817171379)
,p_name=>'P86_MAX_ITEMS_FAC_CO'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'_MAX_ITEMS_FAC_CO'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703749046075171379)
,p_name=>'P86_MAX_ITEMS_FAC_CR'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'Max Items Fac Cr'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703749409340171380)
,p_name=>'P86_DOC_TIPO_SALDO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'Doc Tipo Saldo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703749849822171380)
,p_name=>'P86_DOC_TIPO_MOV'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'Doc Tipo Mov'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703750278553171380)
,p_name=>'P86_CLI_PERS_REPRE'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'Cli Pers Repre'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703750628512171381)
,p_name=>'P86_CLI_DOC_IDENT_REPRE'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'Cli Doc Ident Repre'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703751048530171381)
,p_name=>'P86_IND_CUOTA'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'Ind Cuota'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703751452431171381)
,p_name=>'P86_DOC_CLI_NRO_PED'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'Doc Cli Nro Ped'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703751823982171382)
,p_name=>'P86_W_IMP_LIM_CR_EMPR'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'W Imp Lim Cr Empr'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703752271254171382)
,p_name=>'P86_W_IMP_CHEQ_RECHAZADO'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'W Imp Cheq Rechazado'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703752660689171382)
,p_name=>'P86_W_IMP_LIM_CR_DISP_GRUPO'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'W Imp Lim Cr Disp Grupo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703753041311171382)
,p_name=>'P86_W_IMP_CHEQ_DIFERIDO'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'W Imp Cheq Diferido'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703753493816171382)
,p_name=>'P86_W_ANT_DOC_LEGAJO'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'W Ant Doc Legajo Vend'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703753874692171382)
,p_name=>'P86_W_CATEG'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'W Categ'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703754267789171383)
,p_name=>'P86_CLI_LOCALIDAD'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'Cli Localidad'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703754677104171383)
,p_name=>'P86_V_INI_NUM'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'V Ini Num'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_03=>'right'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703755027883171383)
,p_name=>'P86_VAL_INI_N'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'New'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_03=>'right'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703755429248171383)
,p_name=>'P86_DOC_CANAL_INI'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'Doc Canal Ini'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703755867154171384)
,p_name=>'P86_W_ANT_DOC_MON'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'W Ant Doc Mon'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703756279485171384)
,p_name=>'P86_MON_SIMBOLO'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'Mon Simbolo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703756614039171384)
,p_name=>'P86_W_MON_DEC_PRECIO'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'W Mon Dec Precio'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703757060652171385)
,p_name=>'P86_W_MON_DEC_IMP'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'W Mon Dec Imp'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703757447055171385)
,p_name=>'P86_CTA_CHEQ_DIF_RESPALDO'
,p_item_sequence=>270
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'Cta Cheq Dif Respaldo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703757850074171386)
,p_name=>'P86_CLAVE_STOPES'
,p_item_sequence=>280
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'Clave Stopes'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703758225495171386)
,p_name=>'P86_EMPR_IND_GANADERA'
,p_item_sequence=>290
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'Empr Ind Ganadera'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703758676808171386)
,p_name=>'P86_IND_TRAER_PRESUPUESTO'
,p_item_sequence=>300
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'Ind Traer Presupuesto'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703759091746171386)
,p_name=>'P86_S_TOTAL_FLETE_LOC'
,p_item_sequence=>310
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'S Total Flete Loc'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703759416705171387)
,p_name=>'P86_S_TOTAL_FLETE_MON'
,p_item_sequence=>320
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'S Total Flete Mon'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
end;
/
begin
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703759824253171388)
,p_name=>'P86_S_CONC_FLETE'
,p_item_sequence=>330
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'S Conc Flete'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703760243339171390)
,p_name=>'P86_S_CTACO_FLETE'
,p_item_sequence=>340
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'S Ctaco Flete'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703760658251171390)
,p_name=>'P86_W_CTA_MERC_CRED'
,p_item_sequence=>350
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'W_CTA_MERC_CRED'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703761046868171390)
,p_name=>'P86_W_CTA_MERC_CONT'
,p_item_sequence=>360
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'W_CTA_MERC_CONT'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703761421386171390)
,p_name=>'P86_W_CTA_IMPU'
,p_item_sequence=>370
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'CTA IMPU'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703761800428171391)
,p_name=>'P86_W_CTA_MERC_CONT_EX'
,p_item_sequence=>380
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'CTA MERC CONT EX'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703762294348171391)
,p_name=>'P86_W_CTA_MERC_CRED_EX'
,p_item_sequence=>390
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'CTA MERC CRED EX'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703762625300171392)
,p_name=>'P86_P_SUC_LOCALIDAD'
,p_item_sequence=>400
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'LOCALIDAD SUC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703763053829171392)
,p_name=>'P86_P_MAX_ITEMS_FAC_CO'
,p_item_sequence=>410
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'max items fac co'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703763463077171392)
,p_name=>'P86_P_MAX_ITEMS_FAC_CR'
,p_item_sequence=>420
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'max items fac cred'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703763885635171392)
,p_name=>'P86_W_LIM_AUTORIZADO'
,p_item_sequence=>430
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'W_LIM_AUTORIZADO'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703764207431171392)
,p_name=>'P86_DIAS_DE_ATRASOS'
,p_item_sequence=>440
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'DIAS_DE_ATRASOS'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703764688787171393)
,p_name=>'P86_S_PRES_ART_SUP_LIMITE'
,p_item_sequence=>450
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'S_PRES_ART_SUP_LIMITE'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703765057868171393)
,p_name=>'P86_DOC_NETO_GRAV_LOC'
,p_item_sequence=>460
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'DOC_NETO_GRAV_LOC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703765417745171393)
,p_name=>'P86_DOC_NETO_EXEN_LOC'
,p_item_sequence=>470
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'DOC_NETO_EXEN_LOC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703765814342171393)
,p_name=>'P86_DOC_IVA_LOC'
,p_item_sequence=>480
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'DOC_IVA_LOC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703766247270171393)
,p_name=>'P86_DOC_NETO_GRAV_MON'
,p_item_sequence=>490
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'DOC_NETO_GRAV_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703766673730171393)
,p_name=>'P86_DOC_NETO_EXEN_MON'
,p_item_sequence=>500
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'DOC_NETO_EXEN_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703767078560171394)
,p_name=>'P86_DOC_IVA_MON'
,p_item_sequence=>510
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'DOC_IVA_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703767417565171394)
,p_name=>'P86_DOC_SALDO_LOC'
,p_item_sequence=>520
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'DOC_SALDO_LOC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703767847784171394)
,p_name=>'P86_DOC_SALDO_MON'
,p_item_sequence=>530
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'DOC_SALDO_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703768234030171394)
,p_name=>'P86_DOC_IND_STK'
,p_item_sequence=>540
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'DOC_IND_STK'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703768669037171394)
,p_name=>'P86_DOC_CLAVE_STK'
,p_item_sequence=>550
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'DOC_CLAVE_STK'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703769014257171394)
,p_name=>'P86_DOC_OPERADOR'
,p_item_sequence=>560
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'DOC_OPERADOR'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703769430002171395)
,p_name=>'P86_W_IMP_LIM_CR_GRUPO'
,p_item_sequence=>570
,p_item_plug_id=>wwv_flow_api.id(1112273233172739252)
,p_prompt=>'P86_W_IMP_LIM_CR_GRUPO'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703770126665171395)
,p_name=>'P86_P_LIM_FAC_ADIC'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(1131333483411954182)
,p_prompt=>'P Lim Fac Adic'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703770580157171395)
,p_name=>'P86_G_VAL_INI_N'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(1131333483411954182)
,p_prompt=>'G Val Ini N'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703770989387171396)
,p_name=>'P86_P_FAC_NEGRO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(1131333483411954182)
,p_prompt=>'P86_P_FAC_NEGRO'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703771623457171396)
,p_name=>'P86_INDICADOR_CNTRL_CLI'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(1133615516149962144)
,p_prompt=>'Indicador Cntrl Cli'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703772018515171396)
,p_name=>'P86_IND_PED_AUTO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(1133615516149962144)
,p_prompt=>'Ind Ped Auto'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703772491981171396)
,p_name=>'P86_IND_VENCIMIENTO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(1133615516149962144)
,p_prompt=>'Ind Vencimiento'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703772847095171398)
,p_name=>'P86_IND_RUC'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(1133615516149962144)
,p_prompt=>'Ind Ruc'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703773508503171400)
,p_name=>'P86_DCON_MERC_EXEN_LOC'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(1162900349574258157)
,p_prompt=>'DCON_MERC_EXEN_LOC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703773991355171401)
,p_name=>'P86_DCON_MERC_EXEN_MON'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(1162900349574258157)
,p_prompt=>'DCON_MERC_EXEN_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703774374834171401)
,p_name=>'P86_DCON_MERC_GRAV_LOC'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(1162900349574258157)
,p_prompt=>'DCON_MERC_GRAV_LOC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703774797922171401)
,p_name=>'P86_DCON_MERC_GRAV_MON'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(1162900349574258157)
,p_prompt=>'DCON_MERC_GRAV_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703775114205171401)
,p_name=>'P86_DCON_IVA_LOC'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(1162900349574258157)
,p_prompt=>'DCON_IVA_LOC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703775528191171402)
,p_name=>'P86_DCON_IVA_MON'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(1162900349574258157)
,p_prompt=>'DCON_IVA_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703776229793171402)
,p_name=>'P86_W_NETO_EXEN_LOC'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(1162901063624258164)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703776693644171402)
,p_name=>'P86_W_NETO_EXEN_MON'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(1162901063624258164)
,p_prompt=>'W_NETO_EXEN_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703777099081171403)
,p_name=>'P86_W_NETO_GRAV_LOC'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(1162901063624258164)
,p_prompt=>'W_NETO_GRAV_LOC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703777413592171403)
,p_name=>'P86_W_NETO_GRAV_MON'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(1162901063624258164)
,p_prompt=>'W_NETO_GRAV_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703777818697171403)
,p_name=>'P86_DET_BRUTO_LOC'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(1162901063624258164)
,p_prompt=>'DET_BRUTO_LOC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703778210090171403)
,p_name=>'P86_DET_BRUTO_MON'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(1162901063624258164)
,p_prompt=>'DET_BRUTO_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703778604726171403)
,p_name=>'P86_DET_NETO_LOC'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(1162901063624258164)
,p_prompt=>'DET_NETO_LOC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703779083526171403)
,p_name=>'P86_DET_NETO_MON'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(1162901063624258164)
,p_prompt=>'DET_NETO_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703779426544171404)
,p_name=>'P86_DET_IVA_LOC'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(1162901063624258164)
,p_prompt=>'DET_IVA_LOC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703779805260171404)
,p_name=>'P86_DET_IVA_MON'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(1162901063624258164)
,p_prompt=>'DET_IVA_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703780269989171404)
,p_name=>'P86_W_BRUTO_EXEN_PRECIO_MON'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(1162901063624258164)
,p_prompt=>'W_BRUTO_EXEN_PRECIO_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703780626615171404)
,p_name=>'P86_W_IVA_MON'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(1162901063624258164)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703781028165171404)
,p_name=>'P86_W_IVA_LOC'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_api.id(1162901063624258164)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703781789415171405)
,p_name=>'P86_DOCU_GRAV_NETO_LOC'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(1165033756759214582)
,p_prompt=>'DOCU_GRAV_NETO_LOC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703782112551171405)
,p_name=>'P86_DOCU_GRAV_NETO_MON'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(1165033756759214582)
,p_prompt=>'DOCU_GRAV_NETO_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703782561065171405)
,p_name=>'P86_DOCU_GRAV_BRUTO_LOC'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(1165033756759214582)
,p_prompt=>'DOCU_GRAV_BRUTO_LOC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703782991358171405)
,p_name=>'P86_DOCU_GRAV_BRUTO_MON'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(1165033756759214582)
,p_prompt=>'DOCU_GRAV_BRUTO_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703783399873171405)
,p_name=>'P86_DOCU_EXEN_NETO_LOC'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(1165033756759214582)
,p_prompt=>'DOCU_EXEN_NETO_LOC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703783780796171406)
,p_name=>'P86_DOCU_EXEN_NETO_MON'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(1165033756759214582)
,p_prompt=>'DOCU_EXEN_NETO_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703784159282171406)
,p_name=>'P86_DOCU_EXEN_BRUTO_LOC'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(1165033756759214582)
,p_prompt=>'DOCU_EXEN_BRUTO_LOC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703784502246171406)
,p_name=>'P86_DOCU_EXEN_BRUTO_MON'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(1165033756759214582)
,p_prompt=>'DOCU_EXEN_BRUTO_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703784983291171406)
,p_name=>'P86_DETA_CLAVE_DOC'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(1165033756759214582)
,p_prompt=>'deta clave doc'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703785683267171410)
,p_name=>'P86_S_TOTAL'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703786059565171411)
,p_name=>'P86_S_TIPO_FACTURA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Tipo factura'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>'STATIC:1 - CONTADO;1,2 - CREDITO;2,3 - VTA.IVA INCLUIDO;3'
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'POPUP'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703786407474171411)
,p_name=>'P86_S_TIPO_FACTURA_DESC'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703786832517171411)
,p_name=>'P86_IND_FACT_MANUAL'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_item_default=>'N'
,p_prompt=>'Factura manual'
,p_display_as=>'NATIVE_YES_NO'
,p_tag_attributes=>'disabled'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_grid_column_css_classes=>'apex_disabled'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'CUSTOM'
,p_attribute_02=>'S'
,p_attribute_03=>'Si'
,p_attribute_04=>'N'
,p_attribute_05=>'No'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703787298345171411)
,p_name=>'P86_DOC_FEC_DOC'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_item_default=>'sysdate'
,p_item_default_type=>'PLSQL_EXPRESSION'
,p_prompt=>'Fecha'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703787694619171412)
,p_name=>'P86_DOC_SERIE'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'NroFactura'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>2
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703788098922171412)
,p_name=>'P86_DOC_NRO_DOC'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
end;
/
begin
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703788423035171412)
,p_name=>'P86_DOC_TIMBRADO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Timbrado'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703788837114171412)
,p_name=>'P86_W_IMP_LIM_CR_DISP_EMPR'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Lim Cr Disp'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703789265021171412)
,p_name=>'P86_S_NRO_PEDIDO'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Nro.Pedido'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703789673559171413)
,p_name=>'P86_CLI_IND_MOD_CANAL'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703790095615171413)
,p_name=>'P86_EXPORTACION'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_item_default=>'N'
,p_prompt=>unistr('Exportaci\00F3n?')
,p_display_as=>'NATIVE_YES_NO'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_display_when=>'P_EMPRESA'
,p_display_when2=>'1'
,p_display_when_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'CUSTOM'
,p_attribute_02=>'S'
,p_attribute_03=>'S'
,p_attribute_04=>'N'
,p_attribute_05=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703790412668171413)
,p_name=>'P86_W_LIM_CR_ESPECIAL'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703790889577171413)
,p_name=>'P86_DOC_CLI'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Cliente'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT C.CLI_CODIGO||''-''||C.CLI_NOM||'' Vendedor: ''||C.CLI_VENDEDOR || ''-'' || E.EMPL_NOMBRE || '' '' || E.EMPL_APE DESCIPCION,',
'       C.CLI_CODIGO CODIGO',
'  FROM FIN_CLIENTE C, PER_EMPLEADO E',
' WHERE C.CLI_EST_CLI <> ''I''',
'   AND C.CLI_VENDEDOR = E.EMPL_LEGAJO(+)',
'   AND CLI_EMPR = EMPL_EMPRESA(+)',
'   AND CLI_EMPR = :P_EMPRESA',
' ORDER BY 1',
''))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703791260012171414)
,p_name=>'P86_DOC_CLI_NOM'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703791637495171414)
,p_name=>'P86_PED_AUTO'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Ped. Auto'
,p_display_as=>'NATIVE_YES_NO'
,p_tag_attributes=>'disabled'
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_css_classes=>'apex_disabled'
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'APPLICATION'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703792086838171414)
,p_name=>'P86_CLI_DIR'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>unistr('Direcci\00F3n')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>6
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703792462148171414)
,p_name=>'P86_CLI_DOC_TIPO'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_display_as=>'NATIVE_HIDDEN'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703792873953171414)
,p_name=>'P86_DOC_CANAL'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Canal'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT C.CAN_CODIGO || '' - '' || C.CAN_DESC A, C.CAN_CODIGO B',
'    FROM FAC_CANAL_VENTA C',
'   WHERE C.CAN_EMPR = :P_EMPRESA',
''))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'POPUP'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703793212310171414)
,p_name=>'P86_CAN_DESC'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703793654036171415)
,p_name=>'P86_W_CLI_MAX_DIAS_ATRASO'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703794063244171415)
,p_name=>'P86_LIPR_NRO_LISTA_PRECIO'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Lista precio'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703794414246171415)
,p_name=>'P86_DOC_CTA_BCO'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Caja/Bco'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_FIN_CTA_BANCO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select cta_codigo||'' - ''||bco_desc ||'' - ''|| cta_desc ||'' - ''|| mon_simbolo DISPLAY_VALUE, cta_codigo RETURN_VALUE',
'from fin_cuenta_bancaria, fin_banco, gen_moneda where cta_mon = mon_codigo (+) ',
'and cta_bco = bco_codigo (+) ',
' AND CTA_EMPR=BCO_EMPR(+)',
'      AND CTA_EMPR=MON_EMPR(+)',
'     AND CTA_EMPR   = :P_EMPRESA',
'and cta_empr = :p_empresa ',
'order by cta_codigo,bco_desc, cta_codigo, cta_desc'))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703794874184171415)
,p_name=>'P86_S_BANCO_CTA'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703795273884171415)
,p_name=>'P86_S_CTA_DESC'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703795644103171416)
,p_name=>'P86_DOC_MON'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Moneda'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_MONEDA'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT MON_CODIGO || '' - '' || MON_DESC AS DISPLAY_VALUE,',
'       MON_CODIGO AS RETURN_VALUE',
'  FROM GEN_MONEDA',
' WHERE MON_EMPR = :P_EMPRESA',
' ORDER BY 1',
''))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'POPUP'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703796056016171416)
,p_name=>'P86_DOC_MON_DESC'
,p_item_sequence=>270
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703796403494171416)
,p_name=>'P86_DOC_CLI_RUC'
,p_item_sequence=>280
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Cli RUC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'READONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703796843908171416)
,p_name=>'P86_W_DOC_TASA_US'
,p_is_required=>true
,p_item_sequence=>290
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'TasaU$'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703797247869171417)
,p_name=>'P86_S_DEP'
,p_item_sequence=>300
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Deposito'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT D.DEP_CODIGO || '' - '' || D.DEP_DESC A, D.DEP_CODIGO B',
'  FROM STK_DEPOSITO D',
' WHERE D.DEP_EMPR = :P_EMPRESA',
'   AND D.DEP_SUC = :P_SUCURSAL',
''))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'POPUP'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703797634936171418)
,p_name=>'P86_S_DEP_DESC'
,p_item_sequence=>310
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703798040165171418)
,p_name=>'P86_S_RECARGO'
,p_item_sequence=>320
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Recargo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703798445862171419)
,p_name=>'P86_DOC_LEGAJO'
,p_item_sequence=>330
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Vendedor'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT EMPL_LEGAJO || '' - '' || EMPL_NOMBRE A, EMPL_LEGAJO B',
'  FROM PER_EMPLEADO',
' WHERE EMPL_LEGAJO = (SELECT VEND_LEGAJO',
'                        FROM FAC_VENDEDOR',
'                       WHERE VEND_LEGAJO = EMPL_LEGAJO',
'                         AND VEND_EMPR = :P_EMPRESA)',
'   AND EMPL_EMPRESA = :P_EMPRESA',
' ORDER BY EMPL_NOMBRE',
''))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'POPUP'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703798892248171419)
,p_name=>'P86_DOC_LEGAJO_DESC'
,p_item_sequence=>340
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703799270033171420)
,p_name=>'P86_DOC_CLI_TEL'
,p_item_sequence=>350
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Cli telefono'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703799659511171420)
,p_name=>'P86_S_PORC_DTO'
,p_item_sequence=>360
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Porc Desc.'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>3
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703800084330171421)
,p_name=>'P86_S_PLAZO_PAGO'
,p_item_sequence=>370
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Plazo Pago'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703800411571171421)
,p_name=>'P86_MEN_MENSAJE_DESC'
,p_item_sequence=>380
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Mensaje'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'select men_mensaje mensaje, men_codigo from gen_mensaje WHERE MEN_EMPR= :P_EMPRESA order by 1'
,p_lov_display_null=>'YES'
,p_lov_null_text=>'%'
,p_cHeight=>1
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703800860704171421)
,p_name=>'P86_DOC_IND_IMPR_CONYUGUE'
,p_item_sequence=>390
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Codeudor(Conyuge)'
,p_display_as=>'NATIVE_YES_NO'
,p_grid_label_column_span=>1
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'CUSTOM'
,p_attribute_02=>'S'
,p_attribute_03=>'Si'
,p_attribute_04=>'N'
,p_attribute_05=>'No'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703801256608171422)
,p_name=>'P86_DOC_IND_IMPR_VENDEDOR'
,p_item_sequence=>400
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Codeudor(Vendedor)'
,p_display_as=>'NATIVE_YES_NO'
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'CUSTOM'
,p_attribute_02=>'S'
,p_attribute_03=>'Si'
,p_attribute_04=>'N'
,p_attribute_05=>'No'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703801631407171422)
,p_name=>'P86_PROFORMA'
,p_item_sequence=>410
,p_item_plug_id=>wwv_flow_api.id(963630287610068454)
,p_prompt=>'Proforma'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>1
,p_display_when=>'P_EMPRESA'
,p_display_when2=>'1'
,p_display_when_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703802310550171422)
,p_name=>'P86_F_TOT_DTO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(963630383935068455)
,p_item_default=>'0'
,p_prompt=>'Total DTO'
,p_format_mask=>'999G999G999G999G990D0000'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703802709648171422)
,p_name=>'P86_F_TOT_IVA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(963630383935068455)
,p_item_default=>'0'
,p_prompt=>'Total IVA'
,p_format_mask=>'999G999G999G999G990D0000'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703803186292171423)
,p_name=>'P86_F_TOTAL'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(963630383935068455)
,p_prompt=>'Total NETO'
,p_format_mask=>'999G999G999G999G990D0000'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703803599060171423)
,p_name=>'P86_IND_CARGA_DETALLE'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(963630383935068455)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703809343537171437)
,p_name=>'P86_DOCU_CLAVE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(1151960737685758479)
,p_prompt=>'doc_clave'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703810077880171438)
,p_name=>'P86_W_EMPR'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(1139577059517019672)
,p_prompt=>'W Empr'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703810425945171439)
,p_name=>'P86_EMPR_DESC'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(1139577059517019672)
,p_prompt=>'Empr Desc'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703810886012171439)
,p_name=>'P86_W_SUC'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(1139577059517019672)
,p_prompt=>'W Suc'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703811257140171440)
,p_name=>'P86_SUC_DESC'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(1139577059517019672)
,p_prompt=>'Suc Desc'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703811616069171441)
,p_name=>'P86_BX_S_DEP'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(1139577059517019672)
,p_prompt=>'Bx S Dep'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703812044670171441)
,p_name=>'P86_S_EXISTENCIA'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(1139577059517019672)
,p_prompt=>'S Existencia'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703812469731171442)
,p_name=>'P86_S_MAX_FACTURAR'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(1139577059517019672)
,p_prompt=>'S Max Facturar'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703812864809171442)
,p_name=>'P86_S_REMITIDO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(1139577059517019672)
,p_prompt=>'S Remitido'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703813256172171442)
,p_name=>'P86_BX_S_PEDIDO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(1139577059517019672)
,p_prompt=>'Bx S Pedido'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703813652246171442)
,p_name=>'P86_W_IND_LISTVAL'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(1139577059517019672)
,p_prompt=>'W Ind Listval'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703814333304171443)
,p_name=>'P86_W_TOTAL_KG'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'W Total Kg'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703814761006171443)
,p_name=>'P86_DET_CANT_KILOS'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Det Cant Kilos'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703815195897171443)
,p_name=>'P86_DET_COD_IVA'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Det Cod Iva'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703815537764171443)
,p_name=>'P86_IMPU_PORCENTAJE'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Impu Porcentaje'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703815982802171444)
,p_name=>'P86_IMPU_PORC_BASE_IMPO'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Impu Porc Base Impo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703816357337171444)
,p_name=>'P86_DET_ART'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Det Art'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703816785686171444)
,p_name=>'P86_AREM_MON'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Arem Mom'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703817158715171444)
,p_name=>'P86_ART_UNID_MED'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Art Unid Med'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703817577463171444)
,p_name=>'P86_ART_FACTOR_CONVERSION'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Art Factor Conversion'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703817948493171444)
,p_name=>'P86_AREM_PRECIO_VTA'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Arem Precio Vta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703818380762171445)
,p_name=>'P86_ARDE_CANT_ACT'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Arde Cant Act'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703818755359171445)
,p_name=>'P86_ARDE_UBIC'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Arde Ubic'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703819167183171445)
,p_name=>'P86_W_SALDO_PED_SUC_IMP'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'W Saldo Ped Suc Imp'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703819533909171445)
,p_name=>'P86_W_CANT_REM'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'W Cant Rem'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703819924004171445)
,p_name=>'P86_ART_IND_ANIMAL'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Art Ind Animal'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703820325665171446)
,p_name=>'P86_DET_NRO_REMIS'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Det Nro Remis'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
end;
/
begin
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703820732076171446)
,p_name=>'P86_FECHA_REMISION'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Fecha Remision'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703821109968171446)
,p_name=>'P86_DET_CANT_PEDIDO'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Det Cant Pedido'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703821546254171446)
,p_name=>'P86_DET_CANT_GUARDADA'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Det Cant Guardada'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703821993229171446)
,p_name=>'P86_ART_IND_FACT_NEGATIVO'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Art Ind Fact Negativo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703822315401171447)
,p_name=>'P86_ART_CONCEPTO'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Art Concepto'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703822783983171448)
,p_name=>'P86_ART_CTA_CONTABLE'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'Art Cta Contable'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703823150585171448)
,p_name=>'P86_W_BRUTO_GRAV_MON'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'W_BRUTO_GRAV_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703823517345171449)
,p_name=>'P86_W_BRUTO_GRAV_LOC'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'P86_W_NETO_GRAV_LOC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703823939032171449)
,p_name=>'P86_W_BRUTO_EXEN_LOC'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'P86_W_BRUTO_EXEN_LOC'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(703824362458171449)
,p_name=>'P86_W_BRUTO_EXEN_MON'
,p_item_sequence=>270
,p_item_plug_id=>wwv_flow_api.id(1139574573093019647)
,p_prompt=>'P86_W_BRUTO_EXEN_MON'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_api.id(25535943977951769)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(703837490394171472)
,p_validation_name=>'validar_cuotas'
,p_validation_sequence=>10
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
' declare',
' valor number;',
' begin',
' ',
' SELECT count(*) into valor',
'   FROM APEX_COLLECTIONS',
'  WHERE COLLECTION_NAME = ''CUOTA_FACI052'';',
' ',
' if :P86_S_TIPO_FACTURA = ''2'' and valor = 0 then ',
' raise_application_error(-20010, ''No puede insertar sin generar cuotas.'');',
' end if;',
' ',
' end;'))
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_when_button_pressed=>wwv_flow_api.id(703825489757171451)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703856340834171485)
,p_name=>'cambio_tipo_fac'
,p_event_sequence=>20
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_S_TIPO_FACTURA'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703857326755171485)
,p_event_id=>wwv_flow_api.id(703856340834171485)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P86_S_TIPO_FACTURA = 1 THEN ',
' :P86_S_TIPO_FACTURA_DESC := ''CONTADO'';',
' ',
' SELECT NVL(CONF_MAX_ITEMS_FAC_CO,0) ',
'   INTO :P86_MAX_ITEMS_FAC_CO',
'   FROM FAC_CONFIGURACION',
'  WHERE CONF_EMPR=:P_EMPRESA;',
'  ',
'  :P86_DOC_TIPO_MOV := :P86_CONF_COD_FAC_CONT;  ',
' ',
'ELSIF :P86_S_TIPO_FACTURA = 2 THEN ',
'',
' :P86_S_TIPO_FACTURA_DESC := ''CREDITO'';',
' ',
' :P86_DOC_TIPO_MOV := :P86_CONF_COD_FAC_CRED;',
' ',
' SELECT NVL(CONF_MAX_ITEMS_FAC_CR,0) ',
'   INTO :P86_MAX_ITEMS_FAC_CR',
'   FROM FAC_CONFIGURACION',
'  WHERE CONF_EMPR = :P_EMPRESA; ',
' ',
'ELSIF :P86_S_TIPO_FACTURA = 3 THEN ',
'',
' :P86_S_TIPO_FACTURA_DESC := ''VTA. IVA INCLUIDO'';',
' :P86_DOC_TIPO_MOV := :P86_CONF_COD_FAC_CONT; ',
' SELECT NVL(CONF_MAX_ITEMS_FAC_CO,0) ',
'   INTO :P86_MAX_ITEMS_FAC_CO',
'   FROM FAC_CONFIGURACION',
'  WHERE CONF_EMPR = :P_EMPRESA;',
'         ',
'END IF;',
''))
,p_attribute_02=>'P86_S_TIPO_FACTURA,P86_CONF_COD_FAC_CRED,P86_CONF_COD_FAC_CONT'
,p_attribute_03=>'P86_S_TIPO_FACTURA_DESC,P86_MAX_ITEMS_FAC_CO,P86_MAX_ITEMS_FAC_CR,P86_DOC_TIPO_MOV'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703856887756171485)
,p_event_id=>wwv_flow_api.id(703856340834171485)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P86_DOC_SERIE := ''C'';',
'BEGIN',
'      FACI053.PP_BUSCAR_NRO_FACTURA;',
'END;'))
,p_attribute_02=>'P86_DOC_TIPO_MOV'
,p_attribute_03=>'P86_DOC_NRO_DOC,P86_DOC_SERIE,P86_DOC_TIMBRADO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703901134328171512)
,p_name=>'cambio_fac_desc_contado'
,p_event_sequence=>30
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_S_TIPO_FACTURA_DESC'
,p_condition_element=>'P86_S_TIPO_FACTURA_DESC'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'CONTADO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703901606819171512)
,p_event_id=>wwv_flow_api.id(703901134328171512)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DOC_MON_DESC'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703902180561171513)
,p_event_id=>wwv_flow_api.id(703901134328171512)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DOC_CTA_BCO'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703929208076171530)
,p_name=>'cambio_fac_desc_credito'
,p_event_sequence=>40
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_S_TIPO_FACTURA_DESC'
,p_condition_element=>'P86_S_TIPO_FACTURA_DESC'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'CREDITO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703929758275171531)
,p_event_id=>wwv_flow_api.id(703929208076171530)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DOC_CTA_BCO'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703930296684171531)
,p_event_id=>wwv_flow_api.id(703929208076171530)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DOC_MON_DESC'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703930606128171532)
,p_name=>'cambio_fac_desc_vta_iva_incluido'
,p_event_sequence=>50
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_S_TIPO_FACTURA_DESC'
,p_condition_element=>'P86_S_TIPO_FACTURA_DESC'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'VTA.IVA INCLUIDO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703931680592171532)
,p_event_id=>wwv_flow_api.id(703930606128171532)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DOC_MON_DESC'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703931103863171532)
,p_event_id=>wwv_flow_api.id(703930606128171532)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DOC_CTA_BCO'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703923489245171525)
,p_name=>'tipo_fac_1_or_3'
,p_event_sequence=>60
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_S_TIPO_FACTURA'
,p_condition_element=>'P86_S_TIPO_FACTURA'
,p_triggering_condition_type=>'IN_LIST'
,p_triggering_expression=>'1,3'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
,p_display_when_type=>'NEVER'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703924448611171526)
,p_event_id=>wwv_flow_api.id(703923489245171525)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825890241171451)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703926452205171527)
,p_event_id=>wwv_flow_api.id(703923489245171525)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DOC_IND_IMPR_VENDEDOR'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703924939303171526)
,p_event_id=>wwv_flow_api.id(703923489245171525)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703826298838171451)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703926989195171528)
,p_event_id=>wwv_flow_api.id(703923489245171525)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DOC_IND_IMPR_CONYUGUE'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703923916329171526)
,p_event_id=>wwv_flow_api.id(703923489245171525)
,p_event_result=>'FALSE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825890241171451)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703925450428171526)
,p_event_id=>wwv_flow_api.id(703923489245171525)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DOC_IND_IMPR_CONYUGUE'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703925966374171526)
,p_event_id=>wwv_flow_api.id(703923489245171525)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DOC_IND_IMPR_VENDEDOR'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703927314800171529)
,p_name=>'tipo_fac_2'
,p_event_sequence=>70
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_S_TIPO_FACTURA'
,p_condition_element=>'P86_S_TIPO_FACTURA'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'2'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(702611663217493714)
,p_event_id=>wwv_flow_api.id(703927314800171529)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703826298838171451)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703927861429171530)
,p_event_id=>wwv_flow_api.id(703927314800171529)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'let valor = apex.item( "P86_CONF_IND_CUOTA_AUTOMATICA" ).getValue();',
'',
'if ( valor  = ''S'' ) {',
' ',
' document.getElementById(''CUOTAS'').disabled=false;',
' let x = ''S'';',
' $s("P86_IND_CUOTA", x);',
'',
'} else {',
'    ',
'    valor = apex.item("P86_IND_CUOTA").getValue();',
'    ',
'    if (valor = ''N'') {',
'        ',
'      x = ''S'';',
'      $s("P86_IND_CUOTA", x);',
'      ',
'        document.getElementById(''CUOTAS'').disabled = true;',
'        ',
'    }',
'    ',
'}',
'',
'document.getElementById(''FPAGOS'').disabled = true;',
'document.getElementById(''CUOTAS'').disabled = true;',
''))
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703928311457171530)
,p_event_id=>wwv_flow_api.id(703927314800171529)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825890241171451)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703928883780171530)
,p_event_id=>wwv_flow_api.id(703927314800171529)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P86_DOC_CTA_BCO := null;',
':P86_DOC_IND_IMPR_CONYUGUE := ''S'';',
':P86_DOC_IND_IMPR_VENDEDOR := ''S'';'))
,p_attribute_02=>'P86_DOC_CTA_BCO'
,p_attribute_03=>'P86_DOC_IND_IMPR_VENDEDOR,P86_DOC_IND_IMPR_CONYUGUE'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703871587856171494)
,p_name=>'recibir_focus'
,p_event_sequence=>80
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_DOC_FEC_DOC'
,p_condition_element=>'P86_DOC_FEC_DOC'
,p_triggering_condition_type=>'NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'focusin'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703872012988171494)
,p_event_id=>wwv_flow_api.id(703871587856171494)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DOC_FEC_DOC'
,p_attribute_01=>'PLSQL_EXPRESSION'
,p_attribute_04=>'sysdate'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703872427759171494)
,p_name=>'perder_focus'
,p_event_sequence=>90
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_DOC_FEC_DOC'
,p_bind_type=>'bind'
,p_bind_event_type=>'focusout'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703872984320171495)
,p_event_id=>wwv_flow_api.id(703872427759171494)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P86_DOC_FEC_DOC NOT BETWEEN :P86_CONF_PER_ACT_INI',
'                                  AND :P86_CONF_PER_SGTE_FIN',
'THEN',
'  RAISE_APPLICATION_ERROR(-20010,''Fecha debe estar comprendida en el rango de: ''',
'   ||TO_CHAR(:P86_CONF_PER_ACT_INI,''DD-MM-YYYY'')||'' a: ''',
'   ||TO_CHAR(:P86_CONF_PER_SGTE_FIN,''DD-MM-YYYY'')',
'   ||'' para el sistema de FINANZAS!'');',
'END IF;',
'',
'IF :P86_DOC_FEC_DOC NOT BETWEEN :P86_CONF_STK_PER_ACT_INI',
'                                  AND :P86_CONF_STK_PER_SGTE_FIN',
'THEN',
'  RAISE_APPLICATION_ERROR(-20010,''Fecha debe estar comprendida en el rango de: ''',
'   ||TO_CHAR(:P86_CONF_STK_PER_ACT_INI,''DD-MM-YYYY'')||'' a: ''',
'   ||TO_CHAR(:P86_CONF_STK_PER_SGTE_FIN,''DD-MM-YYYY'')',
'   ||'' para el sistema de STOCK!'');',
'END IF;',
'',
'',
'BEGIN',
'  -- Call the procedure',
'  PL_VALIDAR_HAB_MES_FIN(FECHA   => :P86_DOC_FEC_DOC,',
'                         EMPRESA => :P_EMPRESA,',
'                         USUARIO => :APP_USER);',
'END;',
'',
'',
'BEGIN',
'  -- Call the procedure',
'  PL_VALIDAR_HAB_MES_FIN(FECHA   => :P86_DOC_FEC_DOC,',
'                         EMPRESA => :P_EMPRESA,',
'                         USUARIO => :APP_USER);',
'',
'  PL_VALIDAR_HAB_MES_STK(FECHA   => :P86_DOC_FEC_DOC,',
'                         EMPRESA => :P_EMPRESA,',
'                         USUARIO => :APP_USER);',
'',
'EXCEPTION',
' WHEN OTHERS THEN',
'  RAISE_APPLICATION_ERROR(-20010, ''ERROR EN VALIDACION DE FECHAS DE PERIODOS FIN O STK''|| SQLCODE);',
'END;',
'',
''))
,p_attribute_02=>'P86_DOC_FEC_DOC,P86_CONF_PER_ACT_INI,P86_CONF_PER_SGTE_FIN,P86_CONF_STK_PER_ACT_INI,P86_CONF_STK_PER_SGTE_FIN,P86_DOC_MON'
,p_attribute_03=>'P86_W_DOC_TASA_US'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703873301708171495)
,p_name=>'cambiar_valor_pedido'
,p_event_sequence=>120
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_S_NRO_PEDIDO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703873811380171495)
,p_event_id=>wwv_flow_api.id(703873301708171495)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P86_S_NRO_PEDIDO IS NOT NULL THEN',
'',
' -- PP_BUSCAR_PEDIDO BUSCAMOS EL PEDIDO SI EXISTE ',
'  DECLARE',
'    V_ESTADO VARCHAR2(1);',
'  BEGIN',
'  ',
'    SELECT FAC_PEDIDO.PED_ESTADO',
'      INTO V_ESTADO',
'      FROM FAC_PEDIDO',
'     WHERE FAC_PEDIDO.PED_NRO = :P86_S_NRO_PEDIDO',
'       AND PED_EMPR = :P_EMPRESA;',
'  ',
'    IF V_ESTADO = ''F'' THEN',
'      RAISE_APPLICATION_ERROR(-20010,''El pedido ya ha sido facturado'');',
'    END IF;',
'  ',
'    SELECT PED_CLAVE,',
'           PED_MON,',
'           PED_CLI,',
'           DECODE(PED_CLI_NOM, NULL, CLI_NOM, PED_CLI_NOM),',
'           DECODE(PED_CLI_DIR, NULL, CLI_DIR, PED_CLI_DIR),',
'           CLI_RUC,',
'           SUBSTR(DECODE(PED_CLI_TEL, NULL, CLI_TEL, PED_CLI_TEL), 1, 15),',
'           PED_OBS,',
'           PED_LIST_PRECIO',
'      INTO :P86_DOC_CLI_NRO_PED,',
'           :P86_DOC_MON,',
'           :P86_DOC_CLI,',
'           :P86_DOC_CLI_NOM,',
'           :P86_CLI_DIR,',
'           :P86_DOC_CLI_RUC,',
'           :P86_DOC_CLI_TEL,',
'           :P86_MEN_MENSAJE_DESC,',
'           :P86_LIPR_NRO_LISTA_PRECIO',
'      FROM FAC_PEDIDO, GEN_MONEDA, FIN_CLIENTE, FIN_FICHA_HOLDING',
'     WHERE PED_MON = MON_CODIGO',
'       AND PED_CLI = CLI_CODIGO(+)',
'       AND CLI_COD_FICHA_HOLDING = HOL_CODIGO(+)',
'       AND PED_MON = HOL_MON(+)',
'       AND PED_NRO = :P86_S_NRO_PEDIDO',
'       AND PED_EMPR = MON_EMPR',
'       AND PED_EMPR = CLI_EMPR(+)',
'       AND CLI_EMPR = HOL_EMPR(+)',
'       AND PED_EMPR = :P_EMPRESA;',
'    ',
'  EXCEPTION',
'    WHEN NO_DATA_FOUND THEN',
'      :P86_DOC_CLI_NRO_PED := NULL;',
'      RAISE_APPLICATION_ERROR(-20010,''No existe el pedido. Verifique!'');',
'    ',
'  END;',
' ',
' ',
'END IF;',
'',
'',
'',
''))
,p_attribute_02=>'P86_S_NRO_PEDIDO'
,p_attribute_03=>'P86_DOC_CLI_NRO_PED,P86_DOC_MON,P86_DOC_CLI,P86_DOC_CLI_NOM,P86_CLI_DIR,P86_DOC_CLI_RUC,P86_DOC_CLI_TEL,P86_MEN_MENSAJE_DESC,P86_LIPR_NRO_LISTA_PRECIO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703874282945171495)
,p_name=>'cambio_valor_cli'
,p_event_sequence=>130
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_DOC_CLI'
,p_condition_element=>'P86_DOC_CLI'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703875759529171496)
,p_event_id=>wwv_flow_api.id(703874282945171495)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  V_NRO_LISTA   NUMBER;',
'  V_DEFINICION  VARCHAR2(1);',
'  V_ZONA        VARCHAR2(100);',
'  CANT_FAC_PERM NUMBER;',
'  CANT_FAC_PEND NUMBER;',
'  COMP          VARCHAR2(2);',
'BEGIN',
'',
'IF :P86_S_TIPO_FACTURA = 2 AND :P86_EXPORTACION = ''N'' THEN  ',
'    ',
'      FAC_CANT_FACT_PENDIENTE(:P86_DOC_CLI,',
'                              CANT_FAC_PERM,',
'                              CANT_FAC_PEND,',
'                              :P_EMPRESA);',
'    ',
'    IF CANT_FAC_PERM <= CANT_FAC_PEND THEN',
'        ',
'        BEGIN',
'          SELECT ''X''',
'            INTO COMP',
'            FROM FIN_AUTORIZ_ESPEC',
'           WHERE AUES_CLI = :P86_DOC_CLI',
'             AND AUES_FEC_AUTORIZ = TO_DATE(TO_CHAR(:P86_DOC_FEC_DOC, ''DD/MM/YYYY''), ''DD/MM/YYYY'')',
'             AND AUES_UTILIZADA = ''N''',
'             AND AUES_FAC_ADI = ''S''',
'             AND AUES_EMPR = :P_EMPRESA;',
'             ',
'          :P86_P_LIM_FAC_ADIC := ''S'';',
unistr('          RAISE_APPLICATION_ERROR(-20010,''El holding alcanz\00F3 su limite de facturas pendientes pero podra facturar mediante que se otorgo un permiso especial'');'),
'',
'        EXCEPTION',
'          WHEN NO_DATA_FOUND THEN',
unistr('            RAISE_APPLICATION_ERROR(-20010,''El holding alcanz\00F3 el l\00EDmite de facturas pendientes permitidas Cantidad permitida: '' || CANT_FAC_PERM || '' Cantidad pendientes: '' || CANT_FAC_PEND);'),
'        END;',
'        ',
'      END IF;',
'  END IF;',
'  ',
'END;'))
,p_attribute_02=>'P86_S_TIPO_FACTURA,P86_EXPORTACION,P86_DOC_CLI,P86_DOC_FEC_DOC'
,p_attribute_03=>'P86_P_LIM_FAC_ADIC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703874758762171495)
,p_event_id=>wwv_flow_api.id(703874282945171495)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
' IF :P_EMPRESA = 1 THEN',
'  FACI053.PP_VALIDA_CANAL;',
' END IF;',
'',
' BEGIN',
'-- CONTROL CLIENTE',
'  FACI053.PP_CONTROL_CLIENTE(INDICADOR => :P86_INDICADOR_CNTRL_CLI);',
' END;',
'',
'',
'',
'',
''))
,p_attribute_03=>'P86_INDICADOR_CNTRL_CLI'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703876233688171496)
,p_event_id=>wwv_flow_api.id(703874282945171495)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
' DECLARE',
'    V_CLI_REP NUMBER;',
'  BEGIN',
'    SELECT COUNT(*)',
'      INTO V_CLI_REP',
'      FROM FIN_CLIENTE',
'     WHERE (CLI_DIADOM IS NOT NULL OR CLI_DIALUN IS NOT NULL OR',
'            CLI_DIAMAR IS NOT NULL OR CLI_DIAMIE IS NOT NULL OR',
'            CLI_DIAJUE IS NOT NULL OR CLI_DIAVIE IS NOT NULL OR',
'            CLI_DIASAB IS NOT NULL)',
'       AND CLI_PEDIDO_REPETITIVO IS NULL',
'       AND CLI_CODIGO = :P86_DOC_CLI',
'       AND CLI_EMPR = :P_EMPRESA;',
'  ',
'',
' IF V_CLI_REP > 0 THEN',
'   :P86_IND_PED_AUTO := 1;',
' ELSE',
'   :P86_IND_PED_AUTO := 0;',
' END IF;',
'',
'END;',
'',
''))
,p_attribute_02=>'P86_DOC_CLI'
,p_attribute_03=>'P86_IND_PED_AUTO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703875260235171496)
,p_event_id=>wwv_flow_api.id(703874282945171495)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'-- VALIDAR CLIENTE   ',
'DECLARE',
'',
'  V_BLOQUEADO   FIN_CLIENTE.CLI_BLOQ_LIM_CR%TYPE;',
'  V_ESTADO      FIN_CLIENTE.CLI_EST_CLI%TYPE;',
'  C_VARIABLE    FIN_CLIENTE.CLI_CATEG%TYPE;',
'  C_VARIABLE2   FIN_CLIENTE.CLI_PORC_EXEN_IVA%TYPE;',
'  C_VARIABLE3   FIN_CLIENTE.CLI_IMP_LIM_CR%TYPE;',
'  ',
'',
'--*',
'BEGIN',
'  SELECT CLI_CATEG, ',
'         CLI_BLOQ_LIM_CR, CLI_EST_CLI, CLI_PORC_EXEN_IVA,',
'         CLI_IMP_LIM_CR',
'  INTO   C_VARIABLE,',
'         V_BLOQUEADO,',
'         V_ESTADO,',
'         C_VARIABLE2, ',
'         C_VARIABLE3',
'  FROM   FIN_CLIENTE',
'  WHERE  CLI_CODIGO = :P86_DOC_CLI',
'  AND CLI_EMPR= :P_EMPRESA;',
'  ',
'  ',
'',
'  IF  V_ESTADO NOT IN (''A'',''M'') THEN',
'    IF :P86_S_TIPO_FACTURA = 2 THEN',
'      RAISE_APPLICATION_ERROR(-20010,''EL cliente no es activo.!'');',
'    ELSE',
'      RAISE_APPLICATION_ERROR(-20010, ''El cliente no es activo. Desea continuar?'');',
'    END IF;',
'  END IF;',
'  ',
'  IF  V_ESTADO = ''M'' THEN',
'      :P86_VAL_MOROSO:=1;',
'      --RAISE_APPLICATION_ERROR(-20010, ''El cliente es moroso.'');',
'  END IF;',
'  ',
'',
'  IF  V_BLOQUEADO = ''S'' THEN',
'    IF :P86_S_TIPO_FACTURA = 2 THEN',
'      RAISE_APPLICATION_ERROR(-20010,''El limite de cr?dito del cliente se encuentra bloqueado.!'');',
'    ELSE',
'        RAISE_APPLICATION_ERROR(-20010, ''El limite de cr?dito del cliente se encuentra bloqueado!. Desea continuar?'');',
'     END IF;',
'  END IF;',
'',
'    ---------------------------------PP_BUSCAR_VEND_CLIENTE;',
'',
'  :P86_W_CATEG             := C_VARIABLE;',
'  :P86_W_CLI_PORC_EXEN_IVA := C_VARIABLE2;',
'  :P86_W_IMP_LIM_CR_GRUPO  := C_VARIABLE3;',
'',
'',
'EXCEPTION',
'  WHEN NO_DATA_FOUND THEN',
'     RAISE_APPLICATION_ERROR(-20010, ''Cliente inexistente.'');',
'END;  ',
'',
'DECLARE',
'VALORES NUMBER;',
'BEGIN',
'  FACI053.PP_TRAER_DESC_CLI(P_DOC_CLI                     => :P86_DOC_CLI,',
'                            P_EMPRESA                     => :P_EMPRESA,',
'                            P_DOC_FEC_DOC                 => :P86_DOC_FEC_DOC,',
'                            P_W_IMP_LIM_CR_EMPR           => :P86_W_IMP_LIM_CR_EMPR,',
'                            P_W_IMP_LIM_CR_DISP_GRUPO     => :P86_W_IMP_LIM_CR_DISP_GRUPO,',
'                            P_W_IMP_LIM_CR_DISP_EMPR      => :P86_W_IMP_LIM_CR_DISP_EMPR,',
'                            P_W_IMP_CHEQ_DIFERIDO         => :P86_W_IMP_CHEQ_DIFERIDO,',
'                            P_W_IMP_CHEQ_RECHAZADO        => :P86_W_IMP_CHEQ_RECHAZADO,',
'                            P_CLI_NOM                     => :P86_DOC_CLI_NOM,',
'                            P_CLI_DIR                     => :P86_CLI_DIR,',
'                            P_DOC_CLI_TEL                 => :P86_DOC_CLI_TEL,',
'                            P_DOC_CLI_RUC                 => :P86_DOC_CLI_RUC,',
'                            P_CLI_PERS_REPRESENTANTE      => :P86_CLI_PERS_REPRE,',
'                            P_CLI_DOC_IDENT_REPRESENTANTE => :P86_CLI_DOC_IDENT_REPRE,',
'                            P_W_CLI_MAX_DIAS_ATRASO       => :P86_W_CLI_MAX_DIAS_ATRASO,',
'                            P_CLI_LOCALIDAD               => :P86_CLI_LOCALIDAD,',
'                            W_CLI_MAX_DIAS_ATRASO         => VALORES,',
'                            P_DOC_CANAL                   => :P86_DOC_CANAL,',
'                            P_CLI_IND_MOD_CANAL           => :P86_CLI_IND_MOD_CANAL',
'                            ,O_CLI_DOC_TIPO                =>:P86_CLI_DOC_TIPO',
'                            ,out_vendedor        => :P86_DOC_LEGAJO',
'                           );',
'END;',
'     ',
'--RAISE_APPLICATION_ERROR(-20010, :P86_W_CLI_MAX_DIAS_ATRASO||'' YOU NEEDR'');',
'--P86_W_IMP_LIM_CR_EMPR,P86_W_IMP_LIM_CR_DISP_GRUPO,P86_W_IMP_LIM_CR_DISP_EMPR,P86_W_IMP_CHEQ_DIFERIDO,P86_W_IMP_CHEQ_RECHAZADO,P86_DOC_CLI_NOM,P86_CLI_DIR,P86_DOC_CLI_TEL,P86_DOC_CLI_RUC,P86_CLI_PERS_REPRE,P86_CLI_DOC_IDENT_REPRE,P86_W_CLI_MAX_DIAS_'
||'ATRASO,P86_CLI_LOCALIDAD,P86_DOC_CANAL,P86_CLI_IND_MOD_CANAL,P86_W_CATEG,P86_W_CLI_PORC_EXEN_IVA,P86_W_IMP_LIM_CR_GRUPO,P86_VAL_MOROSO',
'',
''))
,p_attribute_02=>'P86_DOC_CLI,P86_DOC_FEC_DOC,P86_W_IMP_LIM_CR_EMPR,P86_W_IMP_LIM_CR_DISP_GRUPO,P86_W_IMP_LIM_CR_DISP_EMPR,P86_W_IMP_CHEQ_DIFERIDO,P86_W_IMP_CHEQ_RECHAZADO,P86_S_TIPO_FACTURA'
,p_attribute_03=>'P86_W_IMP_LIM_CR_EMPR,P86_W_IMP_LIM_CR_DISP_GRUPO,P86_W_IMP_LIM_CR_DISP_EMPR,P86_W_IMP_CHEQ_DIFERIDO,P86_W_IMP_CHEQ_RECHAZADO,P86_DOC_CLI_NOM,P86_CLI_DIR,P86_DOC_CLI_TEL,P86_DOC_CLI_RUC,P86_CLI_PERS_REPRE,P86_CLI_DOC_IDENT_REPRE,P86_W_CLI_MAX_DIAS_AT'
||'RASO,P86_CLI_LOCALIDAD,P86_DOC_CANAL,P86_CLI_IND_MOD_CANAL,P86_W_CATEG,P86_W_CLI_PORC_EXEN_IVA,P86_W_IMP_LIM_CR_GRUPO,P86_VAL_MOROSO,P86_CLI_DOC_TIPO,P86_DOC_LEGAJO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703876646373171496)
,p_name=>'cambio_valor_ind_cntrl_cliente'
,p_event_sequence=>140
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_INDICADOR_CNTRL_CLI'
,p_condition_element=>'P86_INDICADOR_CNTRL_CLI'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'1'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703878180741171498)
,p_event_id=>wwv_flow_api.id(703876646373171496)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DOC_CLI_NOM'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703877197291171496)
,p_event_id=>wwv_flow_api.id(703876646373171496)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DOC_CLI_NOM'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703877659704171498)
,p_event_id=>wwv_flow_api.id(703876646373171496)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_W_CATEG'
,p_attribute_01=>'FUNCTION_BODY'
,p_attribute_06=>':P86_W_CATEG := :P86_CONF_CATEG_CLI_ESPO;'
,p_attribute_07=>'P86_CONF_CATEG_CLI_ESPO'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
end;
/
begin
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703878510008171498)
,p_name=>'cambio_valor_valid_cntd_pedido'
,p_event_sequence=>150
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_DOC_CLI'
,p_condition_element=>'P86_IND_PED_AUTO'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'1'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703879566675171499)
,p_event_id=>wwv_flow_api.id(703878510008171498)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_CONFIRM'
,p_attribute_01=>unistr('El cliente posee pedidos repetitivos, \00BFdesea generarlo autom\00E1ticamente?')
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703880017253171500)
,p_event_id=>wwv_flow_api.id(703878510008171498)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':P86_PED_AUTO := NULL;'
,p_attribute_03=>'P86_PED_AUTO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703879051765171499)
,p_event_id=>wwv_flow_api.id(703878510008171498)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P86_PED_AUTO := ''S'';',
'',
'',
'  '))
,p_attribute_03=>'P86_PED_AUTO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703880574427171500)
,p_event_id=>wwv_flow_api.id(703878510008171498)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  V_NRO_LISTA   NUMBER;',
'  V_DEFINICION  VARCHAR2(1);',
'  V_ZONA        VARCHAR2(100);',
'  CANT_FAC_PERM NUMBER;',
'  CANT_FAC_PEND NUMBER;',
'  COMP          VARCHAR2(2);',
'',
'BEGIN',
'',
'BEGIN',
'      SELECT FIN_CLIENTE.CLI_REC_LPREC,',
'             DECODE(FIN_CLIENTE.CLI_REC_LPREC,',
'                    ''C'',',
'                    H.HOL_NRO_LISTA_PRECIO,',
'                    FAC_ZONA.ZONA_LPRECIO),',
'             FAC_ZONA.ZONA_CODIGO || ''-  '' || FAC_ZONA.ZONA_DESC',
'        INTO V_DEFINICION, V_NRO_LISTA, V_ZONA',
'        FROM FIN_CLIENTE,',
'             FAC_ZONA,',
'             FIN_FICHA_HOLDING H,',
'             FAC_LISTA_PRECIO  C,',
'             FAC_LISTA_PRECIO  Z',
'       WHERE FAC_ZONA.ZONA_CODIGO = FIN_CLIENTE.CLI_ZONA',
'         AND FIN_CLIENTE.CLI_COD_FICHA_HOLDING = H.HOL_CODIGO',
'         AND FIN_CLIENTE.CLI_CODIGO = :P86_DOC_CLI',
'         AND H.HOL_NRO_LISTA_PRECIO = C.LIPE_NRO_LISTA_PRECIO(+)',
'         AND FAC_ZONA.ZONA_LPRECIO = Z.LIPE_NRO_LISTA_PRECIO(+)',
'         AND CLI_EMPR = ZONA_EMPR',
'         AND CLI_EMPR = :P_EMPRESA',
'         AND ZONA_EMPR = HOL_EMPR',
'         AND HOL_EMPR = C.LIPE_EMPR(+)',
'         AND FAC_ZONA.ZONA_EMPR = Z.LIPE_EMPR(+);',
'    ',
'      IF V_NRO_LISTA IS NULL THEN',
'        IF V_DEFINICION = ''C'' THEN',
'          RAISE_APPLICATION_ERROR(-20010,''Falta seleccionar una lista de precios en FINM002'');',
'        ELSIF V_DEFINICION = ''Z'' THEN',
'          RAISE_APPLICATION_ERROR(-20010,''Falta seleccionar una lista de precios en FACM003, Zona= '' ||',
'                           V_ZONA);',
'        ELSE',
'          RAISE_APPLICATION_ERROR(-20010,''Las opciones para lista de precios son por Cliente o por Zona, verifique FACM015 o FINM002 o FACM003 '');',
'        END IF;',
'      END IF;',
'    ',
'EXCEPTION',
'      WHEN OTHERS THEN',
'        IF V_DEFINICION = ''C'' THEN',
'          RAISE_APPLICATION_ERROR(-20010,''Falta seleccionar una lista de precios en FINM002'');',
'        ELSIF V_DEFINICION = ''Z'' THEN',
'          RAISE_APPLICATION_ERROR(-20010,''Falta seleccionar una lista de precios en FACM003'');',
'        ELSE',
'          RAISE_APPLICATION_ERROR(-20010,''Las opciones para lista de precios son por Cliente o por Zona, verifique FACM015 o FINM002 o FACM003 '');',
'        END IF;',
'END;',
'',
'',
'DECLARE',
'      V_MENSAJE          VARCHAR2(500);',
'      V_EXISTEN_DETALLES VARCHAR2(1);',
'      V_ART_SIN_LISTA    VARCHAR2(500);',
'      CURSOR C_CURSOR IS',
'        SELECT ART_COD_ALFANUMERICO,',
'               FAC_LISTA_PRECIO_DET.LIPR_NRO_LISTA_PRECIO',
'          FROM STK_ARTICULO, FAC_CLI_PEDIDOS_REP, FAC_LISTA_PRECIO_DET',
'         WHERE FAC_CLI_PEDIDOS_REP.PREP_ART =',
'               FAC_LISTA_PRECIO_DET.LIPR_ART(+)',
'           AND STK_ARTICULO.ART_CODIGO = FAC_CLI_PEDIDOS_REP.PREP_ART',
'           AND (FAC_LISTA_PRECIO_DET.LIPR_NRO_LISTA_PRECIO = V_NRO_LISTA OR',
'               FAC_LISTA_PRECIO_DET.LIPR_NRO_LISTA_PRECIO IS NULL)',
'           AND FAC_CLI_PEDIDOS_REP.PREP_EMPR =',
'               FAC_LISTA_PRECIO_DET.LIPR_EMPR(+)',
'           AND ART_EMPR = FAC_CLI_PEDIDOS_REP.PREP_EMPR(+)',
'           AND ART_EMPR = :P_EMPRESA',
'           AND FAC_CLI_PEDIDOS_REP.PREP_CLI = :P86_DOC_CLI',
'         ORDER BY 1 DESC;',
'    ',
'BEGIN',
'    ',
'  FOR V_CURSOR IN C_CURSOR LOOP',
'    IF V_CURSOR.LIPR_NRO_LISTA_PRECIO IS NULL THEN',
'       V_ART_SIN_LISTA := V_CURSOR.ART_COD_ALFANUMERICO || '','' ||V_ART_SIN_LISTA;',
'     END IF;',
'  END LOOP;',
'    ',
'    IF V_ART_SIN_LISTA IS NOT NULL THEN',
'        V_MENSAJE := ''Algunos articulos del pedido a generar: '' ||',
'                     V_ART_SIN_LISTA ||',
unistr('                     ''no tiene asignados listas de precios N\00BA '' ||'),
'                     V_NRO_LISTA || '', favor revisar FACM015 y FACM013'';',
'      ',
'',
'        RAISE_APPLICATION_ERROR(-20010,V_MENSAJE);',
'      ',
'    END IF;',
'   ',
'END;',
'',
'END;'))
,p_attribute_02=>'P86_DOC_CLI'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703882308404171502)
,p_name=>'cambio_valor_canal'
,p_event_sequence=>160
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_DOC_CANAL'
,p_condition_element=>'P86_DOC_CANAL'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703882814446171502)
,p_event_id=>wwv_flow_api.id(703882308404171502)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_V_INI_NUM,P86_DOC_CANAL_INI'
,p_attribute_01=>'FUNCTION_BODY'
,p_attribute_06=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P86_V_INI_NUM := :P86_DOC_CANAL;',
':P86_DOC_CANAL_INI := :P86_DOC_CANAL;'))
,p_attribute_07=>'P86_DOC_CANAL'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703883395165171502)
,p_event_id=>wwv_flow_api.id(703882308404171502)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P86_CAN_DESC := FACI053.FP_MOSTRAR_CANAL;',
''))
,p_attribute_02=>'P86_DOC_CANAL'
,p_attribute_03=>'P86_CAN_DESC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703860593786171486)
,p_name=>'cambio_valor_banco'
,p_event_sequence=>170
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_DOC_CTA_BCO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703861022862171486)
,p_event_id=>wwv_flow_api.id(703860593786171486)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P86_S_TIPO_FACTURA = 1 OR :P86_S_TIPO_FACTURA = 3 THEN',
'  --si es factura contado o Comprob. Vta.',
' ',
'   BEGIN',
'     FACI053.PP_VERIF_EXIST_CTA_BCO(P_EMPRESA              => :P_EMPRESA,',
'                                   P_DOC_CTA_BCO           => :P86_DOC_CTA_BCO,',
'                                   P_BCO_DESC              => :P86_S_BANCO_CTA,',
'                                   P_CTA_DESC              => :P86_S_CTA_DESC,',
'                                   P_DOC_MON               => :P86_DOC_MON,',
'                                   P_CTA_CHEQ_DIF_RESPALDO => :P86_CTA_CHEQ_DIF_RESPALDO);',
'   END;',
'',
'',
'ELSE',
'  --si es factura credito',
'  :P86_DOC_CTA_BCO := NULL;',
'  :P86_S_BANCO_CTA := NULL;',
'  :P86_S_CTA_DESC := NULL;',
'END IF;',
''))
,p_attribute_02=>'P86_S_TIPO_FACTURA,P86_DOC_CTA_BCO'
,p_attribute_03=>'P86_S_BANCO_CTA,P86_S_CTA_DESC,P86_DOC_MON,P86_CTA_CHEQ_DIF_RESPALDO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703861462216171487)
,p_name=>'cambio_valor_mon'
,p_event_sequence=>190
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_DOC_MON'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703862925547171489)
,p_event_id=>wwv_flow_api.id(703861462216171487)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'  SELECT MON_DESC, MON_SIMBOLO, MON_DEC_IMP, MON_DEC_PRECIO',
'  INTO   :P86_DOC_MON_DESC,',
'         :P86_MON_SIMBOLO,',
'         :P86_W_MON_DEC_IMP,',
'         :P86_W_MON_DEC_PRECIO         ',
'  FROM   GEN_MONEDA',
'  WHERE MON_CODIGO = :P86_DOC_MON',
'    AND MON_EMPR = :P_EMPRESA;',
'  ',
'  IF :P86_DOC_MON = :P_MON_LOC THEN',
'	  :P86_W_DOC_TASA_US := 1;',
'	ELSE',
'   ',
'    :P86_W_DOC_TASA_US:=GEN_COT_TIPO_MOV(:P_EMPRESA,:P86_DOC_MON, :P86_DOC_TIPO_MOV, :P86_DOC_FEC_DOC);--mvera 15/12/2021.',
'   ',
'  ',
'   /*',
'    SELECT COT_TASA',
'      INTO :P86_W_DOC_TASA_US',
'      FROM STK_COTIZACION',
'     WHERE COT_FEC = :P86_DOC_FEC_DOC -- TO_CHAR(TRUNC(:P86_DOC_FEC_DOC), ''DD-MM-YYYY'')',
'       AND COT_MON = :P86_DOC_MON',
'       AND COT_EMPR = :P_EMPRESA;*/',
'                        ',
'  END IF;',
'  ',
'  IF  :P86_DOC_CLI IS NOT NULL THEN',
'    BEGIN',
'     FACI053.PP_BUSCAR_CLI_EMPR_MONEDA;',
'    END;',
'  END IF;',
'  ',
'-----------------------------------------  ',
'  ',
'-----------------------------------------',
'--P86_DOC_MON,P86_DOC_CLI,P86_DOC_FEC_DOC,P86_S_TIPO_FACTURA,P_EMPRESA,P_MON_LOC,P86_DOC_TIPO_MOV',
'--P86_DOC_MON_DESC,P86_MON_SIMBOLO,P86_W_MON_DEC_IMP,P86_W_MON_DEC_PRECIO,P86_W_DOC_TASA_US',
'',
' EXCEPTION',
'  WHEN NO_DATA_FOUND THEN',
unistr('     RAISE_APPLICATION_ERROR(-20010, ''Cotizaci\00F3n del dia no encontrada!'');'),
' WHEN OTHERS THEN',
'     RAISE_APPLICATION_ERROR(-20010, SQLCODE||'' Error ''||SQLERRM);',
'end;',
'',
'',
''))
,p_attribute_02=>'P86_DOC_MON,P86_DOC_CLI,P86_DOC_FEC_DOC,P86_S_TIPO_FACTURA,P_EMPRESA,P_MON_LOC,P86_DOC_TIPO_MOV'
,p_attribute_03=>'P86_DOC_MON_DESC,P86_MON_SIMBOLO,P86_W_MON_DEC_IMP,P86_W_MON_DEC_PRECIO,P86_W_DOC_TASA_US'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703863412590171489)
,p_event_id=>wwv_flow_api.id(703861462216171487)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'-- si cambio la moneda o si plazo de pago todavia no tiene datos',
'-- entonces se debe buscar plazo de pago y porcentaje de descuento',
'    BEGIN',
'      -- Call the procedure',
'      FACI053.PP_VALIDAR_MONEDA;',
'',
'    EXCEPTION',
'     WHEN OTHERS THEN ',
'      RAISE_APPLICATION_ERROR(-20010, '' Mensaje: ''||sqlerrm);',
'    END;',
'',
'END;',
'',
'',
'IF :P86_DOC_MON = 2 THEN',
'   BEGIN',
'    -- Call the procedure',
'    FACI053.PP_CTRL_LIM_CR;',
'   END;',
'END IF;',
''))
,p_attribute_02=>'P86_DOC_MON,P86_DOC_CLI,P86_S_NRO_PEDIDO,P86_W_ANT_DOC_MON,P86_S_PLAZO_PAGO,P86_DOC_CLI,P_MON_LOC,P_MON_US,P86_S_TIPO_FACTURA,P86_W_CLI_MAX_DIAS_ATRASO,P86_W_DOC_TASA_US,P86_DOC_FEC_DOC'
,p_attribute_03=>'P86_W_LIM_CR_ESPECIAL,P86_S_PORC_DTO,P86_S_PLAZO_PAGO,P86_S_RECARGO,P86_LIPR_NRO_LISTA_PRECIO,P86_W_CLI_MAX_DIAS_ATRASO,P86_RETORNO_VALIDAR_MON,P86_RETORNO_VALIDAR2_MON'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703862451990171488)
,p_event_id=>wwv_flow_api.id(703861462216171487)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P86_W_ANT_DOC_MON := :P86_DOC_MON;',
'',
'IF :P86_S_DEP IS NULL THEN ',
' :P86_S_DEP := 1;',
'END IF;'))
,p_attribute_02=>'P86_DOC_MON'
,p_attribute_03=>'P86_W_ANT_DOC_MON,P86_S_DEP'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703861947019171488)
,p_event_id=>wwv_flow_api.id(703861462216171487)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P86_RETORNO_VALIDAR_MON > :P86_W_CLI_MAX_DIAS_ATRASO THEN',
'NULL;',
'END IF;',
'',
''))
,p_attribute_02=>'P86_RETORNO_VALIDAR_MON,P86_W_CLI_MAX_DIAS_ATRASO'
,p_attribute_03=>'P86_RETORNO_VALIDAR2_MON'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703880915162171501)
,p_name=>'cambio_valor_ind_vencimiento'
,p_event_sequence=>210
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_IND_VENCIMIENTO'
,p_condition_element=>'P86_IND_VENCIMIENTO'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'VALIDAR'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703881422926171501)
,p_event_id=>wwv_flow_api.id(703880915162171501)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.message.confirm(''El cliente tiene un vencimiento con dias de atrasos.'', function(okPressed) {',
'      if(okPressed){',
'        document.getElementById(''CUOTAS'').disabled=false;',
'     }              ',
'})',
''))
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703881917359171501)
,p_event_id=>wwv_flow_api.id(703880915162171501)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.message.confirm(''El cliente tiene un vencimiento con ''||to_char(V_CANT_DIAS)||'' dias de atraso!. Desea continuar?'', ',
'                           function(okPressed) {',
'      if(okPressed){',
'        document.getElementById(''CUOTAS'').disabled=false;',
'        var x = 1;',
'        $s(''P86_IND_ALERT_AUT_ESP'', x);',
'     }else{',
'        var x = 2;',
'        $s(''P86_IND_ALERT_AUT_ESP'', x);',
'     }',
'})',
''))
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703919801849171524)
,p_name=>'cambio_valor_deposito'
,p_event_sequence=>230
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_BX_S_DEP'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703920362954171524)
,p_event_id=>wwv_flow_api.id(703919801849171524)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- Call the procedure',
'  PL_CONTROL_OPER_DEP(I_EMPRESA   => :P_EMPRESA,',
'                      I_SUCURSAL  => :P_SUCURSAL,',
'                      I_DEPOSITO  => :P86_S_DEP,',
'                      I_OPERACION => ''EXT'',',
'                      I_CLAVE     => :P86_CLAVE_STOPES);',
'END;',
''))
,p_attribute_02=>'P86_BX_S_DEP'
,p_attribute_03=>'P86_CLAVE_STOPES'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703920761088171524)
,p_name=>'cambio_valor_lista_precio'
,p_event_sequence=>240
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_LIPR_NRO_LISTA_PRECIO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703921236240171524)
,p_event_id=>wwv_flow_api.id(703920761088171524)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- Call the procedure',
'  FACI053.PP_VALIDAR_LISTA_PRECIOS;',
'END;',
''))
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703859654771171486)
,p_name=>'cambio_valor_ruc'
,p_event_sequence=>250
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_DOC_CLI_RUC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703860173289171486)
,p_event_id=>wwv_flow_api.id(703859654771171486)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- Call the procedure',
'  FACI053.PP_VALIDAR_RUC(P_VALOR => :P86_IND_RUC);',
'END;',
''))
,p_attribute_03=>'P86_IND_RUC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703863830271171490)
,p_name=>'cambio_valor_ind_ruc'
,p_event_sequence=>260
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_IND_RUC'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
,p_display_when_type=>'NEVER'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703864338964171490)
,p_event_id=>wwv_flow_api.id(703863830271171490)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'PLUGIN_BE.CTB.ALERTIFY'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'ALERT'
,p_attribute_04=>'Al RUC'
,p_attribute_07=>'OK'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703864750200171490)
,p_name=>'perder_focus_recargo'
,p_event_sequence=>270
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_S_RECARGO'
,p_condition_element=>'P86_S_RECARGO'
,p_triggering_condition_type=>'NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'focusout'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703865295703171491)
,p_event_id=>wwv_flow_api.id(703864750200171490)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_S_RECARGO'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'0'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703865669499171492)
,p_name=>'cambio_valor_vededor'
,p_event_sequence=>280
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_DOC_LEGAJO'
,p_condition_element=>'P86_DOC_LEGAJO'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703866602034171492)
,p_event_id=>wwv_flow_api.id(703865669499171492)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DOC_LEGAJO_DESC'
,p_attribute_01=>'SQL_STATEMENT'
,p_attribute_03=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT EMPL_NOMBRE A',
'  FROM PER_EMPLEADO',
' WHERE EMPL_LEGAJO = (SELECT VEND_LEGAJO',
'                        FROM FAC_VENDEDOR',
'                       WHERE VEND_LEGAJO = EMPL_LEGAJO',
'                         AND VEND_EMPR = :P_EMPRESA)',
'   AND EMPL_EMPRESA = :P_EMPRESA',
'   AND EMPL_LEGAJO  = :P86_DOC_LEGAJO'))
,p_attribute_07=>'P86_S_DEP,P86_DOC_LEGAJO'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703866105516171492)
,p_event_id=>wwv_flow_api.id(703865669499171492)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_W_ANT_DOC_LEGAJO'
,p_attribute_01=>'FUNCTION_BODY'
,p_attribute_06=>':P86_W_ANT_DOC_LEGAJO := :P86_DOC_LEGAJO;'
,p_attribute_07=>'P86_DOC_LEGAJO'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703867063305171492)
,p_name=>'perder_focus_porc_dto'
,p_event_sequence=>290
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_S_PORC_DTO'
,p_condition_element=>'P86_S_PORC_DTO'
,p_triggering_condition_type=>'NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'focusout'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703867575693171493)
,p_event_id=>wwv_flow_api.id(703867063305171492)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_S_PORC_DTO'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'0'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703867968148171493)
,p_name=>'cambio_valor_s_porc_dto'
,p_event_sequence=>300
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_S_PORC_DTO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703868451891171493)
,p_event_id=>wwv_flow_api.id(703867968148171493)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF NOT :P86_S_PORC_DTO BETWEEN 0 AND 100 THEN ',
' RAISE_APPLICATION_ERROR(-20010, ''Debe ser un valor entre 0 y 100!'');',
'END IF;'))
,p_attribute_02=>'P86_S_PORC_DTO'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703868842425171493)
,p_name=>'perder_focus_plazo'
,p_event_sequence=>310
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_S_PLAZO_PAGO'
,p_condition_element=>'P86_S_PLAZO_PAGO'
,p_triggering_condition_type=>'NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'focusout'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703869375310171493)
,p_event_id=>wwv_flow_api.id(703868842425171493)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_S_PLAZO_PAGO'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'0'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703869701557171493)
,p_name=>'cambio_valor_plazo'
,p_event_sequence=>320
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_S_PLAZO_PAGO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703870252041171494)
,p_event_id=>wwv_flow_api.id(703869701557171493)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if :P86_S_TIPO_FACTURA <> 1 then',
' BEGIN',
'  FACI053.PP_VALIDA_PLAZO;',
' END;',
'',
'end if;',
''))
,p_attribute_02=>'P86_S_TIPO_FACTURA'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703870659040171494)
,p_name=>'cambio_valor_proforma'
,p_event_sequence=>330
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_PROFORMA'
,p_condition_element=>'P86_PROFORMA'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703871196515171494)
,p_event_id=>wwv_flow_api.id(703870659040171494)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P86_EXPORTACION = ''S'' THEN ',
'BEGIN',
'    /*',
'	SELECT pro_clave,pro_artic_cod,PRO_FREIGHT_PRICE,PRO_FREIGHT_COIN',
'	  INTO :P86_DOC_EXP_PROFORMA,:P86_PROF_ARTIC_FLETE,:P86_PROF_PREC_FLETE,:P86_PROF_FLETE_MON',
'	 FROM FAC_PROFORMA',
'	WHERE PRO_NRO = :P86_PROFORMA',
'	  AND PRO_CLI = :P86_DOC_CLI',
'	  AND PRO_EMPR=:P_EMPRESA;',
'    */',
'    -- P86_DOC_EXP_PROFORMA,P86_PROF_ARTIC_FLETE,P86_PROF_PREC_FLETE,P86_PROF_FLETE_MON',
'    ',
'    /*',
'    SELECT pro_clave,pro_artic_cod,PRO_FREIGHT_PRICE,PRO_FREIGHT_COIN, D.PROD_NET_WEIGTH',
'    INTO :P86_DOC_EXP_PROFORMA,:P86_PROF_ARTIC_FLETE,:P86_PROF_PREC_FLETE,:P86_PROF_FLETE_MON,:P86_W_TOTAL_KG',
'     FROM FAC_PROFORMA,FAC_PROFORMA_DET D',
'    WHERE PRO_NRO = :P86_PROFORMA',
'        AND PRO_CLI = :P86_DOC_CLI',
'        AND PRO_EMPR= :P_EMPRESA',
'       -- ',
'        AND PRO_CLAVE=PROD_CLAVE_DET',
'        AND PRO_EMPR=PROD_EMPR ;',
'   */',
'   --P86_DOC_EXP_PROFORMA,P86_PROF_ARTIC_FLETE,P86_PROF_PREC_FLETE,P86_PROF_FLETE_MON,P86_W_TOTAL_KG',
'   ',
'   SELECT PRO_CLAVE,',
'    PRO_ARTIC_COD,',
'    PRO_FREIGHT_PRICE,',
'    PRO_FREIGHT_COIN,',
'    SUM(D.PROD_NET_WEIGTH)',
'    INTO :P86_DOC_EXP_PROFORMA,',
'    :P86_PROF_ARTIC_FLETE,',
'    :P86_PROF_PREC_FLETE,',
'    :P86_PROF_FLETE_MON,',
'    :P86_W_TOTAL_KG',
'    FROM FAC_PROFORMA, FAC_PROFORMA_DET D',
'    WHERE PRO_NRO = :P86_PROFORMA',
'    AND PRO_CLI = :P86_DOC_CLI',
'    AND PRO_EMPR = :P_EMPRESA',
'    --',
'    AND PRO_CLAVE = PROD_CLAVE_DET',
'    AND PRO_EMPR = PROD_EMPR',
'    GROUP BY PRO_CLAVE, PRO_ARTIC_COD, PRO_FREIGHT_PRICE, PRO_FREIGHT_COIN;',
'    --',
'  ',
'      ',
'      ',
'EXCEPTION',
'	WHEN NO_DATA_FOUND THEN',
'	   RAISE_APPLICATION_ERROR(-20010, ''Proforma inexistente o no pertenece al cliente'');',
'END;	   ',
'',
'END IF;'))
,p_attribute_02=>'P86_DOC_CLI,P86_PROFORMA,P86_EXPORTACION'
,p_attribute_03=>'P86_DOC_EXP_PROFORMA,P86_PROF_ARTIC_FLETE,P86_PROF_PREC_FLETE,P86_PROF_FLETE_MON,P86_W_TOTAL_KG'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703883702975171502)
,p_name=>'cambio_valor_y_validacion_unidad_vta'
,p_event_sequence=>340
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_S_UNIDAD_VTA'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703884219823171503)
,p_event_id=>wwv_flow_api.id(703883702975171502)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P86_S_UNIDAD_VTA =''F''  THEN ',
'',
'  IF :P86_PROFORMA IS NULL THEN',
'    RAISE_APPLICATION_ERROR(-20010,''No se encuentra la proforma para calcular el flete'');',
'  ELSE',
'    BEGIN',
'    ',
'    /*',
'      :P86_S_ART:=NULL;',
'      :P86_D_NRO_REMIS:=NULL;',
'      :P86_DET_CANT:=NULL;',
'      :P86_S_PRECIO:=NULL;',
'     */',
'      ',
'      -- Call the procedure',
'      --FACI053.PP_CARGAR_DATOS_FLETE;                              ',
'      ',
'      SELECT S.ART_CODIGO',
'       INTO :P86_S_ART',
'      FROM STK_ARTICULO S',
'       WHERE S.ART_CODIGO = :P86_PROF_ARTIC_FLETE',
'       AND ART_EMPR = :P_EMPRESA;',
'      ',
'      --:P86_S_UNIDAD_VTA:=''U'';',
'      :P86_DET_CANT:=:P86_W_TOTAL_KG;',
'      :P86_S_PRECIO:=:P86_PROF_PREC_FLETE;',
'      ',
'      ',
'    END;',
'  END IF;',
'  ',
'--END IF;',
'',
'ELSE',
'',
'    IF (NOT :P86_S_UNIDAD_VTA = ''U'' AND NOT :P86_S_UNIDAD_VTA = ''K'') OR :P86_S_UNIDAD_VTA IS NULL  THEN  ',
'      RAISE_APPLICATION_ERROR(-20010,''Debe elegir U=Unidades , K=Kilogramos'');',
'    END IF;',
'',
'END IF;',
''))
,p_attribute_02=>'P86_S_UNIDAD_VTA,P86_PROFORMA,P86_PROF_ARTIC_FLETE'
,p_attribute_03=>'P86_S_ART,P86_D_NRO_REMIS,P86_DET_CANT,P86_S_PRECIO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703884701691171503)
,p_event_id=>wwv_flow_api.id(703883702975171502)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P86_S_UNIDAD_VTA IS NULL THEN',
'  :P86_S_UNIDAD_VTA:=''U'';',
'ELSE',
'  IF NOT :P86_S_UNIDAD_VTA =''U'' AND NOT :P86_S_UNIDAD_VTA =''K'' AND NOT :P86_S_UNIDAD_VTA =''F'' THEN',
'    RAISE_APPLICATION_ERROR(-20010, ''Debe elegir U=Unidades , K=Kilogramos'');',
'  END IF;',
'END IF;  '))
,p_attribute_02=>'P86_S_UNIDAD_VTA'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703885200357171503)
,p_event_id=>wwv_flow_api.id(703883702975171502)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(1150165108893515964)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703885686898171503)
,p_name=>'cambio_y_validacion_articulo'
,p_event_sequence=>350
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_S_ART'
,p_condition_element=>'P86_S_ART'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703886142136171503)
,p_event_id=>wwv_flow_api.id(703885686898171503)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_D_ART_DESC'
,p_attribute_01=>'SQL_STATEMENT'
,p_attribute_03=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select art_desc From stk_articulo ',
'where art_est = ''A''',
' AND ART_EMPR= :P_EMPRESA ',
' AND ART_CODIGO = :P86_S_ART;',
''))
,p_attribute_07=>'P86_S_ART'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703886693534171503)
,p_event_id=>wwv_flow_api.id(703885686898171503)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P86_W_IND_LISTVAL := ''N'';',
'',
'BEGIN',
' FACI053.PP_CONTROL_ARTICULO;',
'END;'))
,p_attribute_02=>'P86_S_NRO_PEDIDO,P86_AREM_MON,P_MON_LOC,P_MON_US,P86_EXPORTACION,P86_DET_COD_IVA,P86_LIPR_NRO_LISTA_PRECIO,P86_DOC_MON,P86_DET_ART,P86_S_RECARGO,P86_S_PRECIO,P86_DOC_FEC_DOC,P86_W_DOC_TASA_US,P86_W_MON_DEC_IMP,P86_S_DEP,P_EMPRESA,P_SUCURSAL,P86_IMPU_'
||'PORC_BASE_IMPO,P86_IMPU_PORCENTAJE'
,p_attribute_03=>'P86_W_IND_LISTVAL,P86_DET_COD_IVA,P86_IMPU_PORCENTAJE,P86_IMPU_PORC_BASE_IMPO,P86_AREM_MON,P86_ARDE_CANT_ACT,P86_ARDE_UBIC,P86_W_SALDO_PED_SUC_IMP,P86_W_CANT_REM,P86_ART_IND_ANIMAL,P86_DET_ART,P86_S_CONC_FLETE,P86_S_CTACO_FLETE'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703887015695171504)
,p_name=>'cambio_validacion_art_ind_animal'
,p_event_sequence=>380
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_S_ART'
,p_condition_element=>'P86_S_ART'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'S'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703887535193171504)
,p_event_id=>wwv_flow_api.id(703887015695171504)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DETA_PESO_UN'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703888015114171504)
,p_event_id=>wwv_flow_api.id(703887015695171504)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DETA_PESO_TT'
);
end;
/
begin
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703888523304171504)
,p_event_id=>wwv_flow_api.id(703887015695171504)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DETA_PESO_UN'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703889077245171504)
,p_event_id=>wwv_flow_api.id(703887015695171504)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DETA_PESO_UN'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703889469709171504)
,p_name=>'cambio_valor_ind_modificacion_precio'
,p_event_sequence=>390
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_S_ART'
,p_condition_element=>'P86_CONF_IND_MODIFICAR_PRECIO'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'S'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703889998633171505)
,p_event_id=>wwv_flow_api.id(703889469709171504)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_S_PRECIO'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703890415752171505)
,p_event_id=>wwv_flow_api.id(703889469709171504)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_S_PRECIO'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703890878317171505)
,p_name=>'cambio_validacion_nro_remision'
,p_event_sequence=>400
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_D_NRO_REMIS'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703891352965171505)
,p_event_id=>wwv_flow_api.id(703890878317171505)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'FACI053.PP_VALIDAR_REMI;',
'END;'))
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703844169045171476)
,p_name=>'cambio_valor_cantidad'
,p_event_sequence=>410
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_DET_CANT'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703844603392171477)
,p_event_id=>wwv_flow_api.id(703844169045171476)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'	',
'  V_MAX_CANT_FACT NUMBER := (:P86_ARDE_CANT_ACT  --Cantidad maxima a facturar',
'                            + NVL(:P86_W_SALDO_PED_SUC_IMP, 0)',
'                            - NVL(:P86_W_CANT_REM, 0));',
'',
unistr('  V_MESSAGE VARCHAR2(200) := ''Cantidad a facturar del art\00EDculo '''),
'          ||:P86_S_ART||'' - ''||:P86_D_ART_DESC||'' supera existencia en el deposito: ''||TO_CHAR(V_MAX_CANT_FACT);',
'',
'BEGIN',
'		 ',
'		 IF NVL(:P86_DET_CANT_PEDIDO,0) > 0 AND :P86_DET_CANT > :P86_DET_CANT_PEDIDO THEN',
'				RAISE_APPLICATION_ERROR(-20010, ''Esta cantidad corresponde a un pedido pendiente, y no puede ser superior a: ''||:P86_DET_CANT_PEDIDO);',
'		 END IF;		',
'			',
'		 IF  :P86_DET_CANT IS NULL THEN',
'		   :P86_DET_CANT := 1;',
'		 END IF;',
'		',
'',
'		 IF :P86_S_UNIDAD_VTA = ''U'' then',
'		   :P86_DET_CANT_GUARDADA := :P86_DET_CANT * NVL(:P86_ART_FACTOR_CONVERSION,1);',
'		 ELSE',
'		   :P86_DET_CANT_GUARDADA := :P86_DET_CANT;',
'		 END IF; 	',
'		 ',
'	   IF  NVL(:P86_DET_NRO_REMIS,0) <> 0 THEN',
'		     BEGIN',
'              FACI053.PP_VALIDAR_REMI_DET;',
'             END;',
'		   ELSE',
'		     IF   :P86_DET_CANT > V_MAX_CANT_FACT THEN',
'		       IF :P86_CONF_FACT_CERO = ''N'' and :P_SUCURSAL = 4 THEN',
'		        RAISE_APPLICATION_ERROR(-20010,V_MESSAGE);',
'		       ELSE',
unistr('		       	 IF NVL(:P86_ART_IND_FACT_NEGATIVO,''S'') = ''N'' and :P_SUCURSAL = 4 THEN --S\00F3lo Loma Plata'),
'		       	 	RAISE_APPLICATION_ERROR(-20010,V_MESSAGE);',
'		         END IF;',
'		       END IF;',
'		     END IF;',
'		   END IF;',
'           ',
'		 ------------------------------------------------------------------------------------------',
'		 BEGIN',
'		  FACI053.PP_CARGAR_CANT_KILOS;',
'		 END;',
'		 ------------------------------------------------------------------------------------------',
'		 	',
'		 IF :P86_DETA_PESO_UN IS NOT NULL THEN ',
'		 		:P86_DETA_PESO_TT	 := ROUND(:P86_DET_CANT * :P86_DETA_PESO_UN,2)	;',
'		 END IF;',
'		 ',
'		 ------------------------------------------------------------------------------------------',
'		 ',
'EXCEPTION',
'  WHEN OTHERS THEN',
'     RAISE_APPLICATION_ERROR(-20010,  SQLCODE);',
'END;  ',
'',
''))
,p_attribute_02=>'P86_ARDE_CANT_ACT,P86_W_SALDO_PED_SUC_IMP,P86_W_CANT_REM,P86_D_ART_DESC,P86_S_ART,P86_DET_CANT_PEDIDO,P86_DET_CANT,P86_ART_FACTOR_CONVERSION,P86_W_IND_LISTVAL,P86_DET_NRO_REMIS,P86_DET_ART,P86_CONF_FACT_CERO,P86_ART_IND_FACT_NEGATIVO,P86_PROF_ARTIC_F'
||'LETE,P86_PROF_PREC_FLETE,P86_S_UNIDAD_VTA,P86_DETA_PESO_UN'
,p_attribute_03=>'P86_DET_CANT_GUARDADA,P86_S_PRECIO,P86_DET_CANT_KILOS,P86_DETA_PESO_TT'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703845129200171478)
,p_event_id=>wwv_flow_api.id(703844169045171476)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P86_PROFORMA IS NOT NULL THEN',
'	:P86_W_IND_LISTVAL := ''N'';',
'ELSE',
'  :P86_DET_CANT := :P86_DET_CANT;',
'  :P86_W_IND_LISTVAL := ''N'';',
'END IF;',
'',
''))
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703846948880171479)
,p_name=>'cambio_validacion_precio'
,p_event_sequence=>420
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_S_PRECIO'
,p_condition_element=>'P86_S_PRECIO'
,p_triggering_condition_type=>'LESS_THAN'
,p_triggering_expression=>'0'
,p_bind_type=>'live'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703847437091171480)
,p_event_id=>wwv_flow_api.id(703846948880171479)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P86_ART_UNID_MED = ''KG''  AND :P86_S_UNIDAD_VTA = ''U'' THEN',
'',
'    BEGIN',
'        FACI053.PP_RECALCULAR_PRECIO;',
'    END;',
'',
'    :P86_AREM_PRECIO_VTA := :P86_S_PRECIO/nvl(:P86_ART_FACTOR_CONVERSION,1);',
'',
'ELSE',
'',
'    BEGIN',
'        FACI053.PP_RECALCULAR_PRECIO;',
'    END;',
'',
'    :P86_AREM_PRECIO_VTA := :P86_S_PRECIO;',
'',
'END IF;	',
'',
'IF :P86_DET_ART = :P86_PROF_ARTIC_FLETE THEN',
'    :P86_S_TOTAL_FLETE_LOC := ((:P86_DET_CANT * :P86_S_PRECIO) * NVL(:P86_W_DOC_TASA_US,1));',
'    :P86_S_TOTAL_FLETE_MON := (:P86_DET_CANT * :P86_S_PRECIO);',
'END IF;'))
,p_attribute_02=>'P86_S_PRECIO,P86_ART_UNID_MED,P86_S_UNIDAD_VTA,P86_IMPU_PORC_BASE_IMPO,P86_IMPU_PORCENTAJE,P86_ART_FACTOR_CONVERSION,P86_DOC_MON,P_EMPRESA'
,p_attribute_03=>'P86_AREM_PRECIO_VTA,P86_S_TOTAL_FLETE_LOC,P86_S_TOTAL_FLETE_MON,P86_S_CONC_FLETE,P86_S_CTACO_FLETE,P86_S_PRECIO,P86_DOC_MON,P_EMPRESA'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703847868310171480)
,p_name=>'cambio_valor_peso_un'
,p_event_sequence=>430
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_DETA_PESO_UN'
,p_bind_type=>'bind'
,p_bind_event_type=>'focusout'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703848392626171481)
,p_event_id=>wwv_flow_api.id(703847868310171480)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P86_ART_IND_ANIMAL =''S'' AND :P86_DETA_PESO_UN <= 0 THEN ',
'	 RAISE_APPLICATION_ERROR(-20010, ''Debe ingresar un valor mayor a CERO en "PESO UNITARIO"!'');	 ',
'END IF; ',
'',
'IF NVL(:P86_G_VAL_INI_N, 0) <> :P86_DETA_PESO_UN THEN 	 ',
'	 :P86_DETA_PESO_TT := ROUND(:P86_DET_CANT * :P86_DETA_PESO_UN, 2); ',
'END IF; ',
'',
':P86_G_VAL_INI_N := :P86_DETA_PESO_UN;',
''))
,p_attribute_02=>'P86_ART_IND_ANIMAL,P86_DETA_PESO_UN,P86_G_VAL_INI_N,P86_DET_CANT'
,p_attribute_03=>'P86_DETA_PESO_TT'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703848745753171481)
,p_name=>'cambio_valor_peso_TT'
,p_event_sequence=>440
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_DETA_PESO_TT'
,p_condition_element=>'P86_DETA_PESO_TT'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703849291503171482)
,p_event_id=>wwv_flow_api.id(703848745753171481)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P86_ART_IND_ANIMAL =''S'' AND NVL(:P86_DETA_PESO_TT,0) <= 0 THEN ',
'	 RAISE_APPLICATION_ERROR(-20010, ''Debe ingresar un valor mayor a CERO en "PESO TOTAL"!'');	 ',
'END IF; ',
'',
' :P86_DETA_PESO_UN := ROUND( :P86_DETA_PESO_TT / :P86_DET_CANT ,3 );',
' :P86_G_VAL_INI_N := :P86_DETA_PESO_TT;',
'	  ',
'	  ',
''))
,p_attribute_02=>'P86_ART_IND_ANIMAL,P86_DETA_PESO_TT,P86_DET_CANT'
,p_attribute_03=>'P86_DETA_PESO_UN,P86_G_VAL_INI_N'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703849690047171482)
,p_name=>'agregar_items'
,p_event_sequence=>450
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(703725864993171355)
,p_condition_element=>'P86_IND_EDIT'
,p_triggering_condition_type=>'NOT_EQUALS'
,p_triggering_expression=>'S'
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703851176543171483)
,p_event_id=>wwv_flow_api.id(703849690047171482)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- Call the procedure',
'  FACI053.PP_COLLECTION_UPDATE;',
'END;',
''))
,p_attribute_02=>'P86_S_UNIDAD_VTA,P86_S_ART,P86_D_NRO_REMIS,P86_S_PRECIO,P86_S_IMPORTE,P86_DETA_PESO_UN,P86_DETA_PESO_TT,P86_IND_ITEM_EDIT,P86_DET_CANT,P86_KG_GANCHO,P86_RENDIMIENTO'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703852181441171483)
,p_event_id=>wwv_flow_api.id(703849690047171482)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- Call the procedure',
'  FACI053.PP_COLLECTION_ADICIONAR_ITEM;',
'END;',
'',
''))
,p_attribute_02=>'P86_S_UNIDAD_VTA,P86_S_ART,P86_D_NRO_REMIS,P86_S_PRECIO,P86_S_IMPORTE,P86_DETA_PESO_UN,P86_DETA_PESO_TT,P86_DET_CANT,P86_KG_GANCHO,P86_RENDIMIENTO,P86_EXPORTACION,P86_DOC_MON'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703850172304171482)
,p_event_id=>wwv_flow_api.id(703849690047171482)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'SECUENCIAS NUMBER;',
'BEGIN',
'',
'SELECT NVL(MAX(SEQ_ID),1) INTO SECUENCIAS FROM APEX_COLLECTIONS WHERE COLLECTION_NAME = ''DETALLES_FACI052'';',
'',
'',
'FACI053.PP_RECALCULAR_DETALLES(SECUENCIA => SECUENCIAS);',
'',
'',
'END;'))
,p_attribute_02=>'P86_W_MON_DEC_IMP,P86_AREM_PRECIO_VTA,P86_S_PORC_DTO,P86_IMPU_PORCENTAJE,P86_W_CLI_PORC_EXEN_IVA,P86_DOC_MON,P_EMPRESA,P_MON_LOC,P86_DET_BRUTO_MON,P86_W_DOC_TASA_US,P86_DET_NETO_MON,P86_DET_IVA_MON,P86_S_TIPO_FACTURA,P86_W_BRUTO_EXEN_LOC,P86_W_BRUTO_'
||'GRAV_LOC,P86_W_NETO_EXEN_MON,P86_W_NETO_GRAV_MON,P86_W_NETO_GRAV_LOC,P86_S_UNIDAD_VTA,P86_S_ART,P86_DET_CANT,P86_S_PRECIO,P86_IMPU_PORC_BASE_IMPO'
,p_attribute_03=>'P86_DET_BRUTO_LOC,P86_DET_BRUTO_MON,P86_DET_NETO_LOC,P86_DET_NETO_MON,P86_DET_IVA_LOC,P86_DET_IVA_MON,P86_W_BRUTO_EXEN_PRECIO_MON,P86_W_BRUTO_EXEN_MON,P86_W_BRUTO_GRAV_MON,P86_W_BRUTO_EXEN_LOC,P86_W_BRUTO_GRAV_LOC,P86_W_NETO_EXEN_LOC,P86_W_NETO_EXEN_'
||'MON,P86_W_NETO_GRAV_LOC,P86_W_NETO_GRAV_MON,P86_W_IVA_MON,P86_W_IVA_LOC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703851628339171483)
,p_event_id=>wwv_flow_api.id(703849690047171482)
,p_event_result=>'FALSE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(979564349744323147)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703852617448171483)
,p_event_id=>wwv_flow_api.id(703849690047171482)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'SECUENCIAS NUMBER;',
'BEGIN',
'',
'SELECT NVL(MAX(SEQ_ID),1) INTO SECUENCIAS FROM APEX_COLLECTIONS WHERE COLLECTION_NAME = ''DETALLES_FACI052'';',
'',
'',
'FACI053.PP_RECALCULAR_DETALLES(SECUENCIA => SECUENCIAS);',
'',
'END;'))
,p_attribute_02=>'P86_W_MON_DEC_IMP,P86_AREM_PRECIO_VTA,P86_S_PORC_DTO,P86_IMPU_PORCENTAJE,P86_W_CLI_PORC_EXEN_IVA,P86_DOC_MON,P_EMPRESA,P_MON_LOC,P86_DET_BRUTO_MON,P86_W_DOC_TASA_US,P86_DET_NETO_MON,P86_DET_IVA_MON,P86_S_TIPO_FACTURA,P86_W_BRUTO_EXEN_LOC,P86_W_BRUTO_'
||'GRAV_LOC,P86_W_NETO_EXEN_MON,P86_W_NETO_GRAV_MON,P86_W_NETO_GRAV_LOC,P86_S_UNIDAD_VTA,P86_S_ART,P86_DET_CANT,P86_S_PRECIO,P86_IMPU_PORC_BASE_IMPO'
,p_attribute_03=>'P86_DET_BRUTO_LOC,P86_DET_BRUTO_MON,P86_DET_NETO_LOC,P86_DET_NETO_MON,P86_DET_IVA_LOC,P86_DET_IVA_MON,P86_W_BRUTO_EXEN_PRECIO_MON,P86_W_BRUTO_EXEN_MON,P86_W_BRUTO_GRAV_MON,P86_W_BRUTO_EXEN_LOC,P86_W_BRUTO_GRAV_LOC,P86_W_NETO_EXEN_LOC,P86_W_NETO_EXEN_'
||'MON,P86_W_NETO_GRAV_LOC,P86_W_NETO_GRAV_MON,P86_W_IVA_MON,P86_W_IVA_LOC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703853158883171483)
,p_event_id=>wwv_flow_api.id(703849690047171482)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(979564349744323147)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703850641047171482)
,p_event_id=>wwv_flow_api.id(703849690047171482)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(1167429410501956872)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703853692532171484)
,p_event_id=>wwv_flow_api.id(703849690047171482)
,p_event_result=>'TRUE'
,p_action_sequence=>70
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'l_total       NUMBER;',
'l_total_iva   NUMBER;',
'l_total_bruto NUMBER;',
'l_moneda 	  number;',
'l_porc_desc	  number;',
'l_cantidad 	  number;',
'BEGIN',
'	l_moneda := :P86_DOC_MON;',
'',
'	SELECT SUM(total) + SUM(V_DOC_IVA_MON) total',
'	     , SUM(V_DOC_IVA_MON)			   total_iva',
'	     , SUM(TOTAL_BRUTO)				   total_bruto',
'	  INTO l_total,',
'	  	   l_total_iva,',
'	  	   l_total_bruto',
'	FROM(SELECT    ',
'	       SUM( to_number(C005) * case when l_moneda = 1 then trunc(to_number(C004)) else to_number(c004) end )    total, -- C004: Cantidad C005 Total',
'	       case when l_moneda = 1 then trunc(TO_NUMBER(C013)) else TO_NUMBER(C013) end   V_DOC_IVA_MON,',
'	       C002              ARTICULO,',
'	       SUM( case when l_moneda = 1 then trunc(to_number(C025)) else to_number(c025) end ) TOTAL_BRUTO',
'	    FROM APEX_COLLECTIONS ',
'	    WHERE COLLECTION_NAME = ''DETALLES_FACI052''',
'	    GROUP BY C002, C013',
'	    );  ',
'                       ',
'	:P86_F_TOTAL := TO_NUMBER(l_total, ''9999999999999D9999'', ''NLS_NUMERIC_CHARACTERS='''',.'''''');',
'	:P86_F_TOT_IVA := TO_NUMBER(l_total_iva, ''9999999999999D9999'', ''NLS_NUMERIC_CHARACTERS='''',.'''''');',
'',
'	l_porc_desc := :P86_S_PORC_DTO;',
'	l_cantidad  := :P86_DET_CANT;',
'',
'	IF l_porc_desc IS NOT NULL THEN ',
'	    :P86_F_TOT_DTO := ( ((l_total_bruto * l_porc_desc) / 100) *  l_cantidad);',
'	ELSE',
'	    :P86_F_TOT_DTO := 0;',
'	END IF;',
'END;'))
,p_attribute_02=>'P86_DOC_MON'
,p_attribute_03=>'P86_F_TOTAL,P86_F_TOT_IVA,P86_F_TOT_DTO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703854162480171484)
,p_event_id=>wwv_flow_api.id(703849690047171482)
,p_event_result=>'TRUE'
,p_action_sequence=>80
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P86_S_UNIDAD_VTA:=''U'';',
':P86_S_ART:=NULL;',
':P86_D_NRO_REMIS:=NULL;',
':P86_DET_CANT:=NULL;',
':P86_S_PRECIO:=NULL;',
':P86_D_ART_DESC:=NULL;'))
,p_attribute_03=>'P86_S_UNIDAD_VTA,P86_S_ART,P86_D_NRO_REMIS,P86_DET_CANT,P86_S_PRECIO,P86_D_ART_DESC'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703857703302171485)
,p_name=>'borrar_detalles'
,p_event_sequence=>470
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_DELETE_ID'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703858228306171485)
,p_event_id=>wwv_flow_api.id(703857703302171485)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  FACI053.PP_COLLECTION_BORRAR_DETALLE(I_SEQ => :P86_DELETE_ID);',
'END;',
''))
,p_attribute_02=>'P86_DELETE_ID'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703858781277171486)
,p_event_id=>wwv_flow_api.id(703857703302171485)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(979564349744323147)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703859280846171486)
,p_event_id=>wwv_flow_api.id(703857703302171485)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  TOTAL       NUMBER;',
'  IVA_MON     NUMBER;',
'  TOTAL_BRUTO NUMBER;',
'BEGIN',
'',
'  SELECT SUM(CANTIDAD) + SUM(V_DOC_IVA_MON),',
'         SUM(V_DOC_IVA_MON),',
'         SUM(TOTAL_BRUTO)',
'    INTO TOTAL, IVA_MON, TOTAL_BRUTO',
'    FROM (SELECT SUM(C005*C004) CANTIDAD,--SUM(C005) * SUM(C004)',
'                 TO_NUMBER(C013) V_DOC_IVA_MON,',
'                 C002 ARTICULO,',
'                 SUM(C025) TOTAL_BRUTO',
'            FROM APEX_COLLECTIONS',
'           WHERE COLLECTION_NAME = ''DETALLES_FACI052''',
'           GROUP BY C002, C013);',
'',
'  IF :P86_DOC_MON = 1 THEN',
'    :P86_F_TOTAL   := TO_NUMBER(TOTAL,',
'                                  ''9999999999999D9999'',',
'                                  ''NLS_NUMERIC_CHARACTERS='''',.'''''');',
'    :P86_F_TOT_IVA := TO_NUMBER(IVA_MON,',
'                                  ''9999999999999D9999'',',
'                                  ''NLS_NUMERIC_CHARACTERS='''',.'''''');',
'  ELSIF :P86_DOC_MON = 2 THEN',
'    :P86_F_TOTAL   := TO_NUMBER(TOTAL,',
'                                  ''9999999999999D9999'',',
'                                  ''NLS_NUMERIC_CHARACTERS='''',.'''''');',
'    :P86_F_TOT_IVA := TO_NUMBER(IVA_MON,',
'                                  ''9999999999999D9999'',',
'                                  ''NLS_NUMERIC_CHARACTERS='''',.'''''');',
'  ELSIF :P86_DOC_MON = 3 THEN',
'    :P86_F_TOTAL   := TO_NUMBER(TOTAL,',
'                                  ''9999999999999D9999'',',
'                                  ''NLS_NUMERIC_CHARACTERS='''',.'''''');',
'    :P86_F_TOT_IVA := TO_NUMBER(IVA_MON,',
'                                  ''9999999999999D9999'',',
'                                  ''NLS_NUMERIC_CHARACTERS='''',.'''''');',
'  ',
'  END IF;',
'  ',
'  --:P86_F_TOTAL := TOTAL;',
'  --:P86_F_TOT_IVA := IVA_MON;',
'',
'  IF :P86_S_PORC_DTO IS NOT NULL THEN',
'  ',
'    :P86_F_TOT_DTO := ((TOTAL_BRUTO * :P86_S_PORC_DTO) / 100) *',
'                        :P86_DET_CANT;',
'  ',
'  ELSE',
'  ',
'    :P86_F_TOT_DTO := 0;',
'  ',
'  END IF;',
'',
'END;',
''))
,p_attribute_02=>'P86_DOC_MON'
,p_attribute_03=>'P86_F_TOTAL,P86_F_TOT_IVA,P86_F_TOT_DTO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703921665958171524)
,p_name=>'cargar_datos_detalles'
,p_event_sequence=>490
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_IND_ITEM_EDIT'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703922139527171525)
,p_event_id=>wwv_flow_api.id(703921665958171524)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_S_UNIDAD_VTA,P86_S_ART,P86_D_NRO_REMIS,P86_S_PRECIO,P86_DETA_PESO_UN,P86_DETA_PESO_TT,P86_KG_GANCHO,P86_DET_RENDIMIENTO'
,p_attribute_01=>'SQL_STATEMENT'
,p_attribute_03=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT C001 UNIDAD_VTA,',
'       C002 ARTICULO,',
'       TO_NUMBER(C003) NRO_REMISION,',
'       TO_NUMBER(C004,''9999999999999D99'',''NLS_NUMERIC_CHARACTERS='''',.'''''') CANTIDAD,',
'       TO_NUMBER(C005,''9999999999999D99'',''NLS_NUMERIC_CHARACTERS='''',.'''''') PRECIO,',
'       TO_NUMBER(C007,''9999999999999D99'',''NLS_NUMERIC_CHARACTERS='''',.'''''') PESO_UNIDAD,',
'       TO_NUMBER(C008,''9999999999999D99'',''NLS_NUMERIC_CHARACTERS='''',.'''''') PESO_TONELADA,',
'       TO_NUMBER(C030,''9999999999999D99'',''NLS_NUMERIC_CHARACTERS='''',.'''''') KG_GANCHO,',
'       TO_NUMBER(C031,''9999999999999D99'',''NLS_NUMERIC_CHARACTERS='''',.'''''') RENDIMIENTO',
'  FROM APEX_COLLECTIONS A',
' WHERE A.COLLECTION_NAME = ''DETALLES_FACI052''',
'   AND SEQ_ID = :P86_IND_ITEM_EDIT'))
,p_attribute_07=>'P86_IND_ITEM_EDIT'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703922549717171525)
,p_name=>'borrar_detalles_btn'
,p_event_sequence=>500
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(703725483823171354)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703923081499171525)
,p_event_id=>wwv_flow_api.id(703922549717171525)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_CLEAR'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_S_ART,P86_D_ART_DESC,P86_D_NRO_REMIS,P86_S_PRECIO,P86_S_IMPORTE,P86_DETA_PESO_UN,P86_DETA_PESO_TT,P86_IND_EDIT,P86_IND_ITEM_EDIT'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703899379429171512)
,p_name=>'asignar_valores_canales'
,p_event_sequence=>530
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_DOC_CANAL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
,p_display_when_type=>'FUNCTION_BODY'
,p_display_when_cond=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--VALIDACION DEL TEMA DE CANALES',
'DECLARE',
'CANAL VARCHAR2(1);',
'BEGIN',
'',
' SELECT CLI_IND_MOD_CANAL',
' INTO CANAL',
'  FROM FIN_CLIENTE C',
' WHERE C.CLI_EST_CLI <> ''I''',
'   AND CLI_EMPR = :P_EMPRESA',
'   AND C.CLI_CODIGO =  :P86_DOC_CLI;',
'',
'',
'',
'IF CANAL = ''N'' THEN ',
' IF NVL(:P86_V_INI_NUM,0) <> NVL(:P86_DOC_CANAL,0) THEN ',
'	return true; ',
' END IF;',
'END IF;',
'',
'',
'EXCEPTION ',
' WHEN NO_DATA_FOUND THEN ',
'  return false;',
'END;',
'',
''))
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703899882126171512)
,p_event_id=>wwv_flow_api.id(703899379429171512)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':P86_DOC_CANAL := :P86_V_INI_NUM;'
,p_attribute_02=>'P86_V_INI_NUM'
,p_attribute_03=>'P86_DOC_CANAL'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703891717333171505)
,p_name=>'cambio_valor_deposito_'
,p_event_sequence=>540
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_S_DEP'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703892288585171506)
,p_event_id=>wwv_flow_api.id(703891717333171505)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_S_DEP_DESC'
,p_attribute_01=>'SQL_STATEMENT'
,p_attribute_03=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT D.DEP_DESC A',
'  FROM STK_DEPOSITO D',
' WHERE D.DEP_EMPR = :P_EMPRESA',
'   AND D.DEP_SUC = :P_SUCURSAL',
'   AND D.DEP_CODIGO = :P86_S_DEP'))
,p_attribute_07=>'P86_S_DEP'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703904232759171514)
,p_name=>'ocultar_regiones_incial'
,p_event_sequence=>570
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703904731133171514)
,p_event_id=>wwv_flow_api.id(703904232759171514)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(1151960737685758479)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703906715823171515)
,p_event_id=>wwv_flow_api.id(703904232759171514)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(1112273233172739252)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703906269869171515)
,p_event_id=>wwv_flow_api.id(703904232759171514)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(1112275826089739278)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703905734167171514)
,p_event_id=>wwv_flow_api.id(703904232759171514)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(1137419162195941179)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703905205788171514)
,p_event_id=>wwv_flow_api.id(703904232759171514)
,p_event_result=>'TRUE'
,p_action_sequence=>70
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(1139574573093019647)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703908760703171515)
,p_event_id=>wwv_flow_api.id(703904232759171514)
,p_event_result=>'TRUE'
,p_action_sequence=>80
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(1133615516149962144)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703908204549171515)
,p_event_id=>wwv_flow_api.id(703904232759171514)
,p_event_result=>'TRUE'
,p_action_sequence=>90
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(1162900349574258157)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703907702242171515)
,p_event_id=>wwv_flow_api.id(703904232759171514)
,p_event_result=>'TRUE'
,p_action_sequence=>100
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(1131333483411954182)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703907217738171515)
,p_event_id=>wwv_flow_api.id(703904232759171514)
,p_event_result=>'TRUE'
,p_action_sequence=>110
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(1139577059517019672)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703909769317171516)
,p_event_id=>wwv_flow_api.id(703904232759171514)
,p_event_result=>'TRUE'
,p_action_sequence=>120
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(1162901063624258164)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703909210322171516)
,p_event_id=>wwv_flow_api.id(703904232759171514)
,p_event_result=>'TRUE'
,p_action_sequence=>130
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(1165033756759214582)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703910271842171516)
,p_event_id=>wwv_flow_api.id(703904232759171514)
,p_event_result=>'TRUE'
,p_action_sequence=>140
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(1167429410501956872)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703897916187171511)
,p_name=>'cambio_valores_y_validacion_ind_ganaderia'
,p_event_sequence=>580
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_EMPR_IND_GANADERA'
,p_condition_element=>'P86_EMPR_IND_GANADERA'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'S'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703898481922171511)
,p_event_id=>wwv_flow_api.id(703897916187171511)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(1151956920034758441)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703898970948171511)
,p_event_id=>wwv_flow_api.id(703897916187171511)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(1151956920034758441)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703902531182171513)
,p_name=>'cambio_valor_validacion_clientes_autorizacion'
,p_event_sequence=>590
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_IND_ALERT_AUT_ESP'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703903097953171513)
,p_event_id=>wwv_flow_api.id(703902531182171513)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- Call the procedure',
'  --FACI053.PP_VALIDAR_VECIMIENTO_2(VALOR => :P86_IND_ALERT_AUT_ESP);',
'  NULL;',
'END;',
''))
,p_attribute_02=>'P86_IND_ALERT_AUT_ESP'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703855406703171484)
,p_name=>'cambio_valor_tm'
,p_event_sequence=>620
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_DOC_TIPO_MOV'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
end;
/
begin
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703855979236171485)
,p_event_id=>wwv_flow_api.id(703855406703171484)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'SELECT TMOV_TIPO',
'   INTO :P86_DOC_TIPO_SALDO',
'   FROM GEN_TIPO_MOV',
'  WHERE TMOV_CODIGO = :P86_DOC_TIPO_MOV',
'    AND TMOV_EMPR = :P_EMPRESA;',
'    ',
'END;'))
,p_attribute_02=>'P86_DOC_TIPO_MOV'
,p_attribute_03=>'P86_DOC_TIPO_SALDO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703845576404171478)
,p_name=>'valores_timbrado_nulo'
,p_event_sequence=>630
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_DOC_TIMBRADO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
,p_display_when_type=>'NEVER'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703846095008171479)
,p_event_id=>wwv_flow_api.id(703845576404171478)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
' -- validar timbrado',
' PL_VAL_DOC_TIMBRADO(I_NRO_DOC   => :P86_DOC_NRO_DOC,',
'                      I_CLASE_DOC => 1,',
'                      I_FECHA     => :P86_DOC_FEC_DOC,',
'                      O_TIMBRADO  => :P86_DOC_TIMBRADO,',
'                      I_EMPRESA   => :P_EMPRESA);',
'END;',
'',
''))
,p_attribute_02=>'P86_DOC_NRO_DOC,P86_DOC_FEC_DOC'
,p_attribute_03=>'P86_DOC_TIMBRADO'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703846505003171479)
,p_event_id=>wwv_flow_api.id(703845576404171478)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DOC_TIMBRADO'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'0'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703854528198171484)
,p_name=>'validar_timbrado_existencia'
,p_event_sequence=>640
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_DOC_TIMBRADO'
,p_condition_element=>'P86_DOC_TIMBRADO'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703855090658171484)
,p_event_id=>wwv_flow_api.id(703854528198171484)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE ',
'',
'V_EXISTE NUMBER;',
'',
'BEGIN',
'',
'SELECT COUNT(*)',
'      INTO V_EXISTE',
'      FROM FIN_DOCUMENTO',
'     WHERE DOC_NRO_DOC = :P86_DOC_NRO_DOC',
'       AND DOC_TIMBRADO = :P86_DOC_TIMBRADO',
'       AND DOC_TIPO_MOV IN (9, 10)',
'       AND DOC_EMPR = :P_EMPRESA;',
'  ',
'    IF V_EXISTE > 1 THEN',
'      RAISE_APPLICATION_ERROR(-20010,  ''Numero de factura y timbrado existente'');',
'    END IF;',
'    ',
'END;'))
,p_attribute_02=>'P86_DOC_NRO_DOC,P86_DOC_TIMBRADO'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703892686651171506)
,p_name=>'validar_canal'
,p_event_sequence=>650
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_EXPORTACION'
,p_condition_element=>'P86_EXPORTACION'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'S'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703893161501171506)
,p_event_id=>wwv_flow_api.id(703892686651171506)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DOC_CANAL'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703893645932171506)
,p_event_id=>wwv_flow_api.id(703892686651171506)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DOC_CANAL'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'9'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703914585703171521)
,p_name=>'recibir_valor'
,p_event_sequence=>660
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_RETORNO_VALIDAR_MON'
,p_condition_element=>'P86_RETORNO_VALIDAR_MON'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'1'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703915095715171522)
,p_event_id=>wwv_flow_api.id(703914585703171521)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_CONFIRM'
,p_attribute_01=>'El cliente tiene un vencimiento con dias de atraso!. Desea continuar?'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703916022792171522)
,p_event_id=>wwv_flow_api.id(703914585703171521)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825489757171451)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703916549673171522)
,p_event_id=>wwv_flow_api.id(703914585703171521)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703725864993171355)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703915554558171522)
,p_event_id=>wwv_flow_api.id(703914585703171521)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703725483823171354)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703917068416171522)
,p_event_id=>wwv_flow_api.id(703914585703171521)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825036069171450)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703917522859171523)
,p_event_id=>wwv_flow_api.id(703914585703171521)
,p_event_result=>'TRUE'
,p_action_sequence=>70
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825890241171451)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703918013387171523)
,p_event_id=>wwv_flow_api.id(703914585703171521)
,p_event_result=>'TRUE'
,p_action_sequence=>80
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703826298838171451)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703894094033171506)
,p_name=>'recibir_valor_tipo2'
,p_event_sequence=>670
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_RETORNO_VALIDAR_MON'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
,p_display_when_type=>'FUNCTION_BODY'
,p_display_when_cond=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P86_RETORNO_VALIDAR_MON < :P86_W_CLI_MAX_DIAS_ATRASO THEN ',
' RETURN FALSE;',
'ELSIF :P86_RETORNO_VALIDAR_MON > :P86_W_CLI_MAX_DIAS_ATRASO THEN',
' RETURN TRUE;',
'ELSIF :P86_RETORNO_VALIDAR_MON = 1000 THEN ',
' RETURN FALSE;',
'END IF;',
'',
'--RETURN TRUE;'))
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703897558673171510)
,p_event_id=>wwv_flow_api.id(703894094033171506)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'Swal.fire(''El cliente tiene un vencimiento con ''+apex.item( "P86_RETORNO_VALIDAR_MON" ).getValue()+'' dias de atraso!'');',
''))
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703894596934171507)
,p_event_id=>wwv_flow_api.id(703894094033171506)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825489757171451)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703895594628171508)
,p_event_id=>wwv_flow_api.id(703894094033171506)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703725864993171355)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703895088654171508)
,p_event_id=>wwv_flow_api.id(703894094033171506)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703725483823171354)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703896510153171510)
,p_event_id=>wwv_flow_api.id(703894094033171506)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825036069171450)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703896077067171509)
,p_event_id=>wwv_flow_api.id(703894094033171506)
,p_event_result=>'TRUE'
,p_action_sequence=>70
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825890241171451)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703897024329171510)
,p_event_id=>wwv_flow_api.id(703894094033171506)
,p_event_result=>'TRUE'
,p_action_sequence=>80
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703826298838171451)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703918422042171523)
,p_name=>'validar_carga_vendedor'
,p_event_sequence=>690
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_DOC_LEGAJO'
,p_condition_element=>'P86_DOC_LEGAJO'
,p_triggering_condition_type=>'NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'focusout'
,p_display_when_type=>'PLSQL_EXPRESSION'
,p_display_when_cond=>':P_EMPRESA between 1 and 14'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703918967629171523)
,p_event_id=>wwv_flow_api.id(703918422042171523)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'PLUGIN_BE.CTB.ALERTIFY'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'ALERT'
,p_attribute_04=>'El Vendedor no puede estar vacio!'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703919448570171523)
,p_event_id=>wwv_flow_api.id(703918422042171523)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_FOCUS'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P86_DOC_LEGAJO'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703903450331171513)
,p_name=>'MENSAJE_MOROSO'
,p_event_sequence=>700
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_VAL_MOROSO'
,p_condition_element=>'P86_VAL_MOROSO'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'1'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703903981912171514)
,p_event_id=>wwv_flow_api.id(703903450331171513)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'PLUGIN_BE.CTB.ALERTIFY'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'ALERT'
,p_attribute_04=>'El cliente es moroso.'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703840230165171475)
,p_name=>'AL_CAMBIAR_RV2'
,p_event_sequence=>710
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_RETORNO_VALIDAR2_MON'
,p_condition_element=>'P86_RETORNO_VALIDAR2_MON'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'1'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703840773991171475)
,p_event_id=>wwv_flow_api.id(703840230165171475)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'Swal.fire(''El cliente tiene un vencimiento con ''+apex.item( "P86_RETORNO_VALIDAR_MON" ).getValue()+'' dias de atraso!'');'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703841216815171475)
,p_event_id=>wwv_flow_api.id(703840230165171475)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825489757171451)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703841702934171475)
,p_event_id=>wwv_flow_api.id(703840230165171475)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703725864993171355)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703842246506171476)
,p_event_id=>wwv_flow_api.id(703840230165171475)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703725483823171354)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703842758205171476)
,p_event_id=>wwv_flow_api.id(703840230165171475)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825036069171450)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703843249247171476)
,p_event_id=>wwv_flow_api.id(703840230165171475)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825890241171451)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703843741159171476)
,p_event_id=>wwv_flow_api.id(703840230165171475)
,p_event_result=>'TRUE'
,p_action_sequence=>70
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703826298838171451)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703910623368171516)
,p_name=>'AL_CAMBIAR_RV1'
,p_event_sequence=>720
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_RETORNO_VALIDAR1_MON'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703911123040171516)
,p_event_id=>wwv_flow_api.id(703910623368171516)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_CONFIRM'
,p_attribute_01=>'El cliente tiene un vencimiento con dias de atraso!. Desea continuar?'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703911647458171518)
,p_event_id=>wwv_flow_api.id(703910623368171516)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825489757171451)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703912132549171518)
,p_event_id=>wwv_flow_api.id(703910623368171516)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703725864993171355)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703912691828171518)
,p_event_id=>wwv_flow_api.id(703910623368171516)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703725483823171354)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703913104759171519)
,p_event_id=>wwv_flow_api.id(703910623368171516)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825036069171450)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703913687867171521)
,p_event_id=>wwv_flow_api.id(703910623368171516)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825890241171451)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703914173476171521)
,p_event_id=>wwv_flow_api.id(703910623368171516)
,p_event_result=>'TRUE'
,p_action_sequence=>70
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703826298838171451)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703932081543171532)
,p_name=>'AL_CAMBIAR_RM1'
,p_event_sequence=>730
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_RETORNO_MONEDA'
,p_condition_element=>'P86_RETORNO_MONEDA'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'3'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703932549011171532)
,p_event_id=>wwv_flow_api.id(703932081543171532)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'Swal.fire(''La moneda de la lista de precio ''+apex.item( "P86_LIPR_NRO_LISTA_PRECIO" ).getValue()+'' es distinta a la moneda de la factura!.''',
'          +''El cliente tiene un vencimiento con ''+apex.item( "P86_RETORNO_VALIDAR_MON" ).getValue()+'' dias de atraso!'');',
'  '))
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703933037456171532)
,p_event_id=>wwv_flow_api.id(703932081543171532)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825489757171451)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703933578931171533)
,p_event_id=>wwv_flow_api.id(703932081543171532)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703725864993171355)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703934081326171533)
,p_event_id=>wwv_flow_api.id(703932081543171532)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703725483823171354)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703934532867171533)
,p_event_id=>wwv_flow_api.id(703932081543171532)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825036069171450)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703935068295171533)
,p_event_id=>wwv_flow_api.id(703932081543171532)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825890241171451)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703935568902171533)
,p_event_id=>wwv_flow_api.id(703932081543171532)
,p_event_result=>'TRUE'
,p_action_sequence=>70
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703826298838171451)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703936854902171534)
,p_name=>'AL_CAMBIAR_RM3'
,p_event_sequence=>740
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_RETORNO_MONEDA'
,p_condition_element=>'P86_RETORNO_MONEDA'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'1'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703937393985171534)
,p_event_id=>wwv_flow_api.id(703936854902171534)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'Swal.fire(''El cliente tiene un vencimiento con ''+apex.item( "P86_RETORNO_VALIDAR_MON" ).getValue()+'' dias de atraso!'');',
''))
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703937812295171534)
,p_event_id=>wwv_flow_api.id(703936854902171534)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825489757171451)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703938319380171534)
,p_event_id=>wwv_flow_api.id(703936854902171534)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703725864993171355)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703938880726171535)
,p_event_id=>wwv_flow_api.id(703936854902171534)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703725483823171354)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703939374314171535)
,p_event_id=>wwv_flow_api.id(703936854902171534)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825036069171450)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703939879445171535)
,p_event_id=>wwv_flow_api.id(703936854902171534)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703825890241171451)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703940337708171535)
,p_event_id=>wwv_flow_api.id(703936854902171534)
,p_event_result=>'TRUE'
,p_action_sequence=>70
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703826298838171451)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703935974311171534)
,p_name=>'AL_CAMBIAR_RM2'
,p_event_sequence=>750
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_RETORNO_MONEDA'
,p_condition_element=>'P86_RETORNO_MONEDA'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'2'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703936422065171534)
,p_event_id=>wwv_flow_api.id(703935974311171534)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'JAVASCRIPT_EXPRESSION'
,p_affected_elements=>'Swal.fire(''La moneda de la lista de precio ''+apex.item( "P86_LIPR_NRO_LISTA_PRECIO" ).getValue()+'' es distinta a la moneda de la factura!.'');  '
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703900257590171512)
,p_name=>'Nuevo'
,p_event_sequence=>760
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_IND_PROFORMA'
,p_condition_element=>'P86_IND_PROFORMA'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703900777019171512)
,p_event_id=>wwv_flow_api.id(703900257590171512)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(703725864993171355)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(703839354288171474)
,p_name=>'cambiar_valor_det_rendimiento'
,p_event_sequence=>770
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P86_KG_GANCHO'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(703839831400171475)
,p_event_id=>wwv_flow_api.id(703839354288171474)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P86_ART_IND_ANIMAL =''S'' AND :P86_KG_GANCHO <= 0 THEN ',
'	 RAISE_APPLICATION_ERROR(-20010, ''Debe ingresar un valor mayor a CERO en "PESO AL GANCHO"!'');	 ',
'END IF; ',
'',
':P86_DET_RENDIMIENTO :=  ROUND(:P86_KG_GANCHO / :P86_DETA_PESO_TT * 100,3); ',
'',
':P86_G_VAL_INI_N := :P86_KG_GANCHO;'))
,p_attribute_02=>'P86_ART_IND_ANIMAL,P86_KG_GANCHO,P86_DETA_PESO_TT'
,p_attribute_03=>'P86_DET_RENDIMIENTO,P86_G_VAL_INI_N'
,p_attribute_04=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(703838103397171472)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Guardar_Datos'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'FIN         FIN_DOCUMENTO%ROWTYPE;',
'OBS         VARCHAR2(5000);',
'EXPORTACION VARCHAR2(50);',
'V_URL       varchar2(30000);',
'BEGIN',
'',
'BEGIN',
'',
'SELECT men_mensaje ',
'  INTO OBS',
'  from gen_mensaje ',
' WHERE MEN_EMPR= :P_EMPRESA',
'   AND men_codigo = :P86_MEN_MENSAJE_DESC;',
'',
'EXCEPTION',
' WHEN NO_DATA_FOUND THEN ',
'  OBS := '''';',
'END;',
'',
'',
' IF :P86_EXPORTACION = ''S'' THEN',
'   	EXPORTACION := '' '';',
'    :P86_DOC_CLAVE_STK := null;',
' ELSE ',
'    EXPORTACION := ''S'';',
'    :P86_DOC_CLAVE_STK:=  STK_SEQ_DOCU_NEXTVAL;',
' END IF;',
'',
' :P86_DOC_CLAVE := FIN_SEQ_DOC_NEXTVAL;',
'',
' IF :P86_S_TIPO_FACTURA IN (1,3) THEN ',
' SELECT :P86_DOC_CLAVE,',
'       :P_EMPRESA,',
'       :P86_DOC_CTA_BCO,',
'       :P_SUCURSAL,',
'       :P86_CONF_COD_FAC_CONT,',
'       :P86_DOC_FEC_DOC,',
'       :P86_DOC_CLI,',
'       :P86_DOC_CLI_NOM,',
'       :P86_DOC_NRO_DOC,',
'       :P86_DOC_MON,',
'       :P86_DOC_CLI_DIR,',
'       :P86_DOC_CLI_RUC,',
'       OBS,',
'       0, 0, 0, 0,',
'       :P86_DOC_FEC_DOC,',
'       :APP_USER,',
'       SYSDATE,',
'       ''FAC'',',
'       EXPORTACION,',
'       DECODE(:P86_S_TIPO_FACTURA,1,:P86_CONF_CTA_CONT_DEU_FCON, NULL),',
'       :P86_DOC_TIPO_SALDO,',
'       :P86_DOC_LEGAJO,',
'       :P86_DOC_CLI_TEL,',
'       :P86_DOC_SERIE,',
'       :P86_S_TIPO_FACTURA,',
'       :P86_EXPORTACION,',
'       :P86_DOC_CLAVE_STK,',
'       ''S'',',
'       :P86_DOC_CANAL,',
'       :P86_DOC_CTA_BCO,',
'       :P86_DOC_IND_IMPR_CONYUGUE,',
'       :P86_DOC_IND_IMPR_VENDEDOR,',
'       :P86_W_BRUTO_EXEN_MON,',
'       :P86_W_BRUTO_EXEN_LOC,',
'       :P86_W_BRUTO_GRAV_LOC,',
'       :P86_W_BRUTO_GRAV_MON,',
'       :P86_CLI_DOC_TIPO',
'  INTO ',
'       FIN.DOC_CLAVE,',
'       FIN.DOC_EMPR,',
'       FIN.DOC_CTA_BCO,',
'       FIN.DOC_SUC,',
'       FIN.DOC_TIPO_MOV,',
'       FIN.DOC_FEC_DOC,',
'       FIN.DOC_CLI,',
'       FIN.DOC_CLI_NOM,',
'       FIN.DOC_NRO_DOC,',
'       FIN.DOC_MON,',
'       FIN.DOC_CLI_DIR,',
'       FIN.DOC_CLI_RUC,',
'       FIN.DOC_OBS,',
'       FIN.DOC_SALDO_LOC,',
'       FIN.DOC_SALDO_MON,',
'       FIN.DOC_SALDO_PER_ACT_LOC,',
'       FIN.DOC_SALDO_PER_ACT_MON,',
'       FIN.DOC_FEC_OPER,',
'       FIN.DOC_LOGIN,',
'       FIN.DOC_FEC_GRAB,',
'       FIN.DOC_SIST,',
'       FIN.DOC_IND_EXPORT,',
'       FIN.DOC_CTACO,',
'       FIN.DOC_TIPO_SALDO,',
'       FIN.DOC_LEGAJO,',
'       FIN.DOC_CLI_TEL,',
'       FIN.DOC_SERIE,',
'       FIN.DOC_COND_VTA,',
'       FIN.DOC_IND_EXPORT,',
'       FIN.DOC_CLAVE_STK,',
'       FIN.DOC_IND_STK,',
'       FIN.DOC_CANAL,',
'       FIN.DOC_CTA_BCO_FCON,',
'       FIN.DOC_IND_IMPR_CONYUGUE,',
'       FIN.DOC_IND_IMPR_VENDEDOR,',
'       FIN.DOC_BRUTO_EXEN_MON,',
'       FIN.DOC_BRUTO_EXEN_LOC,',
'       FIN.DOC_BRUTO_GRAV_LOC,',
'       FIN.DOC_BRUTO_GRAV_MON',
'       ,FIN.DOC_TIPO_DOC_CLI_PROV',
'   FROM DUAL;',
' ELSIF :P86_S_TIPO_FACTURA =  2 THEN ',
'',
' DECLARE',
'  SALDO_MON NUMBER;',
'  SALDO_LOC NUMBER;',
' BEGIN',
'',
' IF :P86_DOC_FEC_DOC BETWEEN :P86_CONF_PER_ACT_INI AND :P86_CONF_PER_ACT_FIN THEN',
'       SALDO_LOC := :P86_DOC_SALDO_LOC;',
'       SALDO_MON := :P86_DOC_SALDO_MON;',
' ELSE',
'       SALDO_LOC := 0;',
'       SALDO_MON := 0;',
' END IF;',
'',
' SELECT :P86_DOC_CLAVE,',
'       :P_EMPRESA,',
'       --:P86_DOC_CTA_BCO,',
'       :P_SUCURSAL,',
'       :P86_CONF_COD_FAC_CRED,',
'       :P86_DOC_FEC_DOC,',
'       :P86_DOC_CLI,',
'       :P86_DOC_CLI_NOM,',
'       :P86_DOC_NRO_DOC,',
'       :P86_DOC_MON,',
'       :P86_DOC_CLI_DIR,',
'       :P86_DOC_CLI_RUC,',
'       OBS,',
'       0, 0, 0, 0,',
'       :P86_DOC_FEC_DOC,',
'       :APP_USER,',
'       SYSDATE,',
'       ''FAC'',',
'       EXPORTACION,',
'       DECODE(:P86_S_TIPO_FACTURA,1,:P86_CONF_CTA_CONT_DEU_FCON, NULL),',
'       :P86_DOC_TIPO_SALDO,',
'       :P86_DOC_LEGAJO,',
'       :P86_DOC_CLI_TEL,',
'       :P86_DOC_SERIE,',
'       :P86_S_TIPO_FACTURA,',
'       :P86_EXPORTACION,',
'       :P86_DOC_CLAVE_STK,',
'       ''S'',',
'       :P86_DOC_CANAL,',
'       :P86_DOC_EXP_PROFORMA,--:P86_PROFORMA,',
'       :P86_DOC_CTA_BCO,',
'       :P86_DOC_IND_IMPR_CONYUGUE,',
'       :P86_DOC_IND_IMPR_VENDEDOR,',
'       :P86_W_BRUTO_EXEN_MON,',
'       :P86_W_BRUTO_EXEN_LOC,',
'       :P86_W_BRUTO_GRAV_MON,',
'       :P86_W_BRUTO_GRAV_LOC,',
'       1,',
'       ''S'',',
'       :P86_DOC_TIMBRADO',
'       ,:P86_CLI_DOC_TIPO',
'  INTO ',
'       FIN.DOC_CLAVE,',
'       FIN.DOC_EMPR,',
'       --FIN.DOC_CTA_BCO,',
'       FIN.DOC_SUC,',
'       FIN.DOC_TIPO_MOV,',
'       FIN.DOC_FEC_DOC,',
'       FIN.DOC_CLI,',
'       FIN.DOC_CLI_NOM,',
'       FIN.DOC_NRO_DOC,',
'       FIN.DOC_MON,',
'       FIN.DOC_CLI_DIR,',
'       FIN.DOC_CLI_RUC,',
'       FIN.DOC_OBS,',
'       FIN.DOC_SALDO_LOC,',
'       FIN.DOC_SALDO_MON,',
'       FIN.DOC_SALDO_PER_ACT_LOC,',
'       FIN.DOC_SALDO_PER_ACT_MON,',
'       FIN.DOC_FEC_OPER,',
'       FIN.DOC_LOGIN,',
'       FIN.DOC_FEC_GRAB,',
'       FIN.DOC_SIST,',
'       FIN.DOC_IND_EXPORT,',
'       FIN.DOC_CTACO,',
'       FIN.DOC_TIPO_SALDO,',
'       FIN.DOC_LEGAJO,',
'       FIN.DOC_CLI_TEL,',
'       FIN.DOC_SERIE,',
'       FIN.DOC_COND_VTA,',
'       FIN.DOC_IND_EXPORT,',
'       FIN.DOC_CLAVE_STK,',
'       FIN.DOC_IND_STK,',
'       FIN.DOC_CANAL,',
'       FIN.DOC_EXP_PROFORMA,',
'       FIN.DOC_CTA_BCO_FCON,',
'       FIN.DOC_IND_IMPR_CONYUGUE,',
'       FIN.DOC_IND_IMPR_VENDEDOR,',
'       FIN.DOC_BRUTO_EXEN_MON,',
'       FIN.DOC_BRUTO_EXEN_LOC,',
'       FIN.DOC_BRUTO_GRAV_LOC,',
'       FIN.DOC_BRUTO_GRAV_MON,',
'       FIN.DOC_PLAN_FINAN,',
'       FIN.DOC_IND_CUOTA,',
'       FIN.DOC_TIMBRADO',
'       ,FIN.DOC_TIPO_DOC_CLI_PROV',
'   FROM DUAL;',
'',
'EXCEPTION',
' WHEN OTHERS THEN ',
' RAISE_APPLICATION_ERROR(-20010, SQLCODE||'' HI '' ||SQLERRM);',
'',
'END;',
'',
'END IF;',
'',
'BEGIN',
'',
'  FACI053.PP_ACTUALIZAR_DOCUMENTO_FIN(FIN_DATOS => FIN);',
'  FACI053.PP_ACTUALIZAR_REGISTRO(I_DOC_CLAVE => :P86_DOC_CLAVE, I_DOCU_CLAVE => :P86_DOC_CLAVE_STK);',
'',
' ---PARA QUE CUANDO SEA UNA FACTURA MANUAL NO ACTUALIZE EL NRO',
'IF coalesce(:P86_IND_FACT_MANUAL, ''N'') = ''N'' THEN',
'',
'DECLARE',
'IP_MAQUINA VARCHAR2(4500);',
'BEGIN',
'',
'   SELECT SSS INTO IP_MAQUINA FROM IP_MAQUINA;',
'',
'   IF :P86_DOC_OPERADOR = 1 THEN',
'   ',
'       UPDATE GEN_IMPRESORA',
'         SET IMP_ULT_FACT_OPER1 = :P86_DOC_NRO_DOC',
'       WHERE IMPR_IP = :P0_IP_MAQUINA ',
'       AND IMP_EMPR= :P_EMPRESA;',
'     ',
'   ELSE',
'    ',
'      UPDATE GEN_IMPRESORA',
'         SET IMP_ULT_FACT  = :P86_DOC_NRO_DOC',
'       WHERE IMPR_IP = :P0_IP_MAQUINA ',
'        AND IMP_EMPR= :P_EMPRESA;',
'     ',
'  END IF;',
'  ',
'EXCEPTION',
'  WHEN NO_DATA_FOUND THEN',
'     RAISE_APPLICATION_ERROR(-20010,''No se puede desbloquar la impresora!.'');',
'  WHEN OTHERS THEN',
'     RAISE_APPLICATION_ERROR(-20010, SQLCODE||''PROBLEMAS EN UNLOCK_IMPRESORA'');',
'     ',
'END;',
'',
'END IF;',
'',
'',
'EXCEPTION ',
' WHEN OTHERS THEN',
'  ROLLBACK;',
'  RAISE_APPLICATION_ERROR(-20010, SQLCODE||'' ERROR EN ACTUALIZAR REGISTROS '' ||SQLERRM);',
'END;',
'',
'BEGIN',
'',
'',
' IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => ''DETALLES_FACI052'') THEN',
'    APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => ''DETALLES_FACI052'');',
' END IF;',
' IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => ''CUOTA_FACI052'') THEN',
'     APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME =>''CUOTA_FACI052'');',
' END IF;',
' IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => ''CHEQUES_FACI052'') THEN',
'     APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => ''CHEQUES_FACI052'');',
' END IF;',
' IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => ''TARJETA_FACI052'') THEN',
'     APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => ''TARJETA_FACI052'');',
' END IF;',
'  ',
'',
'END;',
'',
'BEGIN',
'  -- CALL THE PROCEDURE',
'  FACI053.pp_llamar_reporte(i_p_clave => :P86_DOC_CLAVE,',
'                            i_p_cliente => :P86_DOC_CLI,',
'                            i_p_empresa => :p_empresa);',
'END;',
'',
'V_URL := APEX_UTIL.PREPARE_URL(P_URL           => ''f?p=109:3010:&APP_SESSION.:::::'',',
'                                   P_CHECKSUM_TYPE => ''SESSION'');',
'								   ',
'								   ',
'    :P86_URL_REPORTE := V_URL;',
'',
' ',
'',
'END;',
'',
''))
,p_process_error_message=>'Error la Procesar!'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(703825489757171451)
,p_process_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
unistr('Acci\00F3n Procesada <br>'),
' <br>',
'Clave Fin: &P86_DOC_CLAVE. <br>',
'',
'<a target="_blank" href="&P86_URL_REPORTE.">Imprimir Factura</a><br>'))
);
end;
/
begin
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(703838948368171474)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'cancelar'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(703825036069171450)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(703837799844171472)
,p_process_sequence=>30
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'cargar_datos_incial'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- Call the procedure',
'  FACI053.PP_CARGAR_DATOS;',
'END;',
'',
'',
'BEGIN',
'',
'',
' IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => ''DETALLES_FACI052'') THEN',
'    APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => ''DETALLES_FACI052'');',
' END IF;',
' IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => ''CUOTA_FACI052'') THEN',
'     APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME =>''CUOTA_FACI052'');',
' END IF;',
' IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => ''CHEQUES_FACI052'') THEN',
'     APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => ''CHEQUES_FACI052'');',
' END IF;',
' IF APEX_COLLECTION.COLLECTION_EXISTS(P_COLLECTION_NAME => ''TARJETA_FACI052'') THEN',
'     APEX_COLLECTION.DELETE_COLLECTION(P_COLLECTION_NAME => ''TARJETA_FACI052'');',
' END IF;',
'  ',
'',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
prompt --application/end_environment
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
