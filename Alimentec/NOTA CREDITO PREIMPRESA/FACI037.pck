CREATE OR REPLACE PACKAGE FACI037 IS

  -- AUTHOR  : PROGRAMACION4
  -- CREATED : 24/02/2021 13:41:54
  -- PURPOSE : NOTA DE CREDITO HILAGRO

  PROCEDURE PP_CARGAR_CAB(I_EMPRESA            IN NUMBER,
                          I_DOC_CLAVE          IN NUMBER,
                          O_DOCU_CLI           OUT NUMBER,
                          O_CTA_BCO            OUT NUMBER,
                          O_DOC_LEGAJO         OUT NUMBER,
                          O_DOCU_OCARGA_LONDON OUT NUMBER,
                          O_DEP_ORIGEN         OUT NUMBER,
                          O_OCA_CAMION         OUT VARCHAR2);

  PROCEDURE PP_CARGAR_DET_FACT(I_EMPRESA IN NUMBER, I_DOC_CLAVE IN NUMBER);

  PROCEDURE PP_BUSCAR_FACT(I_NRO_FACT  IN NUMBER,
                           I_EMPRESA   IN NUMBER,
                           O_DOC_CLAVE OUT NUMBER,
                           O_TOO_MANY  OUT VARCHAR2,
                           FILTRO_ANHO IN VARCHAR2,
                           I_SUCURSAL  IN NUMBER,
                           O_DOC_MON   OUT NUMBER);

  PROCEDURE PP_BORRAR_COLLECTION;

  PROCEDURE PP_OBTENER_ULT_NRO(P_EMPRESA      IN NUMBER,
                               P_DOC_TIPO_MOV IN NUMBER,
                               P_DOC_NRO_DOC  OUT NUMBER);

  PROCEDURE PP_DATOS_CLIENTE(I_EMPRESA  IN NUMBER,
                             I_DOCU_CLI IN NUMBER,
                             O_CLI_NOM  OUT VARCHAR2,
                             O_CLI_RUC  OUT VARCHAR2);

  PROCEDURE PP_ADD_DET(I_EMPRESA                  IN NUMBER,
                       I_SEQ_ID                   IN NUMBER,
                       I_ART_CODIGO               IN NUMBER,
                       I_ART_CANT                 IN NUMBER,
                       I_MOT_DEV                  IN NUMBER,
                       I_PRECIO                   IN NUMBER,
                       I_EXENTA                   IN NUMBER,
                       I_GRAVADA                  IN NUMBER,
                       I_IVA                      IN NUMBER,
                       I_IMPU_PORCENTAJE          IN NUMBER,
                       I_IMPU_PORC_BASE_IMPONIBLE IN NUMBER,
                       I_DOC_MON                  IN NUMBER,
                       I_DOC_TASA                 IN NUMBER,
                       I_MON_DEC_IMP              IN NUMBER,
                       I_ART_IMPU                 IN NUMBER,
                       I_DOCU_CLAVE_FAC_NCR       IN NUMBER);

  PROCEDURE PP_CALCULAR_PRECIO(IO_PRECIO                  IN OUT NUMBER,
                               I_CANTIDAD                 IN NUMBER,
                               I_MON_DEC_IMP              IN NUMBER,
                               I_DETA_PORC_DTO            IN NUMBER,
                               I_IMPU_PORC_BASE_IMPONIBLE IN NUMBER,
                               I_IMPU_PORCENTAJE          IN NUMBER,
                               O_IVA                      OUT NUMBER,
                               O_GRAVADO                  OUT NUMBER,
                               O_EXENTA                   OUT NUMBER);

  PROCEDURE PP_ELIMINAR_DET(I_SEQ IN NUMBER);

  PROCEDURE PP_DATOS_ARTICULO(I_EMPRESA                  IN NUMBER,
                              I_ART_CODIGO               IN NUMBER,
                              O_IMPU_PORCENTAJE          OUT NUMBER,
                              O_IMPU_PORC_BASE_IMPONIBLE OUT NUMBER,
                              O_ART_IMPU                 OUT NUMBER);

  PROCEDURE PP_DATOS_MONEDA(I_EMPRESA     IN NUMBER,
                            I_MON_CODIGO  IN NUMBER,
                            I_DOC_FEC_DOC IN DATE,
                            O_MON_DESC    OUT VARCHAR2,
                            O_MON_DEC_IMP OUT NUMBER,
                            O_DOC_TASA    OUT NUMBER,
                            O_DOC_TASA_US OUT NUMBER);

  PROCEDURE PP_DATOS_CTABCO(I_EMPRESA     IN NUMBER,
                            I_CTA_BCO     IN NUMBER,
                            O_CTA_BCO_DES OUT VARCHAR2,
                            O_CTA_MON     OUT NUMBER);

  PROCEDURE PP_GENERAR_CUOTA(I_EMPRESA          IN NUMBER,
                             I_FIRTS_VTO        IN DATE,
                             I_CANT_CUOTA       IN NUMBER,
                             I_DOC_MON          IN NUMBER,
                             I_IMPORTE          IN NUMBER,
                             I_TIPO_VENCIMIENTO IN VARCHAR2,
                             I_DIAS_ENTRE       IN NUMBER);

  FUNCTION FP_FORMAT_CHAR(I_NUMBER IN NUMBER, I_MON_DEC_IMP IN NUMBER)
    RETURN VARCHAR2;

  PROCEDURE PP_ACTUALIZAR_REGISTRO(I_EMPRESA             IN NUMBER,
                                   I_SUCURSAL            IN NUMBER,
                                   I_S_CREDITO           IN VARCHAR2,
                                   I_DOCU_TIMBRADO2      IN NUMBER,
                                   I_DOC_TASA_US         IN NUMBER,
                                   I_DOC_CTA_BCO         IN NUMBER,
                                   I_DOCU_CLAVE_FAC_NCR  IN NUMBER,
                                   I_DOCU_LEGAJO         IN NUMBER,
                                   I_DOCU_NRO_DOC        IN NUMBER,
                                   I_DOCU_DEP_ORIG       IN NUMBER,
                                   I_DOCU_MON            IN NUMBER,
                                   I_DOCU_CLI            IN NUMBER,
                                   I_DOCU_CLI_NOM        IN VARCHAR2,
                                   I_DOCU_CLI_RUC        IN VARCHAR2,
                                   I_DOCU_FEC_EMIS       IN DATE,
                                   I_DOCU_FEC_OPER       IN DATE,
                                   I_DOCU_OBS            IN VARCHAR2,
                                   I_DOCU_AFECT_COMISION IN VARCHAR2,
                                   I_DOCU_OCARGA_LONDON  IN NUMBER);
  /*
  PROCEDURE PP_ESTABL_VARIABLES_BDOCU_STK(I_EMPRESA             IN NUMBER,
                                           I_SUCURSAL            IN NUMBER,
                                           I_S_CREDITO           IN VARCHAR2,
                                           I_DOC_TASA_US         IN NUMBER,
                                           I_DOCU_LEGAJO         IN NUMBER,
                                           I_DOCU_NRO_DOC        IN NUMBER,
                                           I_DOCU_DEP_ORIG       IN NUMBER,
                                           I_DOCU_MON            IN NUMBER,
                                           I_DOCU_CLI            IN NUMBER,
                                           I_DOCU_CLI_NOM        IN VARCHAR2,
                                           I_DOCU_CLI_RUC        IN VARCHAR2,
                                           I_DOCU_FEC_EMIS       IN DATE,
                                           I_DOCU_FEC_OPER       IN DATE,
                                           I_DOCU_OBS            IN VARCHAR2,
                                           I_DOCU_AFECT_COMISION IN VARCHAR2,
                                           I_DOCU_OCARGA_LONDON  IN NUMBER,
                                           I_DOCU_CLAVE_STOPES   IN NUMBER);*/
END FACI037;
/
CREATE OR REPLACE PACKAGE body FACI037 IS

  SIN_PARAMETRO EXCEPTION;
  TFIN_DOCUMENTO              FIN_DOCUMENTO%ROWTYPE;
  TSTK_DOCUMENTO              STK_DOCUMENTO%ROWTYPE;
  BCONF_FAC                   FAC_CONFIGURACION%ROWTYPE;
  BCONF_STK                   STK_CONFIGURACION%ROWTYPE;
  BSUC                        GEN_SUCURSAL%ROWTYPE;
  BCONF_FIN                   FIN_CONFIGURACION%ROWTYPE;
  BCONF_STK_CONF_PER_ACT_INI  DATE;
  BCONF_STK_CONF_PER_ACT_FIN  DATE;
  BCONF_STK_CONF_PER_SGTE_INI DATE;
  BCONF_STK_CONF_PER_SGTE_FIN DATE;
  G_MON                       GEN_MONEDA%ROWTYPE;

  TYPE TPARAMETER IS RECORD(
    P_OPER       NVARCHAR2(200),
    P_TIPO_TABLA NVARCHAR2(200),
    P_MON_US     NUMBER := 2,
    P_MON_LOC    NUMBER := 1,
    P_PROGRAMA   VARCHAR2(50) := 'FACI037');

  PARAMETER TPARAMETER;

  FUNCTION FP_FIN_SEQ_DOC RETURN NUMBER IS
    W_CLAVE NUMBER;
  BEGIN
    W_CLAVE := FIN_SEQ_DOC_NEXTVAL;

    RETURN W_CLAVE;
  END FP_FIN_SEQ_DOC;

  PROCEDURE PP_CARGAR_CAB(I_EMPRESA            IN NUMBER,
                          I_DOC_CLAVE          IN NUMBER,
                          O_DOCU_CLI           OUT NUMBER,
                          O_CTA_BCO            OUT NUMBER,
                          O_DOC_LEGAJO         OUT NUMBER,
                          O_DOCU_OCARGA_LONDON OUT NUMBER,
                          O_DEP_ORIGEN         OUT NUMBER,
                          O_OCA_CAMION         OUT VARCHAR2) IS
    SALIR EXCEPTION;
    V_OCARGA NUMBER;
  BEGIN

    IF I_DOC_CLAVE IS NULL THEN
      RAISE SALIR;
    END IF;

    SELECT SD.DOCU_CLI,
           FD.DOC_CTA_BCO_FCON,
           FD.DOC_LEGAJO,
           SD.DOCU_OCARGA_LONDON,
           SD.DOCU_DEP_ORIG
      INTO O_DOCU_CLI, O_CTA_BCO, O_DOC_LEGAJO, V_OCARGA, O_DEP_ORIGEN
      FROM STK_DOCUMENTO SD, FIN_DOCUMENTO FD
     WHERE SD.DOCU_CLAVE = FD.DOC_CLAVE_STK
       AND SD.DOCU_EMPR = FD.DOC_EMPR
       AND FD.DOC_CLAVE = I_DOC_CLAVE
       AND SD.DOCU_EMPR = I_EMPRESA;

    BEGIN
      IF V_OCARGA IS NULL THEN
        RAISE SIN_PARAMETRO;
      END IF;

      O_DOCU_OCARGA_LONDON := V_OCARGA;

      SELECT C.OCA_CAMION || ' - ' || V.RMAR_VEH_DESC
        INTO O_OCA_CAMION
        FROM STK_ORDEN_CARGA_LONDON C, STK_REMI_VEHICULO V
       WHERE C.OCA_CAMION = V.RMAR_VEH_COD
         AND C.OCA_EMPR = V.RMAR_EMPR
         AND C.OCA_CLAVE = V_OCARGA
         AND OCA_EMPR = I_EMPRESA;

    EXCEPTION
      WHEN SIN_PARAMETRO THEN
        NULL;
    END ORDEN_CARGA;

    BEGIN
      FACI037.PP_CARGAR_DET_FACT(I_EMPRESA   => I_EMPRESA,
                                 I_DOC_CLAVE => I_DOC_CLAVE);
    END;
  EXCEPTION
    WHEN SALIR THEN
      NULL;
  END PP_CARGAR_CAB;

  PROCEDURE PP_CARGAR_DET_FACT(I_EMPRESA IN NUMBER, I_DOC_CLAVE IN NUMBER) IS
    CURSOR CUR_DET IS
      SELECT SDD.DETA_ART,
             SDD.DETA_NRO_ITEM DETA_NRO_ITEM,
             SDD.DETA_CANT DETA_CANT,
             ((SDD.DETA_IMP_NETO_MON + SDD.DETA_IVA_MON) / SDD.DETA_CANT) PRECIO,
             SDD.DETA_IMP_NETO_MON DETA_IMP_NETO_MON,
             SDD.DETA_IVA_MON,
             SDD.DETA_IMPU,
             FD.DOC_CLAVE
        FROM STK_DOCUMENTO_DET SDD, STK_ARTICULO SA, FIN_DOCUMENTO FD
       WHERE FD.DOC_CLAVE = I_DOC_CLAVE
         AND FD.DOC_CLAVE_STK = SDD.DETA_CLAVE_DOC
         AND SDD.DETA_ART = SA.ART_CODIGO
         AND DETA_EMPR = ART_EMPR
         AND ART_EMPR = DOC_EMPR
         AND ART_EMPR = I_EMPRESA
         AND ART_EST = 'A'; --ORDER BY SDD.DETA_NRO_ITEM;
  BEGIN

    APEX_COLLECTION.CREATE_OR_TRUNCATE_COLLECTION(P_COLLECTION_NAME => 'FACI036_DET');

    FOR C IN CUR_DET LOOP
      APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'FACI036_DET',
                                 P_C001            => C.DETA_CANT,
                                 P_C002            => '',
                                 P_C003            => '',
                                 P_C004            => '',
                                 P_C005            => '',
                                 P_C006            => '',
                                 P_C007            => '',
                                 P_C008            => '',
                                 P_C009            => '',
                                 P_C010            => '',
                                 P_N001            => C.DETA_ART,
                                 P_N002            => 0,
                                 P_N003            => C.PRECIO,
                                 P_N004            => 0,
                                 P_N005            => 0);
    END LOOP;

  END PP_CARGAR_DET_FACT;

  FUNCTION FP_BUSCAR_COD_OP(I_EMPRESA IN NUMBER, I_DESC_OPER IN VARCHAR2)
    RETURN NUMBER IS
    V_OPER_CODIGO NUMBER := 0;

  BEGIN
    SELECT OPER_CODIGO
      INTO V_OPER_CODIGO
      FROM STK_OPERACION
     WHERE OPER_DESC = I_DESC_OPER
       AND OPER_EMPR = I_EMPRESA;

    RETURN V_OPER_CODIGO;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20010, 'Tipo operacion inexistente!');
  END FP_BUSCAR_COD_OP;

  FUNCTION FP_COTIZACION(I_EMPRESA      IN NUMBER,
                         I_DOC_MON      IN NUMBER,
                         I_DOC_FEC_EMIS IN DATE) RETURN NUMBER IS

    V_COT_TASA STK_COTIZACION.COT_TASA%TYPE;

  BEGIN
    SELECT COT_TASA
      INTO V_COT_TASA
      FROM STK_COTIZACION
     WHERE trunc(COT_FEC) = trunc(I_DOC_FEC_EMIS)
       AND COT_MON = I_DOC_MON
       AND COT_EMPR = I_EMPRESA;

    RETURN V_COT_TASA;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'Primero debe ingresar la cotizacion del ' ||
                              'dia para la moneda ' || TO_CHAR(I_DOC_MON) || '. Empresa: '||
                              I_EMPRESA||'. Fecha:'||to_char(I_DOC_FEC_EMIS, 'dd/mm/yyyy')
                              );
  END FP_COTIZACION;
--======================================================================================================
  PROCEDURE PP_BUSCAR_FACT(I_NRO_FACT  IN NUMBER,
                           I_EMPRESA   IN NUMBER,
                           O_DOC_CLAVE OUT NUMBER,
                           O_TOO_MANY  OUT VARCHAR2,
                           FILTRO_ANHO IN VARCHAR2,
                           I_SUCURSAL  IN NUMBER,
                           O_DOC_MON   OUT NUMBER) AS
    V_CLAVE NUMBER;
    V_SUC   NUMBER;
    V_DOC_CLAVE_FAC_NCR NUMBER;

  BEGIN

    --RAISE_APPLICATION_ERROR(-20010, 'FILTRO_ANHO: '||FILTRO_ANHO);

    IF I_NRO_FACT IS NOT NULL THEN
      SELECT T.DOC_CLAVE, T.DOC_SUC, T.doc_clave_fac_ncr, T.DOC_MON
        INTO V_CLAVE, V_SUC, V_DOC_CLAVE_FAC_NCR,O_DOC_MON
        FROM FIN_DOCUMENTO T
       WHERE T.DOC_TIPO_MOV IN (9, 10)
         AND T.DOC_EMPR = I_EMPRESA
         AND T.DOC_NRO_DOC = I_NRO_FACT
         and ( FILTRO_ANHO = 'Y' or TO_CHAR(T.DOC_FEC_DOC, 'YYYY') = TO_CHAR(SYSDATE, 'YYYY'));
    ELSE
      NULL;
    END IF;
    IF V_SUC <> I_SUCURSAL THEN
      RAISE_APPLICATION_ERROR(-20010, 'La sucursal del documento no coincide con el sistema');
        END IF;
    IF V_DOC_CLAVE_FAC_NCR IS NOT NULL THEN
      RAISE_APPLICATION_ERROR(-20010, 'El documento ya esta relacionada a una NC '|| V_DOC_CLAVE_FAC_NCR);
          END IF;
              O_DOC_CLAVE := V_CLAVE;
  EXCEPTION
    WHEN TOO_MANY_ROWS THEN
      O_TOO_MANY  := 'S';
      O_DOC_CLAVE := NULL;
    WHEN NO_DATA_FOUND THEN
      O_DOC_CLAVE := NULL;
      RAISE_APPLICATION_ERROR(-20010, 'Documento inexistente.');

  END PP_BUSCAR_FACT;
--======================================================================================================
  PROCEDURE PP_DATOS_CLIENTE(I_EMPRESA  IN NUMBER,
                             I_DOCU_CLI IN NUMBER,
                             O_CLI_NOM  OUT VARCHAR2,
                             O_CLI_RUC  OUT VARCHAR2) AS
  BEGIN
    IF I_DOCU_CLI IS NULL THEN
      RAISE SIN_PARAMETRO;
    END IF;

    SELECT T.CLI_NOM, T.CLI_RUC A
      INTO O_CLI_NOM, O_CLI_RUC
      FROM FIN_CLIENTE T
     WHERE T.CLI_CODIGO = I_DOCU_CLI
       AND T.CLI_EMPR = I_EMPRESA;
  EXCEPTION
    WHEN SIN_PARAMETRO THEN
      NULL;
  END PP_DATOS_CLIENTE;

  PROCEDURE PP_BORRAR_COLLECTION AS
  BEGIN
    APEX_COLLECTION.CREATE_OR_TRUNCATE_COLLECTION(P_COLLECTION_NAME => 'FACI036_DET');
    APEX_COLLECTION.CREATE_OR_TRUNCATE_COLLECTION(P_COLLECTION_NAME => 'FACI036_CUOTA');
  END PP_BORRAR_COLLECTION;

  PROCEDURE PP_OBTENER_ULT_NRO(P_EMPRESA      IN NUMBER,
                               P_DOC_TIPO_MOV IN NUMBER,
                               P_DOC_NRO_DOC  OUT NUMBER) AS
  BEGIN

    P_DOC_NRO_DOC := FACI039.FL_OBTENER_ULT_NRO(I_TIPO_MOV  => P_DOC_TIPO_MOV,
                                                I_IMPRESORA => '',
                                                I_EMPRESA   => P_EMPRESA);

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20002,
                              'Primero debe ingresar un registro para la impresora: ' ||
                              TO_CHAR(FP_IP_USER));
  END PP_OBTENER_ULT_NRO;

  PROCEDURE PP_ADD_DET(I_EMPRESA                  IN NUMBER,
                       I_SEQ_ID                   IN NUMBER,
                       I_ART_CODIGO               IN NUMBER,
                       I_ART_CANT                 IN NUMBER,
                       I_MOT_DEV                  IN NUMBER,
                       I_PRECIO                   IN NUMBER,
                       I_EXENTA                   IN NUMBER,
                       I_GRAVADA                  IN NUMBER,
                       I_IVA                      IN NUMBER,
                       I_IMPU_PORCENTAJE          IN NUMBER,
                       I_IMPU_PORC_BASE_IMPONIBLE IN NUMBER,
                       I_DOC_MON                  IN NUMBER,
                       I_DOC_TASA                 IN NUMBER,
                       I_MON_DEC_IMP              IN NUMBER,
                       I_ART_IMPU                 IN NUMBER,
                       I_DOCU_CLAVE_FAC_NCR       IN NUMBER) AS

    V_IVA_MON     NUMBER;
    V_IVA_LOC     NUMBER;
    V_EXENTA_MON  NUMBER;
    V_EXENTA_LOC  NUMBER;
    V_GRAV_5_MON  NUMBER;
    V_GRAV_5_LOC  NUMBER;
    V_GRAV_10_MON NUMBER;
    V_GRAV_10_LOC NUMBER;
    V_ART_IN_FACT NUMBER;
    V_ART_IN_COLL NUMBER;
  BEGIN

    IF I_DOCU_CLAVE_FAC_NCR IS NOT NULL THEN
      SELECT COUNT(*)
        INTO V_ART_IN_FACT
        FROM FAC_DOCUMENTO_DET T
       WHERE DET_CLAVE_DOC = I_DOCU_CLAVE_FAC_NCR
         AND T.DET_EMPR = I_EMPRESA
         AND T.DET_ART = I_ART_CODIGO;

      /*IF V_ART_IN_FACT = 0 THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'Este articulo no esta en la factura, favor verificar');
      END IF;*/

    END IF;

    SELECT COUNT(*)
      INTO V_ART_IN_COLL
      FROM APEX_COLLECTIONS A
     WHERE A.COLLECTION_NAME = 'FACI036_DET'
       AND A.N001 = I_ART_CODIGO
       AND A.C010 = I_MOT_DEV;

    IF V_ART_IN_COLL > 0 THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'Este articulo y motivo ya esta en la lista, tiene que hacer click en editar');
    END IF;

    IF I_ART_CODIGO IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010, 'El articulo no puede ser nulo.');
    END IF;
    IF NVL(I_ART_CANT, 0) <= 0 THEN
      RAISE_APPLICATION_ERROR(-20010, 'Favor corregir la cantidad.');
    END IF;

    IF I_MOT_DEV IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010, 'El motivo no puede ser nulo');
    END IF;
    IF I_DOC_MON IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010, 'Falta completar la moneda');
    END IF;

    IF NVL(I_PRECIO, 0) <= 0 THEN
      RAISE_APPLICATION_ERROR(-20010, 'Favor corregir el precio.');
    END IF;

    IF I_IMPU_PORCENTAJE = 10 THEN
      V_GRAV_10_MON := NVL(I_GRAVADA, 0);
      V_GRAV_10_LOC := ROUND(V_GRAV_10_MON * I_DOC_TASA, I_MON_DEC_IMP);
    END IF;
    IF I_IMPU_PORCENTAJE = 5 THEN
      V_GRAV_5_MON := NVL(I_GRAVADA, 0);
      V_GRAV_5_LOC := ROUND(V_GRAV_5_MON * I_DOC_TASA, I_MON_DEC_IMP);
    END IF;

    IF I_IMPU_PORCENTAJE IN (5, 10) THEN
      V_IVA_MON := NVL(I_IVA, 0);
      V_IVA_LOC := ROUND(V_IVA_MON * I_DOC_TASA, I_MON_DEC_IMP);
    END IF;

    APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'FACI036_DET',
                               P_N001            => I_ART_CODIGO,
                               P_N002            => I_ART_CANT,
                               P_N003            => NVL(I_PRECIO, 0),
                               P_N004            => NVL(I_EXENTA, 0),
                               P_N005            => NVL(I_GRAVADA, 0),
                               P_C001            => NVL(I_IVA, 0),
                               P_C010            => I_MOT_DEV,
                               P_C011            => I_IMPU_PORCENTAJE,
                               P_C012            => I_IMPU_PORC_BASE_IMPONIBLE,
                               P_C013            => I_ART_IMPU,
                               P_C020            => NVL(V_IVA_MON, 0),
                               P_C021            => NVL(V_IVA_LOC, 0),
                               P_C022            => NVL(V_EXENTA_MON, 0),
                               P_C023            => NVL(V_EXENTA_LOC, 0),
                               P_C024            => NVL(V_GRAV_5_MON, 0),
                               P_C025            => NVL(V_GRAV_5_LOC, 0),
                               P_C026            => NVL(V_GRAV_10_MON, 0),
                               P_C027            => NVL(V_GRAV_10_LOC, 0));

  END PP_ADD_DET;

  PROCEDURE PP_CALCULAR_PRECIO(IO_PRECIO                  IN OUT NUMBER,
                               I_CANTIDAD                 IN NUMBER,
                               I_MON_DEC_IMP              IN NUMBER,
                               I_DETA_PORC_DTO            IN NUMBER,
                               I_IMPU_PORC_BASE_IMPONIBLE IN NUMBER,
                               I_IMPU_PORCENTAJE          IN NUMBER,
                               O_IVA                      OUT NUMBER,
                               O_GRAVADO                  OUT NUMBER,
                               O_EXENTA                   OUT NUMBER) AS
    SALIR EXCEPTION;
    PID           NUMBER; --PORCENTAJE DEL IVA DIRECTO SOBRE EL PRECIO DE VENTA
    PG            NUMBER; --PORCENTAJE GRAVADO
    PE            NUMBER; --PORCENTAJE EXENTO
    V_MON_DEC_IMP NUMBER;
  BEGIN
    NULL;

    V_MON_DEC_IMP := NVL(I_MON_DEC_IMP, 0);

    IF IO_PRECIO IS NULL OR I_CANTIDAD IS NULL THEN
      RAISE SALIR;
    END IF;

    IF NVL(I_CANTIDAD, 0) <= 0 THEN
      RAISE_APPLICATION_ERROR(-20010, 'Error de cantidad');
    END IF;

    IO_PRECIO := ABS(IO_PRECIO);

    --HALLAR PORCENTAJES
    PID := (I_IMPU_PORC_BASE_IMPONIBLE / 100) * (I_IMPU_PORCENTAJE / 100) * 100;
    PG  := I_IMPU_PORC_BASE_IMPONIBLE;
    PE  := 100 - PG;

    IF IO_PRECIO <> 0 THEN
      --SI INGRESO UN VALOR NEGATIVO IMPLICA QUE ES CON IVA INCLUIDO
      --CONVERTIR A POSITIVO
      --RESTAR EL DESCUENTO DEL TOTAL
      IO_PRECIO := IO_PRECIO - (IO_PRECIO * NVL(I_DETA_PORC_DTO, 0) / 100);
      O_IVA     := ROUND(PID / (PID + 100) * IO_PRECIO * I_CANTIDAD,
                         V_MON_DEC_IMP);
      O_GRAVADO := ROUND(PG / (PID + 100) * IO_PRECIO * I_CANTIDAD,
                         V_MON_DEC_IMP);
      O_EXENTA  := ROUND(PE / (PID + 100) * IO_PRECIO * I_CANTIDAD,
                         V_MON_DEC_IMP);
    ELSE
      IO_PRECIO := 0;
      O_IVA     := 0;
      O_GRAVADO := 0;
      O_EXENTA  := 0;
    END IF;

  EXCEPTION
    WHEN SALIR THEN
      IO_PRECIO := IO_PRECIO;
  END PP_CALCULAR_PRECIO;

  PROCEDURE PP_ELIMINAR_DET(I_SEQ IN NUMBER) AS
  BEGIN

    APEX_COLLECTION.DELETE_MEMBER(P_COLLECTION_NAME => 'FACI036_DET',
                                  P_SEQ             => I_SEQ);
    APEX_COLLECTION.RESEQUENCE_COLLECTION(P_COLLECTION_NAME => 'FACI036_DET');

  END PP_ELIMINAR_DET;

  PROCEDURE PP_DATOS_ARTICULO(I_EMPRESA                  IN NUMBER,
                              I_ART_CODIGO               IN NUMBER,
                              O_IMPU_PORCENTAJE          OUT NUMBER,
                              O_IMPU_PORC_BASE_IMPONIBLE OUT NUMBER,
                              O_ART_IMPU                 OUT NUMBER) AS

  BEGIN

    IF I_ART_CODIGO IS NULL THEN
      RAISE SIN_PARAMETRO;
    END IF;

    SELECT IMPU_PORCENTAJE, I.IMPU_PORC_BASE_IMPONIBLE, SA.ART_IMPU
      INTO O_IMPU_PORCENTAJE, O_IMPU_PORC_BASE_IMPONIBLE, O_ART_IMPU
      FROM GEN_IMPUESTO I, STK_ARTICULO SA
     WHERE IMPU_CODIGO = SA.ART_IMPU
       AND IMPU_EMPR = SA.ART_EMPR
       AND SA.ART_EMPR = I_EMPRESA
       AND SA.ART_CODIGO = I_ART_CODIGO;
  EXCEPTION
    WHEN SIN_PARAMETRO THEN
      NULL;
  END PP_DATOS_ARTICULO;

  PROCEDURE PP_DATOS_MONEDA(I_EMPRESA     IN NUMBER,
                            I_MON_CODIGO  IN NUMBER,
                            I_DOC_FEC_DOC IN DATE,
                            O_MON_DESC    OUT VARCHAR2,
                            O_MON_DEC_IMP OUT NUMBER,
                            O_DOC_TASA    OUT NUMBER,
                            O_DOC_TASA_US OUT NUMBER) AS
    V_DOC_TASA_US NUMBER;
    V_DOC_TASA    NUMBER;
  BEGIN
    IF I_MON_CODIGO IS NULL THEN
      RAISE SIN_PARAMETRO;
    END IF;

    IF I_DOC_FEC_DOC IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010, 'La fecha no puede ser nulo');
    END IF;

    SELECT T.MON_SIMBOLO || ' - ' || T.MON_DESC A, T.MON_DEC_IMP
      INTO O_MON_DESC, O_MON_DEC_IMP
      FROM GEN_MONEDA T
     WHERE T.MON_CODIGO = I_MON_CODIGO
       AND T.MON_EMPR = I_EMPRESA;

    IF I_MON_CODIGO = PARAMETER.P_MON_LOC THEN
      V_DOC_TASA := 1;
    ELSE
      V_DOC_TASA := FP_COTIZACION(I_EMPRESA      => I_EMPRESA,
                                  I_DOC_MON      => I_MON_CODIGO,
                                  I_DOC_FEC_EMIS => I_DOC_FEC_DOC);
    END IF;

    IF I_MON_CODIGO <> PARAMETER.P_MON_US THEN
      V_DOC_TASA_US := FP_COTIZACION(I_EMPRESA      => I_EMPRESA,
                                     I_DOC_MON      => PARAMETER.P_MON_US,
                                     I_DOC_FEC_EMIS => I_DOC_FEC_DOC);
    ELSE
      V_DOC_TASA_US := V_DOC_TASA;
    END IF;

    O_DOC_TASA    := V_DOC_TASA;
    O_DOC_TASA_US := V_DOC_TASA_US;

  EXCEPTION
    WHEN SIN_PARAMETRO THEN
      NULL;
  END PP_DATOS_MONEDA;

  PROCEDURE PP_DATOS_CTABCO(I_EMPRESA     IN NUMBER,
                            I_CTA_BCO     IN NUMBER,
                            O_CTA_BCO_DES OUT VARCHAR2,
                            O_CTA_MON     OUT NUMBER) AS
  BEGIN

    IF I_CTA_BCO IS NULL THEN
      RAISE SIN_PARAMETRO;
    END IF;

    SELECT 'cta : ' || T.CTA_CODIGO || ' -bco. : ' || T.CTA_BCO || ' - ' ||
           T.CTA_DESC CTA,
           T.CTA_MON
      INTO O_CTA_BCO_DES, O_CTA_MON
      FROM FIN_CUENTA_BANCARIA T
     WHERE T.CTA_CODIGO = I_CTA_BCO
       AND T.CTA_EMPR = I_EMPRESA;
  EXCEPTION
    WHEN SIN_PARAMETRO THEN
      NULL;
  END PP_DATOS_CTABCO;

  FUNCTION FP_FORMAT_CHAR(I_NUMBER IN NUMBER, I_MON_DEC_IMP IN NUMBER)
    RETURN VARCHAR2 AS
    V_FORMAT VARCHAR2(1000);
  BEGIN
    IF I_MON_DEC_IMP = 0 THEN
      V_FORMAT := '999g999g999g999g999g999g999';
    END IF;
    IF I_MON_DEC_IMP = 2 THEN
      V_FORMAT := '999g999g999g999g999g999g999D00';
    END IF;
    IF I_MON_DEC_IMP = 4 THEN
      V_FORMAT := '999g999g999g999g999g999g999D0000';
    END IF;
    IF I_MON_DEC_IMP = 6 THEN
      V_FORMAT := '999g999g999g999g999g999g999D000000';
    END IF;

    RETURN TRIM(TO_CHAR(I_NUMBER, V_FORMAT));
  END FP_FORMAT_CHAR;

  PROCEDURE PP_GENERAR_CUOTA(I_EMPRESA          IN NUMBER,
                             I_FIRTS_VTO        IN DATE,
                             I_CANT_CUOTA       IN NUMBER,
                             I_DOC_MON          IN NUMBER,
                             I_IMPORTE          IN NUMBER,
                             I_TIPO_VENCIMIENTO IN VARCHAR2,
                             I_DIAS_ENTRE       IN NUMBER) AS
    V_MON_IMP_DEC NUMBER;
  BEGIN

    BEGIN
      SELECT M.MON_DEC_IMP
        INTO V_MON_IMP_DEC
        FROM GEN_MONEDA M
       WHERE M.MON_CODIGO = I_DOC_MON
         AND M.MON_EMPR = I_EMPRESA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20010, 'Error no se encontro la moneda');
    END;
    --VALIDAR CANTIDAD DE CUOTAS SELECCIONADA

    IF NVL(I_CANT_CUOTA, 0) < 1 THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'Debe ingresar la cantidad de cuotas que desea generar!');
    END IF;

    IF MOD(I_CANT_CUOTA, 1) <> 0 THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'Error favor verificar cantidad cuota');
    END IF;
    --VALIDAR FECHA DEL PRIMER VENCIMIENTO
    IF I_FIRTS_VTO IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'Debe ingresar la fecha del primer vencimiento!');
    END IF;

    --VALIDAR TIPO DE VENCIMIENTO
    IF I_CANT_CUOTA > 1 THEN
      IF NVL(I_TIPO_VENCIMIENTO, ' ') NOT IN ('F', 'V') THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'Tipo de vencimiento debe ser F=Fijo, V=Variable!');
      END IF;
    END IF;

    --VALIDAR LOS DIAS ENTRE UNA CUOTA Y LA SIGUIENTE
    --SOLO SI S_TIPO_VENCIMIENTO = 'V' (VARIABLE)
    --Y SI LA CANTIDAD DE CUOTAS ES MAYOR QUE 1
    IF I_CANT_CUOTA > 1 AND I_TIPO_VENCIMIENTO = 'V' THEN
      IF I_DIAS_ENTRE IS NULL THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'Debe ingresar los dias entre una cuota y la otra!');
      END IF;
      IF I_DIAS_ENTRE <= 0 THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'Los dias entre cuotas debe ser un numero > que cero!');
      END IF;
    END IF;

    APEX_COLLECTION.CREATE_OR_TRUNCATE_COLLECTION(P_COLLECTION_NAME => 'FACI036_CUOTA');
    DECLARE
      --I             NUMBER;
      V_FECHA       DATE := I_FIRTS_VTO;
      V_CUO_IMP     NUMBER := ROUND(I_IMPORTE / I_CANT_CUOTA, V_MON_IMP_DEC);
      V_TOT_CUO_IMP NUMBER := 0;
    BEGIN
      FOR I IN 1 .. I_CANT_CUOTA LOOP
        IF I = I_CANT_CUOTA THEN
          V_CUO_IMP := (I_IMPORTE - V_TOT_CUO_IMP);
        END IF;

        APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'FACI036_CUOTA',
                                   P_C001            => V_FECHA,
                                   P_C002            => V_CUO_IMP);

        -- :BCUO_DET.CUO_FEC_VTO   := V_FECHA;
        IF I_TIPO_VENCIMIENTO = 'V' THEN
          V_FECHA := V_FECHA + I_DIAS_ENTRE;
        ELSE
          V_FECHA := ADD_MONTHS(I_FIRTS_VTO, I);
        END IF;
        --   :BCUO_DET.CUO_IMP_MON := V_CUO_IMP;

        V_TOT_CUO_IMP := V_TOT_CUO_IMP + V_CUO_IMP;
      END LOOP;

      --AJUSTAR LA DIFERENCIA POR REDONDEO A LA ULTIMA CUOTA;
      /**/

    END;

  END PP_GENERAR_CUOTA;

  PROCEDURE PP_ACTUALIZAR_CUOTAS IS
    V_FCUO FIN_CUOTA%ROWTYPE;
  BEGIN

    -- ASIGNAR DATOS A LOS DEMAS ELEMENTOS DEL BLOQUE
    FOR C IN (SELECT TO_DATE(C.C001, 'DD/MM/YYYY') FEC_VTO,
                     TO_NUMBER(C.C002) IMP_CUOTA

                FROM APEX_COLLECTIONS C
               WHERE C.COLLECTION_NAME = 'FACI036_CUOTA') LOOP
      V_FCUO.CUO_CLAVE_DOC := TFIN_DOCUMENTO.DOC_CLAVE;
      V_FCUO.CUO_EMPR      := TFIN_DOCUMENTO.DOC_EMPR;
      V_FCUO.CUO_FEC_VTO   := C.FEC_VTO;
      V_FCUO.CUO_IMP_MON   := C.IMP_CUOTA;

      IF TFIN_DOCUMENTO.DOC_MON = PARAMETER.P_MON_LOC THEN
        V_FCUO.CUO_IMP_LOC := V_FCUO.CUO_IMP_MON;
      ELSE
        V_FCUO.CUO_IMP_LOC := ROUND(V_FCUO.CUO_IMP_MON *
                                    TFIN_DOCUMENTO.DOC_TASA,
                                    G_MON.MON_DEC_IMP);
      END IF;
      INSERT INTO FIN_CUOTA VALUES V_FCUO;
    END LOOP;

  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20010, 'error cuotas');
  END PP_ACTUALIZAR_CUOTAS;

  PROCEDURE PP_FAC_DOCUMENTO_DET IS
    BDOC_DET FAC_DOCUMENTO_DET%ROWTYPE;
  BEGIN

    FOR C IN (SELECT A.SEQ_ID ITEM,
                     A.N001 ART,
                     A.N002 CANTIDAD,
                     A.N003 PRECIO,
                     ROUND((C020), G_MON.MON_DEC_IMP) DOC_IVA_MON,
                     ROUND((C021), G_MON.MON_DEC_IMP) DOC_IVA_LOC,
                     ROUND((C022), G_MON.MON_DEC_IMP) DOC_EXENTA_MON,
                     ROUND((C023), G_MON.MON_DEC_IMP) DOC_EXENTA_LOC,
                     ROUND((C024) + (C026), G_MON.MON_DEC_IMP) DOC_GRAV_MON,
                     ROUND((C025) + (C027), G_MON.MON_DEC_IMP) DOC_GRAV_LOC,
                     TO_NUMBER(C013) ART_IMPU,
                     C010 MOTIVO_DEVOLUCION
                FROM APEX_COLLECTIONS A
               WHERE A.COLLECTION_NAME = 'FACI036_DET')

     LOOP

      BDOC_DET.DET_CLAVE_DOC     := TFIN_DOCUMENTO.DOC_CLAVE;
      BDOC_DET.DET_EMPR          := TFIN_DOCUMENTO.DOC_EMPR;
      BDOC_DET.DET_NRO_ITEM      := C.ITEM;
      BDOC_DET.DET_ART           := C.ART;
      BDOC_DET.DET_CANT          := C.CANTIDAD;
      BDOC_DET.DET_TIPO_COMISION := 'N';
      BDOC_DET.DET_PORC_DTO      := 0;
      BDOC_DET.DET_PORC_COMISION := 0;
      BDOC_DET.DET_NRO_REMISION  := TFIN_DOCUMENTO.DOC_REMISION;
      BDOC_DET.DET_PRECIO_UNIT   := C.PRECIO;
      BDOC_DET.DET_IVA_MON       := C.DOC_IVA_MON;
      BDOC_DET.DET_IVA_LOC       := C.DOC_IVA_LOC;
      BDOC_DET.DET_COD_IVA       := C.ART_IMPU;
      BDOC_DET.DET_BRUTO_MON     := NVL(C.DOC_GRAV_MON, 0) +
                                    NVL(C.DOC_EXENTA_MON, 0);
      BDOC_DET.DET_BRUTO_LOC     := NVL(C.DOC_GRAV_LOC, 0) +
                                    NVL(C.DOC_EXENTA_LOC, 0);
      BDOC_DET.DET_NETO_MON      := NVL(C.DOC_GRAV_MON, 0) +
                                    NVL(C.DOC_EXENTA_MON, 0);
      BDOC_DET.DET_NETO_LOC      := NVL(C.DOC_GRAV_LOC, 0) +
                                    NVL(C.DOC_EXENTA_LOC, 0);

      COM_DOC_RECIBIDOS_TABLAS.INSERTAR_FAC_DOC_DET(V_TABLA             => PARAMETER.P_TIPO_TABLA,
                                                    V_DET_CLAVE_DOC     => BDOC_DET.DET_CLAVE_DOC,
                                                    V_DET_NRO_ITEM      => BDOC_DET.DET_NRO_ITEM,
                                                    V_DET_ART           => BDOC_DET.DET_ART,
                                                    V_DET_NRO_REMIS     => BDOC_DET.DET_NRO_REMIS,
                                                    V_DET_CANT          => BDOC_DET.DET_CANT,
                                                    V_DET_BRUTO_LOC     => BDOC_DET.DET_BRUTO_LOC,
                                                    V_DET_BRUTO_MON     => BDOC_DET.DET_BRUTO_MON,
                                                    V_DET_COD_IVA       => BDOC_DET.DET_COD_IVA,
                                                    V_DET_IVA_LOC       => BDOC_DET.DET_IVA_LOC,
                                                    V_DET_IVA_MON       => BDOC_DET.DET_IVA_MON,
                                                    V_DET_PORC_DTO      => BDOC_DET.DET_PORC_DTO,
                                                    V_DET_NETO_LOC      => BDOC_DET.DET_NETO_LOC,
                                                    V_DET_NETO_MON      => BDOC_DET.DET_NETO_MON,
                                                    V_DET_TIPO_COMISION => BDOC_DET.DET_TIPO_COMISION,
                                                    V_DET_EMPR          => BDOC_DET.DET_EMPR);

    END LOOP;
  END PP_FAC_DOCUMENTO_DET;

  PROCEDURE PP_STK_DOCUMENTO_DET IS
    BDETA_STK STK_DOCUMENTO_DET%ROWTYPE;
    V_IMPU    VARCHAR2(5) := ' ';

  BEGIN

    FOR C IN (SELECT A.SEQ_ID ITEM,
                     A.N001 ART,
                     A.N002 CANTIDAD,
                     A.N003 PRECIO,
                     ROUND((C020), G_MON.MON_DEC_IMP) DOC_IVA_MON,
                     ROUND((C021), G_MON.MON_DEC_IMP) DOC_IVA_LOC,
                     ROUND((C022), G_MON.MON_DEC_IMP) DOC_EXENTA_MON,
                     ROUND((C023), G_MON.MON_DEC_IMP) DOC_EXENTA_LOC,
                     ROUND((C024) + (C026), G_MON.MON_DEC_IMP) DOC_GRAV_MON,
                     ROUND((C025) + (C027), G_MON.MON_DEC_IMP) DOC_GRAV_LOC,
                     TO_NUMBER(C013) ART_IMPU,
                     C010 MOTIVO_DEVOLUCION
                FROM APEX_COLLECTIONS A
               WHERE A.COLLECTION_NAME = 'FACI036_DET') LOOP

      BDETA_STK.DETA_CLAVE_DOC     := TSTK_DOCUMENTO.DOCU_CLAVE;
      BDETA_STK.DETA_EMPR          := TSTK_DOCUMENTO.DOCU_EMPR;
      BDETA_STK.DETA_NRO_ITEM      := C.ITEM;
      BDETA_STK.DETA_ART           := C.ART;
      BDETA_STK.DETA_NRO_REM       := NULL;
      BDETA_STK.DETA_CANT          := C.CANTIDAD;
      BDETA_STK.DETA_IMP_BRUTO_LOC := NVL(C.DOC_GRAV_LOC, 0) +
                                      NVL(C.DOC_EXENTA_LOC, 0);
      BDETA_STK.DETA_IMP_BRUTO_MON := NVL(C.DOC_GRAV_MON, 0) +
                                      NVL(C.DOC_EXENTA_MON, 0);
      BDETA_STK.DETA_IMP_NETO_MON  := NVL(C.DOC_GRAV_MON, 0) +
                                      NVL(C.DOC_EXENTA_MON, 0);
      BDETA_STK.DETA_IMP_NETO_LOC  := NVL(C.DOC_GRAV_LOC, 0) +
                                      NVL(C.DOC_EXENTA_LOC, 0);
      BDETA_STK.DETA_IVA_LOC       := C.DOC_IVA_LOC;
      BDETA_STK.DETA_IVA_MON       := C.DOC_IVA_MON;
      BDETA_STK.DETA_PORC_COMISION := 0;
      BDETA_STK.DETA_DESC_POR_CANT := 0;
      BDETA_STK.DETA_MOT_NCR       := C.MOTIVO_DEVOLUCION;

      IF NVL(C.ART_IMPU, 0) = 1 THEN
        V_IMPU := 'N';
      ELSE
        V_IMPU := 'S';
      END IF;

      BDETA_STK.DETA_IMPU := V_IMPU;

      COM_DOC_RECIBIDOS_TABLAS.INSERTAR_DOC_DET_STK(V_TABLA              => PARAMETER.P_TIPO_TABLA,
                                                    V_DETA_CLAVE_DOC     => BDETA_STK.DETA_CLAVE_DOC,
                                                    V_DETA_NRO_ITEM      => BDETA_STK.DETA_NRO_ITEM,
                                                    V_DETA_ART           => BDETA_STK.DETA_ART,
                                                    V_DETA_EMPR          => BDETA_STK.DETA_EMPR,
                                                    V_DETA_NRO_REM       => BDETA_STK.DETA_NRO_REM,
                                                    V_DETA_CANT          => BDETA_STK.DETA_CANT,
                                                    V_DETA_IMP_NETO_LOC  => BDETA_STK.DETA_IMP_NETO_LOC,
                                                    V_DETA_IMP_NETO_MON  => BDETA_STK.DETA_IMP_NETO_MON,
                                                    V_DETA_IMPU          => BDETA_STK.DETA_IMPU,
                                                    V_DETA_IVA_LOC       => BDETA_STK.DETA_IVA_LOC,
                                                    V_DETA_IVA_MON       => BDETA_STK.DETA_IVA_MON,
                                                    V_DETA_PORC_DTO      => BDETA_STK.DETA_PORC_DTO,
                                                    V_DETA_IMP_BRUTO_LOC => BDETA_STK.DETA_IMP_BRUTO_LOC,
                                                    V_DETA_IMP_BRUTO_MON => BDETA_STK.DETA_IMP_BRUTO_MON,
                                                    V_DETA_MOT_NCR       => BDETA_STK.DETA_MOT_NCR);

    END LOOP;
  END PP_STK_DOCUMENTO_DET;

  PROCEDURE PP_VALIDAR_TOTAL_CUOTAS IS
    V_IMPORTE_FACT NUMBER;
    V_IMPORTE_CUO  NUMBER;
  BEGIN
    SELECT NVL(SUM(C020) + SUM(C022) + SUM(C024) + SUM(C026), 0) NUMERO
      INTO V_IMPORTE_FACT
      FROM APEX_COLLECTIONS C
     WHERE C.COLLECTION_NAME = 'FACI036_DET';

    SELECT NVL(SUM(C.C002), 0) NUMERO
      INTO V_IMPORTE_CUO
      FROM APEX_COLLECTIONS C
     WHERE C.COLLECTION_NAME = 'FACI036_CUOTA';
    IF V_IMPORTE_FACT <> V_IMPORTE_CUO THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'Error la cuota no coincide con el documento');
    END IF;
  END PP_VALIDAR_TOTAL_CUOTAS;

  PROCEDURE PP_ESTABL_VARIABLES_BDOCU_STK(I_EMPRESA             IN NUMBER,
                                          I_SUCURSAL            IN NUMBER,
                                          I_S_CREDITO           IN VARCHAR2,
                                          I_DOC_TASA_US         IN NUMBER,
                                          I_DOCU_LEGAJO         IN NUMBER,
                                          I_DOCU_NRO_DOC        IN NUMBER,
                                          I_DOCU_DEP_ORIG       IN NUMBER,
                                          I_DOCU_MON            IN NUMBER,
                                          I_DOCU_CLI            IN NUMBER,
                                          I_DOCU_CLI_NOM        IN VARCHAR2,
                                          I_DOCU_CLI_RUC        IN VARCHAR2,
                                          I_DOCU_FEC_EMIS       IN DATE,
                                          I_DOCU_FEC_OPER       IN DATE,
                                          I_DOCU_OBS            IN VARCHAR2,
                                          I_DOCU_AFECT_COMISION IN VARCHAR2,
                                          I_DOCU_OCARGA_LONDON  IN NUMBER,
                                          I_DOCU_CLAVE_STOPES   IN NUMBER,
                                          I_DOCU_TIMBRADO       IN NUMBER) IS
    --VAL VARCHAR2(80);
    V_TIPO_DOC_PROV_CLI NUMBER;
  BEGIN
    NULL;


      IF TSTK_DOCUMENTO.DOCU_CLI IS NOT NULL  AND I_EMPRESA IN (1,2) THEN-------------LV
             BEGIN
               SELECT CLI_DOC_TIPO
                 INTO V_TIPO_DOC_PROV_CLI
                 FROM FIN_CLIENTE
                WHERE CLI_EMPR = I_EMPRESA
                  AND CLI_CODIGO = TSTK_DOCUMENTO.DOCU_CLI;

                  IF V_TIPO_DOC_PROV_CLI IS NULL THEN
                     RAISE_APPLICATION_ERROR (-20001,'!FAVOR, CONFIGURAR EL TIPO DE DOCUMENTO DEL CLIENTE ANTES DE CONTINUAR- PROG 2-1-10!');
                  END IF;
              EXCEPTION WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR (-20001,'!FAVOR, CONFIGURAR EL TIPO DE DOCUMENTO DEL CLIENTE ANTES DE CONTINUAR- PROG 2-1-10!');
              END;
      END IF;

    TSTK_DOCUMENTO.DOCU_CLAVE := STK_SEQ_DOCU_NEXTVAL;

    -- ASIGNAR VALORES A ALGUNOS CAMPOS DE BDOCU_STK
    TSTK_DOCUMENTO.DOCU_EMPR     := I_EMPRESA;
    TSTK_DOCUMENTO.DOCU_SUC_ORIG := I_SUCURSAL;
    TSTK_DOCUMENTO.DOCU_MON      := I_DOCU_MON;
    TSTK_DOCUMENTO.DOCU_DEP_ORIG := I_DOCU_DEP_ORIG;
    TSTK_DOCUMENTO.DOCU_FEC_EMIS := I_DOCU_FEC_EMIS;
    TSTK_DOCUMENTO.DOCU_FEC_OPER := I_DOCU_FEC_OPER;

    TSTK_DOCUMENTO.DOCU_AFECT_COMISION := I_DOCU_AFECT_COMISION;

    TSTK_DOCUMENTO.DOCU_LEGAJO           := I_DOCU_LEGAJO;
    TSTK_DOCUMENTO.DOCU_NRO_DOC          := I_DOCU_NRO_DOC;
    TSTK_DOCUMENTO.DOCU_TIPO_MOV         := 16;
    TSTK_DOCUMENTO.DOCU_OBS              := I_DOCU_OBS;
    TSTK_DOCUMENTO.DOCU_CODIGO_OPER      := FP_BUSCAR_COD_OP(I_EMPRESA   => I_EMPRESA,
                                                             I_DESC_OPER => 'DEV_VENTA');
    TSTK_DOCUMENTO.DOCU_FEC_GRAB         := SYSDATE;
    TSTK_DOCUMENTO.DOCU_LOGIN            := GEN_DEVUELVE_USER;
    TSTK_DOCUMENTO.DOCU_SIST             := SUBSTR(PARAMETER.P_PROGRAMA,
                                                   1,
                                                   3);
    TSTK_DOCUMENTO.DOCU_IND_CONSIGNACION := NULL;
    TSTK_DOCUMENTO.DOCU_SERIE            := 'C';
    TSTK_DOCUMENTO.DOCU_OPERADOR         := 2;
    TSTK_DOCUMENTO.DOCU_AFECT_COMISION   := I_DOCU_AFECT_COMISION;
    TSTK_DOCUMENTO.DOCU_CLAVE_STOPES     := I_DOCU_CLAVE_STOPES;
    TSTK_DOCUMENTO.DOCU_TIMBRADO         := I_DOCU_TIMBRADO;
    TSTK_DOCUMENTO.DOCU_CLI              := I_DOCU_CLI;
    TSTK_DOCUMENTO.DOCU_CLI_NOM          := I_DOCU_CLI_NOM;
    TSTK_DOCUMENTO.DOCU_CLI_RUC          := I_DOCU_CLI_RUC;
    TSTK_DOCUMENTO.DOCU_TASA_US          := I_DOC_TASA_US;

    TSTK_DOCUMENTO.DOCU_OCARGA_LONDON := I_DOCU_OCARGA_LONDON;

   TSTK_DOCUMENTO.DOCU_TIPO_DOC_CLI_PROV :=V_TIPO_DOC_PROV_CLI;---LV
   TSTK_DOCUMENTO.DOCU_TIPO_FACTURA := 1;



    IF I_S_CREDITO = 'S' THEN
      TSTK_DOCUMENTO.DOCU_IND_CUOTA := 'S';
    END IF;

    --HALLAR TOTALES BRUTO
    TSTK_DOCUMENTO.DOCU_GRAV_BRUTO_LOC := 0;
    TSTK_DOCUMENTO.DOCU_GRAV_BRUTO_MON := 0;
    TSTK_DOCUMENTO.DOCU_EXEN_BRUTO_LOC := 0;
    TSTK_DOCUMENTO.DOCU_EXEN_BRUTO_MON := 0;

    SELECT ROUND(SUM(C020), G_MON.MON_DEC_IMP) IVA_MON,
           ROUND(SUM(C021), G_MON.MON_DEC_IMP) IVA_LOC,
           ROUND(SUM(C022), G_MON.MON_DEC_IMP) EXENTA_MON,
           ROUND(SUM(C023), G_MON.MON_DEC_IMP) EXENTA_LOC,
           ROUND(SUM(C024) + SUM(C026), G_MON.MON_DEC_IMP) GRAVADA_MON,
           ROUND(SUM(C025) + SUM(C027), G_MON.MON_DEC_IMP) GRAVADA_LOC
    /*ROUND(SUM(C024), G_MON.MON_DEC_IMP) GRA5_MON,
    ROUND(SUM(C025), G_MON.MON_DEC_IMP) GRA5_LOC,
    ROUND(SUM(C026), G_MON.MON_DEC_IMP) GRA10_MON,
    ROUND(SUM(C027), G_MON.MON_DEC_IMP) GRA10_LOC,
    ROUND(SUM(C020) + SUM(C022) + SUM(C024) + SUM(C026),
          G_MON.MON_DEC_IMP) TOT_MON,
    ROUND(SUM(C021) + SUM(C023) + SUM(C025) + SUM(C027),
          G_MON.MON_DEC_IMP) TOT_LOC*/
      INTO TSTK_DOCUMENTO.DOCU_IVA_MON,
           TSTK_DOCUMENTO.DOCU_IVA_LOC,
           TSTK_DOCUMENTO.DOCU_EXEN_BRUTO_MON,
           TSTK_DOCUMENTO.DOCU_EXEN_BRUTO_LOC,
           TSTK_DOCUMENTO.DOCU_GRAV_BRUTO_MON,
           TSTK_DOCUMENTO.DOCU_GRAV_BRUTO_LOC
      FROM APEX_COLLECTIONS A
     WHERE A.COLLECTION_NAME = 'FACI036_DET';

    TSTK_DOCUMENTO.DOCU_EXEN_NETO_MON := TSTK_DOCUMENTO.DOCU_EXEN_BRUTO_MON;
    TSTK_DOCUMENTO.DOCU_EXEN_NETO_LOC := TSTK_DOCUMENTO.DOCU_EXEN_BRUTO_LOC;
    TSTK_DOCUMENTO.DOCU_GRAV_NETO_MON := TSTK_DOCUMENTO.DOCU_GRAV_BRUTO_MON;
    TSTK_DOCUMENTO.DOCU_GRAV_NETO_LOC := TSTK_DOCUMENTO.DOCU_GRAV_BRUTO_LOC;

  END PP_ESTABL_VARIABLES_BDOCU_STK;

  PROCEDURE PP_ACTUALIZAR_DOCUMENTO_FIN(I_DOCU_CLAVE_FAC_NCR IN NUMBER,
                                        I_DOC_CTA_BCO        IN NUMBER,
                                        I_S_CREDITO          IN VARCHAR2) IS
    V_DOCU_TOT_MON NUMBER;
    V_DOCU_TOT_LOC NUMBER;
  BEGIN
    NULL;

    IF TSTK_DOCUMENTO.DOCU_LEGAJO IS NULL THEN
      RAISE_APPLICATION_ERROR(-20010, 'Vendedor no puede ser nulo!');
    END IF;

    IF TFIN_DOCUMENTO.DOC_CLAVE IS NULL THEN
      TFIN_DOCUMENTO.DOC_CLAVE := FP_FIN_SEQ_DOC;
    END IF;

    TFIN_DOCUMENTO.DOC_EMPR := TSTK_DOCUMENTO.DOCU_EMPR;

    TFIN_DOCUMENTO.DOC_SUC            := TSTK_DOCUMENTO.DOCU_SUC_ORIG;
    TFIN_DOCUMENTO.DOC_NRO_DOC        := TSTK_DOCUMENTO.DOCU_NRO_DOC;
    TFIN_DOCUMENTO.DOC_MON            := TSTK_DOCUMENTO.DOCU_MON;
    TFIN_DOCUMENTO.DOC_PROV           := TSTK_DOCUMENTO.DOCU_PROV;
    TFIN_DOCUMENTO.DOC_CLI            := TSTK_DOCUMENTO.DOCU_CLI;
    TFIN_DOCUMENTO.DOC_LEGAJO         := TSTK_DOCUMENTO.DOCU_LEGAJO;
    TFIN_DOCUMENTO.DOC_CLI_NOM        := TSTK_DOCUMENTO.DOCU_CLI_NOM;
    TFIN_DOCUMENTO.DOC_CLI_RUC        := TSTK_DOCUMENTO.DOCU_CLI_RUC;
    TFIN_DOCUMENTO.DOC_FEC_OPER       := TSTK_DOCUMENTO.DOCU_FEC_OPER;
    TFIN_DOCUMENTO.DOC_SERIE          := TSTK_DOCUMENTO.DOCU_SERIE;
    TFIN_DOCUMENTO.DOC_TIMBRADO       := TSTK_DOCUMENTO.DOCU_TIMBRADO;
    TFIN_DOCUMENTO.DOC_FEC_DOC        := TSTK_DOCUMENTO.DOCU_FEC_EMIS;
    TFIN_DOCUMENTO.DOC_BRUTO_GRAV_LOC := TSTK_DOCUMENTO.DOCU_GRAV_BRUTO_LOC;
    TFIN_DOCUMENTO.DOC_BRUTO_GRAV_MON := TSTK_DOCUMENTO.DOCU_GRAV_BRUTO_MON;
    TFIN_DOCUMENTO.DOC_BRUTO_EXEN_LOC := TSTK_DOCUMENTO.DOCU_EXEN_BRUTO_LOC;
    TFIN_DOCUMENTO.DOC_BRUTO_EXEN_MON := TSTK_DOCUMENTO.DOCU_EXEN_BRUTO_MON;
    TFIN_DOCUMENTO.DOC_NETO_GRAV_LOC  := TSTK_DOCUMENTO.DOCU_GRAV_NETO_LOC;
    TFIN_DOCUMENTO.DOC_NETO_GRAV_MON  := TSTK_DOCUMENTO.DOCU_GRAV_NETO_MON;
    TFIN_DOCUMENTO.DOC_NETO_EXEN_LOC  := TSTK_DOCUMENTO.DOCU_EXEN_NETO_LOC;
    TFIN_DOCUMENTO.DOC_NETO_EXEN_MON  := TSTK_DOCUMENTO.DOCU_EXEN_NETO_MON;
    TFIN_DOCUMENTO.DOC_IVA_LOC        := TSTK_DOCUMENTO.DOCU_IVA_LOC;
    TFIN_DOCUMENTO.DOC_IVA_MON        := TSTK_DOCUMENTO.DOCU_IVA_MON;
    TFIN_DOCUMENTO.DOC_OBS            := TSTK_DOCUMENTO.DOCU_OBS;
    TFIN_DOCUMENTO.DOC_IND_STK        := 'S';
    TFIN_DOCUMENTO.DOC_CLAVE_STK      := TSTK_DOCUMENTO.DOCU_CLAVE;
    TFIN_DOCUMENTO.DOC_TIPO_MOV       := TSTK_DOCUMENTO.DOCU_TIPO_MOV;

    TFIN_DOCUMENTO.DOC_TIPO_DOC_CLI_PROV  := TSTK_DOCUMENTO.DOCU_TIPO_DOC_CLI_PROV;
    TFIN_DOCUMENTO.DOC_TIPO_FACTURA       := TSTK_DOCUMENTO.DOCU_TIPO_FACTURA;

    IF TFIN_DOCUMENTO.DOC_CLAVE_STK IS NULL THEN
      --CON ESTE MENSAJE SE ASEGURA QUE SE GUARDE UNA CLAVE DE STOCK
      RAISE_APPLICATION_ERROR(-20010,
                              'No puede quedar en blanco doc_clave_stk!.');
    END IF;

    TFIN_DOCUMENTO.DOC_IND_CONSIGNACION := TSTK_DOCUMENTO.DOCU_IND_CONSIGNACION;
    TFIN_DOCUMENTO.DOC_LOGIN            := TSTK_DOCUMENTO.DOCU_LOGIN;
    TFIN_DOCUMENTO.DOC_FEC_GRAB         := TSTK_DOCUMENTO.DOCU_FEC_GRAB;
    TFIN_DOCUMENTO.DOC_SIST             := TSTK_DOCUMENTO.DOCU_SIST;
    TFIN_DOCUMENTO.DOC_OPERADOR         := TSTK_DOCUMENTO.DOCU_OPERADOR;
    --*
    V_DOCU_TOT_MON := TSTK_DOCUMENTO.DOCU_EXEN_NETO_MON +
                      TSTK_DOCUMENTO.DOCU_GRAV_NETO_MON +
                      TSTK_DOCUMENTO.DOCU_IVA_MON;
    V_DOCU_TOT_LOC := TSTK_DOCUMENTO.DOCU_EXEN_NETO_LOC +
                      TSTK_DOCUMENTO.DOCU_GRAV_NETO_LOC +
                      TSTK_DOCUMENTO.DOCU_IVA_LOC;

    -- RAISE_APPLICATION_ERROR(-20010,I_S_CREDITO);
    IF I_S_CREDITO = 'N' THEN
      -----* 24/01/2001 DOC_CTA_BCO SE PUEDE ELEGIR DESDE AHORA DESDE LA PANTALLA
      -----* POR ESTA RAZON NO ES NECESARIO ESTA FUNCION QUE ASIGNA CODIGO DE LA
      -----* CUENTA/BANCO.
      IF TSTK_DOCUMENTO.DOCU_SUC_ORIG = 1 AND TSTK_DOCUMENTO.DOCU_EMPR = 1 THEN
        TFIN_DOCUMENTO.DOC_CTA_BCO_NCR := I_DOC_CTA_BCO;
        TFIN_DOCUMENTO.DOC_CTA_BCO     := 119;
      ELSE
        TFIN_DOCUMENTO.DOC_CTA_BCO := I_DOC_CTA_BCO;
      END IF;

      TFIN_DOCUMENTO.DOC_IND_CUOTA         := NULL;
      TFIN_DOCUMENTO.DOC_SALDO_LOC         := 0;
      TFIN_DOCUMENTO.DOC_SALDO_MON         := 0;
      TFIN_DOCUMENTO.DOC_SALDO_PER_ACT_LOC := 0;
      TFIN_DOCUMENTO.DOC_SALDO_PER_ACT_MON := 0;
    ELSE
      TFIN_DOCUMENTO.DOC_CTA_BCO   := NULL;
      TFIN_DOCUMENTO.DOC_IND_CUOTA := 'S';
      TFIN_DOCUMENTO.DOC_SALDO_LOC := V_DOCU_TOT_LOC;
      TFIN_DOCUMENTO.DOC_SALDO_MON := V_DOCU_TOT_MON;

      IF TFIN_DOCUMENTO.DOC_FEC_DOC BETWEEN BCONF_FIN.CONF_PER_ACT_INI AND
         BCONF_FIN.CONF_PER_ACT_FIN THEN
        TFIN_DOCUMENTO.DOC_SALDO_PER_ACT_LOC := TFIN_DOCUMENTO.DOC_SALDO_LOC;
        TFIN_DOCUMENTO.DOC_SALDO_PER_ACT_MON := TFIN_DOCUMENTO.DOC_SALDO_MON;
      ELSE
        TFIN_DOCUMENTO.DOC_SALDO_PER_ACT_LOC := 0;
        TFIN_DOCUMENTO.DOC_SALDO_PER_ACT_MON := 0;
      END IF;
    END IF;
    --*
    -- TFIN_DOCUMENTO.DOC_SNC_CLAVE := TSTK_DOCUMENTO.NRO_SNC;
    BEGIN
      SELECT TMOV_TIPO
        INTO TFIN_DOCUMENTO.DOC_TIPO_SALDO
        FROM GEN_TIPO_MOV
       WHERE TMOV_CODIGO = TSTK_DOCUMENTO.DOCU_TIPO_MOV
         AND TMOV_EMPR = TSTK_DOCUMENTO.DOCU_EMPR;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'Tipo de documento inexistente: ' ||
                                TO_CHAR(TSTK_DOCUMENTO.DOCU_TIPO_MOV));
    END;

    ----
    TFIN_DOCUMENTO.DOC_CLAVE_FAC_NCR := I_DOCU_CLAVE_FAC_NCR;
    ----
    BEGIN
      -- CALL THE PROCEDURE
      COM_DOC_RECIBIDOS_TABLAS.INSERTAR_DOCUMENTO_STK(V_TABLA                 => PARAMETER.P_TIPO_TABLA,
                                                      V_DOCU_CLAVE            => TSTK_DOCUMENTO.DOCU_CLAVE,
                                                      V_DOCU_EMPR             => TSTK_DOCUMENTO.DOCU_EMPR,
                                                      V_DOCU_CODIGO_OPER      => TSTK_DOCUMENTO.DOCU_CODIGO_OPER,
                                                      V_DOCU_NRO_DOC          => TSTK_DOCUMENTO.DOCU_NRO_DOC,
                                                      V_DOCU_SUC_ORIG         => TSTK_DOCUMENTO.DOCU_SUC_ORIG,
                                                      V_DOCU_DEP_ORIG         => TSTK_DOCUMENTO.DOCU_DEP_ORIG,
                                                      V_DOCU_SUC_DEST         => TSTK_DOCUMENTO.DOCU_SUC_DEST,
                                                      V_DOCU_DEP_DEST         => TSTK_DOCUMENTO.DOCU_DEP_DEST,
                                                      V_DOCU_MON              => TSTK_DOCUMENTO.DOCU_MON,
                                                      V_DOCU_PROV             => TSTK_DOCUMENTO.DOCU_PROV,
                                                      V_DOCU_CLI              => TSTK_DOCUMENTO.DOCU_CLI,
                                                      V_DOCU_CLI_NOM          => TSTK_DOCUMENTO.DOCU_CLI_NOM,
                                                      V_DOCU_CLI_RUC          => TSTK_DOCUMENTO.DOCU_CLI_RUC,
                                                      V_DOCU_LEGAJO           => TSTK_DOCUMENTO.DOCU_LEGAJO,
                                                      V_DOCU_FEC_EMIS         => TSTK_DOCUMENTO.DOCU_FEC_EMIS,
                                                      V_DOCU_TIPO_MOV         => TSTK_DOCUMENTO.DOCU_TIPO_MOV,
                                                      V_DOCU_GRAV_NETO_LOC    => TSTK_DOCUMENTO.DOCU_GRAV_NETO_LOC,
                                                      V_DOCU_GRAV_NETO_MON    => TSTK_DOCUMENTO.DOCU_GRAV_NETO_MON,
                                                      V_DOCU_EXEN_NETO_LOC    => TSTK_DOCUMENTO.DOCU_EXEN_NETO_LOC,
                                                      V_DOCU_EXEN_NETO_MON    => TSTK_DOCUMENTO.DOCU_EXEN_NETO_MON,
                                                      V_DOCU_GRAV_BRUTO_LOC   => TSTK_DOCUMENTO.DOCU_GRAV_BRUTO_LOC,
                                                      V_DOCU_GRAV_BRUTO_MON   => TSTK_DOCUMENTO.DOCU_GRAV_BRUTO_MON,
                                                      V_DOCU_EXEN_BRUTO_LOC   => TSTK_DOCUMENTO.DOCU_EXEN_BRUTO_LOC,
                                                      V_DOCU_EXEN_BRUTO_MON   => TSTK_DOCUMENTO.DOCU_EXEN_BRUTO_MON,
                                                      V_DOCU_IVA_LOC          => TSTK_DOCUMENTO.DOCU_IVA_LOC,
                                                      V_DOCU_IVA_MON          => TSTK_DOCUMENTO.DOCU_IVA_MON,
                                                      V_DOCU_TASA_US          => TSTK_DOCUMENTO.DOCU_TASA_US,
                                                      V_DOCU_IND_CUOTA        => TSTK_DOCUMENTO.DOCU_IND_CUOTA,
                                                      V_DOCU_IND_CONSIGNACION => TSTK_DOCUMENTO.DOCU_IND_CONSIGNACION,
                                                      V_DOCU_OBS              => TSTK_DOCUMENTO.DOCU_OBS,
                                                      V_DOCU_LOGIN            => TSTK_DOCUMENTO.DOCU_LOGIN,
                                                      V_DOCU_FEC_GRAB         => TSTK_DOCUMENTO.DOCU_FEC_GRAB,
                                                      V_DOCU_SIST             => TSTK_DOCUMENTO.DOCU_SIST,
                                                      V_DOCU_OPERADOR         => TSTK_DOCUMENTO.DOCU_OPERADOR,
                                                      V_DOCU_SERIE            => TSTK_DOCUMENTO.DOCU_SERIE,
                                                      V_DOCU_OCARGA_LONDON    => TSTK_DOCUMENTO.DOCU_OCARGA_LONDON,
                                                      V_DOCU_CLAVE_STOPES     => TSTK_DOCUMENTO.DOCU_CLAVE_STOPES,
                                                      V_DOCU_FEC_OPER         => TSTK_DOCUMENTO.DOCU_FEC_OPER,
                                                      V_DOCU_AFECT_COMISION   => TSTK_DOCUMENTO.DOCU_AFECT_COMISION,
                                                      V_TIPO_DOC_PRO_CLI      => TSTK_DOCUMENTO.DOCU_TIPO_DOC_CLI_PROV,
                                                      V_TIPO_FACTURA          => TSTK_DOCUMENTO.DOCU_TIPO_FACTURA);
    END;

    BEGIN
      -- CALL THE PROCEDURE
      COM_DOC_RECIBIDOS_TABLAS.INSERTAR_DOCUMENTO_FIN(V_TABLA                => PARAMETER.P_TIPO_TABLA,
                                                      V_DOC_CLAVE            => TFIN_DOCUMENTO.DOC_CLAVE,
                                                      V_DOC_EMPR             => TFIN_DOCUMENTO.DOC_EMPR,
                                                      V_DOC_CTA_BCO          => TFIN_DOCUMENTO.DOC_CTA_BCO,
                                                      V_DOC_SUC              => TFIN_DOCUMENTO.DOC_SUC,
                                                      V_DOC_TIPO_MOV         => TFIN_DOCUMENTO.DOC_TIPO_MOV,
                                                      V_DOC_NRO_DOC          => TFIN_DOCUMENTO.DOC_NRO_DOC,
                                                      V_DOC_TIPO_SALDO       => TFIN_DOCUMENTO.DOC_TIPO_SALDO,
                                                      V_DOC_MON              => TFIN_DOCUMENTO.DOC_MON,
                                                      V_DOC_PROV             => TFIN_DOCUMENTO.DOC_PROV,
                                                      V_DOC_CLI              => TFIN_DOCUMENTO.DOC_CLI,
                                                      V_DOC_CLI_NOM          => TFIN_DOCUMENTO.DOC_CLI_NOM,
                                                      V_DOC_CLI_DIR          => TFIN_DOCUMENTO.DOC_CLI_DIR,
                                                      V_DOC_CLI_RUC          => TFIN_DOCUMENTO.DOC_CLI_RUC,
                                                      V_DOC_CLI_TEL          => TFIN_DOCUMENTO.DOC_CLI_TEL,
                                                      V_DOC_LEGAJO           => TFIN_DOCUMENTO.DOC_LEGAJO,
                                                      V_DOC_BCO_CHEQUE       => TFIN_DOCUMENTO.DOC_BCO_CHEQUE,
                                                      V_DOC_FEC_CHEQUE       => TFIN_DOCUMENTO.DOC_FEC_CHEQUE,
                                                      V_DOC_FEC_OPER         => TFIN_DOCUMENTO.DOC_FEC_OPER,
                                                      V_DOC_FEC_DOC          => TFIN_DOCUMENTO.DOC_FEC_DOC,
                                                      V_DOC_BRUTO_EXEN_LOC   => TFIN_DOCUMENTO.DOC_BRUTO_EXEN_LOC,
                                                      V_DOC_BRUTO_EXEN_MON   => TFIN_DOCUMENTO.DOC_BRUTO_EXEN_MON,
                                                      V_DOC_BRUTO_GRAV_LOC   => TFIN_DOCUMENTO.DOC_BRUTO_GRAV_LOC,
                                                      V_DOC_BRUTO_GRAV_MON   => TFIN_DOCUMENTO.DOC_BRUTO_GRAV_MON,
                                                      V_DOC_NETO_EXEN_LOC    => TFIN_DOCUMENTO.DOC_NETO_EXEN_LOC,
                                                      V_DOC_NETO_EXEN_MON    => TFIN_DOCUMENTO.DOC_NETO_EXEN_MON,
                                                      V_DOC_NETO_GRAV_LOC    => TFIN_DOCUMENTO.DOC_NETO_GRAV_LOC,
                                                      V_DOC_NETO_GRAV_MON    => TFIN_DOCUMENTO.DOC_NETO_GRAV_MON,
                                                      V_DOC_IVA_LOC          => TFIN_DOCUMENTO.DOC_IVA_LOC,
                                                      V_DOC_IVA_MON          => TFIN_DOCUMENTO.DOC_IVA_MON,
                                                      V_DOC_OBS              => TFIN_DOCUMENTO.DOC_OBS,
                                                      V_DOC_BASE_IMPON_LOC   => TFIN_DOCUMENTO.DOC_BASE_IMPON_LOC,
                                                      V_DOC_BASE_IMPON_MON   => TFIN_DOCUMENTO.DOC_BASE_IMPON_MON,
                                                      V_DOC_IND_STK          => TFIN_DOCUMENTO.DOC_IND_STK,
                                                      V_DOC_CLAVE_STK        => TFIN_DOCUMENTO.DOC_CLAVE_STK,
                                                      V_DOC_IND_CUOTA        => TFIN_DOCUMENTO.DOC_IND_CUOTA,
                                                      V_DOC_IND_CONSIGNACION => TFIN_DOCUMENTO.DOC_IND_CONSIGNACION,
                                                      V_DOC_PLAN_FINAN       => TFIN_DOCUMENTO.DOC_PLAN_FINAN,
                                                      V_DOC_LOGIN            => TFIN_DOCUMENTO.DOC_LOGIN,
                                                      V_DOC_FEC_GRAB         => TFIN_DOCUMENTO.DOC_FEC_GRAB,
                                                      V_DOC_SIST             => TFIN_DOCUMENTO.DOC_SIST,
                                                      V_DOC_OPERADOR         => TFIN_DOCUMENTO.DOC_OPERADOR,
                                                      V_DOC_CANT_PAGARE      => TFIN_DOCUMENTO.DOC_CANT_PAGARE,
                                                      V_DOC_SERIE            => TFIN_DOCUMENTO.DOC_SERIE,
                                                      V_DOC_TIMBRADO         => TFIN_DOCUMENTO.DOC_TIMBRADO,
                                                      V_DOC_CANAL            => TFIN_DOCUMENTO.DOC_CANAL,
                                                      V_DOC_SISTEMA_AUX      => 'FACI036',
                                                      V_DOC_CLAVE_FAC_NCR    => TFIN_DOCUMENTO.DOC_CLAVE_FAC_NCR,
                                                      V_DOC_CTA_BCO_NCR      => TFIN_DOCUMENTO.DOC_CTA_BCO_NCR,
                                                      V_DOC_SNC_CLAVE        => TFIN_DOCUMENTO.DOC_SNC_CLAVE,
                                                      V_TIPO_DOC_PRO_CLI     => TFIN_DOCUMENTO.DOC_TIPO_DOC_CLI_PROV,
                                                      V_TIPO_FACTURA         => TFIN_DOCUMENTO.DOC_TIPO_FACTURA);
    END;

  END PP_ACTUALIZAR_DOCUMENTO_FIN;

  PROCEDURE PP_ACTUALIZAR_DOC_CONCEPTO IS
    V_ITEM           NUMBER := 0;
    V_CLAVE_CONCEPTO NUMBER := 0;
    V_CLAVE_CTACO    NUMBER := 0;
    V_DCON           FIN_DOC_CONCEPTO%ROWTYPE;

    --- PROCEDIMIENTO PARA AGREGAR CONCEPTOS EN CASO QUE LOS IMPORTES SEAN DISTINTOS DE CERO.
    PROCEDURE AGREGAR_CONCEPTO(IN_CONC     IN NUMBER,
                               IN_CTACO    IN NUMBER,
                               IN_GRAV_LOC IN NUMBER,
                               IN_GRAV_MON IN NUMBER,
                               IN_EXEN_LOC IN NUMBER,
                               IN_EXEN_MON IN NUMBER,
                               IN_IVA_LOC  IN NUMBER,
                               IN_IVA_MON  IN NUMBER) IS

    BEGIN
      IF (IN_GRAV_LOC + IN_EXEN_LOC + IN_IVA_LOC) > 0 THEN
        V_ITEM                     := V_ITEM + 1;
        V_DCON.DCON_CLAVE_DOC      := TFIN_DOCUMENTO.DOC_CLAVE;
        V_DCON.DCON_EMPR           := TFIN_DOCUMENTO.DOC_EMPR;
        V_DCON.DCON_ITEM           := V_ITEM;
        V_DCON.DCON_CLAVE_CONCEPTO := IN_CONC;
        V_DCON.DCON_TIPO_SALDO     := 'C';
        V_DCON.DCON_CLAVE_CTACO    := IN_CTACO;
        V_DCON.DCON_GRAV_LOC       := IN_GRAV_LOC;
        V_DCON.DCON_GRAV_MON       := IN_GRAV_MON;
        V_DCON.DCON_EXEN_LOC       := IN_EXEN_LOC;
        V_DCON.DCON_EXEN_MON       := IN_EXEN_MON;
        V_DCON.DCON_IVA_LOC        := IN_IVA_LOC;
        V_DCON.DCON_IVA_MON        := IN_IVA_MON;

        COM_DOC_RECIBIDOS_TABLAS.INSERTAR_DOC_CONCEPTO(V_TABLA               => PARAMETER.P_TIPO_TABLA,
                                                       V_DCON_CLAVE_DOC      => V_DCON.DCON_CLAVE_DOC,
                                                       V_DCON_ITEM           => V_DCON.DCON_ITEM,
                                                       V_DCON_CLAVE_CONCEPTO => V_DCON.DCON_CLAVE_CONCEPTO,
                                                       V_DCON_CLAVE_CTACO    => V_DCON.DCON_CLAVE_CTACO,
                                                       V_DCON_TIPO_SALDO     => V_DCON.DCON_TIPO_SALDO,
                                                       V_DCON_EXEN_LOC       => V_DCON.DCON_EXEN_LOC,
                                                       V_DCON_EXEN_MON       => V_DCON.DCON_EXEN_MON,
                                                       V_DCON_GRAV_LOC       => V_DCON.DCON_GRAV_LOC,
                                                       V_DCON_GRAV_MON       => V_DCON.DCON_GRAV_MON,
                                                       V_DCON_IVA_LOC        => V_DCON.DCON_IVA_LOC,
                                                       V_DCON_IVA_MON        => V_DCON.DCON_IVA_MON,
                                                       V_DCON_EMPR           => V_DCON.DCON_EMPR);

      END IF;
    END AGREGAR_CONCEPTO;

  BEGIN
    --  PP_CALCULAR_TOTALES_CONCEPTO;

    V_CLAVE_CONCEPTO := BCONF_FAC.CONF_CONC_IMPU_DEV;
    V_CLAVE_CTACO    := BSUC.SUC_CTA_IMPU;

    FOR C IN (SELECT A.SEQ_ID ITEM,
                     A.N001 ART,
                     A.N002 CANTIDAD,
                     A.N003 PRECIO,
                     ROUND((C020), G_MON.MON_DEC_IMP) DOC_IVA_MON,
                     ROUND((C021), G_MON.MON_DEC_IMP) DOC_IVA_LOC,
                     ROUND((C022), G_MON.MON_DEC_IMP) DOC_EXENTA_MON,
                     ROUND((C023), G_MON.MON_DEC_IMP) DOC_EXENTA_LOC,
                     ROUND((C024) + (C026), G_MON.MON_DEC_IMP) DOC_GRAV_MON,
                     ROUND((C025) + (C027), G_MON.MON_DEC_IMP) DOC_GRAV_LOC,
                     TO_NUMBER(C013) ART_IMPU
                FROM APEX_COLLECTIONS A
               WHERE A.COLLECTION_NAME = 'FACI036_DET') LOOP

      --NOTA DE CREDITO
      IF (C.DOC_GRAV_LOC) <> 0 THEN

        AGREGAR_CONCEPTO(IN_CONC     => BCONF_FAC.CONF_CONC_DEV,
                         IN_CTACO    => BSUC.SUC_CTA_DEV,
                         IN_GRAV_LOC => NVL(C.DOC_GRAV_LOC, 0),
                         IN_GRAV_MON => NVL(C.DOC_GRAV_MON, 0),
                         IN_EXEN_LOC => 0,
                         IN_EXEN_MON => 0,
                         IN_IVA_LOC  => 0,
                         IN_IVA_MON  => 0);

      END IF;
      IF (C.DOC_EXENTA_LOC) <> 0 THEN

        AGREGAR_CONCEPTO(IN_CONC     => BCONF_FAC.CONF_CONC_DEV,
                         IN_CTACO    => BSUC.SUC_CTA_DEV_EX,
                         IN_GRAV_LOC => 0,
                         IN_GRAV_MON => 0,
                         IN_EXEN_LOC => NVL(C.DOC_EXENTA_LOC, 0),
                         IN_EXEN_MON => NVL(C.DOC_EXENTA_MON, 0),
                         IN_IVA_LOC  => 0,
                         IN_IVA_MON  => 0);
      END IF;

      AGREGAR_CONCEPTO(IN_CONC     => V_CLAVE_CONCEPTO,
                       IN_CTACO    => V_CLAVE_CTACO,
                       IN_GRAV_LOC => 0,
                       IN_GRAV_MON => 0,
                       IN_EXEN_LOC => 0,
                       IN_EXEN_MON => 0,
                       IN_IVA_LOC  => NVL(C.DOC_IVA_LOC, 0),
                       IN_IVA_MON  => NVL(C.DOC_IVA_MON, 0));
    END LOOP;

  END PP_ACTUALIZAR_DOC_CONCEPTO;

  PROCEDURE PP_IN_FINI002 IS
    V_DOC_CLAVE_DEP        NUMBER := FP_FIN_SEQ_DOC;
    V_DOC_CLAVE_EXT        NUMBER := FP_FIN_SEQ_DOC;
    V_DCON_CLAVE_CTACO_EXT NUMBER;
    V_IMP_LOC              NUMBER;
    V_IMP_MON              NUMBER;
    V_CUENTA_DE            NUMBER := 19;
  BEGIN

    V_IMP_LOC := TSTK_DOCUMENTO.DOCU_EXEN_NETO_LOC +
                 TSTK_DOCUMENTO.DOCU_GRAV_NETO_LOC +
                 TSTK_DOCUMENTO.DOCU_IVA_LOC;
    V_IMP_MON := TSTK_DOCUMENTO.DOCU_EXEN_NETO_MON +
                 TSTK_DOCUMENTO.DOCU_GRAV_NETO_MON +
                 TSTK_DOCUMENTO.DOCU_IVA_MON;

    SELECT CTA_CLAVE_CTACO
      INTO V_DCON_CLAVE_CTACO_EXT
      FROM FIN_CUENTA_BANCARIA
     WHERE CTA_EMPR = 1
       AND CTA_CODIGO = 119
       AND CTA_EMPR = TFIN_DOCUMENTO.DOC_EMPR;

    INSERT INTO FIN_DOCUMENTO_FINI002
      (DOC_CLAVE_DEP,
       DOC_CTA_BCO_DEP,
       DOC_TIPO_SALDO_DEP,
       DOC_NRO_DOC_DEP,
       DOC_SUC_DEP,
       DOC_FEC_OPER_DEP,
       DOC_FEC_DOC_DEP,
       DOC_MON_DEP,
       DOC_NETO_EXEN_LOC_DEP,
       DOC_NETO_EXEN_MON_DEP,
       DOC_OBS_DEP,
       DOC_LOGIN_DEP,
       DOC_FEC_GRAB_DEP,
       DOC_SIST_DEP,
       DCON_CLAVE_CONCEPTO_DEP,
       DCON_TIPO_SALDO_DEP,
       DOC_CLAVE_EXT,
       DOC_CTA_BCO_EXT,
       DOC_TIPO_SALDO_EXT,
       DOC_MON_EXT,
       DOC_NETO_EXEN_LOC_EXT,
       DOC_NETO_EXEN_MON_EXT,
       DCON_CLAVE_CONCEPTO_EXT,
       DCON_TIPO_SALDO_EXT,
       DCON_CLAVE_CTACO_EXT,
       FINI002_ESTADO,
       FINI002_TRANS_OPAUX,
       DOC_CLAVE_NTCR,
       DOC_EMPR)
    VALUES
      (V_DOC_CLAVE_DEP, --OK
       119, --OK
       'D', --OK
       TSTK_DOCUMENTO.DOCU_NRO_DOC, --OK
       TSTK_DOCUMENTO.DOCU_SUC_ORIG, --OK
       TSTK_DOCUMENTO.DOCU_FEC_EMIS, --OK
       TSTK_DOCUMENTO.DOCU_FEC_OPER, --OK
       TSTK_DOCUMENTO.DOCU_MON, --OK
       V_IMP_LOC, --OK
       V_IMP_MON, --OK
       'NOTA DE CREDITO', --OK
       GEN_DEVUELVE_USER, --OK
       SYSDATE, --OK
       'FAC', --OK
       3,
       'D',
       V_DOC_CLAVE_EXT, --OK
       TFIN_DOCUMENTO.DOC_CTA_BCO_NCR, --DESTINO   --OK
       'C', --OK
       TFIN_DOCUMENTO.DOC_MON, --OK
       V_IMP_LOC, --OK
       V_IMP_MON, --OK
       5,
       'C',
       V_DCON_CLAVE_CTACO_EXT,
       'P', --OK
       'N',
       TFIN_DOCUMENTO.DOC_CLAVE,
       TFIN_DOCUMENTO.DOC_EMPR); --OK

    -- ESTA VALIDACION, ES PARA IMPEDIR QUE LA CAJA DE EXTRACCION Y DEPOSITO SEAN IGUALES, SE DIO ALGUNOS CASOS Y PARA EVITAR CASOS FUTUROS. 12/12/2019
    IF V_CUENTA_DE = TFIN_DOCUMENTO.DOC_CTA_BCO THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'La caja de deposito y extraccion no pueden ser iguales ' ||
                              'Caja de deposito: ' || V_CUENTA_DE ||
                              ' Caja de extraccion: ' ||
                              TFIN_DOCUMENTO.DOC_CTA_BCO);
    END IF;

  END PP_IN_FINI002;

  PROCEDURE PP_ACTUALIZAR_REGISTRO(I_EMPRESA             IN NUMBER,
                                   I_SUCURSAL            IN NUMBER,
                                   I_S_CREDITO           IN VARCHAR2,
                                   I_DOCU_TIMBRADO2      IN NUMBER,
                                   I_DOC_TASA_US         IN NUMBER,
                                   I_DOC_CTA_BCO         IN NUMBER,
                                   I_DOCU_CLAVE_FAC_NCR  IN NUMBER,
                                   I_DOCU_LEGAJO         IN NUMBER,
                                   I_DOCU_NRO_DOC        IN NUMBER,
                                   I_DOCU_DEP_ORIG       IN NUMBER,
                                   I_DOCU_MON            IN NUMBER,
                                   I_DOCU_CLI            IN NUMBER,
                                   I_DOCU_CLI_NOM        IN VARCHAR2,
                                   I_DOCU_CLI_RUC        IN VARCHAR2,
                                   I_DOCU_FEC_EMIS       IN DATE,
                                   I_DOCU_FEC_OPER       IN DATE,
                                   I_DOCU_OBS            IN VARCHAR2,
                                   I_DOCU_AFECT_COMISION IN VARCHAR2,
                                   I_DOCU_OCARGA_LONDON  IN NUMBER) IS

    V_MENSAJE       VARCHAR2(500);
    V_CLAVE_AUT_EXT NUMBER;
    V_AUX           NUMBER;
    V_OCA_ESTADO    VARCHAR2(2);
    V_OCA_CLAVE     NUMBER;
    V_DOC_TASA_US   NUMBER;

    PROCEDURE PI_VALIDAR_DATOS IS
    BEGIN

      --EN CDA SE DETECTO QUE ALGUNAS NC MANUALES INGRESARON SIN TIMBRADO, PARA EVITAR ESE PROBLEMA. 03/04/2020
      IF I_DOCU_TIMBRADO2 IS NULL THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'Timbrado no tiene valor, comuniquese con el Dpto. de Informatica');
      END IF;
      IF I_DOCU_DEP_ORIG IS NULL THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'El deposito no puede quedar nulo.');
      END IF;
      IF I_DOCU_LEGAJO IS NULL THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'El vendedor no puede quedar nulo.');
      END IF;
      IF I_DOCU_MON IS NULL THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'El vendedor no puede quedar nulo.');
      END IF;

      IF I_DOCU_FEC_EMIS IS NULL THEN
        RAISE_APPLICATION_ERROR(-20010, 'Fecha emision no puede ser nula!');
      END IF;

      IF I_S_CREDITO = 'N' AND I_DOC_CTA_BCO IS NULL THEN
        RAISE_APPLICATION_ERROR(-20010, 'Caja/Bco no puede ser nulo');
      END IF;

      IF I_DOCU_FEC_OPER IS NULL THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'Fecha operacion no puede ser nula!');
      END IF;

      IF NOT I_DOCU_FEC_OPER BETWEEN BCONF_FIN.CONF_PER_ACT_INI AND
         BCONF_FIN.CONF_PER_SGTE_FIN THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'La fecha de operacion: ' ||
                                TO_CHAR(I_DOCU_FEC_OPER, 'DD-MM-YYYY') ||
                                ' no coincide con el periodo del sistema de finanzas!');
      END IF;

      IF I_DOCU_FEC_EMIS NOT BETWEEN BCONF_STK_CONF_PER_ACT_INI AND
         BCONF_STK_CONF_PER_SGTE_FIN THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'Fecha debe estar comprendida en el rango de: ' ||
                                TO_CHAR(BCONF_STK_CONF_PER_ACT_INI,
                                        'DD/MM/YYYY') || ' a: ' ||
                                TO_CHAR(BCONF_STK_CONF_PER_SGTE_FIN,
                                        'DD/MM/YYYY') || ', seg.sist.STK!');
      END IF;
      IF I_DOCU_FEC_EMIS NOT BETWEEN BCONF_FIN.CONF_PER_ACT_INI AND
         BCONF_FIN.CONF_PER_SGTE_FIN THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'Fecha debe estar comprendida en el rango de: ' ||
                                TO_CHAR(BCONF_FIN.CONF_PER_ACT_INI,
                                        'DD/MM/YYYY') || ' a: ' ||
                                TO_CHAR(BCONF_FIN.CONF_PER_SGTE_FIN,
                                        'DD/MM/YYYY') || ', seg.sist.FIN!');
      END IF;

      GENERAL.PL_VALIDAR_HAB_MES_FIN(FECHA   => I_DOCU_FEC_EMIS,
                                     EMPRESA => I_EMPRESA,
                                     USUARIO => GEN_DEVUELVE_USER);

      GENERAL.PL_VALIDAR_HAB_MES_STK(FECHA   => I_DOCU_FEC_EMIS,
                                     EMPRESA => I_EMPRESA,
                                     USUARIO => GEN_DEVUELVE_USER);

    END PI_VALIDAR_DATOS;

    PROCEDURE PI_VALIDAR_DETALLE IS
    BEGIN
      FOR C IN (SELECT A.SEQ_ID ITEM,
                       A.N001 ART,
                       A.N002 CANTIDAD,
                       A.N003 PRECIO,
                       ROUND((C020), G_MON.MON_DEC_IMP) DOC_IVA_MON,
                       ROUND((C021), G_MON.MON_DEC_IMP) DOC_IVA_LOC,
                       ROUND((C022), G_MON.MON_DEC_IMP) DOC_EXENTA_MON,
                       ROUND((C023), G_MON.MON_DEC_IMP) DOC_EXENTA_LOC,
                       ROUND((C024) + (C026), G_MON.MON_DEC_IMP) DOC_GRAV_MON,
                       ROUND((C025) + (C027), G_MON.MON_DEC_IMP) DOC_GRAV_LOC,
                       TO_NUMBER(C013) ART_IMPU,
                       C010 MOTIVO_DEVOLUCION
                  FROM APEX_COLLECTIONS A
                 WHERE A.COLLECTION_NAME = 'FACI036_DET') LOOP

        IF C.CANTIDAD = 0 THEN
          APEX_COLLECTION.DELETE_MEMBER(P_COLLECTION_NAME => 'FACI036_DET',
                                        P_SEQ             => C.ITEM);
        END IF;
        IF C.CANTIDAD <> 0 AND C.PRECIO = 0 THEN
          RAISE_APPLICATION_ERROR(-20010,
                                  'Hay un item con cantidad que tiene cantidad pero no precio');
        END IF;
      END LOOP;

      APEX_COLLECTION.RESEQUENCE_COLLECTION(P_COLLECTION_NAME => 'FACI036_DET');
    END PI_VALIDAR_DETALLE;

  BEGIN

    SELECT *
      INTO BCONF_FAC
      FROM FAC_CONFIGURACION C
     WHERE C.CONF_EMPR = I_EMPRESA;

    SELECT *
      INTO BCONF_STK
      FROM STK_CONFIGURACION C
     WHERE C.CONF_EMPR = I_EMPRESA;

    SELECT PERI_FEC_INI, PERI_FEC_FIN
      INTO BCONF_STK_CONF_PER_ACT_INI, BCONF_STK_CONF_PER_ACT_FIN
      FROM STK_PERIODO
     WHERE PERI_CODIGO = BCONF_STK.CONF_PERIODO_ACT
       AND PERI_EMPR = I_EMPRESA;

    SELECT PERI_FEC_INI, PERI_FEC_FIN
      INTO BCONF_STK_CONF_PER_SGTE_INI, BCONF_STK_CONF_PER_SGTE_FIN
      FROM STK_PERIODO
     WHERE PERI_CODIGO = BCONF_STK.CONF_PERIODO_SGTE
       AND PERI_EMPR = I_EMPRESA;

    SELECT *
      INTO G_MON
      FROM GEN_MONEDA M
     WHERE M.MON_CODIGO = I_DOCU_MON
       AND M.MON_EMPR = I_EMPRESA;

    SELECT *
      INTO BSUC
      FROM GEN_SUCURSAL S
     WHERE S.SUC_EMPR = I_EMPRESA
       AND S.SUC_CODIGO = I_SUCURSAL;

    SELECT *
      INTO BCONF_FIN
      FROM FIN_CONFIGURACION
     WHERE CONF_EMPR = I_EMPRESA;

    PI_VALIDAR_DATOS;
    PI_VALIDAR_DETALLE;

    GENERAL.PP_AUT_ESP_DEPOSITO(P_EMPRESA            => I_EMPRESA,
                                P_OPER_LOGIN         => GEN_DEVUELVE_USER,
                                P_SUCURSAL           => I_SUCURSAL,
                                P_DEPOSITO           => I_DOCU_DEP_ORIG,
                                P_OPERACION          => 'DEP',
                                P_CLAVE_AUTORIZACION => V_CLAVE_AUT_EXT);

    IF V_CLAVE_AUT_EXT IS NULL THEN
      FACI039.PL_CONTROL_OPER_DEP(I_EMPRESA   => I_EMPRESA,
                                  I_SUCURSAL  => I_SUCURSAL,
                                  I_DEPOSITO  => I_DOCU_DEP_ORIG,
                                  I_OPERACION => 'DEP',
                                  P_USUARIO   => GEN_DEVUELVE_USER,
                                  I_CLAVE     => V_AUX); --LA CLAVE QUE RETORNA YA NO SE VA A UTILIZAR

    END IF;

    --PP_VALIDAR_CAMBIOS;

    IF I_S_CREDITO = 'S' THEN
      PP_VALIDAR_TOTAL_CUOTAS;
    END IF;

    BEGIN
      SELECT T.OCA_ESTADO
        INTO V_OCA_ESTADO
        FROM STK_ORDEN_CARGA_LONDON T
       WHERE T.OCA_CLAVE = I_DOCU_OCARGA_LONDON
         AND T.OCA_EMPR = I_EMPRESA;

      IF V_OCA_ESTADO <> 'A' THEN
        V_OCA_CLAVE := NULL;
      ELSE
        V_OCA_CLAVE := I_DOCU_OCARGA_LONDON;
      END IF;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;

    V_DOC_TASA_US := I_DOC_TASA_US;

    IF V_DOC_TASA_US IS NULL THEN
    V_DOC_TASA_US := FP_COTIZACION(I_EMPRESA      => I_EMPRESA,
                                  I_DOC_MON      => 2,
                                  I_DOC_FEC_EMIS => I_DOCU_FEC_EMIS);
    END IF;

    PP_ESTABL_VARIABLES_BDOCU_STK(I_EMPRESA             => I_EMPRESA,
                                  I_SUCURSAL            => I_SUCURSAL,
                                  I_S_CREDITO           => I_S_CREDITO,
                                  I_DOC_TASA_US         => V_DOC_TASA_US,
                                  I_DOCU_LEGAJO         => I_DOCU_LEGAJO,
                                  I_DOCU_NRO_DOC        => I_DOCU_NRO_DOC,
                                  I_DOCU_DEP_ORIG       => I_DOCU_DEP_ORIG,
                                  I_DOCU_MON            => I_DOCU_MON,
                                  I_DOCU_CLI            => I_DOCU_CLI,
                                  I_DOCU_CLI_NOM        => I_DOCU_CLI_NOM,
                                  I_DOCU_CLI_RUC        => I_DOCU_CLI_RUC,
                                  I_DOCU_FEC_EMIS       => I_DOCU_FEC_EMIS,
                                  I_DOCU_FEC_OPER       => I_DOCU_FEC_OPER,
                                  I_DOCU_OBS            => I_DOCU_OBS,
                                  I_DOCU_AFECT_COMISION => I_DOCU_AFECT_COMISION,
                                  I_DOCU_OCARGA_LONDON  => V_OCA_CLAVE,
                                  I_DOCU_CLAVE_STOPES   => V_CLAVE_AUT_EXT,
                                  I_DOCU_TIMBRADO       => I_DOCU_TIMBRADO2);

    PP_ACTUALIZAR_DOCUMENTO_FIN(I_DOCU_CLAVE_FAC_NCR => I_DOCU_CLAVE_FAC_NCR,
                                I_DOC_CTA_BCO        => I_DOC_CTA_BCO,
                                I_S_CREDITO          => I_S_CREDITO);

    PP_FAC_DOCUMENTO_DET;
    PP_STK_DOCUMENTO_DET;

    PP_ACTUALIZAR_DOC_CONCEPTO;

    IF I_SUCURSAL = 1 AND I_EMPRESA = 1 THEN
      PP_IN_FINI002;
    END IF;

    IF I_S_CREDITO = 'S' THEN
      PP_ACTUALIZAR_CUOTAS;
    END IF;

    IF TSTK_DOCUMENTO.DOCU_TIPO_MOV = 16 THEN
      DECLARE
      BEGIN
        UPDATE GEN_IMPRESORA
           SET IMP_ULT_NOTA_CREDITO = TSTK_DOCUMENTO.DOCU_NRO_DOC,
               IMP_NRO_TIMBRADO     = TSTK_DOCUMENTO.DOCU_TIMBRADO
         WHERE IMPR_IP = FP_IP_USER
           AND IMP_EMPR = I_EMPRESA;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
      END;
    END IF;

    V_MENSAJE := 'Guardado correctamente numero :' ||
                 TFIN_DOCUMENTO.DOC_NRO_DOC || '</br>' ||
                 '<a target="_blank" href="f?p=&APP_ID.:8:&SESSION.::::P8_LISTA_FACT:' ||
                 TFIN_DOCUMENTO.DOC_CLAVE || '">Imprimir nota de credito</a>
                  </br>';

    APEX_APPLICATION.G_PRINT_SUCCESS_MESSAGE := V_MENSAJE;

  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20010, 'Error: ' || SQLERRM);
  END PP_ACTUALIZAR_REGISTRO;

END FACI037;
/
