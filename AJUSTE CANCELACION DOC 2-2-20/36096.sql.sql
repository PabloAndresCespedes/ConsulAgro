-- Created on 19/08/2022 by PROGRAMACION7 
declare 
  l_empresa  number := 17;
  l_tipo_mov number := 32;
  l_desde    date   := '01/07/2022';
begin
  <<mega_cool_insert_loop>>
  for i in (select f.doc_clave          clave_cab,
       f.doc_empr           empresa,
       f.doc_neto_exen_loc  exento_loc,
       f.doc_neto_exen_mon  exento_mon,
       cc.fcon_clave        concepto,
       cc.fcon_clave_ctaco  cuenta,
       cc.fcon_tipo_saldo   tipo_saldo
  from fin_documento f
  inner join fin_configuracion c on ( c.conf_empr = f.doc_empr )
  inner join fin_concepto cc on ( cc.fcon_clave = c.conf_conc_cadpro and cc.fcon_empr = c.conf_empr )
  where f.doc_empr     = l_empresa
  and   f.doc_tipo_mov = l_tipo_mov
  and   (f.doc_fec_doc between l_desde and last_day(l_desde))
  and   f.doc_clave not in ( select c.dcon_clave_doc from fin_doc_concepto c where c.dcon_empr = f.doc_empr ))
  loop
    insert into fin_doc_concepto(dcon_clave_doc, dcon_item,     dcon_clave_concepto, dcon_clave_ctaco, dcon_tipo_saldo,
                                 dcon_exen_loc,  dcon_exen_mon, dcon_grav_loc,       dcon_grav_mon,    dcon_iva_loc,    
                                 dcon_iva_mon,   dcon_empr
                                 )
                           values(i.clave_cab    ,1,            i.concepto,          i.cuenta,         i.tipo_saldo,
                                  i.exento_loc,  i.exento_mon,  0,                   0,                0,
                                  0,             i.empresa
                                  );
  end loop mega_cool_insert_loop;
  
end;
