CREATE OR REPLACE TRIGGER 
gen_usuarios_programa_biu 
    before insert or update  
    on "GEN_USUARIOS_PROGRAMA" 
    for each row 
begin 
    if :new.id is null then 
        :new.id := per_usuarios_programa_seq.nextval; -->iba a ser PER pero no mejor GEN
    end if; 
end gen_usuarios_programa_biu; 
/
