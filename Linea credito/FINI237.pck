CREATE OR REPLACE PACKAGE FINI237 IS

  TYPE T_CAB IS RECORD(
    DOC_NRO_TRANS_COMBUS   NVARCHAR2(200),
    CATEG_CLI              NVARCHAR2(200),
    DOC_NRO_RECIBO_INICIAL NVARCHAR2(200),
    LINEA_DE_NEGOCIO       NVARCHAR2(200),
    DOC_FEC_OPER           NVARCHAR2(200),
    DOC_FEC_DOC            NVARCHAR2(200),
    CUO_FEC_VTO            NVARCHAR2(200),
    CHK_CUOTA              NVARCHAR2(200),
    FEC_CORTE              NVARCHAR2(200),
    CLIENTE                NVARCHAR2(200));

  -- PUBLIC FUNCTION AND PROCEDURE DECLARATIONS
  function obt_cat_cli_func_grupo(
    in_empresa in number
  )return fin_configuracion.conf_cat_cli_func_grupo%type;
  
  function obt_empresa_func_grupo(
    in_cliente fin_cliente.cli_codigo%type,
    in_empresa fin_cliente.cli_empr%type
  ) return fin_cliente.cli_empresa_funcionario%type;
  
  function obt_tmv_fact_cred_emit(
    in_empresa in number
  )return fin_configuracion.conf_fact_cr_emit%type;
    
  FUNCTION WHERE_PP_CARGAR_DETALLE(DOC_FEC_DOC IN DATE,
                                   CUO_FEC_VTO IN DATE,
                                   S_FEC_VTO   IN DATE,
                                   CHK_CUOTA   IN VARCHAR2) RETURN NUMBER;

  PROCEDURE PP_LLAMAR_REPORT(I_NRO_COMBUS IN NUMBER, I_EMPRESA IN NUMBER);

  PROCEDURE PP_ACTUALIZAR_REGISTRO(I_EMPRESA                IN NUMBER,
                                   I_CATEG_CLI              IN NUMBER,
                                   I_DOC_FEC_OPER           IN DATE,
                                   I_DOC_FEC_DOC            IN DATE,
                                   I_LINEA_NEGOCIO          IN NUMBER,
                                   I_DOC_NRO_RECIBO_INICIAL NUMBER,
                                   O_NRO_TRANS_COMBUS       OUT NUMBER);

  PROCEDURE PP_CONSULTAR(I_EMPRESA       IN NUMBER,
                         I_CATEG_CLI     IN NUMBER,
                         I_CHK_CUOTA     IN VARCHAR2,
                         I_FEC_VTO       IN DATE,
                         I_LINEA_NEGOCIO IN NUMBER,
                         I_CLIENTE       IN NUMBER,
                         I_FEC_CORTE     IN date,
                         in_empr_grupo    in gen_empresa.empr_codigo%type := null
                         );

  PROCEDURE PP_CONSULTAR_EXISTENTE(I_EMPRESA              IN NUMBER,
                                   I_DOC_NRO_TRANS_COMBUS IN NUMBER,
                                   O_DOC_FEC_OPER         OUT DATE,
                                   O_DOC_FEC_DOC          OUT DATE,
                                   O_DOC_NRO_INICIAL      OUT NUMBER,
                                   O_CATEG_CLI            OUT NUMBER);

  PROCEDURE PP_CAMBIAR_ESTADO(I_EMPRESA IN NUMBER, I_SEQ IN NUMBER);

  FUNCTION FP_NFORMAT(I_NUMBER IN NUMBER) RETURN VARCHAR2;

  PROCEDURE PP_BORRAR_REGISTRO(I_EMPRESA              IN NUMBER,
                               I_CATEG_CLI            IN NUMBER,
                               I_DOC_NRO_TRANS_COMBUS IN number
                               );

  -- 22/08/2022 11:25:13 @PabloACespedes \(^-^)/
  -- retorna una lista de los datos de los empleados del grupo
  function pipe_per_empleado_grupo( in_empr_grupo in varchar2 )
  return t_per_empleado_grupo_t pipelined;

END FINI237;
/
CREATE OR REPLACE PACKAGE BODY FINI237 IS
  -- 22/08/2022 8:04:32 @PabloACespedes \(^-^)/
  -- constantes
  co_funcionario_tagro constant number := 3; --> Tabla: fac_categoria >> 3: FUNCIONARIO TRANSAGR
  co_funcionario_hila  constant number := 4;
  co_funcionario_halt  constant number := 5;
  
  co_mnd_gs            constant number := 1;
  
  co_empr_hilagro      constant number := 1;
  co_empr_tagro        constant number := 2;
  co_empr_halten       constant number := 8;
  
  -- lineas de negocio TAGRO
  -- estos estan en duro en la app 100 page 6504 item P6504_LINEA_DE_NEGOCIO (PENDIENTE A MEJORAR)
  co_lin_neg_uniforme     constant number := 0;
  co_lin_neg_granos       constant number := 1;
  co_lin_neg_insumos      constant number := 3;
  co_lin_neg_combustible  constant number := 4;
  co_lin_neg_taller_otros constant number := 5;
  co_lin_neg_repuesto     constant number := 7;
  co_lin_neg_no_operativo constant number := 9; 
  
  -- RRHH
  co_per_conc_adelanto      constant number := 1; --> Tabla=per_concepto: ADELANTOS(+)
  co_per_form_pago_srv_pers constant number := 3; --> Tabla: PER_FORMA_PAGO >> 3: SERVICIOS PERSONALES
  
    
  FUNCTION WHERE_PP_CARGAR_DETALLE(DOC_FEC_DOC IN DATE,
                                   CUO_FEC_VTO IN DATE,
                                   S_FEC_VTO   IN DATE,
                                   CHK_CUOTA   IN varchar2
                                   ) RETURN NUMBER IS
  BEGIN
  
    IF S_FEC_VTO IS NOT NULL THEN
      IF ((DOC_FEC_DOC <=
         (CASE WHEN CHK_CUOTA = 'N' THEN S_FEC_VTO ELSE NULL END) AND
         CUO_FEC_VTO <= (LAST_DAY(TO_DATE(S_FEC_VTO, 'DD/MM/YYYY')))) OR
         CUO_FEC_VTO <=
         (CASE WHEN CHK_CUOTA = 'S' THEN S_FEC_VTO ELSE NULL END)) THEN
        RETURN 1;
      ELSE
        RETURN 0;
      END IF;
    ELSE
      RETURN 1;
    END IF;
  END WHERE_PP_CARGAR_DETALLE;
  
  -- 22/08/2022 8:25:51 @PabloACespedes \(^-^)/
  -- obtiene tipo movimeinto que es factura credito emitida
  function obt_tmv_fact_cred_emit(
    in_empresa in number
  )return fin_configuracion.conf_fact_cr_emit%type is
  l_r fin_configuracion.conf_fact_cr_emit%type;
  begin
    select tc.conf_fact_cr_emit
    into l_r
    from fin_configuracion tc
    where tc.conf_empr = in_empresa;
    
    if l_r is null then
      raise no_data_found;
    else
      return l_r;
    end if;
    
  exception
    when no_data_found then
      Raise_application_error(-20000, 'Es necesario configurar el Tipo de Movimiento Factura Cr'||chr(225)||'dito Emitida');
  end obt_tmv_fact_cred_emit;
  ---
  function obt_concepto_salario(
    in_empresa in number
  ) return per_configuracion.conf_conc_salario%type is
    l_r per_configuracion.conf_conc_salario%type;
  begin
    select pc.conf_conc_salario
    into l_r
    from per_configuracion pc
    where pc.conf_empr=in_empresa;
    
    if l_r is null then
      raise no_data_found;
    else
      return l_r;  
    end if;
    
  exception
    when no_data_found then
      Raise_application_error(-20000, 'Es necesario configurar el concepto salario para la empresa');
  end obt_concepto_salario;
  --
  
  -- 22/08/2022 7:59:37 @PabloACespedes \(^-^)/
  -- Obtiene la categoria para funcionario de grupo
  function obt_cat_cli_func_grupo(
    in_empresa in number
  )return fin_configuracion.conf_cat_cli_func_grupo%type
  is
   l_r fin_configuracion.conf_cat_cli_func_grupo%type;
  begin
    select cx.conf_cat_cli_func_grupo
    into l_r
    from fin_configuracion cx
    where cx.conf_empr = in_empresa;
    
    if l_r is null then
      raise no_data_found;
    else
      return l_r;
    end if;
    
  exception
    when no_data_found then
      Raise_application_error(-20000, 'Es necesario definir el c'||chr(243)||'digo de funcionario de grupo');
  end obt_cat_cli_func_grupo;
  --
  function obt_empresa_func_grupo(
    in_cliente fin_cliente.cli_codigo%type,
    in_empresa fin_cliente.cli_empr%type
  ) return fin_cliente.cli_empresa_funcionario%type is
  l_r fin_cliente.cli_empresa_funcionario%type;
  begin
    select fc.cli_empresa_funcionario
    into l_r
    from fin_cliente fc
    where fc.cli_codigo = in_cliente
    and   fc.cli_empr   = in_empresa
    and   fc.cli_empresa_funcionario is not null;
    
    return l_r;
  exception
    when no_data_found then
      Raise_application_error(-20000, 'El funcionario Cod: '|| in_cliente ||' no tiene definido su empresa del Grupo');
  end obt_empresa_func_grupo;
  --
  function obt_tmv_adelanto_prov(
    in_empresa number
  ) return fin_configuracion.conf_adelanto_pro%type is
  l_r fin_configuracion.conf_adelanto_pro%type;
  begin
    select ap.conf_adelanto_pro
    into l_r
    from fin_configuracion ap
    where ap.conf_empr = in_empresa;
    
    if l_r is null then
      raise no_data_found;
    else
      return l_r;
    end if;
  exception
    when no_data_found then
      Raise_application_error(-20000, 'Es necesario configurar el Tipo de Movimiento Adelanto Proveedor');
  end obt_tmv_adelanto_prov;
  --
  function pipe_per_empleado_grupo(in_empr_grupo in varchar2)
  return t_per_empleado_grupo_t
  pipelined is  
  l_query           varchar2(4000);
  l_sufijo          gen_empresa.empr_sufijo%type;
  c_datos_tabla     sys_refcursor;
  
  l_cod_empl number;
  l_cod_prov number;
  l_cod_empr number;
  begin
    <<obt_sufijo>>
    begin
      select e.empr_sufijo
      into l_sufijo
      from gen_empresa e
      where e.empr_codigo = in_empr_grupo;
      
    exception
      when no_data_found then
        l_sufijo := null;
    end obt_sufijo;
    
    if l_sufijo is not null then--> si tiene sufijo se_maneja por tabla independiente, caso contrario usa PER_EMPLEADO
      l_query := 'select legajo         as empl_legajo,
                         :in_empr_grupo as empl_empresa,
                         proveedor_id   as empl_codigo_prov
                  from PER_EMPLEADO_'||l_sufijo;
    else
      l_query := 'select empl_legajo,
                         empl_empresa,
                         empl_codigo_prov
                  from PER_EMPLEADO
                  where empl_empresa=:in_empr_grupo';
    end if;
    
    open c_datos_tabla for l_query using in_empr_grupo;
    
    <<obt_datos>>
    loop
      fetch c_datos_tabla into l_cod_empl, l_cod_empr, l_cod_prov;
      exit when c_datos_tabla%notfound;
      
      pipe row ( t_per_empleado_grupo(cod_empl => l_cod_empl,
                                      cod_empr => l_cod_empr,
                                      cod_prov => l_cod_prov
                                      )
                );
    end loop obt_datos;
    
    close c_datos_tabla;
    return;
  end pipe_per_empleado_grupo;
  
  --
  PROCEDURE PP_INICIAR_COLLECTION AS
  BEGIN
    APEX_COLLECTION.CREATE_OR_TRUNCATE_COLLECTION(P_COLLECTION_NAME => 'FINI257_DETALLE');
  
  END PP_INICIAR_COLLECTION;

  PROCEDURE PP_ADD_MEMBER(I_EMPRESA       IN NUMBER,
                          I_CLIENTE       IN NUMBER,
                          I_NOMBRE        IN VARCHAR2,
                          I_PROV_DEST     IN NUMBER,
                          I_FACTURA       IN NUMBER,
                          I_FECHA         IN DATE,
                          I_FEC_CUOTA     IN DATE,
                          I_IMPORTE       IN NUMBER,
                          I_TIPO_MOV      IN NUMBER,
                          I_LINEA_NEGOCIO IN NUMBER,
                          I_EMPLEADO      IN NUMBER,
                          I_TMOV_ABREV    IN VARCHAR2,
                          I_LNEGOCIO_DESC IN VARCHAR2,
                          I_DOC_CLAVE     IN NUMBER,
                          I_CAT_CODIGO    IN NUMBER,
                          I_DOC_FCON_HIL  IN NUMBER,
                          in_empresa_func in number := null
                          ) AS
  BEGIN
  
    APEX_COLLECTION.ADD_MEMBER(P_COLLECTION_NAME => 'FINI257_DETALLE',
                               P_C001            => I_CLIENTE,
                               P_C002            => I_NOMBRE,
                               P_C003            => I_PROV_DEST,
                               P_C004            => I_FACTURA,
                               P_C005            => I_FECHA,
                               P_C006            => I_FEC_CUOTA,
                               P_C007            => I_IMPORTE,
                               P_C008            => I_TIPO_MOV,
                               P_C009            => I_LINEA_NEGOCIO,
                               P_C010            => I_EMPLEADO,
                               P_C011            => I_TMOV_ABREV,
                               P_C012            => I_LNEGOCIO_DESC,
                               P_C013            => I_DOC_CLAVE,
                               P_C014            => I_CAT_CODIGO,
                               P_C015            => 'Si',
                               P_C016            => I_DOC_FCON_HIL,
                               p_c017            => in_empresa_func
                               );
  
  END PP_ADD_MEMBER;

  PROCEDURE PP_CARGAR_DETALLE(I_EMPRESA       IN NUMBER,
                              I_CATEG_CLI     IN NUMBER,
                              I_CHK_CUOTA     IN VARCHAR2,
                              I_FEC_VTO       IN DATE,
                              I_LINEA_NEGOCIO IN NUMBER,
                              I_CLIENTE       IN NUMBER,
                              I_FEC_CORTE     IN DATE) IS
  
    C           NUMBER := 0;
    V_EMPRESA   NUMBER := 0;
    V_PROV_DEST NUMBER;
  BEGIN
    --BREAK;
    IF I_CATEG_CLI = 4 THEN
      V_EMPRESA := 1;
    ELSIF I_CATEG_CLI = 5 THEN
      V_EMPRESA := 8;
    ELSE
      V_EMPRESA := I_EMPRESA;
    END IF;
  
    FOR V IN (SELECT A.FCAT_CODIGO,
                     A.FCAT_DESC,
                     D.CLI_CODIGO,
                     D.CLI_NOM,
                     C.DOC_TIPO_MOV,
                     DECODE(A.FCAT_CODIGO,
                            3,
                            F.EMPL_LEGAJO,
                            D.CLI_COD_EMPL_EMPR_ORIG) CLI_COD_EMPL_EMPR_ORIG,
                     C.DOC_NRO_DOC,
                     C.DOC_FEC_DOC,
                     E.CUO_CLAVE_DOC,
                     NVL(C.DOC_LINEA_NEGOCIO, 9) LINEA_NEGOCIO, ---9 ES NO OPERATIVO,
                     E.CUO_FEC_VTO,
                     LL.LIN_DESC,
                     TM.TMOV_ABREV,
                     E.CUO_SALDO_MON CUO_IMP_MON
                FROM FIN_DOCUMENTO     C,
                     FIN_CLIENTE       D,
                     FAC_CATEGORIA     A,
                     FIN_CUOTA         E,
                     PER_EMPLEADO      F,
                     FAC_LINEA_NEGOCIO LL,
                     GEN_TIPO_MOV      TM
               WHERE C.DOC_EMPR = I_EMPRESA
                    
                 AND C.DOC_CLI = D.CLI_CODIGO
                 AND C.DOC_EMPR = D.CLI_EMPR
                    
                 AND D.CLI_CATEG = A.FCAT_CODIGO
                 AND D.CLI_EMPR = A.FCAT_EMPR
                    
                 AND TM.TMOV_CODIGO = C.DOC_TIPO_MOV
                 AND TM.TMOV_EMPR = C.DOC_EMPR
                    
                 AND NVL(C.DOC_LINEA_NEGOCIO, 9) = LL.LIN_CODIGO
                 AND C.DOC_EMPR = LL.LIN_EMPR
                    
                 AND C.DOC_CLAVE = E.CUO_CLAVE_DOC
                 AND C.DOC_EMPR = E.CUO_EMPR
                    
                 AND (DOC_FEC_DOC <=
                     DECODE(I_CHK_CUOTA, 'N', I_FEC_VTO, NULL) OR
                     CUO_FEC_VTO <=
                     DECODE(I_CHK_CUOTA, 'S', I_FEC_VTO, NULL))
                    
                 AND C.DOC_SALDO_MON > 0
                 AND E.CUO_SALDO_MON > 0
                 AND C.DOC_TIPO_MOV = 10
                 AND C.DOC_MON = 1
                 AND C.DOC_LINEA_NEGOCIO = I_LINEA_NEGOCIO
                    
                 AND A.FCAT_CODIGO = I_CATEG_CLI
                    
                 AND (C.DOC_CLI = I_CLIENTE OR I_CLIENTE IS NULL)
                 AND D.CLI_CODIGO = EMPL_CODIGO_CLI(+)
                 AND D.CLI_EMPR = EMPL_EMPRESA(+)
                 AND (DOC_FEC_DOC <= I_FEC_CORTE OR I_FEC_CORTE IS NULL)
                 AND F.EMPL_FORMA_PAGO <> 3
                 AND C.DOC_PER_CONCEPTO IS NULL
               ORDER BY 3, 6, 7) LOOP
      V_PROV_DEST := NULL;
      IF I_CATEG_CLI <> 3 THEN
        IF V.CLI_COD_EMPL_EMPR_ORIG IS NULL THEN
          RAISE_APPLICATION_ERROR(-20010,
                                  'Esta persona :' || V.CLI_NOM ||
                                  ' no posee un codigo de empleado, puede provocar algunos errores si continua');
        END IF;
      
        BEGIN
          SELECT EMPL_CODIGO_PROV
            INTO V_PROV_DEST
            FROM PER_EMPLEADO
           WHERE EMPL_EMPRESA = V_EMPRESA
             AND EMPL_LEGAJO = V.CLI_COD_EMPL_EMPR_ORIG;
        END;
      END IF;
    
      PP_ADD_MEMBER(I_EMPRESA       => I_EMPRESA,
                    I_CLIENTE       => V.CLI_CODIGO,
                    I_NOMBRE        => V.CLI_NOM,
                    I_PROV_DEST     => V_PROV_DEST,
                    I_FACTURA       => V.DOC_NRO_DOC,
                    I_FECHA         => V.DOC_FEC_DOC,
                    I_FEC_CUOTA     => V.CUO_FEC_VTO,
                    I_IMPORTE       => V.CUO_IMP_MON,
                    I_TIPO_MOV      => V.DOC_TIPO_MOV,
                    I_LINEA_NEGOCIO => V.LINEA_NEGOCIO,
                    I_EMPLEADO      => V.CLI_COD_EMPL_EMPR_ORIG,
                    I_TMOV_ABREV    => V.TMOV_ABREV,
                    I_LNEGOCIO_DESC => V.LIN_DESC,
                    I_DOC_CLAVE     => V.CUO_CLAVE_DOC,
                    I_CAT_CODIGO    => V.FCAT_CODIGO,
                    I_DOC_FCON_HIL  => '');
    
    END LOOP;
  
  END PP_CARGAR_DETALLE;

  PROCEDURE PP_VALIDAR_DATOS(I_EMPRESA IN NUMBER) IS
    V_FCAT_DESC VARCHAR2(500);
  BEGIN
  
    FOR C IN (SELECT A.SEQ_ID,
                     A.C001   CLIENTE,
                     A.C002   NOMBRE,
                     A.C003   PROV_DEST,
                     A.C004   FACTURA,
                     A.C005   FECHA,
                     A.C006   FEC_CUOTA,
                     A.C007   IMPORTE,
                     A.C008   TIPO_MOV,
                     A.C009   LINEA_NEGOCIO,
                     A.C010   EMPLEADO,
                     A.C011   TMOV_ABREV,
                     A.C012   LNEGOCIO_DESC,
                     A.C013   DOC_CLAVE,
                     A.C014   FCAT_CODIGO,
                     A.C015   PROCESAR,
                     A.C016   DOC_FCON_HIL
                FROM APEX_COLLECTIONS A
               WHERE A.COLLECTION_NAME = 'FINI257_DETALLE'
                 AND NVL(A.C015, 'X') = 'Si') LOOP
    
      IF C.EMPLEADO IS NULL THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'El cliente ' || C.CLIENTE || '- ' ||
                                C.NOMBRE ||
                                ', no tiene codigo de empleado.');
      END IF;
    
      IF C.FCAT_CODIGO IS NOT NULL THEN
        SELECT FCAT_DESC
          INTO V_FCAT_DESC
          FROM FAC_CATEGORIA
         WHERE FCAT_CODIGO = C.FCAT_CODIGO
           AND FCAT_EMPR = I_EMPRESA;
      ELSE
        V_FCAT_DESC := NULL;
      END IF;
    
      IF C.PROV_DEST IS NULL THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'El cliente ' || C.CLIENTE || '- ' ||
                                C.NOMBRE ||
                                ', no tiene codigo de proveedor 17-1-5 de la empresa de ' ||
                                V_FCAT_DESC);
      END IF;
    
    END LOOP;
  
  END PP_VALIDAR_DATOS;

  PROCEDURE PP_INSERTAR_DOCUMENTO(I_EMPRESA                IN NUMBER,
                                  I_CATEG_CLI              IN NUMBER,
                                  I_DOC_FEC_OPER           IN DATE,
                                  I_DOC_FEC_DOC            IN DATE,
                                  I_LINEA_NEGOCIO          IN NUMBER,
                                  I_DOC_NRO_RECIBO_INICIAL NUMBER,
                                  O_NRO_TRANS_COMBUS       OUT NUMBER) IS
    V_CLAVE_FIN            NUMBER;
    V_RECIBO               NUMBER := 0; -- := :DOC_NRO_RECIBO_INICIAL;
    V_CUENTA               NUMBER := 1;
    V_CLI_ANT              NUMBER := 0;
    V_BAND                 NUMBER := 0;
    V_LINEA_NEGOCIO        VARCHAR2(50);
    V_DOC_NRO_TRANS_COMBUS NUMBER;
    V_FCAT_DESC            VARCHAR2(500);
    
    l_funcionario_grupo    fin_configuracion.conf_cat_cli_func_grupo%type;
    l_tmv_fact_cre_emit    fin_configuracion.conf_fact_cr_emit%type;
  BEGIN
    
    l_funcionario_grupo := obt_cat_cli_func_grupo(in_empresa => i_empresa);
    l_tmv_fact_cre_emit := obt_tmv_fact_cred_emit( in_empresa => i_empresa );
    
    SELECT NVL(MAX(T.DOC_NRO_TRANS_COMBUS), 0) + 1 A
      INTO V_DOC_NRO_TRANS_COMBUS
      FROM FIN_DOCUMENTO T
     WHERE T.DOC_EMPR = I_EMPRESA
       AND T.DOC_FEC_DOC > TRUNC(SYSDATE - 300, 'Y');
  
    V_RECIBO := I_DOC_NRO_RECIBO_INICIAL;
  
    DELETE FROM FIN_FINI037_TEMP WHERE EMPR = I_EMPRESA;
  
    V_CLAVE_FIN := FIN_SEQ_DOC_NEXTVAL;

    FOR C IN (SELECT A.SEQ_ID,
                     A.C001   CLIENTE,
                     A.C002   NOMBRE,
                     A.C003   PROV_DEST,
                     A.C004   FACTURA,
                     A.C005   FECHA,
                     A.C006   FEC_CUOTA,
                     A.C007   IMPORTE,
                     A.C008   TIPO_MOV,
                     A.C009   LINEA_NEGOCIO,
                     A.C010   EMPLEADO,
                     A.C011   TMOV_ABREV,
                     A.C012   LNEGOCIO_DESC,
                     A.C013   DOC_CLAVE,
                     A.C014   FCAT_CODIGO,
                     A.C015   PROCESAR,
                     A.C016   DOC_FCON_HIL,
                     a.c017   empresa_funcionario
                FROM APEX_COLLECTIONS A
               WHERE A.COLLECTION_NAME = 'FINI257_DETALLE'
                 AND NVL(A.C015, 'X') = 'Si')
    loop
       
      IF V_BAND = 0 THEN
        V_CLI_ANT   := C.CLIENTE;
        V_CLAVE_FIN := FIN_SEQ_DOC_NEXTVAL;
        V_BAND      := 1;
        V_LINEA_NEGOCIO := C.LINEA_NEGOCIO;
      END IF;
    
      IF V_CLI_ANT <> C.CLIENTE 
      or V_LINEA_NEGOCIO <> C.LINEA_NEGOCIO 
      or V_CUENTA = 15 
      then 
        V_CLI_ANT       := C.CLIENTE;
        V_LINEA_NEGOCIO := C.LINEA_NEGOCIO;
      
        V_CLAVE_FIN := FIN_SEQ_DOC_NEXTVAL;
        V_RECIBO    := V_RECIBO + 1;
        V_CUENTA    := 1;
        
      END IF;
    
      IF C.FCAT_CODIGO IS NOT NULL THEN
        SELECT FCAT_DESC
          INTO V_FCAT_DESC
          FROM FAC_CATEGORIA
         WHERE FCAT_CODIGO = C.FCAT_CODIGO
           AND FCAT_EMPR = I_EMPRESA;
      ELSE
        V_FCAT_DESC := NULL;
      END IF;
    
      --========
      BEGIN
        IF C.TIPO_MOV = l_tmv_fact_cre_emit THEN
          NULL;
          INSERT INTO FIN_FINI037_TEMP
            (FCAT_CODIGO,
             FCAT_DESC,
             CLI_CODIGO,
             CLI_NOM,
             CLI_COD_EMPL_EMPR_ORIG,
             DOC_NRO_DOC,
             DOC_FEC_DOC,
             CUO_FEC_VTO,
             CUO_IMP_MON,
             CLAVE_FIN,
             NRO_RECIBO,
             CLAVE_FAC,
             FECHA_DOC,
             FECHA_OPER,
             EMPR,
             TIPO_MOV,
             LINEA_NEGOCIO,
             LIN_CON_HIL)
          VALUES
            (C.FCAT_CODIGO,
             V_FCAT_DESC,
             C.CLIENTE,
             C.NOMBRE,
             C.EMPLEADO,
             C.FACTURA,
             C.FECHA,
             C.FEC_CUOTA,
             C.IMPORTE,
             V_CLAVE_FIN,
             NVL(V_RECIBO, V_DOC_NRO_TRANS_COMBUS),
             C.DOC_CLAVE,
             I_DOC_FEC_DOC,
             I_DOC_FEC_OPER,
             I_EMPRESA,
             C.TIPO_MOV,
             C.LNEGOCIO_DESC,
             C.DOC_FCON_HIL);
        ELSE
          INSERT INTO FIN_FINI037_TEMP
            (FCAT_CODIGO,
             FCAT_DESC,
             CLI_CODIGO,
             CLI_NOM,
             CLI_COD_EMPL_EMPR_ORIG,
             DOC_NRO_DOC,
             DOC_FEC_DOC,
             CUO_FEC_VTO,
             CUO_IMP_MON,
             CLAVE_FIN,
             NRO_RECIBO,
             CLAVE_FAC,
             FECHA_DOC,
             FECHA_OPER,
             EMPR,
             TIPO_MOV,
             LINEA_NEGOCIO,
             LIN_CON_HIL)
          VALUES
            (C.FCAT_CODIGO,
             V_FCAT_DESC,
             C.CLIENTE,
             C.NOMBRE,
             C.EMPLEADO,
             C.FACTURA,
             C.FECHA,
             C.FEC_CUOTA,
             C.IMPORTE,
             FIN_SEQ_DOC_NEXTVAL,
             NVL(V_RECIBO, V_DOC_NRO_TRANS_COMBUS),
             C.DOC_CLAVE,
             I_DOC_FEC_DOC,
             I_DOC_FEC_OPER,
             I_EMPRESA,
             C.TIPO_MOV,
             C.LNEGOCIO_DESC,
             C.DOC_FCON_HIL);
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20010, SQLERRM);
      END;
    
      --========
    
      V_CUENTA := V_CUENTA + 1;
    
      UPDATE GEN_IMPRESORA
         SET IMP_ULT_REC_PAGO_EMIT = V_RECIBO
       WHERE IMPR_IP = FP_IP_USER
         AND IMP_EMPR = I_EMPRESA;
    
    --END IF;
    
    END LOOP;
    --commit;
    
    IF I_CATEG_CLI <> co_funcionario_tagro THEN
      FIN_INS_REC_FUN_HIL(I_OPERACION   => 'I',
                          I_CATEGORIA   => I_CATEG_CLI,
                          I_NRO_PROCESO => V_DOC_NRO_TRANS_COMBUS,
                          IN_EMPRESA    => I_EMPRESA
                          );
    ELSE
      ---DOC_NRO_TRANS_COMBUS NRO DE TRANSACCION PARA RRHH CON RELACION A FINANZA
    
      IF I_LINEA_NEGOCIO = 4 THEN
        --=============================  
        FIN_INS_REC_FUN(I_OPERACION   => 'I',
                        I_CATEGORIA   => I_CATEG_CLI,
                        I_NRO_PROCESO => V_DOC_NRO_TRANS_COMBUS,
                        IN_EMPRESA    => I_EMPRESA);
        --=============================
      ELSIF I_LINEA_NEGOCIO = 0 THEN
        --============================= 
        FIN_INS_REC_UNIFORME_FUNC(I_OPERACION   => 'I',
                                  I_NRO_PROCESO => V_DOC_NRO_TRANS_COMBUS,
                                  I_EMPRESA     => I_EMPRESA);
        --============================= 
      ELSIF I_LINEA_NEGOCIO = 5 THEN
      
        --FIN_INS_REC_FUN('I',I_CATEG_CLI , V_DOC_NRO_TRANS_COMBUS);  
        --============================= 
        FIN_INS_REC_REPUESTO_FUNC(I_OPERACION   => 'I',
                                  I_NRO_PROCESO => V_DOC_NRO_TRANS_COMBUS,
                                  I_EMPRESA     => I_EMPRESA,
                                  I_CATEGORIA   => I_CATEG_CLI);
        --============================= 
      ELSIF I_LINEA_NEGOCIO = 9 THEN
        --============================= 
        FIN_INS_REC_VARIOS_FUNC(I_OPERACION   => 'I',
                                I_NRO_PROCESO => V_DOC_NRO_TRANS_COMBUS,
                                I_EMPRESA     => I_EMPRESA);
        --============================= 
      END IF;
    END IF;
  
    O_NRO_TRANS_COMBUS := V_DOC_NRO_TRANS_COMBUS;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20010, SQLERRM);
  END PP_INSERTAR_DOCUMENTO;

  PROCEDURE PP_LLAMAR_REPORT(I_NRO_COMBUS IN NUMBER, I_EMPRESA IN NUMBER) IS
  
    SALIR EXCEPTION;
    V_PARAMETROS    VARCHAR2(600);
    V_IDENTIFICADOR VARCHAR2(2) := '&'; --SE UTILIZA PARA QUE AL COMPILAR NO LO TOME COMO PARAMETROS
    V_REPORTE       VARCHAR2(40) := 'FINI237_LOTE';
  BEGIN
  
    /*
       V_WHERE :=     ' AND R.DOC_NRO_TRANS_COMBUS = '||I_NRO_REC;
    
     
    
    
    */
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_NRO_REC=' ||
                    URL_ENCODE(I_NRO_COMBUS);
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_EMPRESA=' ||
                    APEX_UTIL.URL_ENCODE(I_EMPRESA);
    
    IF gen_devuelve_user IN ('NATALIA', 'LETIGA') THEN
    V_REPORTE    := 'FINI237_RRHH';
   ELSE
      V_REPORTE    := 'FINI237';
   END IF;
    /*  V_PARAMETROS := 'P_FORMATO=' || APEX_UTIL.URL_ENCODE('pdf');
    
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_EMPRESA=' ||
                    APEX_UTIL.URL_ENCODE(I_EMPRESA);
    
    V_PARAMETROS := V_PARAMETROS || V_IDENTIFICADOR || 'P_NRO_COMBUS=' ||
                    APEX_UTIL.URL_ENCODE(I_NRO_COMBUS);*/
  
    DELETE FROM GEN_PARAMETROS_REPORT WHERE USUARIO = GEN_DEVUELVE_USER;
  
    INSERT INTO GEN_PARAMETROS_REPORT
      (PARAMETROS, USUARIO, NOMBRE_REPORTE, FORMATO_SALIDA)
    VALUES
      (V_PARAMETROS, GEN_DEVUELVE_USER, V_REPORTE, 'pdf');
  
  EXCEPTION
    WHEN SALIR THEN
      RAISE_APPLICATION_ERROR(-20010, SQLERRM);
  END PP_LLAMAR_REPORT;

  PROCEDURE PP_ACTUALIZAR_REGISTRO(I_EMPRESA                IN NUMBER,
                                   I_CATEG_CLI              IN NUMBER,
                                   I_DOC_FEC_OPER           IN DATE,
                                   I_DOC_FEC_DOC            IN DATE,
                                   I_LINEA_NEGOCIO          IN NUMBER,
                                   I_DOC_NRO_RECIBO_INICIAL NUMBER,
                                   O_NRO_TRANS_COMBUS       OUT NUMBER) IS
  BEGIN
  
    IF I_CATEG_CLI IS NULL THEN
      GENERAL.PL_VALIDAR_HAB_MES_FIN(FECHA   => I_DOC_FEC_OPER,
                                     EMPRESA => I_EMPRESA,
                                     USUARIO => GEN_DEVUELVE_USER);
    
      GENERAL.PL_VALIDAR_HAB_MES_FIN(FECHA   => I_DOC_FEC_DOC,
                                     EMPRESA => I_EMPRESA,
                                     USUARIO => GEN_DEVUELVE_USER);
    END IF;
  
    IF I_CATEG_CLI <> co_funcionario_tagro THEN
      PP_VALIDAR_DATOS(I_EMPRESA => I_EMPRESA);
    END IF;
  
    PP_INSERTAR_DOCUMENTO(I_EMPRESA                => I_EMPRESA,
                          I_CATEG_CLI              => I_CATEG_CLI,
                          I_LINEA_NEGOCIO          => I_LINEA_NEGOCIO,
                          I_DOC_FEC_OPER           => I_DOC_FEC_OPER,
                          I_DOC_FEC_DOC            => I_DOC_FEC_DOC,
                          I_DOC_NRO_RECIBO_INICIAL => I_DOC_NRO_RECIBO_INICIAL,
                          O_NRO_TRANS_COMBUS       => O_NRO_TRANS_COMBUS
                          );
  
  END PP_ACTUALIZAR_REGISTRO;

  PROCEDURE PP_CARGAR_DETALLE_NO_OPER_C(I_EMPRESA IN number,
                                        I_CHK_CUOTA IN VARCHAR2,
                                        I_FEC_VTO   IN date,
                                        I_CLIENTE   IN NUMBER,
                                        I_FEC_CORTE IN DATE) IS
    C NUMBER;
    l_tmv_factura_cre_emit fin_configuracion.conf_fact_cr_emit%type;
  BEGIN
    l_tmv_factura_cre_emit := obt_tmv_fact_cred_emit(in_empresa => I_EMPRESA);
    
    FOR V IN (SELECT A.FCAT_CODIGO,
                     A.FCAT_DESC,
                     D.CLI_CODIGO,
                     D.CLI_NOM,
                     DECODE(A.FCAT_CODIGO,
                            co_funcionario_tagro,
                            F.EMPL_LEGAJO,
                            D.CLI_COD_EMPL_EMPR_ORIG) CLI_COD_EMPL_EMPR_ORIG,
                     C.DOC_NRO_DOC,
                     C.DOC_FEC_DOC,
                     E.CUO_CLAVE_DOC,
                     E.CUO_FEC_VTO,
                     E.CUO_SALDO_MON CUO_IMP_MON,
                     C.DOC_TIPO_MOV
                FROM FIN_DOCUMENTO C,
                     FIN_CLIENTE   D,
                     FAC_CATEGORIA A,
                     FIN_CUOTA     E,
                     PER_EMPLEADO  F
               WHERE C.DOC_EMPR = I_EMPRESA
                    
                 AND C.DOC_CLI = D.CLI_CODIGO
                 AND C.DOC_EMPR = D.CLI_EMPR
                    
                 AND D.CLI_CATEG = A.FCAT_CODIGO
                 AND D.CLI_EMPR = A.FCAT_EMPR
                    
                 AND C.DOC_CLAVE = E.CUO_CLAVE_DOC
                 AND C.DOC_EMPR = E.CUO_EMPR
                    
                 AND ((DOC_FEC_DOC <=
                     DECODE(I_CHK_CUOTA, 'N', I_FEC_VTO, NULL) AND
                     CUO_FEC_VTO <= (LAST_DAY(I_FEC_VTO))) OR
                     CUO_FEC_VTO <=
                     DECODE(I_CHK_CUOTA, 'S', I_FEC_VTO, NULL))
                    
                 AND C.DOC_PER_CONCEPTO = co_per_conc_adelanto
                 AND C.DOC_SALDO_MON > 0
                 AND E.CUO_SALDO_MON > 0
                 AND C.DOC_TIPO_MOV    = l_tmv_factura_cre_emit
                 AND F.EMPL_FORMA_PAGO <> co_per_form_pago_srv_pers
                 AND C.DOC_MON = co_mnd_gs
                 AND C.DOC_LINEA_NEGOCIO IN (co_lin_neg_no_operativo,
                                             co_lin_neg_combustible,
                                             co_lin_neg_granos,
                                             co_lin_neg_repuesto
                                             )
                 AND A.FCAT_CODIGO = co_funcionario_tagro
                 AND D.CLI_CODIGO = EMPL_CODIGO_CLI(+)
                 AND D.CLI_EMPR = EMPL_EMPRESA(+)
                 AND (DOC_FEC_DOC <= I_FEC_CORTE OR I_FEC_CORTE IS NULL)
                 AND (C.DOC_CLI = I_CLIENTE OR I_CLIENTE IS NULL)
               ORDER BY 3, 6, 7) LOOP
    
      PP_ADD_MEMBER(I_EMPRESA       => I_EMPRESA,
                    I_CLIENTE       => V.CLI_CODIGO,
                    I_NOMBRE        => V.CLI_NOM,
                    I_PROV_DEST     => '',
                    I_FACTURA       => V.DOC_NRO_DOC,
                    I_FECHA         => V.DOC_FEC_DOC,
                    I_FEC_CUOTA     => V.CUO_FEC_VTO,
                    I_IMPORTE       => V.CUO_IMP_MON,
                    I_TIPO_MOV      => V.DOC_TIPO_MOV,
                    I_LINEA_NEGOCIO => '',
                    I_EMPLEADO      => V.CLI_COD_EMPL_EMPR_ORIG,
                    I_TMOV_ABREV    => '',
                    I_LNEGOCIO_DESC => '',
                    I_DOC_CLAVE     => V.CUO_CLAVE_DOC,
                    I_CAT_CODIGO    => V.FCAT_CODIGO,
                    I_DOC_FCON_HIL  => '');
    END LOOP;
  
  END PP_CARGAR_DETALLE_NO_OPER_C;

  PROCEDURE PP_CARGAR_DETALLE_NO_OPER(I_EMPRESA   IN NUMBER,
                                      I_CHK_CUOTA IN VARCHAR2,
                                      I_FEC_VTO   IN date,
                                      I_CLIENTE   IN NUMBER,
                                      I_FEC_CORTE IN date                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   ) IS

    C number;
    l_tmv_fact_cred_emit fin_configuracion.conf_fact_cr_emit%type;
  BEGIN
    l_tmv_fact_cred_emit := obt_tmv_fact_cred_emit(in_empresa => i_empresa);
    
    FOR V IN (SELECT A.FCAT_CODIGO,
                     A.FCAT_DESC,
                     D.CLI_CODIGO,
                     D.CLI_NOM,
                     DECODE(A.FCAT_CODIGO,
                            co_funcionario_tagro,
                            F.EMPL_LEGAJO,
                            D.CLI_COD_EMPL_EMPR_ORIG) CLI_COD_EMPL_EMPR_ORIG,
                     C.DOC_NRO_DOC,
                     C.DOC_FEC_DOC,
                     E.CUO_CLAVE_DOC,
                     E.CUO_FEC_VTO,
                     E.CUO_SALDO_MON CUO_IMP_MON,
                     C.DOC_TIPO_MOV
                FROM FIN_DOCUMENTO C,
                     FIN_CLIENTE   D,
                     FAC_CATEGORIA A,
                     FIN_CUOTA     E,
                     PER_EMPLEADO  F
               WHERE C.DOC_EMPR = I_EMPRESA
                    
                 AND C.DOC_CLI = D.CLI_CODIGO
                 AND C.DOC_EMPR = D.CLI_EMPR
                    
                 AND D.CLI_CATEG = A.FCAT_CODIGO
                 AND D.CLI_EMPR = A.FCAT_EMPR
                    
                 AND C.DOC_CLAVE = E.CUO_CLAVE_DOC
                 AND C.DOC_EMPR = E.CUO_EMPR
                    
                 AND ((DOC_FEC_DOC <=
                     DECODE(I_CHK_CUOTA, 'N', I_FEC_VTO, NULL) AND
                     CUO_FEC_VTO <= (LAST_DAY(I_FEC_VTO))) OR
                     CUO_FEC_VTO <=
                     DECODE(I_CHK_CUOTA, 'S', I_FEC_VTO, NULL))
                 AND C.DOC_PER_CONCEPTO = co_per_conc_adelanto
                 AND C.DOC_SALDO_MON > 0
                 AND E.CUO_SALDO_MON > 0
                 AND C.DOC_TIPO_MOV = l_tmv_fact_cred_emit
                 AND F.EMPL_FORMA_PAGO <> co_per_form_pago_srv_pers
                 AND C.DOC_MON = co_mnd_gs
                 AND C.DOC_LINEA_NEGOCIO IN (co_lin_neg_no_operativo,
                                             co_lin_neg_combustible, 
                                             co_lin_neg_granos, 
                                             co_lin_neg_repuesto
                                             )
                 AND A.FCAT_CODIGO = co_funcionario_tagro
                 AND D.CLI_CODIGO = EMPL_CODIGO_CLI(+)
                 AND D.CLI_EMPR = EMPL_EMPRESA(+)
                    
                 AND (C.DOC_CLI = I_CLIENTE OR I_CLIENTE IS NULL)
                 AND (DOC_FEC_DOC <= I_FEC_CORTE OR I_FEC_CORTE IS NULL)
              
               ORDER BY 3, 6, 7) LOOP
    
      PP_ADD_MEMBER(I_EMPRESA       => I_EMPRESA,
                    I_CLIENTE       => V.CLI_CODIGO,
                    I_NOMBRE        => V.CLI_NOM,
                    I_PROV_DEST     => '',
                    I_FACTURA       => V.DOC_NRO_DOC,
                    I_FECHA         => V.DOC_FEC_DOC,
                    I_FEC_CUOTA     => V.CUO_FEC_VTO,
                    I_IMPORTE       => V.CUO_IMP_MON,
                    I_TIPO_MOV      => V.DOC_TIPO_MOV,
                    I_LINEA_NEGOCIO => '',
                    I_EMPLEADO      => V.CLI_COD_EMPL_EMPR_ORIG,
                    I_TMOV_ABREV    => '',
                    I_LNEGOCIO_DESC => '',
                    I_DOC_CLAVE     => V.CUO_CLAVE_DOC,
                    I_CAT_CODIGO    => V.FCAT_CODIGO,
                    I_DOC_FCON_HIL  => '');
    
    END LOOP;
  
  END PP_CARGAR_DETALLE_NO_OPER;

  PROCEDURE PP_CARGAR_DET_NO_OPER_VARIOS(I_EMPRESA IN NUMBER,
                                         --I_CATEG_CLI              IN NUMBER,
                                         I_CHK_CUOTA IN VARCHAR2,
                                         I_FEC_VTO   IN DATE,
                                         --I_LINEA_NEGOCIO          IN NUMBER,
                                         I_CLIENTE   IN NUMBER,
                                         I_FEC_CORTE IN DATE) IS
    C NUMBER;
    l_tmv_factura_cre_emit fin_configuracion.conf_fact_cr_emit%type;
    l_conc_salario         per_configuracion.conf_conc_salario%type;
  begin
    -- inicializacion de configuracion
    l_tmv_factura_cre_emit := obt_tmv_fact_cred_emit(in_empresa => I_EMPRESA);
    l_conc_salario         := obt_concepto_salario(in_empresa => i_empresa);
    
    FOR V IN (SELECT A.FCAT_CODIGO,
                     A.FCAT_DESC,
                     D.CLI_CODIGO,
                     D.CLI_NOM,
                     DECODE(A.FCAT_CODIGO,
                            co_funcionario_tagro,
                            F.EMPL_LEGAJO,
                            D.CLI_COD_EMPL_EMPR_ORIG) CLI_COD_EMPL_EMPR_ORIG,
                     C.DOC_NRO_DOC,
                     C.DOC_FEC_DOC,
                     E.CUO_CLAVE_DOC,
                     E.CUO_FEC_VTO,
                     E.CUO_SALDO_MON CUO_IMP_MON,
                     C.DOC_LINEA_NEGOCIO,
                     C.DOC_PER_CONCEPTO,
                     c.doc_tipo_mov
                FROM FIN_DOCUMENTO C,
                     FIN_CLIENTE   D,
                     FAC_CATEGORIA A,
                     FIN_CUOTA     E,
                     PER_EMPLEADO  F
               WHERE C.DOC_EMPR = I_EMPRESA
                    
                 AND C.DOC_CLI = D.CLI_CODIGO
                 AND C.DOC_EMPR = D.CLI_EMPR
                    
                 AND D.CLI_CATEG = A.FCAT_CODIGO
                 AND D.CLI_EMPR = A.FCAT_EMPR
                    
                 AND C.DOC_CLAVE = E.CUO_CLAVE_DOC
                 AND C.DOC_EMPR = E.CUO_EMPR
                    
                 AND ((DOC_FEC_DOC <=
                     DECODE(I_CHK_CUOTA, 'N', I_FEC_VTO, NULL) AND
                     CUO_FEC_VTO <= (LAST_DAY(I_FEC_VTO)))
                     
                     OR CUO_FEC_VTO <=
                     DECODE(I_CHK_CUOTA, 'S', I_FEC_VTO, NULL))
                    
                 AND (C.DOC_PER_CONCEPTO = l_conc_salario OR C.DOC_PER_CONCEPTO IS NULL)
                 AND C.DOC_SALDO_MON > 0
                 AND E.CUO_SALDO_MON > 0
                 AND C.DOC_TIPO_MOV = l_tmv_factura_cre_emit
                 AND F.EMPL_FORMA_PAGO <> co_per_form_pago_srv_pers
                 AND C.DOC_MON = co_mnd_gs
                 AND C.DOC_LINEA_NEGOCIO IN (co_lin_neg_no_operativo,
                                             co_lin_neg_combustible,
                                             co_lin_neg_granos,
                                             co_lin_neg_insumos,
                                             co_lin_neg_repuesto
                                             )
                 AND A.FCAT_CODIGO = co_funcionario_tagro
                 AND D.CLI_CODIGO = EMPL_CODIGO_CLI(+)
                 AND D.CLI_EMPR = EMPL_EMPRESA(+)
                 AND (DOC_FEC_DOC <= I_FEC_CORTE OR I_FEC_CORTE IS NULL)
                 AND (C.DOC_CLI = I_CLIENTE OR I_CLIENTE IS NULL)
               ORDER BY 3, 6, 7) LOOP
    
      IF V.DOC_LINEA_NEGOCIO <> co_lin_neg_combustible THEN
        NULL;
      
        PP_ADD_MEMBER(I_EMPRESA       => I_EMPRESA,
                      I_CLIENTE       => V.CLI_CODIGO,
                      I_NOMBRE        => V.CLI_NOM,
                      I_PROV_DEST     => '',
                      I_FACTURA       => V.DOC_NRO_DOC,
                      I_FECHA         => V.DOC_FEC_DOC,
                      I_FEC_CUOTA     => V.CUO_FEC_VTO,
                      I_IMPORTE       => V.CUO_IMP_MON,
                      I_TIPO_MOV      => V.DOC_TIPO_MOV,
                      I_LINEA_NEGOCIO => '',
                      I_EMPLEADO      => V.CLI_COD_EMPL_EMPR_ORIG,
                      I_TMOV_ABREV    => '',
                      I_LNEGOCIO_DESC => '',
                      I_DOC_CLAVE     => V.CUO_CLAVE_DOC,
                      I_CAT_CODIGO    => V.FCAT_CODIGO,
                      I_DOC_FCON_HIL  => ''
                      );
      
      ELSIF V.DOC_LINEA_NEGOCIO = co_lin_neg_combustible AND V.DOC_PER_CONCEPTO IS NOT NULL THEN
        ---- ACA ENTRA TODO LO QUE SON DE ESTACION DE SERVICIO PERO EN AUTOSERVICE  
        NULL;
      
        PP_ADD_MEMBER(I_EMPRESA       => I_EMPRESA,
                      I_CLIENTE       => V.CLI_CODIGO,
                      I_NOMBRE        => V.CLI_NOM,
                      I_PROV_DEST     => '',
                      I_FACTURA       => V.DOC_NRO_DOC,
                      I_FECHA         => V.DOC_FEC_DOC,
                      I_FEC_CUOTA     => V.CUO_FEC_VTO,
                      I_IMPORTE       => V.CUO_IMP_MON,
                      I_TIPO_MOV      => V.DOC_TIPO_MOV,
                      I_LINEA_NEGOCIO => '',
                      I_EMPLEADO      => V.CLI_COD_EMPL_EMPR_ORIG,
                      I_TMOV_ABREV    => '',
                      I_LNEGOCIO_DESC => '',
                      I_DOC_CLAVE     => V.CUO_CLAVE_DOC,
                      I_CAT_CODIGO    => V.FCAT_CODIGO,
                      I_DOC_FCON_HIL  => ''
                      );
      
      END IF;
      C := C + 1;
    END LOOP;
  END PP_CARGAR_DET_NO_OPER_VARIOS;

  PROCEDURE PP_CARGAR_DETALLE_HIL_C(I_EMPRESA     IN NUMBER,
                                    I_CATEG_CLI   IN NUMBER,
                                    I_CHK_CUOTA   IN VARCHAR2,
                                    I_FEC_VTO     IN DATE,
                                    I_CLIENTE     IN NUMBER,
                                    I_FEC_CORTE   IN date,
                                    in_empr_grupo in gen_empresa.empr_codigo%type := null
                                    ) is
    C           NUMBER := 0;
    V_EMPRESA   NUMBER := 0;
    V_PROV_DEST NUMBER;
    l_funcionario_grupo fin_configuracion.conf_cat_cli_func_grupo%type;
    l_tmv_fact_cre_emit fin_configuracion.conf_fact_cr_emit%type;
    l_tmv_adelanto_prov fin_configuracion.conf_adelanto_pro%type;

  begin
    l_funcionario_grupo := obt_cat_cli_func_grupo(in_empresa => i_empresa);
    
    IF I_CATEG_CLI = co_funcionario_hila THEN
      V_EMPRESA := co_empr_hilagro;
    ELSIF I_CATEG_CLI = co_funcionario_halt THEN
      V_EMPRESA := co_empr_halten;
    elsif i_categ_cli = l_funcionario_grupo then
      if i_cliente is not null then
        V_EMPRESA := obt_empresa_func_grupo( in_cliente => i_cliente, in_empresa => i_empresa );
      else
        v_empresa := in_empr_grupo;
      end if;
    ELSE
      V_EMPRESA := I_EMPRESA;
    END IF;
    
    -- obt el tmv fact cred emitida
    l_tmv_fact_cre_emit := obt_tmv_fact_cred_emit( in_empresa => i_empresa );
    l_tmv_adelanto_prov := obt_tmv_adelanto_prov( in_empresa => i_empresa );
    
    FOR V IN (SELECT A.FCAT_CODIGO,
                     A.FCAT_DESC,
                     D.CLI_CODIGO,
                     D.CLI_NOM,
                     DECODE(A.FCAT_CODIGO,
                            co_funcionario_tagro,
                            F.EMPL_LEGAJO,
                            D.CLI_COD_EMPL_EMPR_ORIG) CLI_COD_EMPL_EMPR_ORIG,
                     C.DOC_NRO_DOC,
                     C.DOC_FEC_DOC,
                     E.CUO_CLAVE_DOC,
                     E.CUO_FEC_VTO,
                     E.CUO_SALDO_MON CUO_IMP_MON,
                     NVL(C.DOC_LINEA_NEGOCIO, co_lin_neg_no_operativo) LINEA_NEGOCIO,
                     LL.LIN_DESC,
                     C.DOC_TIPO_MOV,
                     TM.TMOV_ABREV,
                     LL.LIN_FCON_HIL,
                     null cod_prov_destino
                FROM FIN_DOCUMENTO     C,
                     FIN_CLIENTE       D,
                     FAC_CATEGORIA     A,
                     FIN_CUOTA         E,
                     PER_EMPLEADO      F,
                     FAC_LINEA_NEGOCIO LL,
                     GEN_TIPO_MOV      TM
               WHERE C.DOC_EMPR = co_empr_tagro
                 AND C.DOC_CLI = D.CLI_CODIGO
                 AND C.DOC_EMPR = D.CLI_EMPR
                    
                 AND D.CLI_CATEG = A.FCAT_CODIGO
                 AND D.CLI_EMPR = A.FCAT_EMPR
                    
                 AND C.DOC_CLAVE = E.CUO_CLAVE_DOC
                 AND C.DOC_EMPR = E.CUO_EMPR
                    
                 AND TM.TMOV_CODIGO = C.DOC_TIPO_MOV
                 AND TM.TMOV_EMPR = C.DOC_EMPR
                    
                 AND NVL(C.DOC_LINEA_NEGOCIO, co_lin_neg_no_operativo) = LL.LIN_CODIGO
                 AND NVL(C.DOC_EMPR, co_empr_tagro) = LL.LIN_EMPR
                    
                 AND ((DOC_FEC_DOC <=
                     DECODE(I_CHK_CUOTA, 'N', I_FEC_VTO, NULL) AND
                     CUO_FEC_VTO <= (LAST_DAY(I_FEC_VTO))) OR
                     CUO_FEC_VTO <=
                     DECODE(I_CHK_CUOTA, 'S', I_FEC_VTO, NULL))
                    
                 AND C.DOC_SALDO_MON > 0
                 AND E.CUO_SALDO_MON > 0
                 AND C.DOC_TIPO_MOV = l_tmv_fact_cre_emit
                 AND C.DOC_MON = co_mnd_gs
                    
                 AND A.FCAT_CODIGO = co_funcionario_hila
                    
                 AND (C.DOC_CLI = I_CLIENTE OR I_CLIENTE IS NULL)
                 AND D.CLI_CODIGO = f.EMPL_CODIGO_CLI(+)
                 AND D.CLI_EMPR   = f.EMPL_EMPRESA(+)
                    
                 AND C.DOC_PER_CONCEPTO IS NULL
                 AND (DOC_FEC_DOC <= I_FEC_CORTE OR I_FEC_CORTE IS NULL)
                 and in_empr_grupo is null
              UNION
              SELECT A.FCAT_CODIGO,
                     A.FCAT_DESC,
                     D.CLI_CODIGO,
                     D.CLI_NOM,
                     DECODE(A.FCAT_CODIGO,
                            co_funcionario_tagro,
                            F.EMPL_LEGAJO,
                            D.CLI_COD_EMPL_EMPR_ORIG) CLI_COD_EMPL_EMPR_ORIG,
                     C.DOC_NRO_DOC,
                     C.DOC_FEC_DOC,
                     E.CUO_CLAVE_DOC,
                     E.CUO_FEC_VTO,
                     E.CUO_SALDO_MON CUO_IMP_MON,
                     NVL(C.DOC_LINEA_NEGOCIO, co_lin_neg_no_operativo) LINEA_NEGOCIO,
                     LL.LIN_DESC,
                     C.DOC_TIPO_MOV,
                     TM.TMOV_ABREV,
                     LL.LIN_FCON_HIL,
                     null cod_prov_destino
                FROM FIN_DOCUMENTO     C,
                     FIN_CLIENTE       D,
                     FAC_CATEGORIA     A,
                     FIN_CUOTA         E,
                     PER_EMPLEADO      F,
                     FAC_LINEA_NEGOCIO LL,
                     GEN_TIPO_MOV      TM
               WHERE C.DOC_EMPR = co_empr_tagro
                 AND C.DOC_PROV = D.CLI_CODIGO ----ESTA PARTE SE RELACION CON PROV, PARA NO VOLVER A RELACIONAR CON FINPERSONA
                 AND C.DOC_EMPR = D.CLI_EMPR ---POR QUE CLIENTE Y PROVEEDOR TIENEN EN MISMO CODIGO EN TRANSAGRO
                    
                 AND D.CLI_CATEG = A.FCAT_CODIGO
                 AND D.CLI_EMPR = A.FCAT_EMPR
                    
                 AND C.DOC_CLAVE = E.CUO_CLAVE_DOC
                 AND C.DOC_EMPR = E.CUO_EMPR
                    
                 AND TM.TMOV_CODIGO = C.DOC_TIPO_MOV
                 AND TM.TMOV_EMPR = C.DOC_EMPR
                    
                 AND NVL(C.DOC_LINEA_NEGOCIO, co_lin_neg_no_operativo) = LL.LIN_CODIGO
                 AND NVL(C.DOC_EMPR, co_empr_tagro) = LL.LIN_EMPR
                    
                 AND E.CUO_FEC_VTO <= I_FEC_VTO
                    
                 AND C.DOC_SALDO_MON > 0
                 AND E.CUO_SALDO_MON > 0
                 AND C.DOC_TIPO_MOV = l_tmv_adelanto_prov
                 AND C.DOC_MON = co_mnd_gs
                    
                 AND A.FCAT_CODIGO = co_funcionario_hila
                    
                 AND (C.DOC_CLI = I_CLIENTE OR I_CLIENTE IS NULL)
                 AND D.CLI_CODIGO = EMPL_CODIGO_CLI(+)
                 AND D.CLI_EMPR = EMPL_EMPRESA(+)
                    
                 AND C.DOC_PER_CONCEPTO IS NULL
                 AND (DOC_FEC_DOC <= I_FEC_CORTE OR I_FEC_CORTE IS NULL)
                 and in_empr_grupo is null
                -- FUNCIONARIOS DEL GRUPO
                union all
                SELECT A.FCAT_CODIGO,
                     A.FCAT_DESC,
                     D.CLI_CODIGO,
                     D.CLI_NOM,
                     DECODE(A.FCAT_CODIGO,
                            co_funcionario_tagro,
                            F.EMPL_LEGAJO,
                            D.CLI_COD_EMPL_EMPR_ORIG) CLI_COD_EMPL_EMPR_ORIG,
                     C.DOC_NRO_DOC,
                     C.DOC_FEC_DOC,
                     E.CUO_CLAVE_DOC,
                     E.CUO_FEC_VTO,
                     E.CUO_SALDO_MON CUO_IMP_MON,
                     NVL(C.DOC_LINEA_NEGOCIO, co_lin_neg_no_operativo) LINEA_NEGOCIO,
                     LL.LIN_DESC,
                     C.DOC_TIPO_MOV,
                     TM.TMOV_ABREV,
                     lld.concepto_id LIN_FCON_HIL,
                     f.empl_codigo_prov cod_prov_destino
                FROM FIN_DOCUMENTO     C,
                     FIN_CLIENTE       D,
                     FAC_CATEGORIA     A,
                     FIN_CUOTA         E,
                     (select eg.cod_empl empl_legajo,
                             eg.cod_prov empl_codigo_prov,
                             eg.cod_empr empl_empresa
                      from table(fini237.pipe_per_empleado_grupo(in_empr_grupo => in_empr_grupo)) eg
                     ) F,
                     FAC_LINEA_NEGOCIO LL,
                     fac_linea_negocio_det lld,
                     GEN_TIPO_MOV      TM
               WHERE C.DOC_EMPR = co_empr_tagro
                 AND C.DOC_CLI  = D.CLI_CODIGO
                 AND C.DOC_EMPR = D.CLI_EMPR
                    
                 AND D.CLI_CATEG = A.FCAT_CODIGO
                 AND D.CLI_EMPR  = A.FCAT_EMPR
                    
                 AND C.DOC_CLAVE = E.CUO_CLAVE_DOC
                 AND C.DOC_EMPR  = E.CUO_EMPR
                    
                 AND TM.TMOV_CODIGO = C.DOC_TIPO_MOV
                 AND TM.TMOV_EMPR   = C.DOC_EMPR
                    
                 AND NVL(C.DOC_LINEA_NEGOCIO, co_lin_neg_no_operativo) = LL.LIN_CODIGO
                 AND NVL(C.DOC_EMPR, co_empr_tagro)                    = LL.LIN_EMPR
                 
                 and ll.lin_codigo = lld.linea_neg_id
                 and ll.lin_empr   = lld.empresa_id
                 and lld.empresa_grupo_id = in_empr_grupo
                 
                 AND (
                     (DOC_FEC_DOC <= DECODE(I_CHK_CUOTA, 'N', I_FEC_VTO, NULL) AND CUO_FEC_VTO <= (LAST_DAY(I_FEC_VTO)))
                     OR
                     CUO_FEC_VTO <= DECODE(I_CHK_CUOTA, 'S', I_FEC_VTO, NULL)
                     )
                    
                 AND C.DOC_SALDO_MON > 0
                 AND E.CUO_SALDO_MON > 0
                 AND C.DOC_TIPO_MOV = l_tmv_fact_cre_emit
                 AND C.DOC_MON      = co_mnd_gs
                    
                 AND A.FCAT_CODIGO = l_funcionario_grupo
                    
                 AND (C.DOC_CLI = I_CLIENTE OR I_CLIENTE IS NULL)
                 
                 AND D.cli_cod_empl_empr_orig  = f.empl_legajo
                 and d.cli_empresa_funcionario = f.empl_empresa
                  
                 AND C.DOC_PER_CONCEPTO IS NULL
                 AND (DOC_FEC_DOC <= I_FEC_CORTE OR I_FEC_CORTE IS NULL)
                 UNION
                 SELECT A.FCAT_CODIGO,
                     A.FCAT_DESC,
                     D.CLI_CODIGO,
                     D.CLI_NOM,
                     DECODE(A.FCAT_CODIGO,
                            co_funcionario_tagro,
                            F.EMPL_LEGAJO,
                            D.CLI_COD_EMPL_EMPR_ORIG) CLI_COD_EMPL_EMPR_ORIG,
                     C.DOC_NRO_DOC,
                     C.DOC_FEC_DOC,
                     E.CUO_CLAVE_DOC,
                     E.CUO_FEC_VTO,
                     E.CUO_SALDO_MON CUO_IMP_MON,
                     NVL(C.DOC_LINEA_NEGOCIO, co_lin_neg_no_operativo) LINEA_NEGOCIO,
                     LL.LIN_DESC,
                     C.DOC_TIPO_MOV,
                     TM.TMOV_ABREV,
                     lld.concepto_id LIN_FCON_HIL,
                     f.empl_codigo_prov cod_prov_destino
                FROM FIN_DOCUMENTO     C,
                     FIN_CLIENTE       D,
                     FAC_CATEGORIA     A,
                     FIN_CUOTA         E,
                     (select eg.cod_empl empl_legajo,
                             eg.cod_prov empl_codigo_prov,
                             eg.cod_empr empl_empresa
                      from table(fini237.pipe_per_empleado_grupo(in_empr_grupo => in_empr_grupo)) eg
                     ) F,
                     FAC_LINEA_NEGOCIO LL,
                     fac_linea_negocio_det lld,
                     GEN_TIPO_MOV      TM
               WHERE C.DOC_EMPR = co_empr_tagro
                 AND C.DOC_PROV = D.CLI_CODIGO ----ESTA PARTE SE RELACION CON PROV, PARA NO VOLVER A RELACIONAR CON FINPERSONA
                 AND C.DOC_EMPR = D.CLI_EMPR ---POR QUE CLIENTE Y PROVEEDOR TIENEN EN MISMO CODIGO EN TRANSAGRO
                    
                 AND D.CLI_CATEG = A.FCAT_CODIGO
                 AND D.CLI_EMPR  = A.FCAT_EMPR
                    
                 AND C.DOC_CLAVE = E.CUO_CLAVE_DOC
                 AND C.DOC_EMPR  = E.CUO_EMPR
                    
                 AND TM.TMOV_CODIGO = C.DOC_TIPO_MOV
                 AND TM.TMOV_EMPR   = C.DOC_EMPR
                    
                 AND NVL(C.DOC_LINEA_NEGOCIO, co_lin_neg_no_operativo) = LL.LIN_CODIGO
                 AND NVL(C.DOC_EMPR, co_empr_tagro)                    = LL.LIN_EMPR
                    
                 and ll.lin_codigo = lld.linea_neg_id
                 and ll.lin_empr   = lld.empresa_id
                 and lld.empresa_grupo_id = in_empr_grupo
                 
                 AND E.CUO_FEC_VTO <= I_FEC_VTO
                    
                 AND C.DOC_SALDO_MON > 0
                 AND E.CUO_SALDO_MON > 0
                 AND C.DOC_TIPO_MOV = l_tmv_adelanto_prov
                 AND C.DOC_MON      = co_mnd_gs
                    
                 AND A.FCAT_CODIGO = l_funcionario_grupo
                    
                 AND (C.DOC_CLI = I_CLIENTE OR I_CLIENTE IS NULL)
                 
                 AND D.cli_empresa_funcionario = f.empl_empresa
                 AND D.CLI_COD_EMPL_EMPR_ORIG  = f.empl_legajo
                    
                 AND C.DOC_PER_CONCEPTO IS NULL
                 AND (DOC_FEC_DOC <= I_FEC_CORTE OR I_FEC_CORTE IS NULL)
               ORDER BY 3, 6, 7
               ) LOOP
    
      IF  I_CATEG_CLI not in ( co_funcionario_tagro, l_funcionario_grupo)
      THEN
        IF V.CLI_COD_EMPL_EMPR_ORIG IS NULL THEN
          RAISE_APPLICATION_ERROR(-20010,
                                  'Esta persona :' || V.CLI_NOM ||
                                  ' no posee un codigo de empleado, puede provocar algunos errores si continua');
        END IF;
        
        BEGIN
          SELECT EMPL_CODIGO_PROV
            INTO V_PROV_DEST
            FROM PER_EMPLEADO
           WHERE EMPL_EMPRESA = V_EMPRESA
             AND EMPL_LEGAJO = V.CLI_COD_EMPL_EMPR_ORIG;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20001,'FAVOR VERIFICAR FICHA DEL CLIENTE '||V.CLI_CODIGO||'. '||V.CLI_NOM);
        END;
      END IF;
      
    
      PP_ADD_MEMBER(I_EMPRESA       => I_EMPRESA,
                    I_CLIENTE       => V.CLI_CODIGO,
                    I_NOMBRE        => V.CLI_NOM,
                    I_PROV_DEST     => case
                                         when I_CATEG_CLI = l_funcionario_grupo then
                                           v.cod_prov_destino
                                         else
                                           V_PROV_DEST
                                        end,
                    I_FACTURA       => V.DOC_NRO_DOC,
                    I_FECHA         => V.DOC_FEC_DOC,
                    I_FEC_CUOTA     => V.CUO_FEC_VTO,
                    I_IMPORTE       => V.CUO_IMP_MON,
                    I_TIPO_MOV      => V.DOC_TIPO_MOV,
                    I_LINEA_NEGOCIO => V.LINEA_NEGOCIO,
                    I_EMPLEADO      => V.CLI_COD_EMPL_EMPR_ORIG,
                    I_TMOV_ABREV    => V.TMOV_ABREV,
                    I_LNEGOCIO_DESC => V.LIN_DESC,
                    I_DOC_CLAVE     => V.CUO_CLAVE_DOC,
                    I_CAT_CODIGO    => V.FCAT_CODIGO,
                    I_DOC_FCON_HIL  => V.LIN_FCON_HIL,
                    in_empresa_func => in_empr_grupo
                    );
    
      C := C + 1;
    
    END LOOP;
  END PP_CARGAR_DETALLE_HIL_C;

  PROCEDURE PP_CARGAR_DETALLE_HIL(I_EMPRESA   IN NUMBER,
                                  I_CATEG_CLI IN NUMBER,
                                  I_CHK_CUOTA IN VARCHAR2,
                                  I_FEC_VTO   IN date,
                                  I_CLIENTE   IN NUMBER,
                                  I_FEC_CORTE IN date,
                                  in_empr_grupo in gen_empresa.empr_codigo%type := null
                                  ) IS
    C           NUMBER := 0;
    V_EMPRESA   NUMBER := 0;
    V_PROV_DEST NUMBER;
    l_funcionario_grupo fin_configuracion.conf_cat_cli_func_grupo%type;
    l_tmv_fact_cre_emit fin_configuracion.conf_fact_cr_emit%type;
    l_tmv_adelanto_prov fin_configuracion.conf_adelanto_pro%type;
  begin
    
    l_funcionario_grupo := obt_cat_cli_func_grupo(in_empresa => i_empresa);
    
    IF I_CATEG_CLI = co_funcionario_hila THEN
      V_EMPRESA := co_empr_hilagro;
    ELSIF I_CATEG_CLI = co_funcionario_halt THEN
      V_EMPRESA := co_empr_halten;
    elsif i_categ_cli = l_funcionario_grupo then
      if i_cliente is not null then
        V_EMPRESA := obt_empresa_func_grupo( in_cliente => i_cliente, in_empresa => i_empresa );
      else
        v_empresa := in_empr_grupo;
      end if;  
    ELSE
      V_EMPRESA := I_EMPRESA;
    END IF;
    
    -- obt el tmv fact cred emitida
    l_tmv_fact_cre_emit := obt_tmv_fact_cred_emit( in_empresa => i_empresa );
    l_tmv_adelanto_prov := obt_tmv_adelanto_prov( in_empresa => i_empresa );
    
    FOR V IN (SELECT A.FCAT_CODIGO,
                     A.FCAT_DESC,
                     D.CLI_CODIGO,
                     D.CLI_NOM,
                     DECODE(A.FCAT_CODIGO,
                            co_funcionario_tagro,
                            F.EMPL_LEGAJO,
                            D.CLI_COD_EMPL_EMPR_ORIG) CLI_COD_EMPL_EMPR_ORIG,
                     C.DOC_NRO_DOC,
                     C.DOC_FEC_DOC,
                     E.CUO_CLAVE_DOC,
                     E.CUO_FEC_VTO,
                     E.CUO_SALDO_MON CUO_IMP_MON,
                     NVL(C.DOC_LINEA_NEGOCIO, co_lin_neg_no_operativo) LINEA_NEGOCIO, ---9 ES NO OPERATIVO,
                     LL.LIN_DESC,
                     C.DOC_TIPO_MOV,
                     TM.TMOV_ABREV,
                     LL.LIN_FCON_HIL,
                     null cod_prov_destino
                FROM FIN_DOCUMENTO     C,
                     FIN_CLIENTE       D,
                     FAC_CATEGORIA     A,
                     FIN_CUOTA         E,
                     PER_EMPLEADO      F,
                     FAC_LINEA_NEGOCIO LL,
                     GEN_TIPO_MOV      TM
               WHERE C.DOC_EMPR = co_empr_tagro
                 AND C.DOC_CLI = D.CLI_CODIGO
                 AND C.DOC_EMPR = D.CLI_EMPR
                    
                 AND D.CLI_CATEG = A.FCAT_CODIGO
                 AND D.CLI_EMPR = A.FCAT_EMPR
                    
                 AND C.DOC_CLAVE = E.CUO_CLAVE_DOC
                 AND C.DOC_EMPR = E.CUO_EMPR
                    
                 AND TM.TMOV_CODIGO = C.DOC_TIPO_MOV
                 AND TM.TMOV_EMPR = C.DOC_EMPR
                    
                 AND NVL(C.DOC_LINEA_NEGOCIO, co_lin_neg_no_operativo) = LL.LIN_CODIGO
                 AND NVL(C.DOC_EMPR, co_empr_tagro) = LL.LIN_EMPR
                    
                 AND FINI237.WHERE_PP_CARGAR_DETALLE(DOC_FEC_DOC,
                                                     CUO_FEC_VTO,
                                                     I_FEC_VTO,
                                                     I_CHK_CUOTA) = 1
                    
                 AND (E.CUO_FEC_VTO <= I_FEC_VTO OR I_FEC_VTO IS NULL)
                    
                 AND C.DOC_SALDO_MON > 0
                 AND E.CUO_SALDO_MON > 0
                 AND C.DOC_TIPO_MOV = l_tmv_fact_cre_emit
                 AND C.DOC_MON      = co_mnd_gs
                    
                 AND A.FCAT_CODIGO = co_funcionario_hila
                    
                 AND (C.DOC_CLI = I_CLIENTE OR I_CLIENTE IS NULL)
                 AND D.CLI_CODIGO = EMPL_CODIGO_CLI(+)
                 AND D.CLI_EMPR   = EMPL_EMPRESA(+)
                 AND C.DOC_PER_CONCEPTO IS NULL
                 AND (DOC_FEC_DOC <= I_FEC_CORTE OR I_FEC_CORTE IS NULL)
                 and in_empr_grupo is null
              UNION
              SELECT A.FCAT_CODIGO,
                     A.FCAT_DESC,
                     D.CLI_CODIGO,
                     D.CLI_NOM,
                     DECODE(A.FCAT_CODIGO,
                            co_funcionario_tagro,
                            F.EMPL_LEGAJO,
                            D.CLI_COD_EMPL_EMPR_ORIG) CLI_COD_EMPL_EMPR_ORIG,
                     C.DOC_NRO_DOC,
                     C.DOC_FEC_DOC,
                     E.CUO_CLAVE_DOC,
                     E.CUO_FEC_VTO,
                     E.CUO_SALDO_MON CUO_IMP_MON,
                     NVL(C.DOC_LINEA_NEGOCIO, co_lin_neg_no_operativo) LINEA_NEGOCIO,
                     LL.LIN_DESC,
                     C.DOC_TIPO_MOV,
                     TM.TMOV_ABREV,
                     LL.LIN_FCON_HIL,
                     null cod_prov_destino
                FROM FIN_DOCUMENTO     C,
                     FIN_CLIENTE       D,
                     FAC_CATEGORIA     A,
                     FIN_CUOTA         E,
                     PER_EMPLEADO      F,
                     FAC_LINEA_NEGOCIO LL,
                     GEN_TIPO_MOV      TM
               WHERE C.DOC_EMPR = co_empr_tagro
                 AND C.DOC_PROV = D.CLI_CODIGO ----ESTA PARTE SE RELACION CON PROV, PARA NO VOLVER A RELACIONAR CON FINPERSONA
                 AND C.DOC_EMPR = D.CLI_EMPR ---POR QUE CLIENTE Y PROVEEDOR TIENEN EN MISMO CODIGO EN TRANSAGRO
                    
                 AND D.CLI_CATEG = A.FCAT_CODIGO
                 AND D.CLI_EMPR  = A.FCAT_EMPR
                    
                 AND C.DOC_CLAVE = E.CUO_CLAVE_DOC
                 AND C.DOC_EMPR  = E.CUO_EMPR
                    
                 AND TM.TMOV_CODIGO = C.DOC_TIPO_MOV
                 AND TM.TMOV_EMPR   = C.DOC_EMPR
                    
                 AND NVL(C.DOC_LINEA_NEGOCIO, co_lin_neg_no_operativo) = LL.LIN_CODIGO
                 AND NVL(C.DOC_EMPR, co_empr_tagro) = LL.LIN_EMPR
                    
                 AND (E.CUO_FEC_VTO <= I_FEC_VTO OR I_FEC_VTO IS NULL)
                    
                 AND C.DOC_SALDO_MON > 0
                 AND E.CUO_SALDO_MON > 0
                 AND C.DOC_TIPO_MOV = l_tmv_adelanto_prov
                 AND C.DOC_MON = co_mnd_gs
                    
                 AND A.FCAT_CODIGO = co_funcionario_hila
                    
                 AND (C.DOC_CLI = I_CLIENTE OR I_CLIENTE IS NULL)
                 AND D.CLI_CODIGO = EMPL_CODIGO_CLI(+)
                 AND D.CLI_EMPR = EMPL_EMPRESA(+)
                 AND C.DOC_PER_CONCEPTO IS NULL
                 AND (DOC_FEC_DOC <= I_FEC_CORTE OR I_FEC_CORTE IS NULL)
                 and in_empr_grupo is null
               -- FUNCIONARIOS DE GRUPO DE EMPRESAS
               union all
               SELECT A.FCAT_CODIGO,
                     A.FCAT_DESC,
                     D.CLI_CODIGO,
                     D.CLI_NOM,
                     DECODE(A.FCAT_CODIGO,
                            co_funcionario_tagro,
                            F.EMPL_LEGAJO,
                            D.CLI_COD_EMPL_EMPR_ORIG) CLI_COD_EMPL_EMPR_ORIG,
                     C.DOC_NRO_DOC,
                     C.DOC_FEC_DOC,
                     E.CUO_CLAVE_DOC,
                     E.CUO_FEC_VTO,
                     E.CUO_SALDO_MON CUO_IMP_MON,
                     NVL(C.DOC_LINEA_NEGOCIO, co_lin_neg_no_operativo) LINEA_NEGOCIO,
                     LL.LIN_DESC,
                     C.DOC_TIPO_MOV,
                     TM.TMOV_ABREV,
                     LLd.concepto_id LIN_FCON_HIL,
                     f.empl_codigo_prov cod_prov_destino
                FROM FIN_DOCUMENTO     C,
                     FIN_CLIENTE       D,
                     FAC_CATEGORIA     A,
                     FIN_CUOTA         E,
                     (select eg.cod_empl empl_legajo,
                             eg.cod_prov empl_codigo_prov,
                             eg.cod_empr empl_empresa
                      from table(fini237.pipe_per_empleado_grupo(in_empr_grupo => in_empr_grupo)) eg
                     ) F,
                     FAC_LINEA_NEGOCIO LL,
                     fac_linea_negocio_det lld,
                     GEN_TIPO_MOV      TM
               WHERE C.DOC_EMPR = co_empr_tagro
                 AND C.DOC_CLI  = D.CLI_CODIGO
                 AND C.DOC_EMPR = D.CLI_EMPR
                    
                 AND D.CLI_CATEG = A.FCAT_CODIGO
                 AND D.CLI_EMPR  = A.FCAT_EMPR
                    
                 AND C.DOC_CLAVE = E.CUO_CLAVE_DOC
                 AND C.DOC_EMPR  = E.CUO_EMPR
                    
                 AND TM.TMOV_CODIGO = C.DOC_TIPO_MOV
                 AND TM.TMOV_EMPR   = C.DOC_EMPR
                    
                 AND NVL(C.DOC_LINEA_NEGOCIO, co_lin_neg_no_operativo) = LL.LIN_CODIGO
                 AND NVL(C.DOC_EMPR, co_empr_tagro) = LL.LIN_EMPR
                 
                 and ll.lin_codigo = lld.linea_neg_id
                 and ll.lin_empr   = lld.empresa_id
                 and lld.empresa_grupo_id = in_empr_grupo
                    
                 AND FINI237.WHERE_PP_CARGAR_DETALLE(DOC_FEC_DOC,
                                                     CUO_FEC_VTO,
                                                     I_FEC_VTO,
                                                     I_CHK_CUOTA) = 1
                    
                 AND (E.CUO_FEC_VTO <= I_FEC_VTO OR I_FEC_VTO IS NULL)
                    
                 AND C.DOC_SALDO_MON > 0
                 AND E.CUO_SALDO_MON > 0
                 AND C.DOC_TIPO_MOV = l_tmv_fact_cre_emit
                 AND C.DOC_MON      = co_mnd_gs
                    
                 AND A.FCAT_CODIGO = l_funcionario_grupo
                    
                 AND (C.DOC_CLI = I_CLIENTE OR I_CLIENTE IS NULL)
                 
                 AND D.cli_empresa_funcionario = f.empl_empresa
                 AND D.CLI_COD_EMPL_EMPR_ORIG  = f.empl_legajo
                 
                 AND C.DOC_PER_CONCEPTO IS NULL
                 AND (DOC_FEC_DOC <= I_FEC_CORTE OR I_FEC_CORTE IS NULL)
                 UNION
                 SELECT A.FCAT_CODIGO,
                     A.FCAT_DESC,
                     D.CLI_CODIGO,
                     D.CLI_NOM,
                     DECODE(A.FCAT_CODIGO,
                            co_funcionario_tagro,
                            F.EMPL_LEGAJO,
                            D.CLI_COD_EMPL_EMPR_ORIG) CLI_COD_EMPL_EMPR_ORIG,
                     C.DOC_NRO_DOC,
                     C.DOC_FEC_DOC,
                     E.CUO_CLAVE_DOC,
                     E.CUO_FEC_VTO,
                     E.CUO_SALDO_MON CUO_IMP_MON,
                     NVL(C.DOC_LINEA_NEGOCIO, co_lin_neg_no_operativo) LINEA_NEGOCIO,
                     LL.LIN_DESC,
                     C.DOC_TIPO_MOV,
                     TM.TMOV_ABREV,
                     LLd.concepto_id LIN_FCON_HIL,
                     f.empl_codigo_prov cod_prov_destino
                FROM FIN_DOCUMENTO     C,
                     FIN_CLIENTE       D,
                     FAC_CATEGORIA     A,
                     FIN_CUOTA         E,
                     (select eg.cod_empl empl_legajo,
                             eg.cod_prov empl_codigo_prov,
                             eg.cod_empr empl_empresa
                      from table(fini237.pipe_per_empleado_grupo(in_empr_grupo => in_empr_grupo)) eg
                     ) F,
                     FAC_LINEA_NEGOCIO LL,
                     fac_linea_negocio_det lld,
                     GEN_TIPO_MOV      TM
               WHERE C.DOC_EMPR = co_empr_tagro
                 AND C.DOC_PROV = D.CLI_CODIGO ----ESTA PARTE SE RELACION CON PROV, PARA NO VOLVER A RELACIONAR CON FINPERSONA
                 AND C.DOC_EMPR = D.CLI_EMPR ---POR QUE CLIENTE Y PROVEEDOR TIENEN EN MISMO CODIGO EN TRANSAGRO
                    
                 AND D.CLI_CATEG = A.FCAT_CODIGO
                 AND D.CLI_EMPR  = A.FCAT_EMPR
                    
                 AND C.DOC_CLAVE = E.CUO_CLAVE_DOC
                 AND C.DOC_EMPR  = E.CUO_EMPR
                    
                 AND TM.TMOV_CODIGO = C.DOC_TIPO_MOV
                 AND TM.TMOV_EMPR   = C.DOC_EMPR
                    
                 AND NVL(C.DOC_LINEA_NEGOCIO, co_lin_neg_no_operativo) = LL.LIN_CODIGO
                 AND NVL(C.DOC_EMPR, co_empr_tagro) = LL.LIN_EMPR
                 
                 and ll.lin_codigo = lld.linea_neg_id
                 and ll.lin_empr   = lld.empresa_id
                 and lld.empresa_grupo_id = in_empr_grupo
                    
                 AND (E.CUO_FEC_VTO <= I_FEC_VTO OR I_FEC_VTO IS NULL)
                    
                 AND C.DOC_SALDO_MON > 0
                 AND E.CUO_SALDO_MON > 0
                 AND C.DOC_TIPO_MOV = l_tmv_adelanto_prov
                 AND C.DOC_MON      = co_mnd_gs
                    
                 AND A.FCAT_CODIGO = l_funcionario_grupo
                    
                 AND (C.DOC_CLI = I_CLIENTE OR I_CLIENTE IS NULL)
                 
                 AND D.cli_empresa_funcionario = f.empl_empresa
                 AND D.CLI_COD_EMPL_EMPR_ORIG  = f.empl_legajo
                 
                 AND C.DOC_PER_CONCEPTO IS NULL
                 AND (DOC_FEC_DOC <= I_FEC_CORTE OR I_FEC_CORTE IS NULL)
               ORDER BY 3, 6, 7
               ) LOOP
    
      IF I_CATEG_CLI <> co_funcionario_tagro THEN
        IF V.CLI_COD_EMPL_EMPR_ORIG IS NULL THEN
          RAISE_APPLICATION_ERROR(-20010,
                                  'Esta persona :' || V.CLI_NOM ||
                                  ' no posee un codigo de empleado, puede provocar algunos errores si continua');
        END IF;
      
        -- en el caso que sea funcionario de grupo esta en la query ya a donde debe registrar
        if i_categ_cli = l_funcionario_grupo then
          V_PROV_DEST := v.cod_prov_destino;
        else
          SELECT EMPL_CODIGO_PROV
            INTO V_PROV_DEST
            FROM PER_EMPLEADO
           WHERE EMPL_EMPRESA = V_EMPRESA
             AND EMPL_LEGAJO = V.CLI_COD_EMPL_EMPR_ORIG;
        end if;
        
        PP_ADD_MEMBER(I_EMPRESA       => I_EMPRESA,
                      I_CLIENTE       => V.CLI_CODIGO,
                      I_NOMBRE        => V.CLI_NOM,
                      I_PROV_DEST     => V_PROV_DEST,
                      I_FACTURA       => V.DOC_NRO_DOC,
                      I_FECHA         => V.DOC_FEC_DOC,
                      I_FEC_CUOTA     => V.CUO_FEC_VTO,
                      I_IMPORTE       => V.CUO_IMP_MON,
                      I_TIPO_MOV      => V.DOC_TIPO_MOV,
                      I_LINEA_NEGOCIO => V.LINEA_NEGOCIO,
                      I_EMPLEADO      => V.CLI_COD_EMPL_EMPR_ORIG,
                      I_TMOV_ABREV    => V.TMOV_ABREV,
                      I_LNEGOCIO_DESC => V.LIN_DESC,
                      I_DOC_CLAVE     => V.CUO_CLAVE_DOC,
                      I_CAT_CODIGO    => V.FCAT_CODIGO,
                      I_DOC_FCON_HIL  => V.LIN_FCON_HIL,
                      in_empresa_func => in_empr_grupo
                      );
      
      END IF;
    
    END LOOP;
  END PP_CARGAR_DETALLE_HIL;

  PROCEDURE PP_TRAER_CLIENTE_RESTRING(I_EMPRESA IN NUMBER,
                                      
                                      I_CLIENTE IN NUMBER) IS
    V_CLI_NOM VARCHAR2(500);
  BEGIN
    NULL;
    SELECT CLI_NOM
      INTO V_CLI_NOM
      FROM FIN_CLIENTE
     WHERE CLI_CATEG = 4
       AND CLI_CODIGO = I_CLIENTE
       AND CLI_EMPR = I_EMPRESA;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20010,
                              'Cliente inexistente o esta restringido para el Usuario');
    
  END PP_TRAER_CLIENTE_RESTRING;

  PROCEDURE PP_CONSULTAR(I_EMPRESA       IN NUMBER,
                         I_CATEG_CLI     IN NUMBER,
                         I_CHK_CUOTA     IN VARCHAR2,
                         I_FEC_VTO       IN DATE,
                         I_LINEA_NEGOCIO IN NUMBER,
                         I_CLIENTE       IN NUMBER,
                         I_FEC_CORTE     IN DATE,
                         in_empr_grupo    in gen_empresa.empr_codigo%type := null
                         ) AS
  
  l_cat_funcionario_grupo  fin_configuracion.conf_cat_cli_func_grupo%type;
  l_consulta_es_para_grupo number;
  BEGIN
  
    PP_INICIAR_COLLECTION;
  
    IF I_LINEA_NEGOCIO IN (co_lin_neg_uniforme, co_lin_neg_no_operativo) then
    
      IF I_CATEG_CLI <> co_funcionario_tagro THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'No puede cargar esta categoria a empresa externa');
      END IF;
    
      IF I_LINEA_NEGOCIO = co_lin_neg_no_operativo then
        IF I_CHK_CUOTA = 'S' then
        
          PP_CARGAR_DETALLE_NO_OPER_C(I_EMPRESA   => I_EMPRESA,
                                      I_CHK_CUOTA => I_CHK_CUOTA,
                                      I_FEC_VTO   => I_FEC_VTO,
                                      I_CLIENTE   => I_CLIENTE,
                                      I_FEC_CORTE => I_FEC_CORTE);
        
        else
        
          PP_CARGAR_DETALLE_NO_OPER(I_EMPRESA   => I_EMPRESA,
                                    I_CHK_CUOTA => I_CHK_CUOTA,
                                    I_FEC_VTO   => I_FEC_VTO,
                                    I_CLIENTE   => I_CLIENTE,
                                    I_FEC_CORTE => I_FEC_CORTE);
        
        END IF;
      else
        PP_CARGAR_DET_NO_OPER_VARIOS(I_EMPRESA   => I_EMPRESA,
                                     I_CHK_CUOTA => I_CHK_CUOTA,
                                     I_FEC_VTO   => I_FEC_VTO,
                                     I_CLIENTE   => I_CLIENTE,
                                     I_FEC_CORTE => I_FEC_CORTE);
      END IF;
    
    ELSE
      IF I_CATEG_CLI <> co_funcionario_tagro then
        -- 22/08/2022 9:55:37 @PabloACespedes \(^-^)/
        -- validar que se_elija la empresa si es la categoria de funcionarios del grupo
        l_cat_funcionario_grupo := obt_cat_cli_func_grupo(in_empresa => i_empresa);
        
        -- prevalidacion para ver si es necesario enviar el argumento o solo pasar nulo al cargar el detalle
        if l_cat_funcionario_grupo = I_CATEG_CLI then
          l_consulta_es_para_grupo := 1;
        else
          l_consulta_es_para_grupo := 0;
        end if;
        
        -- si es categoria del grupo y no tiene la empresa, lanza el error
        if l_consulta_es_para_grupo = 1
        and in_empr_grupo is null then
          apex_error.add_error (
            p_message          => 'Es necesario seleccionar la empresa del grupo para realizar la consulta!',
            p_display_location =>  apex_error.c_inline_with_field_and_notif,
            p_page_item_name   => 'P6504_EMPRESA_GRUPO'
          );
        end if;
         
        IF I_CHK_CUOTA = 'S' then
          PP_CARGAR_DETALLE_HIL_C(I_EMPRESA     => I_EMPRESA,
                                  I_CATEG_CLI   => I_CATEG_CLI,
                                  I_CHK_CUOTA   => I_CHK_CUOTA,
                                  I_FEC_VTO     => I_FEC_VTO,
                                  I_CLIENTE     => I_CLIENTE,
                                  I_FEC_CORTE   => I_FEC_CORTE,
                                  in_empr_grupo => case when l_consulta_es_para_grupo = 1 then in_empr_grupo else null end
                                  );
        else
          PP_CARGAR_DETALLE_HIL(I_EMPRESA     => I_EMPRESA,
                                I_CATEG_CLI   => I_CATEG_CLI,
                                I_CHK_CUOTA   => I_CHK_CUOTA,
                                I_FEC_VTO     => I_FEC_VTO,
                                I_CLIENTE     => I_CLIENTE,
                                I_FEC_CORTE   => I_FEC_CORTE,
                                in_empr_grupo => case when l_consulta_es_para_grupo = 1 then in_empr_grupo else null end
                                );
        END IF;
      ELSE
        --   RAISE_APPLICATION_ERROR(-20010, 'PP_CARGAR_DETALLE '||I_FEC_VTO);
        PP_CARGAR_DETALLE(I_EMPRESA       => I_EMPRESA,
                          I_CATEG_CLI     => I_CATEG_CLI,
                          I_CHK_CUOTA     => I_CHK_CUOTA,
                          I_FEC_VTO       => I_FEC_VTO,
                          I_LINEA_NEGOCIO => I_LINEA_NEGOCIO,
                          I_CLIENTE       => I_CLIENTE,
                          I_FEC_CORTE     => I_FEC_CORTE);
      END IF;
    END IF;
  END PP_CONSULTAR;

  PROCEDURE PP_CONSULTAR_EXISTENTE(I_EMPRESA              IN NUMBER,
                                   I_DOC_NRO_TRANS_COMBUS IN NUMBER,
                                   O_DOC_FEC_OPER         OUT DATE,
                                   O_DOC_FEC_DOC          OUT DATE,
                                   O_DOC_NRO_INICIAL      OUT NUMBER,
                                   O_CATEG_CLI            OUT NUMBER) AS
  BEGIN
  
    PP_INICIAR_COLLECTION;
  
    BEGIN
      SELECT FECHA, FEC_OPER, CAT, MIN(NRO_INI)NRO_INI
        INTO O_DOC_FEC_DOC, O_DOC_FEC_OPER, O_CATEG_CLI, O_DOC_NRO_INICIAL
        FROM (SELECT DISTINCT DOC_FEC_DOC FECHA,
                              DOC_FEC_OPER FEC_OPER,
                              FCAT_CODIGO CAT,
                              MIN(DOC_NRO_DOC) NRO_INI
              
                FROM FIN_DOCUMENTO, FIN_CLIENTE, FAC_CATEGORIA
               WHERE DOC_CLI = CLI_CODIGO
                 AND DOC_EMPR = CLI_EMPR
                    
                 AND CLI_CATEG = FCAT_CODIGO
                 AND CLI_EMPR = FCAT_EMPR
                    
                 AND DOC_EMPR = I_EMPRESA
                 AND DOC_NRO_TRANS_COMBUS = I_DOC_NRO_TRANS_COMBUS
               GROUP BY DOC_FEC_DOC, DOC_FEC_OPER, FCAT_CODIGO
              UNION
              SELECT DISTINCT DOC_FEC_DOC,
                              DOC_FEC_OPER,
                              FCAT_CODIGO,
                              MIN(DOC_NRO_DOC) NRO_INI
                FROM FIN_DOCUMENTO, FIN_CLIENTE, FAC_CATEGORIA
               WHERE DOC_PROV = CLI_CODIGO
                 AND DOC_EMPR = CLI_EMPR
                    
                 AND CLI_CATEG = FCAT_CODIGO
                 AND CLI_EMPR = FCAT_EMPR
                    
                 AND DOC_EMPR = I_EMPRESA
                    
                 AND DOC_NRO_TRANS_COMBUS = I_DOC_NRO_TRANS_COMBUS
               GROUP BY DOC_FEC_DOC, DOC_FEC_OPER, FCAT_CODIGO)
                GROUP BY FECHA, FEC_OPER, CAT;
    
      FOR V IN (SELECT A.FCAT_CODIGO,
                       A.FCAT_DESC,
                       D.CLI_CODIGO,
                       D.CLI_NOM,
                       D.CLI_COD_EMPL_EMPR_ORIG,
                       DC.DOC_NRO_DOC,
                       DC.DOC_FEC_DOC,
                       E.PAG_CLAVE_PAGO,
                       E.PAG_FEC_VTO,
                       E.PAG_IMP_MON,
                       C.DOC_CLAVE_FIN_DESTINO,
                       C.DOC_TIPO_MOV
                  FROM FIN_DOCUMENTO  C,
                       FIN_CLIENTE    D,
                       FAC_CATEGORIA  A,
                       FIN_UNION_PAGO E,
                       FIN_DOCUMENTO  DC
                 WHERE C.DOC_EMPR = I_EMPRESA
                   AND C.DOC_CLI = D.CLI_CODIGO
                   AND C.DOC_EMPR = D.CLI_EMPR
                      
                   AND D.CLI_CATEG = A.FCAT_CODIGO
                   AND D.CLI_EMPR = A.FCAT_EMPR
                      
                   AND C.DOC_CLAVE = E.PAG_CLAVE_PAGO
                   AND C.DOC_EMPR = E.PAG_EMPR
                      
                   AND C.DOC_NRO_TRANS_COMBUS = I_DOC_NRO_TRANS_COMBUS
                   AND E.PAG_CLAVE_DOC = DC.DOC_CLAVE
                   AND E.PAG_EMPR = DC.DOC_EMPR
                UNION ALL
                SELECT A.FCAT_CODIGO,
                       A.FCAT_DESC,
                       D.CLI_CODIGO,
                       D.CLI_NOM,
                       D.CLI_COD_EMPL_EMPR_ORIG,
                       DC.DOC_NRO_DOC,
                       DC.DOC_FEC_DOC,
                       E.PAG_CLAVE_PAGO,
                       E.PAG_FEC_VTO,
                       E.PAG_IMP_MON,
                       C.DOC_CLAVE_FIN_DESTINO,
                       C.DOC_TIPO_MOV
                  FROM FIN_DOCUMENTO  C,
                       FIN_CLIENTE    D,
                       FAC_CATEGORIA  A,
                       FIN_UNION_PAGO E,
                       FIN_DOCUMENTO  DC
                 WHERE C.DOC_EMPR = I_EMPRESA
                   AND C.DOC_PROV = D.CLI_CODIGO
                   AND C.DOC_EMPR = D.CLI_EMPR
                      
                   AND D.CLI_CATEG = A.FCAT_CODIGO
                   AND D.CLI_EMPR = A.FCAT_EMPR
                      
                   AND C.DOC_CLAVE = E.PAG_CLAVE_PAGO
                   AND C.DOC_EMPR = E.PAG_EMPR
                      
                   AND C.DOC_NRO_TRANS_COMBUS = I_DOC_NRO_TRANS_COMBUS
                   AND E.PAG_CLAVE_DOC = DC.DOC_CLAVE
                   AND E.PAG_EMPR = DC.DOC_EMPR) LOOP
      
        PP_ADD_MEMBER(I_EMPRESA       => I_EMPRESA,
                      I_CLIENTE       => V.CLI_CODIGO,
                      I_NOMBRE        => V.CLI_NOM,
                      I_PROV_DEST     => '',
                      I_FACTURA       => V.DOC_NRO_DOC,
                      I_FECHA         => V.DOC_FEC_DOC,
                      I_FEC_CUOTA     => V.PAG_FEC_VTO,
                      I_IMPORTE       => V.PAG_IMP_MON,
                      I_TIPO_MOV      => V.DOC_TIPO_MOV,
                      I_LINEA_NEGOCIO => '', --V.LINEA_NEGOCIO,
                      I_EMPLEADO      => V.CLI_COD_EMPL_EMPR_ORIG,
                      I_TMOV_ABREV    => '', --V.TMOV_ABREV,
                      I_LNEGOCIO_DESC => '', --V.LIN_DESC,
                      I_DOC_CLAVE     => V.PAG_CLAVE_PAGO,
                      I_CAT_CODIGO    => V.FCAT_CODIGO,
                      I_DOC_FCON_HIL  => '');
      END LOOP;
    EXCEPTION
      WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20010,
                                'Error en cargando la lista del historico' || '-' ||
                                SQLERRM);
      
    END;
  
  END PP_CONSULTAR_EXISTENTE;

  PROCEDURE PP_CAMBIAR_ESTADO(I_EMPRESA IN NUMBER, I_SEQ IN NUMBER) AS
    V_NEW_ESTADO VARCHAR2(5);
  BEGIN
  
    FOR C IN (SELECT A.SEQ_ID,
                     A.C001   CLIENTE,
                     A.C002   NOMBRE,
                     A.C003   PROV_DEST,
                     A.C004   FACTURA,
                     A.C005   FECHA,
                     A.C006   FEC_CUOTA,
                     A.C007   IMPORTE,
                     A.C008   TIPO_MOV,
                     A.C009   LINEA_NEGOCIO,
                     A.C010   EMPLEADO,
                     A.C011   TMOV_ABREV,
                     A.C012   LNEGOCIO_DESC,
                     A.C013   DOC_CLAVE,
                     A.C014   FCAT_CODIGO,
                     A.C015   PROCESAR
                FROM APEX_COLLECTIONS A
               WHERE A.COLLECTION_NAME = 'FINI257_DETALLE'
                 AND SEQ_ID = I_SEQ) LOOP
    
      IF C.PROCESAR = 'Si' THEN
        V_NEW_ESTADO := 'No';
      ELSE
        V_NEW_ESTADO := 'Si';
      END IF;
    
      APEX_COLLECTION.UPDATE_MEMBER(P_COLLECTION_NAME => 'FINI257_DETALLE',
                                    P_SEQ             => C.SEQ_ID,
                                    P_C001            => C.CLIENTE,
                                    P_C002            => C.NOMBRE,
                                    P_C003            => C.PROV_DEST,
                                    P_C004            => C.FACTURA,
                                    P_C005            => C.FECHA,
                                    P_C006            => C.FEC_CUOTA,
                                    P_C007            => C.IMPORTE,
                                    P_C008            => C.TIPO_MOV,
                                    P_C009            => C.LINEA_NEGOCIO,
                                    P_C010            => C.EMPLEADO,
                                    P_C011            => C.TMOV_ABREV,
                                    P_C012            => C.LNEGOCIO_DESC,
                                    P_C013            => C.DOC_CLAVE,
                                    P_C014            => C.FCAT_CODIGO,
                                    P_C015            => V_NEW_ESTADO);
    
    END LOOP;
  
  END PP_CAMBIAR_ESTADO;

  FUNCTION FP_NFORMAT(I_NUMBER IN NUMBER) RETURN VARCHAR2 AS
  BEGIN
    RETURN TO_CHAR(I_NUMBER, '999G999G999G999');
  END FP_NFORMAT;

  PROCEDURE PP_BORRAR_REGISTRO(I_EMPRESA              IN NUMBER,
                               I_CATEG_CLI            IN NUMBER,
                               I_DOC_NRO_TRANS_COMBUS IN NUMBER) IS
  
  BEGIN
  
    -- IF  FP_CONFIRMAR_REG('?Desea borrar '||:SYSTEM.CURRENT_VALUE||'?','AL_BORRAR') = 'CONFIRMAR' THEN
  
    IF I_CATEG_CLI <> co_funcionario_tagro THEN
      FIN_INS_REC_FUN_HIL(I_OPERACION   => 'D',
                          I_CATEGORIA   => I_CATEG_CLI,
                          I_NRO_PROCESO => I_DOC_NRO_TRANS_COMBUS,
                          IN_EMPRESA    => I_EMPRESA);
    ELSE
    
      FIN_INS_REC_FUN(I_OPERACION   => 'D',
                      I_CATEGORIA   => I_CATEG_CLI,
                      I_NRO_PROCESO => I_DOC_NRO_TRANS_COMBUS,
                      IN_EMPRESA    => I_EMPRESA);
    END IF;
  
  END PP_BORRAR_REGISTRO;
  
  
END FINI237;
/
