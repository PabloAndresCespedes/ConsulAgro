create or replace package FINM008 is
  PROCEDURE PP_ACTUALIZAR_FIN_CONF(P_CONF_EMPR                   IN NUMBER,
                                   P_CONF_PER_ACT_INI            IN DATE,
                                   P_CONF_PER_ACT_FIN            IN DATE,
                                   P_CONF_PER_SGTE_INI           IN DATE,
                                   P_CONF_PER_SGTE_FIN           IN DATE,
                                   P_CONF_IND_HAB_MES_ACT        IN VARCHAR2,
                                   P_CONF_IND_IVA                IN VARCHAR2,
                                   P_CONF_COD_PAIS_LOC           IN NUMBER,
                                   P_CONF_PERM_DOC               IN NUMBER,
                                   P_CONF_FACT_CO_REC            IN NUMBER,
                                   P_CONF_FACT_CO_EMIT           IN NUMBER,
                                   P_CONF_FACT_CR_REC            IN NUMBER,
                                   P_CONF_FACT_CR_EMIT           IN NUMBER,
                                   P_CONF_RECIBO_PAGO_REC        IN NUMBER,
                                   P_CONF_RECIBO_PAGO_EMIT       IN NUMBER,
                                   P_CONF_RECIBO_CNCR_REC        IN NUMBER,
                                   P_CONF_RECIBO_CNCR_EMIT       IN NUMBER,
                                   P_CONF_RECIBO_CADCLI_EMIT     IN NUMBER,
                                   P_CONF_NOTA_CR_REC            IN NUMBER,
                                   P_CONF_NOTA_CR_EMIT           IN NUMBER,
                                   P_CONF_NOTA_DB_REC            IN NUMBER,
                                   P_CONF_NOTA_DB_EMIT           IN NUMBER,
                                   P_CONF_COD_DEP                IN NUMBER,
                                   P_CONF_COD_EXTRACCION         IN NUMBER,
                                   P_CONF_FACT_COMPRA            IN NUMBER,
                                   P_CONF_RETENCION_REC          IN NUMBER,
                                   P_CONF_RETENCION_EMIT         IN NUMBER,
                                   P_CONF_ADELANTO_CLI           IN NUMBER,
                                   P_CONF_DIF_CAMBIO_DB          IN NUMBER,
                                   P_CONF_DIF_CAMBIO_CR          IN NUMBER,
                                   P_CONF_TMOV_PAGO_NC           IN NUMBER,
                                   P_CONF_TMOV_DECL_JURADA       IN NUMBER,
                                   P_CONF_TMOV_DEVOL_ADEL_CLI    IN NUMBER,
                                   P_CONF_RECIBO_CAN_FAC_EMIT    IN NUMBER,
                                   P_CONF_RECIBO_CAN_FAC_REC     IN NUMBER,
                                   P_CONF_CONCEPTO_DEP           IN NUMBER,
                                   P_CONF_CONCEPTO_EXT           IN NUMBER,
                                   P_CONF_CONCEPTO_COBRO         IN NUMBER,
                                   P_CONF_CONCEPTO_PAGO          IN NUMBER,
                                   P_CONF_CONC_DIF_CAMBIO_DB     IN NUMBER,
                                   P_CONF_CONC_DIF_CAMBIO_CR     IN NUMBER,
                                   P_CONF_CONC_CADCLI            IN NUMBER,
                                   P_CONF_CONC_PAGO_NC           IN NUMBER,
                                   P_CONF_CLAVE_CTACO_DEUDORES   IN NUMBER,
                                   P_CONF_CLAVE_CTACO_ACREEDORES IN NUMBER,
                                   P_CONF_ADELANTO_PRO           IN NUMBER,
                                   P_CONF_RECIBO_CADPRO_REC      IN NUMBER,
                                   P_CONF_TMOV_DEVOL_ADEL_PRO    IN NUMBER,
                                   P_CONF_CONC_CADPRO            IN NUMBER,
                                   P_CONF_TMOV_PAGO_NC_REC       IN NUMBER,
                                   P_CONF_CONC_PAGO_NC_REC       IN NUMBER,
                                   P_CONF_TMOV_PROD_CONDICIONAL  IN NUMBER,
                                   P_CONF_TMOV_PROD_PROMEDIO     IN NUMBER,
                                   P_CONF_TMOV_BONIF_PROMEDIO    IN NUMBER,
                                   P_CONF_DIAS_GRACIA            IN NUMBER,
                                   P_CONF_IND_RESUMEN_COBRANZA   IN NUMBER,
                                   P_CONF_CONC_DEP_CTACTE        IN NUMBER,
                                   P_CONF_CONC_EXT_CTACTE        IN NUMBER,
                                   P_CONF_TMOV_RES_VTA_ZZ        IN NUMBER,
                                   P_CONF_CONC_TC_INGRESO        IN NUMBER,
                                   P_CONF_CONC_TC_LIQUIDACION    IN NUMBER,
                                   P_CONF_CONC_ANTICIPO_PERSONAL IN NUMBER,
                                   P_CONF_TMOV_ORDEN_COMPRA      IN NUMBER,
                                   P_CONF_IND_GENERAR_INTERES    IN VARCHAR2,
                                   P_CONF_ULT_FEC_CALC_INT       IN DATE,

                                   P_CONF_IND_RESTRINGIR_CTACONT IN VARCHAR2,

                                   P_CONF_TMOV_RETENCION      IN NUMBER,
                                   P_CONF_CONC_RETEN_CENTRAL  IN NUMBER,
                                   P_CONF_CONC_RETEN_ASUNCION IN NUMBER,
                                   P_CONF_CONC_RETEN_LOMA     IN NUMBER,
                                   P_CONF_CONC_RETEN_CAACUPE  IN NUMBER,
                                   P_CONF_CONC_RETEN_CDE      IN NUMBER,
                                   P_CONF_SUELDO_MINIMO       IN NUMBER,
                                   P_CONF_CONC_RENTA_CENTRAL  IN NUMBER,
                                   P_CONF_CONC_RENTA_ASUNCION IN NUMBER,
                                   P_CONF_CONC_RENTA_LOMA     IN NUMBER,
                                   P_CONF_TMOV_RENTA          IN NUMBER,

                                   P_CONF_CONC_PMO_REC_DB         IN NUMBER,
                                   P_CONF_CONC_INT_PMO_REC_DB     IN NUMBER,
                                   P_CONF_CONC_INT_PMO_REC_CR     IN NUMBER,
                                   P_CONF_TMOV_INT_REC            IN NUMBER,
                                   P_CONF_TMOV_PMO_REC            IN NUMBER,
                                   P_CONF_CONC_INT_PAG_PMO_REC_CR IN NUMBER,
                                   P_CONF_CONC_PMO_REC_CR         IN NUMBER,
                                   P_CONF_TMOV_INT_REC_CAN        IN NUMBER,
                                   --  P_CONF_TMOV_DECLA_JUR IN NUMBER,
                                   P_CONF_CONC_RETEN_CONCEPCION  IN NUMBER,
                                   P_CONF_CONC_RENTA_CONCEPCION  IN NUMBER,
                                   P_CONF_CONC_RETEN_SANTAROSA   IN NUMBER,
                                   P_CONF_CONC_RENTA_SANTAROSA   IN NUMBER,
                                   P_CONF_PORC_MOROS_CLI         IN NUMBER,
                                   P_CONF_CONC_RETENCION_EMIT    IN NUMBER,
                                   P_CONF_MONTO_MINIMO_RETENCION IN NUMBER,
                                    P_CONF_T_ORDEN_COMPRA IN NUMBER,
                                   P_CONF_PCT_RETENCION          IN NUMBER,
                                   p_CONF_FEC_LIM_MOD            IN DATE,
                                   in_cat_cli_func_grupo         in number := null -- 12/08/2022 11:39:54 @PabloACespedes \(^-^)/
                                                                                   -- identificara cual es la categoria para asociar el empl
                                   
                                   
                                   );

 PROCEDURE PP_TAER_CUENTAS_CON(P_EMPRESA IN NUMBER,
                               P_CODIGO IN NUMBER,
                               P_CLAVE OUT NUMBER);


PROCEDURE PP_TRAER_CONCEPTO(P_EMPRESA IN NUMBER,
                            P_CODIGO IN NUMBER,
                            P_CLAVE OUT NUMBER);


PROCEDURE PP_MOSTRAR_CONCEPTO(P_EMPRESA IN NUMBER,
                            P_CODIGO OUT NUMBER,
                            P_CLAVE IN NUMBER);


  PROCEDURE PP_VALIDAR_CAMPOS(P_CAMPOS IN VARCHAR2);


PROCEDURE PP_ALTER_TRIGGER(TriggerName IN VARCHAR2,
                           Estado IN VARCHAR2);


end FINM008;
/
CREATE OR REPLACE PACKAGE BODY FINM008 IS


PROCEDURE PP_TRAER_CONCEPTO(P_EMPRESA IN NUMBER,
                            P_CODIGO IN NUMBER,
                            P_CLAVE OUT NUMBER)
                            IS


BEGIN
   IF P_CODIGO IS NOT NULL THEN

      SELECT  FCON_CLAVE
        INTO P_CLAVE
        FROM FIN_CONCEPTO
       WHERE FCON_CODIGO =P_CODIGO
         AND FCON_EMPR= P_EMPRESA;
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20001, 'Concepto inexistente');
  END PP_TRAER_CONCEPTO;


 PROCEDURE PP_TAER_CUENTAS_CON(P_EMPRESA IN NUMBER,
                               P_CODIGO IN NUMBER,
                               P_CLAVE OUT NUMBER)
 IS
 BEGIN

 IF P_CODIGO IS NOT NULL THEN

      SELECT  CTAC_CLAVE
        INTO P_CLAVE
        FROM CNT_CUENTA
       WHERE CTAC_NRO = P_CODIGO
         AND CTAC_EMPR= P_EMPRESA;

    ELSE
      P_CLAVE       := NULL;

    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      P_CLAVE       := NULL;
      RAISE_APPLICATION_ERROR(-20001, 'Cuenta Contable inexistente');
   END PP_TAER_CUENTAS_CON;


  PROCEDURE PP_MOSTRAR_CONCEPTO(P_EMPRESA IN NUMBER,
                            P_CODIGO OUT NUMBER,
                            P_CLAVE IN NUMBER)
                            IS
 BEGIN
      IF P_CLAVE IS NOT NULL THEN

      SELECT FCON_CODIGO
        INTO P_CODIGO
        FROM FIN_CONCEPTO
       WHERE FCON_CLAVE =P_CLAVE
         AND FCON_EMPR= P_EMPRESA;
    END IF;
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
     --- RAISE_APPLICATION_ERROR(-20001, 'Concepto inexistente');



   END PP_MOSTRAR_CONCEPTO;

    PROCEDURE PP_VALIDAR_CAMPOS(P_CAMPOS IN VARCHAR2) IS

  BEGIN
    IF P_CAMPOS NOT IN ('S', 'N') THEN
      RAISE_APPLICATION_ERROR(-20001, 'Debe ser S o N !');
    ELSE
      IF P_CAMPOS IS NULL THEN
        RAISE_APPLICATION_ERROR(-20001,
                                'El Elemento no puede quedar nulo!');
      END IF;
    end if;

  END PP_VALIDAR_CAMPOS;

 PROCEDURE PP_ALTER_TRIGGER(TriggerName IN VARCHAR2,
                           Estado IN VARCHAR2) IS
  TRIGGER_NO_EXISTE EXCEPTION;
  PRAGMA EXCEPTION_INIT(TRIGGER_NO_EXISTE, -4080);
BEGIN
    GEN_EXEC('ALTER TRIGGER ' ||TriggerName||' '|| Estado);

EXCEPTION
  WHEN TRIGGER_NO_EXISTE THEN
    IF Estado = 'ENABLE' Then
      raise_application_error(-20001,'Trigger de FINS001 (Resumen de Cobranza) no existe!');
    ELSE
      NULL;
    END IF;
END PP_ALTER_TRIGGER;
  -- PRIVATE TYPE DECLARATIONS
  PROCEDURE PP_ACTUALIZAR_FIN_CONF(P_CONF_EMPR                   IN NUMBER,
                                   P_CONF_PER_ACT_INI            IN DATE,
                                   P_CONF_PER_ACT_FIN            IN DATE,
                                   P_CONF_PER_SGTE_INI           IN DATE,
                                   P_CONF_PER_SGTE_FIN           IN DATE,
                                   P_CONF_IND_HAB_MES_ACT        IN VARCHAR2,
                                   P_CONF_IND_IVA                IN VARCHAR2,
                                   P_CONF_COD_PAIS_LOC           IN NUMBER,
                                   P_CONF_PERM_DOC               IN NUMBER,
                                   P_CONF_FACT_CO_REC            IN NUMBER,
                                   P_CONF_FACT_CO_EMIT           IN NUMBER,
                                   P_CONF_FACT_CR_REC            IN NUMBER,
                                   P_CONF_FACT_CR_EMIT           IN NUMBER,
                                   P_CONF_RECIBO_PAGO_REC        IN NUMBER,
                                   P_CONF_RECIBO_PAGO_EMIT       IN NUMBER,
                                   P_CONF_RECIBO_CNCR_REC        IN NUMBER,
                                   P_CONF_RECIBO_CNCR_EMIT       IN NUMBER,
                                   P_CONF_RECIBO_CADCLI_EMIT     IN NUMBER,
                                   P_CONF_NOTA_CR_REC            IN NUMBER,
                                   P_CONF_NOTA_CR_EMIT           IN NUMBER,
                                   P_CONF_NOTA_DB_REC            IN NUMBER,
                                   P_CONF_NOTA_DB_EMIT           IN NUMBER,
                                   P_CONF_COD_DEP                IN NUMBER,
                                   P_CONF_COD_EXTRACCION         IN NUMBER,
                                   P_CONF_FACT_COMPRA            IN NUMBER,
                                   P_CONF_RETENCION_REC          IN NUMBER,
                                   P_CONF_RETENCION_EMIT         IN NUMBER,
                                   P_CONF_ADELANTO_CLI           IN NUMBER,
                                   P_CONF_DIF_CAMBIO_DB          IN NUMBER,
                                   P_CONF_DIF_CAMBIO_CR          IN NUMBER,
                                   P_CONF_TMOV_PAGO_NC           IN NUMBER,
                                   P_CONF_TMOV_DECL_JURADA       IN NUMBER,
                                   P_CONF_TMOV_DEVOL_ADEL_CLI    IN NUMBER,
                                   P_CONF_RECIBO_CAN_FAC_EMIT    IN NUMBER,
                                   P_CONF_RECIBO_CAN_FAC_REC     IN NUMBER,
                                   P_CONF_CONCEPTO_DEP           IN NUMBER,
                                   P_CONF_CONCEPTO_EXT           IN NUMBER,
                                   P_CONF_CONCEPTO_COBRO         IN NUMBER,
                                   P_CONF_CONCEPTO_PAGO          IN NUMBER,
                                   P_CONF_CONC_DIF_CAMBIO_DB     IN NUMBER,
                                   P_CONF_CONC_DIF_CAMBIO_CR     IN NUMBER,
                                   P_CONF_CONC_CADCLI            IN NUMBER,
                                   P_CONF_CONC_PAGO_NC           IN NUMBER,
                                   P_CONF_CLAVE_CTACO_DEUDORES   IN NUMBER,
                                   P_CONF_CLAVE_CTACO_ACREEDORES IN NUMBER,
                                   P_CONF_ADELANTO_PRO           IN NUMBER,
                                   P_CONF_RECIBO_CADPRO_REC      IN NUMBER,
                                   P_CONF_TMOV_DEVOL_ADEL_PRO    IN NUMBER,
                                   P_CONF_CONC_CADPRO            IN NUMBER,
                                   P_CONF_TMOV_PAGO_NC_REC       IN NUMBER,
                                   P_CONF_CONC_PAGO_NC_REC       IN NUMBER,
                                   P_CONF_TMOV_PROD_CONDICIONAL  IN NUMBER,
                                   P_CONF_TMOV_PROD_PROMEDIO     IN NUMBER,
                                   P_CONF_TMOV_BONIF_PROMEDIO    IN NUMBER,
                                   P_CONF_DIAS_GRACIA            IN NUMBER,
                                   P_CONF_IND_RESUMEN_COBRANZA   IN NUMBER,
                                   P_CONF_CONC_DEP_CTACTE        IN NUMBER,
                                   P_CONF_CONC_EXT_CTACTE        IN NUMBER,
                                   P_CONF_TMOV_RES_VTA_ZZ        IN NUMBER,
                                   P_CONF_CONC_TC_INGRESO        IN NUMBER,
                                   P_CONF_CONC_TC_LIQUIDACION    IN NUMBER,
                                   P_CONF_CONC_ANTICIPO_PERSONAL IN NUMBER,
                                   P_CONF_TMOV_ORDEN_COMPRA      IN NUMBER,
                                   P_CONF_IND_GENERAR_INTERES    IN VARCHAR2,
                                   P_CONF_ULT_FEC_CALC_INT       IN DATE,

                                   P_CONF_IND_RESTRINGIR_CTACONT IN VARCHAR2,

                                   P_CONF_TMOV_RETENCION      IN NUMBER,
                                   P_CONF_CONC_RETEN_CENTRAL  IN NUMBER,
                                   P_CONF_CONC_RETEN_ASUNCION IN NUMBER,
                                   P_CONF_CONC_RETEN_LOMA     IN NUMBER,
                                   P_CONF_CONC_RETEN_CAACUPE  IN NUMBER,
                                   P_CONF_CONC_RETEN_CDE      IN NUMBER,
                                   P_CONF_SUELDO_MINIMO       IN NUMBER,
                                   P_CONF_CONC_RENTA_CENTRAL  IN NUMBER,
                                   P_CONF_CONC_RENTA_ASUNCION IN NUMBER,
                                   P_CONF_CONC_RENTA_LOMA     IN NUMBER,
                                   P_CONF_TMOV_RENTA          IN NUMBER,

                                   P_CONF_CONC_PMO_REC_DB         IN NUMBER,
                                   P_CONF_CONC_INT_PMO_REC_DB     IN NUMBER,
                                   P_CONF_CONC_INT_PMO_REC_CR     IN NUMBER,
                                   P_CONF_TMOV_INT_REC            IN NUMBER,
                                   P_CONF_TMOV_PMO_REC            IN NUMBER,
                                   P_CONF_CONC_INT_PAG_PMO_REC_CR IN NUMBER,
                                   P_CONF_CONC_PMO_REC_CR         IN NUMBER,
                                   P_CONF_TMOV_INT_REC_CAN        IN NUMBER,
                                   --  P_CONF_TMOV_DECLA_JUR IN NUMBER,
                                   P_CONF_CONC_RETEN_CONCEPCION  IN NUMBER,
                                   P_CONF_CONC_RENTA_CONCEPCION  IN NUMBER,
                                   P_CONF_CONC_RETEN_SANTAROSA   IN NUMBER,
                                   P_CONF_CONC_RENTA_SANTAROSA   IN NUMBER,
                                   P_CONF_PORC_MOROS_CLI         IN NUMBER,
                                   P_CONF_CONC_RETENCION_EMIT    IN NUMBER,
                                   P_CONF_MONTO_MINIMO_RETENCION IN NUMBER,
                                   P_CONF_T_ORDEN_COMPRA IN NUMBER,
                                   P_CONF_PCT_RETENCION          IN NUMBER,
                                   P_CONF_FEC_LIM_MOD            IN DATE,
                                   in_cat_cli_func_grupo         in number := null -- 12/08/2022 11:39:54 @PabloACespedes \(^-^)/
                                                                                   -- identificara cual es la categoria para asociar el empl
                                   
                                   
                                   )

   IS
  BEGIN
    IF P_CONF_EMPR IS NOT NULL THEN

      UPDATE FIN_CONFIGURACION
         SET CONF_EMPR                    = P_CONF_EMPR,
             CONF_PER_ACT_INI             = P_CONF_PER_ACT_INI,
             CONF_PER_ACT_FIN             = P_CONF_PER_ACT_FIN,
             CONF_PER_SGTE_INI            = P_CONF_PER_SGTE_INI,
             CONF_PER_SGTE_FIN            = P_CONF_PER_SGTE_FIN,
             CONF_IND_HAB_MES_ACT         = P_CONF_IND_HAB_MES_ACT,
             CONF_IND_IVA                 = P_CONF_IND_IVA,
             CONF_COD_PAIS_LOC            = P_CONF_COD_PAIS_LOC,
             CONF_PERM_DOC                = P_CONF_PERM_DOC,
             CONF_FACT_CO_REC             = P_CONF_FACT_CO_REC,
             CONF_FACT_CO_EMIT            = P_CONF_FACT_CO_EMIT,
             CONF_FACT_CR_REC             = P_CONF_FACT_CR_REC,
             CONF_FACT_CR_EMIT            = P_CONF_FACT_CR_EMIT,
             CONF_RECIBO_PAGO_REC         = P_CONF_RECIBO_PAGO_REC,
             CONF_RECIBO_PAGO_EMIT        = P_CONF_RECIBO_PAGO_EMIT,
             CONF_RECIBO_CNCR_REC         = P_CONF_RECIBO_CNCR_REC,
             CONF_RECIBO_CNCR_EMIT        = P_CONF_RECIBO_CNCR_EMIT,
             CONF_RECIBO_CADCLI_EMIT      = P_CONF_RECIBO_CADCLI_EMIT,
             CONF_NOTA_CR_REC             = P_CONF_NOTA_CR_REC,
             CONF_NOTA_CR_EMIT            = P_CONF_NOTA_CR_EMIT,
             CONF_NOTA_DB_REC             = P_CONF_NOTA_DB_REC,
             CONF_NOTA_DB_EMIT            = P_CONF_NOTA_DB_EMIT,
             CONF_COD_DEP                 = P_CONF_COD_DEP,
             CONF_COD_EXTRACCION          = P_CONF_COD_EXTRACCION,
             CONF_FACT_COMPRA             = P_CONF_FACT_COMPRA,
             CONF_RETENCION_REC           = P_CONF_RETENCION_REC,
             CONF_RETENCION_EMIT          = P_CONF_RETENCION_EMIT,
             CONF_ADELANTO_CLI            = P_CONF_ADELANTO_CLI,
             CONF_DIF_CAMBIO_DB           = P_CONF_DIF_CAMBIO_DB,
             CONF_DIF_CAMBIO_CR           = P_CONF_DIF_CAMBIO_CR,
             CONF_TMOV_PAGO_NC            = P_CONF_TMOV_PAGO_NC,
             CONF_TMOV_DECL_JURADA        = P_CONF_TMOV_DECL_JURADA,
             CONF_TMOV_DEVOL_ADEL_CLI     = P_CONF_TMOV_DEVOL_ADEL_CLI,
             CONF_RECIBO_CAN_FAC_EMIT     = P_CONF_RECIBO_CAN_FAC_EMIT,
             CONF_RECIBO_CAN_FAC_REC      = P_CONF_RECIBO_CAN_FAC_REC,
             CONF_CONCEPTO_DEP            = P_CONF_CONCEPTO_DEP,
             CONF_CONCEPTO_EXT            = P_CONF_CONCEPTO_EXT,
             CONF_CONCEPTO_COBRO          = P_CONF_CONCEPTO_COBRO,
             CONF_CONCEPTO_PAGO           = P_CONF_CONCEPTO_PAGO,
             CONF_CONC_DIF_CAMBIO_DB      = P_CONF_CONC_DIF_CAMBIO_DB,
             CONF_CONC_DIF_CAMBIO_CR      = P_CONF_CONC_DIF_CAMBIO_CR,
             CONF_CONC_CADCLI             = P_CONF_CONC_CADCLI,
             CONF_CONC_PAGO_NC            = P_CONF_CONC_PAGO_NC,
             CONF_CLAVE_CTACO_DEUDORES    = P_CONF_CLAVE_CTACO_DEUDORES,
             CONF_CLAVE_CTACO_ACREEDORES  = P_CONF_CLAVE_CTACO_ACREEDORES,
             CONF_ADELANTO_PRO            = P_CONF_ADELANTO_PRO,
             CONF_RECIBO_CADPRO_REC       = P_CONF_RECIBO_CADPRO_REC,
             CONF_TMOV_DEVOL_ADEL_PRO     = P_CONF_TMOV_DEVOL_ADEL_PRO,
             CONF_CONC_CADPRO             = P_CONF_CONC_CADPRO,
             CONF_TMOV_PAGO_NC_REC        = P_CONF_TMOV_PAGO_NC_REC,
             CONF_CONC_PAGO_NC_REC        = P_CONF_CONC_PAGO_NC_REC,
             CONF_TMOV_PROD_CONDICIONAL   = P_CONF_TMOV_PROD_CONDICIONAL,
             CONF_TMOV_PROD_PROMEDIO      = P_CONF_TMOV_PROD_PROMEDIO,
             CONF_TMOV_BONIF_PROMEDIO     = P_CONF_TMOV_BONIF_PROMEDIO,
             CONF_DIAS_GRACIA             = P_CONF_DIAS_GRACIA,
             CONF_IND_RESUMEN_COBRANZA    = P_CONF_IND_RESUMEN_COBRANZA,
             CONF_CONC_DEP_CTACTE         = P_CONF_CONC_DEP_CTACTE,
             CONF_CONC_EXT_CTACTE         = P_CONF_CONC_EXT_CTACTE,
             CONF_TMOV_RES_VTA_ZZ         = P_CONF_TMOV_RES_VTA_ZZ,
             CONF_CONC_TC_INGRESO         = P_CONF_CONC_TC_INGRESO,
             CONF_CONC_TC_LIQUIDACION     = P_CONF_CONC_TC_LIQUIDACION,
             CONF_CONC_ANTICIPO_PERSONAL  = P_CONF_CONC_ANTICIPO_PERSONAL,
             CONF_CONC_ORDEN_COMPRA       = P_CONF_TMOV_ORDEN_COMPRA,
             CONF_IND_GENERAR_INTERES     = P_CONF_IND_GENERAR_INTERES,
             CONF_ULT_FEC_CALC_INT        = P_CONF_ULT_FEC_CALC_INT,
             CONF_IND_RESTRINGIR_CTACONT  = P_CONF_IND_RESTRINGIR_CTACONT,
             CONF_TMOV_RETENCION          = P_CONF_TMOV_RETENCION,
             CONF_CONC_RETEN_CAACUPE      = P_CONF_CONC_RETEN_CAACUPE, 
             CONF_CONC_RETEN_CDE          = P_CONF_CONC_RETEN_CDE,  
            /* CONF_CONC_RETEN_CENTRAL      = P_CONF_CONC_RETEN_CENTRAL,
             CONF_CONC_RETEN_ASUNCION     = P_CONF_CONC_RETEN_ASUNCION,
             CONF_CONC_RETEN_LOMA         = P_CONF_CONC_RETEN_LOMA,*/
             CONF_SUELDO_MINIMO           = P_CONF_SUELDO_MINIMO,
            /* CONF_CONC_RENTA_CENTRAL      = P_CONF_CONC_RENTA_CENTRAL,
             CONF_CONC_RENTA_ASUNCION     = P_CONF_CONC_RENTA_ASUNCION,
             CONF_CONC_RENTA_LOMA         = P_CONF_CONC_RENTA_LOMA,*/
             CONF_TMOV_RENTA              = P_CONF_TMOV_RENTA,
             -- CONF_CONC_PMO_REC_DB         = P_CONF_CONC_PMO_REC_DB,
            -- CONF_CONC_INT_PMO_REC_DB     = P_CONF_CONC_INT_PMO_REC_DB,
            -- CONF_CONC_INT_PMO_REC_CR     = P_CONF_CONC_INT_PMO_REC_CR,
             CONF_TMOV_INT_REC            = P_CONF_TMOV_INT_REC,
             CONF_TMOV_PMO_REC            = P_CONF_TMOV_PMO_REC,
          --   CONF_CONC_INT_PAG_PMO_REC_CR = P_CONF_CONC_INT_PAG_PMO_REC_CR,
            -- CONF_CONC_PMO_REC_CR         = P_CONF_CONC_PMO_REC_CR,
             CONF_TMOV_INT_REC_CAN        = P_CONF_TMOV_INT_REC_CAN,
            -- CONF_CONC_RETEN_CONCEPCION   = P_CONF_CONC_RETEN_CONCEPCION,
             --CONF_CONC_RENTA_CONCEPCION   = P_CONF_CONC_RENTA_CONCEPCION,
             --CONF_CONC_RETEN_SANTAROSA    = P_CONF_CONC_RETEN_SANTAROSA,
             --CONF_CONC_RENTA_SANTAROSA    = P_CONF_CONC_RENTA_SANTAROSA,
             CONF_PORC_MOROS_CLI          = P_CONF_PORC_MOROS_CLI,
             CONF_MONTO_MINIMO_RETENCION  = P_CONF_MONTO_MINIMO_RETENCION,
             CONF_PCT_RETENCION           = P_CONF_PCT_RETENCION,
             CONF_TMOV_ORDEN_COMPRA       = P_CONF_T_ORDEN_COMPRA,
             CONF_CONC_RETENCION_EMIT     = P_CONF_CONC_RETENCION_EMIT,
             CONF_FEC_LIM_MOD             = P_CONF_FEC_LIM_MOD,
             
             conf_cat_cli_func_grupo = in_cat_cli_func_grupo-- 12/08/2022 11:41:30 @PabloACespedes \(^-^)/
             
             
              WHERE CONF_EMPR = P_CONF_EMPR;
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20001, 'Error al acualizar, avise al departamento de informatica');


      if P_CONF_EMPR=1 then
       UPDATE FIN_CONFIGURACION
         SET
            CONF_CONC_RETEN_CONCEPCION    = P_CONF_CONC_RETEN_CONCEPCION,
             CONF_CONC_RENTA_CONCEPCION   = P_CONF_CONC_RENTA_CONCEPCION,
             CONF_CONC_RETEN_SANTAROSA    = P_CONF_CONC_RETEN_SANTAROSA,
             CONF_CONC_RENTA_SANTAROSA    = P_CONF_CONC_RENTA_SANTAROSA,
             CONF_CONC_RENTA_CENTRAL      = P_CONF_CONC_RENTA_CENTRAL,
             CONF_CONC_RENTA_ASUNCION     = P_CONF_CONC_RENTA_ASUNCION,
             CONF_CONC_RENTA_LOMA         = P_CONF_CONC_RENTA_LOMA,
             CONF_CONC_RETEN_CENTRAL      = P_CONF_CONC_RETEN_CENTRAL,
             CONF_CONC_RETEN_ASUNCION     = P_CONF_CONC_RETEN_ASUNCION,
             CONF_CONC_PMO_REC_CR         = P_CONF_CONC_PMO_REC_CR,
             CONF_CONC_INT_PAG_PMO_REC_CR = P_CONF_CONC_INT_PAG_PMO_REC_CR,
             CONF_CONC_INT_PMO_REC_CR     = P_CONF_CONC_INT_PMO_REC_CR,
             CONF_CONC_PMO_REC_DB         = P_CONF_CONC_PMO_REC_DB,
             CONF_CONC_INT_PMO_REC_DB     = P_CONF_CONC_INT_PMO_REC_DB,
             CONF_CONC_RETEN_LOMA         = P_CONF_CONC_RETEN_LOMA
              WHERE CONF_EMPR = P_CONF_EMPR;
              end if;
    -- EN CASO DE SER HILAGRO ACTUALIZA EL OPEM_IND_HAB_MES_ACT EN GEN_OPERADOR_EMPRESA
           if P_CONF_EMPR=1 then

         UPDATE GEN_OPERADOR_EMPRESA
       SET
       OPEM_IND_HAB_MES_ACT = P_CONF_IND_HAB_MES_ACT
       WHERE OPEM_EMPR = P_CONF_EMPR;
       END IF;
       COMMIT;
  END PP_ACTUALIZAR_FIN_CONF;
END FINM008;
/
