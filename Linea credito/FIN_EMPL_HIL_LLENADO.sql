CREATE OR REPLACE VIEW FIN_EMPL_HIL_LLENADO AS
SELECT DISTINCT EMPL_LEGAJO CODIGO,
                EMPL_NOMBRE || ' ' || EMPL_APE NOMBRE,
                EMPL_DOC_IDENT RUC,
                EMPL_DIR DIRECCION,
                EMPL_LOCALIDAD LOCALIDAD,
                EMPL_TEL TELEFONO,
                EMPL_FEC_NAC FECHA_NAC,
                'HILAGRO' EMPRESA
  FROM PER_EMPLEADO e
 WHERE EMPL_EMPRESA = 1
 union all 
 SELECT DISTINCT c.legajo CODIGO,
                c.nombre || ' ' || c.apellido NOMBRE,
                c.documento_identidad RUC,
                c.direccion DIRECCION,
                null LOCALIDAD,
                c.telefono TELEFONO,
                c.fecha_nac FECHA_NAC,
                'CONSULTAGRO' EMPRESA
  FROM PER_EMPLEADO_cagro c

 ORDER BY 1 DESC;
