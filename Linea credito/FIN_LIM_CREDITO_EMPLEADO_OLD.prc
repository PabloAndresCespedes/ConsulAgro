CREATE OR REPLACE PROCEDURE FIN_LIM_CREDITO_EMPLEADO(V_FEC_DOC            IN DATE,
                                                     V_CLI                IN NUMBER,
                                                     V_IMP_LIM_CR_EMPR    IN OUT NUMBER,
                                                     V_EMPR               IN NUMBER,
                                                     V_IMP_LIM_DISP_GRUPO IN OUT NUMBER,
                                                     V_IMP_LIM_DISP_EMPR  IN OUT NUMBER,--------------este  es el que define el limite de credito
                                                     V_IMP_CHEQ_DIFERIDO  IN OUT NUMBER,
                                                     V_IMP_CHEQ_RECHAZADO IN OUT NUMBER,
                                                     V_EMPL_LEGAJO        IN NUMBER DEFAULT NULL) IS




 /*CURSOR GRUP_CUR IS
    SELECT CLEM_EMPR,
           CLEM_SALDO_ACT_TENT_MON,
           CLEM_IMP_LIM_CR,
           CLEM_MON,
           COT_TASA
      FROM STK_COTIZACION, FIN_CLI_EMPRESA
     WHERE COT_MON(+) = CLEM_MON
       AND COT_FEC(+) = V_FEC_DOC
       AND CLEM_EMPR = V_EMPR
       AND COT_EMPR = CLEM_EMPR
       AND CLEM_CLI = V_CLI;*/

  /*CURSOR CHEQ_CUR IS
    SELECT CHEQ_EMPR, CHEQ_MON, CHEQ_IMPORTE, COT_TASA, CHEQ_SITUACION
      FROM FIN_CHEQUE, STK_COTIZACION
     WHERE CHEQ_CLI = V_CLI
       AND CHEQ_EMPR = COT_EMPR
       AND CHEQ_EMPR = V_EMPR
       AND CHEQ_SITUACION NOT IN ('C', 'A')
       AND CHEQ_MON = COT_MON(+)
       AND COT_FEC(+) = V_FEC_DOC;*/

  CURSOR CHEQ_CUR IS
    SELECT CHEQ_EMPR, CHEQ_MON, CHEQ_IMPORTE, fin_buscar_cotizacion_empr(mon => CHEQ_MON,
                                            emp => CHEQ_EMPR) COT_TASA, CHEQ_SITUACION
      FROM FIN_CHEQUE, STK_COTIZACION, FIN_CLIENTE A
     WHERE CHEQ_EMPR = COT_EMPR(+)
       AND CHEQ_EMPR = V_EMPR
       AND CHEQ_SITUACION NOT IN ('C', 'A')
       AND CHEQ_MON = COT_MON(+)
       AND COT_FEC(+) = V_FEC_DOC
       AND CHEQ_CLI = A.CLI_CODIGO
       AND CHEQ_EMPR = A.CLI_EMPR
      AND CLI_COD_FICHA_HOLDING IN ( SELECT A.CLI_COD_FICHA_HOLDING
                                         FROM FIN_CLIENTE A
                                         WHERE CLI_EMPR = V_EMPR
                                         AND  CLI_CODIGO  =V_CLI);----------------------------LV COTIZACION POR HOLGIND Y POR fin_buscar_cotizacion_empr


  V_TASA               NUMBER;
  V_TOT_SALDO_GRUP_LOC NUMBER := 0;
  V_MON_LOC            NUMBER := 1;
  V_PENDIENTES         NUMBER := 0;
  V_PRESTAMOS          NUMBER := 0;
  V_LIM_GRUPO          NUMBER := 0;
  V_TOT_SALDO_EMPR_LOC NUMBER := 0;

  V_COD_HOLDING NUMBER := 0;

  V_CLI_NOM    VARCHAR2(500);
  V_CLI_RUC    VARCHAR2(500);
  V_CLI_RUC_DV VARCHAR2(500);
  MAR_AUT_ESP  NUMBER;

  V_EMPLEADO NUMBER;
  V_COD_EMPL_TRANSAGRO NUMBER;
  V_CUO_PROVEEDOR NUMBER;
  V_IMP_HILAGRO_TRAN NUMBER;
  V_COD_PROVEEDOR NUMBER;

  V_DEUDA_TRANSAGRO NUMBER;

  V_CLI_CATEG NUMBER;
  V_CLI_COD_EMPL NUMBER;

  V_OPCION VARCHAR2(1);

  V_CLIENTE NUMBER;
  V_LIMITE_CREDITO_EMPLEADO NUMBER;
    V_CUO_PROVEEDOR_TEMPORAL NUMBER;
      V_VACACION NUMBER;
BEGIN
--RAISE_APPLICATION_ERROR (-20001, 'ADFADSF');
------------VERIFICAMOS DE QUE EMPRESA ES EL EMPLEADO

  IF V_EMPR = 2 THEN

    SELECT CLI_CATEG, CLI_COD_EMPL_EMPR_ORIG
      INTO V_CLI_CATEG, V_CLI_COD_EMPL
      FROM FIN_CLIENTE
      WHERE CLI_EMPR = 2
      AND CLI_CODIGO = V_CLI;



    ---------------------------------esta parte controla si el empleado es de hilagro
    IF V_CLI_CATEG = 4 THEN

      BEGIN
         SELECT EMPL_LEGAJO, A.EMPL_CODIGO_PROV, EMPL_COD_CLIENTE, EMPL_LIMITE_CRED
           INTO V_EMPLEADO, V_COD_PROVEEDOR, V_CLIENTE, V_IMP_LIM_CR_EMPR
          FROM PER_EMPLEADO  A
          WHERE A.EMPL_LEGAJO = V_CLI_COD_EMPL
          AND A.EMPL_EMPRESA = 1;
       EXCEPTION
          WHEN NO_DATA_FOUND THEN
         NULL;
       END;

      V_OPCION := 'B';
    ELSE

         SELECT EMPL_LEGAJO, A.EMPL_CODIGO_PROV, A.EMPL_CODIGO_CLI, EMPL_LIMITE_CRED
           INTO V_EMPLEADO, V_COD_PROVEEDOR, V_CLIENTE, V_IMP_LIM_CR_EMPR
           FROM PER_EMPLEADO  A
          WHERE A.EMPL_CODIGO_CLI = V_CLI---V_CLI_COD_EMPL
            AND A.EMPL_EMPRESA = 2;

      V_OPCION := 'A';
      V_CLIENTE := V_CLI;
    END IF;

  ELSIF V_EMPR = 1 THEN


  IF V_EMPL_LEGAJO IS NULL THEN
       BEGIN
         SELECT EMPL_LEGAJO, A.EMPL_CODIGO_PROV, EMPL_LIMITE_CRED
           INTO V_EMPLEADO, V_COD_PROVEEDOR, V_IMP_LIM_CR_EMPR
          FROM PER_EMPLEADO  A
          WHERE A.EMPL_COD_CLIENTE =V_CLI--- V_CLI
          AND A.EMPL_EMPRESA =V_EMPR;
       EXCEPTION
          WHEN NO_DATA_FOUND THEN
         NULL;
       END;

       V_CLIENTE := V_CLI;
       V_OPCION := 'B';
   ELSE
     BEGIN
         SELECT EMPL_LEGAJO, A.EMPL_CODIGO_PROV, EMPL_LIMITE_CRED, EMPL_COD_CLIENTE
           INTO V_EMPLEADO, V_COD_PROVEEDOR, V_IMP_LIM_CR_EMPR, V_CLIENTE
          FROM PER_EMPLEADO  A
          WHERE A.EMPL_LEGAJO =V_EMPL_LEGAJO
            AND A.EMPL_EMPRESA =V_EMPR;
       EXCEPTION
          WHEN NO_DATA_FOUND THEN
         NULL;
       END;

       V_OPCION := 'B';
   END IF;
  END IF;

  V_IMP_LIM_DISP_GRUPO := 0;
  V_IMP_LIM_DISP_EMPR  := 0;
  V_IMP_CHEQ_DIFERIDO  := 0;
  V_IMP_CHEQ_RECHAZADO := 0;


  IF V_OPCION = 'A' THEN




          SELECT CONF_MON_LOC
            INTO V_MON_LOC
            FROM GEN_CONFIGURACION
           WHERE CONF_EMPR = 2;

         /* SELECT CLI_IMP_LIM_CR
            INTO V_LIM_GRUPO
            FROM FIN_CLIENTE
           WHERE CLI_CODIGO = V_CLIENTE
             AND CLI_EMPR = 2;*/

          -- hallar el limite de credito disponible del grupo empresarial
          -- y el limite de credito de la empresa
        /*  FOR RGRUP IN GRUP_CUR LOOP
            IF RGRUP.CLEM_MON = V_MON_LOC THEN
              V_TASA := 1;
            ELSIF RGRUP.COT_TASA IS NULL OR RGRUP.COT_TASA = 0 THEN
              RAISE_APPLICATION_ERROR(-20101,
                                      'Debe ingresar la cotizacion del dia:' ||
                                      TO_CHAR(V_FEC_DOC, 'DD-MM-YYYY') ||
                                      ' para la moneda:' ||
                                      TO_CHAR(RGRUP.CLEM_MON));
            ELSE
              V_TASA := RGRUP.COT_TASA;
            END IF;

            V_TOT_SALDO_GRUP_LOC := V_TOT_SALDO_GRUP_LOC +
                                    RGRUP.CLEM_SALDO_ACT_TENT_MON * V_TASA;

            IF RGRUP.CLEM_EMPR = V_EMPR THEN
              V_IMP_LIM_CR_EMPR := V_IMP_LIM_CR_EMPR +
                                   RGRUP.CLEM_IMP_LIM_CR * V_TASA;

              V_TOT_SALDO_EMPR_LOC := V_TOT_SALDO_EMPR_LOC +
                                      RGRUP.CLEM_SALDO_ACT_TENT_MON * V_TASA;
            END IF;
          END LOOP;*/

          -- Para calcular el monto en cheques diferidos.

          V_IMP_LIM_CR_EMPR    := V_IMP_LIM_CR_EMPR;
          V_TOT_SALDO_GRUP_LOC := 0;
          V_TOT_SALDO_EMPR_LOC := 0;

          FOR RCHEQ IN CHEQ_CUR LOOP
            IF RCHEQ.CHEQ_MON = V_MON_LOC THEN
              V_TASA := 1;
            ELSIF RCHEQ.COT_TASA IS NULL OR RCHEQ.COT_TASA = 0 THEN
              RAISE_APPLICATION_ERROR(-20201,
                                      'Debe ingresar la cotizacion del dia:' ||
                                      TO_CHAR(V_FEC_DOC, 'DD-MM-YYYY') ||
                                      ' para la moneda:' ||
                                      TO_CHAR(RCHEQ.CHEQ_MON));
            ELSE
              V_TASA := RCHEQ.COT_TASA;
            END IF;


            V_TOT_SALDO_GRUP_LOC := V_TOT_SALDO_GRUP_LOC +
                                    RCHEQ.CHEQ_IMPORTE * V_TASA;

            IF RCHEQ.CHEQ_EMPR = V_EMPR THEN
              V_TOT_SALDO_EMPR_LOC := V_TOT_SALDO_EMPR_LOC +
                                      RCHEQ.CHEQ_IMPORTE * V_TASA;
            END IF;
            IF RCHEQ.CHEQ_SITUACION NOT IN ('D', 'I') THEN
              V_IMP_CHEQ_RECHAZADO := V_IMP_CHEQ_RECHAZADO +
                                      RCHEQ.CHEQ_IMPORTE * V_TASA;
            ELSE
              V_IMP_CHEQ_DIFERIDO := V_IMP_CHEQ_DIFERIDO +
                                     RCHEQ.CHEQ_IMPORTE * V_TASA;
            END IF;
          END LOOP;


    SELECT NVL(SUM(CUO_IMP_MON),0) IMPORTE-----------------------------------EL IMPORTE DE LAS DEUDAS DE LOS EMPLEADOS EN TRANSAGRO
      INTO V_DEUDA_TRANSAGRO
      FROM  (
      SELECT NVL(E.CUO_SALDO_MON,0) CUO_IMP_MON,DOC_TIPO_MOV, DOC_CLI
                FROM FIN_DOCUMENTO C,
                     FIN_CUOTA     E
               WHERE C.DOC_EMPR = 2
                 AND C.DOC_CLAVE = E.CUO_CLAVE_DOC
                 AND C.DOC_EMPR = E.CUO_EMPR
                 AND  CUO_FEC_VTO <= V_FEC_DOC
                 AND C.DOC_SALDO_MON > 0
                 AND E.CUO_SALDO_MON > 0
                 AND C.DOC_MON = 1
                 AND C.DOC_CLI = V_CLIENTE
              UNION ALL
              SELECT NVL(E.CUO_SALDO_MON,0) CUO_IMP_MON, DOC_TIPO_MOV, DOC_PROV
                FROM FIN_DOCUMENTO C,
                     FIN_CUOTA     E
               WHERE C.DOC_EMPR = 2
                 AND C.DOC_CLAVE = E.CUO_CLAVE_DOC
                 AND C.DOC_EMPR = E.CUO_EMPR
                 AND E.CUO_FEC_VTO <=V_FEC_DOC
                 AND C.DOC_SALDO_MON > 0
                 AND E.CUO_SALDO_MON > 0
                 AND C.DOC_TIPO_MOV =31
                 AND C.DOC_MON = 1
                 AND C.DOC_PROV = V_CLIENTE);


          V_TOT_SALDO_GRUP_LOC := V_TOT_SALDO_GRUP_LOC+NVL(V_DEUDA_TRANSAGRO,0);
          V_TOT_SALDO_EMPR_LOC := V_TOT_SALDO_EMPR_LOC+NVL(V_DEUDA_TRANSAGRO,0);

          IF V_TOT_SALDO_GRUP_LOC > V_LIM_GRUPO THEN
            V_IMP_LIM_DISP_GRUPO := 0;
          ELSE
            V_IMP_LIM_DISP_GRUPO := V_LIM_GRUPO - V_TOT_SALDO_GRUP_LOC;
          END IF;

          IF V_TOT_SALDO_EMPR_LOC > V_IMP_LIM_CR_EMPR THEN
            V_IMP_LIM_DISP_EMPR := 0;
          ELSE
            V_IMP_LIM_DISP_EMPR := V_IMP_LIM_CR_EMPR - V_TOT_SALDO_EMPR_LOC;
          END IF;







  ELSE
  ---------------------------------------------------------------------**** EMPLEADOS DE HILAGRO
  -------------------------------------------------------------------------TIENE EN CUENTA SI ES CLIENTE EN TRANASGRO


   IF V_CLIENTE IS NOT NULL THEN
       BEGIN
            ---TRAE EL HOLDING DEL CLIENTE
            SELECT CLI_COD_FICHA_HOLDING, CLI_NOM, CLI_RUC, CLI_RUC_DV
              INTO V_COD_HOLDING, V_CLI_NOM, V_CLI_RUC, V_CLI_RUC_DV
              FROM FIN_CLIENTE T
             WHERE T.CLI_CODIGO = V_CLIENTE
               AND T.CLI_EMPR = 1;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              -- Cuando no lo encuentra por codigo de cliente, lo busca por holding
            begin
             SELECT CLI_COD_FICHA_HOLDING, CLI_NOM, CLI_RUC, CLI_RUC_DV
                INTO V_COD_HOLDING, V_CLI_NOM, V_CLI_RUC, V_CLI_RUC_DV
                FROM FIN_CLIENTE T
               WHERE T.CLI_COD_FICHA_HOLDING = V_CLIENTE
                 AND T.CLI_EMPR = 1;
               EXCEPTION
            WHEN NO_DATA_FOUND THEN
                null;
             end ;
          END;

          DECLARE
            MARCA VARCHAR(1);
          BEGIN

            SELECT 'X'
              INTO MARCA
              FROM FIN_CLI_LIST_NEGRA FCL
             WHERE (FCL.CLISNE_CI_RUC LIKE V_CLI_RUC OR
                   FCL.CLISNE_CI_RUC LIKE V_CLI_RUC_DV OR
                   FCL.CLISNE_CI LIKE V_CLI_RUC OR
                   FCL.CLISNE_CI LIKE V_CLI_RUC_DV OR
                   FCL.CLISNE_NOMBRE LIKE UPPER(V_CLI_NOM)
                   -- OR fcl.CLISNE_PROPIE LIKE UPPER(V_CLI_NOM)
                   )
               AND FCL.CLISNE_EMPR = 1;

            IF MARCA = 'X' THEN
              V_IMP_LIM_CR_EMPR    := 0;
              V_IMP_LIM_DISP_EMPR  := 0;
              V_IMP_LIM_DISP_GRUPO := 0;
              RETURN;
            ELSE
              NULL;
            END IF;

          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              NULL;
            WHEN TOO_MANY_ROWS THEN
              V_IMP_LIM_CR_EMPR    := 0;
              V_IMP_LIM_DISP_EMPR  := 0;
              V_IMP_LIM_DISP_GRUPO := 0;
              RETURN;
          END;

          ---SI EL HOLDING DEL CLIENTE TIENE CHEQUES RECHASADO, RETENIDOS O PRESTAMOS VENCIDOS SUS DISPONIBILIDADES SE QUEDAN EN 0
          DECLARE
            MAR_CHEQUES NUMBER;

          BEGIN
            MAR_CHEQUES := 0;
            MAR_AUT_ESP := 0;

            IF V_IMP_LIM_CR_EMPR = 0 AND MAR_AUT_ESP = 0 THEN
              V_IMP_LIM_CR_EMPR    := 0;
              V_IMP_LIM_DISP_EMPR  := 0;
              V_IMP_LIM_DISP_GRUPO := 0;
              RETURN;
            END IF;

            SELECT SUM(CUO.CUO_SALDO_LOC)
              INTO V_PRESTAMOS
              FROM FIN_DOCUMENTO, FIN_CUOTA CUO, FIN_CLIENTE C
             WHERE DOC_CLAVE = CUO_CLAVE_DOC
               AND DOC_EMPR = CUO_EMPR
               AND DOC_EMPR = 1

               AND DOC_CLI = C.CLI_CODIGO
               AND DOC_EMPR = C.CLI_EMPR

               AND C.CLI_COD_FICHA_HOLDING = V_COD_HOLDING

               AND DOC_TIPO_MOV IN (71, 72)
               AND CUO_SALDO_LOC > 0
               AND CUO_FEC_VTO < V_FEC_DOC;

            IF MAR_AUT_ESP = 0 THEN
              --- SI TIENE PERMISO ESPECIAL NO REALIZA EL CONTROL DE CHEQUES Y PRESTAMOS

              SELECT COUNT(CH.CHEQ_CLAVE)
                INTO MAR_CHEQUES
                FROM FIN_CHEQUE CH, FIN_CLIENTE FC
               WHERE CH.CHEQ_CLI = FC.CLI_CODIGO
                 AND CH.CHEQ_EMPR = FC.CLI_EMPR
                 AND CH.CHEQ_EMPR = 1

                 AND FC.CLI_COD_FICHA_HOLDING = V_COD_HOLDING
                 AND CH.CHEQ_SITUACION IN ('R', 'T');

              IF MAR_CHEQUES > 0  THEN
                V_IMP_LIM_CR_EMPR    := 0;
                V_IMP_LIM_DISP_EMPR  := 0;
                V_IMP_LIM_DISP_GRUPO := 0;
                RETURN;
              END IF;

            END IF;

          END;
          ----------------------------------------------------------------------------------------------------------------------------------

          ---TRAE LOS PENDIENTES EN CHEQUE
          --dbms_output.put_line(to_char(sysdate,'dd/mm/yyyy hh24:mi:ss' )||' 01');
          DECLARE
            CURSOR CHEQ_CUR IS
              SELECT CHEQ_EMPR, CHEQ_MON, CHEQ_IMPORTE, fin_buscar_cotizacion_empr(mon => CHEQ_MON,
                                            emp => CHEQ_EMPR) COT_TASA, CHEQ_SITUACION
                FROM FIN_CHEQUE, STK_COTIZACION, FIN_CLIENTE C
               WHERE C.CLI_CODIGO = CHEQ_CLI
                 AND C.CLI_EMPR = CHEQ_EMPR
                 AND CHEQ_EMPR = 1

                 AND C.CLI_COD_FICHA_HOLDING = V_COD_HOLDING

                 AND CHEQ_SITUACION NOT IN ('C', 'A', 'D')
                 AND CHEQ_MON = COT_MON(+)
                 AND CHEQ_EMPR = COT_EMPR(+)

                 AND COT_FEC(+) = V_FEC_DOC;

          BEGIN

            FOR RCHEQ IN CHEQ_CUR LOOP
              IF RCHEQ.CHEQ_MON = V_MON_LOC THEN
                V_TASA := 1;
              ELSIF RCHEQ.COT_TASA IS NULL OR RCHEQ.COT_TASA = 0 THEN
                RAISE_APPLICATION_ERROR(-20202,
                                        'Debe ingresar la cotizacion del dia:' ||
                                        TO_CHAR(V_FEC_DOC, 'DD-MM-YYYY') ||
                                        ' para la moneda:' ||
                                        TO_CHAR(RCHEQ.CHEQ_MON));
              ELSE
                V_TASA := RCHEQ.COT_TASA;
              END IF;

              IF RCHEQ.CHEQ_SITUACION = 'R' THEN
                V_IMP_CHEQ_RECHAZADO := V_IMP_CHEQ_RECHAZADO +
                                        RCHEQ.CHEQ_IMPORTE * V_TASA;
              ELSIF RCHEQ.CHEQ_SITUACION IN ('I', 'T') THEN
                V_IMP_CHEQ_DIFERIDO := V_IMP_CHEQ_DIFERIDO +
                                       RCHEQ.CHEQ_IMPORTE * V_TASA;
              END IF;
            END LOOP;
          END;
          --dbms_output.put_line(to_char(sysdate,'dd/mm/yyyy hh24:mi:ss' )||' 02');

        /*  SELECT NVL(SUM(DECODE(M.TMOV_IND_DBCR_CTA,
                                'C',
                                (DOC_SALDO_LOC) * -1,
                                DOC_SALDO_LOC)),
                     0) PENDIENTES
            INTO V_PENDIENTES
            FROM FIN_DOCUMENTO, GEN_TIPO_MOV M, FIN_CLIENTE
           WHERE DOC_TIPO_MOV = TMOV_CODIGO
             AND DOC_EMPR = TMOV_EMPR
             AND DOC_EMPR = 1
             AND DOC_TIPO_MOV NOT IN
                 (9, 11, 70, 71, 72, 22, 25, 12, 13, 48, 47, 24, 16,75,8)
             AND DOC_CLI = CLI_CODIGO
             AND CLI_COD_FICHA_HOLDING = V_COD_HOLDING
             AND DOC_SALDO_MON > 0
             AND FIN_DOCUMENTO.DOC_EMPR = FIN_CLIENTE.CLI_EMPR;*/

            SELECT sum(CUO_SALDO_MON)
           INTO V_PENDIENTES
            FROM FIN_DOCUMENTO, GEN_TIPO_MOV M, FIN_CLIENTE, fin_cuota a
           WHERE DOC_TIPO_MOV = TMOV_CODIGO
             AND DOC_EMPR = TMOV_EMPR
             AND DOC_EMPR = 1
             and doc_clave = a.cuo_clave_doc
             and doc_Empr = a.cuo_empr
             AND DOC_TIPO_MOV NOT IN
                 (9, 11, 70, 71, 72, 22, 25, 12, 13, 48, 47, 24, 16,75,8)
             AND DOC_CLI = CLI_CODIGO
           --  and doc_cli = 3568
             and a.cuo_fec_vto <= V_FEC_DOC---'31/01/2021'
             and a.CUO_SALDO_MON >0
             AND CLI_COD_FICHA_HOLDING = V_COD_HOLDING---V_COD_HOLDING
            -- AND DOC_SALDO_MON > 0
           --  and
             AND FIN_DOCUMENTO.DOC_EMPR = FIN_CLIENTE.CLI_EMPR ;

   END IF;


   ------------------------------*** ADELANTO PROVEEDOR
             SELECT  SUM(CUO_SALDO_MON)
               INTO V_CUO_PROVEEDOR
              FROM FIN_DOCUMENTO   A,
                   FIN_CUOTA       B,
                   fin_doc_concepto s,
                   FIN_CONCEPTO     C
             WHERE A.DOC_CLAVE = B.CUO_CLAVE_DOC
               AND A.DOC_EMPR = B.CUO_EMPR
               AND A.DOC_PROV = V_COD_PROVEEDOR
               AND DCON_CLAVE_DOC  = DOC_CLAVE
                AND S.DCON_EMPR = CUO_EMPR
                
                AND S.DCON_CLAVE_CONCEPTO = C.FCON_CLAVE
                AND S.DCON_EMPR           = C.FCON_EMPR
                
                AND NVL(C.FCON_IND_AFECTA_LINEA,'S') = 'S'
                
                ---AND S.DCON_CLAVE_CONCEPTO NOT IN (1493,9498)
                ---AND S.DCON_CLAVE_CONCEPTO <>1493
               AND DOC_TIPO_MOV IN( 81,31)---= 31
               AND B.CUO_FEC_VTO <= V_FEC_DOC
               AND TRUNC(CUO_SALDO_MON) > 0
               AND DOC_EMPR = 1;

 ------------------------------*** ADELANTO PROVEEDOR PENDIENTE DE APROBACION
         SELECT  NVL(SUM(CUO_IMP_MON),0)
             INTO V_CUO_PROVEEDOR_TEMPORAL
              FROM FIN_DOCUMENTO_COMI015_TEMP   A,
                   FIN_CUOTA_COMI015_TEMP       B,
                   FIN_DOC_CONCEPTO_COMI015_TEMP S,
                   FIN_CONCEPTO     C
             WHERE A.DOC_CLAVE = B.CUO_CLAVE_DOC
               AND A.DOC_EMPR = B.CUO_EMPR
               AND A.DOC_PROV = V_COD_PROVEEDOR
               AND DOC_TIPO_MOV IN( 81,31)
               AND B.CUO_FEC_VTO <= V_FEC_DOC
                AND DCON_CLAVE_DOC  = DOC_CLAVE
                AND S.DCON_EMPR = CUO_EMPR
                
                
                AND S.DCON_CLAVE_CONCEPTO = C.FCON_CLAVE
                AND S.DCON_EMPR           = C.FCON_EMPR
                
                AND NVL(C.FCON_IND_AFECTA_LINEA,'S') = 'S'
                
             --   AND S.DCON_CLAVE_CONCEPTO NOT IN (1493,9498)
                --AND S.DCON_CLAVE_CONCEPTO <>1493
               and a.doc_fec_doc >= '01/'||to_char(TO_DATE(V_FEC_DOC),'MM/YYYY')
               AND NVL(A.COMI005_ESTADO,'P') = 'P'
               AND DOC_EMPR = 1;



          SELECT NVL(SUM(B.PDDET_IMP),0)
                INTO V_VACACION
                FROM PER_DOCUMENTO A, PER_DOCUMENTO_DET B, PER_CONCEPTO C
               WHERE A.PDOC_CLAVE = B.PDDET_CLAVE_DOC
                 AND A.PDOC_EMPR = B.PDDET_EMPR
                 AND B.PDDET_CLAVE_CONCEPTO = C.PCON_CLAVE
                 AND B.PDDET_EMPR = C.PCON_EMPR
                 AND A.PDOC_EMPR = 1
                 AND PDOC_FEC BETWEEN '01/' || TO_CHAR(TO_DATE(V_FEC_DOC), 'MM/YYYY') AND V_FEC_DOC
                 AND C.PCON_CLAVE in( 6)--14,
                 AND A.PDOC_EMPLEADO =V_EMPLEADO;




   ------------------VERIFICAMOS SI EL EMPLEADO ES CLIENTE EN TRANSAGRO


      BEGIN
        SELECT CLI_CODIGO
          INTO V_COD_EMPL_TRANSAGRO
          FROM FIN_CLIENTE
         WHERE CLI_COD_EMPL_EMPR_ORIG = V_EMPLEADO
           AND CLI_CATEG IN (4)
           AND CLI_EMPR = 2;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
         WHEN TOO_MANY_ROWS THEN
          NULL;
       END ;


   IF V_COD_EMPL_TRANSAGRO IS NOT NULL THEN

   SELECT NVL(SUM(CUO_IMP_MON),0) IMPORTE-----------------------------------EL IMPORTE DE LAS DEUDAS DE LOS EMPLEADOS DE HILAGRO EN TRANSAGRO
      INTO V_IMP_HILAGRO_TRAN
      FROM  (
      SELECT NVL(E.CUO_SALDO_MON,0) CUO_IMP_MON,DOC_TIPO_MOV, DOC_CLI
                FROM FIN_DOCUMENTO C,
                     FIN_CUOTA     E
               WHERE C.DOC_EMPR = 2
                 AND C.DOC_CLAVE = E.CUO_CLAVE_DOC
                 AND C.DOC_EMPR = E.CUO_EMPR
                 AND  CUO_FEC_VTO <= V_FEC_DOC
                 AND C.DOC_SALDO_MON > 0
                 AND E.CUO_SALDO_MON > 0
                 AND C.DOC_MON = 1
                 AND C.DOC_CLI = V_COD_EMPL_TRANSAGRO
              UNION ALL
              SELECT NVL(E.CUO_SALDO_MON,0) CUO_IMP_MON, DOC_TIPO_MOV, DOC_PROV
                FROM FIN_DOCUMENTO C,
                     FIN_CUOTA     E
               WHERE C.DOC_EMPR = 2
                 AND C.DOC_CLAVE = E.CUO_CLAVE_DOC
                 AND C.DOC_EMPR = E.CUO_EMPR
                 AND E.CUO_FEC_VTO <=V_FEC_DOC
                 AND C.DOC_SALDO_MON > 0
                 AND E.CUO_SALDO_MON > 0
                 AND C.DOC_TIPO_MOV IN(31)--<> 26-- 31
                 AND C.DOC_MON = 1
                 AND C.DOC_PROV = V_COD_EMPL_TRANSAGRO);




    END IF;


         /*  V_TOT_SALDO_GRUP_LOC := V_IMP_CHEQ_RECHAZADO       +   V_IMP_CHEQ_DIFERIDO       +
                                   NVL(V_PENDIENTES, 0)       +   NVL(V_PRESTAMOS, 0)       +
                                   NVL(V_CUO_PROVEEDOR,0)     +   NVL(V_IMP_HILAGRO_TRAN,0) + NVL(V_CUO_PROVEEDOR_TEMPORAL,0) ;*/



           V_TOT_SALDO_GRUP_LOC := V_IMP_CHEQ_RECHAZADO       +   V_IMP_CHEQ_DIFERIDO       +
                                   NVL(V_PENDIENTES, 0)       +   NVL(V_PRESTAMOS, 0)       +
                                   NVL(V_CUO_PROVEEDOR,0)     +   NVL(V_IMP_HILAGRO_TRAN,0) +NVL(V_CUO_PROVEEDOR_TEMPORAL,0)+ NVL(V_VACACION,0);

          IF V_TOT_SALDO_GRUP_LOC > V_IMP_LIM_CR_EMPR THEN
            V_IMP_LIM_DISP_GRUPO := 0;
          ELSE
            --V_IMP_LIM_DISP_GRUPO := V_LIM_GRUPO - V_TOT_SALDO_GRUP_LOC;
            V_IMP_LIM_DISP_GRUPO := V_IMP_LIM_CR_EMPR - V_TOT_SALDO_GRUP_LOC;

          END IF;



          V_IMP_LIM_DISP_EMPR := V_IMP_LIM_DISP_GRUPO ;

    END IF;


END;
/
