CREATE OR REPLACE PROCEDURE FIN_LIM_CREDITO(V_FEC_DOC            IN DATE,
                                            V_CLI                IN NUMBER,
                                            V_IMP_LIM_CR_EMPR    IN OUT NUMBER,
                                            V_EMPR               IN NUMBER,
                                            V_IMP_LIM_DISP_GRUPO IN OUT NUMBER,
                                            V_IMP_LIM_DISP_EMPR  IN OUT NUMBER,
                                            V_IMP_CHEQ_DIFERIDO  IN OUT NUMBER,
                                            V_IMP_CHEQ_RECHAZADO IN OUT NUMBER) IS

  CURSOR GRUP_CUR IS
    SELECT CLEM_EMPR,
           CLEM_SALDO_ACT_TENT_MON,
           CLEM_IMP_LIM_CR,
           CLEM_MON,
           COT_TASA
      FROM STK_COTIZACION, FIN_CLI_EMPRESA
     WHERE COT_MON(+) = CLEM_MON
       AND COT_EMPR(+) = CLEM_EMPR
       AND COT_FEC(+) = V_FEC_DOC       
       AND CLEM_EMPR = V_EMPR
       AND CLEM_CLI = V_CLI;

  CURSOR CHEQ_CUR IS
    SELECT CHEQ_EMPR, CHEQ_MON, CHEQ_IMPORTE, COT_TASA, CHEQ_SITUACION
      FROM FIN_CHEQUE, STK_COTIZACION
     WHERE CHEQ_CLI = V_CLI
       AND CHEQ_EMPR = COT_EMPR
       AND CHEQ_EMPR = V_EMPR
       AND CHEQ_SITUACION NOT IN ('C', 'A')
       AND CHEQ_MON = COT_MON(+)
       AND COT_FEC(+) = V_FEC_DOC;

  V_TASA               NUMBER;
  V_TOT_SALDO_GRUP_LOC NUMBER := 0;
  V_MON_LOC            NUMBER := 1;
  V_PENDIENTES         NUMBER := 0;
  V_PRESTAMOS          NUMBER := 0;
  V_LIM_GRUPO          NUMBER := 0;
  V_TOT_SALDO_EMPR_LOC NUMBER := 0;

  V_COD_HOLDING NUMBER := 0;

  V_CLI_NOM    VARCHAR2(500);
  V_CLI_RUC    VARCHAR2(500);
  V_CLI_RUC_DV VARCHAR2(500);
  MAR_AUT_ESP  NUMBER;
BEGIN
  V_IMP_LIM_DISP_GRUPO := 0;
  V_IMP_LIM_DISP_EMPR  := 0;
  V_IMP_CHEQ_DIFERIDO  := 0;
  V_IMP_CHEQ_RECHAZADO := 0;
  V_IMP_LIM_CR_EMPR    := 0;

  IF V_EMPR = 2 then --@TAGRO
    SELECT CONF_MON_LOC
      INTO V_MON_LOC
      FROM GEN_CONFIGURACION
     WHERE CONF_EMPR = V_EMPR;
  
    SELECT CLI_IMP_LIM_CR
      INTO V_LIM_GRUPO
      FROM FIN_CLIENTE
     WHERE CLI_CODIGO = V_CLI
       AND CLI_EMPR = V_EMPR;
  
    -- hallar el limite de credito disponible del grupo empresarial
    -- y el limite de credito de la empresa
    FOR RGRUP IN GRUP_CUR LOOP
      IF RGRUP.CLEM_MON = V_MON_LOC THEN
        V_TASA := 1;
      ELSIF RGRUP.COT_TASA IS NULL OR RGRUP.COT_TASA = 0 THEN
        RAISE_APPLICATION_ERROR(-20101,
                                'Debe ingresar la cotizacion del dia:' ||
                                TO_CHAR(V_FEC_DOC, 'DD-MM-YYYY') ||
                                ' para la moneda:' ||
                                TO_CHAR(RGRUP.CLEM_MON));
      ELSE
        V_TASA := RGRUP.COT_TASA;
      END IF;
    
      V_TOT_SALDO_GRUP_LOC := V_TOT_SALDO_GRUP_LOC +
                              RGRUP.CLEM_SALDO_ACT_TENT_MON * V_TASA;
    
      IF RGRUP.CLEM_EMPR = V_EMPR THEN
        V_IMP_LIM_CR_EMPR := V_IMP_LIM_CR_EMPR +
                             RGRUP.CLEM_IMP_LIM_CR * V_TASA;
      
        V_TOT_SALDO_EMPR_LOC := V_TOT_SALDO_EMPR_LOC +
                                RGRUP.CLEM_SALDO_ACT_TENT_MON * V_TASA;
      END IF;
    END LOOP;
  
    -- Para calcular el monto en cheques diferidos.
    FOR RCHEQ IN CHEQ_CUR LOOP
      IF RCHEQ.CHEQ_MON = V_MON_LOC THEN
        V_TASA := 1;
      ELSIF RCHEQ.COT_TASA IS NULL OR RCHEQ.COT_TASA = 0 THEN
        RAISE_APPLICATION_ERROR(-20201,
                                'Debe ingresar la cotizacion del dia:' ||
                                TO_CHAR(V_FEC_DOC, 'DD-MM-YYYY') ||
                                ' para la moneda:' ||
                                TO_CHAR(RCHEQ.CHEQ_MON));
      ELSE
        V_TASA := RCHEQ.COT_TASA;
      END IF;
      V_TOT_SALDO_GRUP_LOC := V_TOT_SALDO_GRUP_LOC +
                              RCHEQ.CHEQ_IMPORTE * V_TASA;
    
      IF RCHEQ.CHEQ_EMPR = V_EMPR THEN
        V_TOT_SALDO_EMPR_LOC := V_TOT_SALDO_EMPR_LOC +
                                RCHEQ.CHEQ_IMPORTE * V_TASA;
      END IF;
      IF RCHEQ.CHEQ_SITUACION NOT IN ('D', 'I') THEN
        V_IMP_CHEQ_RECHAZADO := V_IMP_CHEQ_RECHAZADO +
                                RCHEQ.CHEQ_IMPORTE * V_TASA;
      ELSE
        V_IMP_CHEQ_DIFERIDO := V_IMP_CHEQ_DIFERIDO +
                               RCHEQ.CHEQ_IMPORTE * V_TASA;
      END IF;
    END LOOP;
  
    IF V_TOT_SALDO_GRUP_LOC > V_LIM_GRUPO THEN
      V_IMP_LIM_DISP_GRUPO := 0;
    ELSE
      V_IMP_LIM_DISP_GRUPO := V_LIM_GRUPO - V_TOT_SALDO_GRUP_LOC;
    END IF;
  
    IF V_TOT_SALDO_EMPR_LOC > V_IMP_LIM_CR_EMPR THEN
      V_IMP_LIM_DISP_EMPR := 0;
    ELSE
      V_IMP_LIM_DISP_EMPR := V_IMP_LIM_CR_EMPR - V_TOT_SALDO_EMPR_LOC;
    END IF;
  else --> DISTINTOS A @TAGRO
    BEGIN
      ---TRAE EL HOLDING DEL CLIENTE
      SELECT CLI_COD_FICHA_HOLDING, CLI_NOM, CLI_RUC, CLI_RUC_DV
        INTO V_COD_HOLDING, V_CLI_NOM, V_CLI_RUC, V_CLI_RUC_DV
        FROM FIN_CLIENTE T
       WHERE T.CLI_CODIGO = V_CLI
         AND T.CLI_EMPR = V_EMPR;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        -- Cuando no lo encuentra por codigo de cliente, lo busca por holding
        SELECT CLI_COD_FICHA_HOLDING, CLI_NOM, CLI_RUC, CLI_RUC_DV
          INTO V_COD_HOLDING, V_CLI_NOM, V_CLI_RUC, V_CLI_RUC_DV
          FROM FIN_CLIENTE T
         WHERE T.CLI_COD_FICHA_HOLDING = V_CLI
           AND T.CLI_EMPR = V_EMPR;
    END;
  
    DECLARE
      MARCA VARCHAR(1);
    BEGIN
    
      SELECT 'X'
        INTO MARCA
        FROM FIN_CLI_LIST_NEGRA FCL
       WHERE (FCL.CLISNE_CI_RUC LIKE V_CLI_RUC OR
             FCL.CLISNE_CI_RUC LIKE V_CLI_RUC_DV OR
             FCL.CLISNE_CI LIKE V_CLI_RUC OR
             FCL.CLISNE_CI LIKE V_CLI_RUC_DV OR
             FCL.CLISNE_NOMBRE LIKE UPPER(V_CLI_NOM)
             -- OR fcl.CLISNE_PROPIE LIKE UPPER(V_CLI_NOM)
             )
         AND FCL.CLISNE_EMPR = V_EMPR;
    
      IF MARCA = 'X' THEN
        V_IMP_LIM_CR_EMPR    := 0;
        V_IMP_LIM_DISP_EMPR  := 0;
        V_IMP_LIM_DISP_GRUPO := 0;
        RETURN;
      ELSE
        NULL;
      END IF;
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
      WHEN TOO_MANY_ROWS THEN
        V_IMP_LIM_CR_EMPR    := 0;
        V_IMP_LIM_DISP_EMPR  := 0;
        V_IMP_LIM_DISP_GRUPO := 0;
        RETURN;
    END;
  
    ---SI EL HOLDING DEL CLIENTE TIENE CHEQUES RECHASADO, RETENIDOS O PRESTAMOS VENCIDOS SUS DISPONIBILIDADES SE QUEDAN EN 0
    DECLARE
      MAR_CHEQUES NUMBER;
    
    BEGIN
      MAR_CHEQUES := 0;
      MAR_AUT_ESP := 0;
    
      SELECT COUNT(FA.AUES_CLI)
        INTO MAR_AUT_ESP
        FROM FIN_AUTORIZ_ESPEC FA, FIN_CLIENTE FC
       WHERE FA.AUES_CLI = FC.CLI_CODIGO
         AND FA.AUES_EMPR = FC.CLI_EMPR
         AND FA.AUES_EMPR = V_EMPR
            
         AND FC.CLI_COD_FICHA_HOLDING = V_COD_HOLDING
         AND FA.AUES_FEC_AUTORIZ BETWEEN V_FEC_DOC - 1 AND V_FEC_DOC
         AND FA.AUES_UTILIZADA = 'N';
    
      ---TRAE EL PRESUPUESTO ASIGNADO Y TOTAL DE SALDO DEL HOLDING
      BEGIN
        SELECT HOL_LIMITE_CRED
          INTO V_IMP_LIM_CR_EMPR
          FROM FIN_FICHA_HOLDING
         WHERE HOL_CODIGO = V_COD_HOLDING
           AND HOL_EMPR = V_EMPR;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          RAISE_APPLICATION_ERROR(-20202,
                                  'El cliente no pertenece a ningun holding');
      END;
    
      IF V_IMP_LIM_CR_EMPR = 0 AND MAR_AUT_ESP = 0 THEN
        V_IMP_LIM_CR_EMPR    := 0;
        V_IMP_LIM_DISP_EMPR  := 0;
        V_IMP_LIM_DISP_GRUPO := 0;
        RETURN;
      END IF;
    
      SELECT SUM(CUO.CUO_SALDO_LOC)
        INTO V_PRESTAMOS
        FROM FIN_DOCUMENTO, FIN_CUOTA CUO, FIN_CLIENTE C
       WHERE DOC_CLAVE = CUO_CLAVE_DOC
         AND DOC_EMPR = CUO_EMPR
            
         AND DOC_CLI = C.CLI_CODIGO
         AND DOC_EMPR = C.CLI_EMPR
            
         AND C.CLI_COD_FICHA_HOLDING = V_COD_HOLDING
         AND C.CLI_EMPR = V_EMPR
            
         AND DOC_TIPO_MOV IN (71, 72)
         AND CUO_SALDO_LOC > 0
         AND CUO_FEC_VTO < V_FEC_DOC;
    
      IF MAR_AUT_ESP = 0 THEN
        --- SI TIENE PERMISO ESPECIAL NO REALIZA EL CONTROL DE CHEQUES Y PRESTAMOS
      
        SELECT COUNT(CH.CHEQ_CLAVE)
          INTO MAR_CHEQUES
          FROM FIN_CHEQUE CH, FIN_CLIENTE FC
         WHERE CH.CHEQ_CLI = FC.CLI_CODIGO
           AND CH.CHEQ_EMPR = FC.CLI_EMPR
           AND CH.CHEQ_EMPR = V_EMPR
              
           AND FC.CLI_COD_FICHA_HOLDING = V_COD_HOLDING
           AND CH.CHEQ_SITUACION IN ('R', 'T');
      
        IF MAR_CHEQUES > 0 OR V_PRESTAMOS > 0 THEN
          V_IMP_LIM_CR_EMPR    := 0;
          V_IMP_LIM_DISP_EMPR  := 0;
          V_IMP_LIM_DISP_GRUPO := 0;
          RETURN;
        END IF;
      
      END IF;
    
    END;
    ----------------------------------------------------------------------------------------------------------------------------------
  
    ---TRAE LOS PENDIENTES EN CHEQUE
    --dbms_output.put_line(to_char(sysdate,'dd/mm/yyyy hh24:mi:ss' )||' 01');
    DECLARE
      CURSOR CHEQ_CUR IS
        SELECT CHEQ_EMPR, CHEQ_MON, CHEQ_IMPORTE, COT_TASA, CHEQ_SITUACION
          FROM FIN_CHEQUE, STK_COTIZACION, FIN_CLIENTE C
         WHERE C.CLI_CODIGO = CHEQ_CLI
           AND C.CLI_EMPR = CHEQ_EMPR
           
           AND C.CLI_COD_FICHA_HOLDING = V_COD_HOLDING
           AND C.CLI_EMPR = V_EMPR
              
           AND CHEQ_SITUACION NOT IN ('C', 'A', 'D')
           AND CHEQ_MON = COT_MON(+)
           AND CHEQ_EMPR = COT_EMPR(+)
              
           AND COT_FEC(+) = V_FEC_DOC;
    
    BEGIN
    
      FOR RCHEQ IN CHEQ_CUR LOOP
        IF RCHEQ.CHEQ_MON = V_MON_LOC THEN
          V_TASA := 1;
        ELSIF RCHEQ.COT_TASA IS NULL OR RCHEQ.COT_TASA = 0 THEN
          RAISE_APPLICATION_ERROR(-20202,
                                  'Debe ingresar la cotizacion del dia:' ||
                                  TO_CHAR(V_FEC_DOC, 'DD-MM-YYYY') ||
                                  ' para la moneda:' ||
                                  TO_CHAR(RCHEQ.CHEQ_MON));
        ELSE
          V_TASA := RCHEQ.COT_TASA;
        END IF;
      
        IF RCHEQ.CHEQ_SITUACION = 'R' THEN
          V_IMP_CHEQ_RECHAZADO := V_IMP_CHEQ_RECHAZADO +
                                  RCHEQ.CHEQ_IMPORTE * V_TASA;
        ELSIF RCHEQ.CHEQ_SITUACION IN ('I', 'T') THEN
          V_IMP_CHEQ_DIFERIDO := V_IMP_CHEQ_DIFERIDO +
                                 RCHEQ.CHEQ_IMPORTE * V_TASA;
        END IF;
      END LOOP;
    END;
    --dbms_output.put_line(to_char(sysdate,'dd/mm/yyyy hh24:mi:ss' )||' 02');
  
    SELECT NVL(SUM(DECODE(M.TMOV_IND_DBCR_CTA,
                          'C',
                          (DOC_SALDO_LOC) * -1,
                          DOC_SALDO_LOC)),
               0) PENDIENTES
      INTO V_PENDIENTES
      FROM FIN_DOCUMENTO, GEN_TIPO_MOV M, FIN_CLIENTE
     WHERE DOC_TIPO_MOV = TMOV_CODIGO
       AND DOC_EMPR = TMOV_EMPR
       
       AND DOC_TIPO_MOV NOT IN (9, 11, 70, 71, 72, 22, 25, 12, 13, 48, 47, 24, /*16,*/75,8)-- tm16 tiene en cuenta en el 2-3-20
       AND DOC_SALDO_MON > 0
       
       AND DOC_CLI = CLI_CODIGO
       AND DOC_EMPR = CLI_EMPR
       
       AND CLI_COD_FICHA_HOLDING = V_COD_HOLDING
       AND CLI_EMPR = V_EMPR
       
       ;
  
    --V_TOT_SALDO_GRUP_LOC:=V_IMP_CHEQ_RECHAZADO+V_IMP_CHEQ_DIFERIDO+nvl(v_PENDIENTES,0)+ fin_interes_factura(v_cli); --suma los intereses por pagos atrasados.
    V_TOT_SALDO_GRUP_LOC := V_IMP_CHEQ_RECHAZADO + V_IMP_CHEQ_DIFERIDO +
                            NVL(V_PENDIENTES, 0) + NVL(V_PRESTAMOS, 0);
  
    IF V_TOT_SALDO_GRUP_LOC > V_IMP_LIM_CR_EMPR THEN
      V_IMP_LIM_DISP_GRUPO := 0;
    ELSE
      --V_IMP_LIM_DISP_GRUPO := V_LIM_GRUPO - V_TOT_SALDO_GRUP_LOC;
      V_IMP_LIM_DISP_GRUPO := V_IMP_LIM_CR_EMPR - V_TOT_SALDO_GRUP_LOC;
    
    END IF;
  
    --------
    V_IMP_LIM_DISP_EMPR := V_IMP_LIM_DISP_GRUPO;
  
  END IF;
  --dbms_output.put_line(to_char(sysdate,'dd/mm/yyyy hh24:mi:ss' )||' 06');
  --------
END;
/
